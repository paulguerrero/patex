function compilePatternedit(options)
    
    if nargin < 1
        options = {};
    end
    
    utilpath = 'mlutil';
    
    % unload all currently loaded mex files
    clear mex; %#ok<CLMEX>
    
    % compile
    currentfolder = pwd;
    try
        disp('compiling utility functions...');
        cd mlutil;
        compileMlutil(options);
        cd(currentfolder);

        disp('compiling geometry functions...');
        cd mlgeometry;
        compileMlgeometry(options,['../',utilpath]);
        cd(currentfolder);
    catch err
        cd(currentfolder);
        if exist('precompiled.conf','file')
            delete('precompiled.conf')
        end
        rethrow(err);
    end
    
    % update precompiled info
    updatePrecompiledInfo('precompiled.conf',{},...
        'C++');
    
    disp('Everything compiled.');
end
