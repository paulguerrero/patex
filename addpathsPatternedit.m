function addpathsPatternedit

    paths = fullfile(pwd,{...
        'mlutil',... % utility functions
        'mlgeometry',... % geometry functions
        'mlcircular',... % circular mean
        'mluiutil',... % for user interface utilities
        'mluiwidget',... % for user interface utilities
        'mlsvg',... % to read/write svg files
        'mlscene',... % scene elements
        'relations',... % for geometric relationships
        'uiwidgets',... % for user interface widgets, such as the editors
        'derivest',... % derivative estimation (including jacobian, hessian, etc.)
        });
    
    addpath(paths{:});
end
