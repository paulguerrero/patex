classdef ChooserelsPropagationSolver < PatternSolver
    
properties(SetAccess=protected)
    % last inputs
    newelmposes = zeros(2,0);
    
    % last solution
    elmposes = zeros(2,0);
    relvals = zeros(1,0);
end

properties(Access=protected)
    oldelmposes = zeros(2,0);
    targetrelvals = zeros(1,0);
    targetelmposes = zeros(2,0);
    relweights = zeros(1,0);
    elmweights = zeros(1,0);

    choicestates = [];
    choicevisitednodesmask = [];
    choicechangednodesmask = [];
    choicequeuednodeops = cell(1,0);
%     choiceopenmask = zeros(1,0);
    choicecost = zeros(1,0);    
    choicetree = sparse([]);
    
    fullstate = zeros(0,1);
    substateinds = zeros(0,1);
    
    % just for visualization
    currentnodeinds = zeros(1,0);
    currentvisitedmask = zeros(0,1);
    currentchangedmask = zeros(0,1);
    
    visfunc = [];
end

methods

% obj = ChooserelsPropagationSolver
% obj = ChooserelsPropagationSolver(pgraph)
function obj = ChooserelsPropagationSolver(varargin)
    superarginds = zeros(1,0);
    if numel(varargin) == 1
        superarginds = 1;
    end
    
    obj@PatternSolver(varargin{superarginds});
    varargin(superarginds) = [];
    
    if numel(varargin) == 0
        % do nothing
    else
        error('Invalid arguments.');
    end
end

function clear(obj)
    obj.clear@PatternSolver;
    
    obj.elmposes = zeros(2,0);
    obj.relvals = zeros(1,0);
    
    obj.oldelmposes = zeros(2,0);
    obj.targetrelvals = zeros(1,0);
    obj.targetelmposes = zeros(2,0);
    obj.relweights = zeros(1,0);
    obj.elmweights = zeros(1,0);

    obj.choicestates = [];
    obj.choicevisitednodesmask = [];
    obj.choicechangednodesmask = [];
    obj.choicequeuednodeops = cell(1,0);
%     obj.choiceopenmask = zeros(1,0);
    obj.choicecost = zeros(1,0);    
    obj.choicetree = sparse([]);
    
    obj.fullstate = zeros(0,1);
    obj.substateinds = zeros(0,1);
    
    obj.currentvisitedmask = zeros(0,1);
    obj.currentchangedmask = zeros(0,1);
end

function prepare(obj,newelmposes)

    obj.prepare@PatternSolver;
    
    elms = obj.pgraph.elms;
    rels = obj.pgraph.rels;
    
    obj.oldelmposes = [elms.pose];
    obj.oldelmposes = obj.oldelmposes(1:obj.elmposedim,:);
    
    obj.newelmposes = newelmposes;
    obj.newelmposes = obj.newelmposes(1:obj.elmposedim,:);
    
    obj.targetrelvals = nan(1,numel(rels));
    obj.targetelmposes = nan(obj.elmposedim,numel(elms));
    obj.relweights = ones(1,numel(rels));
    obj.elmweights = ones(1,numel(elms));
    
    userchangedelmmask = any(obj.oldelmposes ~= obj.newelmposes,1);
    obj.targetelmposes(:,userchangedelmmask) = obj.newelmposes(:,userchangedelmmask);
    obj.elmweights(userchangedelmmask) = 1000;
    
    obj.choicestates = zeros(numel(elms)*obj.elmposedim + numel(rels),0);
    obj.choicevisitednodesmask = false(size(obj.pgraph.adjacency,1),0);
    obj.choicechangednodesmask = false(size(obj.pgraph.adjacency,1),0);
    obj.choicequeuednodeops = cell(1,0);
%     obj.choiceopenmask = zeros(1,0);
    obj.choicecost = zeros(1,0);    
    obj.choicetree = sparse([]);
    
    obj.fullstate = nan(numel(elms)*obj.elmposedim + numel(rels),1);
    obj.substateinds = zeros(numel(elms)*obj.elmposedim + numel(rels),1);

    obj.currentvisitedmask = zeros(0,1);
    obj.currentchangedmask = zeros(0,1);
end

function stateinds = node2stateinds(obj,nodeind)
    statereloffset = numel(obj.pgraph.elms) * obj.elmposedim;
    if nodeind <= numel(obj.pgraph.elms)
        stateinds = (1:obj.elmposedim) + (nodeind-1)*obj.elmposedim;
    else
        stateinds = (nodeind - numel(obj.pgraph.elms)) + statereloffset;
    end
end

function [elmposes,relvals] = solve(obj,newelmposes)
    
    obj.clear;
    
    obj.prepare(newelmposes);
    
    elms = obj.pgraph.elms;
    rels = obj.pgraph.rels;
    
    elmposes = [elms.pose];
    elmposes = elmposes(1:obj.elmposedim,:);
    relvals = [rels.value];
    
    statereloffset = numel(elms) * obj.elmposedim;
    
    userchangedelmind = find(any(obj.oldelmposes ~= obj.newelmposes,1));

    if numel(userchangedelmind) > 1
        error('Only one changed element is supported for now.');
    elseif numel(userchangedelmind) == 0
        return;
    end
    
    visitedmask = false(1,numel(elms)+numel(rels));
    changedmask = false(1,numel(elms)+numel(rels));
    queuednodeops = zeros(1,numel(elms)+numel(rels));
    
    % starting from the element changed by the user
    startstate = cat(1,elmposes(:),relvals(:));
    startnodeind = userchangedelmind;
    
    queuednodeops(startnodeind) = 1;
    
    obj.choicestates(:,end+1) = startstate;
    obj.choicevisitednodesmask(:,end+1) = visitedmask;
    obj.choicechangednodesmask(:,end+1) = changedmask;
    obj.choicequeuednodeops{end+1} = queuednodeops;
    obj.choicecost(:,end+1) = nan;
    obj.choicetree(end+1,:) = 0;
    obj.choicetree(:,end+1) = 0;
    obj.choicetree(1,1) = 1;
    
    nodelevel = obj.pgraph.level;
    nodestateinds = cell(1,numel(elms)+numel(rels));
    for i=1:numel(elms)+numel(rels)
        nodestateinds{i} = obj.node2stateinds(i);
    end
    
    if not(obj.pgraph.balancedchildlevels)
        error('Only graph with balanced child levels are supported for now.');
        % otherwise getting all queued nodes of a single level does not work
    end
    
    % open choices have the state and parameters from before they are
    % executed, non-open choices have the state and parameters from
    % after they were executed

    while(true)
        choiceind = find(isnan(obj.choicecost),1,'first');
        
        if isempty(choiceind)
            % no more choices to be made, choice tree is done, leafs are the
            % possible interpretations
            break;
        end
        
        % get the state of the choice node
%         nodeind = obj.choicenodeind(:,choiceind);
        state =  obj.choicestates(:,choiceind);
        visitedmask = obj.choicevisitednodesmask(:,choiceind);
        changedmask = obj.choicechangednodesmask(:,choiceind);
        queuednodeops = obj.choicequeuednodeops{choiceind};
        
        % maximum error tolerance of the optimization per function (above
        % that, the constraints being optimized for will be considered to
        % be unfulfillable and the current choice will be flagged as
        % invalid)
        optimtolerance = 0.1;
        
        while any(queuednodeops ~= 0)
            
            nodeinds = find(queuednodeops ~= 0);
            nodeinds = nodeinds(nodelevel(nodeinds) == max(nodelevel(nodeinds)));
            
            choice = queuednodeops(nodeinds);
            queuednodeops(nodeinds) = 0;
            
            % show current state
            obj.fullstate = state;
            obj.substateinds = zeros(0,1);
            obj.currentnodeinds = nodeinds;
            obj.currentvisitedmask = visitedmask;
            obj.currentchangedmask = changedmask;
            obj.outfun(zeros(0,1),[],[]);
            
            parentnodeinds = cell(1,numel(nodeinds));
            childnodeinds = cell(1,numel(nodeinds));
            for i = 1:numel(nodeinds)
                parentnodeinds{i} = obj.pgraph.parents(nodeinds(i));
                childnodeinds{i} = obj.pgraph.childs(nodeinds(i));
            end

            optimizewithparentsmask = choice == 1 & cellfun(@(x) any(visitedmask(x)),parentnodeinds);
            optimizewithchildsmask = choice == 1 & not(optimizewithparentsmask) & cellfun(@(x) any(visitedmask(x)),childnodeinds);
            unconnectedmask = choice == 1 & not(optimizewithchildsmask) & not(optimizewithparentsmask);
            
            if any(optimizewithparentsmask)
                % optimize state of node with values of visited parents as
                % target for the relationship functions

                pnodeinds = unique(cat(1,parentnodeinds{optimizewithparentsmask}));
                pnodeinds = pnodeinds(visitedmask(pnodeinds));
                prelinds = pnodeinds - numel(elms);

                obj.substateinds = [nodestateinds{nodeinds(optimizewithparentsmask)}];
                obj.targetrelvals = nan(1,numel(rels));
                obj.targetrelvals(prelinds) = state(prelinds+statereloffset);
                obj.fullstate = state;
                
                startsubstate = obj.fullstate(obj.substateinds);
                
                if not(all(isnan(obj.targetrelvals)))
                    % optimize
                    [bestsubstate,val,exitflag,output,valgrad,~] = fminunc(...
                        @(x) obj.evalsubstate_nograd(x),startsubstate,...
                        optimoptions(@fminunc,...
                        'MaxFunEvals',100000,... % matlab default is 100*number of variables
                        'TolFun',1e-7,... % matlab default is 1e-6
                        'GradObj','off',...
                        'Diagnostics','on',...
                        'FunValCheck','on',...
                        'OutputFcn',@(state,optimValues,solverflag) obj.outfun(state,optimValues,solverflag),...
                        'DerivativeCheck','off'));
                     
                    if val > optimtolerance*numel(prelinds)
                        % above maximum error tolerance, could not find a
                        % valid state in this configuration, do not
                        % continue working on this choice
                        visitedmask(nodeinds) = true;
                        changedmask(nodeinds) = choice == 1;
                        obj.choicecost = inf;
                        break;
                    end
                
                end
                
                state(obj.substateinds) = bestsubstate;
            end
            
            if any(optimizewithchildsmask)
                % optimize state of node with values of visited childs as
                % target for the relationship functions
                % (= just re-compute the relatinoship function from the
                % state of its childs)

                optimizewithchildsinds = find(optimizewithchildsmask);
                for i=1:numel(optimizewithchildsinds)
                    nind = nodeinds(optimizewithchildsinds(i));
                    input1nodeind = find(obj.pgraph.adjacency(nind,:) == -1);
                    input2nodeind = find(obj.pgraph.adjacency(nind,:) == 1);
                    
                    input1stateinds = obj.node2stateinds(input1nodeind);
                    input2stateinds = obj.node2stateinds(input2nodeind);
                    
                    if input1nodeind <= numel(elms)
                        input1stateinds = reshape(input1stateinds,obj.elmposedim,[]);
                    end
                    if input2nodeind <= numel(elms)
                        input2stateinds = reshape(input2stateinds,obj.elmposedim,[]);
                    end

                    state(nodestateinds{nind}) = rels(nind-numel(elms)).compute_o(...
                        state(input1stateinds),...
                        state(input2stateinds));
                end
            end
            
            if any(unconnectedmask)
                if any(nodeinds(unconnectedmask) > numel(elms))
                    error('Non-element node with no visited parents or childs found..');
                end

                % set to new pose for that element or to new value for that
                % feature
                % also the root node (the user edit) is handled here
                unconnectedinds = find(unconnectedmask);
                for i=1:numel(unconnectedinds)
                    nind = nodeinds(unconnectedinds(i));
                    state(nodestateinds{nind}) = obj.newelmposes(1:obj.elmposedim,nind);
                end
            end

            visitedmask(nodeinds) = true;
            changedmask(nodeinds) = choice == 1;

            % queue next operations and branch choice tree if necessary
            for n=1:numel(nodeinds)
                if choice(n) == 1 || any(visitedmask(childnodeinds{n}))
                    % add change choice for child nodes
                    for i=1:numel(childnodeinds{n})
                        queuednodeops(childnodeinds{n}(i)) = 1;
                    end
                end
            end
            
            choicesmade = false;
            
            % have to add them one after the other, otherwise there are too
            % many combinations of combined parent node change/keep choices
            for n=1:numel(nodeinds)
                
                % actual choices are only made for unvisited parent nodes if
                % the current node changes
                if choice(n) == 1 && any(not(visitedmask(parentnodeinds{n})))
                    % add all combinations of choices for the unvisited parent
                    % nodes to the tree
                    
                    choicesmade = true;
                    
                    unvisitedparentnodeinds = parentnodeinds{n}(not(visitedmask(parentnodeinds{n})));
                    if numel(unvisitedparentnodeinds) > 8
                        error('Too many choices in a single node.');
                    end
                    for j=1:2^numel(unvisitedparentnodeinds)
                        parentchoices = zeros(1,numel(unvisitedparentnodeinds));
                        binarychoices = de2bi(j-1);
                        binarychoices = binarychoices(end:-1:1);
                        parentchoices(end-numel(binarychoices)+1:end) = binarychoices;
                        parentchoices = (parentchoices-0.5) .* 2; % from [0,1] to [-1,1]

                        parentchoicequeuednodeops = queuednodeops;
                        mask = parentchoicequeuednodeops(unvisitedparentnodeinds) < 1; % can't change nodes that have already been queued to be changed
                        parentchoicequeuednodeops(unvisitedparentnodeinds(mask)) = parentchoices(mask);
                        
                        duplicatechoiceinds = findDuplicateChoices(obj,state,visitedmask,changedmask,parentchoicequeuednodeops);
                        
                        if isempty(duplicatechoiceinds)
                            obj.choicestates(:,end+1) = state;
                            obj.choicevisitednodesmask(:,end+1) = visitedmask;
                            obj.choicechangednodesmask(:,end+1) = changedmask;
                            obj.choicequeuednodeops{:,end+1} = parentchoicequeuednodeops;
%                             obj.choiceopenmask(:,end+1) = true;
                            obj.choicecost(:,end+1) = obj.choicecost(choiceind);
                            obj.choicetree(end+1,:) = 0;
                            obj.choicetree(:,end+1) = 0;
                            obj.choicetree(end,choiceind) = 1;
                        else
                            % do nothing, this exact choice is already in
                            % the choice tree
                        end
                    end
                end
            end
            
            if choicesmade
                % continue with one of the possible choices
                obj.choicecost(choiceind) = inf;
                break;
            end
        
        end
        
        if isnan(obj.choicecost(choiceind))
            % compute score for the choice
            obj.choicecost(choiceind) = obj.eval_regularization(state);
        end
        
        % choice path finished, write everything back to choice
        obj.choicestates(:,choiceind) = state;
        obj.choicevisitednodesmask(:,choiceind) = visitedmask;
        obj.choicechangednodesmask(:,choiceind) = changedmask;
        obj.choicequeuednodeops{choiceind} = queuednodeops;
%         obj.choiceopenmask(choiceind) = false;
    end
    
    [bestchoicecost,bestchoiceind] = min(obj.choicecost);
    
    elmposes = reshape(obj.choicestate(1:statreloffset,bestchoiceind),obj.elmposedim,numel(elms));
    relvals = obj.choicestate(statreloffset+1:end,bestchoiceind);
end

function duplicatechoiceinds = findDuplicateChoices(obj,state,visitedmask,changedmask,queuednodeops)
    duplicatechoiceinds = find(...
        all(bsxfun(@eq,obj.choicestates,state),1) & ...
        all(bsxfun(@eq,obj.choicevisitednodesmask,visitedmask),1) & ...    
        all(bsxfun(@eq,obj.choicechangednodesmask,changedmask),1) & ...
        cellfun(@(x) numel(x) == numel(queuednodeops) && all(x == queuednodeops),obj.choicequeuednodeops));
end

function setVisfunction(obj,func)
    obj.visfunc = func;
end

function stop = outfun(obj,substate,optimValues,optimizerstateflag)
    
    state = obj.fullstate;
    state(obj.substateinds) = substate;
    
    if not(isempty(optimValues))
        disp(['*** iteration ',num2str(optimValues.iteration),':']);
        disp(['    objective function value: ',num2str(optimValues.fval)]);
    end
%     disp('    gradient:');
%     disp(optimValues.gradient);
    
    if not(isempty(obj.visfunc))
        eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        eposes(1:obj.elmposedim,:) = reshape(state(1:numel(obj.pgraph.elms)*obj.elmposedim),obj.elmposedim,[]);
        stop = obj.visfunc(eposes,obj.currentvisitedmask,obj.currentchangedmask,obj.currentnodeinds);
    else
        stop = false;
    end
end

function val = eval_regularization(obj,state)
    
    state = state';
    val = 0;
    
    nrels = numel(obj.pgraph.rels);
    
    elmposevar = 0.05.^2;
    rvals = zeros(1,nrels);
    rvars = zeros(1,nrels);
    statereloffset = numel(obj.pgraph.elms) * obj.elmposedim;
    
    for i=1:numel(obj.relfs)
        % evaluate all relationships functions of one type
        [v,var] = obj.relfs{i}(...
            reshape(state(obj.relfinput1inds{i}),size(obj.relfinput1inds{i})),...
            reshape(state(obj.relfinput2inds{i}),size(obj.relfinput2inds{i})));
        
        rvals(obj.relfoutputinds{i}-statereloffset) = v;
        rvars(obj.relfoutputinds{i}-statereloffset) = var;
    end
    
    % change in relationships compared
    changedrelmask = min(1,abs(rvals - [obj.pgraph.rels.value])./(sqrt([obj.pgraph.rels.variance]).*0.5));
    
    % how many relationships changed
    val = val + sum(changedrelmask)^2 * 0.05;
    
    % sum of average element changes in all element groups (or how many
    % elment groups changed)
    elmposediff = obj.oldelmposes - reshape(...
            state(obj.elmposeinds),size(obj.elmposeinds));
    for i=1:numel(obj.egroupelminds)
        if not(isempty(obj.egroupelminds{i})) % && not(any(activemask(obj.egroupelminds{i})))
            val = val + mean(sum(elmposediff(:,obj.egroupelminds{i}).^2,1) ./ elmposevar);
%             val = val + mean(1 ./ (1 + exp(-sqrt(sum(elmposediff.^2,1) ./ elmposevar) ./ 2))) * 100;
        end
    end
    
    % for each group member: the difference to the average value of all
    % group members
%     changedrelmask = not(isnan(obj.targetrelvals_changed));
%     rellagrangemult_combined = rellagrangemult(1:numel(obj.targetrelvals));
%     rellagrangemult_combined(changedrelmask) = ...
%         rellagrangemult_combined(changedrelmask) + ...
%         rellagrangemult(numel(obj.targetrelvals)+1:end);
    
    for i=1:numel(obj.relgrprelinds)
        if numel(obj.relgrprelinds{i}) > 1
            grprelvals = state(obj.relgrprelinds{i}+statereloffset);
            grpavg = mean(grprelvals);
            residual = grprelvals - grpavg;
            if obj.groupcircular(i)
                residual = smod(residual,-pi,pi);
            end
%             val = val + sum((residual).^2 ./ relvars(obj.relgrprelinds{i}))*9999;
%             val = val + sum((residual).^2 ./ relvars(obj.relgrprelinds{i}))*100;
            val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i})) * mean(1-changedrelmask(obj.relgrprelinds{i})) * 100;
%             val = val + sum(((residual).^2 ./ relvars(obj.relgrprelinds{i})) .* rellagrangemult_combined(obj.relgrprelinds{i})) * 100;
        end
    end
    
    % cost of destroying groups:
    for i=1:numel(obj.relgrprelinds)
        if numel(obj.relgrprelinds{i}) > 1
            val = val + max(0,numel(obj.relgrprelinds{i}) - sum(1-changedrelmask(obj.relgrprelinds{i})))^2 * 100;
        end
    end
end


function val = evalsubstate_nograd(obj,substate)
    
    % todo: for relationships that need other properties of the elements
    % other than their pose (e.g. distance to the boundary): need to move
    % elements to the position given by the state and pass them to the
    % relationship function (boundary vertex positions relative to the pose
    % act like constants in the relationship function)
    
    state = obj.fullstate;
    state(obj.substateinds) = substate;
    state = state';
    val = 0;

    for i=1:numel(obj.relfs)
        
        activemask = not(isnan(obj.targetrelvals(obj.relfrelinds{i})));
        
        if any(activemask)
            % evaluate all relationships functions of one type
            [v,var] = obj.relfs{i}(...
                reshape(state(obj.relfinput1inds{i}(:,activemask)),size(obj.relfinput1inds{i}(:,activemask))),...
                reshape(state(obj.relfinput2inds{i}(:,activemask)),size(obj.relfinput2inds{i}(:,activemask))));
            residual = v-obj.targetrelvals(obj.relfrelinds{i}(activemask));
            if obj.relfcircular(i)
                residual = smod(residual,-pi,pi);
            end
            val = val + (residual.^2 ./ var) * obj.relweights(obj.relfrelinds{i}(activemask))'; % weighted mahalanobis distance
        end
    end
    
    % residual between element poses and target poses (squared L2 norm)
    elmposevar = 0.05.^2;
    activemask = not(any(isnan(obj.targetelmposes),1));
    if any(activemask)
        poseresidual = obj.targetelmposes(1:obj.elmposedim,activemask) - reshape(...
            state(obj.elmposeinds(:,activemask)),...
            size(obj.elmposeinds(:,activemask)));
        poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)) = smod(...
            poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)),-pi,pi);
        val = val + (sum(poseresidual.^2,1) ./ elmposevar) * obj.elmweights(:,activemask)';
    end
end

end

end
