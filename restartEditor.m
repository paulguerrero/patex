function restartEditor

    initPatternedit;
    
    closeEditor;
    
    % create editor
    evalin('base','editor = PatternEditorFigure;');
end
