classdef ChooserelsBruteforceSolver < PatternSolver
    
properties(SetAccess=protected)
    % last solution
    solutionelmposes = cell(1,0);
    solutionrelvals = cell(1,0);
    solutioncost = zeros(1,0);
end

properties(Access=protected)
    targetelmposes = zeros(2,0);
    elmweights = zeros(1,0);
    
    targetrelvals = zeros(1,0);
    relweights = zeros(1,0);

    oldelmposes = zeros(2,0);
    newelmposes = zeros(2,0);

    visfunc = [];
end

methods

% obj = ChooserelsBruteforceSolver
% obj = ChooserelsBruteforceSolver(pgraph)
function obj = ChooserelsBruteforceSolver(varargin)
    superarginds = zeros(1,0);
    if numel(varargin) == 1
        superarginds = 1;
    end
    
    obj@PatternSolver(varargin{superarginds});
    varargin(superarginds) = [];
    
    if numel(varargin) == 0
        % do nothing
    else
        error('Invalid arguments.');
    end
end

function clear(obj)
    obj.clear@PatternSolver;
    
    obj.solutionelmposes = cell(1,0);
    obj.solutionrelvals = cell(1,0);
    obj.solutioncost = zeros(1,0);

    obj.targetelmposes = zeros(2,0);
    obj.elmweights = zeros(1,0);
    
    obj.targetrelvals = zeros(1,0);
    obj.relweights = zeros(1,0);

    obj.oldelmposes = zeros(2,0);
    obj.newelmposes = zeros(2,0);
end

function prepare(obj,newelmposes,newrelvals)
    obj.prepare@PatternSolver;
    
    obj.oldelmposes = [obj.pgraph.elms.pose];
    obj.oldelmposes = obj.oldelmposes(1:obj.elmposedim,:);
    
    obj.newelmposes = newelmposes;
    obj.newelmposes = obj.newelmposes(1:obj.elmposedim,:);
    
    obj.targetrelvals = nan(1,numel(obj.rels));
    obj.targetelmposes = nan(obj.elmposedim,numel(obj.pgraph.elms));
    obj.relweights = ones(1,numel(obj.rels));
    obj.elmweights = ones(1,numel(obj.pgraph.elms));
    
    userchangedelmmask = any(obj.oldelmposes ~= obj.newelmposes,1);
    obj.targetelmposes(:,userchangedelmmask) = obj.newelmposes(:,userchangedelmmask);
    obj.elmweights(userchangedelmmask) = 1000;
end

function [selmposes,srelvals,scost] = solve(obj,newelmposes,newrelvals)
    
    obj.clear;
    
    obj.prepare(newelmposes,newrelvals);
    
%     elms = obj.pgraph.elms;
%     rels = obj.pgraph.rels;
    
    ncombs = 2^numel(obj.rels);
    
    oldrelvals = [obj.rels.value];
    
    if ncombs > 2^52
        % mantissa of double is 52 bit
        error('Too many combinations, even the number of combinations cannot be represented accurately with a double');
    elseif ncombs > 100000
        warning(['There are ',num2str(ncombs),' combinations, this will take some time.']);
    end
    
    startstate = obj.patternstate;
    
    maxsolutions = 1000;
%     maxsolutions = 3;
    nsolutions = min(maxsolutions,ncombs);
    
    elmoffset = obj.pgraph.elmoffset;
    elm2stateinds = cat(1,obj.node2stateinds{elmoffset+1 : elmoffset+numel(obj.pgraph.elms)});
    rel2stateinds = cat(1,obj.node2stateinds{obj.rel2nodeinds(1:numel(obj.rels))});
    
    warning('off','optim:fminunc:SwitchingMethod');
    
    % optimize pattern for all combinations of keep/ignore relationships
    selmposes = cell(1,nsolutions);
    srelvals = cell(1,nsolutions);
    scost = inf(1,nsolutions);
    totaltime = 0;
    for i=1:ncombs
        
%         % temp (only evaluate the first few combinations)
%         if i > nsolutions
%             break;
%         end
        
        disp(['Combination ',num2str(i),' / ',num2str(ncombs),' - estimated time remaining: ',num2str((totaltime * (ncombs/(i-1))) / 3600),' hours']);
        combtimeid = tic;
        % set current combination
        relignored = de2bi(i-1,numel(obj.rels),'left-msb');
        
        obj.targetrelvals = nan(1,numel(obj.rels));
        obj.targetrelvals(not(relignored)) = oldrelvals(not(relignored));
        
%         % with diagnostics
%         optoptions = optimoptions(@fminunc,...
%             'MaxFunEvals',100000,... % matlab default is 100*number of variables
%             'TolFun',1e-7,... % matlab default is 1e-6
%             'GradObj','off',...
%             'Diagnostics','on',...
%             'FunValCheck','on',...
%             'OutputFcn',@(x,optimValues,state) obj.outfun(x,optimValues,state),...
%             'DerivativeCheck','off');
        
        % without (faster)
        optoptions = optimoptions(@fminunc,...
            'MaxFunEvals',100000,... % matlab default is 100*number of variables
            'TolFun',1e-7,... % matlab default is 1e-6
            'GradObj','off',...
            'Diagnostics','off',...
            'FunValCheck','off',...
            'Display','off',...
            'DerivativeCheck','off');
        
        % optimize pattern
        [state,cost,~,~,~,~] = fminunc(...
            @(x) obj.evalstate_nograd(x),startstate,...
            optoptions);
        
        % add regularization term to cost (not directly in objective
        % function because it depends on which combination is used)
        % todo: try adding the reg. term to the optimization
        cost = cost + obj.eval_regularization(state);
        
        % only keep solution if it is among the current n best solutions
        [maxcost,maxcostind] = max(scost);
        if cost < maxcost
            selmposes{maxcostind} = reshape(state(elm2stateinds),obj.elmposedim,numel(obj.pgraph.elms));
            srelvals{maxcostind} = state(rel2stateinds);
            scost(maxcostind) = cost;
        end
        
        combtime = toc(combtimeid);
        totaltime = totaltime + combtime;
    end
    
    warning('on','optim:fminunc:SwitchingMethod');
    
    % sort by cost
    [~,perm] = sort(scost,'ascend');
    selmposes = selmposes(perm);
    srelvals = srelvals(perm);
    scost = scost(perm);
    
    obj.solutionelmposes = selmposes;
    obj.solutionrelvals = srelvals;
    obj.solutioncost = scost;
end

function setVisfunction(obj,func)
    obj.visfunc = func;
end

function stop = outfun(obj,state,optimValues,optimizerstateflag)
%     disp(['*** iteration ',num2str(optimValues.iteration),':']);
%     disp(['    objectivefunction value: ',num2str(optimValues.fval)]);
%     disp('    gradient:');
%     disp(optimValues.gradient);
    
    if not(isempty(obj.visfunc))
        eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        
        elmoffset = obj.pgraph.elmoffset;
        elm2stateinds = cat(1,obj.node2stateinds{elmoffset+1 : elmoffset+numel(obj.pgraph.elms)});
        
        eposes(1:2,:) = reshape(state(elm2stateinds),obj.elmposedim,[]);
        
        relactive = double(not(isnan(obj.targetrelvals)));
        stop = obj.visfunc(obj,eposes,relactive);
    else
        stop = false;
    end
end

function grad = gradient(obj,state)
    if isempty(obj.targetrelvals) && isempty(obj.elmtargetposes)
        warning('Must set objective values before computing the gradient.');
        return;
    end
    
    if nargin < 2 || isempty(state)
        % default is current pgraph state
        state = [...
            cat(1,obj.pgraph.elms.position);...
            cat(1,obj.pgraph.rels.value)];
    end
    
    grad = gradest(@(x) obj.evalstate_nograd(x),state);
end

% separate gradients for the contribution of each element to the objective function
% (individual contributions are summed up to get the objective function value,
% so gradients are summed up to get objective function gradient as well)
function jac = jacobian_separate(obj,state)
    if isempty(obj.targetrelvals) && isempty(obj.elmtargetposes)
        warning('Must set objective values before computing the gradient.');
        return;
    end
    
    if nargin < 2 || isempty(state)
        % default is current pgraph state
        state = [...
            cat(1,obj.pgraph.elms.position);...
            cat(1,obj.pgraph.rels.value)];
    end
    
    jac = jacobianest(@(x) obj.evalstate_nograd_separate(x),state);    
end

% separate gradients for each relationship function, put in a vector the
% same size as the state
function jac = reljacobian_separate(obj,state)
    if isempty(obj.targetrelvals) && isempty(obj.elmtargetposes)
        warning('Must set objective values before computing the gradient.');
        return;
    end
    
    if nargin < 2 || isempty(state)
        % default is current pgraph state
        state = [...
            cat(1,obj.pgraph.elms.position);...
            cat(1,obj.pgraph.rels.value)];
    end
    
    jac = jacobianest(@(x) obj.evalrels_nograd_separate(x),state);    
end

function [val,grad] = evalstate(obj,state)
    
    % todo: for relationships that need other properties of the elements
    % other than their pose (e.g. distance to the boundary): need to move
    % elements to the position given by the state and pass them to the
    % relationship function (boundary vertex positions relative to the pose
    % act like constants in the relationship function)
    
    state = state';
    val = 0;
    grad = zeros(1,numel(state));
    
    for i=1:numel(obj.relfs)
        
        % evaluate all relationships functions of one type
        [v,var,g] = obj.relfs{i}(...
            reshape(state(obj.relfinput1inds{i}),size(obj.relfinput1inds{i})),...
            reshape(state(obj.relfinput2inds{i}),size(obj.relfinput2inds{i})));
        
        % residual between relationships and target values (squared L2 norm)
        activemask = not(isnan(obj.targetrelvals{i}(obj.relfrelinds{i})));
        if any(activemask)
            residual = v(activemask)-obj.targetrelvals{i}(obj.relfrelinds{i}(activemask));
            if obj.relfcircular(i)
                residual = smod(residual,-pi,pi);
            end
            val = val + (residual.^2 ./ var(activemask)) * obj.relweights(obj.relfrelinds{i}(activemask))'; % weighted mahalanobis distance
            
            % gradient w*f(x)^2 is w*2*f(x) * g where g is df/dx
            relinputdim = size(obj.relfinput1inds{i},1);
            g = bsxfun(@times,2.*(residual./var(activemask))*obj.relweights(obj.relfrelinds{i}(activemask)),g(1:relinputdim,:,:));
            grad(obj.relfinput1inds{i}(:,activemask)) = reshape(...
                grad(obj.relfinput1inds{i}(:,activemask)),...
                size(obj.relfinput1inds{i}(:,activemask))) ...
                + g(:,:,1);
            grad(obj.relfinput2inds{i}(:,activemask)) = reshape(...
                grad(obj.relfinput2inds{i}(:,activemask)),...
                size(obj.relfinput2inds{i}(:,activemask))) ...
                + g(:,:,2);
        end
            
        % residual between relationship values in the state and actual
        % relationship values (squared L2 norm)
        residual = v-state(obj.relfoutputinds{i});
        if obj.relfcircular(i)
            residual = smod(residual,-pi,pi);
        end
        val = val + (residual.^2 ./ var) * obj.relweights(obj.relfrelinds{i})';

        warning('todo: gradient not fully computed.');
%         % for gradient:
%         grad(obj.relfoutputinds{i}) = ...
    end
    
    % residual between element poses and target poses (squared L2 norm)
    activemask = not(any(isnan(obj.targetelmposes),1));
    if any(activemask)
        elmconstraintvar = 0.05.^2;
        poseresidual = obj.targetelmposes(1:obj.elmposeind,activemask) - reshape(...
            state(obj.elmposestateinds(:,activemask)),...
            size(obj.elmposestateinds(:,activemask)));
        poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)) = smod(...
            poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)),-pi,pi);
        val = val + (sum(poseresidual.^2,1) ./ elmconstraintvar) * obj.elmweights(:,activemask)';
        
        % gradient of distance length one, but since it would be
        % 2*totargetdist*(totarget/totargetdist), totargetdist cancels out
        g = bsxfun(@times,2.*obj.elmweights(:,activemask).*(1./elmconstraintvar),-poseresidual);
        grad(obj.elmposestateinds(:,activemask)) = reshape(...
            grad(obj.elmposestateinds(:,activemask)),...
            size(obj.elmposestateinds(:,activemask))) ...
            + g;
    end
    
    grad = grad';
end

function val = evalstate_nograd(obj,state)
    
    % todo: for relationships that need other properties of the elements
    % other than their pose (e.g. distance to the boundary): need to move
    % elements to the position given by the state and pass them to the
    % relationship function (boundary vertex positions relative to the pose
    % act like constants in the relationship function)
    
    state = state';
    val = 0;

    for i=1:numel(obj.relfs)
        
        % evaluate all relationships functions of one type
        [v,var] = obj.relfs{i}(...
            reshape(state(obj.relfinput1inds{i}),size(obj.relfinput1inds{i})),...
            reshape(state(obj.relfinput2inds{i}),size(obj.relfinput2inds{i})));
        
        % residual between relationships and target values (squared L2 norm)
        activemask = not(isnan(obj.targetrelvals(obj.relfrelinds{i})));
        if any(activemask)

            residual = v(activemask)-obj.targetrelvals(obj.relfrelinds{i}(activemask));
            if obj.relfcircular(i)
                residual = smod(residual,-pi,pi);
            end
            val = val + (residual.^2 ./ var(activemask)) * obj.relweights(obj.relfrelinds{i}(activemask))'; % weighted mahalanobis distance
        end
        
        % residual between relationship values in the state and actual
        % relationship values (squared L2 norm)
        residual = v-state(obj.relfoutputinds{i});
        if obj.relfcircular(i)
            residual = smod(residual,-pi,pi);
        end
        val = val + (residual.^2 ./ var) * obj.relweights(obj.relfrelinds{i})';
    end
    
    % residual between element poses and target poses (squared L2 norm)
    activemask = not(any(isnan(obj.targetelmposes),1));
    if any(activemask)
        elmconstraintvar = 0.05.^2;
        poseresidual = obj.targetelmposes(1:obj.elmposedim,activemask) - reshape(...
            state(obj.elmposestateinds(:,activemask)),...
            size(obj.elmposestateinds(:,activemask)));
        poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)) = smod(...
            poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)),-pi,pi);
        val = val + (sum(poseresidual.^2,1) ./ elmconstraintvar) * obj.elmweights(:,activemask)';
    end
end

% contribution of each element and each relationship to the objective
% function
function val = evalstate_nograd_separate(obj,state)
    
    % todo: for relationships that need other properties of the elements
    % other than their pose (e.g. distance to the boundary): need to move
    % elements to the position given by the state and pass them to the
    % relationship function (boundary vertex positions relative to the pose
    % act like constants in the relationship function)
    
    state = state';
    val = zeros(size(state));

    for i=1:numel(obj.relfs)
        
        % evaluate all relationships functions of one type
        [v,var] = obj.relfs{i}(...
            reshape(state(obj.relfinput1inds{i}),size(obj.relfinput1inds{i})),...
            reshape(state(obj.relfinput2inds{i}),size(obj.relfinput2inds{i})));
        
        % residual between relationships and target values (squared L2 norm)
        activemask = not(isnan(obj.targetrelvals(obj.relfrelinds{i})));
        if any(activemask)

            residual = v(activemask)-obj.targetrelvals(obj.relfrelinds{i}(activemask));
            if obj.relfcircular(i)
                residual = smod(residual,-pi,pi);
            end
            val(obj.relfoutputinds{i}(activemask)) = val(obj.relfoutputinds{i}(activemask)) + ...
                (residual.^2 ./ var(activemask)) * obj.relweights(obj.relfrelinds{i}(activemask))'; % weighted mahalanobis distance
        end
        
        % residual between relationship values in the state and actual
        % relationship values (squared L2 norm)
        residual = v-state(obj.relfoutputinds{i});
        if obj.relfcircular(i)
            residual = smod(residual,-pi,pi);
        end
        val(obj.relfoutputinds{i}) = val(obj.relfoutputinds{i}) + ...
            (residual.^2 ./ var) * obj.relweights(obj.relfrelinds{i})';
    end
    
    % residual between element poses and target poses (squared L2 norm)
    activemask = not(any(isnan(obj.targetelmposes),1));
    if any(activemask)
        elmconstraintvar = 0.05.^2;
        poseresidual = obj.targetelmposes(1:obj.elmposedim,activemask) - reshape(...
            state(obj.elmposestateinds(:,activemask)),...
            size(obj.elmposestateinds(:,activemask)));
        poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)) = smod(...
            poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)),-pi,pi);
        val(obj.elmposestateinds(:,activemask)) = reshape(...
            val(obj.elmposestateinds(:,activemask)),...
            size(obj.elmposestateinds(:,activemask))) + ...
            (sum(poseresidual.^2,1) ./ elmconstraintvar) * obj.elmweights(:,activemask)';
    end
    
    val = val';
end

% contribution of each element and each relationship to the objective
% function
function val = evalrels_nograd_separate(obj,state)
    
    % todo: for relationships that need other properties of the elements
    % other than their pose (e.g. distance to the boundary): need to move
    % elements to the position given by the state and pass them to the
    % relationship function (boundary vertex positions relative to the pose
    % act like constants in the relationship function)
    
    state = state';
    val = zeros(size(state));
    
    for i=1:numel(obj.relfs)
        [v,var] = obj.relfs{i}(...
            reshape(state(obj.relfinput1inds{i}),size(obj.relfinput1inds{i})),...
            reshape(state(obj.relfinput2inds{i}),size(obj.relfinput2inds{i})));
        
        val(obj.relfoutputinds{i}) = val(obj.relfoutputinds{i}) + ...
            (v ./ sqrt(var)) .* obj.relweights(obj.relfrelinds{i});
    end
    
    val = val';
end

function val = eval_regularization(obj,state)
    
    state = state';
    val = 0;
    
    nrels = numel(obj.rels);
    
%     elmposevar = 0.05.^2;
    rvals = zeros(1,nrels);
    rvars = zeros(1,nrels);
    statereloffset = numel(obj.pgraph.elms) * obj.elmposedim;
    
    for i=1:numel(obj.relfs)
        % evaluate all relationships functions of one type
        [v,var] = obj.relfs{i}(...
            reshape(state(obj.relfinput1inds{i}),size(obj.relfinput1inds{i})),...
            reshape(state(obj.relfinput2inds{i}),size(obj.relfinput2inds{i})));
        
        rvals(obj.relfoutputinds{i}-statereloffset) = v;
        rvars(obj.relfoutputinds{i}-statereloffset) = var;
    end
    
    % change in relationships compared
    changedrelmask = min(1,abs(rvals - [obj.rels.value])./(sqrt([obj.rels.variance]).*0.5));
    
    % how many relationships changed
    val = val + sum(changedrelmask)^2 * 0.05;
    
    % sum of average element changes in all element groups (or how many
    % elment groups changed)
    elmposediff = obj.oldelmposes - reshape(...
            state(obj.elmposestateinds),size(obj.elmposestateinds));
    for i=1:numel(obj.elmgrpelminds)
        if not(isempty(obj.elmgrpelminds{i})) % && not(any(activemask(obj.elmgrpelminds{i})))
            val = val + mean(sum(elmposediff(:,obj.elmgrpelminds{i}).^2,1) ./ obj.elmposevar);
%             val = val + mean(1 ./ (1 + exp(-sqrt(sum(elmposediff.^2,1) ./ obj.elmposevar) ./ 2))) * 100;
        end
    end
    
    % for each group member: the difference to the average value of all
    % group members
%     changedrelmask = not(isnan(obj.targetrelvals_changed));
%     rellagrangemult_combined = rellagrangemult(1:numel(obj.targetrelvals));
%     rellagrangemult_combined(changedrelmask) = ...
%         rellagrangemult_combined(changedrelmask) + ...
%         rellagrangemult(numel(obj.targetrelvals)+1:end);
    
    for i=1:numel(obj.relgrprelinds)
        if numel(obj.relgrprelinds{i}) > 1
            grprelvals = state(obj.relgrprelinds{i}+statereloffset);
            grpavg = mean(grprelvals);
            residual = grprelvals - grpavg;
            if obj.relgrpcircular(i)
                residual = smod(residual,-pi,pi);
            end
%             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*9999;
            val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*1000;
%             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i})) * mean(1-changedrelmask(obj.relgrprelinds{i})) * 100;
%             val = val + sum(((residual).^2 ./ rvars(obj.relgrprelinds{i})) .* rellagrangemult_combined(obj.relgrprelinds{i})) * 100;
        end
    end
    
%     % cost of destroying groups:
%     for i=1:numel(obj.relgrprelinds)
%         if numel(obj.relgrprelinds{i}) > 1
%             val = val + max(0,numel(obj.relgrprelinds{i}) - sum(1-changedrelmask(obj.relgrprelinds{i})))^2 * 100;
%         end
%     end
end

end

end
