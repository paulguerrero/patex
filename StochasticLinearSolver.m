classdef StochasticLinearSolver < StandardPatternSolver
    
properties(SetAccess=protected)
    % last solution

    solutionstartstate = [];
    solutionsubstateinds = cell(1,0);
    solutionactiveconsrelinds = cell(1,0);
    solutionactivefixnodeinds = cell(1,0);
    solutionactiveregnodeinds = cell(1,0);
    solutioncombs = [];
    
    
    % diagnostic information for paper
    lintimeneeded = [];
    
    componentnodecount = [];
    totalnodecount = [];
    
    nsol_total = [];
    nsol_randompick = [];
    nsol_intuitive = [];
    nsol_linrandompick = [];
    nsol_prevalid = [];
    nsol_valid = [];
    nsol_distinct = [];
end

properties(Access=protected)
    visfunc = [];
end

methods(Static)

function s = defaultSettings
	s = catstruct(...
        StandardPatternSolver.defaultSettings,...
        struct(...
            'maxgenvariations',10000,...        
            'maxlinearevals',150,...
            'maxsolutions',10,...
            'maxcost',PatternSolver.defaultSettings.synctolerance*1000,...
            'solutiondistancethresh',2, ... % in multiples of sqrt(sync. tolerance)
            'elmsolutiondistancethresh',2, ... % in multiples of sqrt(sync. tolerance)
            'usenonlinear',false ... % only for evaluation (test using the non-linear solver directly)
        )); 
end
    
end

methods

% obj = StochasticLinearSolver
% obj = StochasticLinearSolver(obj2)
% obj = StochasticLinearSolver(pgraph)
function obj = StochasticLinearSolver(varargin)
    superarginds = zeros(1,0);
    if numel(varargin) == 1  && isa(varargin{1},'PatternGraph')
        superarginds = 1;
    end
    
    obj@StandardPatternSolver(varargin{superarginds});
    varargin(superarginds) = [];
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1  && isa(varargin{1},'StochasticLinearSolver')
        obj.copyFrom(varargin{1});
    else
        error('Invalid arguments.');
    end
end

function copyFrom(obj,obj2)
    obj.copyFrom@StandardPatternSolver(obj2);
    
    obj.solutionstartstate = obj2.solutionstartstate;
    obj.solutionsubstateinds = obj2.solutionsubstateinds;
    obj.solutionactiveconsrelinds = obj2.solutionactiveconsrelinds;
    obj.solutionactivefixnodeinds = obj2.solutionactivefixnodeinds;
    obj.solutionactiveregnodeinds = obj2.solutionactiveregnodeinds;
    obj.solutioncombs = obj2.solutioncombs;
    
    obj.lintimeneeded = obj2.lintimeneeded;
    obj.componentnodecount = obj2.componentnodecount;
    obj.totalnodecount = obj2.totalnodecount;
    obj.nsol_total = obj2.nsol_total;
    obj.nsol_randompick = obj2.nsol_randompick;
    obj.nsol_intuitive = obj2.nsol_intuitive;
    obj.nsol_linrandompick = obj2.nsol_linrandompick;
    obj.nsol_prevalid = obj2.nsol_prevalid;
    obj.nsol_valid = obj2.nsol_valid;
    obj.nsol_distinct = obj2.nsol_distinct;
end

function obj2 = clone(obj)
    obj2 = StochasticLinearSolver(obj);
end

function clear(obj)
    obj.clear@StandardPatternSolver;
    
    obj.solutionstartstate = [];
    obj.solutionsubstateinds = cell(1,0);
    obj.solutionactiveconsrelinds = cell(1,0);
    obj.solutionactivefixnodeinds = cell(1,0);
    obj.solutionactiveregnodeinds = cell(1,0);
    obj.solutioncombs = [];
end

function prepare(obj,newelmposes,newrelvals)

    obj.prepare@StandardPatternSolver(newelmposes,newrelvals);
    
    obj.solutionstartstate = zeros(numel(obj.patternstate),0);
    obj.solutionsubstateinds = cell(1,0);
    obj.solutionactiveconsrelinds = cell(1,0);
    obj.solutionactivefixnodeinds = cell(1,0);
    obj.solutionactiveregnodeinds = cell(1,0);
    obj.solutioncombs = [];
end

% [selmposes,srelvals,scost] = solve(obj,newelmposes,editedelminds,newrelvals)
% [selmposes,srelvals,scost] = solve(obj,newelmposes,newrelvals,editedelminds,nvpairs)
function [selmposes,srelvals,scost] = solve(obj,newelmposes,newrelvals,editedelminds,varargin)

    disp('**** linear solve ****');
    
    optionalargs = struct(...
        'fixedelminds',zeros(1,0),...
        'combinations',zeros(0,0));
    [optionalargs,additionalargmask] = nvpairs2struct(varargin,optionalargs,false,0);
    varargin(additionalargmask) = [];
    obj.settings = nvpairs2struct(varargin,obj.settings);
    
    obj.clear;

    if isempty(newelmposes) || isempty(newrelvals)
        error('Pattern without elements or without relationships.');
    end
    
    obj.prepare(newelmposes,newrelvals);
    
    usereditedelminds = editedelminds;    
    usereditednodeinds = usereditedelminds + obj.pgraph.elmoffset;
    
    % find the current pattern graph component: all nodes that are
    % connected to any of the elements changed by the user
    if isempty(usereditednodeinds)
        componentnodeinds = zeros(1,0);
    else
        nodedist = min(obj.pgraph.nodedist(usereditednodeinds,:),[],1);
        grpdist = inf(1,size(obj.pgraph.groups,2));
        for i=obj.relgrpinds
            grpdist(i) = min(nodedist(logical(obj.pgraph.groups(:,i))));
        end
        grpsubsetinds = find(not(isinf(grpdist)));
        componentnodeinds = find(any(obj.pgraph.groups(:,grpsubsetinds),2) | not(isinf(nodedist')))'; %#ok<FNDSB>
    end
    componentrelinds = obj.pgraph.node2relinds(componentnodeinds);
    componentrelinds(isnan(componentrelinds)) = [];
    componentstateinds = cat(1,obj.node2stateinds{componentnodeinds});
    
    obj.componentnodecount = numel(componentnodeinds);
    obj.totalnodecount = numel(obj.pgraph.nodes);
    
    % only optimize pattern graph nodes in the current component
    substatemask = false(size(obj.patternstate));
    substatemask(componentstateinds) = true;
    substatemask(obj.nodefixorig2stateinds) = false;
    substatemask(obj.nodefixnew2stateinds) = false;
    substatemask(obj.grpfix2stateinds) = false;
    substatemask(obj.elmposestateinds(:,optionalargs.fixedelminds)) = false;
    substatemask(obj.elmposestateinds(logical(obj.affine2simpose(obj.pgraph.fixedposeinds)))) = false; % fix elements as prescribed by the pattern graph
    substateinds = find(substatemask);
    
    % all relationships in the current component are active
    activeconsrelinds = componentrelinds;
%     activefixnodeinds = componentnodeinds;
    activefixnodeinds = usereditednodeinds;
    activeregnodeinds = componentnodeinds;
    
    % generate combinations
    if size(optionalargs.combinations,1) == 0
        obj.solutioncombs = obj.generateCombinations(...
            obj.settings.maxgenvariations,obj.settings.maxlinearevals,componentnodeinds,usereditednodeinds);
    else
        obj.solutioncombs = optionalargs.combinations;
    end
    combrelgrpinds = find(any(obj.relgrps(componentrelinds,:),1));

    % get relationships and their childs (to filter out inconsistent
    % combinations later)
    nodesandchilds = nan(3,numel(obj.pgraph.nodes));
    nodesandchilds(1,:) = 1:numel(obj.pgraph.nodes);
    for i=1:numel(obj.pgraph.nodes)
        nodesandchilds(1+1:1+numel(obj.childnodeinds{i}),i) = obj.childnodeinds{i};
    end
    relsandchilds = obj.pgraph.node2relinds(nodesandchilds(:,obj.pgraph.rel2nodeinds)')';
    metarelsandchilds = relsandchilds(:,not(any(isnan(relsandchilds),1)));
    
    
    % compute jacobian and objective value at starting point (before user
    % change)
    origstate = obj.patternstate;
    origstatevar = obj.patternstatevar;
    origstate(obj.nodefixnew2stateinds(usereditednodeinds)) = 1;
    
    [~,~,objfun_consinds] = obj.objfunprops(...
            activeconsrelinds,activefixnodeinds,activeregnodeinds,...
            obj.settings.consweight,obj.settings.fixweight,obj.settings.regweight,...
            obj.settings.regw_relfixed,obj.settings.regw_elmgrpschanged,obj.settings.regw_relgrpschanged,obj.settings.regw_grpsbroken);

    % zeroth-order approximation and first-order approximation
    % (estimate objective function value and jacobian of the relationships
    % at the original state)
    [origval,origjacobian] = obj.evalobjective_vector(...
        origstate,origstatevar,origstate(substateinds),substateinds,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        obj.settings.consweight,obj.settings.fixweight,obj.settings.regweight,...
        obj.settings.regw_relfixed,obj.settings.regw_elmgrpschanged,obj.settings.regw_relgrpschanged,obj.settings.regw_grpsbroken);
    
%     % to compare the analytic jacobian of the objective function to the
%     % finite difference jacobian
%     origjacobian_fd = obj.estimate_jacobian(...
%         origstate,origstatevar,origstate(substateinds),substateinds,...
%         activeconsrelinds,activefixnodeinds,activeregnodeinds,...
%         consweight,fixweight,regweight,...
%         regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    
    tic;
    
    keepcombs = false(1,size(obj.solutioncombs,2));
    combstate = zeros(numel(origstate),0);
    combstatevar = zeros(numel(origstate),0);
    combsubstatefreemask = false(numel(substateinds),0);
    for i=1:size(obj.solutioncombs,2)
        comb = obj.solutioncombs(:,i);
        [cstate,cstatevar,csubstatefreemask,cisvalid] = obj.getCombinationState(comb,combrelgrpinds,metarelsandchilds,substateinds,usereditedelminds); %#ok<FNDSB>
        if cisvalid
            combstate(:,end+1) = cstate; %#ok<AGROW>
            combstatevar(:,end+1) = cstatevar; %#ok<AGROW>
            combsubstatefreemask(:,end+1) = csubstatefreemask; %#ok<AGROW>
            keepcombs(i) = true;
        end
        
    end
    obj.solutioncombs(:,not(keepcombs)) = [];
    
    [~,~,clusterind] = unique(combsubstatefreemask','rows');
    samefreemaskclusters = array2cell_mex(1:size(combsubstatefreemask,2),clusterind');
    
    disp(['    Evaluating ',num2str(size(obj.solutioncombs,2)),' solutions with ',num2str(numel(samefreemaskclusters)),' different free states ...']);
    
    nodestateinds = cat(1,obj.node2stateinds{:}); %#ok<NASGU>
    obj.solutionstate = zeros(numel(origstate),size(combstate,2));
    obj.solutionstatevar = zeros(numel(origstatevar),size(combstate,2));
    obj.solutionstartstate = zeros(numel(origstate),size(combstate,2));
    obj.solutioncost = zeros(1,size(combstate,2));
    obj.solutionsubstateinds = cell(1,size(combstate,2));
    solutiondists = zeros(size(combstate,2),size(combstate,2));
    
    statecomputedmask = false(1,size(combstate,2));

    warning('off','MATLAB:rankDeficientMatrix');    
    for j=1:numel(samefreemaskclusters)
        
        invfreejac = pinv( origjacobian(:, combsubstatefreemask(:,samefreemaskclusters{j}(1)) ) );
        
        for i=samefreemaskclusters{j}'
            

            % compute linear estimates for the solution for each of the combinations
            [stateestimate,stateestimateval] = obj.linear_solution_origstart(...
                origstate,origstatevar,combstate(:,i),substateinds,combsubstatefreemask(:,i),origval,origjacobian,invfreejac);
            
            obj.solutionstate(:,i) = stateestimate;
            obj.solutionstatevar(:,i) = combstatevar(:,i);
            obj.solutionstartstate(:,i) = combstate(:,i);
            obj.solutionsubstateinds{:,i} = substateinds(combsubstatefreemask(:,i));
            if isempty(objfun_consinds)
                cost = 0;
            else
                cost = mean(stateestimateval(objfun_consinds).^2,1);
            end
            obj.solutioncost(:,i) = cost;

            
            
            if obj.settings.usenonlinear
                % run non-linear solver (use linear as input, otherwise it is
                % too slow to converge, may require 10 minutes or so)
                startstate = stateestimate;
                startstatevar = origstatevar;
                nlsubstateinds = substateinds(combsubstatefreemask(:,i));

                objfun = @(x) obj.evalobjective_vecnorm(...
                    startstate,startstatevar,...
                    x,nlsubstateinds,...
                    activeconsrelinds,activefixnodeinds,activeregnodeinds,...
                    obj.settings.consweight,obj.settings.fixweight,obj.settings.regweight,...
                    obj.settings.regw_relfixed,obj.settings.regw_elmgrpschanged,obj.settings.regw_relgrpschanged,obj.settings.regw_grpsbroken);

                if isempty(nlsubstateinds)
                    bestsubstate = startstate(nlsubstateinds);
                    objectiveval = 0;
                else
                    [bestsubstate,objectiveval] = fminunc(...
                        objfun,...
                        startstate(nlsubstateinds),...
                        optimoptions(@fminunc,...
                        'Algorithm','trust-region',...'quasi-newton',...
                        'MaxFunEvals',100000,... % matlab default is 100*number of variables
                        'MaxIter',30,...%100,... % 100 for trust region, 200 for quasi-newton
                        'TolFun',1e-4,... %1e-6,... % matlab default is 1e-6
                        'TolX',1e-10,... % default for interior point is 1e-10
                        'GradObj','on',...
                        'Diagnostics','off',...
                        'Display','final',...%'off',... 
                        'FunValCheck','off',...
                        'DerivativeCheck','off'));
                end

                obj.solutionstate(:,i) = startstate;
                obj.solutionstate(nlsubstateinds,i) = bestsubstate;
                obj.solutionstatevar(:,i) = combstatevar(:,i);
                obj.solutionstartstate(:,i) = combstate(:,i);
                obj.solutionsubstateinds{:,i} = nlsubstateinds;
                obj.solutioncost(:,i) = objectiveval;

                disp(['linear cost: ',num2str(cost)]);
                disp(['non-linear cost: ',num2str(objectiveval)]);
            end

            if obj.settings.maxsolutions < inf
                statediff = bsxfun(@minus,obj.solutionstate(obj.relstateinds,i),obj.solutionstate(obj.relstateinds,statecomputedmask));
                circularmask = obj.statecircular(obj.relstateinds);
                if any(circularmask)
                    statediff(circularmask,:) = smod(statediff(circularmask,:),-pi,pi);
                end
                solutiondists(i,statecomputedmask) = sqrt(max(...
                    bsxfun(@rdivide,statediff,sqrt(obj.patternstatevar(obj.relstateinds,:))).^2,...
                    [],1));
            end
            
            statecomputedmask(i) = true;
        end
    end
    obj.lintimeneeded = toc;
    disp(['Elapsed time is ',num2str(obj.lintimeneeded), ' seconds.']);
    warning('on','MATLAB:rankDeficientMatrix');
    
    obj.nsol_prevalid = size(obj.solutioncombs,2);

    % remove solutions with high cost % temphere (comment in)
    if obj.settings.maxcost < inf
        mask = obj.solutioncost > obj.settings.maxcost;%obj.settings.synctolerance*100;
        disp(['    Keeping ',num2str(sum(not(mask))),' solutions with reasonably low cost ...']);
        obj.solutioncombs(:,mask) = [];
        obj.solutioncost(:,mask) = [];
        obj.solutionstate(:,mask) = [];
        obj.solutionstatevar(:,mask) = [];
        obj.solutionstartstate(:,mask) = [];
        obj.solutionsubstateinds(mask) = [];
        solutiondists(:,mask) = [];
        solutiondists(mask,:) = [];
    end
    
    obj.nsol_valid = size(obj.solutioncombs,2);

    
    solutiondists = solutiondists + solutiondists';
        
    % remove solutions with negative scaling (if pose size is active)
    if obj.settings.elmposedim >= 4
        mask = any(obj.solutionstate(obj.elmposestateinds(4,:),:) < 0.000001,1);
        obj.solutioncombs(:,mask) = [];
        obj.solutioncost(:,mask) = [];
        obj.solutionstate(:,mask) = [];
        obj.solutionstatevar(:,mask) = [];
        obj.solutionstartstate(:,mask) = [];
        obj.solutionsubstateinds(mask) = [];
        solutiondists(:,mask) = [];
        solutiondists(mask,:) = [];
    end
    
    
    % remove solutions that are too similar
    if obj.settings.solutiondistancethresh < inf && size(obj.solutionstate,2) > 1
        keepcombs = false(1,size(obj.solutioncombs,2));
        
        % cluster linear state estimates by pair-wise distance
        l = linkage(squareform(solutiondists,'tovector'),'complete');
        maxclusterdist = sqrt(obj.settings.synctolerance)*obj.settings.solutiondistancethresh;
        combsolutioninds = cluster(l,'cutoff',maxclusterdist,'criterion','distance');
        solutioncombinds = array2cell_mex(1:numel(combsolutioninds),combsolutioninds');
        
        disp(['    Keeping ',num2str(numel(solutioncombinds)),' solutions that are distinct enough based on relationship values ...']);
        
        % pick the combination in each cluster with most fixed relationships
        for i=1:numel(solutioncombinds)
            combs = obj.solutioncombs(:,solutioncombinds{i});
            fixedcount = sum(combs~=0,1);
            [maxfixedcount,~] = max(fixedcount);
            inds = find(fixedcount == maxfixedcount);
            
            fixedtoorigcount = sum(combs(:,inds)==1,1);
            [~,ind] = max(fixedtoorigcount);
            ind = inds(ind);
            
            keepcombs(solutioncombinds{i}(ind)) = true;
        end
        
        obj.solutioncombs(:,not(keepcombs)) = [];    
        obj.solutioncost(:,not(keepcombs)) = [];
        obj.solutionstate(:,not(keepcombs)) = [];
        obj.solutionstatevar(:,not(keepcombs)) = [];
        obj.solutionstartstate(:,not(keepcombs)) = [];
        obj.solutionsubstateinds(not(keepcombs)) = [];
        solutiondists(:,not(keepcombs)) = [];
        solutiondists(not(keepcombs),:) = [];    
    end
    
    % compute element-based distance
    solutionelmdists = zeros(size(obj.solutionstate,2),size(obj.solutionstate,2));
    for i=2:size(obj.solutionstate,2)
        elmstateinds = obj.elmposestateinds(:);
        statediff = bsxfun(@minus,obj.solutionstate(elmstateinds,i),obj.solutionstate(elmstateinds,1:i-1));
        circularmask = obj.statecircular(elmstateinds);
        if any(circularmask)
            statediff(circularmask,:) = smod(statediff(circularmask,:),-pi,pi);
        end
        solutionelmdists(i,1:i-1) = sqrt(sum(...
            bsxfun(@rdivide,statediff,sqrt(obj.patternstatevar(elmstateinds,:))).^2,...
            1));
    end
    solutionelmdists = solutionelmdists + solutionelmdists';
    
    % remove solutions with elements that are too similar
    if obj.settings.elmsolutiondistancethresh < inf && size(obj.solutionstate,2) > 1
        keepcombs = false(1,size(obj.solutioncombs,2));
        
        % cluster linear state estimates by pair-wise distance
        l = linkage(squareform(solutionelmdists,'tovector'),'complete');
        maxclusterdist = sqrt(obj.settings.synctolerance)*obj.settings.elmsolutiondistancethresh;
        combsolutioninds = cluster(l,'cutoff',maxclusterdist,'criterion','distance');
        solutioncombinds = array2cell_mex(1:numel(combsolutioninds),combsolutioninds');
        
        disp(['    Keeping ',num2str(numel(solutioncombinds)),' solutions that are distinct enough based on elements poses ...']);
        
        % pick the combination in each cluster with most fixed relationships
        for i=1:numel(solutioncombinds)
            combs = obj.solutioncombs(:,solutioncombinds{i});
            fixedcount = sum(combs~=0,1);
            [maxfixedcount,~] = max(fixedcount);
            inds = find(fixedcount == maxfixedcount);
            
            fixedtoorigcount = sum(combs(:,inds)==1,1);
            [~,ind] = max(fixedtoorigcount);
            ind = inds(ind);
            
            keepcombs(solutioncombinds{i}(ind)) = true;
        end
        
        obj.solutioncombs(:,not(keepcombs)) = [];    
        obj.solutioncost(:,not(keepcombs)) = [];
        obj.solutionstate(:,not(keepcombs)) = [];
        obj.solutionstatevar(:,not(keepcombs)) = [];
        obj.solutionstartstate(:,not(keepcombs)) = [];
        obj.solutionsubstateinds(not(keepcombs)) = [];
        solutiondists(:,not(keepcombs)) = [];
        solutiondists(not(keepcombs),:) = [];  
        solutionelmdists(:,not(keepcombs)) = [];
        solutionelmdists(not(keepcombs),:) = [];  
    end
    
    % sort solutions by dissimilarity to each other (go down the linkage
    % tree)
    if size(obj.solutionstate,2) > 1
        % cluster linear state estimates by pair-wise distance
        l = linkage(squareform(solutionelmdists,'tovector'),'complete'); % for element pose distances
        
        % get all clusters in the linkage tree
        nsols = size(obj.solutionstate,2);
        solutioninds = nan(1,size(l,1));
        clustersolinds = [1:nsols,nan(1,size(l,1))];
        for i=1:size(l,1)
            solind1 = clustersolinds(l(i,1));
            solind2 = clustersolinds(l(i,2));
            
            comb1 = obj.solutioncombs(:,solind1);
            comb2 = obj.solutioncombs(:,solind2);
            
            fixedcount1 = sum(comb1~=0,1);
            fixedcount2 = sum(comb2~=0,1);
            if fixedcount1 == fixedcount2
                fixedcount1 = sum(comb1==1,1);
                fixedcount2 = sum(comb2==1,1);
            end
            
            if fixedcount1 > fixedcount2
                clustersolinds(i+nsols) = solind1;
                solutioninds(i) = solind2;
            else
                clustersolinds(i+nsols) = solind2;
                solutioninds(i) = solind1;
            end
        end
        solutioninds(:,end+1) = clustersolinds(end);
        solutioninds = solutioninds(end:-1:1);
        
        
        obj.solutioncombs = obj.solutioncombs(:,solutioninds);    
        obj.solutioncost = obj.solutioncost(:,solutioninds);
        obj.solutionstate = obj.solutionstate(:,solutioninds);
        obj.solutionstatevar = obj.solutionstatevar(:,solutioninds);
        obj.solutionstartstate = obj.solutionstartstate(:,solutioninds);
        obj.solutionsubstateinds = obj.solutionsubstateinds(solutioninds);
        solutiondists = solutiondists(:,solutioninds);
        solutiondists = solutiondists(solutioninds,:);
        solutionelmdists = solutionelmdists(:,solutioninds);
        solutionelmdists = solutionelmdists(solutioninds,:);
    end
    
    obj.nsol_distinct = size(obj.solutioncombs,2);
    
%     % sort by solution cost instead
%     [~,perm] = sort(obj.solutioncost,'descend');
%         obj.solutioncombs = obj.solutioncombs(:,perm);    
%         obj.solutioncost = obj.solutioncost(:,perm);
%         obj.solutionstate = obj.solutionstate(:,perm);
%         obj.solutionstatevar = obj.solutionstatevar(:,perm);
%         obj.solutionstartstate = obj.solutionstartstate(:,perm);
%         obj.solutionsubstateinds = obj.solutionsubstateinds(perm);
%         solutiondists = solutiondists(:,perm);
%         solutiondists = solutiondists(perm,:);
%         solutionelmdists = solutionelmdists(:,perm);
%         solutionelmdists = solutionelmdists(perm,:);
    
    % keep the max. number of solutions
    if obj.settings.maxsolutions < inf
        keepn = min(obj.settings.maxsolutions,size(obj.solutionstate,2));
        obj.solutioncombs = obj.solutioncombs(:,1:keepn);    
        obj.solutioncost = obj.solutioncost(:,1:keepn);
        obj.solutionstate = obj.solutionstate(:,1:keepn);
        obj.solutionstatevar = obj.solutionstatevar(:,1:keepn);
        obj.solutionstartstate = obj.solutionstartstate(:,1:keepn);
        obj.solutionsubstateinds = obj.solutionsubstateinds(1:keepn);
        solutiondists = solutiondists(:,1:keepn);
        solutiondists = solutiondists(1:keepn,:); %#ok<NASGU>
        solutionelmdists = solutionelmdists(:,1:keepn);
        solutionelmdists = solutionelmdists(1:keepn,:); %#ok<NASGU>
    end
    
    for i=1:size(obj.solutionstate,2)
        % re-evaluate all free relationships in the solution
        % (the linear approximation results in some discrepancies in the element
        % positions and relationships values)
        solutionfreenodeinds = obj.state2nodeinds(obj.solutionsubstateinds{i});
        solutionfreenodeinds(isnan(solutionfreenodeinds)) = [];
        solutionfreerelinds = obj.pgraph.node2relinds(solutionfreenodeinds);
        solutionfreerelinds(isnan(solutionfreerelinds)) = [];
        [obj.solutionstate(:,i),obj.solutionstatevar(:,i)] = obj.evalrelsFromElements(...
            obj.solutionstate(:,i),obj.solutionstatevar(:,i),solutionfreerelinds);

%         % set fixed state of nodes based on closeness of their values to
%         % the new/original values
%         obj.solutionstate(:,i) = obj.nodefixFromNodestate(obj.solutionstate(:,i));
%         obj.solutionstate(:,i) = obj.groupfixFromNodestate(obj.solutionstate(:,i),obj.solutionstatevar(:,i));
    end
    
    obj.solutionactiveconsrelinds = repmat({activeconsrelinds},1,size(obj.solutionstate,2));
    obj.solutionactivefixnodeinds = repmat({activefixnodeinds},1,size(obj.solutionstate,2));
    obj.solutionactiveregnodeinds = repmat({activeregnodeinds},1,size(obj.solutionstate,2));
    
    % re-evaluate the cost (the current cost is only the consistency cost
    % of the nodes)
    for i=1:size(obj.solutionstate,2)
        % The consistency cost is not useful here, since the linear
        % Approximation does not fulfill the fixed relationships exactly.
        % Use only regularization term.
        obj.solutioncost(i) = obj.evalobjective_vecnorm(...
            obj.solutionstate(:,i),obj.solutionstatevar(:,i),obj.solutionstate(obj.solutionsubstateinds{i},i),obj.solutionsubstateinds{i},...
            obj.solutionactiveconsrelinds{i},obj.solutionactivefixnodeinds{i},obj.solutionactiveregnodeinds{i},...
            0,0,obj.settings.regweight,...%obj.settings.consweight,obj.settings.fixweight,...
            0,obj.settings.regw_elmgrpschanged,obj.settings.regw_relgrpschanged,obj.settings.regw_grpsbroken); % obj.settings.regw_relfixed
    end
    
%     % sort by ascending cost
%     [~,perm] = sort(obj.solutioncost,'ascend');
%     obj.solutioncombs = obj.solutioncombs(:,perm);
%     obj.solutioncost = obj.solutioncost(perm);
%     obj.solutionstate = obj.solutionstate(:,perm);
%     obj.solutionstatevar = obj.solutionstatevar(:,perm);
%     obj.solutionstartstate = obj.solutionstartstate(:,perm);
%     obj.solutionsubstateinds = obj.solutionsubstateinds(perm);
%     obj.solutionactiveconsrelinds = obj.solutionactiveconsrelinds(perm);
%     obj.solutionactivefixnodeinds = obj.solutionactivefixnodeinds(perm);
%     obj.solutionactiveregnodeinds = obj.solutionactiveregnodeinds(perm);
    
%     clusterthreshold = sqrt(obj.settings.synctolerance);
%     keepinds = StandardPatternSolver.findUniqueStates(...
%         obj.solutionstate,obj.solutionstatevar,clusterthreshold);
%     
%     obj.solutioncombs = obj.solutioncombs(:,keepinds);
%     obj.solutioncost = obj.solutioncost(keepinds);
%     obj.solutionstate = obj.solutionstate(:,keepinds);
%     obj.solutionstatevar = obj.solutionstatevar(:,keepinds);
%     obj.solutionstartstate = obj.solutionstartstate(:,keepinds);
%     obj.solutionsubstateinds = obj.solutionsubstateinds(keepinds);
%     obj.solutionactiveconsrelinds = obj.solutionactiveconsrelinds(keepinds);
%     obj.solutionactivefixnodeinds = obj.solutionactivefixnodeinds(keepinds);
%     obj.solutionactiveregnodeinds = obj.solutionactiveregnodeinds(keepinds);

    
    % get element poses and relationship values from states
    obj.solutionelmposes = cell(1,size(obj.solutionstate,2));
    obj.solutionrelvals = cell(1,size(obj.solutionstate,2));
    for i=1:size(obj.solutionstate,2)
        eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        eposes(1:obj.settings.elmposedim,:) = reshape(obj.solutionstate(obj.elmposestateinds(:),i),obj.settings.elmposedim,numel(obj.pgraph.elms));
        eposes = PatternSolver.sim2affinepose(eposes);
        obj.solutionelmposes{i} = eposes;
        obj.solutionrelvals{i} = obj.solutionstate(obj.relstateinds,i)';
    end

    % set outputs
    selmposes = obj.solutionelmposes;
    srelvals = obj.solutionrelvals;
    scost = obj.solutioncost;
    
    disp('done');
end

function combs = generateCombinations(obj,maxgencombs,maxoutcombs,componentnodeinds,usereditednodeinds) %#ok<INUSD>
    
%     rngstate = rng(50152527); %  seed rng to get repeatable results
    
    componentrelinds = obj.pgraph.node2relinds(componentnodeinds);
    componentrelinds(isnan(componentrelinds)) = [];
    componentrelgrpinds = find(any(obj.relgrps(componentrelinds,:),1));
    
    
    componentrelgrpnchoices = cellfun(@(x) numel(x),obj.relgrpchangedinds(1,componentrelgrpinds))+2;
    
    % generate combinations of indicator values for the relationship groups
    % (each group can either be set to free, old value, or any of the new
    % values that members of the group have been changed to)
    % 0: free, 1: fixed to original value, >1: fixed to one of the new values in the group
    totalncombs = prod(componentrelgrpnchoices);
    if totalncombs > 2^50
        % the chance that we pick even a single duplicate at this point
        % in 10000 picks is 8*10^-8
        combs = 0.000001 + rand(numel(componentrelgrpnchoices),min(totalncombs,maxgencombs))*0.99999;
        combs = round(bsxfun(@times,combs,componentrelgrpnchoices')+0.5) - 1;
    else
        combs = randpermvec(zeros(1,numel(componentrelgrpinds)),componentrelgrpnchoices-1,min(totalncombs,maxgencombs))';
    end
    
    disp(['    Total of ',num2str(totalncombs),' possible solutions...']);
    disp(['    Randomly generating ',num2str(size(combs,2)),' solutions...']);
    
    obj.nsol_total = totalncombs;
    obj.nsol_randompick = size(combs,2);
    
    % initial filtering
    
    componentgrpinds = obj.relgrpinds(componentrelgrpinds);
    fixedtonewmask = combs >= 2;
    
    % remove combinations where a group is fixed to new but none of its
    % childs is fixed to new
    filtermask = true(1,size(combs,2));
    for i=1:numel(componentgrpinds)
        gchilds = obj.pgraph.groupChilds(componentgrpinds(i),componentgrpinds);
        if not(isempty(gchilds))
            filtermask(fixedtonewmask(i,:) & not(any(fixedtonewmask(gchilds,:),1))) = false;
        end
    end
    combs = combs(:,filtermask);
    fixedtonewmask = fixedtonewmask(:,filtermask);
    
    disp(['    Filtering based on consistent changes, keeping ',num2str(size(combs,2)),' solutions.']);
    
    % remove combinations where different pattern graph branches are
    % fixed to new (i.e. parent and child are fixed to new should not be
    % removed)
    % that means any two groups on the same level?
    componentgrplvl = zeros(1,numel(componentgrpinds));
    for i=1:numel(componentgrpinds)
        membernodeinds = find(obj.pgraph.groups(:,componentgrpinds(i)));
        cgrplvl = unique(obj.pgraph.level(membernodeinds)); %#ok<FNDSB>
        if numel(cgrplvl) ~= 1
            error('Group with members on multiple pattern graph levels found.');
        end
        componentgrplvl(i) = cgrplvl;
    end
    filtermask = true(1,size(combs,2));
    for i=min(componentgrplvl):max(componentgrplvl)
        filtermask = filtermask & sum(fixedtonewmask(componentgrplvl == i,:),1) <= 1;
    end
    combs = combs(:,filtermask);
    fixedtonewmask = fixedtonewmask(:,filtermask); %#ok<NASGU>
    
    disp(['    Filtering based on single change, keeping ',num2str(size(combs,2)),' solutions.']);
    
    obj.nsol_intuitive = size(combs,2);
    
    
    % pick a random subset of the larger random subset of combinations,
    % with higher chance of picking combinations with many fixed
    % relationships
    ncombs = min(maxoutcombs,size(combs,2));
    combfixedcount = sum(combs~=0,1);
    
    
    disp(['    Randomly picking a subset of ',num2str(ncombs),' solutions...']);
    
    obj.nsol_linrandompick = ncombs;
    
    if ncombs < size(combs,2)
        pickedcombmask = false(1,size(combs,2));
        while true
            nremaining = ncombs - sum(pickedcombmask);

            if nremaining <= 0
                break;
            end

            unpickedinds = find(not(pickedcombmask));

            % higher chance of picking combinations with many fixed
            % relationships
            strata = [0,cumsum(combfixedcount(unpickedinds))];

            r = mindistrand(nremaining,min(diff(strata)),0,strata(end));

            combinds = nan(1,numel(r));
            for i=1:numel(r)
                combinds(i) = unpickedinds(min(numel(unpickedinds),find(r(i) - strata >= 0,1,'last')));
            end

            pickedcombmask(combinds) = true;
        end
    else
        pickedcombmask = true(1,size(combs,2));
    end
    
    combs = combs(:,pickedcombmask);
    
%     rng(rngstate); % temp: set rng back to original state
end

function [combstate,combstatevar,combsubstatefreemask,combisvalid] = getCombinationState(...
        obj,comb,combrelgrpinds,metarelsandchilds,substateinds,usereditedelminds)
    
    elmoffset = obj.pgraph.elmoffset;
%     usereditedelminds = find(any(obj.elmposechanged,1));
    usereditednodeinds = usereditedelminds + elmoffset;
    unchangedrelnodeinds = obj.pgraph.rel2nodeinds(not(obj.relvalchanged));
    elmnodeinds = (1:numel(obj.pgraph.elms)) + elmoffset;
    
    relfixorigstateinds = obj.nodefixorig2stateinds(obj.pgraph.rel2nodeinds);
    relfixnewstateinds = obj.nodefixnew2stateinds(obj.pgraph.rel2nodeinds);
    
    combstate = obj.patternstate;
    combstate(relfixorigstateinds) = 0;%1;
    combstate(relfixnewstateinds) = 0;
    combstatevar = obj.patternstatevar;

    combstatefreemask = false(size(combstate));
    combstatefreemask(substateinds) = true;
    for j=1:numel(combrelgrpinds)
        grprelinds = logical(obj.relgrps(:,combrelgrpinds(j)));
        grpcircular = obj.relgrpcircular(combrelgrpinds(j));

        stateinds = relfixorigstateinds(grprelinds);
        combstate(stateinds) = comb(j) == 1;
        combstatefreemask(stateinds) = false;


        stateinds = relfixnewstateinds(grprelinds);
        if comb(j)>=2
%             combstate(stateinds) = ismember(find(grprelinds),obj.relgrpchangedinds{1,combrelgrps(j)}{comb(j)-1});
            combstate(stateinds) = 1; % here for set all in group to new
        else
            combstate(stateinds) = 0;
        end
        combstatefreemask(stateinds) = false;

        if comb(j) == 1
            % set all group relationships to their original values
            stateinds = obj.relstateinds(grprelinds);
            combstate(stateinds) = obj.origrelvals(grprelinds);
            combstatefreemask(stateinds) = false;
        elseif comb(j) >= 2

            % fix all group relationships to one of the new
            % values (wich one is determined by comb(j))
            stateinds = obj.relstateinds(grprelinds);

            if strcmp(obj.settings.relgrouptype,'change')
                if grpcircular
                    combstate(stateinds) = smod(obj.origrelvals(grprelinds) + obj.relgrpchangedinds{2,combrelgrpinds(j)}(comb(j)-1),-pi,pi); % intialize all members to new value
                else
                    combstate(stateinds) = obj.origrelvals(grprelinds) + obj.relgrpchangedinds{2,combrelgrpinds(j)}(comb(j)-1); % intialize all members to new value
                end
            elseif strcmp(obj.settings.relgrouptype,'value')
                combstate(stateinds) = obj.relgrpchangedinds{2,combrelgrpinds(j)}(comb(j)-1); % intialize all members to new value
            end

            % fix only the members that share one of the new
            % values to this new value (wich one is determined
            % by comb(j))
%             stateinds = obj.relstateinds(obj.relgrpchangedinds{1,combrelgrps(j)}{comb(j)-1});
            stateinds = obj.relstateinds(grprelinds); % here for set all in group to new

            combstatefreemask(stateinds) = false;
        end
    end

    % modify state with the element(s) changed by the user
    combstate(obj.elmposestateinds(:,usereditedelminds)) = obj.newelmposes(:,usereditedelminds);
    combstate(obj.nodefixorig2stateinds(usereditednodeinds)) = 0;
    combstate(obj.nodefixnew2stateinds(usereditednodeinds)) = 1;
    
    
    % change here to set what should be constrained to the user edit: just
    % the position of the elements or the orientation of the elements as
    % well (the user might only move the element, leave the rotation alone
    % and expect the element to be rotated by our method)
%     combstatefreemask(obj.elmposestateinds(:,userchangedelmind)) = false; % do not change the element(s) modified by the user
%     combstatefreemask(obj.elmposestateinds(1:2,userchangedelminds)) = false; % do not change the element(s) modified by the user % nhere
    
    combstatefreemask(obj.nodefixorig2stateinds(usereditednodeinds)) = false; % do not change the fixed status of the element(s) changed by the user
    combstatefreemask(obj.nodefixnew2stateinds(usereditednodeinds)) = false; % do not change the fixed status of the element(s) changed by the user
    combstatefreemask(obj.nodefixorig2stateinds(elmnodeinds)) = false; % do not change the fixed status of element nodes
    combstatefreemask(obj.nodefixnew2stateinds(elmnodeinds)) = false; % do not change the fixed status of element nodes
    combstatefreemask(obj.nodefixnew2stateinds(unchangedrelnodeinds)) = false; % do not change the fixed to new status of unchanged relationship nodes (should remain at 0)
    combstatefreemask(obj.grpfix2stateinds) = false; % do not change groupfix state
    
    combsubstatefreemask = combstatefreemask(substateinds);

    
    % test combination for validity
    
    combisvalid = true;
    
    % search for combinations where the relationship and both childs are
    % fixed (either to old or new value) and test if there are conflicting
    % fixed relationships in the combination
    relfixedmask = any(obj.relgrps(:,combrelgrpinds(comb > 0)),2);
    relandchildsfixedinds = metarelsandchilds(1,all(relfixedmask(metarelsandchilds),1));
    if combisvalid && not(isempty(relandchildsfixedinds))

        wnormresidual = evalobjective_vector(...
            obj,combstate,combstatevar,combstate,(1:numel(combstate))',...
            relandchildsfixedinds,zeros(1,0),zeros(1,0),...
            1,0,0,...
            0,0,0,0);
        wnormresidual = wnormresidual.^2;

        conflictrelinds = relandchildsfixedinds(wnormresidual > obj.settings.synctolerance);

        combisvalid = isempty(conflictrelinds);
    end
end

% estimate solution based on linear constraints (linear realtionships)
function se = linear_solution_diffmanip(...
        obj,newstate,origstate,origstatevar,substateinds,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...    
        consweight,fixweight,regweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken)
    
    % can be thought of as change in relationships that is estimated not
    % alter the relationship values (estimated at the original state)
    
    % do not include nodefix indicators the substate for estimating the jacobian
    [~,origjacobian] = obj.evalobjective_vector(...
        origstate,origstatevar,origstate(substateinds),substateinds,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        consweight,fixweight,regweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    
    statechange = newstate-origstate;
    if any(obj.statecircular)
        statechange(obj.statecircular) = smod(statechange(obj.statecircular),-pi,pi);
    end
    
    se = origstate;
    lagrangemult = (origjacobian*origjacobian') \ (origjacobian*statechange(substateinds));
    correctedstatechange = statechange(substateinds) - origjacobian' * lagrangemult;
    se(substateinds) = origstate(substateinds) + correctedstatechange;
    if any(obj.statecircular)
        se(obj.statecircular) = smod(se(obj.statecircular),-pi,pi); %#ok<NASGU>
    end
    
    error('Need to update this method.');
end

% output: state estimates and objective function value estimates
% [se,ove,se2,ove2] = linear_solution_origstart(
%         obj,origstate,origstatevar,fixedstate,substateinds,substatefreemask,
%         origval,origjacobian)
% [se,ove,se2,ove2] = linear_solution_origstart(
%         obj,origstate,origstatevar,fixedstate,substateinds,substatefreemask,
%         activeconsrelinds,activefixnodeinds,activeregnodeinds,...
%         consweight,fixweight,regweight,...
%         regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken)
function [se,ove,se2,ove2] = linear_solution_origstart(...
        obj,origstate,origstatevar,fixedstate,substateinds,substatefreemask,origval,origjacobian,invfreejac,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        consweight,fixweight,regweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken)
    
    % can be thought of as setting change in free relationships to value
    % that is estimated to cancel the estimated change in relationship
    % values introduced by fixed relationship
    % (estimations at original state)

    % get fixed/free state indices
    substatefixedmask = not(substatefreemask);
    fixedsubstateinds = substateinds(substatefixedmask);
    freesubstateinds = substateinds(substatefreemask);
    
    % Taylor expansion at the original state
    if (ischar(origval) && strcmp(origval,'compute')) || (ischar(origjacobian) && strcmp(origjacobian,'compute'))
        % zeroth-order and first-order approximation
        % value and jacobian of the relationships at the original state
        [origval,origjacobian] = obj.evalobjective_vector(...
            origstate,origstatevar,origstate(substateinds),substateinds,...
            activeconsrelinds,activefixnodeinds,activeregnodeinds,...
            consweight,fixweight,regweight,...
            regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    
%         % first-order approximation
%         % estimate jacobian of the relationships at the original state
%         origjacobian = obj.estimate_jacobian(...
%             origstate,origstatevar,origstate(substateinds),substateinds,...
%             activeconsrelinds,activefixnodeinds,activeregnodeinds,...
%             consweight,fixweight,regweight,...
%             regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    end
    
    % get fixed state change
    fixedstatechange = fixedstate(fixedsubstateinds) - origstate(fixedsubstateinds);
    if any(obj.statecircular(fixedsubstateinds))
        fixedstatechange(obj.statecircular(fixedsubstateinds)) = smod(fixedstatechange(obj.statecircular(fixedsubstateinds)),-pi,pi);
    end
    
    % find best values for free state change    
    % minimize taylor expansion at original state over the free state
    % subspace, while keeping the fixed state subspace fixed
    freestatechange = invfreejac * ...
        (-origval - origjacobian(:,substatefixedmask) * fixedstatechange);
    if any(obj.statecircular(freesubstateinds))
        freestatechange(obj.statecircular(freesubstateinds)) = smod(freestatechange(obj.statecircular(freesubstateinds)),-pi,pi);
    end
    
    % assemble solution
    se = fixedstate;
    se(fixedsubstateinds) = origstate(fixedsubstateinds) + fixedstatechange;
    se(freesubstateinds) = origstate(freesubstateinds) + freestatechange;
    if any(obj.statecircular)
        se(obj.statecircular) = smod(se(obj.statecircular),-pi,pi);
    end
    
    % taylor expansion (with two terms)
    ove = origval + origjacobian(:,substatefixedmask) * fixedstatechange + origjacobian(:,substatefreemask) * freestatechange;
    
    if nargout >= 3
        freestatechange2 = origjacobian(:,substatefreemask) \ ...
            (-origval - origjacobian(:,substatefixedmask) * fixedstatechange);
        if any(obj.statecircular(freesubstateinds))
            freestatechange2(obj.statecircular(freesubstateinds)) = smod(freestatechange2(obj.statecircular(freesubstateinds)),-pi,pi);
        end

        se2 = fixedstate;
        se2(fixedsubstateinds) = origstate(fixedsubstateinds) + fixedstatechange;
        se2(freesubstateinds) = origstate(freesubstateinds) + freestatechange2;
        
        if any(obj.statecircular)
            se2(obj.statecircular) = smod(se2(obj.statecircular),-pi,pi);
        end
        
        ove2 = origval + origjacobian(:,substatefixedmask) * fixedstatechange + origjacobian(:,substatefreemask) * freestatechange2;
    end
end

function [se,ove,se2,ove2] = linear_solution_fixedstart(...
        obj,fixedstate,fixedstatevar,substateinds,substatefreemask,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        consweight,fixweight,regweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken)
    
    % can be thought of as change in free relationships to value that is
    % estimated to bring all relationships to 0 (estimated at original
    % state but with fixed relationships)

    % get fixed/free state indices
    freesubstateinds = substateinds(substatefreemask);
    
    % Taylor expansion at the original state but with the fixed
    % relationship values
    
    % zeroth-order and first-order approximation
    % estimate jacobian of the relationships at the original state but with
    % fixed values
    [fixedval,fixedjacobian] = obj.evalobjective_vector(...
        fixedstate,fixedstatevar,fixedstate(substateinds),substateinds,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        consweight,fixweight,regweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    
%     % first-order approximation
%     % estimate jacobian of the relationships at the original state
%     fixedjacobian = obj.estimate_jacobian(...
%         fixedstate,fixedstatevar,fixedstate(substateinds),substateinds,...
%         activeconsrelinds,activefixnodeinds,activeregnodeinds,...    
%         consweight,fixweight,regweight,...
%         regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    
    % find best values for free state change
    % minimze taylor expansion at the original state modified by the fixed
    % relationship values over the free state subspace 
    freestatechange = pinv(fixedjacobian(:,substatefreemask)) * -fixedval;
    if any(obj.statecircular(freesubstateinds))
        freestatechange(obj.statecircular(freesubstateinds)) = smod(freestatechange(obj.statecircular(freesubstateinds)),-pi,pi);
    end
    
    % assemble solution
    se = fixedstate;
    se(freesubstateinds) = fixedstate(freesubstateinds) + freestatechange;
    if any(obj.statecircular)
        se(obj.statecircular) = smod(se(obj.statecircular),-pi,pi);
    end
    
    % taylor expansion (with two terms)
    ove = fixedval + fixedjacobian(:,substatefreemask) * freestatechange;
    
    if nargout >= 3
        freestatechange2 = fixedjacobian(:,substatefreemask) \ -fixedval;
        if any(obj.statecircular(freesubstateinds))
            freestatechange2(obj.statecircular(freesubstateinds)) = smod(freestatechange2(obj.statecircular(freesubstateinds)),-pi,pi);
        end

        se2 = fixedstate;
        se2(freesubstateinds) = fixedstate(freesubstateinds) + freestatechange2;
        if any(obj.statecircular)
            se2(obj.statecircular) = smod(se2(obj.statecircular),-pi,pi);
        end
        
        ove2 = fixedval + fixedjacobian(:,substatefreemask) * freestatechange2;
    end
end

function setVisfunction(obj,func)
    obj.visfunc = func;
end

function clearFunctions(obj)
    % needed to save the solver
    obj.visfunc = [];
    obj.relfs = cell(1,0);
end

function stop = outfun(obj,fullstate,fullstatevar,substate,substateinds,iter,fval) %#ok<INUSL>

    if nargin < 4
        substate = zeros(0,1);
        substateinds = zeros(0,1);
    end
    
    state = fullstate;
    state(substateinds) = substate;
    
    if nargin >= 7
        disp(['*** iteration ',num2str(iter),':']);
        disp(['    objective function value: ',num2str(fval)]);
    end
    
    if not(isempty(obj.visfunc))
        elmposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        
        elmposes(1:obj.settings.elmposedim,:) = reshape(state(obj.elmposestateinds),obj.settings.elmposedim,[]);
        
        nodefixorig = state(obj.nodefixorig2stateinds);
        nodefixnew = state(obj.nodefixnew2stateinds);
        
        elmposes = PatternSolver.sim2affinepose(elmposes);
        
        stop = obj.visfunc(...
            obj,...
            elmposes,...
            nodefixorig,...
            nodefixnew);
        
    else
        stop = false;
    end
end

end

end
