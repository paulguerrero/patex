classdef ChooserelsBCDPropagationSolver < PatternSolver
    
properties(SetAccess=protected)
    % last inputs
    newelmposes = zeros(2,0);
    
    % last solution
    solutionchoiceinds = zeros(1,0);
    solutioncost = zeros(1,0);
    solutionstate = [];
    solutionstatevar = [];
    solutionelmposes = cell(1,0);
    solutionrelvals = cell(1,0);

%     groupmode = 'implicit'; % 'explicit' or 'implicit' (extend pattern graph)
    groupmode = 'explicit'; % 'explicit' or 'implicit' (extend pattern graph)
    
    synctolerance = 0.001;
    
    propagatestrength = 1;
    maxiter = 1000;
%     maxzerodeltaiter = 50;
    minsyncdelta = 0.001;
    deltahistlen = 50;
    

    visualize = 'all';
%     visualize = 'steps';
%     visualize = 'solutions';
%     visualize = 'none';
end

properties(Access=protected)
    oldelmposes = zeros(2,0);
    relweights = zeros(1,0);
    elmweights = zeros(1,0);

    choicestate = [];
    choicestatevar = [];
    choicenodevisited = [];
    choicegroupvisited = [];
    choicenodefixed = []; % -1 for undecided, 0 for not fixed, 1 for fixed
    choicenodesyncqueue = cell(1,0);
    choiceiter = zeros(1,0);
    choiceregcost = zeros(1,0);
    choicesynccost = zeros(1,0);
    choicesynccostdelta = [];
    choiceclosed = false(1,0);
    choicetree = sparse([]);
    
    fullstate = zeros(0,1);
    fullstatevar = zeros(0,1);
    substateinds = zeros(0,1); % substate being optimized (like block coordinate descent)
    activerelinds = zeros(1,0); % activated constraints (equations)
%     fixedstateinds = zeros(0,1);
    
    % for visualization and for computing the regularizer
    currentnodeinds = zeros(1,0);
    currentvisitedmask = false(0,1);
    currentsyncmask = true(0,1);
    currentfixedflag = false(0,1);
    
    visfunc = [];
    
    stop = false;
end

methods

% obj = ChooserelsBCDPropagationSolver
% obj = ChooserelsBCDPropagationSolver(pgraph)
function obj = ChooserelsBCDPropagationSolver(varargin)
    superarginds = zeros(1,0);
    if numel(varargin) == 1
        superarginds = 1;
    end
    
    obj@PatternSolver(varargin{superarginds});
    varargin(superarginds) = [];
    
    if numel(varargin) == 0
        % do nothing
    else
        error('Invalid arguments.');
    end
end

function clear(obj)
    obj.clear@PatternSolver;
    
    obj.solutionchoiceinds = zeros(1,0);
    obj.solutioncost = zeros(1,0);
    obj.solutionstate = [];
    obj.solutionstatevar = [];
    obj.solutionelmposes = cell(1,0);
    obj.solutionrelvals = cell(1,0);
    
    obj.oldelmposes = zeros(2,0);
    obj.relweights = zeros(1,0);
    obj.elmweights = zeros(1,0);
    
    obj.choicestate = [];
    obj.choicestatevar = [];
    obj.choicenodevisited = [];
    obj.choicegroupvisited = [];
    obj.choicenodefixed = [];
    obj.choicenodesyncqueue = cell(1,0);
    obj.choiceiter = zeros(1,0);
    obj.choiceregcost = zeros(1,0);
    obj.choicesynccost = zeros(1,0);
    obj.choicesynccostdelta = [];
    obj.choiceclosed = false(1,0);
    obj.choicetree = sparse([]);
    
    obj.fullstate = zeros(0,1);
    obj.fullstatevar = zeros(0,1);
    obj.substateinds = zeros(0,1);
    obj.activerelinds = zeros(1,0);
%     obj.fixedstateinds = zeros(0,1);
    
    obj.currentnodeinds = zeros(1,0);
    obj.currentvisitedmask = false(0,1);
    obj.currentsyncmask = true(0,1);
    obj.currentfixedflag = zeros(0,1);
    
    obj.stop = false;
end

function prepare(obj,newelmposes)

    obj.prepare@PatternSolver;
    
    nodecount = numel(obj.pgraph.nodes);
    statedim = numel(obj.patternstate);
    groupcount = size(obj.pgraph.groups,2);
    
    obj.oldelmposes = [obj.pgraph.elms.pose];
    obj.oldelmposes = obj.oldelmposes(1:obj.elmposedim,:);
    
    obj.newelmposes = PatternSolver.affine2simpose(newelmposes);
    obj.newelmposes = obj.newelmposes(1:obj.elmposedim,:);
    
    obj.relweights = ones(1,numel(obj.pgraph.rels));
    obj.elmweights = ones(1,numel(obj.pgraph.elms));
    
    obj.choicestate = zeros(statedim,0);
    obj.choicestatevar = zeros(statedim,0);
    obj.choicenodevisited = false(nodecount,0);
    obj.choicegroupvisited = false(groupcount,0);
    obj.choicenodefixed = zeros(nodecount,0);
    obj.choicenodesyncqueue = cell(1,0);
    obj.choiceiter = zeros(1,0);
    obj.choiceregcost = zeros(1,0);    
    obj.choicesynccost = zeros(1,0);
    obj.choicesynccostdelta = zeros(obj.deltahistlen,0);
    obj.choiceclosed = false(1,0);
    obj.choicetree = sparse([]);
    
    obj.fullstate = nan(statedim,1);
    obj.fullstatevar = nan(statedim,1);
    obj.substateinds = zeros(0,1);
    obj.activerelinds = zeros(1,0);
%     obj.fixedstateinds = zeros(0,1);

    obj.currentnodeinds = zeros(1,0);
    obj.currentvisitedmask = false(nodecount,1);
    obj.currentsyncmask = true(nodecount,1);
    obj.currentfixedflag = zeros(nodecount,1);
    
    obj.stop = false;
end

function [relval,relvar] = computerel(obj,state,nodeind)
                
    input1stateinds = obj.node2stateinds{find(obj.pgraph.adjacency(nodeind,:) == -1)}; %#ok<FNDSB>
    input2stateinds = obj.node2stateinds{find(obj.pgraph.adjacency(nodeind,:) == 1)}; %#ok<FNDSB>
    
    rel = obj.pgraph.rels(obj.pgraph.node2relinds(nodeind));
    [relval,relvar] = rel.compute_o(...
        state(input1stateinds),...
        state(input2stateinds));
end

function [relval,relvar,residual,normresidual,wnormresidual] = computeresidual(obj,state,statevar,nodeinds)
    
    if not(iscolumn(nodeinds))
        nodeinds = nodeinds(:);
    end
    
    nstateind = cat(1,obj.node2stateinds{nodeinds});
    nrelinds = obj.pgraph.node2relinds(nodeinds);
    
    relval = zeros(numel(nodeinds),1);
    relvar = zeros(numel(nodeinds),1);
    for i=1:numel(nodeinds)
        [relval(i),relvar(i)] = obj.computerel(state,nodeinds(i));
    end

    residual = relval - state(nstateind);
    mask = [obj.pgraph.rels(nrelinds).iscircular];
    residual(mask) = smod(residual(mask),-pi,pi);
    
    normresidual = (residual.^2 ./ statevar(nstateind));
    wnormresidual = normresidual .* obj.relweights(nrelinds)';
end


function [state,statevar,objectiveval] = optimizenodes(...
        obj,state,statevar,nodeinds,fixedflag,visitedmask)
    
    % only optmize free nodes
    freenodeinds = nodeinds(fixedflag(nodeinds) < 1);
    freenodeparentinds = obj.parentnodeinds(freenodeinds);

    obj.fullstate = state;
    obj.fullstatevar = statevar;
    obj.substateinds = unique(cat(1,obj.node2stateinds{freenodeinds}));
    
    % determine which relationships should be active to optimize the node
    % values

    % active relationships: relationships connecting the free nodes with
    % their children (freenodeinds) and parents,siblings (allpnodeinds)
    obj.activerelinds = obj.pgraph.node2relinds(unique([freenodeinds,freenodeparentinds{:}]));
    obj.activerelinds(isnan(obj.activerelinds)) = [];

    freenodemask = false(size(visitedmask));
    freenodemask(freenodeinds) = true;
   
    originalrelweights = obj.relweights;
    
    activerelparent = obj.pgraph.rel2nodeinds(obj.activerelinds);
    activerelchilds = obj.childnodeinds(activerelparent);
    % number of nodes visited for each relationships
    activerelunvisited = ...
        cellfun(@(x) sum(not(visitedmask(x)) & not(freenodemask(x))),activerelchilds)' + ...
        (not(visitedmask(activerelparent)) & not(freenodemask(activerelparent)));
    if obj.propagatestrength > 0 && not(all(activerelunvisited == activerelunvisited(1)))
        % if not all of the relationships have the same amount of unvisited
        % nodes (these are the nodes residuals can be pushed to)
        
        % propagation is done from active relations in this subset of
        % activerelinds to all other active realations
        mask = activerelunvisited == min(activerelunvisited);
        visitedactiverelinds = obj.activerelinds(mask);
        visitedactivenodelevel = obj.pgraph.level(activerelparent(mask));
        mask = visitedactivenodelevel == max(visitedactivenodelevel);
        propsrcrelinds = visitedactiverelinds(mask);
        propsrcrelmask = false(1,numel(obj.pgraph.rels));
        propsrcrelmask(propsrcrelinds) = true;
        
        obj.relweights(not(propsrcrelmask)) = obj.relweights(not(propsrcrelmask)) .* ...
            ((1-obj.propagatestrength) * 1 + ...
            obj.propagatestrength * 0);
    end
    
    mask = obj.relweights(obj.activerelinds) == 0;
    obj.activerelinds(mask) = [];
    activerelparent(mask) = [];
    activerelchilds(mask) = [];
    activerelunvisited(mask) = [];
    

    % compute relationships directly from childs if parent node is in
    % the set of free nodes, child nodes are not in the set of free
    % nodes and parent node is not child of any other active
    % relationship
    if not(isempty(obj.activerelinds))
    
        directcomputemask = ...
            freenodemask(activerelparent) & ...
            cellfun(@(x) not(any(freenodemask(x))),activerelchilds)' & ...
            not(ismember(activerelparent,unique([activerelchilds{:}])))';
        
        if any(directcomputemask)
            
            directcomputeparent = activerelparent(directcomputemask);

            for i=1:numel(directcomputeparent)
                dcparentstateinds = obj.node2stateinds{directcomputeparent(i)};
                [state(dcparentstateinds),statevar(dcparentstateinds)] = obj.computerel(state,directcomputeparent(i));
            end
            
            obj.activerelinds(directcomputemask) = [];
            activerelparent(directcomputemask) = [];
            activerelchilds(directcomputemask) = [];
            activerelunvisited(directcomputemask) = [];
        end
        
        objectiveval = 0;
    end
    
    % optimize relationship values in all other cases
    if not(isempty(obj.activerelinds))
        startsubstate = obj.fullstate(obj.substateinds);

        warning('off','optim:fminunc:SwitchingMethod');

        if strcmp(obj.visualize,'all')
            ofun = @(state,optimValues,solverflag) obj.outfun(state,optimValues,solverflag);
        else
            ofun = [];
        end

        % optimize
        [bestsubstate,objectiveval,exitflag,~,~,~] = fminunc(...
            @(x) obj.evalsubstate_nograd(x),startsubstate,...
            optimoptions(@fminunc,...
            'MaxFunEvals',100000,... % matlab default is 100*number of variables
            'TolFun',1e-7,... % matlab default is 1e-6
            'GradObj','off',...
            'Diagnostics','off',...
            'Display','off',... %'final',...
            'FunValCheck','on',...
            'OutputFcn',ofun,...
            'DerivativeCheck','off'));

        warning('on','optim:fminunc:SwitchingMethod');
        
        if exitflag == -1
            obj.stop = true;
        end

        state(obj.substateinds) = bestsubstate;
        statevar = obj.fullstatevar;
    end
    
    obj.relweights = originalrelweights;
end

% function mask = checkSynced(obj,state,statevar,nodeinds)
%     [relval,relvar,residual,normresidual,wnormresidual] = computeresidual(obj,state,statevar,nodeinds);
%     mask = wnormresidual < obj.synctolerance;
% end

function syncednodemask = checkSynced(obj,state,statevar,nodeinds)
    
    syncednodemask = false(1,numel(nodeinds));
    for i=1:numel(nodeinds)
        nind = nodeinds(i);

        if not(isnan(obj.pgraph.node2relinds(nind)))
            % relationship node
            [~,~,~,~,childresidual] = obj.computeresidual(state,statevar,nind);
        else
            % other node (element node probably)
            childresidual = 0;
        end

        if childresidual < obj.synctolerance;
            childsyncednodemask = true;
        else
            childsyncednodemask = false;
        end

        % check the residual of the parents with the new (possibly
        % slightly different) value of the node
        parentresidual = zeros(1,numel(obj.parentnodeinds{nind}));
        for j=1:numel(obj.parentnodeinds{nind})
            pind = obj.parentnodeinds{nind}(j);
            [~,~,~,~,parentresidual(j)] = obj.computeresidual(state,statevar,pind);
        end

        if all(parentresidual < obj.synctolerance)
            parentsyncednodemask = true;
        else
            parentsyncednodemask = false;
        end

        syncednodemask(i) = childsyncednodemask && parentsyncednodemask;
    end
    
    disp(['childresidual: ',num2str(childresidual),' parentresidual: ',num2str(parentresidual),' syncterolance: ',num2str(obj.synctolerance)]);
end

function [fixedrelparent,fixedrelchilds,fixedrelsyncedmask] = checkFixedrelsynced(obj,state,statevar,nodeinds,fixedflag)
    relparent = [nodeinds,obj.parentnodeinds{nodeinds}];
    relchilds = obj.childnodeinds(relparent);

    leafmask = cellfun(@(x) isempty(x), relchilds);

    relparent(leafmask) = [];
    relchilds(leafmask) = [];

    fixedrelmask = find(fixedflag(relparent) >= 1 & cellfun(@(x) all(fixedflag(x) >= 1),relchilds)');
    fixedrelparent = relparent(fixedrelmask);
    fixedrelchilds = relchilds(fixedrelmask);

    fixedrelsyncedmask = false(1,numel(fixedrelparent));
    for j=1:numel(fixedrelparent)
        [~,~,~,~,fixedrelresidual] = obj.computeresidual(state,statevar,fixedrelparent(j));

        if fixedrelresidual < obj.synctolerance * 10
            fixedrelsyncedmask(j) = true;
        end
    end 
end

function [selmposes,srelvals,scost] = solve(obj,newelmposes,newrelvals)
    
    
%     % temp: clone the pgraph so it does not get deleted after the solver
%     % has finished
%     obj.pgraph = obj.pgraph.clone;
%     for i=1:numel(obj.pgraph.elms)
%         obj.pgraph.elms(i) = obj.pgraph.elms(i).clone;
%     end
%     obj.pgraph.elms.setScenegroup(SceneGroup.empty);
    
    % above this threshold for the objective function (which inlcudes the
    % variance), a node is considered out of sync
    
%     choicepath = 
    
    obj.clear;

%     if not(obj.pgraph.balancedchildlevels)
%         error('Only graph with balanced child levels are supported for now.');
%         % otherwise getting all queued nodes of a single level does not work
%     end

    if strcmp(obj.groupmode,'implicit')
        obj.pgraph.createImplicitRelgroups;
    end
    
    obj.prepare(newelmposes);
    
    elmoffset = obj.pgraph.elmoffset;
    
    nodelevel = obj.pgraph.level;

    
    state = obj.patternstate;
    statevar = obj.patternstatevar;
    
    visitedmask = false(1,numel(obj.pgraph.nodes));
    groupvisitedmask = false(1,size(obj.pgraph.groups,2));
    fixedflag = ones(1,numel(obj.pgraph.nodes)).*-1;
    fixedflag(elmoffset+1:elmoffset+numel(obj.pgraph.elms)) = 0; % elements are never fixed
    syncqueue = zeros(1,0);
    
    relvalchangednodeind = find(((newrelvals - [obj.pgraph.rels.value]).^2 ./ [obj.pgraph.rels.variance]) .* obj.relweights > obj.synctolerance);
    relvalchangednodeind = obj.pgraph.rel2nodeinds(relvalchangednodeind); %#ok<FNDSB>
    relvalchangednodemask = false(numel(obj.pgraph.nodes),1);
    relvalchangednodemask(relvalchangednodeind) = true;
    
    
    userchangedelmind = find(any(obj.oldelmposes ~= obj.newelmposes,1));
    userchangednodeind = userchangedelmind + elmoffset;
    
    if numel(userchangedelmind) > 1
        error('Only one changed element is supported for now.');
    elseif numel(userchangedelmind) == 1
        state(obj.node2stateinds{userchangednodeind}) = obj.newelmposes(:,userchangedelmind);
        state(obj.nodefixorig2stateinds(userchangednodeind)) = 0;
        state(obj.nodefixnew2stateinds(userchangednodeind)) = 1;

        visitedmask(userchangedelmind+elmoffset) = true;
        fixedflag(userchangedelmind+elmoffset) = 1;
        syncqueue = zeros(1,0);
        syncqueue = [syncqueue,obj.parentnodeinds{userchangedelmind+elmoffset}];
        syncqueue = [syncqueue,obj.siblingnodeinds{userchangedelmind+elmoffset}];
        syncqueue = [syncqueue,obj.childnodeinds{userchangedelmind+elmoffset}]; % just in case
    end
    
    obj.choicestate(:,end+1) = state;
    obj.choicestatevar(:,end+1) = statevar;
    obj.choicenodevisited(:,end+1) = visitedmask;
    obj.choicegroupvisited(:,end+1) = groupvisitedmask;
    obj.choicenodefixed(:,end+1) = fixedflag;
    obj.choicenodesyncqueue{:,end+1} = syncqueue;
    obj.choiceclosed(:,end+1) = false;
    obj.choiceregcost(:,end+1) = 0;
    obj.choicesynccost(:,end+1) = obj.computeSynccost(state,statevar);
    obj.choicesynccostdelta(:,end+1) = nan;
    obj.choiceiter = 1;
    obj.choicetree(1,1) = 1;
    

    bestsolutionregcost = inf;
    
    choiceind = 1; % temp
%                     [1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,9,9]
    nodechoicestemp = [0,0,0,0,0,1,0,1,2,1,0,1,0,1,1,1,1,1,1]; % fix centroiddirs as well as higher-level relationships to old value and the changed relationships to new value
% %     nodechoicestemp = [0,0,0,0,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1]; % fix centroiddirs as well as higher-level relationships
%     nodechoicestemp = [0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1]; % fix all relationships to old value
    
%     nodechoicestemp = [0,0,0,0,0,1,1,1,2,1,1,1,1,1,1,1,1,1,1]; % temp (inconsistent)

%     activebranchcount = 1;
    
    while(true)
        if obj.stop
            break;
        end
        
        choiceopeninds = find(not(obj.choiceclosed));
        
        % prefer paths with many unvisited nodes?
        % comment out two lines below to use the choices in nodechoicestemp
        % pick best open choice path
        [~,ind] = min(obj.choiceregcost(choiceopeninds));
        choiceind = choiceopeninds(ind);
        
        disp('--------------');
        disp(['choosing choice ',num2str(choiceind),' iteration ',num2str(obj.choiceiter(choiceind)),' (sync. cost: ',num2str(obj.choicesynccost(choiceind)),' - reg. cost ',num2str(obj.choiceregcost(choiceind)),' - best solution: ',num2str(bestsolutionregcost),')']);
        disp(['active paths: ',num2str(numel(choiceopeninds))]);
        
        if isempty(choiceind) || obj.choiceregcost(choiceind) > bestsolutionregcost * 3
            % no more choices to be made or best available choice is too
            % expensive
            break;
        end
        
        % get the state of the choice node
        state = obj.choicestate(:,choiceind);
        statevar = obj.choicestatevar(:,choiceind);
        visitedmask = obj.choicenodevisited(:,choiceind);
        groupvisitedmask = obj.choicegroupvisited(:,choiceind);
        fixedflag = obj.choicenodefixed(:,choiceind);
        syncqueue = obj.choicenodesyncqueue{:,choiceind};
        
        
%         synccost = obj.computeSynccost(state,statevar);
%         disp(['cost: ',num2str(synccost)]);
        

        % should never happen since fixedflag should never be the same for
        % different choices (it is a choice tree)
%         % check for duplicate states
%         allothermask = true(1,size(obj.choicestate,2));
%         allothermask(choiceind) = false;
%         mask = ...
%             all(bsxfun(@eq,visitedmask,obj.choicenodevisited(:,allothermask)),1) & ...
%             all(bsxfun(@eq,fixedflag,obj.choicenodefixed(:,allothermask)),1) & ...    
%             all(bsxfun(@eq,groupvisitedmask,obj.choicegroupvisited(:,allothermask)),1) & ...
%             all(bsxfun(@rdivide,bsxfun(@minus,state,obj.choicestate(:,allothermask)).^2,statevar) < obj.synctolerance,1);
%         %             all(bsxfun(@eq,syncmask,obj.choicenodesync(:,allothermask)),1) & ...
%         if any(mask)
% %             obj.choicestate(:,choiceind) = [];
% %             obj.choicestatevar(:,choiceind) = [];
% %             obj.choicenodevisited(:,choiceind) = [];
% %             obj.choicegroupvisited(:,choiceind) = [];
% %             obj.choicenodesync(:,choiceind) = [];
% %             obj.choicenodedecision(:,choiceind) = [];
% %             obj.choiceregcost(:,choiceind) = [];
% %             obj.choiceclosed(:,choiceind) = [];
% %             obj.choicetree(choiceind,:) = [];
% %             obj.choicetree(:,choiceind) = [];
%             
%             obj.choiceclosed(:,choiceind) = true; % temp
%             obj.choiceregcost(:,choiceind) = inf; % temp
%             continue;
%         end
        
%         if obj.choiceiter > obj.maxiter
%             
%             disp(['choice ',num2str(choiceind),' reached maximum iteration count (sync. cost: ',num2str(obj.choicesynccost(choiceind)),' - reg. cost ',num2str(obj.choiceregcost(choiceind)),' - best solution: ',num2str(bestsolutionregcost),')']);
%             
%             obj.showState(zeros(0,1),zeros(0,1),state,statevar,zeros(0,1),visitedmask,fixedflag,syncqueue);
%             obj.choiceclosed(choiceind) = true;
%             
%             choiceind = []; % temp
%             
%             continue;
%         end
        
        meansyncdelta = obj.choicesynccostdelta(:,choiceind);        
        nanmask = isnan(meansyncdelta);
        meansyncdelta(nanmask) = 0;
        deltaw = linspace(0,1,size(obj.choicesynccostdelta,1));
        meansyncdelta = (deltaw*meansyncdelta) / sum(deltaw);
        
        disp(['meansyncdelta: ',num2str(meansyncdelta)]);

        % check for choice path stopping conditions
        if isempty(syncqueue) || ...
           obj.choicesynccost(choiceind) < obj.synctolerance || ...
           obj.choiceiter(choiceind) > obj.maxiter || ...
           (obj.choiceiter(choiceind) >= size(obj.choicesynccostdelta,1) && -meansyncdelta < obj.minsyncdelta)
%             obj.choicesynccostdelta(1,choiceind) < obj.minsyncdelta || ...
%            obj.choiceiter(choiceind) - obj.choicesynccostdelta(2,choiceind) > obj.maxzerodeltaiter
            
            % show current state
            if any(strcmp(obj.visualize,{'all','solutions','steps'}))
                obj.showState(zeros(0,1),zeros(0,1),state,statevar,zeros(0,1),visitedmask,fixedflag,syncqueue);
            end
            
            obj.choiceregcost(choiceind) = obj.computeRegularizer(...
                state,statevar,visitedmask,fixedflag,syncqueue,false);%true);
            obj.choicesynccost(choiceind) = obj.computeSynccost(state,statevar);
            
            if obj.choiceregcost(choiceind) < bestsolutionregcost
                bestsolutionregcost = obj.choiceregcost(choiceind);
            end
            
            obj.choiceclosed(choiceind) = true;
            obj.choiceregcost(choiceind) = obj.computeRegularizer(...
                state,statevar,visitedmask,fixedflag,syncqueue,false);
            
            disp(['choice ',num2str(choiceind),' all done (sync. cost: ',num2str(obj.choicesynccost(choiceind)),' - reg. cost ',num2str(obj.choiceregcost(choiceind)),' - best solution: ',num2str(bestsolutionregcost),')']);
            
            choiceind = []; % temp
            
%             activebranchcount = activebranchcount - 1;
            
            continue;
        end
        
%         % from the queue, take all decided nodes, all synced nodes
%         % and a single undecided unsynced node
%         nodeinds = syncqueue(fixedflag(syncqueue) >= 0);
%         queueundecidedmask = fixedflag(syncqueue) == -1;
%         if any(queueundecidedmask)
%             queueunvisitedmask = not(visitedmask(syncqueue));
%             if any(queueunvisitedmask)
%                 undecidednodeinds = syncqueue(queueunvisitedmask & queueundecidedmask);
%             else
%                 undecidednodeinds  = syncqueue(queueundecidedmask);
%             end
%             undecidednodelevel = nodelevel(undecidednodeinds);
%             undecidednodeinds = undecidednodeinds(undecidednodelevel == max(undecidednodelevel));
%             undecidednodeinds = undecidednodeinds(1);
%             nodeinds = [nodeinds,undecidednodeinds]; %#ok<AGROW>
%         end
%         syncqueue(ismember(syncqueue,nodeinds)) = [];
        
        
    
        % take unvisited nodes first
        queueinds = find(not(visitedmask(syncqueue)));
        if isempty(queueinds)
            nodeinds = syncqueue(1);
            syncqueue(1) = [];
        else
            % of the unvisited nodes the highest level
            queuenodelevel = nodelevel(syncqueue);
            queueinds = queueinds(queuenodelevel(queueinds) == max(queuenodelevel(queueinds)));
            nodeinds = syncqueue(queueinds(1));
            syncqueue(queueinds(1)) = [];
        end
        
        
        
        % just use the queue
%         nodeinds = syncqueue(1);
%         syncqueue(1) = [];

        disp(['syncqueue: ',num2str(syncqueue)]);
        
%         visitedmask(nodeinds) = true;
        
        disp(['nodeind: ',num2str(nodeinds)]);
        
        % show current state
        if any(strcmp(obj.visualize,{'all','steps'}))
            obj.showState(zeros(0,1),zeros(0,1),state,statevar,nodeinds,visitedmask,fixedflag,syncqueue);
        end

        % -----------------------------------------------------------------
        % remove nodes that are already (almost) synced
        syncednodemask = obj.checkSynced(state,statevar,nodeinds);

        if any(syncednodemask)
            oldnodeinds = nodeinds; % temp
            
            visitedmask(nodeinds(syncednodemask)) = true;
    
            nodeinds(syncednodemask) = [];

            if isempty(nodeinds)
                obj.choicestate(:,choiceind) = state;
                obj.choicestatevar(:,choiceind) = statevar;
                obj.choicenodevisited(:,choiceind) = visitedmask;
                obj.choicegroupvisited(:,choiceind) = groupvisitedmask;
                obj.choicenodefixed(:,choiceind) = fixedflag;
                obj.choicenodesyncqueue{:,choiceind} = syncqueue;

                obj.choiceregcost(choiceind) = obj.computeRegularizer(...
                    state,statevar,visitedmask,fixedflag,syncqueue,false);%true);
                newsynccost = obj.computeSynccost(state,statevar);
                obj.choicesynccostdelta(:,choiceind) = [obj.choicesynccostdelta(2:end,choiceind);newsynccost - obj.choicesynccost(end,choiceind)];
                obj.choicesynccost(choiceind) = newsynccost;
                obj.choiceiter(choiceind) = obj.choiceiter(choiceind) + 1;

                disp(['choice ',num2str(choiceind),' continues: ',num2str(oldnodeinds),' already synced (sync. cost: ',num2str(obj.choicesynccost(choiceind)),' - reg. cost ',num2str(obj.choiceregcost(choiceind)),')']);

                continue;
            end
        end
        
        % push childs of (unsynced) fixed nodes to the queue
        fixedunsyncedmask = fixedflag(nodeinds) == 1;
        if any(fixedunsyncedmask)
            oldnodeinds = nodeinds; % temp
            
%             visitedmask(nodeinds(fixedunsyncedmask)) = true;

            newqueuednodeinds = zeros(1,0);
            newqueuednodeinds = [newqueuednodeinds,obj.childnodeinds{nodeinds(fixedunsyncedmask)}]; %#ok<AGROW>
            newqueuednodeinds = [newqueuednodeinds,obj.siblingnodeinds{nodeinds(fixedunsyncedmask)}]; %#ok<AGROW>
            syncqueue = unique([syncqueue,newqueuednodeinds],'stable');
            
            visitedmask(nodeinds(fixedunsyncedmask)) = true;
            
            nodeinds(fixedunsyncedmask) = [];

            if isempty(nodeinds)
                obj.choicestate(:,choiceind) = state;
                obj.choicestatevar(:,choiceind) = statevar;
                obj.choicenodevisited(:,choiceind) = visitedmask;
                obj.choicegroupvisited(:,choiceind) = groupvisitedmask;
                obj.choicenodefixed(:,choiceind) = fixedflag;
                obj.choicenodesyncqueue{:,choiceind} = syncqueue;

                obj.choiceregcost(choiceind) = obj.computeRegularizer(...
                    state,statevar,visitedmask,fixedflag,syncqueue,false);%true);
                newsynccost = obj.computeSynccost(state,statevar);
                obj.choicesynccostdelta(:,choiceind) = [obj.choicesynccostdelta(2:end,choiceind);newsynccost - obj.choicesynccost(end,choiceind)];
                obj.choicesynccost(choiceind) = newsynccost;
                obj.choiceiter(choiceind) = obj.choiceiter(choiceind) + 1;

                disp(['choice ',num2str(choiceind),' continues: ',num2str(oldnodeinds),' childs of unsynced nodes pushed to queue (sync. cost: ',num2str(obj.choicesynccost(choiceind)),' - reg. cost ',num2str(obj.choiceregcost(choiceind)),')']);

                continue;
            end
        end
        
        % -----------------------------------------------------------------
        % optimize all (unsynced) free nodes
        % might also use every variable separately and subtract the
        % residual? (like gauss-seidel iteration in mixed integer quadrangulation paper)
        optimizenodemask = fixedflag(nodeinds) == 0;
        % also the fixed ones to push the childs of fixed unsynced nodes to
        % the queue
        if any(optimizenodemask)
            oldnodeinds = nodeinds; % temp
            
%             visitedmask(nodeinds(optimizenodemask)) = true;
            
            [state,statevar,~] = obj.optimizenodes(...
                state,statevar,nodeinds(optimizenodemask),fixedflag,visitedmask);
            
            visitedmask(nodeinds(optimizenodemask)) = true;

            % add parents, childs and siblings to the sync queue
            % (all nodes that might no longer be at a minimum in their subspace,
            % it only makes sense to do coordinate descent on these)
            % (other option: only add nodes that directly depend on the
            % optimized nodes, but would never propagate from paent to child)
            newqueuednodeinds = zeros(1,0);
            newqueuednodeinds = [newqueuednodeinds,obj.parentnodeinds{nodeinds(optimizenodemask)}]; %#ok<AGROW>
            newqueuednodeinds = [newqueuednodeinds,obj.siblingnodeinds{nodeinds(optimizenodemask)}]; %#ok<AGROW>
            newqueuednodeinds = [newqueuednodeinds,obj.childnodeinds{nodeinds(optimizenodemask)}]; %#ok<AGROW>
            newqueuednodeinds = [newqueuednodeinds,nodeinds(optimizenodemask)]; %#ok<AGROW>
            newqueuednodeinds(fixedflag(newqueuednodeinds) == 1) = [];
            syncqueue = unique([syncqueue,newqueuednodeinds],'stable');
            
            % remove visited nodes that have been optimized
            nodeinds(optimizenodemask) = [];

            if isempty(nodeinds)
                
                obj.choicestate(:,choiceind) = state;
                obj.choicestatevar(:,choiceind) = statevar;
                obj.choicenodevisited(:,choiceind) = visitedmask;
                obj.choicegroupvisited(:,choiceind) = groupvisitedmask;
                obj.choicenodefixed(:,choiceind) = fixedflag;
                obj.choicenodesyncqueue{:,choiceind} = syncqueue;

                obj.choiceregcost(choiceind) = obj.computeRegularizer(...
                    state,statevar,visitedmask,fixedflag,syncqueue,false);%true);
                newsynccost = obj.computeSynccost(state,statevar);
                obj.choicesynccostdelta(:,choiceind) = [obj.choicesynccostdelta(2:end,choiceind);newsynccost - obj.choicesynccost(end,choiceind)];
                obj.choicesynccost(choiceind) = newsynccost;
                obj.choiceiter(choiceind) = obj.choiceiter(choiceind) + 1;

                disp(['choice ',num2str(choiceind),' continues: ',num2str(oldnodeinds),' nodes optimized (sync. cost: ',num2str(obj.choicesynccost(choiceind)),' - reg. cost ',num2str(obj.choiceregcost(choiceind)),')']);

                continue;
            end
        end
        
        
        % -----------------------------------------------------------------
        % end the current choice path
        obj.choicestate(:,choiceind) = state;
        obj.choicestatevar(:,choiceind) = statevar;
        obj.choicenodevisited(:,choiceind) = visitedmask;
        obj.choicegroupvisited(:,choiceind) = groupvisitedmask;
        obj.choicenodefixed(:,choiceind) = fixedflag;
       
        obj.choiceregcost(choiceind) = obj.computeRegularizer(...
            state,statevar,visitedmask,fixedflag,syncqueue,false);%true);
        newsynccost = obj.computeSynccost(state,statevar);
        obj.choicesynccostdelta(:,choiceind) = [obj.choicesynccostdelta(2:end,choiceind);newsynccost - obj.choicesynccost(end,choiceind)];
        obj.choicesynccost(choiceind) = newsynccost;
        obj.choiceclosed(choiceind) = true;
        
        disp(['choice ',num2str(choiceind),' ends (sync. cost: ',num2str(obj.choicesynccost(choiceind)),' - reg. cost ',num2str(obj.choiceregcost(choiceind)),') and branches into:']);
        
%         activebranchcount = activebranchcount - 1;
        
        
        % -----------------------------------------------------------------
        % for all (unsynced) undecided nodes, add all combinations of
        % fixed/free to the queue
        % (branching two new paths for each node separately
        % does not work with multiple nodes, since paths are created for
        % descisions on different nodes, which means that two paths can get
        % the same combination of fixed/free flags)

%         ncombs = 2^numel(nodeinds);
        ncombs = 3^numel(nodeinds);
%         ncombs = 3^nodeinds(relvalchangednodemask(nodeinds)) * 2^nodeinds(not(relvalchangednodemask(nodeinds)));
        
        if ncombs > 10000
            error('Too many combinations, use fewer nodes at a time.');
        end
        
        for i=1:ncombs
            
            combstate = state;
            combstatevar = statevar;
            combvisitedmask = visitedmask;
            combgroupvisitedmask = groupvisitedmask;
            combfixedflag = fixedflag;
            combsyncqueue = syncqueue;
            
%             combfixedflag(nodeinds) = de2bi(i-1,numel(nodeinds),'left-msb');
            combfixedflag(nodeinds) = de2bi(i-1,numel(nodeinds),3,'left-msb');
            
            % ignore combinations where an unchanged relationship is
            % assigned the 'fixed to new value'-flag
            if any(combfixedflag==2 & not(relvalchangednodemask))
                continue;
            end
            
            allfixedrelsynced = true;
            
            freemask = combfixedflag(nodeinds)==0;
            fixoldmask = combfixedflag(nodeinds)==1;
            fixnewmask = combfixedflag(nodeinds)==2;
            
            if any(fixnewmask)
               	combstate(cat(1,obj.node2stateinds{nodeinds(fixnewmask)})) = newrelvals(obj.pgraph.node2relinds(nodeinds(fixnewmask)));
                
                % add all changed nodes, their parents, siblings and childs to the queue
                newqueuednodeinds = zeros(1,0);
                newqueuednodeinds = [newqueuednodeinds,obj.parentnodeinds{nodeinds(fixnewmask)}]; %#ok<AGROW>
                newqueuednodeinds = [newqueuednodeinds,obj.siblingnodeinds{nodeinds(fixnewmask)}]; %#ok<AGROW>
                newqueuednodeinds = [newqueuednodeinds,obj.childnodeinds{nodeinds(fixnewmask)}]; %#ok<AGROW>
                newqueuednodeinds = [newqueuednodeinds,nodeinds(fixnewmask)]; %#ok<AGROW>
                newqueuednodeinds(combfixedflag(newqueuednodeinds) == 1) = [];
                combsyncqueue = unique([combsyncqueue,newqueuednodeinds],'stable');
                
                % connsistency check: check if all relationships connected
                % to the node have at least one free or undecided node
                % left, or they relationship is fulfilled (within some
                % generous tolerance), otherwise the we can abort the
                % current path
                
                [~,~,fixedrelsyncedmask] = obj.checkFixedrelsynced(combstate,combstatevar,nodeinds(fixnewmask),combfixedflag);
                if not(all(fixedrelsyncedmask))
                    allfixedrelsynced = false;
                end
            end
            
            if any(freemask)
%                 % current nodes should be treated as unvisited in the
%                 % optimization, then change is propagated to the child nodes
%                 [combstate,combstatevar,~] = obj.optimizenodes(...
%                     combstate,combstatevar,nodeinds(not(fixmask)),combfixedflag,visitedmask);
                [combstate,combstatevar,~] = obj.optimizenodes(...
                    combstate,combstatevar,nodeinds(freemask),combfixedflag,combvisitedmask);

                % add all cahnged nodes, their parents, siblings and childs to the queue
                newqueuednodeinds = zeros(1,0);
                newqueuednodeinds = [newqueuednodeinds,obj.parentnodeinds{nodeinds(freemask)}]; %#ok<AGROW>
                newqueuednodeinds = [newqueuednodeinds,obj.siblingnodeinds{nodeinds(freemask)}]; %#ok<AGROW>
                newqueuednodeinds = [newqueuednodeinds,obj.childnodeinds{nodeinds(freemask)}]; %#ok<AGROW>
                newqueuednodeinds = [newqueuednodeinds,nodeinds(freemask)]; %#ok<AGROW>
                newqueuednodeinds(combfixedflag(newqueuednodeinds) == 1) = [];
                combsyncqueue = unique([combsyncqueue,newqueuednodeinds],'stable');
            end
            
            if any(fixoldmask)
                % do nothing (state did nothing change)
                % add all fixed nodes and their siblings and childs to the queue
%                 newqueuednodeinds = zeros(1,0);
%                 newqueuednodeinds = [newqueuednodeinds,obj.childnodeinds{nodeinds(fixoldmask)}]; %#ok<AGROW>
%                 newqueuednodeinds = [newqueuednodeinds,obj.siblingnodeinds{nodeinds(fixoldmask)}]; %#ok<AGROW>
%                 combsyncqueue = unique([combsyncqueue,newqueuednodeinds],'stable');

                % connsistency check: check if all relationships connected
                % to the node have at least one free or undecided node
                % left, or they relationship is fulfilled (within some
                % generous tolerance), otherwise the we can abort the
                % current path
                
                [~,~,fixedrelsyncedmask] = obj.checkFixedrelsynced(combstate,combstatevar,nodeinds(fixoldmask),combfixedflag);
                if not(all(fixedrelsyncedmask))
                    allfixedrelsynced = false;
                end
                
            end
            
            combvisitedmask(nodeinds) = true;
            
            % add new choice path
            obj.choicestate(:,end+1) = combstate;
            obj.choicestatevar(:,end+1) = combstatevar;
            obj.choicenodevisited(:,end+1) = combvisitedmask;
            obj.choicegroupvisited(:,end+1) = combgroupvisitedmask;
            obj.choicenodefixed(:,end+1) = combfixedflag;
            obj.choicenodesyncqueue{:,end+1} = combsyncqueue;
            obj.choicetree(end+1,:) = 0;
            obj.choicetree(:,end+1) = 0;
            obj.choicetree(end,choiceind) = 1;
            obj.choiceclosed(end+1) = false;
            obj.choiceregcost(end+1) = obj.computeRegularizer(...
                combstate,combstatevar,combvisitedmask,combfixedflag,combsyncqueue,false);%true);
            newsynccost = obj.computeSynccost(combstate,combstatevar);
            obj.choicesynccostdelta(:,end+1) = [obj.choicesynccostdelta(2:end,choiceind);newsynccost - obj.choicesynccost(end,choiceind)];
            obj.choicesynccost(end+1) = newsynccost;
            obj.choiceiter(end+1) = obj.choiceiter(choiceind) + 1;
            
            if not(allfixedrelsynced)
                obj.choicesynccost(end) = inf;
                obj.choiceclosed(end) = true;
            end            
            
            if all(nodechoicestemp(nodeinds)' == combfixedflag(nodeinds))
                choiceind = size(obj.choicestate,2);
            end
            
%             activebranchcount = activebranchcount + 1;
            
            disp(['---> choice ',num2str(size(obj.choicestate,2)),' starts: ',num2str(nodeinds),' - fixed nodes: ',num2str(obj.choicenodefixed(:,end)'),' (sync. cost: ',num2str(obj.choicesynccost(choiceind)),' - reg. cost ',num2str(obj.choiceregcost(end)),')']);
        end

    end
    
    
    % non-leaf nodes are only partial solutions
    obj.solutionchoiceinds = find(sum(abs(obj.choicetree),1)==0 & obj.choiceclosed & not(isinf(obj.choiceregcost)));
    obj.solutioncost = obj.choiceregcost(obj.solutionchoiceinds);
    obj.solutionstate = obj.choicestate(:,obj.solutionchoiceinds);
    obj.solutionstatevar = obj.choicestatevar(:,obj.solutionchoiceinds);
    
%     elmoffset = obj.pgraph.elmoffset;
%     elmstateinds = cat(1,obj.node2stateinds{elmoffset+1 : elmoffset+numel(obj.pgraph.elms)});
%     relstateinds = cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds(1:numel(obj.pgraph.rels))});
%     
%     obj.solutionelmposes = cell(1,size(obj.solutionstate,2));
%     obj.solutionrelvals = cell(1,size(obj.solutionstate,2));
%     for i=1:size(obj.solutionstate,2)
%         obj.solutionelmposes{i} = reshape(obj.solutionstate(elmstateinds,i),obj.elmposedim,numel(obj.pgraph.elms));
%         obj.solutionrelvals{i} = obj.solutionstate(relstateinds,i);
%     end

    % get element poses and relationship values from states
    obj.solutionelmposes = cell(1,size(obj.solutionstate,2));
    obj.solutionrelvals = cell(1,size(obj.solutionstate,2));
    for i=1:size(obj.solutionstate,2)
        eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        eposes(1:obj.elmposedim,:) = reshape(obj.solutionstate(obj.elmposestateinds(:),i),obj.elmposedim,numel(obj.pgraph.elms));
        eposes = PatternSolver.sim2affinepose(eposes);
        obj.solutionelmposes{i} = eposes;
        obj.solutionrelvals{i} = obj.solutionstate(obj.relstateinds,i)';
    end
    
    [~,perm] = sort(obj.solutioncost,'ascend');
    obj.solutionchoiceinds = obj.solutionchoiceinds(perm);
    obj.solutioncost = obj.solutioncost(perm);
    obj.solutionstate = obj.solutionstate(:,perm);
    obj.solutionstatevar = obj.solutionstatevar(:,perm);
    obj.solutionelmposes = obj.solutionelmposes(perm);
    obj.solutionrelvals = obj.solutionrelvals(perm);
    
    clusterthreshold = 0.4;
    keepinds = StandardPatternSolver.findUniqueStates(...
        obj.solutionstate,obj.solutionstatevar,clusterthreshold);
    
    obj.solutionchoiceinds = obj.solutionchoiceinds(keepinds);
    obj.solutioncost = obj.solutioncost(keepinds);
    obj.solutionstate = obj.solutionstate(:,keepinds);
    obj.solutionstatevar = obj.solutionstatevar(:,keepinds);
    obj.solutionelmposes = obj.solutionelmposes(keepinds);
    obj.solutionrelvals = obj.solutionrelvals(keepinds);
    
    
    selmposes = obj.solutionelmposes;
    srelvals = obj.solutionrelvals;
    scost = obj.solutioncost;
    
    if strcmp(obj.groupmode,'implicit')
        obj.pgraph.removeImplicitRelgroups;
    end
end

function cost = computeRegularizer(obj,state,statevar,visitedmask,fixedmask,syncqueue,onlyvisited)
    syncmask = true(1,numel(obj.pgraph.nodes));
    syncmask(syncqueue) = false;
    
    obj.fullstatevar = statevar;
    obj.currentvisitedmask = visitedmask;
    obj.currentsyncmask = syncmask;
    obj.currentfixedflag = fixedmask;
    
    cost = obj.evalregularization(state,onlyvisited);
end

function cost = computeSynccost(obj,state,statevar,substateinds,activerelinds)
    
    if nargin < 4
        substateinds = cat(1,obj.node2stateinds{:});
    end
    
    if nargin < 5
        % constraints that are dependent on the substate nodes are the
        % substate nodes themselves and their parents
        substatenodeinds = unique(obj.state2nodeinds(substateinds));
        substatenodeinds(isnan(substatenodeinds)) = [];
        activenodeinds = unique([substatenodeinds,obj.parentnodeinds{substatenodeinds}]);
        activerelinds = obj.pgraph.node2relinds(activenodeinds);
        activerelinds(isnan(activerelinds)) = [];
    end
    
    obj.fullstate = state;
    obj.fullstatevar = statevar;
    obj.substateinds = substateinds;
    obj.activerelinds = activerelinds;
    
    cost = obj.evalsubstate_nograd(state(obj.substateinds));
end

function duplicatechoiceinds = findDuplicateChoices(obj,state,visitedmask,changedmask,queuednodeops)
    duplicatechoiceinds = find(...
        all(bsxfun(@eq,obj.choicestate,state),1) & ...
        all(bsxfun(@eq,obj.choicenodevisited,visitedmask),1) & ...    
        all(bsxfun(@eq,obj.choicechangednodesmask,changedmask),1) & ...
        cellfun(@(x) numel(x) == numel(queuednodeops) && all(x == queuednodeops),obj.choicequeuednodeops));
end

function setVisfunction(obj,func)
    obj.visfunc = func;
end

function showState(obj,substate,substateinds,state,statevar,nodeinds,visitedmask,fixedmask,syncqueue)
    syncmask = true(1,numel(obj.pgraph.nodes));
    syncmask(syncqueue) = false;
    
    obj.fullstate = state;
    obj.fullstatevar = statevar;
    obj.substateinds = substateinds;
    obj.currentnodeinds = nodeinds;
    obj.currentvisitedmask = visitedmask;
    obj.currentsyncmask = syncmask;
    obj.currentfixedflag = fixedmask;
    
    obj.stop = obj.outfun(substate,[],[]); 
end

function stop = outfun(obj,substate,optimValues,optimizerstateflag) %#ok<INUSD>
%     stop = false;
%     return; % temp
    
    state = obj.fullstate;
    state(obj.substateinds) = substate;
    
%     if not(isempty(optimValues))
%         disp(['*** iteration ',num2str(optimValues.iteration),':']);
%         disp(['    objective function value: ',num2str(optimValues.fval)]);
%     end
    
%     disp('    gradient:');
%     disp(optimValues.gradient);
    
    if not(isempty(obj.visfunc))
        eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        
%         elmoffset = obj.pgraph.elmoffset;
%         elmstateinds = cat(1,obj.node2stateinds{elmoffset+1 : elmoffset+numel(obj.pgraph.elms)});
%         eposes(1:obj.elmposedim,:) = reshape(state(elmstateinds),obj.elmposedim,[]);

        eposes(1:obj.elmposedim,:) = reshape(state(obj.elmposestateinds),size(obj.elmposestateinds));
        
        nongrpmask = true(1,numel(obj.currentvisitedmask));
        offset = obj.pgraph.grpnodeoffset;
        nongrpmask(offset+1:offset+numel(obj.pgraph.grpnodes)) = false;
        offset = obj.pgraph.grpreloffset;
        nongrpmask(offset+1:offset+numel(obj.pgraph.grprels)) = false;
        
        eposes = PatternSolver.sim2affinepose(eposes);
        
        stop = obj.visfunc(...
            obj,...
            eposes,...
            obj.currentvisitedmask(nongrpmask),...
            obj.currentsyncmask(nongrpmask),...
            obj.currentfixedflag(nongrpmask),...
            obj.currentnodeinds(nongrpmask(obj.currentnodeinds)));
        
    else
        stop = false;
    end
end

% % only use visited nodes for the regularization
% function val = eval_regularization_visited(obj,state)
% 
%     state = state';
%     val = 0;
%     
%     nrels = numel(obj.pgraph.rels);
%     
%     % relationships that are relevant
%     nongrpnodemask = false(size(obj.currentvisitedmask));
%     nongrpnodemask([...
%             obj.pgraph.elmoffset+1 : obj.pgraph.elmoffset+numel(obj.pgraph.elms),...        
%             obj.pgraph.elmreloffset+1 : obj.pgraph.elmreloffset+numel(obj.pgraph.elmrels),...
%             obj.pgraph.metareloffset+1 : obj.pgraph.metareloffset+numel(obj.pgraph.metarels)]) = true;
%     
%     % todo: also check that it is not in the sync queue?
%     noderelevantmask = obj.currentvisitedmask  & nongrpnodemask;
%     relrelevantmask = noderelevantmask(obj.pgraph.rel2nodeinds(1:nrels));
%     staterelevantmask = noderelevantmask(obj.state2nodeinds(1:numel(state)));
%     elmrelevantmask = noderelevantmask(obj.pgraph.elmoffset+1:obj.pgraph.elmoffset+numel(obj.pgraph.elms));
%     
%     origrvals = [obj.pgraph.rels.value];
%     origrvars = [obj.pgraph.rels.variance];
%     
%     rvals = nan(1,nrels);
%     rvars = nan(1,nrels);
%     for i=1:numel(obj.relfs)
%         
%         mask = relrelevantmask(obj.relfrelinds{i});
%         
%         if any(mask)
%             % evaluate all relationships functions of one type
%             [v,var] = obj.relfs{i}(...
%                 reshape(state(obj.relfinput1inds{i}(:,mask)),size(obj.relfinput1inds{i}(:,mask))),...
%                 reshape(state(obj.relfinput2inds{i}(:,mask)),size(obj.relfinput2inds{i}(:,mask))));
% 
%             residual = v-state(obj.relfoutputinds{i}(mask));
%             if obj.relfcircular(i)
%                 residual = smod(residual,-pi,pi);
%             end
% %             unsync = (residual.^2 ./ var) * ...
% %                 (obj.relweights(obj.relfrelinds{i}(mask))' .* ...
% %                 fixedstateweights(obj.relfoutputinds{i}(mask)));
%             unsync = (residual.^2 ./ var) * obj.relweights(obj.relfrelinds{i}(mask))';
%             val = val + unsync*1000; % weighted mahalanobis distance
%             
%             rvals(obj.relfrelinds{i}(:,mask)) = v;
%             rvars(obj.relfrelinds{i}(:,mask)) = var;
%         end
%     end
%     
% %     rvals = rvals(relfixedmask);
% %     rvars = rvars(relfixedmask);
% %     origrvals = origrvals(relfixedmask);
% %     origrvars = origrvars(relfixedmask);
%     
%     % total change in the relationship values
%     relchange = min(1,abs(rvals(relrelevantmask) - origrvals(relrelevantmask))./(sqrt(origrvars(relrelevantmask)).*0.5));
%     val = val + sum(relchange)^2 * 0.05;
%     
%     % how many elment groups changed (sum of average element changes in all element groups)
%     % only relevant elements are counted but divided by total number of group members (fixed and unfixed) 
%     if any(elmrelevantmask)
% %         elmposediff = obj.oldelmposes(:,elmfixedmask) - reshape(...
% %                 state(obj.elmposestateinds(:,elmfixedmask)),size(obj.elmposestateinds(:,elmfixedmask)));
%         elmposediff = obj.oldelmposes - reshape(...
%                 state(obj.elmposestateinds),size(obj.elmposestateinds));
%         for i=1:numel(obj.elmgrpelminds)
%             relevantgrpelminds = obj.elmgrpelminds{i}(elmrelevantmask(obj.elmgrpelminds{i}));
%             if not(isempty(relevantgrpelminds)) % && not(any(activemask(obj.elmgrpelminds{i})))
%                 val = val + sum(sum(elmposediff(:,relevantgrpelminds).^2,1) ./ obj.elmposevar) ./ numel(obj.elmgrpelminds{i});
%     %             val = val + mean(1 ./ (1 + exp(-sqrt(sum(elmposediff.^2,1) ./ obj.elmposevar) ./ 2))) * 100;
%             end
%         end
%     end
%     
%     % how many element groups were broken (residual of the group members
%     % to the group average)
%     if any(relrelevantmask)
%         for i=1:numel(obj.relgrprelinds)
% 
%             if numel(obj.relgrprelinds{i}) > 1 && all(relrelevantmask(obj.relgrprelinds{i}))
%                 grprelvals = state(cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds(obj.relgrprelinds{i})}));
%                 if obj.relgrpcircular(i)
%                     grpavg = circ_mean(grprelvals,[],2);
%                 else
%                     grpavg = mean(grprelvals);
%                 end
%                 residual = grprelvals - grpavg;
%                 if obj.relgrpcircular(i)
%                     residual = smod(residual,-pi,pi);
%                 end
%     %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*9999;
% %                 val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*1000;
%                 val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*1;
%     %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i})) * mean(1-changedrelmask(obj.relgrprelinds{i})) * 100;
%     %             val = val + sum(((residual).^2 ./ rvars(obj.relgrprelinds{i})) .* rellagrangemult_combined(obj.relgrprelinds{i})) * 100;
%             end
%         end
%     end
%     
% end

% todo: elements are never visited

function val = evalregularization(obj,state,onlyvisited)
    
    state = state';
    val = 0;
    
    nrels = numel(obj.pgraph.rels);
    nelms = numel(obj.pgraph.elms);
    
    if nargin < 3
        onlyvisited = false;
    end
    
    if strcmp(obj.groupmode,'implicit')
        % do not use the nodes and relationships of the implicit groups
        % groups are included in a separate term
        
        relmask = false(1,nrels);
        relmask(obj.pgraph.node2relinds([...
            obj.pgraph.elmreloffset+1 : obj.pgraph.elmreloffset+numel(obj.pgraph.elmrels),...
            obj.pgraph.metareloffset+1 : obj.pgraph.metareloffset+numel(obj.pgraph.metarels)])) = true;
        elmmask = true(1,nelms);
    else
        relmask = true(1,nrels);
        elmmask = true(1,nelms);
    end
    
    if onlyvisited
        % only consider visited relationships and elements
        relmask(not(obj.currentvisitedmask(obj.pgraph.rel2nodeinds(relmask)))) = false;
        elmmask(not(obj.currentvisitedmask(find(elmmask)+obj.pgraph.elmoffset))) = false;
    end

    rvals = nan(1,nrels);
    rvars = nan(1,nrels);
    
    for i=1:numel(obj.relfs)
        
        mask = relmask(obj.relfrelinds{i});
        
        if any(mask)
        
            % evaluate all relationships functions of one type
            [v,var] = obj.relfs{i}(...
                reshape(state(obj.relfinput1inds{i}(:,mask)),size(obj.relfinput1inds{i}(:,mask))),...
                reshape(state(obj.relfinput2inds{i}(:,mask)),size(obj.relfinput2inds{i}(:,mask))));

%             residual = v-state(obj.relfoutputinds{i}(mask));
%             if obj.relfcircular(i)
%                 residual = smod(residual,-pi,pi);
%             end
%             unsync = (residual.^2 ./ var) * obj.relweights(obj.relfrelinds{i}(mask))';
%             val = val + unsync * 1000; % weighted mahalanobis distance

            rvals(obj.relfrelinds{i}(mask)) = v;
            rvars(obj.relfrelinds{i}(mask)) = var;
        end
    end
    
    origrvals = [obj.pgraph.rels.value];
    origrvars = [obj.pgraph.rels.variance];
    
%     % how many relationships changed (change in relationships normalized by variance)
%     relchange = min(1,abs(rvals(relmask) - origrvals(relmask))./(sqrt(origrvars(relmask)).*0.5));
%     val = val + sum(relchange)^2 * 0.05;
    
%     synctolerance = 0.1;
    maxunsync = 5; % in multiples of the synctolerance
    
    residual(relmask) = rvals(relmask) - origrvals(relmask);
    residual(not(relmask)) = 0;
    for i=1:numel(obj.relgrprelinds)
        if not(isempty(obj.relgrprelinds{i})) % && all(relmask(obj.relgrprelinds{i}))
            maxrelchange = max(residual(obj.relgrprelinds{i}).^2 ./ origrvars(obj.relgrprelinds{i}) );
            val = val + min(maxunsync,(maxrelchange / obj.synctolerance)) * 10 ;
        end
    end
    
%         % sum of average element changes in all element groups (or how many
%         % elment groups changed)
%         elmposediff = obj.oldelmposes - reshape(...
%                 state(obj.elmposestateinds),size(obj.elmposestateinds));
%         for i=1:numel(obj.elmgrpelminds)
%             if not(isempty(obj.elmgrpelminds{i})) % && not(any(activemask(obj.elmgrpelminds{i})))
%                 val = val + mean(sum(elmposediff(:,obj.elmgrpelminds{i}).^2,1) ./ obj.elmposevar);
%             end
%         end
        
    residual = zeros(obj.elmposedim,nelms);
    residual(:,elmmask) = obj.oldelmposes(:,elmmask) - reshape(...
            state(obj.elmposestateinds(:,elmmask)),size(obj.elmposestateinds(:,elmmask)));
    residual(:,not(elmmask)) = 0;
    for i=1:numel(obj.elmgrpelminds)
        if not(isempty(obj.elmgrpelminds{i})) % && all(elmmask(obj.elmgrpelminds{i}))
            maxelmchange = max( sum(bsxfun(@rdivide,residual(:,obj.elmgrpelminds{i}).^2,obj.elmposevar),1) );
            val = val + min(maxunsync,(maxelmchange / obj.synctolerance)) * 50;
        end
    end
        
        
%     for i=1:numel(obj.relgrprelinds)
%         if numel(obj.relgrprelinds{i}) > 1 && all(relmask(obj.relgrprelinds{i}))
%             grpstateinds = cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds(obj.relgrprelinds{i})});
%             grprelvals = state(grpstateinds);
%             if obj.relgrpcircular(i)
%                 grpavg = circ_mean(grprelvals,[],2);
%             else
%                 grpavg = mean(grprelvals);
%             end
%             residual = grprelvals - grpavg;
%             if obj.relgrpcircular(i)
%                 residual = smod(residual,-pi,pi);
%             end
% 
% %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*9999;
% %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*1000;
%             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*10;
% %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i})) * mean(1-changedrelmask(obj.relgrprelinds{i})) * 100;
% %             val = val + sum(((residual).^2 ./ rvars(obj.relgrprelinds{i})) .* rellagrangemult_combined(obj.relgrprelinds{i})) * 100;
%         end
%     end

        for i=1:numel(obj.relgrprelinds)
            if numel(obj.relgrprelinds{i}) > 1 && all(relmask(obj.relgrprelinds{i}))
                grpstateinds = cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds(obj.relgrprelinds{i})});
                grprelvals = state(grpstateinds);
                grprelvar = obj.fullstatevar(grpstateinds)';
                if obj.relgrpcircular(i)
                    grpavg = circ_mean(grprelvals,[],2);
                else
                    grpavg = mean(grprelvals);
                end
                residual = grprelvals - grpavg;
                if obj.relgrpcircular(i)
                    residual = smod(residual,-pi,pi);
                end
                
                maxrelchange = max(residual.^2 ./ grprelvar);
                val = val + min(maxunsync,(maxrelchange / obj.synctolerance)) * 100;
            end
        end
end


% function val = eval_regularization_full(obj,state)
%     
%     state = state';
%     val = 0;
%     
%     nrels = numel(obj.pgraph.rels);
%     
%     if strcmp(obj.groupmode,'implicit')
%         % do not use the nodes and relationships of the implicit groups
%         % groups are included in a separate term
%         
%         relmask = false(1,nrels);
%         relmask(obj.pgraph.node2relinds([...
%             obj.pgraph.elmreloffset+1 : obj.pgraph.elmreloffset+numel(obj.pgraph.elmrels),...
%             obj.pgraph.metareloffset+1 : obj.pgraph.metareloffset+numel(obj.pgraph.metarels)])) = true;
%     else
%         relmask = true(1,nrels);
%     end
% 
%     rvals = nan(1,nrels);
%     rvars = nan(1,nrels);
%     
%     for i=1:numel(obj.relfs)
%         % evaluate all relationships functions of one type
%         [v,var] = obj.relfs{i}(...
%             reshape(state(obj.relfinput1inds{i}),size(obj.relfinput1inds{i})),...
%             reshape(state(obj.relfinput2inds{i}),size(obj.relfinput2inds{i})));
%         
%         rvals(obj.relfrelinds{i}) = v;
%         rvars(obj.relfrelinds{i}) = var;
%     end
%     
%     origrvals = [obj.pgraph.rels.value];
%     origrvars = [obj.pgraph.rels.variance];
%     
% %     % how many relationships changed (change in relationships normalized by variance)
% %     relchange = min(1,abs(rvals(relmask) - origrvals(relmask))./(sqrt(origrvars(relmask)).*0.5));
% %     val = val + sum(relchange)^2 * 0.05;
%     
% %     synctolerance = 0.1;
%     maxunsync = 5; % in multiples of the synctolerance
%     
%     residual = rvals(relmask) - origrvals(relmask);
%     residual(not(relmask)) = 0;
%     for i=1:numel(obj.relgrprelinds)
%         if not(isempty(obj.relgrprelinds{i})) % && all(relmask(obj.relgrprelinds{i}))
%             maxrelchange = max(residual(obj.relgrprelinds{i}).^2 ./ origrvars(obj.relgrprelinds{i}) );
%             val = val + min(maxunsync,(maxrelchange / obj.synctolerance)) * 10 ;
%         end
%     end
%     
% %         % sum of average element changes in all element groups (or how many
% %         % elment groups changed)
% %         elmposediff = obj.oldelmposes - reshape(...
% %                 state(obj.elmposestateinds),size(obj.elmposestateinds));
% %         for i=1:numel(obj.elmgrpelminds)
% %             if not(isempty(obj.elmgrpelminds{i})) % && not(any(activemask(obj.elmgrpelminds{i})))
% %                 val = val + mean(sum(elmposediff(:,obj.elmgrpelminds{i}).^2,1) ./ obj.elmposevar);
% %             end
% %         end
%         
%     residual = obj.oldelmposes - reshape(...
%             state(obj.elmposestateinds),size(obj.elmposestateinds));
%     for i=1:numel(obj.elmgrpelminds)
%         if not(isempty(obj.elmgrpelminds{i})) % && not(any(activemask(obj.elmgrpelminds{i})))
%             maxelmchange = max( sum(residual(:,obj.elmgrpelminds{i}).^2,1) ./ obj.elmposevar );
%             val = val + min(maxunsync,(maxelmchange / obj.synctolerance)) * 50;
%         end
%     end
%         
%         
% %     for i=1:numel(obj.relgrprelinds)
% %         if numel(obj.relgrprelinds{i}) > 1 && all(relmask(obj.relgrprelinds{i}))
% %             grpstateinds = cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds(obj.relgrprelinds{i})});
% %             grprelvals = state(grpstateinds);
% %             if obj.relgrpcircular(i)
% %                 grpavg = circ_mean(grprelvals,[],2);
% %             else
% %                 grpavg = mean(grprelvals);
% %             end
% %             residual = grprelvals - grpavg;
% %             if obj.relgrpcircular(i)
% %                 residual = smod(residual,-pi,pi);
% %             end
% % 
% % %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*9999;
% % %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*1000;
% %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*10;
% % %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i})) * mean(1-changedrelmask(obj.relgrprelinds{i})) * 100;
% % %             val = val + sum(((residual).^2 ./ rvars(obj.relgrprelinds{i})) .* rellagrangemult_combined(obj.relgrprelinds{i})) * 100;
% %         end
% %     end
% 
%         for i=1:numel(obj.relgrprelinds)
%             if numel(obj.relgrprelinds{i}) > 1 && all(relmask(obj.relgrprelinds{i}))
%                 grpstateinds = cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds(obj.relgrprelinds{i})});
%                 grprelvals = state(grpstateinds);
%                 grprelvar = obj.fullstatevar(grpstateinds)';
%                 if obj.relgrpcircular(i)
%                     grpavg = circ_mean(grprelvals,[],2);
%                 else
%                     grpavg = mean(grprelvals);
%                 end
%                 residual = grprelvals - grpavg;
%                 if obj.relgrpcircular(i)
%                     residual = smod(residual,-pi,pi);
%                 end
%                 
%                 maxrelchange = max(residual.^2 ./ grprelvar);
%                 val = val + min(maxunsync,(maxrelchange / obj.synctolerance)) * 100;
%             end
%         end
% end


function val = evalsubstate_nograd(obj,substate)
    
    % todo: for relationships that need other properties of the elements
    % other than their pose (e.g. distance to the boundary): need to move
    % elements to the position given by the state and pass them to the
    % relationship function (boundary vertex positions relative to the pose
    % act like constants in the relationship function)
    
    state = obj.fullstate;
    state(obj.substateinds) = substate;
    state = state';
    val = 0;
    
    activerelmask = false(1,numel(obj.pgraph.rels));
    activerelmask(obj.activerelinds) = true;
    
    % no large weight for fixed nodes - they are never changed
    % anyway since they are not in the substate and using large weights
    % would prevent a propagation of the error between siblings that have a
    % fixed node as parent
%     fixedstateweights = ones(size(state));
%     fixedstateweights(obj.fixedstateinds) = 1000;

    
    % fixed nodes are not in the active set of nodes, so the state does not
    % get changed in the optimization, but the corresponding relationships
    % / elements still get evaluated and added to the cost
    % this is like coordinate (block) descent if dependentstate are
    % the only variables that directly depend on the substate
    
    for i=1:numel(obj.relfs)
        
        activemask = activerelmask(obj.relfrelinds{i});
        
        if any(activemask)
            [v,var] = obj.relfs{i}(...
                reshape(state(obj.relfinput1inds{i}(:,activemask)),size(obj.relfinput1inds{i}(:,activemask))),...
                reshape(state(obj.relfinput2inds{i}(:,activemask)),size(obj.relfinput2inds{i}(:,activemask))));
            
            residual = v-state(obj.relfoutputinds{i}(activemask));
            if obj.relfcircular(i)
                residual = smod(residual,-pi,pi);
            end
%             unsync = (residual.^2 ./ var) * ...
%                 (obj.relweights(obj.relfrelinds{i}(activemask))' .* ...
%                 fixedstateweights(obj.relfoutputinds{i}(activemask)));
            unsync = (residual.^2 ./ var) * obj.relweights(obj.relfrelinds{i}(activemask))';
            val = val + unsync; % weighted mahalanobis distance
            
            obj.fullstatevar(obj.relfoutputinds{i}(activemask)) = var;
        end
        
    end
end

end

% methods(Static)
% 
% 
% 
% end

end
