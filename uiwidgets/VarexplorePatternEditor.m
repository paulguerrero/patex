classdef VarexplorePatternEditor < AxesWidget
    
properties(SetAccess=protected)
    psolver = GuidedPropagationSolver.empty;
    
    pwidget = PatternWidget.empty;
    
    sidepanelVisible = true;
    
    sidepanel = gobjects(1,0);
    
    sidepanelncols = 3;
    sidepanelnrows = 3;
    
    sidepanelrows = gobjects(0,0);
    sidepanelcols = {};
    sidepanelaxes = {};
    colscrollpos = zeros(0,0);
    rowscrollpos = 1;
    
    sidepanelsolvers = {};
    sidepanelsolinds = {};
    sidepanelsolapprox = {}; % true if the soluton is only approximate (linear approximation usually)
    sidepanelrowfixedelminds = {}; % fixed elements in each row (elements that have been directly modified by the user to get these solutions)
    sidepanelpgraphs = {};
    sidepanelcolhistory = zeros(1,0);
    
    createVariationsMode = true;
    
    settings = struct;
end

properties(SetObservable, AbortSet)
    selrowinds = zeros(1,0); % selected row (not row of selected col)
    selcolinds = zeros(2,0); % two elements: column and row (which may be different from selrowinds)
end

properties(Access=protected)
    patternpreloadedListener = event.listener.empty;
    patternpostloadedListener = event.listener.empty;
    toolListener = event.listener.empty;
    selrowindsListener = event.listener.empty;
    selcolindsListener = event.listener.empty;
    
    uigroup = UIHandlegroup.empty;
    
%     gmenu_saveexpanel = gobjects(1,0);
%     gmenu_screenshotrow = gobjects(1,0);
    gmenu_sidepanel = gobjects(1,0);
    gmenu_settings = gobjects(1,0);
   
    gsidepanelpgraphelms = {};
    
    gsolveelms = gobjects(1,0);
    gsolverels = gobjects(1,0);
    gsolvereldir = gobjects(1,0);
   
    draggingelm = false;
    
    draggingrow = false;
    draggingrowx = false;
    draggingrowy = false;
    dragrowind = zeros(1,0);
    dragorigrowscrollpos = zeros(1,0);
    dragorigcolscrollpos = zeros(1,0);
    
    mousepos_screen = [];
    clickedpos_screen = [];
    
    oldselrowinds = zeros(1,0);
    oldselcolinds = zeros(2,0);
    
%     fixedelminds = zeros(1,0); % fixed elements in the current pattern graph

    lastexpanelsavename = '';
    lastexpanelloadname = '';
    lastscreenshotrowsavename = '';
    
    keyrowind = 1;
end

methods(Static)

function s = defaultSettings
    s = struct(...
            'color_sidepanelsolution_background',rgbhex2rgbdec('fcfbfd'),...
            'color_sidepanelsolution_backgroundcolor_selected',rgbhex2rgbdec('edf8e9'),...
            'color_sidepanelbackground',rgbhex2rgbdec('737373'),...
            'color_sidepanelrow_selected',rgbhex2rgbdec('525252'),...
            'color_sidepanelsolution_margin',rgbhex2rgbdec('31a354'),...
            'color_sidepanelmodelements',rgbhex2rgbdec('54278f'),...
            'visualize_nloptimization','none',...
            'fixelements',false); 
end
    
end

methods
    
function obj = VarexplorePatternEditor(ax,pwidget,menus,varargin)
    
    obj = obj@AxesWidget(ax,'Variation Exploration Pattern Editor');
    
    obj.pwidget = pwidget;
    
    obj.uigroup = UIHandlegroup;
    
    obj.settings = VarexplorePatternEditor.defaultSettings;
    if not(isempty(varargin))
        obj.settings = nvpairs2struct(varargin,obj.settings,false,0);
    end
    
    obj.sidepanel = uipanel(obj.pwidget.fig,...
        'Position',[0,0,1,1],... % will be adjusted in layout panels
        'BackgroundColor',obj.settings.color_sidepanelbackground,...
        'BorderType','line',...
        'BorderWidth',2,...
        'HighlightColor',obj.settings.color_sidepanelrow_selected,...
        'Visible','off');
    
    % file menu
%     obj.gmenu_saveexpanel = uimenu(menus.File,...
%         'Label','Save Exploration Panel',...
%         'Callback', {@(src,evt) obj.saveExpanel},...
%         'Checked','off');
%     obj.uigroup.addChilds(obj.gmenu_saveexpanel);
%     
%     obj.gmenu_screenshotrow = uimenu(menus.File,...
%         'Label','Screenshot Row',...
%         'Callback', {@(src,evt) obj.screenshotRow},...
%         'Checked','off');
%     obj.uigroup.addChilds(obj.gmenu_screenshotrow);

    % vis menu
    obj.gmenu_sidepanel = uimenu(menus.Vis,...
        'Label','Exploration Panel',...
        'Callback', {@(src,evt) obj.toggleSidepanelVisible},...
        'Checked',iif(obj.sidepanelVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_sidepanel);
    
    % settings menu
    obj.gmenu_settings = uimenu(menus.Settings,...
        'Label','Settings',...
        'Callback', {@(src,evt) obj.showSettingsdialog},...
        'Checked','off');
    obj.uigroup.addChilds(obj.gmenu_settings);
    
    obj.patternpreloadedListener = obj.pwidget.addlistener('patternpreloaded',@(src,evt) obj.patternpreloadedResponse);
    obj.patternpostloadedListener = obj.pwidget.addlistener('patternpostloaded',@(src,evt) obj.patternpostloadedResponse);
    obj.toolListener = obj.pwidget.addlistener('tool','PostSet',@(src,evt) obj.toolChanged);
    obj.selrowindsListener = obj.addlistener('selrowinds','PostSet',@(src,evt) obj.selrowindsChanged);
    obj.selcolindsListener = obj.addlistener('selcolinds','PostSet',@(src,evt) obj.selcolindsChanged);
    
    obj.psolver = GuidedPropagationSolver;
    obj.pwidget.synctolerance = obj.psolver.settings.synctolerance;
    
    if not(isempty(obj.pwidget.pgraph))
        obj.setSidepanelrow(...
            1,...
            obj.pwidget.pgraph.clone,...
            1,...
            false,...
            obj.psolver.clone,...
            zeros(1,0));
        obj.selrowinds = 1;
        obj.selcolinds = [1;1];
        obj.sidepanelcolhistory(1) = 1;
    end
    
    obj.hide;
end

function delete(obj)
    obj.clear;
    
    delete(obj.patternpreloadedListener(isvalid(obj.patternpreloadedListener)));
    delete(obj.patternpostloadedListener(isvalid(obj.patternpreloadedListener)));
    delete(obj.toolListener(isvalid(obj.toolListener)));
    delete(obj.selrowindsListener(isvalid(obj.selrowindsListener)));
    delete(obj.selcolindsListener(isvalid(obj.selcolindsListener)));
    
    delete(obj.psolver(isvalid(obj.psolver)));
    
    delete(obj.sidepanel(isvalid(obj.sidepanel)));
    
    for i=1:numel(obj.sidepanelpgraphs)
        delete(obj.sidepanelpgraphs{i}(isvalid(obj.sidepanelpgraphs{i})));
    end
    for i=1:numel(obj.sidepanelsolvers)
        delete(obj.sidepanelsolvers{i}(isvalid(obj.sidepanelsolvers{i})));
    end
end

function clear(obj)
    obj.pwidget.clear;
    
    obj.clearLocal;
end

function clearLocal(obj)
    obj.selrowinds = zeros(1,0); % selected row (not row of selected col)
    obj.selcolinds = zeros(2,0); % two elements: column and row (which may be different from selrowinds)
    
    obj.colscrollpos = zeros(0,0);
    obj.rowscrollpos = 1;
    
    obj.hideSolveVis;
    obj.hideSidepanel;
    
    obj.draggingelm = false;
    
    obj.draggingrow = false;
    obj.draggingrowx = false;
    obj.draggingrowy = false;
    obj.dragrowind = zeros(1,0);
    obj.dragorigrowscrollpos = zeros(1,0);
    obj.dragorigcolscrollpos = zeros(1,0);
    
    obj.oldselrowinds = zeros(1,0);
    obj.oldselcolinds = zeros(2,0);
    
%     obj.fixedelminds = zeros(1,0); % fixed elements in the current pattern graph    
    
    obj.sidepanelrows = gobjects(0,0);
    obj.sidepanelcols = {};
    obj.sidepanelaxes = {};
    
    for i=1:numel(obj.sidepanelpgraphs)
        delete(obj.sidepanelpgraphs{i}(isvalid(obj.sidepanelpgraphs{i})));
    end
    obj.sidepanelpgraphs = {};
    obj.sidepanelsolinds = {};
    obj.sidepanelsolapprox = {};
    obj.sidepanelrowfixedelminds = {};
    for i=1:numel(obj.sidepanelsolvers)
        delete(obj.sidepanelsolvers{i}(isvalid(obj.sidepanelsolvers{i})));
    end
    obj.sidepanelsolvers = {}; % do not delete solver, might be used in some other row
    obj.sidepanelcolhistory = zeros(1,0);
    
    obj.psolver.clear;
    obj.psolver.setPatterngraph(PatternGraph);
end

function patternpreloadedResponse(obj)
    obj.clearLocal;
end

function patternpostloadedResponse(obj)
    obj.psolver.setPatterngraph(obj.pwidget.origpgraph.clone);
    obj.setSidepanelrow(...
        1,...
        obj.pwidget.pgraph.clone,...
        1,...
        false,...
        obj.psolver.clone,...
        zeros(1,0));
    obj.selrowinds = 1;
    obj.selcolinds = [1;1];
    obj.sidepanelcolhistory(1) = 1;
    
    if obj.isvisible
        obj.showSidepanel;
    end
end

function screenshotRow(obj,rowind,filename,targetresoltion)

    if nargin < 2 || isempty(rowind)
        rowind = obj.selrowinds(end);
    end
    
    if nargin < 4 || isempty(targetresoltion)
        targetresolution = [1920,1080];
    end
    
    if nargin < 3 || isempty(filename)
        if not(isempty(obj.lastscreenshotrowsavename))
            savename = obj.lastscreenshotrowsavename;
        else
            savename = './screenshot.svg';
        end

        [fname,pathname,~] = ...
            uiputfile({'*.svg','Scalable Vector Graphics (*.svg)';'*.png','PNG Image File (*.png)'},'Save Screenshot',savename);

        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,'/',fname];
        else
            return;
        end
    end
    
    [fdir,fname,fext] = fileparts(filename);
    
    if strcmp(fext,'.png')
        for i=1:numel(obj.sidepanelcols{rowind})
            
            obj.setColscrollpos(rowind,i);
            obj.openVariation(rowind,i);
            drawnow;
            
%             colax = obj.sidepanelaxes{rowind}(i);
            colfilename = [fdir,'/',fname,'_var',num2str(i),fext];
            
            % png screenshot
            currentunits = obj.axes.Units;
            obj.axes.Units = 'pixels';

    %         axsize = floor(obj.axes.Position(3:4));
    %         margin = (axsize-targetresolution)./2;
    %         axrect = [[0,0]+max(0,margin),axsize-max(0,margin)];
    %         axrect(3:4) = axrect(3:4)-axrect(1:2); % convert to width,height
    %         F = getframe(obj.axes,axrect);
    %         if not(isempty(F.colormap))
    %             error('Image screenshots from a non-true color system are not supported yet.');
    %         end
    %         img = ones(targetresolution(2),targetresolution(1),3,'uint8').*255;
    %         imgarea = [[0,0]+abs(min(0,margin)),targetresolution-abs(min(0,margin))];
    %         imgarea_pixinds = [ceil(imgarea(1:2)+0.5),floor(imgarea(3:4)+0.5)];
    %         img(imgarea_pixinds(2):imgarea_pixinds(4),imgarea_pixinds(1):imgarea_pixinds(3),:) = F.cdata;

            F = getframe(obj.axes);
            if not(isempty(F.colormap))
                error('Image screenshots from a non-true color system are not supported yet.');
            end
            F.cdata = imresize(F.cdata,min(targetresolution./[size(F.cdata,2),size(F.cdata,1)]));
            img = ones(targetresolution(2),targetresolution(1),3,'uint8').*255;
            margin = [size(img,2),size(img,1)] - [size(F.cdata,2),size(F.cdata,1)];
            imgarea_pixinds = [ceil(margin./2)+1,ceil(margin./2)+[size(F.cdata,2),size(F.cdata,1)]];
            img(imgarea_pixinds(2):imgarea_pixinds(4),imgarea_pixinds(1):imgarea_pixinds(3),:) = F.cdata;

            obj.axes.Units = currentunits;


            imwrite(img,colfilename,'png');
        end
    elseif strcmp(fext,'.svg')
        % svg screenshot
        for i=1:numel(obj.sidepanelcols{rowind})
            
            obj.setColscrollpos(rowind,i);
            obj.openVariation(rowind,i);
            drawnow;
            
%             colax = obj.sidepanelaxes{rowind}(i);
            colfilename = [fdir,'/',fname,'_var',num2str(i),fext];
            
            graphics2svg(obj.axes,colfilename,[],false,[],[],[],10); % scaled up 100
        end
    else
        error('Unknown file type.');
    end
    obj.lastscreenshotrowsavename = filename;
end

function transformElements(obj,elminds,newpose)
    obj.pwidget.transformElements(elminds,newpose);
    
    obj.createVariations(obj.pwidget.changedelminds);
end

function showSettingsdialog(obj)
    if not(isempty(obj.psolver)) && isvalid(obj.psolver)
        dlgsettings = patterneditorSettingsdlg(obj);
    end
    solversettings = obj.psolver.settings;
    editorsettings = obj.settings;
    
    if isfield(dlgsettings,'solutionmaxcost')
        solversettings.maxcost = dlgsettings.solutionmaxcost;
    end
    if isfield(dlgsettings,'solutiondistanceRelthresh')
        solversettings.solutiondistancethresh = dlgsettings.solutiondistanceRelthresh;
    end
    if isfield(dlgsettings,'solutiondistanceElmthresh')
        solversettings.elmsolutiondistancethresh = dlgsettings.solutiondistanceElmthresh;
    end
    if isfield(dlgsettings,'maxsolutions')
        solversettings.maxsolutions = dlgsettings.maxsolutions;
    end
    if isfield(dlgsettings,'maxlinearevals')
        solversettings.maxlinearevals = dlgsettings.maxlinearevals;
    end
    if isfield(dlgsettings,'maxgenvariations')
        solversettings.maxgenvariations = dlgsettings.maxgenvariations;
    end
    
    if isfield(dlgsettings,'visualize_nloptimization')
        editorsettings.visualize_nloptimization = dlgsettings.visualize_nloptimization;
    end
    
    obj.psolver.setSettings(solversettings);
    for i=1:numel(obj.sidepanelsolvers)
        for j=1:numel(obj.sidepanelsolvers{i})
            if isvalid(obj.sidepanelsolvers{i}(j))
                obj.sidepanelsolvers{i}(j).setSettings(solversettings);
            end
        end
    end
    
    obj.settings = editorsettings;
end

function setSidepanelrow(obj,rowind,pgraphs,solinds,solapprox,solver,fixedelminds)
    if rowind <= numel(obj.sidepanelrows);
        obj.hideSidepanelcol(rowind);
        delete(obj.sidepanelpgraphs{rowind}(isvalid(obj.sidepanelpgraphs{rowind})));
    end
    
    obj.sidepanelrows(rowind) = gobjects(1,1);
    obj.sidepanelcols{rowind} = gobjects(1,numel(pgraphs));
    obj.sidepanelaxes{rowind} = gobjects(1,numel(pgraphs));
    obj.gsidepanelpgraphelms{rowind} = repmat({gobjects(1,0)},1,numel(pgraphs));
    obj.colscrollpos(rowind) = 1;
    
    obj.sidepanelpgraphs{rowind} = pgraphs;
    obj.sidepanelsolinds{rowind} = solinds;
    obj.sidepanelsolapprox{rowind} = solapprox;
    obj.sidepanelsolvers{rowind} = solver;
    obj.sidepanelrowfixedelminds{rowind} = fixedelminds;
    obj.sidepanelcolhistory(rowind) = 0;
end

function removeSidepanelrow(obj,rowind)
    obj.hideSidepanelrow(rowind);
    
    obj.sidepanelrows(rowind) = [];
    obj.sidepanelcols(rowind) = [];
    obj.sidepanelaxes(rowind) = [];
    obj.colscrollpos(rowind) = [];
    obj.gsidepanelpgraphelms(rowind) = [];
    
    delete(obj.sidepanelpgraphs{rowind}(isvalid(obj.sidepanelpgraphs{rowind})));
    obj.sidepanelpgraphs(rowind) = [];
    obj.sidepanelsolinds(rowind) = [];
    obj.sidepanelsolapprox(rowind) = [];
    obj.sidepanelrowfixedelminds(rowind) = [];
    obj.sidepanelsolvers(rowind) = []; % do not delete solver, might be used in some other row
    obj.sidepanelcolhistory(rowind) = [];
    
    if obj.rowscrollpos > rowind
        obj.rowscrollpos = obj.rowscrollpos-1;
    end
end

function saveExpanel(obj,filename)
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastexpanelsavename))
            savename = obj.lastexpanelsavename;
        elseif not(isempty(obj.lastexpanelloadname))
            savename = obj.lastexpanelloadname;
        else
            savename = 'tempexplorationpanel.mat';
        end
        
        [fname,pathname,~] = ...
            uiputfile({
            '*.mat','MAT file (*.mat)'},...
            'Save Exploration Panel',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    disp('exporting exploration panel...');
    obj.lastexpanelsavename = filename;
    
    ep_solvers = obj.sidepanelsolvers;
    ep_solinds = obj.sidepanelsolinds; %#ok<NASGU>
    ep_solapprox = obj.sidepanelsolapprox; %#ok<NASGU>
    ep_rowfixedelminds = obj.sidepanelrowfixedelminds; %#ok<NASGU>
    ep_pgraphs = obj.sidepanelpgraphs; %#ok<NASGU>
    ep_colhistory = obj.sidepanelcolhistory; %#ok<NASGU>
    
    ep_settings = obj.settings; %#ok<NASGU>
    
    for i=1:numel(ep_solvers)
        % solvers can't have handles to functions in the UI, otherwise the
        % whole UI is stored
        ep_solvers{i}.clearFunctions;
    end
    
    save(filename,'ep_solvers','ep_solinds','ep_solapprox','ep_rowfixedelminds','ep_pgraphs','ep_colhistory','ep_settings','-v7.3');
    
    disp('done');
end

function showSidepanel(obj,updaterows,updatecols,updatepgraphs)
    
    if nargin < 2 || isempty(updaterows)
        updaterows = true;
    end
    
    if nargin < 3 || isempty(updatecols)
        updatecols = true;
    end
    
    if nargin < 4 || isempty(updatepgraphs)
        updatepgraphs = true;
    end
    
    if isgraphics(obj.sidepanel)
        if not(strcmp(obj.sidepanel.Visible,'on'))
            obj.sidepanel.Visible = 'on';
            obj.layoutPanels;
        end
        
        % find rows that are in view
        rscrollpos = obj.rowscrollpos;
        viewrange = [rscrollpos,rscrollpos + obj.sidepanelnrows];    

        nrows = numel(obj.sidepanelrows);
        viewrows = max(1,floor(viewrange(1))):min(nrows,ceil(viewrange(2)));
        
        % hide rows that are out of view
        notviewrows = true(1,nrows);
        notviewrows(viewrows) = false;
        notviewrows = find(notviewrows);	
        
        obj.hideSidepanelrow(notviewrows); %#ok<FNDSB>

        % show rows that are in view
        if updaterows
            mask = true(1,numel(viewrows));
        else
            mask = not(isgraphics(obj.sidepanelrows(viewrows)));
        end
        
        if any(mask)
            obj.showSidepanelrows(viewrows(mask),updatecols,updatepgraphs);
        end
    end
end

function hideSidepanel(obj)
    if isgraphics(obj.sidepanel)
        obj.hideSidepanelrow(1:numel(obj.sidepanelrows));
        
        if not(strcmp(obj.sidepanel.Visible,'off'))
            obj.sidepanel.Visible = 'off';
            obj.layoutPanels;
        end
    end
end

function showSidepanelrows(obj,rowinds,updatecols,updatepgraphs)
    if nargin < 2
        rowinds = 1:numel(obj.sidepanelrows);
    end
    
    if nargin < 3 || isempty(updatecols)
        updatecols = true;
    end
    
    if nargin < 4 || isempty(updatepgraphs)
        updatepgraphs = true;
    end
    
    rscrollpos = obj.rowscrollpos;
    
    rowheight = 1/obj.sidepanelnrows;
    
    for i=1:numel(rowinds)
        
        selected = any(rowinds(i) == obj.selrowinds);
        
        ypos = (1-rowheight) - ((rowinds(i)-1)*rowheight - (rscrollpos-1)*rowheight);
        
        if isgraphics(obj.sidepanelrows(rowinds(i)))
            obj.sidepanelrows(rowinds(i)).Position(2) = ypos;
            if selected
                if any(obj.sidepanelrows(rowinds(i)).HighlightColor ~= obj.settings.color_sidepanelrow_selected)
                    obj.sidepanelrows(rowinds(i)).HighlightColor = obj.settings.color_sidepanelrow_selected;
                end
                if any(obj.sidepanelrows(rowinds(i)).BackgroundColor ~= obj.settings.color_sidepanelrow_selected)
                    obj.sidepanelrows(rowinds(i)).BackgroundColor = obj.settings.color_sidepanelrow_selected;
                end
            else
                if any(obj.sidepanelrows(rowinds(i)).HighlightColor ~= obj.settings.color_sidepanelbackground)
                    obj.sidepanelrows(rowinds(i)).HighlightColor = obj.settings.color_sidepanelbackground;
                end
                if any(obj.sidepanelrows(rowinds(i)).BackgroundColor ~= obj.settings.color_sidepanelbackground)
                    obj.sidepanelrows(rowinds(i)).BackgroundColor = obj.settings.color_sidepanelbackground;
                end
            end
        else
            if selected
                bordercolor = obj.settings.color_sidepanelrow_selected;
            else
                bordercolor = obj.settings.color_sidepanelbackground;
            end
            obj.sidepanelrows(rowinds(i)) = uipanel(obj.sidepanel,...
                    'BackgroundColor',bordercolor,...
                    'Position',[0,ypos,1,rowheight],...
                    'BorderType','line',...
                    'BorderWidth',1,...
                    'HighlightColor',bordercolor);
        end
        
        % find columns that are in view
        cscrollpos = obj.colscrollpos(rowinds(i));
        viewrange = [cscrollpos,cscrollpos + obj.sidepanelncols];

        ncols = numel(obj.sidepanelcols{rowinds(i)});
        viewcols = max(1,floor(viewrange(1))):min(ncols,ceil(viewrange(2)));
        
        % hide columnns that are out of view
        notviewcols = true(1,ncols);
        notviewcols(viewcols) = false;
        notviewcols = find(notviewcols);
        
        obj.hideSidepanelcol(rowinds(i),notviewcols); %#ok<FNDSB>
        
        % show columns that are in view
        if updatecols
            mask = true(1,numel(viewcols));
        else
            mask = not(isgraphics(obj.sidepanelcols{rowinds(i)}(viewcols)));
        end
        
        if any(mask)
            obj.showSidepanelcols(rowinds(i),viewcols(mask),updatepgraphs);
        end
    end
end

function setRowscrollpos(obj,spos)
    obj.rowscrollpos = max(1,min(numel(obj.sidepanelrows),spos));
    
    obj.showSidepanel(true,false,false);
end

function setColscrollpos(obj,rowind,spos)
    obj.colscrollpos(rowind) = max(1,min(numel(obj.sidepanelcols{rowind}),spos));
    
    obj.showSidepanelrows(rowind,true,false);
end

function hideSidepanelrow(obj,rowinds)
    delete(obj.sidepanelrows(rowinds(isvalid(obj.sidepanelrows(rowinds)))));
    obj.sidepanelrows(rowinds) = gobjects(1,numel(rowinds));
end

function showSidepanelcols(obj,rowind,colinds,updatepgraphs)
    if nargin < 3
        colinds = 1:numel(obj.sidepanelcols{rowind});
    end
    
    if nargin < 4 || isempty(updatepgraphs)
        updatepgraphs = true;
    end
    
    scrollpos = obj.colscrollpos(rowind);
    
    colwidth = 1/obj.sidepanelncols;
    
    for i=1:numel(colinds)
        xpos = (colinds(i)-1)*colwidth - (scrollpos-1)*colwidth;
        
        selected = any(rowind == obj.selcolinds(1,:) & colinds(i) == obj.selcolinds(2,:));
        rowselected = any(rowind == obj.selrowinds);
        ishistory = any(colinds(i) == obj.sidepanelcolhistory(rowind));
        
        if isgraphics(obj.sidepanelcols{rowind}(colinds(i)))
            obj.sidepanelcols{rowind}(colinds(i)).Position(1) = xpos;

            if selected
                if any(obj.sidepanelcols{rowind}(colinds(i)).BackgroundColor ~= obj.settings.color_sidepanelsolution_backgroundcolor_selected)
                    obj.sidepanelcols{rowind}(colinds(i)).BackgroundColor = obj.settings.color_sidepanelsolution_backgroundcolor_selected;
                    coltxt = findobj(obj.sidepanelcols{rowind}(colinds(i)),'Tag','coltext');
                    coltxt.BackgroundColor = obj.settings.color_sidepanelsolution_backgroundcolor_selected;
                end
            elseif ishistory
                if any(obj.sidepanelcols{rowind}(colinds(i)).BackgroundColor ~= obj.settings.color_sidepanelsolution_backgroundcolor_selected)
                    obj.sidepanelcols{rowind}(colinds(i)).BackgroundColor = obj.settings.color_sidepanelsolution_backgroundcolor_selected;
                    coltxt = findobj(obj.sidepanelcols{rowind}(colinds(i)),'Tag','coltext');
                    coltxt.BackgroundColor = obj.settings.color_sidepanelsolution_backgroundcolor_selected;
                end
            elseif rowselected
                if any(obj.sidepanelcols{rowind}(colinds(i)).BackgroundColor ~= obj.settings.color_sidepanelsolution_background)
                    obj.sidepanelcols{rowind}(colinds(i)).BackgroundColor = obj.settings.color_sidepanelsolution_background;
                    coltxt = findobj(obj.sidepanelcols{rowind}(colinds(i)),'Tag','coltext');
                    coltxt.BackgroundColor = obj.settings.color_sidepanelsolution_background;
                end
            else
                if any(obj.sidepanelcols{rowind}(colinds(i)).BackgroundColor ~= obj.settings.color_sidepanelsolution_background)
                    obj.sidepanelcols{rowind}(colinds(i)).BackgroundColor = obj.settings.color_sidepanelsolution_background;
                    coltxt = findobj(obj.sidepanelcols{rowind}(colinds(i)),'Tag','coltext');
                    coltxt.BackgroundColor = obj.settings.color_sidepanelsolution_background;
                end
            end
            
            if selected
                if any(obj.sidepanelcols{rowind}(colinds(i)).HighlightColor ~= obj.settings.color_sidepanelsolution_margin)
                    obj.sidepanelcols{rowind}(colinds(i)).HighlightColor = obj.settings.color_sidepanelsolution_margin;
                end
            elseif rowselected
                if any(obj.sidepanelcols{rowind}(colinds(i)).HighlightColor ~= obj.settings.color_sidepanelrow_selected)
                    obj.sidepanelcols{rowind}(colinds(i)).HighlightColor = obj.settings.color_sidepanelrow_selected;
%                     obj.sidepanelcols{rowind}(colinds(i)).BackgroundColor = obj.settings.color_sidepanelsolution_background;
                end
            else
                if any(obj.sidepanelcols{rowind}(colinds(i)).HighlightColor ~= obj.settings.color_sidepanelbackground)
                    obj.sidepanelcols{rowind}(colinds(i)).HighlightColor = obj.settings.color_sidepanelbackground;
                end
            end
        else
            if selected    
                backgroundcolor = obj.settings.color_sidepanelsolution_backgroundcolor_selected;
            elseif ishistory
                backgroundcolor = obj.settings.color_sidepanelsolution_backgroundcolor_selected;
            elseif rowselected
                backgroundcolor = obj.settings.color_sidepanelsolution_background;
            else
                backgroundcolor = obj.settings.color_sidepanelsolution_background;
            end
            
            if selected
                bordercolor = obj.settings.color_sidepanelsolution_margin;
            elseif rowselected
                bordercolor =  obj.settings.color_sidepanelrow_selected;
            else
                bordercolor = obj.settings.color_sidepanelbackground;
            end
            obj.sidepanelcols{rowind}(colinds(i)) = uipanel(obj.sidepanelrows(rowind),...
                'BackgroundColor',backgroundcolor,...
                'Position',[xpos,0,colwidth,1],...
                'BorderType','line',...
                'BorderWidth',4,...
                'HighlightColor',bordercolor,...
                'HitTest','off');
            uicontrol(...
                'Parent',obj.sidepanelcols{rowind}(colinds(i)),...
                'Tag','coltext',...
                'Style','text',...
                'FontSize',20,...
                'String',[char(int32('A')+(rowind-1)),num2str(colinds(i)),'/',num2str(numel(obj.sidepanelcols{rowind}))],...
                'Units','pixels',...
                'BackgroundColor',backgroundcolor,...
                'ForegroundColor',obj.settings.color_sidepanelrow_selected,...
                'Position',[0,0,90,40]);
        end
        
        if isgraphics(obj.sidepanelaxes{rowind}(colinds(i)))
            % do nothing
        else
            obj.sidepanelaxes{rowind}(colinds(i)) = axes(...
                'Parent',obj.sidepanelcols{rowind}(colinds(i)),...
                'DataAspectRatio',[1 1 1],...
                'ALim',[0,1],...
                'CLim',[0,1],...
                'XLim',[-1,1],...
                'YLim',[-1,1],...
                'ZLim',[-1,1],...
                'CameraViewAngleMode','manual',...
                'CameraViewAngle',5,... % small angle so that the near and far clipping planes are not too close, projection is orthogonal anyways
                'CameraPositionMode','manual',...
                'CameraTargetMode','manual',...
                'CameraUpVectorMode','manual',...
                'CameraViewAngleMode','manual',...    
                'FontSize',8,...
                'XTick',[],...
                'YTick',[],...
                'ZTick',[],...
                'XGrid','off',...
                'YGrid','off',...
                'ActivePositionProperty','OuterPosition',...
                'LooseInset',[0,0,0,0],...
                'OuterPosition',[0,0,1,1],...
                'Box','on',...
                'Clipping','off',...
                'HitTest','off',...
                'Visible','off');
        end
        
        pg = obj.sidepanelpgraphs{rowind}(colinds(i));
        elminds = 1:numel(pg.elms);
        
        if not(updatepgraphs)
            if numel(obj.gsidepanelpgraphelms{rowind}{colinds(i)}) == numel(elminds)
                elminds = elminds(not(isgraphics(obj.gsidepanelpgraphelms{rowind}{colinds(i)}(elminds))));
            end
            % otherwise just update all elminds
        end
        
        if not(isempty(obj.sidepanelsolvers{rowind})) && not(isempty(obj.sidepanelsolvers{rowind}.pgraph)) && not(isempty(elminds))
            origelmposes = [obj.sidepanelsolvers{rowind}.pgraph.elms(elminds).pose2D];
            origelmposevar = obj.sidepanelsolvers{rowind}.pgraph.elmposevar;
            solelmposes = [obj.sidepanelpgraphs{rowind}(colinds(i)).elms(elminds).pose2D];
            posediff = solelmposes - origelmposes;
            posediff(3:4,:) = smod(posediff(3:4,:),-pi,pi);
            elmchangedmask = any(bsxfun(@rdivide,...
                posediff.^2,...
                origelmposevar) > ...
                obj.sidepanelsolvers{rowind}.settings.synctolerance,1);
            
            overridecolors = nan(3,numel(elminds));
            overridelinewidths = nan(1,numel(elminds));
            
            overridecolors(:,elmchangedmask) = repmat(obj.settings.color_sidepanelmodelements',1,sum(elmchangedmask));
            
            if not(isempty(obj.sidepanelsolvers{rowind}.elmposechanged))
                elmindsuserchanged = any(obj.sidepanelsolvers{rowind}.elmposechanged(:,elminds),1);
                overridecolors(:,elmindsuserchanged) = repmat(obj.pwidget.settings.color_selectedelements',1,sum(elmindsuserchanged));
                overridelinewidths(:,elmindsuserchanged) = 2;
            end
        else
            overridecolors = [];
            overridelinewidths = [];
        end
        
        obj.gsidepanelpgraphelms{rowind}{colinds(i)} = PatternWidget.showPgraphElements(...
            obj.gsidepanelpgraphelms{rowind}{colinds(i)},...
            obj.sidepanelaxes{rowind}(colinds(i)),...        
            obj.sidepanelpgraphs{rowind}(colinds(i)),...
            elminds,...
            'fixedelminds',obj.sidepanelrowfixedelminds{rowind},...
            'explicithierarchyelements',obj.pwidget.explicithierarchyelements,...
            'color_fixedelements',obj.pwidget.settings.color_fixedelements,...
            'overridecolors',overridecolors,...
            'overridelinewidths',overridelinewidths,...
            'color_elements',obj.pwidget.settings.color_elements);

        obj.updateSidepanelaxes(rowind,colinds(i));
        resetCamera(obj.sidepanelaxes{rowind}(colinds(i)));
    end
end


function updateSidepanelaxes(obj,rowind,colind)
    ax = obj.sidepanelaxes{rowind}(colind);
    pg = obj.sidepanelpgraphs{rowind}(colind);
    
    if not(isempty(ax)) && isgraphics(ax)
    
        if isempty(pg) || isempty(pg.elms)
            bbmin = [-1;-1;-1];
            bbmax = [1;1;1];
        else
            fpadding = [0;0;0.05];

            [bbmin,bbmax] = pg.elms.boundingbox;
            if size(bbmin,1) == 2
                bbmin = [bbmin;zeros(1,size(bbmin,2))];
                bbmax = [bbmax;zeros(1,size(bbmax,2))];
            end
            bbmin = min(bbmin,[],2);
            bbmax = max(bbmax,[],2);
            bbdiag = sqrt(sum((bbmax-bbmin).^2,1));
            bbmax = bbmax + bbdiag .* fpadding;
            bbmin = bbmin - bbdiag .* fpadding;
        end
        
        set(ax,'XLim',[bbmin(1),bbmax(1)]);
        set(ax,'YLim',[bbmin(2),bbmax(2)]);
        set(ax,'ZLim',[bbmin(3),bbmax(3)]);
    end
end


function hideSidepanelcol(obj,rowind,colinds)
    if nargin < 3
        colinds = 1:numel(obj.sidepanelcols{rowind});
    end
    
    delete(obj.sidepanelcols{rowind}(colinds(isvalid(obj.sidepanelcols{rowind}(colinds)))));
    obj.sidepanelcols{rowind}(colinds) = gobjects(1,numel(colinds));
    obj.sidepanelaxes{rowind}(colinds) = gobjects(1,numel(colinds));
    obj.gsidepanelpgraphelms{rowind}(colinds) = repmat({gobjects(1,0)},1,numel(colinds));
end

function toolChanged(obj) %#ok<MANU>
    % do nothing
end

function selrowindsChanged(obj)
    
    changedselrowinds = setxor(obj.oldselrowinds,obj.selrowinds);
    
    if not(isempty(changedselrowinds))
        obj.showSidepanelrows(changedselrowinds,true,false);
    end
    
    obj.oldselrowinds = obj.selrowinds;
end

function selcolindsChanged(obj)
    
%     disp(obj.selcolinds);
    
    changedselcolinds = setxor(obj.oldselcolinds',obj.selcolinds','rows')';

    if not(isempty(changedselcolinds))
        
        [selcolrowinds,~,ind] = unique(changedselcolinds(1,:));

        selcolrowcolinds = array2cell_mex(changedselcolinds(2,:),ind',numel(selcolrowinds));

        % iterator over rows
        for i=1:numel(selcolrowinds)
            colinds = selcolrowcolinds{i};
            obj.showSidepanelcols(selcolrowinds(i),colinds,false);
        end
    end
    
    obj.oldselcolinds = obj.selcolinds;
end

function stop = solverCallback(obj,solver,varargin)

    obj.showSolveVis(solver,varargin{:});
    
    pause(0.01);
    drawnow;
    
    stop = false;
end

function showSolveVis(obj,solver,varargin)
    
    if ischar(solver) && strcmp(solver,'none')
        elmposes = varargin{1};
    elseif isa(solver,'LinestNonlinearSolver') || isa(solver,'StochasticLinearSolver')
        elmposes = varargin{1};
        
        nodefixorig = varargin{2};
        if not(isrow(nodefixorig))
            nodefixorig = nodefixorig';
        end
        
        nodefixnew = varargin{3};
        if not(isrow(nodefixnew))
            nodefixnew = nodefixnew';
        end
        
        elmoffset = obj.pwidget.pgraph.elmoffset;
        elmfixorig = nodefixorig(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relfixorig = nodefixorig(obj.pwidget.pgraph.rel2nodeinds);
        
        elmfixnew = nodefixnew(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relfixnew = nodefixnew(obj.pwidget.pgraph.rel2nodeinds);
        
        if obj.pwidget.relationshipsVisible
            obj.pwidget.toggleRelationshipsVisible;
        end
    elseif isa(solver,'GuidedPropagationSolver')
        elmposes = varargin{1};

        nodefixorig = varargin{2};
        if not(isrow(nodefixorig))
            nodefixorig = nodefixorig';
        end

        nodefixnew = varargin{3};
        if not(isrow(nodefixnew))
            nodefixnew = nodefixnew';
        end
        
        if numel(varargin) >= 4
            nodevisited = varargin{4};
            if not(isrow(nodevisited))
                nodevisited = nodevisited';
            end
        else
            nodevisited = true(1,numel(solver.pgraph.nodes));
        end

        elmoffset = obj.pwidget.pgraph.elmoffset;

        elmfixorig = nodefixorig(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relfixorig = nodefixorig(obj.pwidget.pgraph.rel2nodeinds);

        elmfixnew = nodefixnew(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relfixnew = nodefixnew(obj.pwidget.pgraph.rel2nodeinds);

        elmvisited = nodevisited(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relvisited = nodevisited(obj.pwidget.pgraph.rel2nodeinds);

        if obj.pwidget.relationshipsVisible
            obj.pwidget.toggleRelationshipsVisible;
        end
    end
    
    if isa(solver,'ChooserelsDepthfirstSolver') || isa(solver,'LinestNonlinearSolver') || ...
        isa(solver,'StochasticLinearSolver') || (ischar(solver) && strcmp(solver,'none'))
        
        colors = repmat([0.3;0.8;0.3],1,numel(obj.pwidget.pgraph.elms));
        
    elseif isa(solver,'GuidedPropagationSolver')
        
        colors = ...
            bsxfun(@times,max(0,min(1,1-(elmfixorig+elmfixnew))),[0.3;0.8;0.3]) + ...
            bsxfun(@times,max(0,min(1,elmfixorig)),[0.8;0.3;0.3]) + ...
            bsxfun(@times,max(0,min(1,elmfixnew)),[0.3;0.3;0.8]);

        colors = ...
            bsxfun(@times,colors,max(0,min(1,elmvisited))) + ...
            bsxfun(@times,[0.3;0.3;0.3],max(0,min(1,1-elmvisited)));
        
    else
        error('Unknown Solver.');
    end

    elmvisverts = cell(1,numel(obj.pwidget.pgraph.elms));
    for i=1:numel(obj.pwidget.pgraph.elms)
        elmvisverts{i} = posetransform(obj.pwidget.pgraph.elms(i).verts,[elmposes(1:5,i);0],obj.pwidget.pgraph.elms(i).pose2D);
        elmvisverts{i} = [elmvisverts{i};ones(1,size(elmvisverts{i},2)).*0.01]; % add z offset to be on top
    end
    
    obj.gsolveelms = showPolygons(...
        elmvisverts,obj.gsolveelms,obj.axes,...
        'facecolor','none',... % default for non-filled polygons
        'edgecolor',colors);

    layout = nan(2,size(obj.pwidget.pgraph.adjacency,1));
    layout = PatternWidget.updatePGraphlayout_s(...
        obj.pwidget.pgraph.adjacency,...
        elmposes(1:2,:),...
        numel(obj.pwidget.pgraph.elmrels),...
        numel(obj.pwidget.pgraph.metarels),...
        layout);

    offset = numel(obj.pwidget.pgraph.elms);
    srcpos = layout(:,obj.pwidget.pgraph.elmrelsrcind);
    tgtpos = layout(:,obj.pwidget.pgraph.elmreltgtind);
    pos = layout(:,offset+1:offset+numel(obj.pwidget.pgraph.elmrels));

    elmrelverts = reshape([srcpos;pos;tgtpos],2,[]);
    elmrelverts = mat2cell(elmrelverts,2,ones(1,size(pos,2)).*3);

    elmreldirverts = reshape(pos + (tgtpos-pos) .* 0.1,2,[]);
    elmreldirverts = mat2cell(elmreldirverts,2,ones(1,size(pos,2)));

    offset = numel(obj.pwidget.pgraph.elms) + numel(obj.pwidget.pgraph.elmrels);
    srcpos = layout(:,obj.pwidget.pgraph.metarelsrcind);
    tgtpos = layout(:,obj.pwidget.pgraph.metareltgtind);
    pos = layout(:,offset+1:offset+numel(obj.pwidget.pgraph.metarels));

    metarelverts = reshape([srcpos;pos;tgtpos],2,[]);
    metarelverts = mat2cell(metarelverts,2,ones(1,size(pos,2)).*3);

    metareldirverts = reshape(pos + (tgtpos-pos) .* 0.1,2,[]);
    metareldirverts = mat2cell(metareldirverts,2,ones(1,size(pos,2)));

    verts = [elmrelverts,metarelverts];
    dirverts = [elmreldirverts,metareldirverts];

    if(ischar(solver) && strcmp(solver,'none'))
        relcolor = repmat([0.3;0.8;0.3],1,numel(obj.pwidget.pgraph.elmrels)+numel(obj.pwidget.pgraph.metarels));
    elseif isa(solver,'ChooserelsDepthfirstSolver') || isa(solver,'LinestNonlinearSolver') || isa(solver,'StochasticLinearSolver')
        
        relcolor = ...    
            bsxfun(@times,max(0,min(1,1-(relfixorig+relfixnew))),[0.3;0.8;0.3]) + ...
            bsxfun(@times,max(0,min(1,relfixorig)),[0.8;0.3;0.3]) + ...
            bsxfun(@times,max(0,min(1,relfixnew)),[0.3;0.3;0.8]);
        
    elseif isa(solver,'GuidedPropagationSolver')
        
        relcolor = ...    
            bsxfun(@times,max(0,min(1,1-(relfixorig+relfixnew))),[0.3;0.8;0.3]) + ...
            bsxfun(@times,max(0,min(1,relfixorig)),[0.8;0.3;0.3]) + ...
            bsxfun(@times,max(0,min(1,relfixnew)),[0.3;0.3;0.8]);

        relcolor = ...
            bsxfun(@times,relcolor,max(0,min(1,relvisited))) + ...
            bsxfun(@times,[0.3;0.3;0.3],max(0,min(1,1-relvisited)));
    else
        error('Unknown Solver.');
    end

    color = relcolor;
    dircolor = relcolor;

    color = reshape(repmat(color,3,1),3,[]);
    color = mat2cell(color,3,ones(1,size(verts,2)).*3);

    dircolor = reshape(repmat(dircolor,1,1),3,[]);
    dircolor = mat2cell(dircolor,3,ones(1,size(dirverts,2)));

    obj.gsolverels = showPolygons(...
        {verts},obj.gsolverels,obj.axes,...
        'fvcolor',{color},...
        'edgecolor','flat',...
        'facecolor','none',...
        'markersymbol','o',...
        'markerfacecolor','flat');

    if obj.pwidget.relationshipdirsVisible
        obj.gsolvereldir = showPoints(...
            dirverts,obj.gsolvereldir,obj.axes,...
            'fvcolor',dircolor,...
            'markersymbol','s',...
            'markerfacecolor','flat');
    else
        delete(obj.gsolvereldir(isvalid(obj.gsolvereldir)));
        obj.gsolvereldir = gobjects(1,0);            
    end
end

function hideSolveVis(obj)
    delete(obj.gsolveelms(isvalid(obj.gsolveelms)));
    obj.gsolveelms = gobjects(1,0);
    delete(obj.gsolverels(isvalid(obj.gsolverels)));
    obj.gsolverels = gobjects(1,0);
    delete(obj.gsolvereldir(isvalid(obj.gsolvereldir)));
    obj.gsolvereldir = gobjects(1,0);
end

function unblock(obj)
    obj.blockcallbacks = false;
    obj.pwidget.unblock;
end

function mousePressed(obj,src,evt)

    obj.pwidget.mousePressed(src,evt);
    
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;
    
    obj.mousedown = true;

    obj.mousepos = mouseposOnPlane(obj.axes);
    obj.clickedpos = obj.mousepos;
    obj.mousepos_screen = mouseScreenpos;
    obj.clickedpos_screen = obj.mousepos_screen;
    
    if obj.pwidget.selpanel == obj.pwidget.mainpanel
        if obj.pwidget.navwidget.zoommode || ...
               obj.pwidget.navwidget.panmode || ...
               obj.pwidget.navwidget.orbitmode

            % do nothing

        elseif strcmp(obj.pwidget.tool,'elementselect')
            
            % do nothing
        end
    elseif ismember(obj.pwidget.selpanel,obj.sidepanelrows)
        if strcmp(src.SelectionType,'alt') || strcmp(src.SelectionType,'extend')
            % move rows/columns
            clickedpanel = gparent(obj.pwidget.fig.CurrentObject,'uipanel',true);
            [~,rowind] = ismember(clickedpanel,obj.sidepanelrows);
            
            if not(isempty(rowind))
                obj.draggingrow = true;
                obj.dragrowind = rowind;
                obj.dragorigrowscrollpos = obj.rowscrollpos;
                obj.dragorigcolscrollpos = obj.colscrollpos(rowind);
                obj.draggingrowx = false;
                obj.draggingrowy = false;
            end
        else
            clickedpanel = gparent(obj.pwidget.fig.CurrentObject,'uipanel',true);
            [~,rowind] = ismember(clickedpanel,obj.sidepanelrows);
            
            if not(isempty(rowind))
                figunits = obj.pwidget.fig.Units;
                obj.pwidget.fig.Units = 'pixels';
                figclickedpos = obj.pwidget.fig.CurrentPoint;
                obj.pwidget.fig.Units = figunits;
                
                rowunits = obj.sidepanelrows(rowind).Units;
                obj.sidepanelrows(rowind).Units = 'pixels';
                rowwidth = obj.sidepanelrows(rowind).Position(3);
                obj.sidepanelrows(rowind).Units = rowunits;
                colwidth = rowwidth/obj.sidepanelncols;
                
                rowpos = getpixelposition(obj.sidepanelrows(rowind),true);

                colind = floor(obj.colscrollpos(rowind) + (figclickedpos(1) - rowpos(1)) / colwidth);
                if colind < 1 || colind > numel(obj.sidepanelcols{rowind})
                    colind = zeros(1,0);
                end
            else
                rowind = zeros(1,0);
                colind = zeros(1,0);
            end
            
            
            % select pattern variation
            if strcmp(src.SelectionType,'open') && not(isempty(obj.selrowinds)) && not(isempty(colind))
                % user selected a solution from the side panel
                
                obj.sidepanelcolhistory(rowind) = colind;
                
                obj.selrowinds = rowind;
                obj.selcolinds = [rowind;colind];
                
                obj.pwidget.setElementblendout(0.7);
                drawnow;
                
                obj.openVariation(obj.selcolinds(1,end),obj.selcolinds(2,end));
                
                % execute queued callbacks before unblocking, so they get canceled
                % (BusyAction does not seem to do anything, so I have to do it this
                % way)
                obj.pwidget.setElementblendout(0);
                drawnow;
                
            end
        end
    end
    
    obj.blockcallbacks = false;
end

function timeneeded = openVariation(obj,rowind,colind)
    
    timeneeded = 0;
    
    if obj.sidepanelsolapprox{rowind}(colind)
        
        obj.showSidepanelcols(rowind,colind,true);

        solind = obj.sidepanelsolinds{rowind}(colind);
        ps = obj.sidepanelsolvers{rowind};

        obj.pwidget.showStatustext('refining chosen variation ...');
        drawnow;

        % non-linear solution
        nlsolver = LinestNonlinearSolver();
        nlsolver.setVisfunction(@(varargin) obj.solverCallback(varargin{:}));
        nlsolver.solve(ps.solver,solind,...
            'posefixdims',ps.solver.settings.posefixdims,...
            'visualize',obj.settings.visualize_nloptimization);
        
        timeneeded = nlsolver.nltime;

        if numel(nlsolver.solutionelmposes) ~= 1
            error('Invalid solution for the nonlinear solver.');
        end
        
        obj.pwidget.showStatustext('updating UI...');
        drawnow;
        
        
        pg = nlsolver.createSolutionPgraphs(1);

        oldpg = obj.sidepanelpgraphs{rowind}(colind);
        delete(oldpg(isvalid(oldpg)));
        obj.sidepanelpgraphs{rowind}(colind) = pg;
        obj.sidepanelsolapprox{rowind}(colind) = false;

        obj.hideSolveVis;

        obj.showSidepanelcols(rowind,colind,true);
    end
    
    

    ps = obj.sidepanelsolvers{rowind};
    pg = obj.sidepanelpgraphs{rowind}(colind);

    obj.pwidget.setPattern(pg.clone,false);
    obj.psolver = ps.clone;
    obj.pwidget.setFixedelminds(obj.sidepanelrowfixedelminds{rowind});

    obj.psolver.clear;
    
    obj.pwidget.hideStatustext;
    drawnow;
end


function mouseMoved(obj,src,evt)

    obj.pwidget.mouseMoved(src,evt);
    
    if obj.mousedown

        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;

        obj.mousepos = mouseposOnPlane(obj.axes);
        obj.mousepos_screen = mouseScreenpos;

        if obj.pwidget.selpanel == obj.pwidget.mainpanel
            
            if obj.pwidget.navwidget.zoommode || ...
               obj.pwidget.navwidget.panmode || ...
               obj.pwidget.navwidget.orbitmode

                % do nothing

            elseif strcmp(obj.pwidget.tool,'elementselect')
                
                if not(isempty(obj.pwidget.dragelminds)) && (obj.draggingelm || norm(obj.mousepos_screen-obj.clickedpos_screen) > 15) && strcmp(src.SelectionType,'normal')
                    obj.draggingelm = true;
                else
                    obj.draggingelm = false;
                end
            end
        elseif ismember(obj.pwidget.selpanel,obj.sidepanelrows)
            
            if (strcmp(src.SelectionType,'alt') || strcmp(src.SelectionType,'extend')) && not(isempty(obj.dragrowind))
                dragvec = obj.mousepos_screen - obj.clickedpos_screen;
                
                if not(obj.draggingrowx) && not(obj.draggingrowy)
                    if dragvec(1) >= dragvec(2)
                        obj.draggingrowx = true;
                        obj.draggingrowy = false;
                    else
                        obj.draggingrowy = true;
                        obj.draggingrowx = false;
                    end
                end
                
                sidepanelunits = obj.sidepanel.Units;
                obj.sidepanel.Units = 'pixels';
                sidepanelheight = obj.sidepanel.Position(4);
                obj.sidepanel.Units = sidepanelunits;
                
                rowunits = obj.sidepanelrows(obj.dragrowind).Units;
                obj.sidepanelrows(obj.dragrowind).Units = 'pixels';
                rowwidth = obj.sidepanelrows(obj.dragrowind).Position(3);
                obj.sidepanelrows(obj.dragrowind).Units = rowunits;
                
                rowheight = sidepanelheight/obj.sidepanelnrows;
                
                colwidth = rowwidth/obj.sidepanelncols;
                
                if strcmp(src.SelectionType,'extend')
                    obj.setRowscrollpos(obj.dragorigrowscrollpos + dragvec(2) / rowheight);
                else
                    obj.setColscrollpos(obj.dragrowind,obj.dragorigcolscrollpos - dragvec(1) / colwidth);
                end
            else
                % select pattern variation
            end
        end
        
        obj.blockcallbacks = false;    
        
    end
    
end

function mouseReleased(obj,src,evt)

    obj.pwidget.mouseReleased(src,evt);
    
    obj.mousedown = false;
    
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    if obj.pwidget.selpanel == obj.pwidget.mainpanel
        
        if obj.pwidget.navwidget.zoommode || ...
           obj.pwidget.navwidget.panmode || ...
           obj.pwidget.navwidget.orbitmode

            % do nothing

        elseif strcmp(obj.pwidget.tool,'elementselect')
            if not(isempty(obj.pwidget.changedelminds)) && not(isempty(obj.pwidget.lastchangedelminds))

                if obj.draggingelm
                    
                    if obj.createVariationsMode
                        obj.createVariations(obj.pwidget.changedelminds);
                    end
                    
                    obj.draggingelm = false;
                end
            end
        end
    elseif ismember(obj.pwidget.selpanel,obj.sidepanelrows)
        if strcmp(src.SelectionType,'alt')
            % ...
        else
            % ...
        end
    end
    
    obj.draggingrow = false;
    obj.dragrowind = zeros(1,0);
    obj.dragorigrowscrollpos = zeros(1,0);
    obj.dragorigcolscrollpos = zeros(1,0);
    obj.draggingrowx = false;
    obj.draggingrowy = false;
    
    % execute queued callbacks before unblocking, so they get canceled
    % (BusyAction does not seem to do anything, so I have to do it this
    % way)
    drawnow;
    
    obj.blockcallbacks = false;
end

function createVariations(obj,editedelminds)
    obj.pwidget.setElementblendout(0.7);
    drawnow;

    % hierarchy childs are updated as elements are edited, but these
    % should not be in the optimiziation, remove them
    % => should be ok now since edited elements are passed separately, and
    % these don't include the hierarchy child elements. Relationships
    % between elements should stay the same since there should be no
    % relationships between elements with different hierarchy parents =>
    % except for things like rotation absolute rotation, so leave as is for
    % now
    changedpg = obj.pwidget.origpgraph.clone;
    for i=1:numel(obj.pwidget.changedelminds)
        changedpg.elms(obj.pwidget.changedelminds(i)).transform(...
            obj.pwidget.pgraph.elms(obj.pwidget.changedelminds(i)).pose2D,'absolute');
    end
    changedpg.recomputeRelationships;

    if obj.selrowinds == 1
        lastfixedelminds = obj.pwidget.fixedelminds;
    else
        lastfixedelminds = setdiff(obj.pwidget.fixedelminds,obj.sidepanelrowfixedelminds{obj.selrowinds-1});
    end
    
    if false && all(ismember(obj.pwidget.changedelminds,lastfixedelminds))
        % we don't want this behaviour for now

        felminds = setdiff(obj.pwidget.fixedelminds,lastfixedelminds);

        origpg = obj.sidepanelsolvers{obj.selrowinds(end)}.pgraph.clone;
        newpg = origpg.clone;
        for i=1:numel(lastfixedelminds)
            newpg.elms(lastfixedelminds(i)).transform(changedpg.elms(lastfixedelminds(i)).pose2D,'absolute');
        end
        newpg.recomputeRelationships;
        rowsolinds = obj.sidepanelsolvers{obj.selrowinds(end)}.choicesolinds{1};
        combs = obj.sidepanelsolvers{obj.selrowinds(end)}.solver.solutioncombs(:,rowsolinds);
        additionaloptions = {'combinations',combs};

        updatecurrentrow = true;
    else
        
        felminds = obj.pwidget.fixedelminds;
        origpg = obj.pwidget.origpgraph.clone;
        newpg = changedpg;
        additionaloptions = {};

        updatecurrentrow = false;
    end

    felminds(ismember(felminds,obj.pwidget.lastchangedelminds)) = [];

    newselelmpose = [newpg.elms(obj.pwidget.changedelminds).pose2D];
    origselelmpose = [origpg.elms(obj.pwidget.changedelminds).pose2D];

    posefixdims = [1,2];
    if not(isempty(obj.pwidget.changedelminds)) && any(newselelmpose(3,:) ~= origselelmpose(3,:))
        posefixdims(end+1) = 3;
    end
    if not(isempty(obj.pwidget.changedelminds)) && any(newselelmpose(4,:) ~= origselelmpose(4,:))
        posefixdims(end+1) = 4;
    end

    obj.pwidget.showStatustext('finding pattern variations ...');
    drawnow;

    startelminds = obj.pwidget.lastchangedelminds;
    if isempty(startelminds)
        startelminds = 1;
    end
    
    obj.psolver.setVisfunction(@(x1,x2,x3,x4) obj.solverCallback(x1,x2,x3,x4));
    obj.psolver.setPatterngraph(origpg);
    obj.psolver.solve(...
        [newpg.elms.pose2D],[newpg.rels.value],editedelminds,startelminds,...
        'fixedelminds',felminds,...
        'posefixdims',posefixdims,...
        additionaloptions{:});

    uiid = tic;
    
    obj.pwidget.showStatustext('updating UI...');
    drawnow;
    
    delete(newpg(isvalid(newpg)));

    if not(isempty(obj.psolver.choicesolinds))
        solinds = obj.psolver.choicesolinds{1};
    else
        solinds = zeros(1,0);
    end
    solpgs = obj.psolver.createSolutionPgraphs(solinds);

    if obj.settings.fixelements    
        nowfixedelminds = unique([obj.pwidget.fixedelminds,find(any(obj.psolver.elmposechanged,1))]);
    else
        nowfixedelminds = zeros(1,0);
    end

    if updatecurrentrow
        newrowind = obj.selrowinds;
    else
        newrowind = obj.selrowinds+1;
    end
    for i=numel(obj.sidepanelrows):-1:newrowind
        obj.removeSidepanelrow(i);
    end
    obj.setSidepanelrow(...
        newrowind,...
        solpgs,...
        solinds,...
        true(1,numel(solinds)),...
        obj.psolver.clone,...
        nowfixedelminds);

    obj.showSidepanelrows(newrowind);

    if obj.rowscrollpos < numel(obj.sidepanelrows) - (obj.sidepanelnrows-1)
        obj.setRowscrollpos(numel(obj.sidepanelrows) - (obj.sidepanelnrows-1));
    elseif obj.rowscrollpos > numel(obj.sidepanelrows)
        obj.setRowscrollpos(numel(obj.sidepanelrows));
    end

    if obj.colscrollpos > numel(obj.sidepanelcols)
        obj.setColscrollpos(numel(obj.sidepanelcols));
    end

    if updatecurrentrow
        % re-open currently selected column
        newcombs = obj.sidepanelsolvers{obj.selrowinds(end)}.solver.solutioncombs;
        newselcolind = find(all(bsxfun(@eq,newcombs,combs(:,obj.selcolinds(2,end))),1),1,'first');

        if isempty(newselcolind)
            newselcolind = 1;
        end

        obj.selcolinds(2,end) = newselcolind;

        obj.openVariation(obj.selcolinds(1,end),obj.selcolinds(2,end));
    end

    obj.pwidget.setElementblendout(0);
    
    obj.pwidget.hideStatustext;
    drawnow;
    
    disp('time for ui update: ');
    toc(uiid);
end

function keyPressed(obj,src,evt)
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.keyPressed(src,evt);
    end
    
    % tool-specific keys
    if strcmp(obj.pwidget.tool,'elementselect')
        if strcmp(evt.Key,'escape')
            if obj.draggingelm
                obj.draggingelm = false;
            end
        elseif strcmp(evt.Key,'space')
            obj.createVariationsMode = false;
        elseif strcmp(evt.Key,'return')
            % create variations with the selected elements as edited
            % elements (that are being kept as fixed as possible)
            obj.createVariations(obj.pwidget.selelminds);
        elseif strcmp(evt.Key,'downarrow')
            obj.keyrowind = min(numel(obj.sidepanelrows),obj.keyrowind + 1);
        elseif strcmp(evt.Key,'uparrow')
            obj.keyrowind = max(1,obj.keyrowind - 1);
        elseif strcmp(evt.Key,'rightarrow')
            if not(isempty(obj.keyrowind)) && obj.keyrowind >= 1 && obj.keyrowind <= numel(obj.sidepanelrows)
                
                newcolind = round(obj.colscrollpos(obj.keyrowind) + 1);
                newcolind = min(numel(obj.sidepanelcols{obj.keyrowind}),newcolind);
                
                obj.setColscrollpos(obj.keyrowind,newcolind);
            end
        elseif strcmp(evt.Key,'leftarrow')
            if not(isempty(obj.keyrowind)) && obj.keyrowind >= 1 && obj.keyrowind <= numel(obj.sidepanelrows)
                
                newcolind = round(obj.colscrollpos(obj.keyrowind) - 1);
                newcolind = max(1,newcolind);
                
%                 disp(newcolind);
                
                obj.setColscrollpos(obj.keyrowind,newcolind);
            end
        end
    end
end

function keyReleased(obj,src,evt)
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.keyReleased(src,evt);
    end
    
    if strcmp(obj.pwidget.tool,'elementselect')
        if strcmp(evt.Key,'space')
            obj.createVariationsMode = true;
        end
    end
end

function layoutPanels(obj)
    if isgraphics(obj.pwidget.toolpanel) && ...
       isgraphics(obj.pwidget.mainpanel) && ...
       isgraphics(obj.sidepanel)
   
        mainpanelpos = obj.pwidget.mainpanel.Position;
        toolpanelpos = obj.pwidget.toolpanel.Position;
        sidepanelpos = obj.sidepanel.Position;
        
        if strcmp(obj.pwidget.toolpanel.Visible,'on')
            sidepanelpos(4) = max(1-toolpanelpos(4),0.0001);
            mainpanelpos(4) = max(1-toolpanelpos(4),0.0001);
        else
            sidepanelpos(4) = 1;
            mainpanelpos(4) = 1;
        end
        
        if strcmp(obj.sidepanel.Visible,'on')
            sidepanelpos(1) = 0.5;
            sidepanelpos(3) = 0.5;
            
            mainpanelpos(3) = max(1-sidepanelpos(3),0.0001);
        else
            sidepanelpos(1) = 1;
            sidepanelpos(3) = 0;
            
            mainpanelpos(3) = 1;
        end

        if any(obj.pwidget.mainpanel.Position ~= mainpanelpos)
            obj.pwidget.mainpanel.Position = mainpanelpos;
        end
        if any(obj.sidepanel.Position ~= sidepanelpos)
            obj.sidepanel.Position = sidepanelpos;
        end
        if any(obj.pwidget.toolpanel.Position ~= toolpanelpos)
            obj.pwidget.toolpanel.Position = toolpanelpos;
        end
    end
    
    if not(isempty(obj.pwidget))
        obj.pwidget.layoutPanels;
    end
end

function toggleSidepanelVisible(obj)
    obj.sidepanelVisible = not(obj.sidepanelVisible);
    
    if obj.sidepanelVisible
        obj.showSidepanel;
        set(obj.gmenu_sidepanel,'Checked','on');
    else
        obj.hideSidepanel;
        set(obj.gmenu_sidepanel,'Checked','off');
    end
end

function show(obj)
    obj.show@AxesWidget;
    
    if isvalid(obj.uigroup)
        obj.uigroup.show;
    end
    
    obj.layoutPanels;
    
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.show;
    end
    
    if isvalid(obj.toolListener)
        obj.toolListener.Enabled = true;
    end
    if isvalid(obj.selrowindsListener)
        obj.selrowindsListener.Enabled = true;
    end
    if isvalid(obj.selcolindsListener)
        obj.selcolindsListener.Enabled = true;
    end
    
    if obj.sidepanelVisible
        obj.showSidepanel;
    end
    
    if obj.pwidget.relationshipsVisible
        obj.pwidget.toggleRelationshipsVisible;
    end
    
    if obj.pwidget.editorfig.toolpanelVisible
        obj.pwidget.editorfig.toggleToolpanelVisible;
    end
    if not(obj.pwidget.editorfig.navpanelVisible)
        obj.pwidget.editorfig.toggleNavpanelVisible;
    end
    
    if obj.pwidget.explicithierarchyelements
        obj.pwidget.toggleExplicithierarchyelements;
    end
    
    obj.pwidget.tool = 'elementselect';
end

function hide(obj)
    obj.hide@AxesWidget;
    
    if isvalid(obj.uigroup)
        obj.uigroup.hide;
    end
    
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.hide;
    end
    
    if isvalid(obj.toolListener)
        obj.toolListener.Enabled = false;
    end
    if isvalid(obj.selrowindsListener)
        obj.selrowindsListener.Enabled = false;
    end
    if isvalid(obj.selcolindsListener)
        obj.selcolindsListener.Enabled = false;
    end
    
    obj.hideSolveVis;
    
    obj.hideSidepanel;
end

end

end