classdef PatternWidget < AxesWidget
    
properties(SetAccess = protected)
    origpgraph = PatternGraph.empty;
    
    changedrelinds = zeros(1,0);
    changedelminds = zeros(1,0);
    fixedelminds = zeros(1,0); % fixed for solver, but can be changed by user
    linkedelminds = zeros(1,0); % fixed for user, ancestor or descendant in the hierarchy has changed
    % To support changes of a parent and a child in the same hierarchy,
    % relationships between two elements would have to be in the local
    % coordinate system of the closest common ancestor in the hierarchy (or
    % the global coordinate system if there is no common ancestor). This
    % would make everything quite a bit more cumbersome and complicated to
    % work with, so I do not allow changing related elements
    % (ancestors/descendants) in the hierarchy in a single edit for now.
    
    lastchangedelminds = zeros(1,0);
    
    % ui elements
    navwidget = NavigationWidget.empty;
    
    elementsVisible = true;
    relationshipsVisible = true;
    relationshipdirsVisible = true;
    elementorientationsVisible = false;
    elementcentersVisible = false;
    elmdragvisVisible = true;
    
    explicithierarchyelements = false;
    
    fig = gobjects(1,0);
    toolpanel = gobjects(1,0);
    mainpanel = gobjects(1,0);
    
    editorfig = PatternEditorFigure.empty;
    
    pgraph = PatternGraph.empty;
    
    elmblendout = 0;
    
    settings = struct;
    
    gridsnapping = false;
end

events
    patternpreloaded;
    patternpostloaded;
end

properties(SetObservable, AbortSet)
    selelminds = zeros(1,0);
    selrelinds = zeros(1,0);
    selgroupinds = zeros(1,0);
    selinstgroupinds = zeros(1,0);
    
    openedhelmind = zeros(1,0);

    selhlevel = 0;
    
    selpanel = zeros(1,0);
    
    tool = '';
    subtool = '';
    
    synctolerance = StandardPatternSolver.defaultSettings.synctolerance;
end

properties(Dependent, SetAccess=protected)
    pgraphlayout;
end

properties(SetAccess=protected)
    
    selelmindsListener = event.listener.empty;
    selrelindsListener = event.listener.empty;
    selgroupindsListener = event.listener.empty;
    selinstgroupindsListener = event.listener.empty;
    selhlevelListener = event.listener.empty;
    openedhelmindListener = event.listener.empty;
    toolListener = event.listener.empty;
    
    uigroup = UIHandlegroup.empty;
    
    % vis menu
    gmenu_elements = gobjects(1,1);
    gmenu_relationships = gobjects(1,1);
    gmenu_relationshipdirs = gobjects(1,1);
    gmenu_elementorientations = gobjects(1,1);
    gmenu_elementcenters = gobjects(1,1);
    
    % tools menu
    gmenu_elementselect = gobjects(1,0);
    gmenu_relselect = gobjects(1,0);
    gmenu_groupselect = gobjects(1,0);
    gmenu_instgroupselect = gobjects(1,0);
    gmenu_explicithierarchyelements = gobjects(1,0);

    % settings menu
    gmenu_settings = gobjects(1,0);
    
    % help menu
    gmenu_help = gobjects(1,0);

    % toolbar buttons
    ghlevelpanel = gobjects(1,0);
    ghlevellist = gobjects(1,0);
    
    gstatustext = gobjects(1,1);
    
    % axes content
    gelms = gobjects(1,0);
    
    grels = gobjects(1,1);
    greldirs = gobjects(1,1);
    gelmorientations = gobjects(1,1);
    gelmcenters = gobjects(1,1);
    
    gelmdragvis = gobjects(1,1);
    
    p_pgraphlayout = nan(2,0);
    
    mousepos_screen = [];
    clickedpos_screen = [];
    
    lastpatternloadname = '';
    lastpatternsavename = '';
    
    lastrelloadname = '';
    lastrelsavename = '';
    
    draggingelm = false;
    
    dragelmpose = zeros(6,0);
    dragelminds = zeros(1,0);
    dragrelinds = zeros(1,0);
    dragchangedrelinds = zeros(1,0);
    
    oldselelminds = zeros(1,0);
    oldselrelinds = zeros(1,0);
    oldselgroupinds = zeros(1,0);
    oldselinstgroupinds = zeros(1,0);
    
    rotatemode = false;
    scalemode = false;
end

methods(Static)

function s = defaultSettings
    
    s = struct(...
        'color_elements',rgbhex2rgbdec('4D4D4D'),...
        'color_selectedelements',rgbhex2rgbdec('2b8cbe'),...
        'color_draggedelements',rgbhex2rgbdec('2b8cbe'),...
        'color_changedelements',rgbhex2rgbdec('1d91c0'),...
        'color_fixedelements',rgbhex2rgbdec('41b6c4'),...
        'color_linkedelements',rgbhex2rgbdec('7fcdbb'),...
        'color_selectedgroups',rgbhex2rgbdec('7bccc4'),...
        'blendoutstrength_hierarchy',0.8,...
        'linewidth_selectedelements',2); 
end
    
end

methods
    
function obj = PatternWidget(ax,navwidget,editorfig,fig,toolpanel,mainpanel,menus,varargin)
    obj = obj@AxesWidget(ax,'PatternWidget');
    
    obj.navwidget = navwidget;
    obj.navwidget.orbitenabled = false;
    
    obj.fig = fig;
    obj.toolpanel = toolpanel;
    obj.mainpanel = mainpanel;
    
    obj.editorfig = editorfig;
    
    obj.uigroup = UIHandlegroup;
    
    obj.settings = PatternWidget.defaultSettings;
    if not(isempty(varargin))
        obj.settings = nvpairs2struct(varargin,obj.settings,false,0);
    end
    
    % file menu
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Load Pattern...',...
        'Callback', {@(src,evt) obj.loadPattern},...
        'Separator','on'));
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Save Pattern...',...
        'Callback', {@(src,evt) obj.savePattern}));
    
    % vis menu
    obj.gmenu_elements = uimenu(menus.Vis,...
        'Label','Elements',...
        'Callback', {@(src,evt) obj.toggleElementsVisible},...
        'Checked',iif(obj.elementsVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_elements);
    obj.gmenu_relationships = uimenu(menus.Vis,...
        'Label','Relationships',...
        'Callback', {@(src,evt) obj.toggleRelationshipsVisible},...
        'Checked',iif(obj.relationshipsVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_relationships);
    obj.gmenu_relationshipdirs = uimenu(menus.Vis,...
        'Label','Relationship Directions',...
        'Callback', {@(src,evt) obj.toggleRelationshipdirsVisible},...
        'Checked',iif(obj.relationshipdirsVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_relationshipdirs);
    obj.gmenu_elementorientations = uimenu(menus.Vis,...
        'Label','Element Orientations',...
        'Callback', {@(src,evt) obj.toggleElementorientationsVisible},...
        'Checked',iif(obj.elementorientationsVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_elementorientations);
    obj.gmenu_elementcenters = uimenu(menus.Vis,...
        'Label','Element Centers',...
        'Callback', {@(src,evt) obj.toggleElementcentersVisible},...
        'Checked',iif(obj.elementcentersVisible,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_elementcenters);
    
    % tools menu
    obj.gmenu_elementselect = uimenu(menus.Tools,...
        'Label','Select Elements',...
        'Checked',iif(strcmp(obj.tool,'elementselect'),'on','off'),...
        'Callback', {@(src,evt) obj.selectElementsPressed});
    obj.uigroup.addChilds(obj.gmenu_elementselect);
    obj.gmenu_relselect = uimenu(menus.Tools,...
        'Label','Select Relationships',...
        'Checked',iif(strcmp(obj.tool,'relselect'),'on','off'),...
        'Callback', {@(src,evt) obj.selectRelationshipsPressed});
    obj.uigroup.addChilds(obj.gmenu_relselect);
    obj.gmenu_groupselect = uimenu(menus.Tools,...
        'Label','Select Groups',...
        'Checked',iif(strcmp(obj.tool,'groupselect'),'on','off'),...
        'Callback', {@(src,evt) obj.selectGroupsPressed});
    obj.uigroup.addChilds(obj.gmenu_groupselect);
    obj.gmenu_instgroupselect = uimenu(menus.Tools,...
        'Label','Select Instance Groups',...
        'Checked',iif(strcmp(obj.tool,'instgroupselect'),'on','off'),...
        'Callback', {@(src,evt) obj.selectInstgroupsPressed});
    obj.uigroup.addChilds(obj.gmenu_instgroupselect);
    
    % settings menu
    obj.gmenu_explicithierarchyelements = uimenu(menus.Settings,...
        'Label','Explicit Hierarchy Elements',...
        'Callback', {@(src,evt) obj.toggleExplicithierarchyelements},...
        'Checked',iif(obj.explicithierarchyelements,'on','off'));
    obj.uigroup.addChilds(obj.gmenu_explicithierarchyelements);
    
    % help menu
    obj.gmenu_help = uimenu(menus.Help,...
        'Label','Help Webpage',...
        'Callback', {@(src,evt) obj.helpPressed});
    obj.uigroup.addChilds(obj.gmenu_help);

    % toolbar buttons
    obj.ghlevelpanel = uibuttongroup(toolpanel,...
        'Title','Level',...
        'Position',[0,0,0.2,1]);
    obj.uigroup.addChilds(obj.ghlevelpanel);
    obj.ghlevellist = uicontrol(obj.ghlevelpanel,...
        'Style','listbox',...
        'String',{},...
        'Min',0,'Max',1,...
        'Callback', {@(src,evt) obj.hlevellistPressed},...
        'Tag','elementselect',...
        'Units','normalized',...
        'Position',[0,0,1,1]);
    obj.uigroup.addChilds(obj.ghlevellist);
    
    obj.gstatustext = uicontrol(mainpanel,...
       'Style', 'text',...
       'FontSize', 12,...
       'HorizontalAlignment','left',...
       'BackgroundColor','white',...
       'String', '',... %replace something with the text you want
       'Units', 'pixels',...
       'Position', [5,0,200,25],...
       'Visible', 'off');
    obj.uigroup.addChilds(obj.gstatustext);
    
    % event listeners
    obj.selelmindsListener = obj.addlistener('selelminds','PostSet',@(src,evt) obj.selelmindsChanged);
    obj.selrelindsListener = obj.addlistener('selrelinds','PostSet',@(src,evt) obj.selrelindsChanged);
    obj.selgroupindsListener = obj.addlistener('selgroupinds','PostSet',@(src,evt) obj.selgroupindsChanged);
    obj.selinstgroupindsListener = obj.addlistener('selinstgroupinds','PostSet',@(src,evt) obj.selinstgroupindsChanged);
    obj.selhlevelListener = obj.addlistener('selhlevel','PostSet',@(src,evt) obj.selhlevelChanged);
    obj.openedhelmindListener = obj.addlistener('openedhelmind','PostSet',@(src,evt) obj.openedhelmindChanged);
    obj.toolListener = obj.addlistener('tool','PostSet',@(src,evt) obj.toolChanged);
    
    obj.setPattern(PatternGraph);
    
    startfilename = '../data/userstudy/task1_pattern.mat';

    if exist(startfilename,'file')
        obj.loadPattern(startfilename);
    else
        warning('Pattern not found!');
    end
    
    obj.hide;
end

function delete(obj)
    obj.hide();
    
    obj.clear();
    
    delete(obj.selelmindsListener(isvalid(obj.selelmindsListener)));
    delete(obj.selrelindsListener(isvalid(obj.selrelindsListener)));
    delete(obj.selgroupindsListener(isvalid(obj.selgroupindsListener)));
    delete(obj.selinstgroupindsListener(isvalid(obj.selinstgroupindsListener)));
    delete(obj.selhlevelListener(isvalid(obj.selhlevelListener)));
    delete(obj.openedhelmindListener(isvalid(obj.openedhelmindListener)));
    delete(obj.toolListener(isvalid(obj.toolListener)));
    
    delete(obj.uigroup(isvalid(obj.uigroup)));
end

function clear(obj)
    obj.clearPattern;
    
    obj.tool = '';
end

function clearPattern(obj)
    obj.clearPatternedits;
    
    obj.selelminds = zeros(1,0);
    obj.selrelinds = zeros(1,0);
    obj.selgroupinds = zeros(1,0);
    obj.selinstgroupinds = zeros(1,0);
    obj.selhlevel = 0;
    obj.openedhelmind = zeros(1,0);
    obj.p_pgraphlayout = nan(2,0);
    
    obj.fixedelminds = zeros(1,0);
    
    delete(obj.pgraph(isvalid(obj.pgraph)));
    obj.pgraph = PatternGraph.empty;
    delete(obj.origpgraph(isvalid(obj.origpgraph)));
    obj.origpgraph = PatternGraph.empty;
end

function clearPatternedits(obj)
    obj.dragelmpose = zeros(6,0);
    obj.dragelminds = zeros(1,0);
    obj.dragrelinds = zeros(1,0);
    obj.dragchangedrelinds = zeros(1,0);
    
    obj.changedrelinds = zeros(1,0);
    obj.changedelminds = zeros(1,0);
    obj.lastchangedelminds = zeros(1,0);
    obj.linkedelminds = zeros(1,0);
end

% order returned is [elmrels, metarels]
function val = get.pgraphlayout(obj)
    if any(isnan(obj.p_pgraphlayout(1,:)))
        obj.updatePGraphlayout(find(isnan(obj.p_pgraphlayout(1,:)))); %#ok<FNDSB>
    end
    
    val = obj.p_pgraphlayout;
end

function setFixedelminds(obj,fixedelminds)
    obj.fixedelminds = fixedelminds;
    if obj.elementsVisible
        obj.showElements(fixedelminds);
    end
end

function updatePGraphlayout(obj,varargin)
    if not(isempty(obj.pgraph)) && not(isempty(obj.pgraph.adjacency))
    
        obj.p_pgraphlayout = PatternWidget.updatePGraphlayout_s(...
            obj.pgraph.adjacency,...
            [obj.pgraph.elms.position],...
            numel(obj.pgraph.elmrels),...
            numel(obj.pgraph.metarels),...
            obj.p_pgraphlayout,varargin{:});
    else
        obj.p_pgraphlayout = nan(2,0);
    end
end

function helpPressed(obj) %#ok<MANU>
    web('https://bitbucket.org/paulguerrero/patex');
end

function showStatustext(obj,text)
    obj.gstatustext.Visible = 'on';
    obj.gstatustext.String = text;
end

function hideStatustext(obj)
    obj.gstatustext.Visible = 'off';
end

end

methods(Static)

function layout = updatePGraphlayout_s(adjacency,elmpositions,nelmrels,nmetarels,layout,nodeinds)

    nnodes = size(adjacency,1);

    if nargin < 6
        nodeinds = 1:nnodes;
    end

    if isempty(nodeinds)
        return;
    end

    if size(layout,2) ~= size(adjacency,1)
        layout = nan(2,size(adjacency,1));
    end

    nelms = size(elmpositions,2);

    % also queue descendants that have not been computed yet
    descendantinds = PatternGraph.descendants_s(adjacency,nodeinds,find(isnan(layout(1,:)))); %#ok<FNDSB>
    queuedmask = false(1,nnodes);
    queuedmask([nodeinds,descendantinds]) = true;

    elmrelsrcind = PatternGraph.elmrelsrcind_s(adjacency,nelms,nelmrels);
    elmreltgtind = PatternGraph.elmreltgtind_s(adjacency,nelms,nelmrels);
    metarelsrcind = PatternGraph.metarelsrcind_s(adjacency,nelms,nelmrels,nmetarels);
    metareltgtind = PatternGraph.metareltgtind_s(adjacency,nelms,nelmrels,nmetarels);

    srcind = [zeros(1,nelms),elmrelsrcind,metarelsrcind];
    tgtind = [zeros(1,nelms),elmreltgtind,metareltgtind];

    pos = layout;

    % get missing element positions
    elminds = 1:size(elmpositions,2);
    pos(:,elminds(queuedmask(elminds))) = elmpositions(:,queuedmask(elminds));
    queuedmask(elminds(queuedmask(elminds))) = false;

    while(any(queuedmask))
        activemask = srcind > 0 & tgtind > 0; % has source and target
        activemask(activemask) = ...
            queuedmask(activemask) & ...
            not(queuedmask(srcind(activemask))) & ...
            not(queuedmask(tgtind(activemask)));

        if not(any(activemask))
            error('The pattern graph may have cycles or disconnected components.');
        end
        pos(:,activemask) = (pos(:,srcind(activemask)) +  pos(:,tgtind(activemask))) .* 0.5;
        queuedmask(activemask) = false;

        activeinds = find(activemask);

        % find groups of relationships with visualizations that would
        % overlap (if they have same childs)
        % sort because relationships are also overlap if srcind and tgtind are switched            
        descendantinds = sort([srcind(activeinds);tgtind(activeinds)],1);
        [~,~,ind] = unique(descendantinds','rows');

        overlapgroups = array2cell_mex(activeinds,ind');

        for i=1:numel(overlapgroups)

            if numel(overlapgroups{i}) < 2
                continue;
            end

            sind = srcind(overlapgroups{i}(1));
            tind = tgtind(overlapgroups{i}(1));

            if any(sind==0 | tind == 0)
                continue; % is element vertex
            end

            spos = pos(:,sind);
            tpos = pos(:,tind);

            orthdir = zeros(2,1);
            [dir,dist] = cart2pol(tpos(1)-spos(1),tpos(2)-spos(2));
            [orthdir(1),orthdir(2)] = pol2cart(dir+pi/2,dist*0.1);

            for j=1:numel(overlapgroups{i})
                pos(:,overlapgroups{i}(j)) = pos(:,overlapgroups{i}(j)) + ...
                    orthdir .* ceil((j-1)/2) .* (-1).^j;
            end
        end
    end

    layout = pos;
end

end

methods

function loadPattern(obj,filename)
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastpatternloadname))
            loadname = obj.lastpatternloadname;
        elseif not(isempty(obj.lastpatternsavename))
            loadname = obj.lastpatternsavename;
        else
            loadname = 'tempscene.svg';
        end

        [fname,pathname,~] = ...
            uigetfile({...
            '*.mat','MAT file (*.mat)';...
            '*.svg','Scalable Vector Graphics (*.svg)'},...
            'Load Pattern',loadname);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    obj.notify('patternpreloaded');
    
    disp('importing pattern...');
    [~,~,fext] = fileparts(filename);
    if strcmp(fext,'.svg')
        tempscene = SceneImporter.importScene(filename,'format','svg','createpolygonsets',true);
                
        if isempty(tempscene.groups)
            warning('Scene does not have groups.');
        elseif numel(tempscene.groups) > 1
            warning('Scene has multiple groups, using the first one.');
        end

        elms = tempscene.removeElements;
        delete(tempscene(isvalid(tempscene)));

        for i=1:numel(elms)
            if not(isa(elms,'ScenePolygonset'))
                elms(i) = ScenePolygonset(elms(i));
            end

            % define size to be uniform (otherwise information gets lost when transforming to uniform size pose in solver)
            elmrad = elms(i).radius;
            elms(i).defineSize([elmrad;elmrad]);
        end

        patterngraph = PatternGraph;
        patterngraph.addElements(elms);
        
        obj.setPattern(patterngraph);
        
        delete(tempscene(isvalid(tempscene)));
    elseif strcmp(fext,'.mat')
        s = load(filename);
        obj.setPattern(s.patterngraph);
    end
    
    obj.lastpatternloadname = filename;
    disp('done');
    
    obj.notify('patternpostloaded');
end

function savePattern(obj,filename)

    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastpatternsavename))
            savename = obj.lastpatternsavename;
        elseif not(isempty(obj.lastpatternloadname))
            savename = obj.lastpatternloadname;
        else
            savename = 'tempscene.svg';
        end
        
        [fname,pathname,~] = ...
            uiputfile({
            '*.mat','MAT file (*.mat)'},...
            'Save Pattern',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    disp('exporting pattern...');
    obj.lastpatternsavename = filename;
    
    patterngraph = obj.pgraph.clone;
%     patterngraph.elms.setScenegroup(SceneGroup.empty);

    save(filename,'patterngraph','-v7.3');
    
    elms = patterngraph.elms;
    delete(patterngraph(isvalid(patterngraph)));
    delete(elms(isvalid(elms)));
    
    disp('done');
end

function transformElements(obj,elminds,newpose)
    obj.elementPressed(elminds)
    
    obj.pgraph.transformElements(obj.dragelminds,newpose,true);
    
    obj.elementReleased;
    
    obj.selelminds = zeros(1,0);

    if obj.elementsVisible
        obj.showElements(elminds);
    end
    if obj.relationshipsVisible
        obj.showRelationships;
    end

    obj.updateMainaxes;
end

function elementPressed(obj,elminds)
    obj.lastchangedelminds = zeros(1,0);
    if not(isempty(elminds))
        obj.dragelminds = elminds;
        obj.dragelmpose = [obj.pgraph.elms(elminds).pose2D];
        obj.dragrelinds = obj.pgraph.node2relinds(obj.pgraph.ancestors(elminds));
        obj.dragchangedrelinds = zeros(1,0);
    end
end

function elementMoved(obj)

    if not(isempty(obj.dragelminds))
    
        if obj.relationshipsVisible
            % recompute relationships 
            obj.pgraph.recomputeRelationships(obj.dragrelinds);

            % which element- and meta-relationships changed
            rels = [obj.pgraph.elmrels,obj.pgraph.metarels];
            if not(isempty(rels)) && not(isempty(obj.dragrelinds))
                newvals = [rels(obj.dragrelinds).value];
                relcircular = [rels(obj.dragrelinds).iscircular];
                origrels = [obj.origpgraph.elmrels,obj.origpgraph.metarels];
                origrelvals = [origrels.value];
                origrelvars = [origrels.variance];
                residual = origrelvals(obj.dragrelinds) - newvals;
                if any(relcircular)
                    residual(relcircular) = smod(residual(relcircular),-pi,pi);
                end
                obj.dragchangedrelinds = obj.dragrelinds(...
                    (residual).^2 ./ origrelvars(obj.dragrelinds) > ...
                    obj.synctolerance);
            else
                obj.dragchangedrelinds = zeros(1,0);
            end

            % update the pattern graph visualization of nodes that have changed
            elmreloffset = numel(obj.pgraph.elms);
            changednodeinds = [obj.dragelminds,obj.dragrelinds+elmreloffset];
            obj.p_pgraphlayout(:,changednodeinds) = nan;
            obj.updatePGraphlayout(changednodeinds);
        
            obj.showRelationships;
        end
    end
end

function elementReleased(obj)
    
    if not(isempty(obj.dragelminds))
        
        % recompute relationships 
        obj.pgraph.recomputeRelationships(obj.dragrelinds);

        % which element- and meta-relationships changed
        rels = [obj.pgraph.elmrels,obj.pgraph.metarels];
        if not(isempty(rels)) && not(isempty(obj.dragrelinds))
            newvals = [rels(obj.dragrelinds).value];
            relcircular = [rels(obj.dragrelinds).iscircular];
            origrels = [obj.origpgraph.elmrels,obj.origpgraph.metarels];
            origrelvals = [origrels.value];
            origrelvars = [origrels.variance];
            residual = origrelvals(obj.dragrelinds) - newvals;
            if any(relcircular)
                residual(relcircular) = smod(residual(relcircular),-pi,pi);
            end
            obj.dragchangedrelinds = obj.dragrelinds(...
                (residual).^2 ./ origrelvars(obj.dragrelinds) > ...
                obj.synctolerance);
        else
            obj.dragchangedrelinds = zeros(1,0);
        end

        % update the pattern graph visualization of nodes that have changed
        elmreloffset = numel(obj.pgraph.elms);
        changednodeinds = [obj.dragelminds,obj.dragrelinds+elmreloffset];
        obj.p_pgraphlayout(:,changednodeinds) = nan;
        obj.updatePGraphlayout(changednodeinds);

        % get changed elements
        elms = obj.pgraph.elms(obj.dragelminds);
        origelms = obj.origpgraph.elms(obj.dragelminds);
        posediff = [origelms.pose2D] - [elms.pose2D];
        posediff(3:4,:) = smod(posediff(3:4,:),-pi,pi);
        dragelmchangedmask = any(...
            bsxfun(@rdivide,posediff.^2,obj.origpgraph.elmposevar) > obj.synctolerance,...
            1);
        
        mask = ismember(obj.dragelminds(dragelmchangedmask),obj.linkedelminds);
        if any(mask)
            warning('Linked elements have been changed, ignoring their change.');
            dragelmchangedmask(mask) = false;
        end
        
        obj.changedelminds = unique([obj.changedelminds,obj.dragelminds(dragelmchangedmask)]);
        
        delmdescendants = obj.pgraph.hierarchyDescendants(obj.dragelminds(dragelmchangedmask) + obj.pgraph.elmoffset) - obj.pgraph.elmoffset;
        delmancestors = obj.pgraph.hierarchyAncestors(obj.dragelminds(dragelmchangedmask) + obj.pgraph.elmoffset) - obj.pgraph.elmoffset;
        delmdescendants(delmdescendants < 1 | delmdescendants > numel(obj.pgraph.elms)) = [];
        
        
        obj.linkedelminds = unique([obj.linkedelminds,delmdescendants,delmancestors]);
        
        % cannot change a hierarchy descendant or ancestor of an element that has already
        % been changed
        
        obj.lastchangedelminds = obj.dragelminds(dragelmchangedmask);
        
        % get changed relationship
        rels = [obj.pgraph.elmrels,obj.pgraph.metarels];
        origrels = [obj.origpgraph.elmrels,obj.origpgraph.metarels];
        origrelvals = [origrels.value];
        origrelvars = [origrels.variance];
        obj.changedrelinds = find(...
            (origrelvals - [rels.value]).^2 ./ origrelvars > obj.synctolerance);
    end
    
    obj.dragelmpose = zeros(6,0);
    obj.dragelminds = zeros(1,0);
    obj.dragrelinds = zeros(1,0);
    obj.dragchangedrelinds = zeros(1,0);
end

function unblock(obj)
    obj.blockcallbacks = false;
    obj.navwidget.unblock;
end

function mousePressed(obj,src,evt)

    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;
    
    obj.selpanel = gparent(obj.fig.CurrentObject,'uipanel',true);
    
    obj.mousedown = true;

    obj.mousepos = mouseposOnPlane(obj.axes);
    obj.clickedpos = obj.mousepos;
    obj.mousepos_screen = mouseScreenpos;
    obj.clickedpos_screen = obj.mousepos_screen;

    
    if obj.selpanel == obj.mainpanel
        if obj.navwidget.zoommode || ...
           obj.navwidget.panmode || ...
           obj.navwidget.orbitmode

            obj.navwidget.mousePressed(src,evt);
        elseif strcmp(obj.tool,'elementselect')
            
            if strcmp(src.SelectionType,'open')
                % also open linked elements (some descendents may still be
                % editable if they are siblings of the changed elements)
                elmind = PatternWidget.closestElement(...
                    obj.clickedpos(1:2),obj.pgraph,obj.selhlevel,obj.openedhelmind,zeros(1,0),obj.explicithierarchyelements);
                
                if obj.pgraph.hlevel(elmind + obj.pgraph.elmoffset) > 0
                    obj.openedhelmind = elmind;

                    elmind = PatternWidget.closestElement(...
                        obj.clickedpos(1:2),obj.pgraph,obj.selhlevel,obj.openedhelmind,obj.linkedelminds,obj.explicithierarchyelements);

                    obj.selelminds = elmind;
                    obj.selrelinds = zeros(1,0);
                    obj.selgroupinds = zeros(1,0);
                    obj.selinstgroupinds = zeros(1,0);
                end
            else
                elmind = PatternWidget.closestElement(...
                    obj.clickedpos(1:2),obj.pgraph,obj.selhlevel,obj.openedhelmind,obj.linkedelminds,obj.explicithierarchyelements);
                
                obj.selelminds = selectMultiple(...
                    obj.selelminds,elmind,src.SelectionType);

                obj.selrelinds = zeros(1,0);
                obj.selgroupinds = zeros(1,0);
                obj.selinstgroupinds = zeros(1,0);
                
                obj.elementPressed(elmind);
            end
            
        elseif strcmp(obj.tool,'relselect')
            relind = PatternWidget.closestRelationship(...
                obj.clickedpos(1:2),obj.pgraph,obj.pgraphlayout,obj.selhlevel);

            obj.selrelinds = selectMultiple(...
                obj.selrelinds,relind,src.SelectionType);
            obj.selelminds = zeros(1,0);
            obj.selgroupinds = zeros(1,0);
            obj.selinstgroupinds = zeros(1,0);
        elseif strcmp(obj.tool,'groupselect')
            
            obj.selrelinds = zeros(1,0);
            obj.selelminds = zeros(1,0);
            obj.selinstgroupinds = zeros(1,0);
            
            [relind,reldist] = PatternWidget.closestRelationship(...
                obj.clickedpos(1:2),obj.pgraph,obj.pgraphlayout,obj.selhlevel);
            [elmind,elmdist] = PatternWidget.closestElement(...
                obj.clickedpos(1:2),obj.pgraph,obj.selhlevel,obj.openedhelmind,obj.linkedelminds,obj.explicithierarchyelements);
            
            if reldist < elmdist
                offset = numel(obj.pgraph.elms);
                relgroupinds = find(obj.pgraph.groups(relind+offset,:));
                
                if isempty(relgroupinds)
                    obj.selgroupinds = zeros(1,0);
                    
                else
                    [~,ind] = ismember(obj.selgroupinds,relgroupinds);
                    if ind > 0
                        obj.selgroupinds = relgroupinds(mod(ind-1+1,numel(relgroupinds))+1);
                    else
                        obj.selgroupinds = relgroupinds(1);
                    end
                end
            else
                elmgroupinds = find(obj.pgraph.groups(elmind,:));
                
                if isempty(elmgroupinds)
                    obj.selgroupinds = zeros(1,0);
                else
                    [~,ind] = ismember(obj.selgroupinds,elmgroupinds);
                    if ind > 0
                        obj.selgroupinds = elmgroupinds(mod(ind-1+1,numel(elmgroupinds))+1);
                    else
                        obj.selgroupinds = elmgroupinds(1);
                    end
                end
            end
        elseif strcmp(obj.tool,'instgroupselect') 
            
            obj.selrelinds = zeros(1,0);
            obj.selelminds = zeros(1,0);
            obj.selgroupinds = zeros(1,0);
            
            [relind,reldist] = PatternWidget.closestRelationship(...
                obj.clickedpos(1:2),obj.pgraph,obj.pgraphlayout,obj.selhlevel);
            [elmind,elmdist] = PatternWidget.closestElement(...
                obj.clickedpos(1:2),obj.pgraph,obj.selhlevel,obj.openedhelmind,obj.linkedelminds,obj.explicithierarchyelements);
            
            if reldist < elmdist
                offset = numel(obj.pgraph.elms);
                relinstgroupinds = find(obj.pgraph.instgroups(relind+offset,:));
                
                if isempty(relinstgroupinds)
                    obj.selinstgroupinds = zeros(1,0);
                    
                else
                    [~,ind] = ismember(obj.selinstgroupinds,relinstgroupinds);
                    if ind > 0
                        obj.selinstgroupinds = relinstgroupinds(mod(ind-1+1,numel(relinstgroupinds))+1);
                    else
                        obj.selinstgroupinds = relinstgroupinds(1);
                    end
                end
            else
                elminstgroupinds = find(obj.pgraph.instgroups(elmind,:));
                
                if isempty(elminstgroupinds)
                    obj.selinstgroupinds = zeros(1,0);
                else
                    [~,ind] = ismember(obj.selinstgroupinds,elminstgroupinds);
                    if ind > 0
                        obj.selinstgroupinds = elminstgroupinds(mod(ind-1+1,numel(elminstgroupinds))+1);
                    else
                        obj.selinstgroupinds = elminstgroupinds(1);
                    end
                end
            end
        end
    end

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt)
    
    if obj.mousedown

        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;

        obj.mousepos = mouseposOnPlane(obj.axes);
        obj.mousepos_screen = mouseScreenpos;
        
        if obj.selpanel == obj.mainpanel
            if obj.navwidget.zoommode || ...
               obj.navwidget.panmode || ...
               obj.navwidget.orbitmode
                % camera navigation

                obj.navwidget.mouseMoved(src,evt);
            elseif strcmp(obj.tool,'elementselect')

                if not(isempty(obj.dragelminds)) && (obj.draggingelm || norm(obj.mousepos_screen-obj.clickedpos_screen) > 15) && strcmp(src.SelectionType,'normal')

                    obj.draggingelm = true;

                    if strcmp(obj.subtool,'move')
                        dragvec = obj.mousepos(1:2)-obj.clickedpos(1:2);

                        if obj.gridsnapping
                            snapdist = pi/4;
                            [dragdir,draglen] = cart2pol(dragvec(1),dragvec(2));
                            dragdir = smod(round(dragdir/snapdist)*snapdist,-pi,pi);
                            [dragvec(1),dragvec(2)] = pol2cart(dragdir,draglen);
                        end
                        
                        newpose = obj.pgraph.elms(obj.dragelminds).pose2D;
                        newpose(1:2,:) = bsxfun(@plus,obj.dragelmpose(1:2,:),dragvec);
                        obj.pgraph.transformElements(obj.dragelminds,newpose,true);
                        

                        
                    elseif strcmp(obj.subtool,'rotate')
                        clicked_centervec = obj.clickedpos(1:2) - obj.dragelmpose(1:2,end);
                        mouse_centervec = obj.mousepos(1:2) - obj.pgraph.elms(obj.dragelminds).position;
                        clicked_angle = cart2pol(clicked_centervec(1),clicked_centervec(2));
                        mouse_angle = cart2pol(mouse_centervec(1),mouse_centervec(2));
                        
                        orientationdelta = smod(mouse_angle-clicked_angle,-pi,pi);
                        
                        if obj.gridsnapping
                            snapdist = pi/8;
                            orientationdelta = smod(round(orientationdelta/snapdist)*snapdist,-pi,pi);
                        end
                        
                        neworientation = obj.dragelmpose(3,:) + orientationdelta;
                        
                        newpose = obj.pgraph.elms(obj.dragelminds).pose2D;
                        newpose(3,:) = neworientation;
                        obj.pgraph.transformElements(obj.dragelminds,newpose,true);

                    elseif strcmp(obj.subtool,'scale')
                        clicked_centervec = obj.clickedpos(1:2) - obj.dragelmpose(1:2,end);
                        mouse_centervec = obj.mousepos(1:2) - obj.pgraph.elms(obj.dragelminds).position;
                        
                        clicked_dist = sqrt(sum(clicked_centervec.^2,1));
                        mouse_dist = sqrt(sum(mouse_centervec.^2,1));
                        
                        if clicked_dist > 0.001
                        
                            newsize = max(0.001,obj.dragelmpose([4,5],:) * (mouse_dist/clicked_dist));

                            newpose = obj.pgraph.elms(obj.dragelminds).pose2D;
                            newpose(4,:) = newsize(1);
                            newpose(5,:) = newsize(2);
                            obj.pgraph.transformElements(obj.dragelminds,newpose,true);
                        end

                    else
                        error('Unknown subtool for elementselect tool.');
                    end
                    
                    obj.elementMoved;
                    
                    if obj.elmdragvisVisible
                        obj.showElmdragvis;
                        obj.showElements(obj.dragelminds);
                    end

                else
                    obj.draggingelm = false;
                    obj.hideElmdragvis;
                end
                % do nothing
            elseif strcmp(obj.tool,'relselect')
                % do nothing 
            elseif strcmp(obj.tool,'groupselect')
                % do nothing
            end
        end
        
%         % execute queued callbacks before unblocking, so they get canceled
%         % (BusyAction or Interruptible does not seem to do anything, so I
%         % have to do it this way)
            
        obj.blockcallbacks = false;
    end
end

function mouseReleased(obj,src,evt)

    obj.mousedown = false;
    
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    if obj.selpanel == obj.mainpanel
        if obj.navwidget.zoommode || ...
           obj.navwidget.panmode || ...
           obj.navwidget.orbitmode

            obj.navwidget.mouseReleased(src,evt);
        elseif strcmp(obj.tool,'elementselect')
            obj.elementReleased;
            
            if obj.draggingelm

                obj.draggingelm = false;                
                
                obj.hideElmdragvis;
                
                dirtyviselminds = [obj.selelminds,obj.pgraph.hierarchyDescendants(obj.selelminds+obj.pgraph.elmoffset)-obj.pgraph.elmoffset];
                dirtyviselminds(dirtyviselminds < 1 | dirtyviselminds > numel(obj.pgraph.elms)) = [];
                
                obj.selelminds = zeros(1,0);
                
                if obj.elementsVisible
                    obj.showElements(dirtyviselminds);
                end
                if obj.relationshipsVisible
                    obj.showRelationships;
                end
                
                obj.updateMainaxes;
            end
            
            % do nothing
        elseif strcmp(obj.tool,'relselect')
            % do nothing
        elseif strcmp(obj.tool,'groupselect')
            % do nothing
        end
    end
    
        
    % execute queued callbacks before unblocking, so they get canceled
    % (BusyAction or Interruptible does not seem to do anything, so I
    % have to do it this way)
    drawnow;

    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt) %#ok<INUSL>
    
    % global keys
    if strcmp(evt.Key,'1')
        if obj.navwidget.panmode || obj.navwidget.zoommode || obj.navwidget.orbitmode
            obj.navwidget.setPanmode(false);
        else
            obj.navwidget.setPanmode(true);
        end
    elseif strcmp(evt.Key,'c')
        resetCamera(obj.axes);
    elseif strcmp(evt.Key,'add')
        if not(isempty(obj.pgraph))
            maxhlevel = max(obj.pgraph.hlevel);
            obj.selhlevel = min(maxhlevel,obj.selhlevel+1);
        end
    elseif strcmp(evt.Key,'subtract')
        if not(isempty(obj.pgraph))
            obj.selhlevel = max(0,obj.selhlevel-1);
        end
    end
    
    if strcmp(obj.tool,'elementselect')
        if strcmp(evt.Key,'escape')
            if obj.draggingelm
                % move element back to original position
                obj.pgraph.transformElements(obj.dragelminds,obj.dragelmpose,true);
                
                delminds = obj.dragelminds;
                
                obj.elementMoved;

                obj.elementReleased;

                obj.draggingelm = false;

                obj.hideElmdragvis;
                if obj.elementsVisible
                    obj.showElements(delminds);
                end
         
                obj.updateMainaxes;
            elseif not(isempty(obj.openedhelmind))
                parentelm = find(obj.pgraph.hierarchy(:,obj.openedhelmind),1,'first');
                if isempty(parentelm)
                    obj.openedhelmind = zeros(1,0);
                else
                    obj.openedhelmind = parentelm;
                end
                
                obj.selelminds = zeros(1,0);
                obj.selrelinds = zeros(1,0);
                obj.selgroupinds = zeros(1,0);
                obj.selinstgroupinds = zeros(1,0);
            end
        elseif strcmp(evt.Key,'r')
            obj.subtool = 'rotate';
        elseif strcmp(evt.Key,'s')
            obj.subtool = 'scale';
        elseif strcmp(evt.Key,'x')
            obj.gridsnapping = true;
        end
    end
end

function keyReleased(obj,src,evt) %#ok<INUSL>
    
    if strcmp(obj.tool,'elementselect')
        if strcmp(evt.Key,'r') || strcmp(evt.Key,'s')
            obj.subtool = 'move';
        elseif strcmp(evt.Key,'x')
            obj.gridsnapping = false;
        end
    end
end

function setPattern(obj,pgraph,resetcam)
    if nargin < 3
        resetcam = true;
    end
    
    if numel(pgraph) ~= numel(obj.pgraph) || not(all(pgraph == obj.pgraph))
        obj.clearPattern;

        obj.pgraph = pgraph;

        obj.pgraphModified;    

        obj.updateMainaxes;

        if resetcam
            resetCamera(obj.axes);
        end

        obj.showAxescontents;
    end
end

function pgraphModified(obj)
    
    obj.clearPatternedits;
    
    obj.updateOrigpgraph;
    
    if not(isempty(obj.pgraph)) && isvalid(obj.pgraph)
        obj.p_pgraphlayout = nan(2,size(obj.pgraph.adjacency,1));

        maxlevel = max(obj.pgraph.hlevel);
        if not(isempty(maxlevel))
            obj.ghlevellist.String = cellfun(@(x) sprintf('%d',x),num2cell(0:maxlevel),'UniformOutput',false);
            if not(isempty(obj.selhlevel))
                obj.selhlevel = min(obj.selhlevel,maxlevel);
            else
                obj.selhlevel = 0;
            end
        else
            obj.selhlevel = 0;
        end
    end
    
    obj.selelminds = zeros(1,0);
    obj.selrelinds = zeros(1,0);
    obj.selgroupinds = zeros(1,0);
end

function updateOrigpgraph(obj)
    delete(obj.origpgraph(isvalid(obj.origpgraph)));
    obj.origpgraph = obj.pgraph;
    if not(isempty(obj.origpgraph))
        obj.origpgraph = obj.origpgraph.clone;
    end
end

function selelmindsChanged(obj)
    
    if not(isempty(obj.selelminds)) && not(isempty(obj.pgraph))
        disp(['elminds ',num2str(obj.selelminds)]);
    end
    
    changedselelminds = setxor(obj.oldselelminds,obj.selelminds);
    obj.oldselelminds = obj.selelminds;
    obj.showElements(changedselelminds);
end

function selrelindsChanged(obj)
    
    if not(isempty(obj.selrelinds)) && not(isempty(obj.pgraph))
        disp(['relinds ',num2str(obj.selrelinds)]);
        rels = [obj.pgraph.elmrels,obj.pgraph.metarels];
        disp([{rels(obj.selrelinds).type};{rels(obj.selrelinds).value}]');
    end
    
    changedselrelinds = setxor(obj.oldselrelinds,obj.selrelinds);
    obj.oldselrelinds = obj.selrelinds;
    if obj.relationshipsVisible
        obj.showRelationships(changedselrelinds);
    end
end

function selgroupindsChanged(obj)
    changedselgroupinds = setxor(obj.oldselgroupinds,obj.selgroupinds);
    obj.oldselgroupinds = obj.selgroupinds;
    
    nodeinds = find(any(obj.pgraph.groups(:,changedselgroupinds),2));
    changedselelminds = nodeinds - obj.pgraph.elmoffset;
    changedselelminds(changedselelminds < 1 | changedselelminds > numel(obj.pgraph.elms)) = [];
    changeselrelinds = obj.pgraph.node2relinds(nodeinds);
    changeselrelinds(isnan(changeselrelinds)) = [];
    
    if obj.elementsVisible
        obj.showElements(changedselelminds);
    end
    if obj.relationshipsVisible
        obj.showRelationships(changeselrelinds);
    end
    
    disp(['groups: ',num2str(obj.selgroupinds)]);
end

function selinstgroupindsChanged(obj)
    changedselinstgroupinds = setxor(obj.oldselinstgroupinds,obj.selinstgroupinds);
    obj.oldselinstgroupinds = obj.selinstgroupinds;
    
    nodeinds = find(any(obj.pgraph.instgroups(:,changedselinstgroupinds),2));
    changedselelminds = nodeinds - obj.pgraph.elmoffset;
    changedselelminds(changedselelminds < 1 | changedselelminds > numel(obj.pgraph.elms)) = [];
    changeselrelinds = obj.pgraph.node2relinds(nodeinds);
    changeselrelinds(isnan(changeselrelinds)) = [];
    
    if obj.elementsVisible
        obj.showElements(changedselelminds);
    end
    if obj.relationshipsVisible
        obj.showRelationships(changeselrelinds);
    end
    
    disp(['instance groups: ',num2str(obj.selinstgroupinds)]);
end

function toolChanged(obj)
    if strcmp(obj.tool,'elementselect')
        obj.subtool = 'move';
        obj.gmenu_elementselect.Checked = 'on';
        if not(obj.elementsVisible)
            obj.toggleElementsVisible;
        end
    else
        obj.gmenu_elementselect.Checked = 'off';
    end
    if strcmp(obj.tool,'relselect')
        obj.subtool = '';
        obj.gmenu_relselect.Checked = 'on';

        if not(obj.relationshipsVisible)
            obj.toggleRelationshipsVisible;
        end
    else
        obj.gmenu_relselect.Checked = 'off';
    end
    if strcmp(obj.tool,'groupselect')
        obj.subtool = '';
        obj.gmenu_groupselect.Checked = 'on';

        if not(obj.relationshipsVisible)
            obj.toggleRelationshipsVisible;
        end
    else
        obj.gmenu_groupselect.Checked = 'off';
    end
    if strcmp(obj.tool,'instgroupselect')
        obj.subtool = '';
        obj.gmenu_instgroupselect.Checked = 'on';

        if not(obj.relationshipsVisible)
            obj.toggleRelationshipsVisible;
        end
    else
        obj.gmenu_instgroupselect.Checked = 'off';
    end
end

function selectElementsPressed(obj)
    if strcmp(obj.tool,'elementselect')
        obj.tool = '';
    else
        obj.tool = 'elementselect';
    end
end

function selectRelationshipsPressed(obj)
    if strcmp(obj.tool,'relselect')
        obj.tool = '';
    else
        obj.tool = 'relselect';
    end
end

function selectGroupsPressed(obj)
    if strcmp(obj.tool,'groupselect')
        obj.tool = '';
    else
        obj.tool = 'groupselect';
    end
end

function selectInstgroupsPressed(obj)
    if strcmp(obj.tool,'instgroupselect')
        obj.tool = '';
    else
        obj.tool = 'instgroupselect';
    end
end


function hlevellistPressed(obj)
    obj.selhlevel = obj.ghlevellist.Value-1;
end

function selhlevelChanged(obj)
    if obj.elementsVisible
        obj.showElements;
    end
    if obj.relationshipsVisible
        obj.showRelationships;
    end
    if obj.elementorientationsVisible
        obj.showElementorientations;
    end
    if obj.elementcentersVisible
        obj.showElementcenters;
    end
    if not(isempty(obj.ghlevellist)) && isgraphics(obj.ghlevellist)
        if obj.ghlevellist.Value ~= obj.selhlevel+1;
            obj.ghlevellist.Value = obj.selhlevel+1;
        end
    end
end

function openedhelmindChanged(obj)
    if obj.elementsVisible
        obj.showElements;
    end
end

function toggleElementsVisible(obj)
    obj.elementsVisible = not(obj.elementsVisible);
    if obj.elementsVisible
        set(obj.gmenu_elements,'Checked','on');
        obj.showElements;
    else
        set(obj.gmenu_elements,'Checked','off');
        obj.hideElements;
    end    
end

function toggleRelationshipsVisible(obj)
    obj.relationshipsVisible = not(obj.relationshipsVisible);
    if obj.relationshipsVisible
        set(obj.gmenu_relationships ,'Checked','on');
        obj.showRelationships;
    else
        set(obj.gmenu_relationships ,'Checked','off');
        obj.hideRelationships;
    end
end

function toggleRelationshipdirsVisible(obj)
    obj.relationshipdirsVisible = not(obj.relationshipdirsVisible);
    if obj.relationshipdirsVisible
        set(obj.gmenu_relationshipdirs ,'Checked','on');
        if obj.relationshipsVisible
            obj.showRelationships;
        end
    else
        set(obj.gmenu_relationshipdirs ,'Checked','off');
        if obj.relationshipsVisible
            obj.showRelationships;
        end
    end
end

function toggleElementorientationsVisible(obj)
    obj.elementorientationsVisible = not(obj.elementorientationsVisible);
    if obj.elementorientationsVisible
        set(obj.gmenu_elementorientations,'Checked','on');
        obj.showElementorientations;
    else
        set(obj.gmenu_elementorientations,'Checked','off');
        obj.hideElementorientations;
    end
end

function toggleElementcentersVisible(obj)
    obj.elementcentersVisible = not(obj.elementcentersVisible );
    if obj.elementcentersVisible 
        set(obj.gmenu_elementcenters,'Checked','on');
        obj.showElementcenters;
    else
        set(obj.gmenu_elementcenters,'Checked','off');
        obj.hideElementcenters;
    end
end

function toggleExplicithierarchyelements(obj)
    obj.explicithierarchyelements = not(obj.explicithierarchyelements );
    if obj.explicithierarchyelements 
        set(obj.gmenu_explicithierarchyelements,'Checked','on');
        if obj.elementsVisible
            obj.showElements;
        end
    else
        set(obj.gmenu_explicithierarchyelements,'Checked','off');
        if obj.elementsVisible
            obj.showElements;
        end
    end
end

function toggleElmdragvisVisible(obj)
    obj.elmdragvisVisible = not(obj.elmdragvisVisible);
    if obj.elmdragvisVisible
        obj.showElmdragvis;
        obj.showElements(obj.dragelminds);
    else
        obj.hideElmdragvis;
    end
end

function updateMainaxes(obj)
    if not(isempty(obj.axes)) && isgraphics(obj.axes)
    
        if isempty(obj.pgraph) || isempty(obj.pgraph.elms)
            bbmin = [-1;-1;-1];
            bbmax = [1;1;1];
        else
            fpadding = [0;0;0.05];

            [bbmin,bbmax] = obj.pgraph.elms.boundingbox;
            if size(bbmin,1) == 2
                bbmin = [bbmin;zeros(1,size(bbmin,2))];
                bbmax = [bbmax;zeros(1,size(bbmax,2))];
            end
            bbmin = min(bbmin,[],2);
            bbmax = max(bbmax,[],2);
            bbdiag = sqrt(sum((bbmax-bbmin).^2,1));
            bbmax = bbmax + bbdiag .* fpadding;
            bbmin = bbmin - bbdiag .* fpadding;
        end
        
        set(obj.axes,'XLim',[bbmin(1),bbmax(1)]);
        set(obj.axes,'YLim',[bbmin(2),bbmax(2)]);
        set(obj.axes,'ZLim',[bbmin(3),bbmax(3)]);
    end
end

function showAxescontents(obj)
    if obj.elementsVisible
        obj.showElements;
    end
    
    if obj.relationshipsVisible
        obj.showRelationships;
    end
    
    if obj.elementorientationsVisible
        obj.showElementorientations;
    end
    
    if obj.elementcentersVisible
        obj.showElementcenters;
    end
end

function hideAxescontents(obj)
    obj.hideElements;
    obj.hideRelationships;
end

function setElementblendout(obj,blendout)
    if obj.elmblendout ~= blendout
        if obj.elementsVisible
            
            if obj.elmblendout > 0.999
                obj.elmblendout = blendout;
                obj.showElements;
            else
                for i=1:numel(obj.gelms)
                    obj.gelms(i).EdgeColor = bsxfun(@minus,[1,1,1],...
                        bsxfun(@minus,[1,1,1],obj.gelms(i).EdgeColor) .* ((1-blendout) / (1-obj.elmblendout)));
                end
                obj.elmblendout = blendout;
            end
            
        end
    end
end

function showElements(obj,elminds,overridecolors)
    if not(isempty(obj.pgraph)) && isvalid(obj.pgraph)
        
        if nargin < 2
            elminds = 1:numel(obj.pgraph.elms);
        end
        if nargin < 3
            overridecolors = [];
        end
        
        obj.gelms = PatternWidget.showPgraphElements(...
            obj.gelms,obj.axes,obj.pgraph,elminds,...
            'selelminds',obj.selelminds,...
            'selgroupinds',obj.selgroupinds,...
            'selinstgroupinds',obj.selinstgroupinds,...
            'selhlevel',obj.selhlevel,...
            'openedhelmind',obj.openedhelmind,...
            'changedelminds',obj.changedelminds,...
            'fixedelminds',obj.fixedelminds,...
            'linkedelminds',obj.linkedelminds,...
            'overridecolors',overridecolors,...
            'globalblendoutstrength',obj.elmblendout,...
            'explicithierarchyelements',obj.explicithierarchyelements,...
            'hierarcy_blendoutstrength',obj.settings.blendoutstrength_hierarchy,...
            'color_elements',obj.settings.color_elements,...
            'color_changedelements',obj.settings.color_changedelements,...
            'color_fixedelements',obj.settings.color_fixedelements,...
            'color_linkedelements',obj.settings.color_linkedelements,...
            'color_selectedelements',obj.settings.color_selectedelements,...
            'color_selectedgroups',obj.settings.color_selectedgroups,...
            'linewidth_selectedelements',obj.settings.linewidth_selectedelements);
    else    
        obj.hideElements;
    end
end

function hideElements(obj,elminds)
    if nargin < 2
        elminds = 1:numel(obj.gelms);
    end
    
    obj.gelms = PatternWidget.hidePgraphElements(obj.gelms,elminds);
end

function showRelationships(obj,relinds) %#ok<INUSD>
    if not(isempty(obj.pgraph)) && not(isempty(obj.pgraphlayout))
        
%         if nargin < 2
            relinds = 1:numel(obj.pgraph.rels);
%         end

        % relationships are drawn as a single patch, so no relinds needed
        
        elmrelmask = relinds <= numel(obj.pgraph.elmrels);
        elmrelinds = relinds(elmrelmask);
        metarelmask = ...
            relinds > numel(obj.pgraph.elmrels) & ...
            relinds <= numel(obj.pgraph.elmrels)+numel(obj.pgraph.metarels);
        metarelinds = relinds(metarelmask) - numel(obj.pgraph.elmrels);
        
        offset = numel(obj.pgraph.elms);
        srcpos = obj.pgraphlayout(:,obj.pgraph.elmrelsrcind(elmrelinds));
        tgtpos = obj.pgraphlayout(:,obj.pgraph.elmreltgtind(elmrelinds));
        pos = obj.pgraphlayout(:,offset+elmrelinds);
        
        elmrelverts = reshape([srcpos;pos;tgtpos],2,[]);
        elmrelverts = mat2cell(elmrelverts,2,ones(1,size(pos,2)).*3);
        elmrelcolor = repmat(rgbhex2rgbdec('92b1ff')',1,size(elmrelverts,2));
        
        
        elmreldirverts = reshape(pos + (tgtpos-pos) .* 0.1,2,[]);
        elmreldirverts = mat2cell(elmreldirverts,2,ones(1,size(pos,2)));
        elmreldircolor = repmat(rgbhex2rgbdec('92b1ff')',1,size(elmreldirverts,2));
        
        offset = numel(obj.pgraph.elms) + numel(obj.pgraph.elmrels);
        srcpos = obj.pgraphlayout(:,obj.pgraph.metarelsrcind(metarelinds));
        tgtpos = obj.pgraphlayout(:,obj.pgraph.metareltgtind(metarelinds));
        pos = obj.pgraphlayout(:,offset+metarelinds);
        
        metarelverts = reshape([srcpos;pos;tgtpos],2,[]);
        metarelverts = mat2cell(metarelverts,2,ones(1,size(pos,2)).*3);
        metarelcolor = repmat(rgbhex2rgbdec('4ab231')',1,size(metarelverts,2));
        
        metareldirverts = reshape(pos + (tgtpos-pos) .* 0.1,2,[]);
        metareldirverts = mat2cell(metareldirverts,2,ones(1,size(pos,2)));
        metareldircolor = repmat(rgbhex2rgbdec('4ab231')',1,size(metareldirverts,2));
        
        relinds = [relinds(elmrelmask),relinds(metarelmask)];
        relnodeinds = obj.pgraph.rel2nodeinds(relinds);
        
        verts = [elmrelverts,metarelverts];
        color = [elmrelcolor,metarelcolor];
        dirverts = [elmreldirverts,metareldirverts];
        dircolor = [elmreldircolor,metareldircolor];
        
        if not(isempty(obj.selgroupinds))
            selgrouprelinds = find(obj.pgraph.groups(relnodeinds,obj.selgroupinds(end)));
            color(:,selgrouprelinds) = repmat([1;0.5;0.5],1,numel(selgrouprelinds));
            dircolor(:,selgrouprelinds) = repmat([1;0.5;0.5],1,numel(selgrouprelinds));
        end
        
        if not(isempty(obj.selinstgroupinds))
            selinstgrouprelinds = find(obj.pgraph.instgroups(relnodeinds,obj.selinstgroupinds(end)));
            color(:,selinstgrouprelinds) = repmat([1;0.5;0.5],1,numel(selinstgrouprelinds));
            dircolor(:,selinstgrouprelinds) = repmat([1;0.5;0.5],1,numel(selinstgrouprelinds));
        end
        
        changedrelmask = false(1,numel(obj.pgraph.elmrels)+numel(obj.pgraph.metarels));
        changedrelmask(obj.changedrelinds) = true;
        dragchangedrelmask = false(1,numel(obj.pgraph.elmrels)+numel(obj.pgraph.metarels));
        dragchangedrelmask(obj.dragchangedrelinds) = true;
        changedrelmask(obj.dragrelinds) = dragchangedrelmask(obj.dragrelinds);
        crelinds = find(changedrelmask);
        
        if not(isempty(crelinds))
            offset = numel(obj.pgraph.elms);
                
            groupchangedrelinds = obj.pgraph.groupmembers(crelinds+offset)-offset;
            if not(isempty(groupchangedrelinds))
                color(:,groupchangedrelinds) = repmat(rgbhex2rgbdec('b266b1')',1,numel(groupchangedrelinds));
                dircolor(:,groupchangedrelinds) = repmat(rgbhex2rgbdec('b266b1')',1,numel(groupchangedrelinds));
            end
            mask = ismember(relinds,crelinds);

            color(:,mask) = repmat(rgbhex2rgbdec('b266b1')',1,sum(mask));
            dircolor(:,mask) = repmat(rgbhex2rgbdec('b266b1')',1,sum(mask));
        end
        
        if not(isempty(obj.selrelinds))
            mask = ismember(relinds,obj.selrelinds);
            color(:,mask) = repmat([1;0;0],1,sum(mask));
            dircolor(:,mask) = repmat([1;0;0],1,sum(mask));
        end
        
        color = reshape(repmat(color,3,1),3,[]);
        color = mat2cell(color,3,ones(1,size(verts,2)).*3);
        
        dircolor = reshape(repmat(dircolor,1,1),3,[]);
        dircolor = mat2cell(dircolor,3,ones(1,size(dirverts,2)));
        
        elmrelnodeinds = obj.pgraph.elmreloffset+elmrelinds;
        metarelnodeinds = obj.pgraph.metareloffset+metarelinds;
        hlevelrelmask = [...
            obj.pgraph.hlevel(elmrelnodeinds) == obj.selhlevel,...
            obj.pgraph.hlevel(metarelnodeinds) == obj.selhlevel];
        
        if numel(obj.grels) ~= numel(obj.pgraph.elmrels) + numel(obj.pgraph.metarels)
            delete(obj.grels(isvalid(obj.grels)));
            obj.grels = gobjects(1,numel(obj.pgraph.elmrels) + numel(obj.pgraph.metarels));
        end
        
        obj.grels = showPolygons(...
            {verts(hlevelrelmask)},obj.grels,obj.axes,...
            'fvcolor',{color(hlevelrelmask)},...
            'edgecolor','flat',...
            'facecolor','none',...
            'markersymbol','o',...
            'markerfacecolor','flat');
        
        if obj.relationshipdirsVisible
            obj.greldirs = showPoints(...
                dirverts(hlevelrelmask),obj.greldirs,obj.axes,...
                'fvcolor',dircolor(hlevelrelmask),...
                'markersymbol','s',...
                'markerfacecolor','flat');
        else
            delete(obj.greldirs(isvalid(obj.greldirs)));
            obj.greldirs = gobjects(1,1);
        end
    else
        obj.hideRelationships;
    end
end

function hideRelationships(obj,relinds) %#ok<INUSD>
    
    if nargin < 2
        relinds = 1:numel(obj.grels); %#ok<NASGU>
    end
    
    delete(obj.grels(isvalid(obj.grels)));
    obj.grels = gobjects(1,1);
    delete(obj.greldirs(isvalid(obj.greldirs)));
    obj.greldirs = gobjects(1,1);
end

function showElementorientations(obj)
    if not(isempty(obj.pgraph))
    
        elmhlevel = obj.pgraph.hlevel(obj.pgraph.elmoffset+1:obj.pgraph.elmoffset+numel(obj.pgraph.elms));
        hlvlelms = obj.pgraph.elms(elmhlevel == obj.selhlevel);
        pos = [hlvlelms.position];
        [dir(1,:),dir(2,:)] = pol2cart([hlvlelms.orientation],mean([hlvlelms.size],1).*0.3);
        
        verts = [pos;pos+dir];
        verts = reshape(verts,2,[]);
        verts = mat2cell(verts,2,ones(1,numel(hlvlelms)).*2);

        obj.gelmorientations = showPolygons(...
            {verts},obj.gelmorientations,obj.axes,...
            'edgecolor',rgbhex2rgbdec('b266b1')',...
            'facecolor','none');
    else
        obj.hideElementorientations;
    end
end

function hideElementorientations(obj)
    delete(obj.gelmorientations(isvalid(obj.gelmorientations)));
    obj.gelmorientations = gobjects(1,1);
end

function showElementcenters(obj)
    if not(isempty(obj.pgraph))
    
        elmhlevel = obj.pgraph.hlevel(obj.pgraph.elmoffset+1:obj.pgraph.elmoffset+numel(obj.pgraph.elms));
        hlvlelms = obj.pgraph.elms(elmhlevel == obj.selhlevel);
        
        verts = [hlvlelms.position];

        obj.gelmcenters = showPoints(...
            {verts},obj.gelmcenters,obj.axes,...
            'markersymbol','o',...
            'markerfacecolor',rgbhex2rgbdec('b266b1')');
    else
        obj.hideElementcenters;
    end
end

function hideElementcenters(obj)
    delete(obj.gelmcenters(isvalid(obj.gelmcenters)));
    obj.gelmcenters = gobjects(1,1);
end

function showElmdragvis(obj)
    if not(isempty(obj.dragelminds))
        
        if obj.explicithierarchyelements
            draggedverts = {obj.pgraph.elms(obj.dragelminds(end)).polygons.verts};

            draggedverts = closeGraphicsPolylines(draggedverts);

            obj.gelmdragvis = showPolygons(...
                {draggedverts},obj.gelmdragvis,obj.axes,...
                'edgecolor',[0;1;0],...
                'facecolor','none');
        else
            dragelmind = obj.dragelminds(end);
            dragelmnodeind = dragelmind + obj.pgraph.elmoffset;
            
            % get leaf elements in the pattern graph hierarchy of the
            % dragged element
            leafdragnodeinds = [dragelmnodeind,obj.pgraph.hierarchyDescendants(dragelmnodeind)];
            leafdragnodeinds(obj.pgraph.hlevel(leafdragnodeinds) > 0) = [];
            leafdragelminds = leafdragnodeinds - obj.pgraph.elmoffset;
            leafdragelminds(leafdragelminds < 1 | leafdragelminds > numel(obj.pgraph.elms)) = [];
            
            polys = [obj.pgraph.elms(leafdragelminds).polygons];
            draggedverts = {polys.verts};
            
            % transform vertices of these leaf elements from original to
            % dragged position
            draggedvertpolyind = cellind_mex(draggedverts);
            draggedverts = [draggedverts{:}];
            draggedverts = posetransform(draggedverts,obj.dragelmpose,obj.pgraph.elms(obj.dragelminds(end)).pose2D);
            draggedverts = array2cell_mex(draggedverts,[draggedvertpolyind{:}]);
            for i=1:numel(draggedverts)
                draggedverts{i} = reshape(draggedverts{i},2,[]);
            end

            draggedverts = closeGraphicsPolylines(draggedverts);
            
            obj.gelmdragvis = showPolygons(...
                {draggedverts},obj.gelmdragvis,obj.axes,...
                'edgecolor',obj.settings.color_elements',...
                'facecolor','none');
        end
    else
        obj.hideElmdragvis;
    end
end

function hideElmdragvis(obj)
    delete(obj.gelmdragvis(isgraphics(obj.gelmdragvis)));
    obj.gelmdragvis = gobjects(1,1);
end

function layoutPanels(obj)
    obj.layoutPanels@AxesWidget;
    
    mainpanelUnits = obj.mainpanel.Units;
    statustextUnits = obj.gstatustext.Units;
    
    obj.mainpanel.Units = 'pixels';
    obj.gstatustext.Units = 'pixels';
    
    obj.gstatustext.Position = [...
        obj.mainpanel.Position(3)-200,...
        obj.mainpanel.Position(4)-25,...
        200,25];
    
    obj.mainpanel.Units = mainpanelUnits;
    obj.gstatustext.Units = statustextUnits;
end

function show(obj)
    obj.show@AxesWidget;

    if isvalid(obj.uigroup)
        obj.uigroup.show;
    end
    
    if not(isempty(obj.navwidget)) &&  isvalid(obj.navwidget)
        obj.navwidget.show;
    end
    
    obj.layoutPanels;
    
    if isvalid(obj.selelmindsListener)
        obj.selelmindsListener.Enabled = true;
    end
    if isvalid(obj.selrelindsListener)
        obj.selrelindsListener.Enabled = true;
    end
    if isvalid(obj.selgroupindsListener)
        obj.selgroupindsListener.Enabled = true;
    end
    if isvalid(obj.selinstgroupindsListener)
        obj.selinstgroupindsListener.Enabled = true;
    end
    if isvalid(obj.selhlevelListener)
        obj.selhlevelListener.Enabled = true;
    end
    if isvalid(obj.openedhelmindListener)
        obj.openedhelmindListener.Enabled = true;
    end
    if isvalid(obj.toolListener)
        obj.toolListener.Enabled = true;
    end
    
    obj.updateMainaxes;
    obj.showAxescontents;
end

function hide(obj)
    obj.hide@AxesWidget;
    
    if isvalid(obj.uigroup)
        obj.uigroup.hide;
    end

    if not(isempty(obj.navwidget)) &&  isvalid(obj.navwidget)
        obj.navwidget.hide;
    end
    
    if isvalid(obj.selelmindsListener)
        obj.selelmindsListener.Enabled = false;
    end
    if isvalid(obj.selrelindsListener)
        obj.selrelindsListener.Enabled = false;
    end
    if isvalid(obj.selgroupindsListener)
        obj.selgroupindsListener.Enabled = false;
    end
    if isvalid(obj.selinstgroupindsListener)
        obj.selinstgroupindsListener.Enabled = false;
    end
    if isvalid(obj.selhlevelListener)
        obj.selhlevelListener.Enabled = false;
    end
    if isvalid(obj.openedhelmindListener)
        obj.openedhelmindListener.Enabled = false;
    end
    if isvalid(obj.toolListener)
        obj.toolListener.Enabled = false;
    end
    
    obj.hideAxescontents;
end

end

methods(Static)
    
function newelmposes = updateInstances(elmposes,origpgraph,instgrpinds,elmchanged)

    newelmposes = elmposes;
    
    for i=1:numel(instgrpinds)
        elminds = find(origpgraph.instgroups(:,instgrpinds(i)))'-origpgraph.elmoffset;
        elminds(elminds < 1 | elminds > numel(origpgraph.elms)) = [];

        if isempty(elminds)
            continue;
        end

        elmnodeinds = elminds + origpgraph.elmoffset;

        childelminds = cell(1,numel(elminds));
        childelmlocalposes = cell(1,numel(elminds));
        for j=1:numel(elminds)
            childelminds{j} = find(origpgraph.hierarchy(elmnodeinds(j),:))-origpgraph.elmoffset;
            childelminds{j}(childelminds{j} < 1 | childelminds{j} > numel(origpgraph.elms)) = [];

            % transform to local coordinates in the composite element pose of
            % the old pgraph (since the other instances still have the old
            % shape, too)
            origelmpose = origpgraph.elms(elminds(j)).pose2D;

            childelmlocalposes{j} = posetransform(...
                elmposes(:,childelminds{j}),identpose2D,origelmpose);
        end

        childalreadychangedmask = false(1,numel(childelminds{1}));
        for j=1:numel(elminds)
            childchangedmask = elmchanged(childelminds{j});
            if any(childchangedmask & childalreadychangedmask)
                warning('The same element has been changed in two different instances, skipping additional change.');
                childchangedmask(childalreadychangedmask) = false;
            end
            childalreadychangedmask = childalreadychangedmask | childchangedmask;

            if any(childchangedmask)
                for k=1:numel(elminds)
                    if k==j
                        continue;
                    end

                    % again, use the pose of the composite element in the
                    % original unmodified pattern graph, otherwise the
                    % instances are not guaranteed to maintain their symmetry
                    % or other properties that they orignally shared
                    origelmpose = origpgraph.elms(elminds(k)).pose2D;

                    newelmposes(:,childelminds{k}(childchangedmask)) = posetransform(...
                        childelmlocalposes{j}(:,childchangedmask),origelmpose,identpose2D);
                end
            end
        end
    end
end
    
function [elmind,mindist] = closestElement(pos,pgraph,hlevel,openedhelmind,excludeelminds,explicithierarchyelements)

    if nargin < 5 || isempty(explicithierarchyelements)
        excludeelminds = zeros(1,0);
    end
    
    if nargin < 6 || isempty(explicithierarchyelements)
        explicithierarchyelements = true;
    end
    
    if explicithierarchyelements
        elminds = 1:numel(pgraph.elms);
        elmnodeinds = elminds + pgraph.elmoffset;
        hlevelmask = pgraph.hlevel(elmnodeinds) == hlevel;
        hlevelinds = find(hlevelmask);
        [minind,mindist] = PatternWidget.closestFeature(pos,pgraph.elms(hlevelinds));
        elmind = elminds(hlevelinds(minind));
    else
        % get leaf descendent of the child elements of the opened hierarchy
        % element
        if isempty(openedhelmind)
            relminds = pgraph.hierarchyRootelements;
        else
            relminds = find(pgraph.hierarchy(openedhelmind,:));
        end
        relmnodeinds = relminds + pgraph.elmoffset;
        elmnodeinds = unique([relmnodeinds,pgraph.hierarchyDescendants(relmnodeinds)]);
        elmnodeinds = elmnodeinds(pgraph.hlevel(elmnodeinds) == 0);
        elminds = elmnodeinds - pgraph.elmoffset;
        elminds(elminds < 1 | elminds > numel(pgraph.elms)) = [];
        elmnodeinds = elminds + pgraph.elmoffset;
        
        [minind,mindist] = PatternWidget.closestFeature(pos,pgraph.elms(elminds));
        elmnodeind = elmnodeinds(minind);
        
        hancestornodeinds = unique([elmnodeind,pgraph.hierarchyAncestors(elmnodeind)]);
        elmnodeind = relmnodeinds(find(ismember(relmnodeinds,hancestornodeinds),1,'first'));
        elmind = elmnodeind - pgraph.elmoffset;
        
        if isempty(elmind)
            elmind = zeros(1,0);
            mindist = zeros(1,0);
        end
    end
    
    if any(elmind == excludeelminds)
        elmind = zeros(1,0);
        mindist = zeros(1,0);
    end
end
    
function [featind,mindist] = closestFeature(pos,features)
    
    if isempty(features)
        featind = zeros(1,0);
        mindist = zeros(1,0);
        return;
    end
    
    % compute candidates for the closest feature based on bounding boxes of
    % objects (all objects where the lower bound for the distance is no
    % farther away than the smallest upper bound over all objects)
    [bbmin,bbmax] = features.boundingbox;
    
    tobbmin = [bbmin(1,:)-pos(1,:);bbmin(2,:)-pos(2,:)];
    frombbmax = [pos(1,:)-bbmax(1,:);pos(2,:)-bbmax(2,:)];
    dbbmin = max(0,tobbmin);
    dbbmax = max(0,frombbmax);
    dlowerbound = sum(min(dbbmin,dbbmax).^2,1);
    dupperbound = sum(max(dbbmin,dbbmax).^2,1);
    
    % for safety
    if any(dupperbound.*1.00001 < dlowerbound)
        error('upper distance bound < lower distance bound, this should not happen.');
    end
    
    candidateinds = find(dlowerbound <= min(dupperbound));
    
    d = features(candidateinds).pointDistance(pos);
    
    [mindist,featind] = min(d);
    featind = candidateinds(featind);
end

function [relind,mindist] = closestRelationship(pos,pgraph,pgraphlayout,hlevel)
    
    offset = numel(pgraph.elms);
    srcnodepos = pgraphlayout(:,[pgraph.elmrelsrcind,pgraph.metarelsrcind]);
    tgtnodepos = pgraphlayout(:,[pgraph.elmreltgtind,pgraph.metareltgtind]);
    nodepos = pgraphlayout(:,offset+1:end);
    
    elmrelnodeinds = pgraph.elmreloffset+1:pgraph.elmreloffset+numel(pgraph.elmrels);
    metarelnodeinds = pgraph.metareloffset+1:pgraph.metareloffset+numel(pgraph.metarels);
    hlevelrelmask = [...
            pgraph.hlevel(elmrelnodeinds) == hlevel,...
            pgraph.hlevel(metarelnodeinds) == hlevel];
    hlevelrelinds = find(hlevelrelmask);
    
    d = pointLinesegDistance2D_mex(pos,...
        reshape([srcnodepos(:,hlevelrelinds);nodepos(:,hlevelrelinds)],2,[]),...
        reshape([nodepos(:,hlevelrelinds);tgtnodepos(:,hlevelrelinds)],2,[]));
    
    [mindist,lsegind] = min(d);
    relind = floor((lsegind-1) / 2) + 1;
    
    relind = hlevelrelinds(relind);
end

function gobjs = showPgraphElements(gobjs,ax,pgraph,elminds,varargin)

    if nargin < 4 || size(elminds,1) == 0
        elminds = 1:numel(pgraph.elms);
    end
    
    if not(isrow(elminds))
        elminds = elminds';
    end
    
    options = struct(...
        'selelminds',zeros(1,0),...
        'selgroupinds',zeros(1,0),...
        'selinstgroupinds',zeros(1,0),...
        'selhlevel',0,...
        'openedhelmind',zeros(1,0),...
        'changedelminds',zeros(1,0),...
        'fixedelminds',zeros(1,0),...
        'linkedelminds',zeros(1,0),...
        'overridecolors',[],...
        'overridelinewidths',[],...
        'explicithierarchyelements',true,...
        'globalblendoutstrength',0,...
        'hierarcy_blendoutstrength',0.2,...
        'linewidth',1,...
        'color_elements',rgbhex2rgbdec('4D4D4D'),...
        'color_changedelements',rgbhex2rgbdec('1d91c0'),...
        'color_fixedelements',rgbhex2rgbdec('756bb1'),...
        'linewidth_selectedelements',1,...
        'color_linkedelements',rgbhex2rgbdec('7fcdbb'),...
        'color_selectedelements',rgbhex2rgbdec('4eb3d3'),...
        'color_selectedgroups',rgbhex2rgbdec('7bccc4'));
        
    options = nvpairs2struct(varargin,options);

    elmnodeinds = elminds + pgraph.elmoffset;
    elmhlevel = pgraph.hlevel(elmnodeinds);
    
    if not(options.explicithierarchyelements)
        % get the ancestors and descendants of all given elements (the
        % color of the given elements depends on those)
        elminds = unique([elminds,...
            pgraph.hierarchyDescendants(elminds + pgraph.elmoffset)-pgraph.elmoffset,...
            pgraph.hierarchyAncestors(elminds + pgraph.elmoffset)-pgraph.elmoffset]);
        elminds(elminds < 1 | elminds > numel(pgraph.elms)) = [];
        elmnodeinds = elminds+pgraph.elmoffset;
        elmhlevel = pgraph.hlevel(elmnodeinds);
        
        % make sparse matrix of all ancestors of element i in column i
        h = pgraph.hierarchy(elmnodeinds,elmnodeinds);
        ancestors = h;
        parents = ancestors;
        for i=1:max(pgraph.hlevel)
            if not(any(parents(:)))
                break;
            end
            parents = h * parents;
            ancestors = ancestors + parents;
        end
        
        if not(isempty(options.openedhelmind))
            openedelminds = find(pgraph.hierarchy(options.openedhelmind,:));
        else
            openedelminds = pgraph.hierarchyRootelements;
        end
    end
    
    elms = pgraph.elms(elminds);

    if isempty(elms)
        return;
    end

    colors = repmat(options.color_elements',1,numel(elms));
    linewidths = ones(1,numel(elms)).*options.linewidth;

    % apply override colors
    if not(isempty(options.overridecolors))
        mask = all(not(isnan(options.overridecolors)),1);
        
        if not(options.explicithierarchyelements)
            inds = find(mask);
            for i=1:numel(inds);
                propinds = [inds(i),find(ancestors(inds(i),:)),find(ancestors(:,inds(i)))'];
                colors(1,propinds) = options.overridecolors(1,inds(i));
                colors(2,propinds) = options.overridecolors(2,inds(i));
                colors(3,propinds) = options.overridecolors(3,inds(i));
            end
        else
            colors(:,mask) = options.overridecolors(:,mask);    
        end
    end
    
    % apply line widths colors
    if not(isempty(options.overridelinewidths))
        mask = not(isnan(options.overridelinewidths));
        if not(options.explicithierarchyelements)
            inds = find(mask);
            for i=1:numel(inds);
                linewidths([inds(i),find(ancestors(inds(i),:)),find(ancestors(:,inds(i)))']) = options.overridelinewidths(inds(i));
            end
        else
            linewidths(:,mask) = options.overridelinewidths(:,mask);    
        end
    end
    
    % color changed elements
    mask = ismember(elminds,options.changedelminds);
    if not(options.explicithierarchyelements)
        mask = mask | (ancestors*mask')' | (mask*ancestors);
    end
    colors(:,mask) = repmat(options.color_changedelements',1,sum(mask));
    
    % color fixed elements
    mask = ismember(elminds,options.fixedelminds);
    if not(options.explicithierarchyelements)
        mask = mask | (ancestors*mask')' | (mask*ancestors);
    end
    colors(:,mask) = repmat(options.color_fixedelements',1,sum(mask));
    
    % color linked elements
    mask = ismember(elminds,options.linkedelminds);
    if not(options.explicithierarchyelements)
        mask = mask & ismember(elminds,openedelminds);
        mask = mask | (ancestors*mask')' | (mask*ancestors);
    else
        mask = ismember(elminds,options.linkedelminds');
    end
    colors(:,mask) = repmat(options.color_linkedelements',1,sum(mask));

    % color selected elements
    mask = ismember(elms,pgraph.elms(options.selelminds));
    if not(options.explicithierarchyelements)
        mask = mask | (ancestors*mask')' | (mask*ancestors);
    end
    colors(:,mask) = repmat(options.color_selectedelements',1,sum(mask));
    linewidths(mask) = options.linewidth_selectedelements;

    % color selected group elements
    if not(isempty(options.selgroupinds))
        mask = logical(pgraph.groups(elmnodeinds,options.selgroupinds(end)))';
        if not(options.explicithierarchyelements)
            mask = mask | (ancestors*mask')' | (mask*ancestors);
        end
        colors(:,mask) = repmat(options.color_selectedgroups',1,sum(mask));
    end

    % color selected instance group elements
    if not(isempty(options.selinstgroupinds))
        mask = logical(pgraph.instgroups(elmnodeinds,options.selinstgroupinds(end)))';
        if not(options.explicithierarchyelements)
            mask = mask | (ancestors*mask')' | (mask*ancestors);
        end
        colors(:,mask) = repmat(options.color_selectedgroups',1,sum(mask));
    end
    
    hlevelelmmask = elmhlevel == options.selhlevel;

    if not(options.explicithierarchyelements)
        % blend out elements that are not descendants of the opened hierarchy element
        if not(isempty(options.openedhelmind))
            mask = ismember(elminds,openedelminds);
            mask = mask | mask * ancestors;
            colors(:,not(mask)) = bsxfun(@plus,options.hierarcy_blendoutstrength.*[1;1;1],(1-options.hierarcy_blendoutstrength).*colors(:,not(mask)));
        end
        
        visible = repmat({'on'},1,numel(elminds));
        visible(elmhlevel > 0) = {'off'};
        stackz = zeros(1,numel(elminds));
        stackz(hlevelelmmask) = -1;
        
        additionalargs = {'visible',visible,'stackz',stackz};
    else
        colors(:,not(hlevelelmmask)) = bsxfun(@plus,options.hierarcy_blendoutstrength.*[1;1;1],(1-options.hierarcy_blendoutstrength).*colors(:,not(hlevelelmmask)));

        stackz = zeros(1,numel(elminds));
        stackz(hlevelelmmask) = -1;
        additionalargs = {'stackz',stackz};
    end
    
    if numel(gobjs) ~= numel(pgraph.elms)
        delete(gobjs(isvalid(gobjs)));
        gobjs = gobjects(1,numel(pgraph.elms));
    end
    
    if options.globalblendoutstrength > 0
        colors = bsxfun(@plus,...
            options.globalblendoutstrength.*[1;1;1],...
            (1-options.globalblendoutstrength).*colors);
    end
    
    gobjs(elminds) = showScenePolygons(...
        {elms.polygons},gobjs(elminds),ax,...
        'edgecolor',colors,...
        'edgewidth',linewidths,...
        additionalargs{:});
end

function gobjs = hidePgraphElements(gobjs,elminds)
    if nargin < 2
        elminds = 1:numel(gobjs);
    end
    
    delete(gobjs(elminds(isgraphics(gobjs(elminds)))));
    gobjs(elminds) = gobjects(1,numel(elminds));
end

end

end