classdef SolverTestPatternEditor < AxesWidget
    
properties(SetAccess=protected)
    solutionpgraphs = PatternGraph.empty;
    solutioncosts = zeros(1,0);
    
    pwidget = PatternWidget.empty;
end

properties(SetObservable, AbortSet)
    selsolutioninds = zeros(1,0);
    psolver = LinestNonlinearSolver.empty;
end

properties(Access=protected)
    selsolutionindsListener = event.listener.empty;
%     pgraphListener = event.listener.empty;
    
    uigroup = UIHandlegroup.empty;
    
    gupdatebutton = gobjects(1,0);
    gprevsolutionbutton = gobjects(1,0);
    gnextsolutionbutton = gobjects(1,0);
    gsolutioncosttext = gobjects(1,0);
    
    gsolveelms = gobjects(1,0);
    gsolverels = gobjects(1,0);
    gsolvereldir = gobjects(1,0);
    
    stoppressed = false;
    solverisrunning = false;
    
    mousepos_screen = [];
    clickedpos_screen = [];
end
    
methods
    
function obj = SolverTestPatternEditor(ax,pwidget,toolpanel)
    
    obj = obj@AxesWidget(ax,'Solver Test Pattern Editor');
    
    obj.pwidget = pwidget;
    
    obj.uigroup = UIHandlegroup;
    
    obj.gupdatebutton = uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','Update Pattern',...
        'Callback', {@(src,evt) obj.updatePressed},...
        'Units','normalized',...
        'Position',[0.54,0,0.15,1]);
    obj.uigroup.addChilds(obj.gupdatebutton);
    obj.gprevsolutionbutton = uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','<',...
        'Callback', {@(src,evt) obj.prevsolutionPressed},...
        'Units','normalized',...
        'Position',[0.7,0.3,0.1,0.7]);
    obj.uigroup.addChilds(obj.gprevsolutionbutton);
    obj.gnextsolutionbutton = uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','>',...
        'Callback', {@(src,evt) obj.nextsolutionPressed},...
        'Units','normalized',...
        'Position',[0.8,0.3,0.1,0.7]);
    obj.uigroup.addChilds(obj.gnextsolutionbutton);
    obj.gsolutioncosttext = uicontrol(toolpanel,...
        'Style','text',...
        'String','<nothing selected>',...
        'Units','normalized',...
        'Position',[0.7,0.0,0.2,0.2]);
    obj.uigroup.addChilds(obj.gsolutioncosttext);
    
    obj.selsolutionindsListener = obj.addlistener('selsolutioninds','PostSet',@(src,evt) obj.selsolutionindsChanged);
%     obj.pgraphListener = obj.pwidget.addlistener('pgraph','PostSet',@(src,evt) obj.pgraphChanged);
    
%     obj.psolver = ChooserelsRelaxationSolver;
%     obj.psolver = ChooserelsBruteforceSolver;
%     obj.psolver = ChooserelsBCDPropagationSolver2;
%     obj.psolver = ChooserelsDepthfirstBCDPropagationSolver;
%     obj.psolver = ChooserelsGreedyPropagationSolver;
%     obj.psolver = ChooserelsDepthfirstSolver;
%     obj.psolver = ChooserelsGeneticSolver;
    obj.psolver = LinestNonlinearSolver;
%     obj.psolver = GuidedPropagationSolver;
    
    obj.pwidget.synctolerance = obj.psolver.settings.synctolerance;
    
    obj.hide;
end

function delete(obj)
    obj.clear;
    
    delete(obj.selsolutionindsListener(isvalid(obj.selsolutionindsListener)));
%     delete(obj.pgraphListener(isvalid(obj.pgraphListener)));
    
    delete(obj.psolver(isvalid(obj.psolver)));
end

function clear(obj)
    obj.pwidget.clear;
    
    obj.clearLocal;
end

function clearLocal(obj)
    obj.clearSolutions;
    obj.hideSolveVis;
    
    obj.psolver.clear;

    obj.stoppressed = false;
    obj.solverisrunning = false;
end

function patternpreloadedResponse(obj)
    obj.clearLocal;
end

function clearSolutions(obj)
    obj.selsolutioninds = zeros(1,0);
    delete(obj.solutionpgraphs(isvalid(obj.solutionpgraphs)));
    obj.solutionpgraphs = PatternGraph.empty(1,0);
    obj.solutioncosts = zeros(1,0);
end

function updatePressed(obj)
    
    if not(isempty(obj.pwidget.pgraph))
        
        if isempty(obj.pwidget.changedrelinds)
            warning('Nothing changed.');
            return;
        end
        
        obj.clearSolutions;
        
        obj.stoppressed = false;
        obj.pwidget.tool = '';
        
        obj.solverisrunning = true;
%         obj.selpartialsolutioninds = zeros(1,0);

        obj.psolver.setVisfunction(@(varargin) obj.solverCallback(varargin{:}));
        obj.psolver.setPatterngraph(obj.pwidget.origpgraph);
        
        newelmposes = [obj.pwidget.pgraph.elms.pose2D];
        newrelvals = [obj.pwidget.pgraph.rels.value];        
        
        if isa(obj.psolver,'ChooserelsRelaxationSolver')

            obj.psolver.solve(newelmposes,newrelvals);
            
        elseif isa(obj.psolver,'ChooserelsBruteforceSolver')
            
            obj.psolver.solve(newelmposes,newrelvals);

        elseif isa(obj.psolver,'ChooserelsBCDPropagationSolver2')

            obj.psolver.solve(newelmposes,newrelvals);
            
        elseif isa(obj.psolver,'ChooserelsDepthfirstBCDPropagationSolver')

            obj.psolver.solve(newelmposes,newrelvals);
            
        elseif isa(obj.psolver,'ChooserelsGreedyPropagationSolver')

            obj.psolver.solve(newelmposes,newrelvals);

        elseif isa(obj.psolver,'ChooserelsDepthfirstSolver')

            obj.psolver.solve(newelmposes,newrelvals);
            
        elseif isa(obj.psolver,'ChooserelsGeneticSolver')

            obj.psolver.solve(newelmposes,newrelvals);
            
        elseif isa(obj.psolver,'LinestNonlinearSolver')
            
            obj.psolver.solve(newelmposes,newrelvals,'visualize','all');
            
        elseif isa(obj.psolver,'GuidedPropagationSolver')
            
            startelm = obj.pwidget.selelminds;
            if numel(startelm) > 1
                warndlg('Can currently only modify one element at a time.');
                return;
            end

            obj.psolver.solve(newelmposes,newrelvals,startelm);
        else
            error('Unknown pattern solver.');
        end
        
        if isa(obj.psolver,'StandardPatternSolver')
            obj.solutionpgraphs = obj.psolver.createSolutionPgraphs(1:numel(obj.psolver.solutionelmposes));
        else
            for i=1:numel(obj.psolver.solutionelmposes)
                obj.solutionpgraphs(i) = obj.psolver.pgraph.clone;
                solelmposes = obj.solutionpgraphs(i).solutionelmposes{i};

                % update all element instances
                posediff = solelmposes - [obj.psolver.pgraph.elms.pose2D];
                posediff(3:4,:) = smod(posediff(3:4,:),-pi,pi);
                elmchanged = any(bsxfun(@rdivide,...
                    posediff.^2,...
                    obj.psolver.pgraph.elmposevar) > ...
                    obj.psolver.settings.synctolerance*0.01,1);
                solelmposes = PatternWidget.updateInstances(solelmposes,obj.psolver.pgraph,1:size(obj.solutionpgraphs(i).instgroups,2),elmchanged);

                % update all element poses in the new pattern
                % graph and update hierarchy childs from parents
                obj.solutionpgraphs(i).transformElements(1:numel(obj.solutionpgraphs(i).elms),solelmposes);

                % update all hierarchy elements from childs
                obj.solutionpgraphs(i).updateHierarchyelements; 

                obj.solutionpgraphs(i).recomputeRelationships;
            end    
        end
        
        obj.solutioncosts = obj.psolver.solutioncosts;
        
        obj.hideSolveVis;

        obj.selsolutioninds = 1;
        
        obj.solverisrunning = false;
        
        if not(obj.pwidget.relationshipsVisible)
            obj.pwidget.toggleRelationshipsVisible;
        end
    end
end

function prevsolutionPressed(obj)
    if not(isempty(obj.solutionpgraphs))
        obj.selsolutioninds = mod(obj.selsolutioninds-1 - 1,numel(obj.solutionpgraphs)) + 1;
    end
end

function nextsolutionPressed(obj)
    if not(isempty(obj.solutionpgraphs))
        obj.selsolutioninds = mod(obj.selsolutioninds+1 - 1,numel(obj.solutionpgraphs)) + 1;
    end
end

function selsolutionindsChanged(obj)
    if not(isempty(obj.selsolutioninds)) && obj.selsolutioninds <= numel(obj.solutionpgraphs)
        pg = obj.solutionpgraphs(obj.selsolutioninds).clone;
        
        obj.pwidget.setPattern(pg);
        
        obj.gsolutioncosttext.String = [...
            num2str(obj.solutioncosts(obj.selsolutioninds)),'  ',...
            num2str(obj.selsolutioninds),' / ',num2str(numel(obj.solutionpgraphs))];
    else
        obj.gsolutioncosttext.String = '<nothing selected>';
    end
end

function stop = solverCallback(obj,solver,varargin)

    obj.showSolveVis(solver,varargin{:});
    
    pause(0.01);
    drawnow;
    
    if obj.stoppressed
        uiwait;
    end
    
    stop = obj.stoppressed;
    obj.stoppressed = false;
end

function showSolveVis(obj,solver,varargin)
    
    if isa(solver,'ChooserelsRelaxationSolver')
        elmposes = varargin{1};
        relactive = varargin{2};
        
        if obj.pwidget.relationshipsVisible
            obj.pwidget.toggleRelationshipsVisible;
        end
    elseif isa(solver,'ChooserelsBruteforceSolver')
        elmposes = varargin{1};
        relactive = varargin{2};
        
        if obj.pwidget.relationshipsVisible
            obj.pwidget.toggleRelationshipsVisible;
        end
    elseif isa(solver,'ChooserelsBCDPropagationSolver2') || isa(solver,'ChooserelsGreedyPropagationSolver') || isa(solver,'ChooserelsDepthfirstBCDPropagationSolver2')
        elmposes = varargin{1};
        visitedmask = varargin{2};
        if not(isrow(visitedmask))
            visitedmask = visitedmask';
        end
        syncmask = varargin{3};
        if not(isrow(syncmask))
            syncmask = syncmask';
        end
        fixedflag = varargin{4};
        if not(isrow(fixedflag))
            fixedflag = fixedflag';
        end
        nodeinds = varargin{5};
        if not(isrow(nodeinds))
            nodeinds = nodeinds';
        end
        
        if obj.pwidget.relationshipsVisible
            obj.pwidget.toggleRelationshipsVisible;
        end
    elseif isa(solver,'ChooserelsDepthfirstSolver') || isa(solver,'LinestNonlinearSolver') || isa(solver,'StochasticLinearSolver')
        elmposes = varargin{1};
        
        nodefixorig = varargin{2};
        if not(isrow(nodefixorig))
            nodefixorig = nodefixorig';
        end
        
        nodefixnew = varargin{3};
        if not(isrow(nodefixnew))
            nodefixnew = nodefixnew';
        end
        
        elmoffset = obj.pwidget.pgraph.elmoffset;
        elmfixorig = nodefixorig(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relfixorig = nodefixorig(obj.pwidget.pgraph.rel2nodeinds);
        
        elmfixnew = nodefixnew(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relfixnew = nodefixnew(obj.pwidget.pgraph.rel2nodeinds);
        
        if obj.pwidget.relationshipsVisible
            obj.pwidget.toggleRelationshipsVisible;
        end
    elseif isa(solver,'ChooserelsGeneticSolver')
        elmposes = varargin{1};
        
        if obj.pwidget.relationshipsVisible
            obj.pwidget.toggleRelationshipsVisible;
        end
    elseif isa(solver,'GuidedPropagationSolver')
        elmposes = varargin{1};
        
        nodefixorig = varargin{2};
        if not(isrow(nodefixorig))
            nodefixorig = nodefixorig';
        end
        
        nodefixnew = varargin{3};
        if not(isrow(nodefixnew))
            nodefixnew = nodefixnew';
        end
        
        nodevisited = varargin{4};
        if not(isrow(nodevisited))
            nodevisited = nodevisited';
        end
        
        elmoffset = obj.pwidget.pgraph.elmoffset;

        elmfixorig = nodefixorig(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relfixorig = nodefixorig(obj.pwidget.pgraph.rel2nodeinds);
        
        elmfixnew = nodefixnew(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relfixnew = nodefixnew(obj.pwidget.pgraph.rel2nodeinds);
        
        elmvisited = nodevisited(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relvisited = nodevisited(obj.pwidget.pgraph.rel2nodeinds);
        
        if obj.pwidget.relationshipsVisible
            obj.pwidget.toggleRelationshipsVisible;
        end
    else
        error('Unrecognized solver');
    end
    
    if isa(solver,'ChooserelsRelaxationSolver') || ...
       isa(solver,'ChooserelsBruteforceSolver') || isa(solver,'ChooserelsBCDPropagationSolver2') || isa(solver,'ChooserelsGreedyPropagationSolver') || ...
       isa(solver,'ChooserelsDepthfirstSolver') || isa(solver,'ChooserelsGeneticSolver') || isa(solver,'ChooserelsDepthfirstBCDPropagationSolver2') || ... 
       isa(solver,'LinestNonlinearSolver') || isa(solver,'StochasticLinearSolver') || isa(solver,'GuidedPropagationSolver')
        
        % green
        if isa(solver,'ChooserelsBCDPropagationSolver2') || isa(solver,'ChooserelsGreedyPropagationSolver') || isa(solver,'ChooserelsDepthfirstBCDPropagationSolver2')
            colors = ...
                bsxfun(@times,max(0,min(1,visitedmask(1:numel(obj.pwidget.pgraph.elms)))),[0.4;0.8;0.4]) + ...
                bsxfun(@times,max(0,min(1,1-visitedmask(1:numel(obj.pwidget.pgraph.elms)))),[0.7;0.7;0.7]);
            
            elminds = nodeinds(nodeinds <= numel(obj.pwidget.pgraph.elms));
            colors(:,elminds) = repmat([1;0;1],1,numel(elminds));
        elseif isa(solver,'ChooserelsDepthfirstSolver')
            
            colors = ...
                bsxfun(@times,max(0,min(1,1-(elmfixorig+elmfixnew))),[0.3;0.8;0.3]) + ...
                bsxfun(@times,max(0,min(1,elmfixorig)),[0.8;0.3;0.3]) + ...
                bsxfun(@times,max(0,min(1,elmfixnew)),[0.3;0.3;0.8]);
%             
%             colors = repmat([0.3;0.8;0.3],1,numel(obj.pwidget.pgraph.elms));
%             colors(:,elmfixed) = repmat([0.8;0.3;0.3],1,sum(elmfixed));
        elseif isa(solver,'GuidedPropagationSolver')
            
            colors = ...
                bsxfun(@times,max(0,min(1,1-(elmfixorig+elmfixnew))),[0.3;0.8;0.3]) + ...
                bsxfun(@times,max(0,min(1,elmfixorig)),[0.8;0.3;0.3]) + ...
                bsxfun(@times,max(0,min(1,elmfixnew)),[0.3;0.3;0.8]);
            
            colors = ...
                bsxfun(@times,colors,max(0,min(1,elmvisited))) + ...
                bsxfun(@times,[0.3;0.3;0.3],max(0,min(1,1-elmvisited)));
        else
            colors = repmat([0.3;0.8;0.3],1,numel(obj.pwidget.pgraph.elms));
        end

        elmvisverts = cell(1,numel(obj.pwidget.pgraph.elms));
        for i=1:numel(obj.pwidget.pgraph.elms)
            elmvisverts{i} = posetransform(obj.pwidget.pgraph.elms(i).verts,[elmposes(1:5,i);0],obj.pwidget.pgraph.elms(i).pose2D);
        end

        obj.gsolveelms = showPolygons(...
            elmvisverts,obj.gsolveelms,obj.axes,...
            'facecolor','none',... % default for non-filled polygons
            'edgecolor',colors);
    end
    
%     return; % temnp


    if isa(solver,'ChooserelsRelaxationSolver') || isa(solver,'ChooserelsBruteforceSolver') || isa(solver,'ChooserelsBCDPropagationSolver2') || ...
            isa(solver,'ChooserelsGreedyPropagationSolver') || isa(solver,'ChooserelsDepthfirstSolver') || isa(solver,'ChooserelsGeneticSolver') || ...
            isa(solver,'ChooserelsDepthfirstBCDPropagationSolver2') || ...
            isa(solver,'LinestNonlinearSolver') || isa(solver,'StochasticLinearSolver') || isa(solver,'GuidedPropagationSolver')
        
        layout = nan(2,size(obj.pwidget.pgraph.adjacency,1));
        layout = PatternWidget.updatePGraphlayout_s(...
            obj.pwidget.pgraph.adjacency,...
            elmposes(1:2,:),...
            numel(obj.pwidget.pgraph.elmrels),...
            numel(obj.pwidget.pgraph.metarels),...
            layout);
        
        offset = numel(obj.pwidget.pgraph.elms);
        srcpos = layout(:,obj.pwidget.pgraph.elmrelsrcind);
        tgtpos = layout(:,obj.pwidget.pgraph.elmreltgtind);
        pos = layout(:,offset+1:offset+numel(obj.pwidget.pgraph.elmrels));
        
        elmrelverts = reshape([srcpos;pos;tgtpos],2,[]);
        elmrelverts = mat2cell(elmrelverts,2,ones(1,size(pos,2)).*3);
%         elmrelcolor = repmat(rgbhex2rgbdec('2C60D1')',1,size(elmrelverts,2));
        
        elmreldirverts = reshape(pos + (tgtpos-pos) .* 0.1,2,[]);
        elmreldirverts = mat2cell(elmreldirverts,2,ones(1,size(pos,2)));
%         elmreldircolor = repmat(rgbhex2rgbdec('2C60D1')',1,size(elmreldirverts,2));
        
        offset = numel(obj.pwidget.pgraph.elms) + numel(obj.pwidget.pgraph.elmrels);
        srcpos = layout(:,obj.pwidget.pgraph.metarelsrcind);
        tgtpos = layout(:,obj.pwidget.pgraph.metareltgtind);
        pos = layout(:,offset+1:offset+numel(obj.pwidget.pgraph.metarels));
        
        metarelverts = reshape([srcpos;pos;tgtpos],2,[]);
        metarelverts = mat2cell(metarelverts,2,ones(1,size(pos,2)).*3);
%         metarelcolor = repmat(rgbhex2rgbdec('40B740')',1,size(metarelverts,2));
        
        metareldirverts = reshape(pos + (tgtpos-pos) .* 0.1,2,[]);
        metareldirverts = mat2cell(metareldirverts,2,ones(1,size(pos,2)));
%         metareldircolor = repmat(rgbhex2rgbdec('40B740')',1,size(metareldirverts,2));
        
        verts = [elmrelverts,metarelverts];
%         color = [elmrelcolor,metarelcolor];
        dirverts = [elmreldirverts,metareldirverts];
%         dircolor = [elmreldircolor,metareldircolor];
        
        if isa(solver,'ChooserelsBruteforceSolver')        
            relcolor = ...
                bsxfun(@times,max(0,min(1,relactive)),[0;0;1]) + ...
                bsxfun(@times,max(0,min(1,1-relactive)),[1;0;0]);
        elseif isa(solver,'ChooserelsRelaxationSolver')
            relcolor = ...
                bsxfun(@times,max(0,min(1,relactive(1,:))),[0;0;1]) + ...
                bsxfun(@times,max(0,min(1,relactive(2,:))),[1;0;0]);
        elseif isa(solver,'ChooserelsBCDPropagationSolver2') || isa(solver,'ChooserelsGreedyPropagationSolver') || ...
                isa(solver,'ChooserelsDepthfirstBCDPropagationSolver2')
%             relcolor = ...
%                 bsxfun(@times,max(0,min(1,visitedmask(numel(obj.pwidget.pgraph.elms)+1:end))),[1;0;0]) + ...
%                 bsxfun(@times,max(0,min(1,1-visitedmask(numel(obj.pwidget.pgraph.elms)+1:end))),[0;0;1]);
            
            noderelinds = obj.pwidget.pgraph.node2relinds(1:numel(visitedmask));
            noderelmask = not(isnan(noderelinds));

            relcolor = zeros(3,sum(noderelmask));

            mask = fixedflag(noderelmask) == -1 & visitedmask(noderelmask) == 0;
            relcolor(:,mask) = repmat([0.7;0.7;0.7],1,sum(mask));
            
            mask = fixedflag(noderelmask) == -1 & visitedmask(noderelmask) == 1;
            relcolor(:,mask) = repmat([0.4;0.4;0.4],1,sum(mask));
            
            mask = fixedflag(noderelmask) == 1;
            relcolor(:,mask) = repmat([1;0;0],1,sum(mask));
            
            mask = fixedflag(noderelmask) == 0;
            relcolor(:,mask) = repmat([0.4;0.8;0.4],1,sum(mask));
            
            mask = fixedflag(noderelmask) == 2;
            relcolor(:,mask) = repmat([0.4;0.4;0.8],1,sum(mask));

%             fixedcolor = ...
%                 bsxfun(@times,max(0,min(1,fixedflag(numel(obj.pwidget.pgraph.elms)+1:end)==1)),[0;0.5;0]) + ...
%                 bsxfun(@times,max(0,min(1,1-(fixedflag(numel(obj.pwidget.pgraph.elms)+1:end)~=1))),[0;0;0]);
%             relcolor = relcolor + fixedcolor;
            
            currentrelinds = nodeinds(nodeinds > numel(obj.pwidget.pgraph.elms))-numel(obj.pwidget.pgraph.elms);
            relcolor(:,currentrelinds) = repmat([1;0;1],1,numel(currentrelinds));
        elseif isa(solver,'ChooserelsDepthfirstSolver') || isa(solver,'LinestNonlinearSolver') || isa(solver,'StochasticLinearSolver')
%             relcolor = repmat([0.3;0.8;0.3],1,numel(obj.pwidget.pgraph.rels));
%             relcolor(:,relfixed) = repmat([0.8;0.3;0.3],1,sum(relfixed));
            
%             relcolor = ...
%                 bsxfun(@times,max(0,min(1,relfixorig)),[0.8;0.3;0.3]) + ...
%                 bsxfun(@times,max(0,min(1,1-relfixorig)),[0.3;0.8;0.3]);
            
            relcolor = ...    
                bsxfun(@times,max(0,min(1,1-(relfixorig+relfixnew))),[0.3;0.8;0.3]) + ...
                bsxfun(@times,max(0,min(1,relfixorig)),[0.8;0.3;0.3]) + ...
                bsxfun(@times,max(0,min(1,relfixnew)),[0.3;0.3;0.8]);
            
        elseif isa(solver,'ChooserelsGeneticSolver')
            relcolor = repmat([0.3;0.8;0.3],1,numel(obj.pwidget.pgraph.rels));
        elseif isa(solver,'GuidedPropagationSolver')
            
            relcolor = ...    
                bsxfun(@times,max(0,min(1,1-(relfixorig+relfixnew))),[0.3;0.8;0.3]) + ...
                bsxfun(@times,max(0,min(1,relfixorig)),[0.8;0.3;0.3]) + ...
                bsxfun(@times,max(0,min(1,relfixnew)),[0.3;0.3;0.8]);
            
            relcolor = ...
                bsxfun(@times,relcolor,max(0,min(1,relvisited))) + ...
                bsxfun(@times,[0.3;0.3;0.3],max(0,min(1,1-relvisited)));
        end
        
%         color = reshape(repmat(relcolor,3,1),3,[]);
        color = relcolor;
        dircolor = relcolor;
        
        color = reshape(repmat(color,3,1),3,[]);
        color = mat2cell(color,3,ones(1,size(verts,2)).*3);
        
        dircolor = reshape(repmat(dircolor,1,1),3,[]);
        dircolor = mat2cell(dircolor,3,ones(1,size(dirverts,2)));
        
        obj.gsolverels = showPolygons(...
            {verts},obj.gsolverels,obj.axes,...
            'fvcolor',{color},...
            'edgecolor','flat',...
            'facecolor','none',...
            'markersymbol','o',...
            'markerfacecolor','flat');
        
        if obj.pwidget.relationshipdirsVisible
            obj.gsolvereldir = showPoints(...
                dirverts,obj.gsolvereldir,obj.axes,...
                'fvcolor',dircolor,...
                'markersymbol','s',...
                'markerfacecolor','flat');
        else
            delete(obj.gsolvereldir(isvalid(obj.gsolvereldir)));
            obj.gsolvereldir = gobjects(1,0);            
        end
    end
end

function hideSolveVis(obj)
    delete(obj.gsolveelms(isvalid(obj.gsolveelms)));
    obj.gsolveelms = gobjects(1,0);
    delete(obj.gsolverels(isvalid(obj.gsolverels)));
    obj.gsolverels = gobjects(1,0);
    delete(obj.gsolvereldir(isvalid(obj.gsolvereldir)));
    obj.gsolvereldir = gobjects(1,0);
end

function unblock(obj)
    obj.blockcallbacks = false;
    obj.pwidget.unblock;
end

function mousePressed(obj,src,evt)

    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.mousePressed(src,evt);
    end
    
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;
    
%     obj.selpanel = gparent(obj.fig.CurrentObject,'uipanel',true);
    
    obj.mousedown = true;

    obj.mousepos = mouseposOnPlane(obj.axes);
    obj.clickedpos = obj.mousepos;
    obj.mousepos_screen = mouseScreenpos;
    obj.clickedpos_screen = obj.mousepos_screen;
    
    if obj.pwidget.selpanel == obj.pwidget.mainpanel
        if obj.pwidget.navwidget.zoommode || ...
           obj.pwidget.navwidget.panmode || ...
           obj.pwidget.navwidget.orbitmode

            % do nothing

        end
    end
    
    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt)

    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.mouseMoved(src,evt);
    end
    
    if obj.mousedown

        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;

        obj.mousepos = mouseposOnPlane(obj.axes);
        obj.mousepos_screen = mouseScreenpos;
        
        if obj.pwidget.selpanel == obj.pwidget.mainpanel
            if obj.pwidget.navwidget.zoommode || ...
               obj.pwidget.navwidget.panmode || ...
               obj.pwidget.navwidget.orbitmode

                % do nothing

            end
        end
        
        obj.blockcallbacks = false;
    end
end

function mouseReleased(obj,src,evt)

    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.mouseReleased(src,evt);
    end
    
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;
    
    if obj.pwidget.selpanel == obj.pwidget.mainpanel
        if obj.pwidget.navwidget.zoommode || ...
           obj.pwidget.navwidget.panmode || ...
           obj.pwidget.navwidget.orbitmode

            % do nothing

        end
    end
    
    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt)
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.keyPressed(src,evt);
    end
    
    if strcmp(evt.Key,'escape')
        if obj.stoppressed
            uiresume;
        end
    elseif strcmp(evt.Key,'s')
        if obj.stoppressed
            obj.stoppressed = false;
            uiresume;
        else
            obj.stoppressed = true;
        end    
    end
end

function keyReleased(obj,src,evt)
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.keyReleased(src,evt);
    end
end

function show(obj)
    obj.show@AxesWidget;
    
    if isvalid(obj.uigroup)
        obj.uigroup.show;
    end
    
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.show;
    end
    
%     if isvalid(obj.pgraphListener)
%         obj.pgraphListener.Enabled = true;
%     end
    if isvalid(obj.selsolutionindsListener)
        obj.selsolutionindsListener.Enabled = true;
    end
    
    if not(obj.pwidget.relationshipsVisible)
        obj.pwidget.toggleRelationshipsVisible;
    end
    
    if not(obj.pwidget.editorfig.toolpanelVisible)
        obj.pwidget.editorfig.toggleToolpanelVisible;
    end
    
    if not(obj.pwidget.explicithierarchyelements)
        obj.pwidget.toggleExplicithierarchyelements;
    end
    
    obj.pwidget.tool = 'elementselect';
end

function hide(obj)
    obj.hide@AxesWidget;
    
    if isvalid(obj.uigroup)
        obj.uigroup.hide;
    end
    
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.hide;
    end
    
%     if isvalid(obj.pgraphListener)
%         obj.pgraphListener.Enabled = false;
%     end
    if isvalid(obj.selsolutionindsListener)
        obj.selsolutionindsListener.Enabled = false;
    end
    
    obj.hideSolveVis;
end

end

end