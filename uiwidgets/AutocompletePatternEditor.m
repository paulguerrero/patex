classdef AutocompletePatternEditor < AxesWidget
    
properties(SetAccess=protected)
    psolver = GuidedPropagationSolver.empty;
    
    pwidget = PatternWidget.empty;
    
%     overlaycolor = rgbhex2rgbdec('fd8d3c');%6cfe49
%     selchoicecolor = rgbhex2rgbdec('fd8d3c');
%     choicecolor = rgbhex2rgbdec('e31a1c');
%     choicerad = 5;
%     selchoicerad = 10;
%     overlayblendfactor = 0.9;

    settings = struct;
end

properties(Access=protected)
    patternpreloadedListener = event.listener.empty;
    patternpostloadedListener = event.listener.empty;
    toolListener = event.listener.empty;
    
    uigroup = UIHandlegroup.empty;
    
%     gmenu_edithistory = gobjects(1,0);
    gmenu_settings = gobjects(1,0);
    
    goverlayelms = gobjects(1,0);
    goverlayrels = gobjects(1,1);
    goverlayreldirs = gobjects(1,1);
    gchoiceoverlayelms = gobjects(1,0);
    
    gsolveelms = gobjects(1,0);
    gsolverels = gobjects(1,0);
    gsolvereldir = gobjects(1,0);
    
    autocomplete_choiceinds = zeros(1,0);
    
    overlaypgraph = PatternGraph.empty;
    overlaypgraphlayout = nan(2,0);
%     overlayelmposes = zeros(2,0);

%     solutionpgraphs = PatternGraph.empty(1,0);
    
    selchoicesolind = zeros(1,0);
    
    draggingelm = false;
    
    mousepos_screen = [];
    clickedpos_screen = [];
    
    edithistory_pgraphs = cell(1,0);
    edithistory_solvers = cell(1,0);
    
    snapping = true;
    
    lastedithistsavename = '';
    lastedithistloadname = '';
end

methods(Static)

function s = defaultSettings
    
    s = struct(...
            'linewidth_overlay',2,...
            'color_overlay',rgbhex2rgbdec('fd8d3c'),...
            'color_choicecircle',rgbhex2rgbdec('e31a1c'),...
            'color_choicecircle_selected',rgbhex2rgbdec('fd8d3c'),...
            'radius_choicecircle',5,...
            'radius_choicecircle_selected',10,...
            'blendoutstrength_overlay',0.9,...
            'visualize_nloptimization','none',...
            'saveedithistory',true); 
end
    
end

methods
    
function obj = AutocompletePatternEditor(ax,pwidget,menus,varargin)
    
    obj = obj@AxesWidget(ax,'Autocomplete Pattern Editor');
    
    obj.pwidget = pwidget;
    
    obj.uigroup = UIHandlegroup;
    
    obj.settings = AutocompletePatternEditor.defaultSettings;
    if not(isempty(varargin))
        obj.settings = nvpairs2struct(varargin,obj.settings,false,0);
    end
    
%     % file menu
%     obj.gmenu_edithistory = uimenu(menus.File,...
%         'Label','Save Edit History',...
%         'Callback', {@(src,evt) obj.saveEdithistory},...
%         'Checked','off');
%     obj.uigroup.addChilds(obj.gmenu_edithistory);
    
    % settings menu
    obj.gmenu_settings = uimenu(menus.Settings,...
        'Label','Settings',...
        'Callback', {@(src,evt) obj.showSettingsdialog},...
        'Checked','off');
    obj.uigroup.addChilds(obj.gmenu_settings);
    
    obj.patternpreloadedListener = obj.pwidget.addlistener('patternpreloaded',@(src,evt) obj.patternpreloadedResponse);
    obj.patternpostloadedListener = obj.pwidget.addlistener('patternpostloaded',@(src,evt) obj.patternpostloadedResponse);
    obj.toolListener = obj.pwidget.addlistener('tool','PostSet',@(src,evt) obj.toolChanged);
    
    obj.psolver = GuidedPropagationSolver;
    obj.pwidget.synctolerance = obj.psolver.settings.synctolerance;
    
    if obj.settings.saveedithistory
        obj.edithistory_pgraphs(end+1) = {obj.pwidget.origpgraph.clone};
        obj.edithistory_solvers(end+1) = {obj.psolver.clone};
        obj.edithistory_solvers{end}.clonePatterngraph;
    end
    
    obj.hide;
end

function delete(obj)
    obj.clear;
    
    delete(obj.patternpreloadedListener(isvalid(obj.patternpreloadedListener)));
    delete(obj.patternpostloadedListener(isvalid(obj.patternpostloadedListener)));
    delete(obj.toolListener(isvalid(obj.toolListener)));
    delete(obj.psolver(isvalid(obj.psolver)));
end

function clear(obj)
    obj.pwidget.clear;

    obj.clearLocal;
end

function clearLocal(obj)
    obj.hideSolveVis;
    
    obj.selchoicesolind = zeros(1,0);
    obj.autocomplete_choiceinds = zeros(1,0);
    obj.draggingelm = false;
    
    obj.psolver.clear;
    
    obj.hideChoiceOverlayElements;
    obj.clearOverlay;
    
    for i=1:numel(obj.edithistory_pgraphs)
        delete(obj.edithistory_pgraphs{i}(isvalid(obj.edithistory_pgraphs{i})));
    end
    obj.edithistory_pgraphs = cell(1,0);
    for i=1:numel(obj.edithistory_solvers)
        delete(obj.edithistory_solvers{i}(isvalid(obj.edithistory_solvers{i})));
    end
    obj.edithistory_solvers = cell(1,0);
    
%     delete(obj.solutionpgraphs(isvalid(obj.solutionpgraphs)));
%     obj.solutionpgraphs = PatternGraph.empty(1,0);
end

function clearOverlay(obj)
    obj.hideOverlayElements;
    obj.pwidget.setElementblendout(0);
    if not(obj.pwidget.elmdragvisVisible)
        obj.pwidget.toggleElmdragvisVisible
    end
    obj.hideOverlayRelationships;
%     obj.overlayelmposes = zeros(2,0);
    delete(obj.overlaypgraph(isvalid(obj.overlaypgraph)));
    obj.overlaypgraph = PatternGraph.empty;
end

function patternpreloadedResponse(obj)
    obj.clearLocal;
end

function patternpostloadedResponse(obj)
    if obj.settings.saveedithistory
        obj.edithistory_pgraphs(end+1) = {obj.pwidget.origpgraph.clone};
        obj.edithistory_solvers(end+1) = {obj.psolver.clone};
        obj.edithistory_solvers{end}.clonePatterngraph;
    end
end

function showSettingsdialog(obj)
    if not(isempty(obj.psolver)) && isvalid(obj.psolver)
        dlgsettings = patterneditorSettingsdlg(obj);
    end
    solversettings = obj.psolver.settings;
    editorsettings = obj.settings;
    
    if isfield(dlgsettings,'solutionmaxcost')
        solversettings.maxcost = dlgsettings.solutionmaxcost;
    end
    if isfield(dlgsettings,'solutiondistanceRelthresh')
        solversettings.solutiondistancethresh = dlgsettings.solutiondistanceRelthresh;
    end
    if isfield(dlgsettings,'solutiondistanceElmthresh')
        solversettings.elmsolutiondistancethresh = dlgsettings.solutiondistanceElmthresh;
    end
    if isfield(dlgsettings,'maxsolutions')
        solversettings.maxsolutions = dlgsettings.maxsolutions;
    end
    if isfield(dlgsettings,'maxlinearevals')
        solversettings.maxlinearevals = dlgsettings.maxlinearevals;
    end
    if isfield(dlgsettings,'maxgenvariations')
        solversettings.maxgenvariations = dlgsettings.maxgenvariations;
    end
    
    if isfield(dlgsettings,'visualize_nloptimization')
        editorsettings.visualize_nloptimization = dlgsettings.visualize_nloptimization;
    end
    
    obj.psolver.setSettings(solversettings);
    obj.settings = editorsettings;
end

function saveEdithistory(obj,filename)
    if not(obj.settings.saveedithistory)
        warning('Edit history is not available (saving edit history is turned off).');
        return;
    end
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastedithistsavename))
            savename = obj.lastedithistsavename;
        elseif not(isempty(obj.lastedithistloadname))
            savename = obj.lastedithistloadname;
        else
            savename = 'tempedithistory.mat';
        end
        
        [fname,pathname,~] = ...
            uiputfile({
            '*.mat','MAT file (*.mat)'},...
            'Save Edit History',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    disp('exporting edit history...');
    obj.lastedithistsavename = filename;
    
    eh_pgraphs = obj.edithistory_pgraphs;
    eh_solvers = obj.edithistory_solvers;
    eh_settings = obj.settings;
    
    for i=1:numel(eh_solvers)
        % solvers can't have handles to functions in the UI, otherwise the
        % whole UI is stored
        eh_solvers{i}.clearFunctions;
    end
    
%     save(filename,'ep_solvers','ep_solinds','ep_solapprox','ep_rowfixedelminds','ep_pgraphs','ep_colhistory','ep_settings','-v7.3');
%     save(filename,'ep_solvers','-v7.3');
    save(filename,'eh_pgraphs','eh_solvers','eh_settings','-v7.3');
    
    disp('done');
end

function updateOverlayPGraphlayout(obj,varargin)
    if not(isempty(obj.overlaypgraph)) && not(isempty(obj.overlaypgraph.adjacency))

        obj.overlaypgraphlayout = PatternWidget.updatePGraphlayout_s(...
            obj.overlaypgraph.adjacency,...
            [obj.overlaypgraph.elms.position],...
            numel(obj.overlaypgraph.elmrels),...
            numel(obj.overlaypgraph.metarels),...
            obj.overlaypgraphlayout,varargin{:});
    else
        obj.overlaypgraphlayout = nan(2,0);
    end
end

function toolChanged(obj) %#ok<MANU>
    % do nothing
end

function showOverlayElements(obj)
    if not(isempty(obj.overlaypgraph))

        elms = obj.overlaypgraph.elms;
        
        if isempty(elms)
            obj.hideOverlayElements;
            return;
        end
        
%         colors = repmat(rgbhex2rgbdec('FCB156')',1,numel(elms));
%         elmnodeinds = obj.overlaypgraph.elmoffset+1:obj.overlaypgraph.elmoffset+numel(obj.overlaypgraph.elms);
%         hlevelelmmask = obj.overlaypgraph.hlevel(elmnodeinds) == obj.pwidget.selhlevel;
%         colors(:,not(hlevelelmmask)) = bsxfun(@plus,[0.8;0.8;0.8],0.2.*colors(:,not(hlevelelmmask)));
        
        obj.goverlayelms = PatternWidget.showPgraphElements(...
            obj.goverlayelms,...        
            obj.axes,...
            obj.overlaypgraph,...
            1:numel(obj.overlaypgraph.elms),...
            'explicithierarchyelements',obj.pwidget.explicithierarchyelements,...    
            'color_elements',obj.settings.color_overlay,...
            'linewidth',obj.settings.linewidth_overlay);
    else    
        obj.hideElements;
    end
end

function showChoiceOverlayElements(obj,elmind,choicepos,choicedistsq,choiceind)
    
    [~,screenscale] = axes2screen(obj.axes,[0;0]);
    
%     minrad = 2./mean(screenscale);
%     maxrad = 5./mean(screenscale);
% %     mindist = obj.pwidget.synctolerance*5;
% %     maxdist = obj.pwidget.synctolerance*100;
%     mindist = 5./mean(screenscale);
%     maxdist = 200./mean(screenscale);
% %     maxdist = obj.pwidget.synctolerance*100;
%     
% %     disp('-----');
% %     disp([minrad;maxrad]);
% %     disp([mindist;maxdist]);
    

    
    colors = repmat(obj.settings.color_choicecircle',1,size(choicepos,2));
    rad = repmat(obj.settings.radius_choicecircle/mean(screenscale),1,size(choicepos,2));
    if not(isempty(choiceind))
        colors(:,choiceind) = obj.settings.color_choicecircle_selected';
        rad(choiceind) = obj.settings.radius_choicecircle_selected/mean(screenscale);
    end
    
    elmvisverts = cell(1,size(choicepos,2));
    for i=1:size(choicepos,2)
        
%         rad = minrad + (1 - min(1,max(0,sqrt(choicedistsq(i) - mindist) / (maxdist-mindist)))) * (maxrad - minrad);
        
        [elmvisverts{i}(1,:),elmvisverts{i}(2,:)] = polygonCircle(choicepos(1,i),choicepos(2,i),rad(i));
        
%         elmvisverts{i} = posetransform(...
%             obj.pwidget.pgraph.elms(elmind).verts,...
%             choicepose(:,i),...
%             obj.pwidget.pgraph.elms(elmind).pose2D);
    end
    
    
    
%     obj.gchoiceoverlayelms = showPolygons(...
%         elmvisverts,obj.gchoiceoverlayelms,obj.axes,...
%         'facecolor','none',...
%         'edgewidth',2,...
%         'edgecolor',rgbhex2rgbdec('FCB156')'); % orange
    
    obj.gchoiceoverlayelms = showPolygons(...
        elmvisverts,obj.gchoiceoverlayelms,obj.axes,...
        'facecolor',colors,...
        'edgecolor','none'); % orange
end

function hideChoiceOverlayElements(obj)
    delete(obj.gchoiceoverlayelms(isgraphics(obj.gchoiceoverlayelms)));
    obj.gchoiceoverlayelms = gobjects(1,0);
end

function hideOverlayElements(obj)
    delete(obj.goverlayelms(isgraphics(obj.goverlayelms)));
    obj.goverlayelms = gobjects(1,0);
end

function hideOverlayRelationships(obj)
    delete(obj.goverlayrels(isgraphics(obj.goverlayrels)));
    obj.goverlayrels = gobjects(1,1);
    delete(obj.goverlayreldirs(isgraphics(obj.goverlayreldirs)));
    obj.goverlayreldirs = gobjects(1,1);
end

function showOverlayRelationships(obj)
    error('Not imlemented yet.');
end

function stop = solverCallback(obj,solver,varargin)

    obj.showSolveVis(solver,varargin{:});
    
    pause(0.01);
    drawnow;
    
    stop = false;
end

% stop = showSolveVis(obj,'fixedrels',elmposes)
% stop = showSolveVis(obj,'chooserels',elmposes,relactive)
function showSolveVis(obj,solver,varargin)
    
    if isa(solver,'LinestNonlinearSolver') || isa(solver,'StochasticLinearSolver')
        elmposes = varargin{1};
        
        nodefixorig = varargin{2};
        if not(isrow(nodefixorig))
            nodefixorig = nodefixorig';
        end
        
        nodefixnew = varargin{3};
        if not(isrow(nodefixnew))
            nodefixnew = nodefixnew';
        end
        
        elmoffset = obj.pwidget.pgraph.elmoffset;
        elmfixorig = nodefixorig(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relfixorig = nodefixorig(obj.pwidget.pgraph.rel2nodeinds);
        
        elmfixnew = nodefixnew(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relfixnew = nodefixnew(obj.pwidget.pgraph.rel2nodeinds);
        
        if obj.pwidget.relationshipsVisible
            obj.pwidget.toggleRelationshipsVisible;
        end
    elseif isa(solver,'GuidedPropagationSolver')
        elmposes = varargin{1};

        nodefixorig = varargin{2};
        if not(isrow(nodefixorig))
            nodefixorig = nodefixorig';
        end

        nodefixnew = varargin{3};
        if not(isrow(nodefixnew))
            nodefixnew = nodefixnew';
        end

        nodevisited = varargin{4};
        if not(isrow(nodevisited))
            nodevisited = nodevisited';
        end

        elmoffset = obj.pwidget.pgraph.elmoffset;

        elmfixorig = nodefixorig(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relfixorig = nodefixorig(obj.pwidget.pgraph.rel2nodeinds);

        elmfixnew = nodefixnew(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relfixnew = nodefixnew(obj.pwidget.pgraph.rel2nodeinds);

        elmvisited = nodevisited(elmoffset+1:elmoffset+numel(obj.pwidget.pgraph.elms));
        relvisited = nodevisited(obj.pwidget.pgraph.rel2nodeinds);

        if obj.pwidget.relationshipsVisible
            obj.pwidget.toggleRelationshipsVisible;
        end
    end
    
    if isa(solver,'ChooserelsDepthfirstSolver') || isa(solver,'LinestNonlinearSolver') || isa(solver,'StochasticLinearSolver')
        
        colors = repmat([0.3;0.8;0.3],1,numel(obj.pwidget.pgraph.elms));
        
    elseif isa(solver,'GuidedPropagationSolver')
        
        colors = ...
            bsxfun(@times,max(0,min(1,1-(elmfixorig+elmfixnew))),[0.3;0.8;0.3]) + ...
            bsxfun(@times,max(0,min(1,elmfixorig)),[0.8;0.3;0.3]) + ...
            bsxfun(@times,max(0,min(1,elmfixnew)),[0.3;0.3;0.8]);

        colors = ...
            bsxfun(@times,colors,max(0,min(1,elmvisited))) + ...
            bsxfun(@times,[0.3;0.3;0.3],max(0,min(1,1-elmvisited)));
        
    else
        error('Unknown Solver.');
    end

    elmvisverts = cell(1,numel(obj.pwidget.pgraph.elms));
    for i=1:numel(obj.pwidget.pgraph.elms)
        elmvisverts{i} = posetransform(obj.pwidget.pgraph.elms(i).verts,[elmposes(1:5,i);0],obj.pwidget.pgraph.elms(i).pose2D);
    end

    obj.gsolveelms = showPolygons(...
        elmvisverts,obj.gsolveelms,obj.axes,...
        'facecolor','none',... % default for non-filled polygons
        'edgecolor',colors);

    
%     return; % temnp

        
    layout = nan(2,size(obj.pwidget.pgraph.adjacency,1));
    layout = PatternWidget.updatePGraphlayout_s(...
        obj.pwidget.pgraph.adjacency,...
        elmposes(1:2,:),...
        numel(obj.pwidget.pgraph.elmrels),...
        numel(obj.pwidget.pgraph.metarels),...
        layout);

    offset = numel(obj.pwidget.pgraph.elms);
    srcpos = layout(:,obj.pwidget.pgraph.elmrelsrcind);
    tgtpos = layout(:,obj.pwidget.pgraph.elmreltgtind);
    pos = layout(:,offset+1:offset+numel(obj.pwidget.pgraph.elmrels));

    elmrelverts = reshape([srcpos;pos;tgtpos],2,[]);
    elmrelverts = mat2cell(elmrelverts,2,ones(1,size(pos,2)).*3);
%         elmrelcolor = repmat(rgbhex2rgbdec('2C60D1')',1,size(elmrelverts,2));

    elmreldirverts = reshape(pos + (tgtpos-pos) .* 0.1,2,[]);
    elmreldirverts = mat2cell(elmreldirverts,2,ones(1,size(pos,2)));
%         elmreldircolor = repmat(rgbhex2rgbdec('2C60D1')',1,size(elmreldirverts,2));

    offset = numel(obj.pwidget.pgraph.elms) + numel(obj.pwidget.pgraph.elmrels);
    srcpos = layout(:,obj.pwidget.pgraph.metarelsrcind);
    tgtpos = layout(:,obj.pwidget.pgraph.metareltgtind);
    pos = layout(:,offset+1:offset+numel(obj.pwidget.pgraph.metarels));

    metarelverts = reshape([srcpos;pos;tgtpos],2,[]);
    metarelverts = mat2cell(metarelverts,2,ones(1,size(pos,2)).*3);
%         metarelcolor = repmat(rgbhex2rgbdec('40B740')',1,size(metarelverts,2));

    metareldirverts = reshape(pos + (tgtpos-pos) .* 0.1,2,[]);
    metareldirverts = mat2cell(metareldirverts,2,ones(1,size(pos,2)));
%         metareldircolor = repmat(rgbhex2rgbdec('40B740')',1,size(metareldirverts,2));

    verts = [elmrelverts,metarelverts];
%         color = [elmrelcolor,metarelcolor];
    dirverts = [elmreldirverts,metareldirverts];
%         dircolor = [elmreldircolor,metareldircolor];

    if isa(solver,'ChooserelsDepthfirstSolver') || isa(solver,'LinestNonlinearSolver') || isa(solver,'StochasticLinearSolver')
        
        relcolor = ...    
            bsxfun(@times,max(0,min(1,1-(relfixorig+relfixnew))),[0.3;0.8;0.3]) + ...
            bsxfun(@times,max(0,min(1,relfixorig)),[0.8;0.3;0.3]) + ...
            bsxfun(@times,max(0,min(1,relfixnew)),[0.3;0.3;0.8]);
        
    elseif isa(solver,'GuidedPropagationSolver')
        
        relcolor = ...    
            bsxfun(@times,max(0,min(1,1-(relfixorig+relfixnew))),[0.3;0.8;0.3]) + ...
            bsxfun(@times,max(0,min(1,relfixorig)),[0.8;0.3;0.3]) + ...
            bsxfun(@times,max(0,min(1,relfixnew)),[0.3;0.3;0.8]);

        relcolor = ...
            bsxfun(@times,relcolor,max(0,min(1,relvisited))) + ...
            bsxfun(@times,[0.3;0.3;0.3],max(0,min(1,1-relvisited)));
    else
        error('Unknown Solver.');
    end

%         color = reshape(repmat(relcolor,3,1),3,[]);
    color = relcolor;
    dircolor = relcolor;

    color = reshape(repmat(color,3,1),3,[]);
    color = mat2cell(color,3,ones(1,size(verts,2)).*3);

    dircolor = reshape(repmat(dircolor,1,1),3,[]);
    dircolor = mat2cell(dircolor,3,ones(1,size(dirverts,2)));

    obj.gsolverels = showPolygons(...
        {verts},obj.gsolverels,obj.axes,...
        'fvcolor',{color},...
        'edgecolor','flat',...
        'facecolor','none',...
        'markersymbol','o',...
        'markerfacecolor','flat');

    if obj.pwidget.relationshipdirsVisible
        obj.gsolvereldir = showPoints(...
            dirverts,obj.gsolvereldir,obj.axes,...
            'fvcolor',dircolor,...
            'markersymbol','s',...
            'markerfacecolor','flat');
    else
        delete(obj.gsolvereldir(isvalid(obj.gsolvereldir)));
        obj.gsolvereldir = gobjects(1,0);            
    end
end

function hideSolveVis(obj)
    delete(obj.gsolveelms(isvalid(obj.gsolveelms)));
    obj.gsolveelms = gobjects(1,0);
    delete(obj.gsolverels(isvalid(obj.gsolverels)));
    obj.gsolverels = gobjects(1,0);
    delete(obj.gsolvereldir(isvalid(obj.gsolvereldir)));
    obj.gsolvereldir = gobjects(1,0);
end

function unblock(obj)
    obj.blockcallbacks = false;
    obj.pwidget.unblock;
end

function mousePressed(obj,src,evt)

    obj.pwidget.mousePressed(src,evt);
    
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;
    
%     obj.selpanel = gparent(obj.fig.CurrentObject,'uipanel',true);
    
    obj.mousedown = true;

    obj.mousepos = mouseposOnPlane(obj.axes);
    obj.clickedpos = obj.mousepos;
    obj.mousepos_screen = mouseScreenpos;
    obj.clickedpos_screen = obj.mousepos_screen;
    
    if obj.pwidget.selpanel == obj.pwidget.mainpanel

        if obj.pwidget.navwidget.zoommode || ...
           obj.pwidget.navwidget.panmode || ...
           obj.pwidget.navwidget.orbitmode

            % do nothing

        elseif strcmp(obj.pwidget.tool,'elementselect')
            
            obj.snapping = true;
            
            if not(isempty(obj.pwidget.pgraph))
                
                if not(isempty(obj.psolver.choicesolinds)) && not(isempty(obj.pwidget.selelminds)) && ...
                        (numel(obj.pwidget.selelminds) ~= numel(obj.psolver.startelement) || any(obj.pwidget.selelminds ~= obj.psolver.startelement))

                    obj.psolver.updateChoices(obj.pwidget.selelminds(end)); % this will become dragelminds(end)

                    % find the last solutions in the first branches
                    % (before they end or branch themselves), these
                    % are the branches that split due to different
                    % values for the selected elements
                    if numel(obj.psolver.choicesolinds) == 1
                        % there is only one choice
                        obj.autocomplete_choiceinds = 1;
                    else
                        obj.autocomplete_choiceinds = find(obj.psolver.choicetree(:,1));
                    end
                end
                
            end
        end
    end
    
    obj.blockcallbacks = false;
end


function mouseMoved(obj,src,evt)

    obj.pwidget.mouseMoved(src,evt);
    
    if obj.mousedown

        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;

        obj.mousepos = mouseposOnPlane(obj.axes);
        obj.mousepos_screen = mouseScreenpos;

        if obj.pwidget.selpanel == obj.pwidget.mainpanel
            
            if obj.pwidget.navwidget.zoommode || ...
               obj.pwidget.navwidget.panmode || ...
               obj.pwidget.navwidget.orbitmode

                % do nothing

            elseif strcmp(obj.pwidget.tool,'elementselect')
                
                if not(isempty(obj.pwidget.dragelminds)) && (obj.draggingelm || norm(obj.mousepos_screen-obj.clickedpos_screen) > 15) && strcmp(src.SelectionType,'normal')
                    
                    obj.draggingelm = true;
                    
%                     disp(obj.pwidget.dragelmpose);
                    
                    if obj.snapping && not(isempty(obj.autocomplete_choiceinds))
                        choicesolind = obj.psolver.choicesolinds(obj.autocomplete_choiceinds);
                        % pick best solution for each choice
                        for i=1:numel(choicesolind)
                            if isempty(choicesolind)
                                error('Choice without solutions found.');
                            end
                            solcost = obj.psolver.solutioncost(choicesolind{i});
                            [~,ind] = min(solcost);
                            choicesolind{i} = choicesolind{i}(ind);
                        end
                        choicesolind = [choicesolind{:}];
                        
%                         elmpose = obj.pwidget.pgraph.elms(obj.pwidget.dragelminds(end)).pose2D;
%                         mouseoffset = obj.mousepos(1:2) - dragelmpose(1:2);
                        
%                         identclickedpos = posetransform(obj.clickedpos(1:2),identpose2D,elmpose);
                        identclickedpos = posetransform(obj.clickedpos(1:2),identpose2D,obj.pwidget.dragelmpose);
                        
                        choicepose = repmat(obj.pwidget.dragelmpose,1,numel(obj.autocomplete_choiceinds));
                        choiceclickedpos = choicepose(1:2,:);
                        for i=1:numel(obj.autocomplete_choiceinds)
                            choicepose(:,i) = obj.psolver.choiceelmposes{obj.autocomplete_choiceinds(i)}(:,obj.pwidget.dragelminds(end));
                            choiceclickedpos(:,i) = posetransform(identclickedpos,choicepose(:,i),identpose2D);
                        end
%                         disp(choicepose);
                        
%                         choicemousepose = choicepose;
%                         choicemousepose(1:2,:) = bsxfun(@plus,choicepose(1:2,:),mouseoffset);

%                         disp(choiceclickedpos);
                        
                        
                        choiceposedistsq = sum(...
                            bsxfun(@rdivide,...
                            bsxfun(@minus,obj.mousepos(1:2),choiceclickedpos(1:2,:)).^2,...
                            obj.psolver.elmposevar(1:2,:)),...
                            1);
                        
%                         obj.mousepos 
                        
                        [minchoiceposdistsq,choiceind] = min(choiceposedistsq);
                        
                        % show solution overlay if the selected element is
                        % close enough
                        if minchoiceposdistsq < obj.psolver.settings.synctolerance * 5
                            if numel(obj.selchoicesolind) ~= numel(choicesolind(choiceind)) || any(obj.selchoicesolind ~= choicesolind(choiceind))
                                
                                delete(obj.overlaypgraph(isvalid(obj.overlaypgraph)));
                                
%                                 obj.overlaypgraph = obj.psolver.pgraph.clone;
%                                 solelmposes = obj.psolver.choiceelmposes{obj.autocomplete_choiceinds(choiceind)};
%                                 
%                                 disp(solelmposes(:,obj.pwidget.selelminds(1)));
%                                 % update all element poses in the new pattern
%                                 % graph and update hierarchy childs from parents
%                                 obj.overlaypgraph.transformElements(1:numel(obj.overlaypgraph.elms),solelmposes)
% 
%                                 % update all hierarchy elements from childs
%                                 obj.overlaypgraph.updateHierarchyelements;

                                obj.overlaypgraph = obj.psolver.createSolutionPgraphs(choicesolind(choiceind),false);
                                
                                obj.selchoicesolind = choicesolind(choiceind);

                                obj.updateOverlayPGraphlayout;

                                if obj.pwidget.elementsVisible
                                    obj.showOverlayElements;
                                    obj.pwidget.setElementblendout(obj.settings.blendoutstrength_overlay);
                                    if obj.pwidget.elmdragvisVisible
                                        obj.pwidget.toggleElmdragvisVisible
                                    end
                                end
                                if obj.pwidget.relationshipsVisible
    %                                 obj.showOverlayRelationship;
                                end
                            end
                        else
                            obj.clearOverlay;
                            obj.selchoicesolind = zeros(1,0);
                            choiceind = zeros(1,0);
                        end
                        
                        % show choice overlay elements
                        obj.showChoiceOverlayElements(obj.pwidget.dragelminds(end),choiceclickedpos,choiceposedistsq,choiceind);
                    else
                        obj.hideChoiceOverlayElements;
                        obj.clearOverlay;
                        obj.selchoicesolind = zeros(1,0);
                    end
                else
                    obj.draggingelm = false;
%                     obj.pwidget.hideElmdragvis;
                end
            end
        end
        
        obj.blockcallbacks = false;    
        
    end
    
end

function mouseReleased(obj,src,evt)

    obj.pwidget.mouseReleased(src,evt);
    
    obj.mousedown = false;
    
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;


    if obj.pwidget.selpanel == obj.pwidget.mainpanel

        if obj.pwidget.navwidget.zoommode || ...
           obj.pwidget.navwidget.panmode || ...
           obj.pwidget.navwidget.orbitmode

            % do nothing

        elseif strcmp(obj.pwidget.tool,'elementselect')
            

            if obj.draggingelm

                obj.pwidget.setElementblendout(0.7);
                drawnow;

                    % solve
                if isempty(obj.selchoicesolind) || not(obj.snapping)
                    % user positioned the element without snapping
                    % - compute the linear solutions
% 
%                     delete(obj.solutionpgraphs(isvalid(obj.solutionpgraphs)));
%                     obj.solutionpgraphs = PatternGraph.empty(1,0);
                    if not(isempty(obj.pwidget.changedelminds)) && not(isempty(obj.pwidget.lastchangedelminds))

                        % hierarchy childs are updated as elements are edited, but these
                        % should not be in the optimiziation, remove them
                        % => should be ok now since edited elements are passed separately, and
                        % these don't include the hierarchy child elements. Relationships
                        % between elements should stay the same since there should be no
                        % relationships between elements with different hierarchy parents =>
                        % except for things like rotation absolute rotation, so leave as is for
                        % now
                        changedpg = obj.pwidget.origpgraph.clone;
                        for i=1:numel(obj.pwidget.changedelminds)
                            changedpg.elms(obj.pwidget.changedelminds(i)).transform(...
                                obj.pwidget.pgraph.elms(obj.pwidget.changedelminds(i)).pose2D,'absolute');
                        end
                        changedpg.recomputeRelationships;                     

                        newselelmpose = [changedpg.elms(obj.pwidget.changedelminds).pose2D];
                        origselelmpose = [obj.pwidget.origpgraph.elms(obj.pwidget.changedelminds).pose2D];

                        posefixdims = [1,2];
                        if any(newselelmpose(3,:) ~= origselelmpose(3,:))
                            posefixdims(end+1) = 3;
                        end
                        if any(newselelmpose(4,:) ~= origselelmpose(4,:))
                            posefixdims(end+1) = 4;
                        end

                        obj.pwidget.showStatustext('finding pattern variations ...');
                        drawnow;

                        obj.psolver.setVisfunction(@(x1,x2,x3,x4) obj.solverCallback(x1,x2,x3,x4));
                        obj.psolver.setPatterngraph(obj.pwidget.origpgraph);
                        obj.psolver.solve(...
                            [changedpg.elms.pose2D],[changedpg.rels.value],obj.pwidget.lastchangedelminds,obj.pwidget.changedelminds,...
                            'posefixdims',posefixdims);

                        delete(changedpg(isvalid(changedpg)));

                        obj.pwidget.hideStatustext;

                    end

%                     solinds = obj.psolver.choicesolinds{1};
%                     obj.solutionpgraphs = obj.psolver.createSolutionPgraphs(solinds,false);
                else
                    % user snapped the element to an autocomplete solution
                    % compute the full non-linear solution

%                     % linear solution
%                     pg = obj.psolver.pgraph.clone;
%                     for i=1:numel(pg.elms)
%                         pg.transformElements(i,obj.psolver.solutionelmposes{obj.selchoicesolind}(:,i))
%                     end

                    obj.pwidget.showStatustext('refining chosen variation ...');
                    drawnow;

                    % non-linear solution
                    nlsolver = LinestNonlinearSolver(obj.psolver.pgraph);
                    nlsolver.setVisfunction(@(varargin) obj.solverCallback(varargin{:}));
                    nlsolver.setPatterngraph(obj.pwidget.origpgraph);
                    nlsolver.solve(obj.psolver.solver,obj.selchoicesolind,...
                        'posefixdims',obj.psolver.settings.posefixdims,...
                        'visualize',obj.settings.visualize_nloptimization);

                    if numel(nlsolver.solutionelmposes) ~= 1
                        error('Invalid solution for the nonlinear solver.');
                    end

                    pg = nlsolver.createSolutionPgraphs(1);

                    obj.pwidget.hideStatustext;

                    obj.hideSolveVis;

                    if obj.settings.saveedithistory
                        obj.edithistory_pgraphs(end+1) = {pg.clone};
                        obj.edithistory_solvers(end+1) = {obj.psolver.clone};
                        obj.edithistory_solvers{end}.clonePatterngraph;
                    end
                    
                    obj.pwidget.setPattern(pg,false);
                    
                    obj.psolver.clear;
                end

                obj.autocomplete_choiceinds = zeros(1,0);

                obj.draggingelm = false;

                obj.overlaypgraphlayout = nan(2,0);
                obj.clearOverlay;
                obj.hideChoiceOverlayElements;
                obj.selchoicesolind = zeros(1,0);

                obj.pwidget.updateMainaxes;
            end
        end
    end
   
    % execute queued callbacks before unblocking, so they get canceled
    % (BusyAction does not seem to do anything, so I have to do it this
    % way)
    obj.pwidget.setElementblendout(0);
    drawnow;
    
    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt)
    
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.keyPressed(src,evt);
    end
    
    % tool-specific keys
    if strcmp(obj.pwidget.tool,'elementselect')
        if strcmp(evt.Key,'escape')
            if obj.draggingelm
                obj.draggingelm = false;                

%                 obj.autocomplete_choiceinds = zeros(1,0);

                obj.hideChoiceOverlayElements;
                obj.clearOverlay;
                obj.selchoicesolind = zeros(1,0);
                
                obj.pwidget.selelminds = zeros(1,0);
            end
        elseif strcmp(evt.Key,'space')
            if obj.draggingelm
                obj.snapping = false;
                
                obj.hideChoiceOverlayElements;
                obj.clearOverlay;
                obj.selchoicesolind = zeros(1,0);
            end
        elseif strcmp(evt.Key,'backspace')    
            
            if isempty(obj.pwidget.changedelminds)
                if numel(obj.edithistory_pgraphs) >= 2 && isvalid(obj.edithistory_pgraphs{end-1})
                    obj.hideSolveVis;

                    obj.selchoicesolind = zeros(1,0);
                    obj.autocomplete_choiceinds = zeros(1,0);
                    obj.draggingelm = false;

                    obj.hideChoiceOverlayElements;
                    obj.clearOverlay;
                    
                    obj.pwidget.setPattern(obj.edithistory_pgraphs{end-1}.clone,false);
                    obj.psolver.clear;
                    delete(obj.edithistory_pgraphs{end}(isvalid(obj.edithistory_pgraphs{end})));
                    obj.edithistory_pgraphs(end) = [];
                    delete(obj.edithistory_solvers{end}(isvalid(obj.edithistory_solvers{end})));
                    obj.edithistory_solvers(end) = [];
                end
            else
                if numel(obj.edithistory_pgraphs) >= 1 && isvalid(obj.edithistory_pgraphs{end})
                    obj.hideSolveVis;

                    obj.selchoicesolind = zeros(1,0);
                    obj.autocomplete_choiceinds = zeros(1,0);
                    obj.draggingelm = false;

                    obj.hideChoiceOverlayElements;
                    obj.clearOverlay;
                    
                    obj.pwidget.setPattern(obj.edithistory_pgraphs{end}.clone,false);
                    obj.psolver.clear;
                end
            end
        end
    end
end

function keyReleased(obj,src,evt)
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.keyReleased(src,evt);
    end
    
    if strcmp(obj.pwidget.tool,'elementselect')
        if strcmp(evt.Key,'space')
            if obj.draggingelm
                obj.snapping = true;
            end
        end
    end
end

function layoutPanels(obj)
    if not(isempty(obj.pwidget))
        obj.pwidget.layoutPanels;
    end
end

function show(obj)
    obj.show@AxesWidget;
    
    if isvalid(obj.uigroup)
        obj.uigroup.show;
    end
    
    obj.layoutPanels;
    
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.show;
    end
    
    if isvalid(obj.toolListener)
        obj.toolListener.Enabled = true;
    end
    
    if obj.pwidget.relationshipsVisible
        obj.pwidget.toggleRelationshipsVisible;
    end
    
    if obj.pwidget.relationshipsVisible
        obj.pwidget.toggleRelationshipsVisible;
    end
    
    if obj.pwidget.editorfig.toolpanelVisible
        obj.pwidget.editorfig.toggleToolpanelVisible;
    end
    if not(obj.pwidget.editorfig.navpanelVisible)
        obj.pwidget.editorfig.toggleNavpanelVisible;
    end
    
    if obj.pwidget.explicithierarchyelements
        obj.pwidget.toggleExplicithierarchyelements;
    end
    
    obj.pwidget.tool = 'elementselect';
end

function hide(obj)
    obj.hide@AxesWidget;
    
    if isvalid(obj.uigroup)
        obj.uigroup.hide;
    end
    
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.hide;
    end
    
    if isvalid(obj.toolListener)
        obj.toolListener.Enabled = false;
    end
    
    obj.hideSolveVis;
end

end

end