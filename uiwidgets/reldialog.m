function selrelnames = reldialog(relnames,selrelind)
	
    if nargin < 2
        selrelind = [];
    end

    dwidth = 400;
    dheight = 400;
    
    screenpos = get(0,'screensize');
    
    dminx = screenpos(1)+screenpos(3)/2-dwidth/2;
    dminy = screenpos(2)+screenpos(4)/2-dheight/2;
    
    d = dialog(...
        'Position',[dminx dminy dwidth dheight],...
        'Name','Pick Relationships',...
        'WindowStyle','normal');
    
    margin = 5;
    
    rellist = uicontrol('Parent',d,...
        'Style','listbox',...
        'Min',0,...
        'Max',2,... % for multiple selection
        'HorizontalAlignment','left',...
        'Value',selrelind,...
        'String',relnames,...
        'Position',[margin,margin+25,dwidth-margin,dheight-margin-25]);
    donebutton = uicontrol('Parent',d,...
        'Style','pushbutton',...        
        'Position',[dwidth/2-70/2 margin 70 25],...
        'HorizontalAlignment','center',...    
        'String','Done',...
        'Callback',{@(src,evt) donePressed(d,rellist)});
    
    d.KeyPressFcn = {@(src,evt) keyPressed(src,evt,d,rellist)};
    rellist.KeyPressFcn = {@(src,evt) keyPressed(src,evt,d,rellist)};
    donebutton.KeyPressFcn = {@(src,evt) keyPressed(src,evt,d,rellist)};
    
    uiwait(d);
    
    if isvalid(d)
        selrelnames = get(d,'UserData');
        delete(d);
    else
        selrelnames = {};
    end
end

function donePressed(dlg,rellist)
    set(dlg,'UserData',rellist.String(rellist.Value));
    uiresume(dlg);
end

function keyPressed(src,evt,d,rellist) %#ok<INUSL>
    if strcmp(evt.Key,'return') || strcmp(evt.Key,'d')
        donePressed(d,rellist);
    end
end
