classdef PatternGraphEditor < AxesWidget
    
properties(SetAccess=protected)
    
%     elmrelnames = {...
%         'polygonPolygonCentroiddist',...    
%         'polygonPolygonCentroiddir'};
    
    elmrelnames = {...
        'polygonPolygonCentroiddist',...
        'polygonPolygonCentroiddir',...
        'polygonPolygonDirCentroiddirDist',...
        'polygonPolygonDirCentroiddirDiff',...
        'polygonPolygonCentroiddistRelative',...
        'polygonPolygonOrientationdiff',...
        'polygonPolygonOrientationdist',...
        'polygonPolygonSizediff',...
        };
    
%     'relDirdist',... % => does not work with linear approximation
%     'relDist',... % => does not work with linear approximation
    metarelnames = {...
        'relDiff',...
        'relRatio',...
        'relDirdiff',...
        };
    
    pwidget = PatternWidget.empty;
    
    orientationsnapping = false;
end

properties(Access=protected)
    patternpreloadedListener = event.listener.empty;
    selelmindsListener = event.listener.empty;
    selrelindsListener = event.listener.empty;
    toolListener = event.listener.empty;
    
    uigroup = UIHandlegroup.empty;
    
    gmenu_changeorientations = gobjects(1,0);
    gmenu_changecenters = gobjects(1,0);
    
    gselectbuttons = gobjects(1,0);
    gdefineelementbutton = gobjects(1,0);
    gaddrelsbutton = gobjects(1,0);
    ggroupbutton = gobjects(1,0);
    ghelmbutton = gobjects(1,0);
    ginstgrpbutton = gobjects(1,0);
    gfixelmsbutton = gobjects(1,0);

    lastrelselect = cell(1,0);
    
    mousepos_screen = [];
    clickedpos_screen = [];
end
    
methods
    
function obj = PatternGraphEditor(ax,pwidget,toolpanel,menus)
    
    obj = obj@AxesWidget(ax,'Pattern Graph Editor');
    
    obj.pwidget = pwidget;
    
    obj.uigroup = UIHandlegroup;
    
    obj.gmenu_changeorientations = uimenu(menus.Tools,...
        'Label','Change Element Orientations',...
        'Checked',iif(strcmp(obj.pwidget.tool,'changeorientations'),'on','off'),...
        'Callback', {@(src,evt) obj.changeorientationsPressed});
    obj.uigroup.addChilds(obj.gmenu_changeorientations);
    obj.gmenu_changecenters = uimenu(menus.Tools,...
        'Label','Change Element Centers',...
        'Checked',iif(strcmp(obj.pwidget.tool,'changecenters'),'on','off'),...
        'Callback', {@(src,evt) obj.changecentersPressed});
    obj.uigroup.addChilds(obj.gmenu_changecenters);
    
    obj.gselectbuttons = uibuttongroup(toolpanel,...
        'Title','Select',...
        'Position',[0.21,0,0.2,1],...
        'SelectedObject',[],...
        'SelectionChangedFcn',{@(src,evt) obj.toolbuttonPressed});
    obj.uigroup.addChilds(obj.gselectbuttons);
    obj.uigroup.addChilds(uicontrol(obj.gselectbuttons,...
        'Style','togglebutton',...
        'String','Elements',...
        'Min',0,'Max',1,...
        'Value',iif(strcmp(obj.pwidget.tool,'elementselect'),1,0),...
        'Tag','elementselect',...
        'Units','normalized',...
        'Position',[0,0.66,1,0.34]));
    % apparently the first button added to the button group gets selected,
    % regardless of its value
    obj.gselectbuttons.Children(1).Value = iif(strcmp(obj.pwidget.tool,'elementselect'),1,0);
    obj.uigroup.addChilds(uicontrol(obj.gselectbuttons,...
        'Style','togglebutton',...
        'String','Relationships',...
        'Min',0,'Max',1,...
        'Value',iif(strcmp(obj.pwidget.tool,'relselect'),1,0),...
        'Tag','relselect',...
        'Units','normalized',...
        'Position',[0,0.33,1,0.33]));
    obj.uigroup.addChilds(uicontrol(obj.gselectbuttons,...
        'Style','togglebutton',...
        'String','Groups',...
        'Min',0,'Max',1,...
        'Value',iif(strcmp(obj.pwidget.tool,'groupselect'),1,0),...
        'Tag','groupselect',...
        'Units','normalized',...
        'Position',[0,0,1,0.33]));

    obj.gdefineelementbutton = uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','Define Element',...
        'Callback', {@(src,evt) obj.defineelementPressed},...
        'Units','normalized',...
        'Position',[0.42,0.66,0.25,0.33],...
        'Enable','off');
    obj.uigroup.addChilds(obj.gdefineelementbutton);
    obj.gaddrelsbutton = uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','Add Relationships',...
        'Callback', {@(src,evt) obj.addrelsPressed},...
        'Units','normalized',...
        'Position',[0.42,0.33,0.25,0.33],...
        'Enable','off');
    obj.uigroup.addChilds(obj.gaddrelsbutton);
    obj.ggroupbutton = uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','Group Rel. or Elem.',...
        'Callback', {@(src,evt) obj.groupPressed},...
        'Units','normalized',...
        'Position',[0.42,0.0,0.25,0.33],...
        'Enable','off');
    obj.uigroup.addChilds(obj.ggroupbutton);
    obj.ghelmbutton = uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','Create Hierarchy Element',...
        'Callback', {@(src,evt) obj.helmPressed},...
        'Units','normalized',...
        'Position',[0.67,0.66,0.25,0.33],...
        'Enable','off');
    obj.uigroup.addChilds(obj.ghelmbutton);
    obj.ginstgrpbutton = uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','Create Instance Group',...
        'Callback', {@(src,evt) obj.instgrpPressed},...
        'Units','normalized',...
        'Position',[0.67,0.33,0.25,0.33],...
        'Enable','off');
    obj.uigroup.addChilds(obj.ginstgrpbutton);
    obj.gfixelmsbutton = uicontrol(toolpanel,...
        'Style','pushbutton',...
        'String','Fix Elements',...
        'Callback', {@(src,evt) obj.fixelmsPressed},...
        'Units','normalized',...
        'Position',[0.67,0,0.25,0.33],...
        'Enable','off');
    obj.uigroup.addChilds(obj.gfixelmsbutton);
    
    obj.patternpreloadedListener = obj.pwidget.addlistener('patternpreloaded',@(src,evt) obj.patternpreloadedResponse);
    obj.selelmindsListener = obj.pwidget.addlistener('selelminds','PostSet',@(src,evt) obj.selelmindsChanged);
    obj.selrelindsListener = obj.pwidget.addlistener('selrelinds','PostSet',@(src,evt) obj.selrelindsChanged);
    obj.toolListener = obj.pwidget.addlistener('tool','PostSet',@(src,evt) obj.toolChanged);
    
    obj.hide;
end

function delete(obj)
    obj.clear;
    
    delete(obj.patternpreloadedListener(isvalid(obj.patternpreloadedListener)));
    delete(obj.selelmindsListener(isvalid(obj.selelmindsListener)));
    delete(obj.selrelindsListener(isvalid(obj.selrelindsListener)));
    delete(obj.toolListener(isvalid(obj.toolListener)));
end

function clear(obj)
    obj.pwidget.clear;

    obj.clearLocal;
end

function clearLocal(obj)
    obj.lastrelselect = cell(1,0);
end

function patternpreloadedResponse(obj)
    obj.clearLocal;
end

function toolbuttonPressed(obj)
    if isempty(obj.gselectbuttons.SelectedObject)
        obj.pwidget.tool = '';
    else
        obj.pwidget.tool = obj.gselectbuttons.SelectedObject.Tag;
    end
end

function selrelindsChanged(obj)
    
    if numel(obj.pwidget.selrelinds) >= 1
        obj.ggroupbutton.Enable = 'on';
    elseif numel(obj.pwidget.selelminds) < 1
        obj.ggroupbutton.Enable = 'off';
    end
    
    if numel(obj.pwidget.selrelinds) >= 2
        obj.gaddrelsbutton.Enable = 'on';
        
    elseif numel(obj.pwidget.selelminds) < 2
        obj.gaddrelsbutton.Enable = 'off';
    end
end

function selelmindsChanged(obj)

    if numel(obj.pwidget.selelminds) >= 1
        obj.ggroupbutton.Enable = 'on';
    elseif numel(obj.pwidget.selrelinds) < 1
        obj.ggroupbutton.Enable = 'off';
    end
    
    if numel(obj.pwidget.selelminds) >= 2
        obj.gaddrelsbutton.Enable = 'on';
    elseif numel(obj.pwidget.selrelinds) < 2
        obj.gaddrelsbutton.Enable = 'off';
    end
    
    if numel(obj.pwidget.selelminds) >= 2
        obj.gaddrelsbutton.Enable = 'on';
    elseif numel(obj.pwidget.selrelinds) < 2
        obj.gaddrelsbutton.Enable = 'off';
    end
    
    if numel(obj.pwidget.selelminds) >= 1
        obj.ghelmbutton.Enable = 'on';
    else
        obj.ghelmbutton.Enable = 'off';
    end
    
    if numel(obj.pwidget.selelminds) >= 2
        obj.gdefineelementbutton.Enable = 'on';
    else
        obj.gdefineelementbutton.Enable = 'off';
    end
    
    selelmnodeinds = obj.pwidget.selelminds + obj.pwidget.pgraph.elmoffset;
    if numel(obj.pwidget.selelminds) >= 2 && all(obj.pwidget.pgraph.hlevel(selelmnodeinds) > 0)
        obj.ginstgrpbutton.Enable = 'on';
    else
        obj.ginstgrpbutton.Enable = 'off';
    end
    
    if numel(obj.pwidget.selelminds) >= 1
        obj.gfixelmsbutton.Enable = 'on';
    else
        obj.gfixelmsbutton.Enable = 'off';
    end
end

function toolChanged(obj)
    
    toolbuttonind = find(strcmp({obj.gselectbuttons.Children.Tag},'elementselect'),1,'first');
    if strcmp(obj.pwidget.tool,'elementselect')
        obj.gselectbuttons.Children(toolbuttonind).Value = 1;
    else
        obj.gselectbuttons.Children(toolbuttonind).Value = 0;
    end
    toolbuttonind = find(strcmp({obj.gselectbuttons.Children.Tag},'relselect'),1,'first');
    if strcmp(obj.pwidget.tool,'relselect')
        obj.gselectbuttons.Children(toolbuttonind).Value = 1;
    else
        obj.gselectbuttons.Children(toolbuttonind).Value = 0;
    end
    toolbuttonind = find(strcmp({obj.gselectbuttons.Children.Tag},'groupselect'),1,'first');
    if strcmp(obj.pwidget.tool,'groupselect')
        obj.gselectbuttons.Children(toolbuttonind).Value = 1;
    else
        obj.gselectbuttons.Children(toolbuttonind).Value = 0;
    end
    
    if strcmp(obj.pwidget.tool,'changeorientations')
        obj.gmenu_changeorientations.Checked = 'on';
        
        if not(obj.pwidget.elementorientationsVisible)
            obj.pwidget.toggleElementorientationsVisible;
        end
    else
        obj.gmenu_changeorientations.Checked = 'off';
        
        if obj.pwidget.elementorientationsVisible
            obj.pwidget.toggleElementorientationsVisible;
        end
    end
    
    if strcmp(obj.pwidget.tool,'changecenters')
        obj.gmenu_changecenters.Checked = 'on';
        
        if not(obj.pwidget.elementcentersVisible)
            obj.pwidget.toggleElementcentersVisible;
        end
    else
        obj.gmenu_changecenters.Checked = 'off';
        
        if obj.pwidget.elementcentersVisible
            obj.pwidget.toggleElementcentersVisible;
        end
    end
end

function defineelementPressed(obj)

    selminds = obj.pwidget.selelminds;
    
    if numel(selminds) < 2
        warndlg('Select two or more elements first.');
        return;
    end
    
    mergedelm = ScenePolygonset([obj.pwidget.pgraph.elms(selminds).polygons]);
    
    obj.pwidget.pgraph.removeElements(selminds);
    obj.pwidget.pgraph.addElements(mergedelm);
    
    obj.pwidget.pgraphModified;
    obj.pwidget.updatePGraphlayout;
    
    obj.pwidget.updateMainaxes;
    obj.pwidget.showAxescontents;
end

function addrelsPressed(obj)
    selminds = obj.pwidget.selelminds;
    srelinds = obj.pwidget.selrelinds;
    
    if numel(selminds) ~= 2 && numel(srelinds) ~= 2
        warndlg('Select two elements or two relationships first.');
        return;
    end
    
    if numel(selminds) == 2
        % element relationship
        [~,dialogselrelinds] = ismember(obj.lastrelselect,obj.elmrelnames);
        dialogselrelinds(dialogselrelinds<1) = [];
        selrelnames = reldialog(obj.elmrelnames,dialogselrelinds);
        obj.lastrelselect = selrelnames;
        
        if isempty(selrelnames)
            return;
        end
        
        srcelm = obj.pwidget.pgraph.elms(selminds(1));
        tgtelm = obj.pwidget.pgraph.elms(selminds(2));
        
        selrelnames = selrelnames(FeatureRelationship.isapplicable(selrelnames,[srcelm,tgtelm]));
%         selreltypes = GeomRelationshipType.fromName(selrelnames);
        
        samesrctgtmask = ...
            obj.pwidget.pgraph.elmrelsrcind == selminds(1) & ...
            obj.pwidget.pgraph.elmreltgtind == selminds(2);

        selrelnames(ismember(selrelnames,{obj.pwidget.pgraph.elmrels(samesrctgtmask).type})) = [];
        
        if isempty(selrelnames)
            warning('All selected relationships already exist.');
            return;
        end
        
        r = FeatureRelationship.create(selrelnames,srcelm,tgtelm);
        
        obj.pwidget.pgraph.addElmrels(r,selminds(1),selminds(2));
        
        obj.pwidget.pgraphModified;
        
        if obj.pwidget.relationshipsVisible
            obj.pwidget.showRelationships;
        end
        
    elseif numel(srelinds) == 2
        % meta relationship
        [~,dialogselrelinds] = ismember(obj.lastrelselect,obj.metarelnames);
        dialogselrelinds(dialogselrelinds<1) = [];
        selrelnames = reldialog(obj.metarelnames,dialogselrelinds);
        obj.lastrelselect = selrelnames;
        
        if isempty(selrelnames)
            return;
        end
        
        if srelinds(1) <= numel(obj.pwidget.pgraph.elmrels) && ...
           srelinds(2) <= numel(obj.pwidget.pgraph.elmrels)
            
            srcrel = obj.pwidget.pgraph.elmrels(srelinds(1));
            tgtrel = obj.pwidget.pgraph.elmrels(srelinds(2));
   
        elseif srelinds(1) > numel(obj.pwidget.pgraph.elmrels) && ...
               srelinds(2) > numel(obj.pwidget.pgraph.elmrels)
            
            srcrel = obj.pwidget.pgraph.metarels(srelinds(1)-numel(obj.pwidget.pgraph.elmrels));
            tgtrel = obj.pwidget.pgraph.metarels(srelinds(2)-numel(obj.pwidget.pgraph.elmrels));

        else
            warning('Select to element relationships or two meta-relationships.');
            return;
        end
        
        selrelnames = selrelnames(RelvalRelationship.isapplicable(selrelnames,[srcrel,tgtrel]));
        
%         selreltypes = GeomRelationshipType.fromName(selrelnames);

        srcind = srelinds(1) + numel(obj.pwidget.pgraph.elms);
        tgtind = srelinds(2) + numel(obj.pwidget.pgraph.elms);

        samesrctgtmask = ...
            obj.pwidget.pgraph.metarelsrcind == srcind & ...
            obj.pwidget.pgraph.metareltgtind == tgtind;

        selrelnames(ismember(selrelnames,{obj.pwidget.pgraph.metarels(samesrctgtmask).type})) = [];
        
        if isempty(selrelnames)
            warning('All selected relationships already exist.');
            return;
        end
        
        r = RelvalRelationship.create(selrelnames,srcrel.value,tgtrel.value);
        
        obj.pwidget.pgraph.addMetarels(r,srcind,tgtind);
        
        obj.pwidget.pgraphModified;
        
        if obj.pwidget.relationshipsVisible
            obj.pwidget.showRelationships;
        end
    else
        warning('Select two elements or two relationships first.');
        return;
    end
end

function groupPressed(obj)
    if not(isempty(obj.pwidget.pgraph))

        if not(isempty(obj.pwidget.selelminds))
            
%             obj.pwidget.pgraph.groups(obj.pwidget.selelminds,end+1) = 1;
            obj.pwidget.pgraph.addGroup(obj.pwidget.selelminds,false);
            
        elseif not(isempty(obj.pwidget.selrelinds))
            
%             obj.pwidget.pgraph.groups(obj.pwidget.selrelinds + numel(obj.pwidget.pgraph.elms),end+1) = 1;
            obj.pwidget.pgraph.addGroup(obj.pwidget.selrelinds + numel(obj.pwidget.pgraph.elms),false);
        end

    end
end

function helmPressed(obj)
    if not(isempty(obj.pwidget.pgraph))
        if not(isempty(obj.pwidget.selelminds))
            selelmnodeinds = obj.pwidget.selelminds + obj.pwidget.pgraph.elmoffset;
            
            selelmnodemask = false(1,numel(obj.pwidget.pgraph.nodes));
            selelmnodemask(selelmnodeinds) = true;
            
            hnodeinds = find(any(not(isinf(obj.pwidget.pgraph.nodedist(selelmnodemask,:))),1));
            
            hnodeelminds = hnodeinds - obj.pwidget.pgraph.elmoffset;
            hnodeelminds(hnodeelminds < 0 | hnodeelminds > numel(obj.pwidget.pgraph.elms)) = [];
            
            try
                if not(all(ismember(hnodeelminds,obj.pwidget.selelminds)))
                    error('The selected node are connected to other unselected nodes. Must select a disconnected component of the graph.');
                end
            
                obj.pwidget.pgraph.addHierarchyelement(hnodeinds);
            catch err
                warndlg(['Coud not create hierarchy element: ',err.message]);
            end
            
            obj.pwidget.pgraphModified;
            
            obj.pwidget.showElements;
        end
    end
end

function instgrpPressed(obj)
    if not(isempty(obj.pwidget.pgraph))
        if not(isempty(obj.pwidget.selelminds))
            selelmlevel = obj.pwidget.pgraph.hlevel(obj.pwidget.selelminds + obj.pwidget.pgraph.elmoffset);
            if numel(selelmlevel) < 2  || not(all(selelmlevel > 0))
                warndlg('Must select at least two composite hierarhcy elements.');
                return;
            end
            
            obj.pwidget.pgraph.addInstgroup(obj.pwidget.selelminds);
        end
    end
end

function fixelmsPressed(obj)
    if not(isempty(obj.pwidget.pgraph)) && not(isempty(obj.pwidget.selelminds))
        fixedposeinds = logical(obj.pwidget.pgraph.fixedposeinds(:,obj.pwidget.selelminds + obj.pwidget.pgraph.elmoffset));
        
        obj.pwidget.pgraph.fixPoses(obj.pwidget.selelminds,not(fixedposeinds));
    end
end

function changeorientationsPressed(obj)
    if strcmp(obj.pwidget.tool,'changeorientations')
        obj.pwidget.tool = '';
    else
        obj.pwidget.tool = 'changeorientations';
    end
end

function changecentersPressed(obj)
    if strcmp(obj.pwidget.tool,'changecenters')
        obj.pwidget.tool = '';
    else
        obj.pwidget.tool = 'changecenters';
    end
end

function unblock(obj)
    obj.blockcallbacks = false;
    obj.pwidget.unblock;
end

function mousePressed(obj,src,evt)

    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.mousePressed(src,evt);
    end
    
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;
    
%     obj.selpanel = gparent(obj.fig.CurrentObject,'uipanel',true);
    
    obj.mousedown = true;

    obj.mousepos = mouseposOnPlane(obj.axes);
    obj.clickedpos = obj.mousepos;
    obj.mousepos_screen = mouseScreenpos;
    obj.clickedpos_screen = obj.mousepos_screen;
    
    if obj.pwidget.selpanel == obj.pwidget.mainpanel
        
        if obj.pwidget.navwidget.zoommode || ...
           obj.pwidget.navwidget.panmode || ...
           obj.pwidget.navwidget.orbitmode
       
            % do nothing
            
        elseif strcmp(obj.pwidget.tool,'changeorientations')    
            
            if not(isempty(obj.pwidget.pgraph))
                elmind = PatternWidget.closestElement(...
                    obj.clickedpos(1:2),obj.pwidget.pgraph,obj.pwidget.selhlevel);
                
                obj.pwidget.selelminds = selectMultiple(...
                    obj.pwidget.selelminds,elmind,src.SelectionType);            
                
                obj.pwidget.selrelinds = zeros(1,0);
                obj.pwidget.selgroupinds = zeros(1,0);
            end
        elseif strcmp(obj.pwidget.tool,'changecenters')    
            
            if not(isempty(obj.pwidget.pgraph))
                elmind = PatternWidget.closestElement(...
                    obj.clickedpos(1:2),obj.pwidget.pgraph,obj.pwidget.selhlevel);
                
                obj.pwidget.selelminds = selectMultiple(...
                    obj.pwidget.selelminds,elmind,src.SelectionType);            
                
                obj.pwidget.selrelinds = zeros(1,0);
                obj.pwidget.selgroupinds = zeros(1,0);
            end
        end
    end
    
    obj.blockcallbacks = false;
end


function mouseMoved(obj,src,evt)

    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.mouseMoved(src,evt);
    end
    
    if obj.mousedown

        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;

        obj.mousepos = mouseposOnPlane(obj.axes);
        obj.mousepos_screen = mouseScreenpos;

        if obj.pwidget.selpanel == obj.pwidget.mainpanel
            
            if obj.pwidget.navwidget.zoommode || ...
               obj.pwidget.navwidget.panmode || ...
               obj.pwidget.navwidget.orbitmode

                % do nothing

            elseif strcmp(obj.pwidget.tool,'changeorientations')
                
                dirvec = obj.mousepos(1:2,:) - obj.pwidget.pgraph.elms(obj.pwidget.selelminds(end)).position;
                orientation = cart2pol(dirvec(1),dirvec(2));
                
                if obj.orientationsnapping
                    snapdist = pi/8;
                    orientation = smod(round(orientation/snapdist)*snapdist,-pi,pi);
                end
                
                obj.pwidget.pgraph.elms(obj.pwidget.selelminds).defineOrientation(orientation);

                disp(orientation * (180/pi));

                changedrinds = obj.pwidget.pgraph.node2relinds(obj.pwidget.pgraph.ancestors(obj.pwidget.selelminds));
                obj.pwidget.pgraph.recomputeRelationships(changedrinds);

                obj.pwidget.showElementorientations;

                % re-create all instance groups containing the element
                % (elment ordering might need to change)
                obj.pwidget.selinstgroupinds = zeros(1,0);

                selelmnodeind = obj.pwidget.selelminds(end) + obj.pwidget.pgraph.elmoffset;
                instgroupinds = find(obj.pwidget.pgraph.instgroups(selelmnodeind,:));
                for i=1:numel(instgroupinds)
                    instgrpmembernodeinds = find(obj.pwidget.pgraph.instgroups(:,instgroupinds(i)));

                    obj.pwidget.pgraph.removeInstgroups(instgroupinds(i));
                    obj.pwidget.pgraph.addInstgroup(instgrpmembernodeinds); %#ok<FNDSB>
                end
                
            elseif strcmp(obj.pwidget.tool,'changecenters')
                obj.pwidget.pgraph.elms(obj.pwidget.selelminds).definePosition(obj.mousepos(1:2));
                
                changedrinds = obj.pwidget.pgraph.node2relinds(obj.pwidget.pgraph.ancestors(obj.pwidget.selelminds));
                obj.pwidget.pgraph.recomputeRelationships(changedrinds);
                
                obj.pwidget.showElementcenters;
            end
        end
        
        obj.blockcallbacks = false;
    end
    
end

function mouseReleased(obj,src,evt)

    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.mouseReleased(src,evt);
    end
    
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;
    
    if obj.pwidget.selpanel == obj.pwidget.mainpanel
        if obj.pwidget.navwidget.zoommode || ...
           obj.pwidget.navwidget.panmode || ...
           obj.pwidget.navwidget.orbitmode

            % do nothing

        end
    end
    
    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt)
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.keyPressed(src,evt);
    end
    
    % tool-specific keys
    if strcmp(obj.pwidget.tool,'elementselect')
        if strcmp(evt.Key,'delete')
            if not(isempty(obj.pwidget.pgraph)) && not(isempty(obj.pwidget.selelminds))
                selminds = obj.pwidget.selelminds;
                obj.pwidget.selelminds = zeros(1,0);
                obj.pwidget.selrelinds = zeros(1,0);
                obj.pwidget.selgroupinds = zeros(1,0);

                obj.pwidget.pgraph.removeElements(selminds);

                obj.pwidget.pgraphModified;

                obj.pwidget.updateMainaxes;

                obj.pwidget.updatePGraphlayout;
                obj.pwidget.showAxescontents;
            end
        elseif strcmp(evt.Key,'a')
            obj.addrelsPressed;
        end
    elseif strcmp(obj.pwidget.tool,'relselect')
        if strcmp(evt.Key,'delete')
            if not(isempty(obj.pwidget.pgraph)) && not(isempty(obj.pwidget.selrelinds))
                srelinds = obj.pwidget.selrelinds;
                obj.pwidget.selelminds = zeros(1,0);
                obj.pwidget.selrelinds = zeros(1,0);
                obj.pwidget.selgroupinds = zeros(1,0);

                obj.pwidget.selrelinds = [];

                obj.pwidget.pgraph.removeRelationships(srelinds);

                obj.pwidget.pgraphModified;

                obj.pwidget.updatePGraphlayout;
                obj.pwidget.showRelationships;
            end
        elseif strcmp(evt.Key,'a')
            obj.addrelsPressed;    
        end
    elseif strcmp(obj.pwidget.tool,'groupselect')
        if strcmp(evt.Key,'delete')
            if not(isempty(obj.pwidget.pgraph)) && not(isempty(obj.pwidget.selgroupinds))
                sgroupinds = obj.pwidget.selgroupinds;
                obj.pwidget.selelminds = zeros(1,0);
                obj.pwidget.selrelinds = zeros(1,0);
                obj.pwidget.selgroupinds = zeros(1,0);

                obj.pwidget.pgraph.removeGroups(sgroupinds);

                obj.pwidget.pgraphModified;

                obj.pwidget.updatePGraphlayout;
                obj.pwidget.showRelationships;
            end
        end
    elseif strcmp(obj.pwidget.tool,'instgroupselect')
        if strcmp(evt.Key,'delete')
            if not(isempty(obj.pwidget.pgraph)) && not(isempty(obj.pwidget.selinstgroupinds))
                sinstgroupinds = obj.pwidget.selinstgroupinds;
                obj.pwidget.selelminds = zeros(1,0);
                obj.pwidget.selrelinds = zeros(1,0);
                obj.pwidget.selgroupinds = zeros(1,0);

                obj.pwidget.selinstgroupinds = zeros(1,0);

                obj.pwidget.pgraph.removeInstgroups(sinstgroupinds);

                obj.pwidget.pgraphModified;

                obj.pwidget.updatePGraphlayout;
                obj.pwidget.showRelationships;
            end
        end
    elseif strcmp(obj.pwidget.tool,'changeorientations')
        if strcmp(evt.Key,'x')
            obj.orientationsnapping = true;
        end
    end
end

function keyReleased(obj,src,evt)
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.keyReleased(src,evt);
    end
    
    if strcmp(obj.pwidget.tool,'changeorientations')
        if strcmp(evt.Key,'x')
            obj.orientationsnapping = false;
        end
    end
end

function show(obj)
    obj.show@AxesWidget;
    
    if isvalid(obj.uigroup)
        obj.uigroup.show;
    end
    
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.show;
    end
    
    if isvalid(obj.selelmindsListener)
        obj.selelmindsListener.Enabled = true;
    end
    if isvalid(obj.selrelindsListener)
        obj.selrelindsListener.Enabled = true;
    end
    if isvalid(obj.toolListener)
        obj.toolListener.Enabled = true;
    end
    
    if not(obj.pwidget.relationshipsVisible)
        obj.pwidget.toggleRelationshipsVisible;
    end
    
    if not(obj.pwidget.editorfig.toolpanelVisible)
        obj.pwidget.editorfig.toggleToolpanelVisible;
    end
    
    if not(obj.pwidget.explicithierarchyelements)
        obj.pwidget.toggleExplicithierarchyelements;
    end
    
    obj.pwidget.tool = 'elementselect';
end

function hide(obj)
    obj.hide@AxesWidget;
    
    if isvalid(obj.uigroup)
        obj.uigroup.hide;
    end
    
    if not(isempty(obj.pwidget)) && isvalid(obj.pwidget)
        obj.pwidget.hide;
    end
    
    if isvalid(obj.selelmindsListener)
        obj.selelmindsListener.Enabled = false;
    end
    if isvalid(obj.selrelindsListener)
        obj.selrelindsListener.Enabled = false;
    end
    if isvalid(obj.toolListener)
        obj.toolListener.Enabled = true;
    end
end

end

end