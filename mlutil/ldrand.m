% Low-discrepancy pseudo-random numbers using the Halton sequence.
% Note that only the numbers returned in one call have low discrepancy, the
% numbers returned from two different calls do generally not have low
% discrepancy.
function ldr = ldrand(ndim,n)
    persistent hsetmap;

    if isempty(hsetmap)
        hsetmap = containers.Map('KeyType','double','ValueType','any');
    end
    
    % check if a Halton set with the given dimension exists, if not create
    % one
    if hsetmap.isKey(ndim)
        hset = hsetmap(ndim);
    else
        hset = haltonset(ndim,'Skip',1000);
        hset.scramble('RR2');    
        hsetmap(ndim) = hset;
    end
    
    % get a n-element subsequence of the Halton sequence from a random
    % starting index
    randstartind = randi(min(hset.length-n+1,2^53-1)); % 2^53-1 is max input for randi
	ldr = hset(randstartind:randstartind+n-1,:)';
end
