% extract non-overlapping cliques of a graph, greedily extracting the
% cliques with highest internal weight first (sum of all similarities in a
% clique, => favoring large cliques)
% cliques of one element have zero weight because diagonal is zero
function [cliques,w] = extractCliques(A)
    
    mc = logical(maximalCliques(logical(A)));
    
    cliques = {};
    w = [];
    
    vertinds = 1:size(mc,1);
    while not(isempty(mc))
        cw = zeros(1,size(mc,2));
        
        for i=1:size(mc,2)
%             cw(i) = sum(sum( A(vertinds(mc(:,i)),vertinds(mc(:,i))) ))/2; % half because edges are summed twice
            
            nverts = sum(mc(:,i));
            cw(i) = sum(sum( A(vertinds(mc(:,i)),vertinds(mc(:,i))) )); % half because edges are summed twice
            if nverts > 1
                cw(i) = cw(i) / (nverts^2-nverts); % nverts^2-nverts elements in the sum (diagonal is zero)
                cw(i) = cw(i) * nverts; % weight by the number of elements (favoring larger cliques)
            end
        end
    
        [maxw,maxind] = max(cw);
        
        cliques{end+1} = vertinds(mc(:,maxind)); %#ok<AGROW>
        if numel(cliques{end}) == 1
            w(end+1) = 1; %#ok<AGROW>
        else
            w(end+1) = maxw/sum(mc(:,maxind)); %#ok<AGROW>
            % divide by number of vertices to get the mean of the
            % similarities
        end
        
        vertinds(mc(:,maxind)) = [];
        mc(mc(:,maxind),:) = []; % clear vertices from mc (other cliques remain cliques)
        mc(:,maxind) = [];  % clear clique from mc
    end
end
