% KL-divergence
function kldiv = normkldiv(mean1,sdev1,mean2,sdev2)

    % for derivation see http://stats.stackexchange.com/questions/7440/kl-divergence-between-two-univariate-gaussians
    kldiv = log(sdev2./sdev1) + (sdev1.^2 + (mean1-mean2).^2) ./ (2.*sdev2.^2) - 0.5;
end
