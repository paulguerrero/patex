% n real numbers with equal spacing in log space or linear space
% lin blends between equal spacing in linear (lin=1) and log space (lin=0)
% shiftinv makes the sequence shift invariant by shifting the interval to
% start at 1
function y = linlogspace(x1,x2,n,lin,shiftinv)
    
    if nargin < 5 || isempty(shiftinv)
        shiftinv = false;
    end

    lin = min(1,max(0,lin));
    
    if lin > 0.99999
        % to avoid numeric issues
        y = linspace(x1,x2,n);
        return;
    end
    
    if shiftinv
        % shift the interval [x1,x2] so it starts at 1
        shift = x1-1;
        x2 = x2-shift;
        x1 = 1;
    end
    
    logblendf = log(1-lin);
    y = exp(linspace(log(x1)+logblendf,log(x2)+logblendf,n)) + ...
        linspace(x1*lin,x2*lin,n);
    
    if shiftinv
        % shift the interval back to its original position
        y = y+shift;
    end

end
