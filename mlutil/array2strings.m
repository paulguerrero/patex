function s = array2strings(a,formatSpec,delimiter)
    if nargin < 2
        formatSpec = '%f';
    end
    
    if nargin < 3
        delimiter = char(7); % hopefully not used in format string
        formatSpec(:,end+1) = delimiter;
    end
    
    s = textscan(sprintf(formatSpec,a),'%s','Delimiter',delimiter);
    s = s{1};
end
