function saveToPoly(filename,verts,edges,boundarymarkers,holepoints)

    % create the .poly file
    fileid = fopen(filename,'w+');

    % write vertices
    fprintf(fileid,'%d %d %d %d\n',size(verts,2),2,0,1);
    for i=1:size(verts,2)
        fprintf(fileid,'%d\t%5.10f\t%5.10f\t%d\n',i,verts(1,i),verts(2,i),boundarymarkers(i));
    end

    % write edges
    fprintf(fileid,'%d %d\n',size(edges,2),1);
    for i=1:size(edges,2)
        fprintf(fileid,'%d\t%d\t%d\t%d\n',i,edges(1,i),edges(2,i),boundarymarkers(i));
    end

    if nargin >= 5
        % write holes
        fprintf(fileid,'%d\n',size(holepoints,2));
        for i=1:size(holepoints,2)
            fprintf(fileid,'%d\t%5.10f\t%5.10f\n',i,holepoints(1,i),holepoints(2,i));
        end
    end

    fclose(fileid);
end
