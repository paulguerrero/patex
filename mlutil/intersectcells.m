function vals = intersectcells(A)
    A=cellfun(@unique,A,'UniformOutput',false);
    X=horzcat(A{:});
    [U,I]=unique(sort(X),'first');
    value_count=diff([I' length(X)+1]);
    vals=U(value_count==length(A));
end
