% compute the representative vector of vectors in a n-rosy field
% input and output can be in polar or cartesian representation (polar is
% faster), one 2xn matix for cartesian, two 1xn matrices for polar
function [o1,o2] = rosyrep(in1,in2,in3)
    if nargin == 2
        [angle,len] = cart2pol(in1(1,:),in1(2,:));
        n = in2;
    elseif nargin == 3
        angle = in1;
        len = in2;
        n = in3;
    else
        error('at least two input arguments required');
    end
    
    if nargout <= 1
        o1 = [len.*cos(n*angle);len.*sin(n*angle)];
%         [o1(1,:),o1(2,:)] = pol2cart(angle.*n,len);
    else
        o1 = mod(angle .* n,2*pi);
%         mask = o1>pi;
%         o1(mask) = -2*pi+o1(mask);
        o2 = len;
    end
end
