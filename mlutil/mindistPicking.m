% Problem:
% Given a set of elements that are each member of one category.
% Pick one element of each category such that the average distance
% between all pairs of picked elements is minimal.
% Repeat n times and throw away empty categories (stop if all elements have
% been picked)
% This method uses a greedy growing method to find a good solution
function pickedsets = mindistPicking(dists,cid,nrepeats)
    ciddists = squareform(pdist(cid'));
    dists(ciddists == 0) = inf;
    
    clear ciddists;
    
%     nc = numel(unique(cid));
    pickedsets = minedgecostClique(dists,nrepeats);
end
