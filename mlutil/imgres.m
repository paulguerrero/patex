% aspect is width/height
% res is [width;height]
% pixcenters: true - aspect width/height is between corner pixel centers
%             false - aspect width/height is between corner pixel corners
function res = imgres(aspect,npixels,pixcenters)
    
    if nargin < 3 || isempty(pixcenters)
        pixcenters = true;
    end
    
    res = zeros(2,1);
    if pixcenters
        % solution to:
        % width*height = npixels
        % (width-1) / (height-1) = aspect
        res(1) = 0.5 .* (sqrt(4.*npixels.*aspect + aspect.^2 - 2.*aspect + 1) - aspect + 1);
        res(2) = (sqrt(4.*npixels.*aspect + aspect.^2 - 2.*aspect + 1) + aspect - 1)./(2.*aspect);
    else
        % solution to:
        % width*height = npixels
        % width / height = aspect
        res(1) = sqrt(npixels*aspect);
        res(2) = sqrt(npixels/aspect);
    end
    
    res = max(1,round(res));
end
