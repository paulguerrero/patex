function d = mixedbasis2de(mb,p)

    b = [1,cumprod(p)];
    
    if b(end) > 2^50
        % mantissa of double has 52 bit
        warning('mixedbasis2de:toolarge','Number is too large, output might have numeric errors.');
    end
    
    d = sum(mb .* b(1:end-1));
end
