% subset of csv:
% all fields must be enclosed by double quotation marks
% no line breaks in fields
% no spaces between commas and double quotation marks
% first line is header with field names
function result = csvread(filename,ignoreunmatched)
    
    if nargin < 2 || isempty(ignoreunmatched)
        ignoreunmatched = false;
    end

    fid = fopen(filename,'rt');
    
    % read header
    headerstr = fgetl(fid);
    headerstr([1,end]) = []; % remove leading and trailing double q. mark
    fieldnames = regexp(headerstr,'","','split');
    
    % dots can't be in field names, replace with underscore
    fieldnames = cellfun(@(x) strrep(x,'.','_'),fieldnames,'UniformOutput',false);
    
    allrecords = {};
    
    % read a single record (one line of the csv file)
    recordstr = fgetl(fid);
    while numel(recordstr) >= 2 && strcmp(recordstr([1,end]),'""')
        recordstr([1,end]) = []; % remove leading and trailing double q. mark
        record = regexp(recordstr,'","','split');
        allrecords = [allrecords;record]; %#ok<AGROW>
        recordstr = fgetl(fid);
    end
    
    fclose(fid);

    if ignoreunmatched
        nfields = numel(fieldnames);
        allrecords(:,size(allrecords,2)+1:nfields) = {''};
        allrecords = allrecords(:,1:nfields);
    end
    
    result = cell2struct(allrecords,fieldnames,2);
end
