function obj = constructempty(c,varargin)
    if not(ischar(c))
        c = class(c);
    end
    
    h = str2func([c,'.empty']);
    obj = h(varargin{:});
end
