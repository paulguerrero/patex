function mcells = matrixsplit(m,dim)
%MATRIXSPLIT Split matrix into cell array of sub-matrices along a given
%dimension.
%
%   MCELLS = MATRIXSPLIT(M,DIM) splits matrix m along dimension dim, at all
%   rows/columns that contain NaN (the rows/columns themselves are removed)

if nargin < 2
    dim = 1;
end

if not(ismatrix(m))
    error('Only matrices supported.');
end
if dim ~= 1 && dim ~= 2
    error('Dimension index must be 1 or 2.');
end

if isempty(m)
    if dim == 2
        mcells = cell(1,0);
    else
        mcells = cell(0,1);
    end
else
    inds = find(any(isnan(m),3-dim));
    if iscolumn(inds)
        inds = inds';
    end
    
    startinds = [1,inds+1];
    endinds = [inds-1,size(m,dim)];
    
    mask = startinds > endinds;
    startinds(mask) = [];
    endinds(mask) = [];
    
    if dim == 2
        mcells = cell(1,numel(startinds));
        for i=1:numel(startinds)
            mcells{i} = m(:,startinds(i):endinds(i));
        end
    else
        mcells = cell(numel(startinds),1);
        for i=1:numel(startinds)
            mcells{i} = m(startinds(i):endinds(i),:);
        end
    end
end
