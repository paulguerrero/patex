function I = rgb2nind(I,cmap)
    I = double(rgb2ind(I,cmap)) ./ (size(cmap,1)-1); % rgb2ind returns integers in [0,cmapsize-1]
end
