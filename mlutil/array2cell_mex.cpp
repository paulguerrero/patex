#include "mex.h"

#include<vector>
#include<algorithm>

// usage:
// [cellarray,nonemptyinds] = array2cell_mex(array,cellinds)
// [cellarray,nonemptyinds] = array2cell_mex(array,cellinds,cellcount)
// [cellarray,nonemptyinds] = array2cell_mex(array,cellinds,cellcount,cellshape)
// array: values to put into cells
// cellinds: cell index of each value (must have same dimensions as values)
// cellcount: number of cells in the cell array, all cellinds must be <= cellcount
// cellshape: non-singleton dimension of the array in each cell

struct by_second { 
    bool operator()(std::pair<double,mwIndex> const &a, std::pair<double,double> const &b) { 
        return a.second < b.second;
    }
};

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    // outputs
    if (nrhs < 2 || nrhs > 4) {
        mexErrMsgTxt("Two to four inputs required.");
    }
    
    if(nlhs < 1 || nlhs > 2) {
        mexErrMsgTxt("One or two outputs required.");
    }
    
//     mexPrintf("1\n");
//     mexEvalString("drawnow;");

    // get number of elements in array and validate arrays
    if (!mxIsClass(prhs[0],"double") || !mxIsClass(prhs[1],"double")) {
        mexErrMsgTxt("Array and indices must be of type double.");
    }
    mwSize nelm = 1;
    for (mwIndex i=0; i<mxGetNumberOfDimensions(prhs[0]); i++) {
        if (mxGetDimensions(prhs[0])[i] != mxGetDimensions(prhs[1])[i]) {
            mexErrMsgTxt("Array and indices do not have the same size.");
        }
        nelm *= mxGetDimensions(prhs[0])[i];
    }
    
    // get requested cell count (if given)
    bool hascellcount = false;
    mwSize cellcount = 0;
    if (nrhs >= 3 && !mxIsEmpty(prhs[2])) {
        if (!mxIsClass(prhs[2],"double")) {
            mexErrMsgTxt("Third input must be a double scalar.");
        }
        if (mxGetNumberOfDimensions(prhs[2]) != 2) {
            mexErrMsgTxt("Third input must be a double scalar.");
        }
        if (mxGetM(prhs[2]) != 1 || mxGetN(prhs[2]) != 1 ) {
            mexErrMsgTxt("Third input must be a double scalar.");
        }
        if (mxGetScalar(prhs[2]) < 0) {
            mexErrMsgTxt("Invalid cell count, must be >= 0.");
        }
        cellcount = (mwSize) mxGetScalar(prhs[2]);
        hascellcount = true;
    }
    
    // get non-singleton dimension of the array in each cell
    mwSize cellshape = 0;
    if (nrhs >= 4 && !mxIsEmpty(prhs[3])) {
        if (!mxIsClass(prhs[3],"double")) {
            mexErrMsgTxt("Fourth input must be a double scalar.");
        }
        if (mxGetNumberOfDimensions(prhs[3]) != 2) {
            mexErrMsgTxt("Fourth input must be a double scalar.");
        }
        if (mxGetM(prhs[3]) != 1 || mxGetN(prhs[3]) != 1 ) {
            mexErrMsgTxt("Fourth input must be a double scalar.");
        }
        if (mxGetScalar(prhs[3]) < 1) {
            mexErrMsgTxt("Invalid cell shape, must be >= 1.");
        }
        cellshape = (mwSize) (mxGetScalar(prhs[3])-1); // to c indexing
    }
    
    // get list of pairs (value,cell index)
    double* val_p = mxGetPr(prhs[0]);
    double* ind_p = mxGetPr(prhs[1]);
    std::vector<std::pair<double,mwIndex> > val_ind(nelm);
    for (mwIndex i=0; i<nelm; i++) {
        if (ind_p[i] < 1.0) {
            mexErrMsgTxt("Invalid cell index.");
        }
        val_ind[i].first = val_p[i];
        val_ind[i].second = (mwIndex) (ind_p[i]-1);
    }
    
    // sort pairs by cell index
    std::stable_sort(val_ind.begin(),val_ind.end(),by_second());
    
    // get cell count from maximum index or check if given cell count is >= maximum index
    if (nelm > 0) {
        mwIndex maxind = val_ind.rbegin()->second;
        if (hascellcount && cellcount < maxind+1) {
            mexErrMsgTxt("Index out of bounds for given cell count.");
        }
        cellcount = std::max(cellcount,maxind+1);
    }

    mwSize cellndim = std::max(cellshape+1,(mwSize) 2);
    mwSize* celldims = new mwSize[cellndim];
    std::fill(celldims,celldims+cellndim,1);
    celldims[cellshape] = cellcount;

    // reserve space for non-empty indices if requested
    std::vector<double> nonemptyinds;
    if (nlhs >= 2) {
        nonemptyinds.reserve(cellcount);
    }

    // check if given array is empty
    if (nelm == 0) {
        plhs[0] = mxCreateCellMatrix(1,cellcount);
        if (nlhs >= 2) {
            plhs[1] = mxCreateDoubleMatrix(1,0,mxREAL);
        }
        
        // give empty cells the right shape
        for (mwIndex i=0; i<cellcount; i++) {
            // create cell
            celldims[cellshape] = 0;
            mxArray* cell = mxCreateNumericArray(cellndim,celldims,mxDOUBLE_CLASS,mxREAL);

            // assign cell to cell array
            mxSetCell(plhs[0],i,cell);
        }
        return;
    }
    

    
    // put values in cell array
    plhs[0] = mxCreateCellMatrix(1,cellcount);
//     plhs[0] = mxCreateCellArray(cellndim,celldims);
    
    std::vector<bool> cellisempty(cellcount,true);
    
    mwIndex startind = 0;
    for (mwIndex i=0; i<nelm; i++) {
        // check if next cell index is different
        if (i+1 >= nelm || val_ind[i+1].second != val_ind[startind].second) {
            // create cell
//             mxArray* cell = mxCreateDoubleMatrix((i+1)-startind,1,mxREAL);
            celldims[cellshape] = (i+1)-startind;
            mxArray* cell = mxCreateNumericArray(cellndim,celldims,mxDOUBLE_CLASS,mxREAL);
            
            // copy values to cell
            double* cellelm_p = mxGetPr(cell);
            for (mwIndex k=startind; k<=i; k++) {
                *cellelm_p++ = val_ind[k].first;
            }
            
            // assign cell to cell array
            mwIndex cellind = val_ind[startind].second;
            mxSetCell(plhs[0],cellind,cell);
            
            // save index of current non-empty cell if requested
            if (nlhs >= 2) {
                nonemptyinds.push_back((double) (cellind+1));
            }
            
            cellisempty[cellind] = false;
            
            // update starting index for next cell
            startind = i+1;
        }
    }
    
    // give empty cells the right shape
    for (mwIndex i=0; i<cellcount; i++) {
        if (cellisempty[i]) {
            // create cell
            celldims[cellshape] = 0;
            mxArray* cell = mxCreateNumericArray(cellndim,celldims,mxDOUBLE_CLASS,mxREAL);
            
            // assign cell to cell array
            mxSetCell(plhs[0],i,cell);
        }
    }
    
    delete[] celldims;
    
    // create output for non-empty cell indices if requested
    if (nlhs >= 2) {
        plhs[1] = mxCreateDoubleMatrix(1,nonemptyinds.size(),mxREAL);
        std::copy(nonemptyinds.begin(),nonemptyinds.end(),mxGetPr(plhs[1]));
    }
}
