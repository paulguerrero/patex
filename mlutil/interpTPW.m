function [mpx,mpy] = interpTPW(x,y,mappedx,mappedy,px,py)
    
%     % scattered data interpolation with delaunay triangulation
%     fi = TriScatteredInterp(x',y',mappedx','linear');
%     mpx = fi(px',py');
%     fi.V = mappedy';
%     mpy = fi(px',py');

%     % scattered data interpolation with log kernels
%     mpx = zeros(1,numel(px));
%     mpy = zeros(1,numel(px));
%     width = 0.3;
%     for i=1:numel(px)
%         dists = sqrt((x-px(i)).^2 + ...
%                      (y-py(i)).^2);
% %         weights = 1./log(dists+1);
%         weights = exp(-(dists.^2)./(2.*width.^2));
% %         weights = log(1./dists);
% %         weights = 1./log(dists);
% %         weights = 1./dists;
% %         weights = 1./exp(dists.*10);
%         ind = find(isinf(weights),1,'first');
%         if not(isempty(ind))
%             % if there is an inf weight set the mapped value to the
%             % value at the point with inf weight
%             mpx(i) = mappedx(ind);
%             mpy(i) = mappedy(ind);
%         else
%             mpx(i) = sum(mappedx.*weights)./sum(weights);
%             mpy(i) = sum(mappedy.*weights)./sum(weights);
%         end
%     end
    
    % thin plate warp
	dists = squareform(pdist([x;y]'));
    K = dists.^2 .* log(dists.^2);
    K(logical(eye(size(K)))) = 0; % zero out diagonal
    P = [ones(1,numel(x));x;y]';
    L = [K,P;P',zeros(3)];
    Yx = [mappedx,zeros(1,3)]';
    Yy = [mappedy,zeros(1,3)]';
    Wx = L\Yx;
    Wy = L\Yy;
    Ax = Wx(end-2:end);
    Ay = Wy(end-2:end);
    Wx = Wx(1:end-3);
    Wy = Wy(1:end-3);
    
    dists = pdist2([x;y]',[px;py]');
    mpx = Ax(1) + Ax(2).*px + Ax(3).*py + ... % affine part
        sum(repmat(Wx,1,numel(px)).*(dists.^2 .* log(dists.^2)),1); % thin plate part
    mpy = Ay(1) + Ay(2).*px + Ay(3).*py + ... % affine part
        sum(repmat(Wy,1,numel(px)).*(dists.^2 .* log(dists.^2)),1); % thin plate part
    clear dists;
end
