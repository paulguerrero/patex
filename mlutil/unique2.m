function [C,ia,iall] = unique2(A)
    
    if numel(A) >= 2
        if not(isrow(A))
            error('Input array has to be a single row.');
        end
        
        [A,perm] = sort(A,'ascend');

        diffinds = find(A(2:end)~=A(1:end-1));
        startinds = [1,diffinds+1];

        C = A(startinds);
        
        if nargout >= 2
            ia = perm(startinds);

            if nargout >= 3
                endinds = [diffinds,numel(A)];
                iall = mat2cell(perm,1,endinds-startinds+1);
            end
        end
    elseif numel(A) == 1
        C = A;
        if nargout >= 2
            ia = 1;
            if nargout >= 3
                iall = {1};
            end
        end
    elseif numel(A) == 0
        C = A;
        ia = [];
        iall = {};
    end

end
