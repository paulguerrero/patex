% last channel is assumed to be alpha
function img1 = imblend(img1,img2,method)
        
    if strcmp(method,'add')
        npix = size(img1,1)*size(img1,2);
        pixinds = find(img2(:,:,end) > 0);
        if not(isempty(pixinds))
            apixinds = pixinds + npix*(size(img1,3)-1);
            pixl1alpha = img1(apixinds);
            oneminuspixl2alpha = 1-img2(apixinds);
            fl2 = img2(apixinds);
            fl1 = oneminuspixl2alpha .* pixl1alpha;

            linds = pixinds;
            
%             disp(numel(linds));

            for i=1:size(img1,3)-1
%                 cdata(pixinds+npix*(i-1)) = ...
%                     obj.brushstrokelayer(pixinds+npix*(i-1)) .* obj.brushstrokelayeralpha(pixinds) + ...
%                     cdata(pixinds+npix*(i-1)) .* (1-obj.brushstrokelayeralpha(pixinds)) .* adata(pixinds) + ...
%                     obj.brushstrokelayer(pixinds+npix*(i-1)) .*  (1-obj.brushstrokelayeralpha(pixinds)) .* (1-adata(pixinds));
% 
%                 cdata(cdatainds) = ...
%                     (obj.brushstrokelayer(cdatainds) .* obj.brushstrokelayeralpha(pixinds) + ...
%                     cdata(cdatainds) .* (1-obj.brushstrokelayeralpha(pixinds)) .* adata(pixinds)) ./ ...
%                     (obj.brushstrokelayeralpha(pixinds) + (1-obj.brushstrokelayeralpha(pixinds)) .* adata(pixinds));


                if numel(img2) == size(img1,3)
                    img1(linds) = ...
                        (img1(linds) .* fl1 + img2(i) .* fl2) ./ (fl1 + fl2);
                else
                    img1(linds) = ...
                        (img1(linds) .* fl1 + img2(linds) .* fl2) ./ (fl1 + fl2);
                end

                linds = linds + npix;
            end

            img1(apixinds) = ...
                1 - (1-pixl1alpha) .* oneminuspixl2alpha;
        end
    elseif strcmp(method,'max')
        npix = size(img1,1)*size(img1,2);
        pixinds = find(img2(:,:,end) > img1(:,:,end));
        if not(isempty(pixinds))
            apixinds = pixinds + npix*(size(img1,3)-1);
            
            linds = pixinds;
            
            for i=1:size(img1,3)-1
                if numel(img2) == size(img1,3)
                    img1(linds) = img2(i);
                else
                    img1(linds) = img2(linds);
                end

                linds = linds + npix;
            end
            img1(apixinds) = img2(apixinds);
        end

    else
        error('Unknown blend type.');
    end
end
