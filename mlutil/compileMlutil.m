function compileMlutil(options)
    
    % unload all currently loaded mex files
    clear mex; %#ok<CLMEX>
    
    % get compile options
    if any(strcmp(options,'debug'))
        additionaloptions = {'-g'};
    else
        additionaloptions = {};
    end

    % compile files
    try
        mex('-largeArrayDims',additionaloptions{:},'array2cell_mex.cpp');
        mex('-largeArrayDims',additionaloptions{:},'cellind_mex.cpp');
        mex('-largeArrayDims',additionaloptions{:},'cellisempty_mex.cpp');
        mex('-largeArrayDims',additionaloptions{:},'cellselect_mex.cpp');
        mex('-largeArrayDims',additionaloptions{:},'celldiff_mex.cpp');
        mex('-largeArrayDims',additionaloptions{:},'strjoin_mex.cpp','mexutils.cpp');
    catch err
        if exist(fullfile(pwd,'precompiled.conf'),'file') == 2
            delete('precompiled.conf')
        end
        rethrow(err);
    end
    
    % update precompiled info
    updatePrecompiledInfo('precompiled.conf',{...
        'array2cell_mex',...
        'cellind_mex',...
        'cellisempty_mex',...
        'cellselect_mex',...
        'celldiff_mex',...
        'strjoin_mex'},...
        'C++');
    
    disp('done');
end
