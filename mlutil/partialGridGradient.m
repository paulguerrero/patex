% gradient in a partial 2d grid (i.e. some nodes are missing), not
% necessarily equally spaced
% f: function values (1xn)
% p: point positions (2xn)
% neighbours: neighbour indices for each point (4xn):
% indices into 'p' for left,right,up,down neighbours for each of the points
function g = partialGridGradient(f,p,neighbours)

    g = zeros(2,size(p,2));

    % x direction
    % centered differences for points having left & right neighbours
    mask = neighbours(1,:)>0 & neighbours(2,:)>0;
    g(1,mask) = (f(neighbours(2,mask)) - f(neighbours(1,mask))) ...
                ./ ...
                (p(1,neighbours(2,mask)) - p(1,neighbours(1,mask)));

    % forward differences for points having only left neighbours
    mask = neighbours(1,:)>0 & neighbours(2,:)==0;
    g(1,mask) = (f(mask) - f(neighbours(1,mask))) ...
                ./ ...
                (p(1,mask) - p(1,neighbours(1,mask)));

    % forward differences for points having only right neighbours
    mask = neighbours(1,:)==0 & neighbours(2,:)>0;
    g(1,mask) = (f(neighbours(2,mask)) - f(mask)) ...
                 ./ ...
                (p(1,neighbours(2,mask)) - p(1,mask));

    % zero for points having no neighbours
    mask = neighbours(1,:)==0 & neighbours(2,:)==0;
    g(1,mask) = 0;

    % y direction
    % centered differences for points having up & down neighbours
    mask = neighbours(4,:)>0 & neighbours(3,:)>0;
    g(2,mask) = (f(neighbours(3,mask)) - f(neighbours(4,mask))) ...
                ./ ...
                (p(2,neighbours(3,mask)) - p(2,neighbours(4,mask)));

    % forward differences for points having only down neighbours
    mask = neighbours(4,:)>0 & neighbours(3,:)==0;
    g(2,mask) = (f(mask) - f(neighbours(4,mask))) ...
                ./ ...
                (p(2,mask) - p(2,neighbours(4,mask)));

    % forward differences for points having only up neighbours
    mask = neighbours(4,:)==0 & neighbours(3,:)>0;
    g(2,mask) = (f(neighbours(3,mask)) - f(mask)) ...
                 ./ ...
                (p(2,neighbours(3,mask)) - p(2,mask));

    % zero for points having no neighbours
    mask = neighbours(4,:)==0 & neighbours(3,:)==0;
    g(2,mask) = 0;
end
