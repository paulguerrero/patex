classdef (HandleCompatible) JSONifiable

methods

function jsonstr = obj2json(obj,filename,rootname)
    
    if nargin < 2 || isempty(filename)
        filename = '';
    end
    
    if nargin < 3 || isempty(rootname)
        rootname = '';
    end
    
    jsonstr = savejson(rootname,obj,'FileName',filename);
end

end

end
