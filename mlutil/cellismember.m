function [m,ind] = cellismember(a,b,ordered,fillerval)
% ordered if the cell contents are considered to be ordered (different
% order means different cells)
% fillerval should be set to a value that is not present in any cell of a
% or b (default -inf)

    if nargin < 3 || isempty(ordered)
        ordered = true;
    end

    if nargin < 4 || isempty(fillerval)
        fillerval = -inf;
    end

    if not(iscell(a))
        a = {a};
    end
    
    if not(iscell(b))
        b = {b};
    end
    
    maxcount = 0;
    for i=1:numel(a)
        if not(ordered)
            a{i} = sort(a{i},'ascend');
        end
        if numel(a{i}) > maxcount
            maxcount = numel(a{i});
        end
    end
    for i=1:numel(b)
        if not(ordered)
            b{i} = sort(b{i},'ascend');
        end
        if numel(b{i}) > maxcount
            maxcount = numel(b{i});
        end
    end
    
    fulla = ones(numel(a),maxcount).*fillerval;
    fullb = ones(numel(b),maxcount).*fillerval;

    [ci,ei] = cellind_mex(a);
    fulla(sub2ind(size(fulla),[ci{:}],[ei{:}])) = [a{:}];
    [ci,ei] = cellind_mex(b);
    fullb(sub2ind(size(fullb),[ci{:}],[ei{:}])) = [b{:}];
    
    [m,ind] = ismember(fulla,fullb,'rows');
end
