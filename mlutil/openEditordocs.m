% save with 'saveOpenEditordocs'
function openEditordocs(filename)
    if nargin < 1 || isempty(filename)
        [fname,pathname,~] = ...
            uigetfile({'*.txt','Text (*.txt)'},'Save Open M-Files');

        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,'/',fname];
        else
            return;
        end
    end
    
    docfilenames = cell(0,1);
    fid = fopen(filename,'r');
    tline = fgetl(fid);
    c = 1;
    while ischar(tline)
        docfilenames{c,1} = tline;
        tline = fgetl(fid);
        c = c + 1;
    end
    fclose(fid);
    
    edit(docfilenames{:});
end
