% compute the vectors in a n-rosy field from representative vectors
% input and output can be in polar or cartesian representation (polar is
% faster), one 2xn matix for cartesian, two 1xn matrices for polar
function [o1,o2] = rosyunrep(in1,in2,in3,in4)
    desiredangle = [];
    if size(in1,1) > 1
        if nargin == 2
            [angle,len] = cart2pol(in1(1,:),in1(2,:));
            n = in2;
        elseif nargin == 3
            [angle,len] = cart2pol(in1(1,:),in1(2,:));
            n = in2;
            if size(in3,1) > 1
                [desiredangle,~] = cart2pol(in3(1,:),in3(2,:));
            else
                desiredangle = in3;
            end
            desiredangle = mod(desiredangle,2*pi);
        else
            error('number of input arguments must be 2 or 3 if the first argument is a vector');    
        end
    elseif size(in1,1) == 1
        if nargin == 3
            angle = in1;
            len = in2;
            n = in3;
        elseif nargin == 4
            angle = in1;
            len = in2;
            n = in3;
            if size(in4,1) > 1
                [desiredangle,~] = cart2pol(in4(1,:),in4(2,:));
            else
                desiredangle = in4;
            end
            desiredangle = mod(desiredangle,2*pi);
        else
            error('number of input arguments must be 3 or 4 if the first argument is a scalar');        
        end
    end
    
    angle = mod(angle,2*pi);
%     mask = angle < pi;
%     angle(mask) = 2*pi+angle;
    
    if not(isempty(desiredangle ))
        bestangle = angle./n;
        bestangle = mod(bestangle + ((2*pi)/n) .* round((desiredangle-bestangle)./((2*pi)/n)), 2*pi);
    else
        bestangle = angle./n;
    end

%     if desiredangle > 0
%         bestangle = angle./n;
%         bestangle = mod(bestangle + ((2*pi)/n) .* round((desiredangle-bestangle)./((2*pi)/n)), 2*pi);
%     else
%         bestangle = angle./n;
%     end
    
    if nargout <= 1
        o1 = [len.*cos(bestangle);len.*sin(bestangle)];
%         [o1(1,:),o1(2,:)] = pol2cart(angle./n,len);
    else
        o1 = bestangle;
        o2 = len;
    end
end
