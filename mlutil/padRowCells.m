% cell contents are always padded vertically (i.e. in first dimension)
function [a,nelm] = padRowCells(a,padval)
    nelm = max(cellfun(@(x) numel(x),a),[],2);
    
    for i=1:size(a,1)
        for j=1:size(a,2)
            a{i,j}(end+1:nelm(i),:) = padval;
        end
    end
end
