% n natural numbers with approx. equal spacing in log space or linear space
% Actual number of values returned might be >= n.
% Just a hack, not efficient at all, should not be used in critical code parts
function y = logspacei(x1,x2,n,shiftinv)

    if nargin < 5 || isempty(shiftinv)
        shiftinv = false;
    end

    % shift the interval [x1,x2] so it starts at 1
    if shiftinv
        shift = x1-1;
        x2 = x2 - shift;
        x1 = 1;
    end
    
    if n > (x2-x1)+1
        error('number of values must be <= (x2 - x1) + 1');
    end
    
    logx1 = log(x1);
    logx2 = log(x2);
    
    for subres=n:n*100

        l = exp(linspace(logx1,logx2,subres));
    
        cres = nnz([1,diff(round(l))]);
    
        if (cres >= n)
            y = round(l([1,diff(round(l))] > 0));
            break;
        end

    end
    
    % shift the interval back to its original position
    if shiftinv
        y = y + shift;
    end
end
