% n ascending random numbers with minimum distance d between min and max
function r = mindistrand(n,d,rmin,rmax)
    slack = (rmax-rmin) - d*(n-1);

    if slack < 0
        error('Cannot fit that many values with minimum distance in the given interval.');
    end
    
    minspacingsequence = (0:n-1).*d;
    
    randomsequence = sort(rand(1,n),'ascend').*slack;
    
    r = randomsequence + minspacingsequence + rmin;
end
