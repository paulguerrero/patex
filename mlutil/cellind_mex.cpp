#include "mex.h"

// #include<vector>
// #include<algorithm>

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    if (nrhs != 1) {
        mexErrMsgTxt("One input required.");
    }
    
    if(nlhs != 1 && nlhs != 2) {
        mexErrMsgTxt("One or two outputs required.");
    }
    
//     mexPrintf("1\n");
//     mexEvalString("drawnow;");

    // get pointers to values and indices
    if (!mxIsClass(prhs[0],"cell")) {
        mexErrMsgTxt("Must pass a cell array.");
    }
    
    plhs[0] = mxCreateCellArray(mxGetNumberOfDimensions(prhs[0]),mxGetDimensions(prhs[0]));
    if (nlhs >= 2) {
        plhs[1] = mxCreateCellArray(mxGetNumberOfDimensions(prhs[0]),mxGetDimensions(prhs[0]));
    }
    mwSize ncells = mxGetNumberOfElements(plhs[0]);
    
    for (mwIndex i=0; i<ncells; i++) {
        
        mxArray* inputcell = mxGetCell(prhs[0],i);
        
        if (inputcell != NULL) {

            mxArray* outputcell = mxCreateNumericArray(
                    mxGetNumberOfDimensions(inputcell),mxGetDimensions(inputcell),
                    mxDOUBLE_CLASS,mxREAL);

            mwSize nelm = mxGetNumberOfElements(outputcell);
            double* elm_p = mxGetPr(outputcell);

            for (mwIndex j=0; j<nelm; j++) {
                elm_p[j] = (double) (i+1); // to matlab indexing
            }
            
            mxSetCell(plhs[0],i,outputcell);
            
            if (nlhs >= 2) {
                outputcell = mxCreateNumericArray(
                        mxGetNumberOfDimensions(inputcell),mxGetDimensions(inputcell),
                        mxDOUBLE_CLASS,mxREAL);

                nelm = mxGetNumberOfElements(outputcell);
                elm_p = mxGetPr(outputcell);
                
                for (mwIndex j=0; j<nelm; j++) {
                    elm_p[j] = (double) (j+1); // to matlab indexing
                }
                mxSetCell(plhs[1],i,outputcell);
            }
            
        }
    }
}
