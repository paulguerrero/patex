% vscalespace = gaussScalespace2D(p,v,stddevs,cutoff)
%
% gaussian scale space for a 2D function given as scattered data
% 
% - p are the 2D locations of the data points
% - v is d x n array, where d is dimensionality of the data and n the
% number of data points
% - scales are the scales to be computed, given as std deviations of the
% gaussian kernel
% - cutoff is the cutoff radius for the gaussian kernel, given in multiples
% of the std. deviation
%
% - vscalespace is the d x n x numel(stddevs) gaussian scale space of the
% values v at p
function vscalespace = gaussScalespace2D(p,v,scales,cutoff)
    
    if nargin < 4 || isempty(cutoff)
        cutoff = 3;
    end
    
    posdists = sqrt(...
        bsxfun(@minus,p(1,:)',p(1,:)).^2 + ...
        bsxfun(@minus,p(2,:)',p(2,:)).^2);
    vscalespace = zeros(size(v,1),size(v,2),numel(scales));
    posweights = zeros(size(posdists));
    for j=1:size(vscalespace,3)
        
        mask = posdists <= scales(j)*cutoff;
        
        if any(mask)
            posweights(:) = 0;
            posweights(mask) = exp(-posdists(mask).^2 ./ (2*scales(j)^2));

            vscalespace(:,:,j) = v * posweights';
            vscalespace(:,:,j) = bsxfun(@rdivide,vscalespace(:,:,j),sum(posweights,2)');
        end

%         for k=1:size(v,2)
%             mask = posweights(k,:) > 0;
%             vscalespace(:,k,j) = ...
%                 sum(bsxfun(@times,v(:,mask),posweights(k,mask)),2) ./ ...
%                 sum(posweights(k,mask)); % normalize by sum of weights
%         end

    end
end
