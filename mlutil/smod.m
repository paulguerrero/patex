% shifted modulo - [min,max) instead of [0,max)
function x = smod(x,min,max)
    % generalized x = mod(x-min,max-min)+min :
	x = bsxfun(@plus,bsxfun(@mod,bsxfun(@minus,x,min),max-min),min);
end
