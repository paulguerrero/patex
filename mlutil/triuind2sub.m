% from indices into the upper triangular part of an m x m square matrix to
% subscripts of the same matrix
% offset specifies an offset above the main diagonal (0 includes the main
% diagonal)
function [i,j] = triuind2sub(m,ind,offset)
    
    if not(isscalar(m))
        error('Invalid matrix size, must be scalar.');
    end

    if nargin < 3
        offset = 0;
    end

    % equivalent to indices of upper triangular part of lower-dimensinal matrix
    % with offset added to column indices
    m = m - offset;

    if m < 1
        error('Invalid matrix size, must be at least 1.');
    end
    
    if nargin < 2
        ind = 1:(m^2-m)/2 + m;
    elseif any(ind < 1 | ind > (m^2-m)/2 + m)
        error('Index out of bounds.');
    end

    ii = (m*(m+1))/2 - ind;
    k = floor( (sqrt(8.*ii + 1) - 1) ./ 2);
    i = m-k;
    j = (ind - m.*(m+1)./2) + ((k+1).*(k+2))./2 + (i-1) + offset;
end
