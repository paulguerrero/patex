// s = strjoin_mex(c)
// joins the strings in cell array c (more efficiently than matlab's strjoin)
// c is a cell array of strings
// s is a single string

#include "mex.h"

#include<vector>
#include<string>

#include "mexutils.h"

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{   
    // check number of inputs
    if (nrhs != 1) {
        mexErrMsgTxt("One input required.");
    }
    if (nlhs != 1) {
        mexErrMsgTxt("One output required.");
    }
    
    mwIndex inputind = 0;

    // get input string cell array
    std::vector<std::string> strings;
    readStringCellArray(prhs[inputind],strings);
    ++inputind;
    
    // get sum of size of all strings in the cell array
    mwSize totalsize = 0;
    for (std::vector<std::string>::const_iterator si=strings.begin(); si!=strings.end(); si++) {
        totalsize += si->size();
    }
    
    // concatenate strings
    std::string merged;
    merged.reserve(totalsize);
    for (std::vector<std::string>::const_iterator si=strings.begin(); si!=strings.end(); si++) {
        merged += *si;
    }
    
    // write output
    writeString(merged,plhs[0]);
}
