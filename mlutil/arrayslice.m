function slice = arrayslice(a,d,i) %#ok<INUSD>
    % create string
    ndim = ndims(a);
    selstr = [repmat(':,',1,d-1),'i,',repmat(':,',1,ndim-d)];
    selstr = selstr(1:end-1); % remove trailing comma

    % evaluate selection instruction
    slice = eval(['a(',selstr,')']);
end
