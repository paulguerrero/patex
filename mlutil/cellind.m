function ind = cellind(c)
    if not(iscell(c))
        error('Must pass a cell array.');
    end

    ind = cellfun(@(x,y) ones(size(x)).*y, c, num2cell(1:numel(c)),'UniformOutput',false);
end
