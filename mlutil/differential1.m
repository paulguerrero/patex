function dx = differential1(x,circular)
    if not(iscell(x))
        wascell = false;
        x = {x};
    else
        wascell = true;
    end
    
    dx = cell(size(x));
    for i=1:numel(x)
        if numel(x{i}) < 2
            error('x needs to have at least two elements');
        end
        
        if not(isvector(x{i}))
            error('x needs to be a vector.');
        end
        
        if not(isrow(x{i}))
            x{i} = x{i}';
        end
        
        if circular(i)
            dx{i} = diff(x{i});
            dx{i} = (dx{i} + dx{i}([end,1:end-1])) .* 0.5;
        else
            dx{i} = diff(x{i});
            dx{i} = [dx{i}(1),dx{i}(1:end-1)+dx{i}(2:end),dx{i}(end)] .* 0.5;    
        end
    end
    
    if not(wascell)
        dx = dx{1};
    end
end
