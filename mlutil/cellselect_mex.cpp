#include "mex.h"

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    if (nrhs != 2) {
        mexErrMsgTxt("Two inputs required.");
    }
    
    if (nlhs != 1) {
        mexErrMsgTxt("One output required.");
    }
    
    
    if (!mxIsClass(prhs[0],"cell") || !mxIsClass(prhs[1],"cell")) {
        mexErrMsgTxt("Must pass cell arrays.");
    }
	
	mwSize ncells = mxGetNumberOfElements(prhs[1]);
	if (mxGetNumberOfElements(prhs[0]) != ncells) {
		mexErrMsgTxt("The two cell arrays need to have the same number of cells.");
	}
    
    // create output cell array
	plhs[0] = mxCreateCellArray(mxGetNumberOfDimensions(prhs[1]),mxGetDimensions(prhs[1]));
    
    for (mwIndex i=0; i<ncells; i++) {
        
        mxArray* valuecell = mxGetCell(prhs[0],i);
		mxArray* indexcell = mxGetCell(prhs[1],i);
		
        
        if (indexcell != NULL) {
		
			mwSize nvalue = mxGetNumberOfElements(valuecell);
			mwSize nindex = mxGetNumberOfElements(indexcell);
			
			if (nindex > 0 && valuecell == NULL) {
				mexErrMsgTxt("Given index out of bounds.");
			}

            mxArray* outputcell = mxCreateNumericArray(
                    mxGetNumberOfDimensions(indexcell),mxGetDimensions(indexcell),
                    mxDOUBLE_CLASS,mxREAL);

            double* o_p = mxGetPr(outputcell);
			double* i_p = mxGetPr(indexcell);
			double* v_p = mxGetPr(valuecell);

            for (mwIndex j=0; j<nindex; j++) {
                mwIndex ind = ((mwIndex) i_p[j]);
				if (ind > nvalue || ind <= 0) {
					mexErrMsgTxt("Given index out of bounds.");
				}
				o_p[j] = v_p[ind-1]; // from matlab indexing
            }
            
            mxSetCell(plhs[0],i,outputcell);
        }
    }
}
