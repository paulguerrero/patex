function parammask = findNameValuePairs(args,paramnames)
    
    parammask = false(1,numel(args));
    for i=numel(args)-1:-2:1
        if ischar(args{i}) && any(strcmp(args{i},paramnames))
            parammask([i,i+1]) = true;
        end
    end

end
