% return non-repeating random integers in the range [1..kmax], optionally
% giving a probability w for picking each integer.
% similar to Fisher-Yates shuffling, but with weights and higher complexity
% O(n^2) (due to the cost of updating the cumulative distribution)
function ur = uniquerand(k,nmax,w)

    if nmax == 1
        ur = 1;
        return;
    end
    
    shufflelist = 1:nmax;
    if nargin < 3 || isempty(w)
        % unweighted version
        for i=nmax:-1:2;
            % pick a random index in the remaining shuffle list 
            randind = randi(i);
            
            % swap random index to the end of the list
            temp = shufflelist(i);
            shufflelist(i) = shufflelist(randind);
            shufflelist(randind) = temp;
        end
    else
        % weighted version
        
        % get cumulative probability distribution from w
        cumd = [0,cumsum(w)];
        if cumd(end) == 0.0
            % all weights are zero, use uniform distribution instead
            cumd = linspace(0,1,nmax+1);
        else
            % normalize
            cumd = cumd ./ cumd(end);
        end
        
        for i=nmax:-1:2;
            if cumd(i+1) < eps('double') * 10
                % remaining cumulative distribution values are either 0 or 
                % numeric noise, use uniform distribution instead
                cumd(1:i+1) = linspace(0,1,i+1);
            end
            
            % pick a random index in the remaining shuffle list according
            % to probabilities w
            randval = rand*cumd(i+1);
            randind = find(cumd(1:i)<=randval,1,'last');
            
            % update cumulative distribution
            wdiff = ((cumd(i+1)-cumd(i))) - (cumd(randind+1)-cumd(randind));
            cumd(randind+1:i) = cumd(randind+1:i) + wdiff; 
            
            % swap random index to the end of the list
            temp = shufflelist(i);
            shufflelist(i) = shufflelist(randind);
            shufflelist(randind) = temp;
        end
    end
    
    ur = shufflelist(end-k+1:end);
end
