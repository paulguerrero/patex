% robust in the sense that it is guaranteed to lie within the bounds of the
% inputs (specifically, it is exactly the same as the inputs if all inputs
% are identical)
% could be parallelized in each step for log(n) complexity (assuming enough
% parallel execution units)
function x = mean_robust(x)

    x = x(:)';
    
    count = numel(x);
    termcount = ones(size(x));
    while count > 1
        halfcount = count/2;
        evencount = floor(halfcount)*2;
        newcount = ceil(halfcount);
        
        factors = termcount(2:2:evencount)./(termcount(1:2:evencount)+termcount(2:2:evencount));
        x(1:newcount) = [x(evencount+1:count),x(1:2:evencount) +  (x(2:2:evencount) - x(1:2:evencount)) .* factors];
        termcount(1:newcount) = [termcount(evencount+1:count),termcount(1:2:evencount)+termcount(2:2:evencount)];
        
        count = newcount;
    end
    
    x = x(1);
end
