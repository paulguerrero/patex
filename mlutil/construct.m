function obj = construct(c,varargin)
    h = constructorhandle(c);
    obj = h(varargin{:});
end
