% construct cell array by putting the values of A into the cells with
% indices cellinds. cellcount gives the total number of cells. cellshape
% is the non-singleton dimension for each cell
function [C,nonemptyinds] = array2cell(A,cellinds,cellcount,cellshape)

    if nargin < 3 || isempty(cellcount)
        if isempty(cellinds)
            cellcount = 0;
        else
            cellcount = max(cellinds(:));
        end
    end
    
    if nargin < 4 || isempty(cellshape)
        cellshape = 1;
    end
    
    C = cell(1,cellcount);
    nonemptyinds = zeros(1,0);
    
    if not(isempty(C))
        if not(isempty(A))
            [cellinds,perm] = sort(cellinds(:),'ascend');

            if not(isempty(cellinds)) && cellinds(end) > cellcount
                error('Index out of bounds for given cell count.')
            end

            D = A(perm);
            if isrow(D)
                D = D';
            end

            diffinds = find(cellinds(2:end)~=cellinds(1:end-1));
            startinds = [1;diffinds+1];
            endinds = [diffinds;numel(D)];

            D2 = cell(numel(startinds),1);
            shp = ones(1,max(cellshape,2));
            for i=1:numel(D2)
                if cellshape == 1
                    D2{i} = D(startinds(i):endinds(i));
                else
                    shp(cellshape) = (endinds(i)-startinds(i))+1;
                    D2{i} = reshape(D(startinds(i):endinds(i)),shp);
                end
            end
            D = D2;

            nonemptyinds = cellinds(startinds);
            C(nonemptyinds) = D;
        else
            nonemptyinds = zeros(1,0);
        end

        % give empty cells the right shape
        emptyshp = ones(1,max(cellshape,2));
        emptyshp(cellshape) = 0;
        emptymask = true(1,numel(C));
        emptymask(nonemptyinds) = false;
        C(emptymask) = {zeros(emptyshp)};
    end
end
