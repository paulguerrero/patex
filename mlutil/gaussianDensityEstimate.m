% estimate density at points p from the samples x with weights xw
% using a gaussian kernel with standard deviation sdev
function pd = gaussianDensityEstimate(p,x,sdev,xw)
    if nargin < 4 || isempty(xw)
        xw = ones(1,size(x,2));
    end
    
    dists = pdist2(p',x','euclidean');
    dists = (1/(sdev*sqrt(2*pi))) .* exp(-dists.^2 ./ (2*sdev^2));
    pd = sum(bsxfun(@times,dists,xw),2)' ./ sum(xw);
end
