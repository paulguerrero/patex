function [ism,ind] = ismemberStr(a,b,option)

    if nargin < 3
        ism = false(size(a));
        ind = zeros(size(a));
        for i=1:numel(a)
            firstind = find(strcmp(a(i),b),1,'first');
            if not(isempty(firstind))
                ism(i) = true;
                ind(i) = firstind;
            end
        end
    elseif strcmp(option,'rows')
        ism = false(size(a,1),1);
        ind = zeros(size(a,1),1);
        for i=1:size(a,1)
            firstind = [];
            for j=1:size(b,1)
                if all(strcmp(a(i,:),b(j,:)))
                    firstind = j;
                    break;
                end
            end
%             firstind = find(all(bsxfun(@strcmp,a(i,:),b),2),1,'first');
            if not(isempty(firstind))
                ism(i) = true;
                ind(i) = firstind;
            end
        end
    else
        error('Unknown option.');
    end
end
