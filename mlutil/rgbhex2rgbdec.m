function rgbclr = rgbhex2rgbdec(hexclr,hasnumbersign)
    if nargin < 2 || isempty(hasnumbersign)
        hasnumbersign = false;
    end

    if hasnumbersign && not(hexclr(1) == '#')
        error('Bad format for hex color.');
    end
    
    if hasnumbersign
        rgbclr = [...
            hex2dec(hexclr(:,2:3))./255,...
            hex2dec(hexclr(:,4:5))./255,...
            hex2dec(hexclr(:,6:7))./255];
    else
        rgbclr = [...
            hex2dec(hexclr(:,1:2))./255,...
            hex2dec(hexclr(:,3:4))./255,...
            hex2dec(hexclr(:,5:6))./255];
    end
end
