% Normally distributed pseudo-random numbers using the Halton sequence.
% Note that only the numbers returned in one call have low discrepancy, the
% numbers returned from two different calls do generally not have low
% discrepancy.
% The covariance matrix must be diagonal and the diagonal must be given as
% vector in sigma.
function ldrn = ldrandn(n,mu,sigma)
    
    % get uniformly distributed low-discrepancy samples
    ldrn = ldrand(size(mu,1),n);

    % convert uniform distribution to standard normal distribution
    % (Box-Muller)
    R = sqrt(-2*log(ldrn(1,:)));
    Theta = 2*pi*ldrn(2,:);
    ldrn = [R.*sin(Theta);...
            R.*cos(Theta)];

    if nargin > 1
        % apply mean and variance
        ldrn = bsxfun(@plus,bsxfun(@times,ldrn,sqrt(sigma)),mu);
    end
end
