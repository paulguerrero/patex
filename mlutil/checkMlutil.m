function ok = initMlutil(options)
    
    ok = true;
    
    % check that all necessary files have been compiled for the current architecture
    ext = mexext;
    files = {...
        ['array2cell_mex','.',mexext],...
        ['cellind_mex','.',mexext],...
        ['cellisempty_mex','.',mexext],...
        ['cellselect_mex','.',mexext],...
        ['celldiff_mex','.',mexext],...
        ['strjoin_mex','.',mexext]
    };
    
    for i=1:numel(files)
        if not(exist(files{i},'file'))
            ok = false;
            break;
        end
    end
    
end
