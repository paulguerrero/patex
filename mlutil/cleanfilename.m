function filename = cleanfilename(varargin)

    filename = fullfile(varargin{:});
    
    % replace file separator by '/'
    fsep = filesep;
    if not(fsep == '/')
        filename = strrep(filename,fsep,'/');
    end
    
    % remove leading and trailing separators
    if iscell(filename)
        for i=1:numel(filename)
            if isempty(filename{i})
                continue;
            end
            if filename{i}(1) == '/'
                filename{i}(1) = [];
            end
            if filename{i}(end) == '/'
                filename{i}(end) = [];
            end
        end
    else
        if isempty(filename)
            return;
        end
        if filename(1) == '/'
            filename(1) = [];
        end
        if filename(end) == '/'
            filename(end) = [];
        end
    end
end
