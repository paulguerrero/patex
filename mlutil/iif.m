% inline conditional
function out = iif(varargin)
    ind = find([varargin{1:2:end-1}], 1, 'first');
    if isempty(ind)
         if mod(numel(varargin),2) == 1
            % have else
            out = varargin{end}();
         else
            % no else, return empty array
            out = [];
         end
    else
        out = varargin{2 * ind}();
    end
end
