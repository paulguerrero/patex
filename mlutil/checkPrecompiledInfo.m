function [ok,requiredruntimes] = checkPrecompiledInfo(infofilename)
    
    ok = true;
    
    [functions,filenames,runtimes] = readPrecompiledInfo(infofilename);
    
    ext = mexext;
    
    funcfnames = functions;
    for i=1:numel(funcfnames)
        funcfnames{i} = [funcfnames{i},'.',ext];
    end
    
    [funccompiled,ind] = ismember(funcfnames,filenames);
    requiredruntimes = unique(runtimes(ind(funccompiled)));
    
    if not(all(funccompiled))
        ok = false;
    end
    
    % check that all compiled files actually exist
    compiledfnames = filenames(ind(funccompiled));
    [basepath,~,~] = fileparts(infofilename);
    for i=1:numel(compiledfnames)
        if not(exist(fullfile(basepath,compiledfnames{i}),'file'))
            warning('precompiled info file is out of date, a compiled function listed in the file was not found.');
            ok = false;
        end
    end
end
