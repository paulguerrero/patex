function y = missingint(x,minint)
% returns the smallest integer in the interval [minint,inf) that is
% missing in x
%
% x: set of integers
% minint: any finite integer
% y: smallest integer in [mintint,inf) that is not contained in x
    
    if nargin < 2 || isempt(minint)
        minint = 1;
    end
    
    if isempty(x)
        y = minint;
    else
        x = [minint-1,x(x>=minint)];
        x = unique(x);
        y = x(find(diff(x)>1,1,'first'))+1;
        if isempty(y)
            y = x(end)+1;
        end
    end
end
