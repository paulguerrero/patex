% make the monotonically increasing function given by y (x is assumed to be
% something like 1:numel(y))strictly increasing by deleting repeated entries
% and giving sections with zero slope a very small slope instead
function [y2,keepMask] = funique(y,thresh)

    if nargin < 2
        thresh = 0;
    end
    
    % check if y is monotonically increasing
    if not(all(diff(y) >= 0))
        error('the given function values are not monotonically increasing');
    end
    
    % find first and last repeated value
    r1 = true(size(y));
    r2 = true(size(y));

%     [~,idxfirst] = unique(y,'first');
%     [~,idxlast] = unique(y,'last');
    
    [~,idxfirst] = unique([0,cumsum(diff(y) > thresh)],'first');
    [~,idxlast] = unique([0,cumsum(diff(y) > thresh)],'last');

    r1(idxfirst) = false;
    r2(idxlast) = false;

    idx = 1:numel(y);
    lastvalidx = idx(and(r1,not(r2)));

    % add an eps to the last repeated y-value, so the slope is != 0
    % everywhere
    yt = y;
    keepMask = not(r1 & r2);
    for i=1:numel(lastvalidx)
%         offset = eps(y(lastvalidx(i))) * 10000;
        offset = max(eps(y(lastvalidx(i))) * 10000,thresh);

        if lastvalidx(i) ~= numel(y) && y(lastvalidx(i)+1) - y(lastvalidx(i)) < offset
            % dont keep the last repeated value if the following value has a very
            % small offset anyway
            keepMask(lastvalidx(i)) = false;
        else
            % add an eps to the last repeated value
            yt(lastvalidx(i)) = yt(lastvalidx(i)) + offset;
        end
    end
    
    % remove all repeated samples excluding the first and last repeated sample
    % in a zero-slope interval of the function
    y2 = yt(keepMask);
end