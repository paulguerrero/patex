function vars = getWorkspaceVarnames(workspace,varclass)
    
    vars = evalin(workspace,'who');
    mask = false(1,numel(vars));
    for i=1:numel(vars)
        mask(i) = evalin(workspace,['isa(',vars{i},',''',varclass,''') && isvalid(',vars{i},')']);
    end
    vars = vars(mask);
end
