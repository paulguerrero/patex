% Find the maximum cliques with minimum edge cost in a graph.
% Only positive cost is allowed, use inf in the cost matrix for a
% non-existent edge.
% This method uses a greedy growing method to find a good solution.
function cliques = minedgecostClique(cost,ncliques)

    if any(any(cost < 0))
        error('Negative cost is not allowed.');
    end

    nelm = size(cost,1);
    
    cliques = cell(1,ncliques);
    
    % set lower triangular part to 0 (including diagonal)
    cost(tril(true(size(cost)))) = 0;
    
    remainingmask = true(1,nelm);
    remaininginds = 1:nelm;
    nremaining = nelm;
    elm2remaining = 1:nelm;
    for i=1:ncliques
        
        if nremaining == 0
            break;
        end
        
        % find the lowest value in the remaining part of the matrix
        % temporarily set lower triangular part to nan
        cost(tril(true(size(cost)))) = nan;
        submat = cost(remainingmask,remainingmask);
        [mindist,minind] = min(submat(:));
        cost(tril(true(size(cost)))) = 0;
        [mini,minj] = ind2sub([nremaining,nremaining],minind);
        mini = remaininginds(mini);
        minj = remaininginds(minj);
        
        if isinf(mindist)
            break;
        end
        
        pickedmask = false(1,nelm);
        pickedmask([mini,minj]) = true;
        pickedinds = zeros(1,nelm);
        pickedinds(1:2) = [mini,minj];
        npicked = 2;
        
        remainingmask([mini,minj]) = false;

%         remaininginds([mini,minj]) = remaininginds([nremaining-1,nremaining]);
%         nremaining = nremaining-2;
        
        minjrind = elm2remaining(minj);
        remaininginds(minjrind:nremaining-1) = remaininginds(minjrind+1:nremaining);
        elm2remaining(minj+1:end) = elm2remaining(minj+1:end) - 1;
        elm2remaining(minj) = 0;
        nremaining = nremaining-1;
        
        minirind = elm2remaining(mini);
        remaininginds(minirind:nremaining-1) = remaininginds(minirind+1:nremaining);
        elm2remaining(mini+1:end) = elm2remaining(mini+1:end) - 1;
        elm2remaining(mini) = 0;
        nremaining = nremaining-1;
        
        
        for j=1:nelm
            % find the adjacent element with lowest distance
            
            if nremaining == 0
                break;
            end
            
            s = sum(cost(pickedmask,remainingmask),1);
            s = s+sum(cost(remainingmask,pickedmask),2)';
%             s = s ./ npicked;
            [minavgdist,newelmind] = min(s);
            
            if isinf(minavgdist)
                break;
            end
            
            newelmind = remaininginds(newelmind);
            
%             submat = dists(pickedmask,remainingmask);
%             [mindist1,minind1] = min(submat(:));
%             submat = dists(remainingmask,pickedmask);
%             [mindist2,minind2] = min(submat(:));
%             
%             if isinf(mindist1) && isinf(mindist2)
%                 break; % no more elements
%             elseif mindist1 < mindist2
%                 [mini,minj] = ind2sub([npicked,nremaining],minind1);
%                 newelmind = remaininginds(minj);
%             else
%                 [mini,minj] = ind2sub([nremaining,npicked],minind2);
%                 newelmind = remaininginds(mini);
%             end
            
            pickedmask(newelmind) = true;
            
            pickedinds(npicked+1) = newelmind;
            npicked = npicked+1;
            
            remainingmask(newelmind) = false;
            
%             remaininginds(newelmind) = remaininginds(nremaining);
%             nremaining = nremaining-1;
            newelmindrind = elm2remaining(newelmind);
            remaininginds(newelmindrind:nremaining-1) = remaininginds(newelmindrind+1:nremaining);
            elm2remaining(newelmind+1:end) = elm2remaining(newelmind+1:end) - 1;
            elm2remaining(newelmind) = 0;
            nremaining = nremaining-1;
        end

        cliques{i} = pickedinds(1:npicked);
    end
end
