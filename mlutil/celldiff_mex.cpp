#include "mex.h"

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    if (nrhs != 2) {
        mexErrMsgTxt("Two inputs required.");
    }
    
    if (nlhs != 1) {
        mexErrMsgTxt("One output required.");
    }
    
    if (!mxIsClass(prhs[0],"cell") || !mxIsClass(prhs[1],"cell")) {
        mexErrMsgTxt("Must pass cell arrays.");
    }
	
	mwSize ncells = mxGetNumberOfElements(prhs[0]);
	if (mxGetNumberOfElements(prhs[1]) != ncells) {
		mexErrMsgTxt("The two cell arrays need to have the same number of cells.");
	}

    // create output cell array
	plhs[0] = mxCreateCellArray(mxGetNumberOfDimensions(prhs[0]),mxGetDimensions(prhs[0]));
    
    for (mwIndex i=0; i<ncells; i++) {
        
        mxArray* cell1 = mxGetCell(prhs[0],i);
		mxArray* cell2 = mxGetCell(prhs[1],i);
		
		mwSize ncell1;
		if (cell1 != NULL) {
			ncell1 = mxGetNumberOfElements(cell1);
		} else {
			ncell1 = 0;
		}
        
		mwSize ncell2;
		if (cell2 != NULL) {
			ncell2 = mxGetNumberOfElements(cell2);
		} else {
			ncell2 = 0;
		}
		
		if (ncell1 != ncell2) {
			mexErrMsgTxt("Corresponding cells must have the same number of elements.");
		}
		
		mxArray* outputcell = mxCreateNumericArray(
				mxGetNumberOfDimensions(cell1),mxGetDimensions(cell1),
				mxDOUBLE_CLASS,mxREAL);
		
		double* o_p = mxGetPr(outputcell);
		double* c1_p = mxGetPr(cell1);
		double* c2_p = mxGetPr(cell2);
		
		for (mwIndex j=0; j<ncell1; j++) {
			o_p[j] = c1_p[j] - c2_p[j];
		}
		
		mxSetCell(plhs[0],i,outputcell);
    }
}
