% map from old to new indices after deleting elements delinds from a vector
% of size n
function [map,keepmask,inds] = keepindmap(keepinds,n,inds)
    
    % can't directly use keepinds because it might contain repeated
    % entries
    
    keepmask = false(1,n);
    keepmask(keepinds(not(isnan(keepinds)))) = true;
    map = nan(1,n);
    map(keepmask) = 1:sum(keepmask);
    
    if nargin >= 3
        mask = not(isnan(inds));
        inds(mask) = map(inds(mask));
    end
end
