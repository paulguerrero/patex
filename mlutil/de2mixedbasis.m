function mb = de2mixedbasis(d,p)

    b = [1,cumprod(p)];
    
    if b(end) > 2^50
        % mantissa of double has 52 bit
        warning('de2mixedbasis:toolarge','Number is too large, output might have numeric errors.');
    end
    
    mb = mod(floor(d ./  b(1:end-1)), p);
end
