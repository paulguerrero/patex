% n natural numbers with approx. equal spacing in log space or linear space
% lin blends between equal spacing in linear (lin=1) and log space (lin=0)
% shiftinv makes the sequence shift invariant by shifting the interval to
% start at 1
% Actual number of values returned might be >= n.
% Just a hack, not efficient at all, should not be used in critical code parts
function y = linlogspacei(x1,x2,n,lin,shiftinv)

    if nargin < 5 || isempty(shiftinv)
        shiftinv = false;
    end

    % shift the interval [x1,x2] so it starts at 1
    if shiftinv
        shift = x1-1;
        x2 = x2 - shift;
        x1 = 1;
    end
    
    if n > (x2-x1)+1
        error('number of values must be <= (x2 - x1) + 1');
    end
    
    lin = min(1,max(0,lin));
    
    logx1 = log(x1);
    logx2 = log(x2);
    logblendf = log(1-lin);

    y = zeros(1,0);
    for subres=n:n*100
    
        l = exp(linspace(logx1+logblendf,logx2+logblendf,subres)) + linspace(x1*lin,x2*lin,subres);
    
        cres = nnz([1,diff(round(l))]);
    
        if (cres >= n)
            y = round(l([1,diff(round(l))] > 0));
            break;
        end

    end
    
    % shift the interval back to its original position
    if shiftinv
        y = y + shift;
    end
end
