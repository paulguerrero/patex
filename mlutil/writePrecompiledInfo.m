function writePrecompiledInfo(infofilename,functions,filenames,runtimes)

    if numel(filenames) ~= numel(runtimes)
        error('Must give the same number of filenames and runtimes.');
    end
    
    if isrow(functions)
        functions = functions';
    end
    if isrow(filenames)
        filenames = filenames';
    end
    if isrow(runtimes)
        runtimes = runtimes';
    end
    
    str = sprintf('[functions]\n');
    
    nl = repmat({sprintf('\n')},numel(functions),1);
    functions_str = [functions,nl]';
    str = [str,functions_str{:}];

    str = [str,sprintf('[runtimes]\n')];
    
    sep = repmat({':'},numel(filenames),1);
    nl = repmat({sprintf('\n')},numel(filenames),1);
    runtimes_str = [filenames,sep,runtimes,nl]';
    str = [str,runtimes_str{:}];

    fid = fopen(['./',infofilename],'w');

    if isempty(fid) || fid < 0
        error(['Cannot write to file ''',infofilename,''' to store precompiled architecture and runtime.'])
    end

    fwrite(fid,str);
    
    fclose(fid);
end
