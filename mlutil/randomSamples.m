function [pixinds,pixsubs] = randomSamples(I, n, supprad, seed)
    
    if nargin < 3
        supprad = 0;
    end
    
    if nargin >= 4
        rngsettings = rng(seed);
    else
        rngsettings = rng;
    end
    
    Idim = ndims(I);
    if Idim ~= 2
        error('Input image must be 2D intensities.')
    end

    cd = cumsum(I(:));
    cd = [0;cd./cd(end)];
    
    Imask = true(size(I));
    Imaskinds = find(Imask);
    
    randvals = rand(1,n);
    pixinds = zeros(1,n);
%     pixsubs = zeros(2,n);
%     ov = zeros(1,n);
    actualn = n;
    for i=1:n
        pixinds(i) = Imaskinds(find(cd >= randvals(i),1,'first'));
        pixinds(i) = max(1,pixinds(i)-1);
        [subi,subj] = ind2sub(size(I),pixinds(i));
%         ov(i) = I(pixinds(i));
%         pixsubs(:,i) = [subj,subi];
        
        Imask(max(1,subi-supprad):min(size(I,1),subi+supprad),...
              max(1,subj-supprad):min(size(I,2),subj+supprad)) = false;
        Imaskinds = find(Imask);
          
        cd = cumsum(I(Imask));
        if isempty(cd) || cd(end) == 0
            actualn = i;
            break;
        end
        cd = [0;cd./cd(end)];
        
%         I(max(1,opi-supprad):min(size(I,1),opi+supprad),...
%           max(1,opj-supprad):min(size(I,2),opj+supprad)) = 0;
%         
%         cd = cumsum(I(:));
%         if cd(end) == 0
%             actualn = i;
%             break;
%         end
%         cd = [0;cd./cd(end)];
    end
    
    pixinds(actualn+1:end) = [];
%     pixsubs(:,actualn+1:end) = [];
%     ov(actualn+1:end) = [];
    
    pixinds = unique(pixinds);    
%     [pixinds,inds] = unique(pixinds);
%     pixsubs = pixsubs(:,inds);
    
    if nargout > 1
        [pixsubs(1,:),pixsubs(2,:)] = ind2sub(size(I),pixinds);
    end
    
%     ov = ov(inds);
    
%     I = zeros(size(I));
%     I(pixinds) = ov;
    
    % zero out the remainder of the mask that is not used as sample
%     Imask = false(size(I));
%     Imask(pixinds) = true;

    rng(rngsettings); % restore previous settings
end
