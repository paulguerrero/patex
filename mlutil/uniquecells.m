% fillerval must be a value not occurring in any cell
% also all cell contents and the fillerval must be concatenable into an
% array, i.e. [cells{:}] must work
function [ucells,ia,ic,iall] = uniquecells(cells,fillerval,orderedcontent)

    if nargin < 2 || isempty(fillerval)
        fillerval = nan;
    end
    
    if nargin < 3
        % If true, cells will only be considered unique if they have the
        % same content in the same order.
        % If false, cells will be considered unique if the have the same
        % content, regardless of order.
        orderedcontent = false;
    end
    
    if isempty(cells)
        ucells = cells;
        ia = zeros(0,1);
        ic = zeros(0,1);
        iall = cell(0,1);
        return;
    end
    
    ucells = cells(:);
    ucells = cellfun(@(x) x(:)',ucells,'UniformOutput',false);
    
    maxnelm = max(cellfun(@(x) numel(x),ucells));
    if orderedcontent
        ucells = cellfun(@(x) ...
            {[x,fillerval(1,ones(1,maxnelm-numel(x)))]},...
            ucells);
    else
        ucells = cellfun(@(x) ...
            {[sort(x,'ascend'),fillerval(1,ones(1,maxnelm-numel(x)))]},...
            ucells);
    end
    ucells = reshape(cat(1,ucells{:}),size(ucells,1),[]);

    [ucells,perm] = sortrows(ucells);
    
    diffmask = any(ucells(2:end,:)~=ucells(1:end-1,:),2);
    diffinds = find(diffmask)';
    startinds = [1,diffinds+1];
    
    ia = perm(startinds);
    
    ucells = cells(ia);
    
    % ic
    if nargout >= 3
        ic = cumsum([1;diffmask]);
        ic(perm) = ic;
    end
    
    % iall
    if nargout >= 4
        endinds = [diffinds,numel(cells)];
        iall = mat2cell(perm,endinds-startinds+1,1);
    end
end
