function h = constructorhandle(c)
    if ischar(c)
        h = str2func(c);
    else
        h = str2func(class(c));
    end
end
