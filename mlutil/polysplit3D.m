function [xcells,ycells,zcells] = polysplit3D(x,y,z)
%POLYSPLIT Convert line or polygon parts from vector form to cell arrays
%
%   [LATCELLS,LONCELLS] = POLYSPLIT(LAT,LON) returns the NaN-delimited
%   segments of the vectors LAT and LON as N-by-1 cell arrays with one
%   polygon segment per cell.  LAT and LON must be the same size and have
%   identically-placed NaNs.  The polygon segments are column vectors if
%   LAT and LON are column vectors, and row vectors otherwise.
%
%   See also isShapeMultipart, POLYJOIN, POLYBOOL, POLYCUT.

% Copyright 1996-2011 The MathWorks, Inc.

if isempty(x) && isempty(y)
    xcells = reshape({}, [0 1]);
    ycells = xcells;
else
    validateattributes(x,{'numeric'},{'real','vector'},mfilename,'LAT',1)
    validateattributes(y,{'numeric'},{'real','vector'},mfilename,'LON',2)
    
    [x, y, z] = removeExtraNanSeparators(x, y, z);
    
    % Find NaN locations.
    indx = find(isnan(x(:)));
    
    % Simulate the trailing NaN if it's missing.
    if ~isempty(x) && ~isnan(x(end))
        indx(end+1,1) = numel(x) + 1;
    end
    
    %  Extract each segment into pre-allocated N-by-1 cell arrays, where N is
    %  the number of polygon segments.  (Add a leading zero to the indx array
    %  to make indexing work for the first segment.)
    N = numel(indx);
    xcells = cell(N,1);
    ycells = cell(N,1);
    zcells = cell(N,1);
    indx = [0; indx];
    for k = 1:N
        iStart = indx(k)   + 1;
        iEnd   = indx(k+1) - 1;
        xcells{k} = x(iStart:iEnd);
        ycells{k} = y(iStart:iEnd);
        zcells{k} = z(iStart:iEnd);
    end
end
