function hexclr = rgbdec2rgbhex(rgbclr,addnumbersign)

    if nargin < 2 || isempty(addnumbersign)
        addnumbersign = false;
    end
    
    hexclr = [...
        dec2hex(round(max(0,min(1,rgbclr(:,1))).*255),2),...
        dec2hex(round(max(0,min(1,rgbclr(:,2))).*255),2),...
        dec2hex(round(max(0,min(1,rgbclr(:,3))).*255),2)];
    
    if addnumbersign
        hexclr = hexclr(:,[1,1:end]);
        hexclr(:,1) = '#';
    end
end
