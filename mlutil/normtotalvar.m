% approximate total variation of two 1D normal distributions with mean mu
% and atandard deviation sigma
function totalvar = normtotalvar(mean1,sdev1,mean2,sdev2)

    sampledist = min(sdev1,sdev2) * 0.1;
    samplemin = min(...
        mean1-sdev1*3,...
        mean2-sdev2*3);
    samplemax = max(...
        mean1+sdev1*3,...
        mean2+sdev2*3);
    samplecount = ceil((samplemax-samplemin)/sampledist)+1;
    sampledist = (samplemax-samplemin) / (samplecount+1);
    if samplecount > 100000
        error('Too many samples.'); % for safety
    end
    samples = linspace(samplemin,samplemax,samplecount);
    % integrate numerically over
    % [samplemin-sampledist/2,samplemax+sampledist/2]
    totalvar = 0.5 * sum(...
        normpdf(samples,mean1,sdev1) - ...
        normpdf(samples,mean2,sdev2)) * sampledist;
end
