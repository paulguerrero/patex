function nvp = struct2nvpairs(s,returninds)
    if nargin < 2 || isempty(returninds)
        returninds = false;
    end
    
    if numel(s) ~= 1
        error('Only scalar structs can be converted to name-value pairs (not array of structs).');
    end
    
    if returninds
        nvp = fieldnames(s);
        nvp = [nvp';1:numel(nvp)];
    else
        nvp = [fieldnames(s),struct2cell(s)]';
    end
    nvp = nvp(:);
end
