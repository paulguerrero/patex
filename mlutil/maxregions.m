function regions = maxregions(I,nmsrad,nmsthresh,regionthresh)
    [~,~,maxinds] = nonMaxSuppr(I,nmsrad,nmsthresh);
    % get connected components
    maxvals = I(maxinds);

    [maxvals,perm] = sort(maxvals,'descend');
    maxinds = maxinds(perm);

    flagged = false(size(I));

    regions = cell(1,numel(maxinds));
    for i=1:numel(maxinds)
        if flagged(maxinds(i))
            continue;
        end

        mask = I > maxvals(i)*regionthresh & not(flagged);
        [Ii,Ij] = ind2sub(size(I),maxinds(i));
        regions{i} = bwselect(mask,Ij,Ii); % select connected component at the maximum
        flagged = flagged | regions{i};
    end

end
