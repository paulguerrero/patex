function childelms = getDOMChildElements(node,tagnames,checkcount)
    
    if nargin < 2 || isempty(tagnames)
        tagnames = '';
    end

    childnodelist = node.getChildNodes();
    childelms = cell(1,0);
    for j=1:childnodelist.getLength()
        childnode =  childnodelist.item(j-1);
        if childnode.getNodeType() == childnode.ELEMENT_NODE && (isempty(tagnames) || any(strcmp(childnode.getNodeName(),tagnames)))
            childelms{:,end+1} = childnode; %#ok<AGROW>
        end
    end
    childelms = [childelms{:}];
    
    if nargin >= 3 && not(isempty(checkcount))
        if numel(childelms) ~= checkcount
            error(['Expected ',num2str(checkcount),' child elements, but found ',num2str(numel(childelms)),'.']);
        end
    end
end
