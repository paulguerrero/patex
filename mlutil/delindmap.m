% map from old to new indices after deleting elements delinds from a vector
% of size n
function [map,delmask,inds] = delindmap(delinds,n,inds)

    keepmask = true(1,n);
    keepmask(delinds(not(isnan(delinds)))) = false;
    map = nan(1,n);
    map(keepmask) = 1:sum(keepmask);
    
    delmask = not(keepmask);
    
    if nargin >= 3
        mask = not(isnan(inds));
        inds(mask) = map(inds(mask));
    end
end
