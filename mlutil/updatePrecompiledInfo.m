function updatePrecompiledInfo(infofilename,functions,lang)
    
    functions = functions(:);

    [currentfunctions,currentfilenames,currentruntimes] = readPrecompiledInfo(infofilename);

    selcc = mex.getCompilerConfigurations(lang,'Selected');
    runtimes = selcc.ShortName;
    
    ext = mexext;
    
    filenames = cell(numel(functions),1);
    for i=1:numel(functions)
        filenames{i} = [functions{i},'.',ext];
    end
    runtimes = repmat({runtimes},numel(filenames),1);
    
    functions = unique([currentfunctions;functions]);
    [filenames,ind] = unique([currentfilenames;filenames]);
    runtimes = [currentruntimes;runtimes];
    runtimes = runtimes(ind);
    
    writePrecompiledInfo(infofilename,functions,filenames,runtimes);
end
