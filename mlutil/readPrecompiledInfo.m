function [functions,filenames,runtimes] = readPrecompiledInfo(infofilename)
    functions = cell(0,1);
    filenames = cell(0,1);
    runtimes = cell(0,1);    

    fid = fopen(['./',infofilename],'r');

    if isempty(fid) || fid < 0
        % do nothing
    else
        % get all lines as strings
        lines = cell(0,1);
        tline = fgetl(fid);
        while ischar(tline)
            lines{end+1,:} = tline; %#ok<AGROW>
            tline = fgetl(fid);
        end
        
        % parse lines
        readingfunctions = false;
        readingruntimes = false;
        for i=1:numel(lines)
            
            if strcmp(lines{i},'[functions]')
                readingfunctions = true;
                readingruntimes = false;
            elseif strcmp(lines{i},'[runtimes]')
                readingfunctions = false;
                readingruntimes = true;
            else
                if readingfunctions
                    functions(end+1,:) = lines(i); %#ok<AGROW>
                elseif readingruntimes
                    val = textscan(lines{i},'%s %s','Delimiter',':');
                    if numel(val) ~= 2 || numel(val{1}) ~= 1 || numel(val{2}) ~= 1
                        warning('Invalid line in precompiled info file found.');
                        continue;
                    end
                    filenames(end+1,:) = val{1}(1); %#ok<AGROW>
                    runtimes(end+1,:) = val{2}(1); %#ok<AGROW>
                else
                    warning('Invalid precompiled info file found.');
                    break;
                end
            end
        end
        
        fclose(fid);
    end
end
