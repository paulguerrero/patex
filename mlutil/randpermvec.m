% p: k non-repeating random integer vectors in the subspace defined by
% minint,maxint
% a: k non-repating random integers that were used to generate the vectors
function [p,a] = randpermvec(minint,maxint,k)

    basis = 1 + maxint - minint;
    
    if not(all(basis > 1))
        error('maxint must be > minint');
    end
    
    ncombs = prod(basis);
    
    if ncombs > 2^50
        error('Too many combinations, the number of combinations cannot be represented accurately as a double.');
    end
    
    if k > ncombs
        error('Requested more vectors than possible combinations.');
    end
    
    a = randperm(ncombs,k);
    
    p = zeros(k,numel(basis));
    for i=1:k
        p(i,:) = de2mixedbasis(a(i),basis) + minint;
    end
    
end
