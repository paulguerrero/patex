function closeAllFigures(exceptnames)
    
    if not(iscell(exceptnames))
        exceptnames = {exceptnames};
    end

    fighandles = findobj('Type','figure');
    fignames = get(fighandles,'Name');
    mask = ismember(fignames,exceptnames);
    close(fighandles(not(mask)));
end
