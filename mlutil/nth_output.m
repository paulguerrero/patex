function value = nth_output(n,fcn,varargin)
  [value{1:n}] = fcn(varargin{:});
  value = value{n};
end
