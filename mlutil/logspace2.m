% n real numbers with equal spacing in log space
% shiftinv makes the sequence shift invariant by shifting the interval to
% start at 1
function y = logspace2(x1,x2,n,shiftinv)
    
    if nargin < 5 || isempty(shiftinv)
        shiftinv = false;
    end
    
    % shift the interval [x1,x2] so it starts at 1
    if shiftinv
        shift = x1-1;
        x2 = x2 - shift;
        x1 = 1;
    end
    
    y = exp(linspace(log(x1),log(x2),n));
    
    % shift the interval back to its original position
    if shiftinv
        y = y + shift;
    end
end
