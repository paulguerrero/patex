% Jensen�Shannon divergence of two univariate normal distributions
% since KL-divergence uses logarithm 2, JS-divergence is bounded by 1
function jsdiv = normjsdiv(mean1,sdev1,mean2,sdev2)

    error('Should be average of distributions, not distribution of average of random variable.');
    mean3 = (mean1+mean2) ./ 2;
    sdev3 = (sdev1.^2 + sdev2.^2) ./ 2;

    jsdiv = ...
        0.5 .* normkldiv(mean1,sdev1,mean3,sdev3) + ...
        0.5 .* normkldiv(mean2,sdev2,mean3,sdev3);
end
