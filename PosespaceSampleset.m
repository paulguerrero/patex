classdef PosespaceSampleset < handle

properties
    val = [];
    
    posx = nan;
    posy = nan;
    posz = nan;
    angleu = nan;
    anglev = nan;
    anglew = nan;
    scale = nan;
    mir = nan;
    
    % variance of pose dimensions
    posxvar = nan;
    posyvar = nan;
    poszvar = nan;
    angleuvar = nan;
    anglevvar = nan;
    anglewvar = nan;
    scalevar = nan;
    mirvar = nan;
    
    addinfo = struct;
end

properties(Dependent)
    pose;
    posevar;
end

methods
    
function obj = PosespaceSampleset(val,pose,posevar,varargin)

    if abs(nargin) == 0
        % for empty()
        return;
    elseif abs(nargin) == 1
        obj2 = val;
        obj.copyFrom(obj2);
    elseif abs(nargin) >= 3
    
        if not(isrow(val))
            error('The function values must be a row vector.');
        end

        obj.val = val;


        if isfield(pose,'z')
            % pose values
            obj.posx = pose.x;
            obj.posy = pose.y;
            obj.posz = pose.z;
            obj.angleu = pose.au;
            obj.anglev = pose.av;
            obj.anglew = pose.aw;
            obj.scale = pose.s;
            obj.mir = pose.m;

            % pose variances
            if not(isempty(posevar))
                obj.posxvar = posevar.x;
                obj.posyvar = posevar.y;
                obj.poszvar = posevar.z;
                obj.angleuvar = posevar.au;
                obj.anglevvar = posevar.av;
                obj.anglewvar = posevar.aw;
                obj.scalevar = posevar.s;
                obj.mirvar = posevar.m;
            end
        else
            % pose values
            obj.posx = pose.x;
            obj.posy = pose.y;
            obj.angleu = pose.a;
            obj.scale = pose.s;
            obj.mir = pose.m;

            % pose variances
            if not(isempty(posevar))
                obj.posxvar = posevar.x;
                obj.posyvar = posevar.y;
                obj.angleuvar = posevar.a;
                obj.scalevar = posevar.s;
                obj.mirvar = posevar.m;
            end
        end

        if (isempty(obj.posx) || not(isnan(obj.posx(1)))) && any(size(obj.posx) ~= size(obj.val)) || ...
           (isempty(obj.posy) || not(isnan(obj.posy(1)))) && any(size(obj.posy) ~= size(obj.val)) || ...
           (isempty(obj.posz) || not(isnan(obj.posz(1)))) && any(size(obj.posz) ~= size(obj.val)) || ...
           (isempty(obj.angleu) || not(isnan(obj.angleu(1)))) && any(size(obj.angleu) ~= size(obj.val)) || ...
           (isempty(obj.anglev) || not(isnan(obj.anglev(1)))) && any(size(obj.anglev) ~= size(obj.val)) || ...
           (isempty(obj.anglew) || not(isnan(obj.anglew(1)))) && any(size(obj.anglew) ~= size(obj.val)) || ...
           (isempty(obj.scale) || not(isnan(obj.scale(1)))) && any(size(obj.scale) ~= size(obj.val)) || ...
           (isempty(obj.mir) || not(isnan(obj.mir(1)))) && any(size(obj.mir) ~= size(obj.val))
            error('Not all inputs have the same size.');
        end

        if abs(nargin) >= 4
            if numel(varargin) == 1 && isstruct(varargin{1})
                obj.addinfo = varargin{1};
            else
%                 nvpairs = nvpairs2struct(varargin,{},true);
%                 nvnames = fieldnames(nvpairs);
%                 for i=1:numel(nvnames)
%                     if size(nvpairs.(nvnames{i})) ~= size(val)
%                         error('Not all inputs have the same size.');
%                     end
%                     obj.addinfo.(nvnames{i}) = nvpairs.(nvnames{i});
%                 end
                
                obj.addinfo = nvpairs2struct(varargin);
            end
            
            nvnames = fieldnames(obj.addinfo);
            for i=1:numel(nvnames)
                if size(obj.addinfo.(nvnames{i})) ~= size(val)
                    error('Not all inputs have the same size.');
                end
            end
        end
    
    else
        error('Invalid arguments.');
    end
    
end

function copyFrom(obj,obj2)
    obj.val = obj2.val;
    
    obj.posx = obj2.posx;
    obj.posy = obj2.posy;
    obj.posz = obj2.posz;
    obj.angleu = obj2.angleu;
    obj.anglev = obj2.anglev;
    obj.anglew = obj2.anglew;
    obj.scale = obj2.scale;
    obj.mir = obj2.mir;
    
    obj.posxvar = obj2.posxvar;
    obj.posyvar = obj2.posyvar;
    obj.poszvar = obj2.poszvar;
    obj.angleuvar = obj2.angleuvar;
    obj.anglevvar = obj2.anglevvar;
    obj.anglewvar = obj2.anglewvar;
    obj.scalevar = obj2.scalevar;
    obj.mirvar = obj2.mirvar;
    
    obj.addinfo = obj2.addinfo;
end

function obj2 = clone(obj)
    obj2 = PosespaceSampleset(obj);
end

function sset = subset(obj,inds)
    if nargin < 2
        inds = 1:numel(obj.val);
    end

    if islogical(inds)
        inds = find(inds);
    end

    addinfo_ = obj.addinfo;
    addinfonames = fieldnames(obj.addinfo);
    for i=1:numel(addinfonames)
        addinfo_.(addinfonames{i}) = addinfo_.(addinfonames{i})(inds);
    end

    pose = struct(...
        'x',iif(not(isempty(obj.posx)) && isnan(obj.posx(1)),nan,1,obj.posx(inds)),...
        'y',iif(not(isempty(obj.posy)) && isnan(obj.posy(1)),nan,1,obj.posy(inds)),...
        'z',iif(not(isempty(obj.posz)) && isnan(obj.posz(1)),nan,1,obj.posz(inds)),...
        'au',iif(not(isempty(obj.angleu)) && isnan(obj.angleu(1)),nan,1,obj.angleu(inds)),...
        'av',iif(not(isempty(obj.anglev)) && isnan(obj.anglev(1)),nan,1,obj.anglev(inds)),...
        'aw',iif(not(isempty(obj.anglew)) && isnan(obj.anglew(1)),nan,1,obj.anglew(inds)),...
        's',iif(not(isempty(obj.scale)) && isnan(obj.scale(1)),nan,1,obj.scale(inds)),...
        'm',iif(not(isempty(obj.mir)) && isnan(obj.mir(1)),nan,1,obj.mir(inds)));

    posevar = struct(...
        'x',iif(not(isempty(obj.posxvar)) && isnan(obj.posxvar(1)),nan,1,obj.posxvar(inds)),...
        'y',iif(not(isempty(obj.posyvar)) && isnan(obj.posyvar(1)),nan,1,obj.posyvar(inds)),...
        'z',iif(not(isempty(obj.poszvar)) && isnan(obj.poszvar(1)),nan,1,obj.poszvar(inds)),...
        'au',iif(not(isempty(obj.angleuvar)) && isnan(obj.angleuvar(1)),nan,1,obj.angleuvar(inds)),...
        'av',iif(not(isempty(obj.anglevvar)) && isnan(obj.anglevvar(1)),nan,1,obj.anglevvar(inds)),...
        'aw',iif(not(isempty(obj.anglewvar)) && isnan(obj.anglewvar(1)),nan,1,obj.anglewvar(inds)),...
        's',iif(not(isempty(obj.scalevar)) && isnan(obj.scalevar(1)),nan,1,obj.scalevar(inds)),...
        'm',iif(not(isempty(obj.mirvar)) && isnan(obj.mirvar(1)),nan,1,obj.mirvar(inds)));

    sset = PosespaceSampleset(obj.val(inds),pose,posevar,addinfo_);
end

function p = get.pose(obj)
%         if nargin < 2
%             inds = 1:numel(obj.val);
%         end
%         
%         if islogical(inds)
%             inds = find(inds);
%         end

    p = [...
        iif(not(isempty(obj.posx)) && isnan(obj.posx(1)),[],1,obj.posx);...
        iif(not(isempty(obj.posy)) && isnan(obj.posy(1)),[],1,obj.posy);...
        iif(not(isempty(obj.posz)) && isnan(obj.posz(1)),[],1,obj.posz);...
        iif(not(isempty(obj.angleu)) && isnan(obj.angleu(1)),[],1,obj.angleu);...
        iif(not(isempty(obj.anglev)) && isnan(obj.anglev(1)),[],1,obj.anglev);...
        iif(not(isempty(obj.anglew)) && isnan(obj.anglew(1)),[],1,obj.anglew);...
        iif(not(isempty(obj.scale)) && isnan(obj.scale(1)),[],1,obj.scale);...
        iif(not(isempty(obj.mir)) && isnan(obj.mir(1)),[],1,obj.mir)];
end

function p = get.posevar(obj)
%         if nargin < 2
%             inds = 1:numel(obj.val);
%         end
%         
%         if islogical(inds)
%             inds = find(inds);
%         end

    p = [...
        iif(not(isempty(obj.posxvar)) && isnan(obj.posxvar(1)),[],1,obj.posxvar);...
        iif(not(isempty(obj.posyvar)) && isnan(obj.posyvar(1)),[],1,obj.posyvar);...
        iif(not(isempty(obj.poszvar)) && isnan(obj.poszvar(1)),[],1,obj.poszvar);...
        iif(not(isempty(obj.angleuvar)) && isnan(obj.angleuvar(1)),[],1,obj.angleuvar);...
        iif(not(isempty(obj.anglevvar)) && isnan(obj.anglevvar(1)),[],1,obj.anglevvar);...
        iif(not(isempty(obj.anglewvar)) && isnan(obj.anglewvar(1)),[],1,obj.anglewvar);...
        iif(not(isempty(obj.scalevar)) && isnan(obj.scalevar(1)),[],1,obj.scalevar);...
        iif(not(isempty(obj.mirvar)) && isnan(obj.mirvar(1)),[],1,obj.mirvar)];
end

function obj = remove(obj,inds)
    if islogical(inds)
        inds = find(inds);
    end

    obj.val(inds) = [];
    
    if isempty(obj.posx) || not(isnan(obj.posx))
        obj.posx(inds) = [];
    end
    if isempty(obj.posy) || not(isnan(obj.posy))
        obj.posy(inds) = [];
    end
    if isempty(obj.posz) || not(isnan(obj.posz))
        obj.posz(inds) = [];
    end
    if isempty(obj.angleu) || not(isnan(obj.angleu))
        obj.angleu(inds) = [];
    end
    if isempty(obj.anglev) || not(isnan(obj.anglev))
        obj.anglev(inds) = [];
    end
    if isempty(obj.anglew) || not(isnan(obj.anglew))
        obj.anglew(inds) = [];
    end
    if isempty(obj.scale) || not(isnan(obj.scale))
        obj.scale(inds) = [];
    end
    if isempty(obj.mir) || not(isnan(obj.mir))
        obj.mir(inds) = [];
    end
    addinfonames = fieldnames(obj.addinfo);
    for i=1:numel(addinfonames)
        obj.addinfo.(addinfonames{i})(inds) = [];
    end
end    

function obj = useDefaultForUnavailable(obj,poseelms)
    for i=1:numel(poseelms)
        if strcmp(poseelms{i},'posx') && not(isempty(obj.posx)) && isnan(obj.posx)
            obj.posx = zeros(1,numel(obj.val));
        elseif strcmp(poseelms{i},'posy') && not(isempty(obj.posy)) && isnan(obj.posy)
            obj.posy = zeros(1,numel(obj.val));
        elseif strcmp(poseelms{i},'posz') && not(isempty(obj.posz)) && isnan(obj.posz)
            obj.posz = zeros(1,numel(obj.val));
        elseif strcmp(poseelms{i},'angleu') && not(isempty(obj.angleu)) && isnan(obj.angleu)
            obj.angleu = zeros(1,numel(obj.val));
        elseif strcmp(poseelms{i},'anglev') && not(isempty(obj.anglev)) && isnan(obj.anglev)
            obj.anglev = zeros(1,numel(obj.val));
        elseif strcmp(poseelms{i},'anglew') && not(isempty(obj.anglew)) && isnan(obj.anglew)
            obj.anglew = zeros(1,numel(obj.val));
        elseif strcmp(poseelms{i},'scale') && not(isempty(obj.scale)) && isnan(obj.scale)
            obj.scale = ones(1,numel(obj.val));
        elseif strcmp(poseelms{i},'mir') && not(isempty(obj.mir)) && isnan(obj.mir)
            obj.mir = false(1,numel(obj.val));
        end
    end
end

end

end
