classdef PatternGraph < handle

properties(SetAccess=protected)
    % graph edges (negative for source edge, positive for target edge)
    % rows contain the childs (less meta) of nodes (i.e. element nodes
    % have empty rows)
    adjacency = sparse([]);
    
    % graph nodes (indexed in this order)
    elms = ScenePolygonset.empty(1,0);
    elmrels = reltype.PolygonBoundarydist.empty(1,0);
    metarels = reltype.RelDiff.empty(1,0);
    grpnodes = struct('value',cell(1,0),'variance',cell(1,0));
    grprels = reltype.RelDiff.empty(1,0)
    
    % level of the nodes in the graph (shortest distance to elements)
    level = zeros(1,0);
    
    % groups of graph nodes, each column is a group
    groups = sparse([]);
    
    % hierarchy structure: tree of graph nodes,
    % rows contain the childs of hierarchy nodes (i.e. non-composite nodes
    % have empty rows)
    hierarchy = sparse([]);
    
    % level of the nodes in the hierarchy (shortest distance to hierarchy leafs)
    hlevel = zeros(1,0);
    
    % instance groups: hierarchy elements that are exact clones are grouped
    % together. There should be no relationships between two instance group
    % members. Each column is a group, rows are nodes.
    instgroups = sparse([]);
    
    % variance of element poses
    elmposevar = [0.4;0.4;pi/8;0.2;0.2;1].^2
    
    fixedposeinds = sparse(6,0);
end

properties(Dependent, SetAccess=protected)
    % all nodes of the graph (in a cell array)
    nodes;
    % all relationships of the graph
    rels;
    
    % convert relationship to node indices
    rel2nodeinds;
    % convert node to relationship indices
    node2relinds;
    
    % offset of each node type in the list of nodes
    elmoffset;
    elmreloffset;
    metareloffset;
    grpnodeoffset;
    grpreloffset;
    hnodeoffset;
    
    % source / target child node indices for each node type
    relsrcind;
    reltgtind;
    elmrelsrcind;
    elmreltgtind;
    metarelsrcind;
    metareltgtind;
    grprelsrcind;
    grpreltgtind;
    
    nodedist;
end

properties(Access=protected)
    % dirty if size(...,1) == 0
    p_nodes = cell(0,0);
    p_rels = reltype.PolygonBoundarydist.empty(0,0);
    p_rel2nodeinds = zeros(0,0);
    p_node2relinds = zeros(0,0);
    
    p_nodedist = zeros(0,0);
end

methods

% obj = PatternGraph()
% obj = PatternGraph(obj2)
% obj = PatternGraph(adjacency,elms,elmrels,metarels)
% obj = PatternGraph(adjacency,elms,elmrels,metarels,groups)
function obj = PatternGraph(varargin)
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'PatternGraph')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 4 || numel(varargin) == 5
        obj.adjacency = varargin{1};
        obj.elms = varargin{2};
        obj.elmrels = varargin{3};
        obj.metarels = varargin{4};
        nnodes = numel(obj.elms) + numel(obj.elmrels) + numel(obj.metarels);
        if numel(size(obj.adjacency)) ~= 2 || any(size(obj.adjacency)) ~= nnodes
            error('Invalid size for adjacency matrix');
        end
        if numel(varargin) == 5
            obj.groups = varargin{5};
            if numel(size(obj.groups)) ~= 2 || size(obj.groups,1) ~= nnodes
                error('Invalid size for groups matrix');
            end
        else
            obj.groups = sparse(size(obj.adjacency,1),0);
        end
    else
        error('Invalid arguments.');
    end
end

function delete(obj)
    delete(obj.elms(isvalid(obj.elms)));
    delete(obj.elmrels(isvalid(obj.elmrels)));
    delete(obj.metarels(isvalid(obj.metarels)));
    delete(obj.grprels(isvalid(obj.grprels)));
end

function dirty(obj)
    obj.p_nodes = cell(0,0);
    obj.p_rels = reltype.PolygonBoundarydist.empty(0,0);
    obj.p_rel2nodeinds = zeros(0,0);
    obj.p_node2relinds = zeros(0,0);
    obj.p_nodedist = zeros(1,0);
end

function copyFrom(obj,obj2)
    obj.adjacency = obj2.adjacency;
    obj.elms = obj2.elms;
    for i=1:numel(obj.elms)
        obj.elms(i) = obj.elms(i).clone;
    end
    obj.elmrels = obj2.elmrels;
    for i=1:numel(obj.elmrels)
        obj.elmrels(i) = obj.elmrels(i).clone;
    end
    obj.metarels = obj2.metarels;
    for i=1:numel(obj.metarels)
        obj.metarels(i) = obj.metarels(i).clone;
    end
    obj.grprels = obj2.grprels;
    for i=1:numel(obj.grprels)
        obj.grprels(i) = obj.grprels(i).clone;
    end
    obj.grpnodes = obj2.grpnodes;
    obj.level = obj2.level;
    obj.groups = obj2.groups;
    
    obj.hierarchy = obj2.hierarchy;
    obj.hlevel = obj2.hlevel;
    
    obj.instgroups = obj2.instgroups;
    
    obj.elmposevar = obj2.elmposevar;
    
    obj.fixedposeinds = obj2.fixedposeinds;
    
    obj.dirty;
    
    % can't just clone p_nodes etc., since those should point to the
    % node that was cloned above (and not to a new clone node)
end

function obj2 = clone(obj)
    obj2 = PatternGraph(obj);
end

function setElmposevar(obj,elmposevar)
    
    if numel(elmposevar) ~= 6
        error('Must give variance for all 6 components of the pose.');
    end
    obj.elmposevar(:) = elmposevar;
end

function scale(obj,val)
    if val < 0.0001
        error('Scaling factor too small.');
    end
    
    for i=1:numel(obj.elms)
        obj.elms(i).scale(val,'relative',[0;0]);
    end
    obj.recomputeRelationships;
end

function groupind = addGroup(obj,nodeinds,allowoverlaps)
    if nargin < 3 || isempty(allowoverlaps)
        allowoverlaps = true;
    end
    
    if allowoverlaps
        
        % check if group already exists
        nodemask = false(size(obj.adjacency,1),1);
        nodemask(nodeinds) = true;
        groupind = find(all(bsxfun(@and,obj.groups,nodemask),1));

        if not(isempty(groupind))
            % remove all groups where the nodes are the only element
            grpinds = find(any(obj.groups(nodeinds,:),1));
            grpsize = sum(obj.groups(:,grpinds)~=0,1);
            obj.groups(:,grpinds(grpsize<=1)) = [];

            obj.groups(:,end+1) = 0;
            obj.groups(nodeinds,end) = 1;
            groupind = size(obj.groups,2);
        end
    else
        % remove nodes from all existing groups (and remove empty groups)
        obj.groups(nodeinds,:) = 0;
        obj.groups(:,not(any(obj.groups,1))) = [];

        obj.groups(:,end+1) = 0;
        obj.groups(nodeinds,end) = 1;
        groupind = size(obj.groups,2);
    end
end

function removeGroups(obj,groupinds)
    membernodeinds = find(any(obj.groups(:,groupinds),2));
    obj.groups(:,groupinds) = [];
    
    % add one-member groups for all nodes that are left without a group
    nogroupnodeinds = membernodeinds(not(any(obj.groups(membernodeinds,:),2)));
    obj.groups(nogroupnodeinds,end+1:end+numel(nogroupnodeinds)) = speye(numel(nogroupnodeinds));
end

function fixPoses(obj,elminds,fixmask)
    if elminds < 1 || elminds > numel(obj.elms)
        error('Invalid element indices.');
    end
    
    elmnodeinds = elminds + obj.elmoffset;
    obj.fixedposeinds(:,elmnodeinds) = fixmask;
end

function groupind = addInstgroup(obj,elminds)
    if isempty(elminds)
        return;
    end
    
    elmnodeinds = elminds + obj.elmoffset;
    
    if not(all(obj.hlevel(elmnodeinds))) > 0
        error('All given elements for the instance group must be composite.');
    end
    
    % remove nodes from all existing groups (and remove empty groups)
    obj.instgroups(elmnodeinds,:) = 0;
    obj.instgroups(:,not(any(obj.instgroups,1))) = [];

    obj.instgroups(:,end+1) = 0;
    obj.instgroups(elmnodeinds,end) = 1;
    groupind = size(obj.groups,2);
    
    % get all child elements to local coordinates
    childelminds = cell(1,numel(elminds));
    localchildelmposes = cell(1,numel(elminds));
    ischildnode = false(1,numel(obj.elms));
    for i=1:numel(elminds)
        childelminds{i} = find(obj.hierarchy(elmnodeinds(i),:)) - obj.elmoffset;
        childelminds{i}(childelminds{i} < 1 | childelminds{i} > numel(obj.elms)) = [];
        localchildelmposes{i} = obj.localHierarchyPoses(childelminds{i});
        if any(ischildnode(childelminds{i}))
            error('Different instances share child elements.');
            % child elements must not overlap, otherwise reordering them
            % would not work
        end
        ischildnode(childelminds{i}) = true;
    end
    
    % change order of hierarchy childs of group members so childs are
    % ordered according to corresponding elements (1st corresponding to
    % 1st, etc.). First composite element is used as reference for order.
    for i=2:numel(elminds)
        if numel(localchildelmposes{i}) ~= numel(localchildelmposes{1})
            error('Number of child elements of instances does not match.');
        end
        
        dists = bsxfun(@minus,localchildelmposes{i}(1,:)',localchildelmposes{1}(1,:)).^2 + ...
                bsxfun(@minus,localchildelmposes{i}(2,:)',localchildelmposes{1}(2,:)).^2;
        
%         [~,perm] = min(dists,[],1);
        perm = munkres(dists');
        if any(sort(perm,'ascend') ~= 1:numel(childelminds{i}))
            error('Local element positions do not seem to match in two instances.');
        end
        obj.reorderElements(childelminds{i},perm);
    end
end

function reorderElements(obj,elminds,perm)
    if all(perm == 1:numel(elminds))
        return;
    end
    
    elmnodeinds = elminds + obj.elmoffset;
    
%     obj.adjacency(elmnodeinds,elmnodeinds) = obj.adjacency(elmnodeinds(perm),elmnodeinds(perm));
    obj.adjacency(elmnodeinds,:) = obj.adjacency(elmnodeinds(perm),:);
    obj.adjacency(:,elmnodeinds) = obj.adjacency(:,elmnodeinds(perm));
    
    % graph edges (negative for source edge, positive for target edge)
    % rows contain the childs (less meta) of nodes (i.e. element nodes
    % have empty rows)
    obj.elms(elminds) = obj.elms(elminds(perm));
    obj.level(elmnodeinds) = obj.level(elmnodeinds(perm));
    
    obj.groups(elmnodeinds,:) = obj.groups(elmnodeinds(perm),:);
%     obj.hierarchy(elmnodeinds,elmnodeinds) = obj.hierarchy(elmnodeinds(perm),elmnodeinds(perm));
    obj.hierarchy(elmnodeinds,:) = obj.hierarchy(elmnodeinds(perm),:);
    obj.hierarchy(:,elmnodeinds) = obj.hierarchy(:,elmnodeinds(perm));
    
    obj.hlevel(elmnodeinds) = obj.hlevel(elmnodeinds(perm));
    
    obj.instgroups(elmnodeinds,:) = obj.instgroups(elmnodeinds(perm),:);
    
    obj.fixedposeinds(:,elmnodeinds) = obj.fixedposeinds(:,elmnodeinds(perm));
    
    obj.dirty;
end

function poses = localHierarchyPoses(obj,elminds)
    elmnodeinds = elminds + obj.elmoffset;
    
    elmparentinds = nan(1,numel(elmnodeinds));
    for i=1:numel(elmnodeinds)
        elmparentind = find(obj.hierarchy(:,elmnodeinds(i))) - obj.elmoffset;
        if isempty(elmparentind)
            % do nothing (remains nan)
        elseif numel(elmparentind) == 1
            if elmparentind < 1 || elmparentind > numel(obj.elms)
                error('Corrupt pattern graph, element parent is not an element.');
            end
            elmparentinds(i) = elmparentind;
        else
            error('Corrupt pattern graph, element has more than one hierarchy parent.');
        end
    end
    
    poses = [obj.elms(elminds).pose2D];
    mask = not(isnan(elmparentinds));
    poses(:,mask) = posetransform(poses(:,mask), repmat(identpose2D,1,sum(mask)), [obj.elms(elmparentinds(mask)).pose2D]);
end

function removeInstgroups(obj,groupinds)
    obj.instgroups(:,groupinds) = [];
end

function val = get.elmoffset(obj) %#ok<MANU>
    val = 0;
end

function val = get.elmreloffset(obj)
    val = numel(obj.elms);
end

function val = get.metareloffset(obj)
    val = numel(obj.elms) + numel(obj.elmrels);
end

function val = get.grpnodeoffset(obj)
    val = numel(obj.elms) + numel(obj.elmrels) + numel(obj.metarels);
end

function val = get.grpreloffset(obj)
    val = numel(obj.elms) + numel(obj.elmrels) + numel(obj.metarels) + numel(obj.grpnodes);
end

function val = get.hnodeoffset(obj)
    val = numel(obj.elms) + numel(obj.elmrels) + numel(obj.metarels) + numel(obj.grpnodes) + numel(obj.grprels);
end

function val = get.elmrelsrcind(obj)
    offset = obj.elmreloffset;
    [ind,val] = find(obj.adjacency(offset+1:offset+numel(obj.elmrels),:) < 0);
    val(ind) = val;
    val = val';
end

function val = get.elmreltgtind(obj)
    offset = obj.elmreloffset;
    [ind,val] = find(obj.adjacency(offset+1:offset+numel(obj.elmrels),:) > 0);
    val(ind) = val;
    val = val';
end

function val = get.metarelsrcind(obj)
    offset = obj.metareloffset;
    [ind,val] = find(obj.adjacency(offset+1:offset+numel(obj.metarels),:) < 0);
    val(ind') = val;
    val = val';
end

function val = get.metareltgtind(obj)
    offset = obj.metareloffset;
    [ind,val] = find(obj.adjacency(offset+1:offset+numel(obj.metarels),:) > 0);
    val(ind') = val;
    val = val';
end

function val = get.grprelsrcind(obj)
    offset = obj.grpreloffset;
    [ind,val] = find(obj.adjacency(offset+1:offset+numel(obj.grprels),:) < 0);
    val(ind') = val;
    val = val';
end

function val = get.grpreltgtind(obj)
    offset = obj.grpreloffset;
    [ind,val] = find(obj.adjacency(offset+1:offset+numel(obj.grprels),:) > 0);
    val(ind') = val;
    val = val';
end

function val = get.relsrcind(obj)
    nodeinds = obj.rel2nodeinds(1:numel(obj.rels));
    [ind,val] = find(obj.adjacency(nodeinds,:) < 0);
    val(ind) = val;
    val = val';
end

function val = get.reltgtind(obj)
    nodeinds = obj.rel2nodeinds(1:numel(obj.rels));
    [ind,val] = find(obj.adjacency(nodeinds,:) > 0);
    val(ind) = val;
    val = val';
end

function val = get.nodes(obj)
    if size(obj.p_nodes,1) == 0
        obj.updateNodes;
    end
    val = obj.p_nodes;
end

function val = get.rels(obj)
    if size(obj.p_rels,1) == 0
        obj.updateRels;
    end
    val = obj.p_rels;
end

function val = get.rel2nodeinds(obj)
    if size(obj.p_rel2nodeinds,1) == 0
        obj.updateRel2nodeinds;
    end
    val = obj.p_rel2nodeinds;
end

function val = get.node2relinds(obj)
    if size(obj.p_node2relinds,1) == 0
        obj.updateNode2relinds;
    end
    val = obj.p_node2relinds;
end

function val = get.nodedist(obj)
    if isempty(obj.p_nodedist)
        obj.updateNodedist;
    end
    val = obj.p_nodedist;
end

function updateNodedist(obj)
    adj = obj.adjacency ~= 0;
    adj = adj | adj'; % makes edges undirected
    obj.p_nodedist = graphallshortestpaths(adj,'Directed',false);
end

function updateNodes(obj)
    obj.p_nodes = [...
        num2cell(obj.elms),...
        num2cell(obj.elmrels),...
        num2cell(obj.metarels),...
        num2cell(obj.grpnodes),...
        num2cell(obj.grprels)];
%         mat2cell(obj.hnodes,size(obj.hnodes,1),ones(1,size(obj.hnodes,2)))];
end

function updateRels(obj)
    obj.p_rels = [...
        obj.elmrels,...
        obj.metarels,...
        obj.grprels];
end

function updateRel2nodeinds(obj)
    elmreloffs = obj.elmreloffset;
    metareloffs = obj.metareloffset;
    grpreloffs = obj.grpreloffset;
    
    obj.p_rel2nodeinds = [...
        elmreloffs+1:elmreloffs+numel(obj.elmrels),...
        metareloffs+1:metareloffs+numel(obj.metarels),...
        grpreloffs+1:grpreloffs+numel(obj.grprels)];
end

function updateNode2relinds(obj)
    elmreloffs = obj.elmreloffset;
    metareloffs = obj.metareloffset;
    grpreloffs = obj.grpreloffset;
    
    obj.p_node2relinds = nan(1,numel(obj.nodes));
    
    reloffset = 0;
    obj.p_node2relinds(elmreloffs+1:elmreloffs+numel(obj.elmrels)) = reloffset+1:reloffset+numel(obj.elmrels);
    reloffset = reloffset + numel(obj.elmrels);
    obj.p_node2relinds(metareloffs+1:metareloffs+numel(obj.metarels)) = reloffset+1:reloffset+numel(obj.metarels);
    reloffset = reloffset + numel(obj.metarels);
    obj.p_node2relinds(grpreloffs+1:grpreloffs+numel(obj.grprels)) = reloffset+1:reloffset+numel(obj.grprels);
end

function newelmind = addHierarchyelement(obj,nodeinds)
    elmnodeinds = nodeinds(nodeinds > obj.elmoffset & nodeinds <= obj.elmoffset+numel(obj.elms));
    
    if isempty(elmnodeinds)
        error('A hierarchy element must contain at least one element or hierarchy element.');
    end
    
    % create polygon that represents the composite node
    % currently as convex hull of all childs in the hierarchy
    elminds = elmnodeinds - obj.elmoffset;
    childverts = [obj.elms(elminds).verts];
    chullvertinds = convhull(childverts','simplify',true);
    if chullvertinds(1) == chullvertinds(end)
        chullvertinds(end) = [];
    end
    chull = childverts(:,chullvertinds);
    
    hnodeelm = ScenePolygonset(ScenePolygon(chull,''));
%     hnodepoly.defineOrientation(0);

    newelmind = obj.addElements(hnodeelm,{nodeinds});
    
    obj.dirty;
    
    obj.updateHierarchyelements(newelmind);
end

% update hierarchy elements from their child elements
function updateHierarchyelements(obj,elminds)
    % update the polygon that represents the composite node
    % currently as convex hull of all hierarchy childs
    
    if nargin < 2 || isempty(elminds)
        elminds = 1:numel(obj.elms);
    end
    
    eoffset = obj.elmoffset;

    % ignore level 0 (non-composite) elements
    elmhlevel = obj.hlevel(elminds);
    level0mask = elmhlevel == 0;
    elminds(level0mask) = [];
    elmhlevel(level0mask) = [];
    
    elminds = array2cell_mex(elminds,elmhlevel);
    
    for i=1:numel(elminds)
        for j=1:numel(elminds{i})
            childnodeinds = find(obj.hierarchy(elminds{i}(j)+eoffset,:));
            childelminds = childnodeinds-eoffset;
            childelminds(childelminds < 1 | childelminds > numel(obj.elms)) = [];
            childverts = [obj.elms(childelminds).verts];
            chullvertinds = convhull(childverts','simplify',true);
            if chullvertinds(1) == chullvertinds(end)
                chullvertinds(end) = [];
            end
            
            
            obj.elms(elminds{i}(j)).polygons(1).setVerts(childverts(:,chullvertinds));
%             obj.elms(elminds{i}(j)).updateVerts;
            obj.elms(elminds{i}(j)).definePosition(obj.elms(elminds{i}(j)).center);
%             obj.elms(elminds{i}(j)).defineOrientation(0); % keep orientation
            elmrad = obj.elms(elminds{i}(j)).radius;
            obj.elms(elminds{i}(j)).defineSize([elmrad;elmrad]);
            obj.elms(elminds{i}(j)).defineMirrored(false);
        end
    end
end

% transform elements and update their hierarchy child elements
% pose: [positionx; positiony; rotation; uniform scale]
function transformElements(obj,elminds,newposes,updatehierarchychilds)
    
    if nargin < 4 || isempty(updatehierarchychilds)
        updatehierarchychilds = true;
    end
    
    eoffset = obj.elmoffset;
%     dnodeinds = obj.hierarchyDescendants(elminds+eoffset);
%     if any(ismember(dnodeinds,elminds+eoffset))
%         error('Some given hierarchy elements are childs of other given elements');
%     end
    
    % transform elements by hierarchy level, starting at lowest level
    elmhlevel = obj.hlevel(elminds+obj.elmoffset);
    
    % lowest levels first so elements in lower hierarchy levels get
    % transformed correctly by higher hierarchy levels
    [~,perm] = sort(elmhlevel,'ascend');
    
    for i=perm
        origpose = obj.elms(elminds(i)).pose2D;
        
        posedelta = [...
            bsxfun(@minus,newposes(1:2,i),origpose(1:2));...
            smod(newposes(3,i) - origpose(3),-pi,pi);...
            newposes(4:5,i) ./ origpose(4:5);...
            newposes(6,i) ~= origpose(6)];
        
        if all(posedelta == identpose2D)
            continue;
        end
        
        % transform element
        obj.elms(elminds(i)).mirror(posedelta(6),'relative',origpose(1:2),origpose(3));
        obj.elms(elminds(i)).scale(posedelta(4:5),'relative',origpose(1:2),origpose(3));
        obj.elms(elminds(i)).rotate(posedelta(3),'relative',origpose(1:2));
        obj.elms(elminds(i)).move(posedelta(1:2),'relative');
        
        if updatehierarchychilds
            % transform hierarchy descendants
            delminds = obj.hierarchyDescendants(elminds(i)+eoffset) - obj.elmoffset;
            delminds(delminds < 1 | delminds > numel(obj.elms)) = [];
            for j=1:numel(delminds)
                obj.elms(delminds(j)).mirror(posedelta(6),'relative',origpose(1:2),origpose(3));
                obj.elms(delminds(j)).scale(posedelta(4:5),'relative',origpose(1:2),origpose(3));
                obj.elms(delminds(j)).rotate(posedelta(3),'relative',origpose(1:2));
                obj.elms(delminds(j)).move(posedelta(1:2),'relative');
            end
        end
    end
end

function rinds = hierarchyRootelements(obj)
    elminds = 1:numel(obj.elms);
    elmnodeinds = elminds+obj.elmoffset;
    rinds = elminds(not(any(obj.hierarchy(:,elmnodeinds),1)));
end

function linds = hierarchyLeafelements(obj)
    elminds = 1:numel(obj.elms);
    elmnodeinds = elminds+obj.elmoffset;
    linds = elminds(not(any(obj.hierarchy(elmnodeinds,:),2)));
end

function dinds = hierarchyDescendants(obj,nodeinds)
    dinds = PatternGraph.descendants_s(obj.hierarchy,nodeinds);
end

function ainds = hierarchyAncestors(obj,nodeinds)
    ainds = PatternGraph.ancestors_s(obj.hierarchy,nodeinds);
end

% insertinds in the list of elms (non-integers to specify before or after)
% insertinds = addElements(obj,elms)
% insertinds = addElements(obj,elms,hierarchychilds)
% insertinds = addElements(obj,elms,hierarchychilds,insertinds)
function insertinds = addElements(obj,elms,varargin)
    [obj.elms,insertinds] = obj.addNodes(elms,obj.elms,0,[],[],varargin{:});
end

% insertinds = addElmrels(obj,elmrels,srcind,tgtind)
% insertinds = addElmrels(obj,elmrels,srcind,tgtind,insertinds)
% all must have same source- and target index
% srcind, tgtind in current adjacency matrix 
% insertinds in the list of elmrels (non-integers to specify before or after)
function insertinds = addElmrels(obj,elmrels,srcind,tgtind,varargin)
    
    offset = numel(obj.elms);
    [obj.elmrels,insertinds] = obj.addNodes(elmrels,obj.elmrels,offset,srcind,tgtind,[],varargin{:});
end

% insertinds = addMetarels(obj,metarels,srcind,tgtind)
% insertinds = addMetarels(obj,metarels,srcind,tgtind,insertinds)
% all must have same source- and target index
% srcind, tgtind in current adjacency matrix 
% insertinds in the list of metarels (non-integers to specify before or after)
function insertinds = addMetarels(obj,metarels,srcind,tgtind,varargin)
    
    offset = numel(obj.elms)+numel(obj.elmrels);
    [obj.metarels,insertinds] = obj.addNodes(metarels,obj.metarels,offset,srcind,tgtind,[],varargin{:});
end

% insertinds = addGrpnodes(obj,grpnodes)
% insertinds = addGrpnodes(obj,grpnodes,insertinds)
function insertinds = addGrpnodes(obj,grpnodes,varargin)
    
    offset = numel(obj.elms)+numel(obj.elmrels)+numel(obj.metarels);
    [obj.grpnodes,insertinds] = obj.addNodes(grpnodes,obj.grpnodes,offset,[],[],[],varargin{:});
end

% insertinds = addGrprels(obj,grprels,srcind,tgtind)
% insertinds = addGrprels(obj,grprels,srcind,tgtind,insertinds)
function insertinds = addGrprels(obj,grprels,srcind,tgtind,varargin)
    
    offset = numel(obj.elms)+numel(obj.elmrels)+numel(obj.metarels)+numel(obj.grpnodes);
    [obj.grprels,insertinds] = obj.addNodes(grprels,obj.grprels,offset,srcind,tgtind,[],varargin{:});
end

% returns the updated node list and the indices in the updated node list
% that contain the added nodes (in the same order the nodes were given)
function [nodelist,insertinds] = addNodes(obj,nodes,nodelist,nodelistoffset,srcind,tgtind,hierarchychilds,insertinds)
    
    if nargin < 6 || isempty(srcind) || isempty(tgtind)
        srcind = [];
        tgtind = [];
    end
    
    if nargin < 7 || isempty(hierarchychilds)
        hierarchychilds = cell(1,numel(nodes));
    end
    
    if nargin < 8 || isempty(insertinds)
        % if no insert indices are given, insert at the end of the node list
        insertinds = linspace(numel(nodelist),numel(nodelist)+1,numel(nodes)+2);
        insertinds([1,end]) = [];
    end
    
    if not(isempty(hierarchychilds))
        for i=1:numel(nodes)
            if not(isempty(hierarchychilds{i}))
                if ismember(find(any(obj.hierarchy(:,hierarchychilds{i}),1)),hierarchychilds{i})
                    error('Some of the given hierarchy childs are parents of other given hierarchy childs.');
                end
                if any(obj.hierarchy(:,hierarchychilds{i}),2)
                    error('Some of the given hierarchy childs are already part of a hierarchy element.');
                end
            end
        end
    end
    
    % get node permutation (to get nodes in the correct order)
    [~,nodeperm] = sort([1:size(obj.adjacency,1),insertinds+nodelistoffset]);
    
    % update adjacency matrix
    obj.adjacency(end+1:end+numel(nodes),end+1:end+numel(nodes)) = 0;
    obj.adjacency(end-numel(nodes)+1:end,srcind) = -1;
    obj.adjacency(end-numel(nodes)+1:end,tgtind) = 1;
%     obj.adjacency = obj.adjacency(nodeperm,nodeperm);
    obj.adjacency = obj.adjacency(nodeperm,:);
    obj.adjacency = obj.adjacency(:,nodeperm);
    
    % update node groups (each new node is a group with a single member)
    obj.groups(end+1:end+numel(nodes),end+1:end+numel(nodes)) = speye(numel(nodes));
    obj.groups = obj.groups(nodeperm,:);
    
    obj.instgroups(end+1:end+numel(nodes),:) = 0;
    obj.instgroups = obj.instgroups(nodeperm,:);
    
    % update node levels
    if isempty(srcind)
        srclvl = -1;
    else
        srclvl = obj.level(srcind);
    end
    if isempty(tgtind)
        tgtlvl = -1;
    else
        tgtlvl = obj.level(tgtind);
    end
    obj.level(:,end+1:end+numel(nodes)) = max(srclvl,tgtlvl)+1;
    obj.level = obj.level(:,nodeperm);
    

    % update node list
    [~,nodelistperm] = sort([1:numel(nodelist),insertinds]);
    nodelist(:,end+1:end+numel(nodes)) = nodes;
    nodelist = nodelist(:,nodelistperm);
    
    % find the new indices for the inserted nodes
    newnodeinds(nodelistperm) = 1:numel(nodelistperm);
    insertinds = newnodeinds(end-numel(insertinds)+1:end);
    
    nodeinds = size(obj.adjacency,1)-numel(nodes)+1:size(obj.adjacency,1);
    for i=1:numel(nodes)
        if not(isempty(hierarchychilds{i}))
            obj.hlevel(:,nodeinds(i)) = max(obj.hlevel(:,hierarchychilds{i}))+1;
        elseif not(isempty(srcind)) && not(isempty(tgtind))
            % relationships get the highest level of src and tgt
            obj.hlevel(:,nodeinds(i)) = max(obj.hlevel(srcind),obj.hlevel(tgtind));
        else
            obj.hlevel(:,nodeinds(i)) = 0;
        end
        
        obj.hierarchy(nodeinds(i),hierarchychilds{i}) = 1;
        obj.hierarchy(:,nodeinds(i)) = 0;
    end
    obj.hlevel = obj.hlevel(:,nodeperm);
%     obj.hierarchy = obj.hierarchy(nodeperm,nodeperm);
    obj.hierarchy = obj.hierarchy(nodeperm,:);
    obj.hierarchy = obj.hierarchy(:,nodeperm);
    
    obj.fixedposeinds(:,end+1:end+numel(nodes)) = 0;
    obj.fixedposeinds = obj.fixedposeinds(:,nodeperm);
    
    obj.dirty;
end

function removeinds = removeElements(obj,removeinds,varargin)
    removeinds = obj.removeNodes(removeinds,varargin{:});
end

function removeinds = removeElmrels(obj,removeinds,varargin)
    removeinds = obj.removeNodes(removeinds+obj.elmreloffset,varargin{:})-obj.elmreloffset;
end

function removeinds = removeMetarels(obj,removeinds,varargin)
    removeinds = obj.removeNodes(removeinds+obj.metareloffset,varargin{:})-obj.metareloffset;
end

function removeinds = removeGrpnodes(obj,removeinds,varargin)
    removeinds = obj.removeNodes(removeinds+obj.grpnodeoffset,varargin{:})-obj.grpnodeoffset;
end

function removeinds = removeGrprels(obj,removeinds,varargin)
    removeinds = obj.removeNodes(removeinds+obj.grpreloffset,varargin{:})-obj.grpreloffset;
end

function removeinds = removeRelationships(obj,removeinds,varargin)
    removeinds = obj.removeNodes(removeinds+obj.elmreloffset,varargin{:})-obj.elmreloffset;
end

function removeinds = removeNodes(obj,removeinds,removeorphaned)
    
    if nargin < 3 || isempty(removeorphaned)
        removeorphaned = true;
    end
    
    if removeorphaned
        removeinds = [removeinds,obj.ancestors(removeinds)];
    end
    
    anodeinds = obj.ancestors(removeinds);
    anodemask = false(1,numel(obj.nodes));
    anodemask(anodeinds) = true;
    
    hanodeinds = obj.hierarchyAncestors(removeinds);
    hanodeinds = unique([hanodeinds,obj.ancestors(hanodeinds)]);
    hanodemask = false(1,numel(obj.nodes));
    hanodemask(hanodeinds) = true;
    
    obj.adjacency(removeinds,:) = [];
    obj.adjacency(:,removeinds) = [];
    obj.groups(removeinds,:) = [];
    obj.groups(:,not(any(obj.groups,1))) = []; % remove groups that are now empty
    obj.instgroups(removeinds,:) = [];
    obj.instgroups(:,not(any(obj.instgroups,1))) = []; % remove instance groups that are now empty
    obj.level(:,removeinds) = [];
    
    obj.hierarchy(removeinds,:) = [];
    obj.hierarchy(:,removeinds) = [];
    
    obj.hlevel(:,removeinds) = [];
    
    anodemask(removeinds) = [];
    hanodemask(removeinds) = [];
    
    % recompute level of ancestors of the removed indices
    anodeinds = find(anodemask);
    for i=1:numel(anodeinds)
        childnodeinds = find(obj.adjacency(anodeinds(i),:));
        if isempty(childnodeinds)
            obj.level(anodeinds(i)) = 0;
        else
            obj.level(anodeinds(i)) = max(obj.level(childnodeinds)) + 1;
        end
    end
    
    % recompute level of hierarchy ancestors and their (graph-) ancestors
    % of the removed indices
    % (relationships get the highest hlevel of their child nodes)
    hanodeinds = find(hanodemask);
    for i=1:numel(hanodeinds)
        childnodeinds = find(obj.adjacency(hanodeinds(i),:));
        hchildnodeinds = find(obj.hierarchy(hanodeinds(i),:));
        if not(isempty(hchildnodeinds))
            obj.hlevel(hanodeinds(i)) = max(obj.hlevel(hchildnodeinds)) + 1;
        elseif not(isempty(childnodeinds))
            obj.hlevel(hanodeinds(i)) = max(obj.hlevel(childnodeinds));
        else
            obj.hlevel(hanodeinds(i)) = 0;
        end
    end
    
    obj.grprels(removeinds(removeinds > obj.grpreloffset)-obj.grpreloffset) = [];
    obj.grpnodes(removeinds(removeinds > obj.grpnodeoffset & removeinds <= obj.grpreloffset)-obj.grpnodeoffset) = [];
    obj.metarels(removeinds(removeinds > obj.metareloffset & removeinds <= obj.grpnodeoffset)-obj.metareloffset) = [];
    obj.elmrels(removeinds(removeinds > obj.elmreloffset & removeinds <= obj.metareloffset)-obj.elmreloffset) = [];
    removeelminds = removeinds(removeinds <= obj.elmreloffset);
    obj.elms(removeelminds) = [];
    
    obj.fixedposeinds(:,removeinds) = [];
    
    obj.dirty;
end

function removeOrphanedRelationships(obj)
    
    relinds = numel(obj.elms)+1:size(obj.adjacency,1);
    orphanedrelinds = find(sum(abs(obj.adjacency(relinds,:)),2)~=2)';
    
    obj.removeRelationships(orphanedrelinds,true);
end

% closer to elements (less meta)
function cinds = childs(obj,nodeinds,varargin)
    cinds = PatternGraph.childs_s(obj.adjacency,nodeinds,varargin{:});
end

% farther from elements (more meta)
function pinds = parents(obj,nodeinds,varargin)
    pinds = PatternGraph.parents_s(obj.adjacency,nodeinds,varargin{:});
end

function sinds = siblings(obj,nodeinds,varargin)
    sinds = PatternGraph.siblings_s(obj.adjacency,nodeinds,varargin{:});
end

% closer to elements (less meta)
function dinds = descendants(obj,nodeinds,varargin)
    dinds = PatternGraph.descendants_s(obj.adjacency,nodeinds,varargin{:});
end

% farther from elements (more meta)
function ainds = ancestors(obj,nodeinds,varargin)
    ainds = PatternGraph.ancestors_s(obj.adjacency,nodeinds,varargin{:});
end

function [nodeinds,groupinds] = groupmembers(obj,nodeinds,groupinds)
    if nargin < 3 || isempty(groupinds)
        groupinds = 1:size(obj.groups,2);
    end
    
    groupinds = groupinds(any(obj.groups(nodeinds,groupinds),1));
    nodeinds = find(any(obj.groups(:,groupinds),2));
end

% % adjacency also including siblings
% function extendedAdjacency(obj)
%     eadj = obj.adjacency;
%     nodesib = obj.siblings(1:numel(obj.nodes));
%     for i=1:numel(obj.nodes)
%         eadj(nodesib{i})
%     end
% end

function gparents = groupParents(obj,grpinds,gsubsetinds)
    
    if nargin < 3
        gsubsetinds = 1:size(obj.groups,2);
    end
    
    nodeinds = find(any(obj.groups(:,grpinds),2));

    nparents = obj.parents(nodeinds); %#ok<FNDSB>
    
%         gparents{i} = unique([nparents{logical(grpsubset(:,i))}]);
%         gparents{i} = find(any(obj.groups([nparents{logical(grpsubset(:,i))}],gsubsetinds),2));
    gparents = find(any(obj.groups(nparents,gsubsetinds),2));
    
end

function gchilds = groupChilds(obj,grpinds,gsubsetinds)
    
    if nargin < 3
        gsubsetinds = 1:size(obj.groups,2);
    end
    
    nodeinds = find(any(obj.groups(:,grpinds),2));
    
    nchilds = obj.childs(nodeinds); %#ok<FNDSB>
    
%         gchilds{i} = unique([nchilds{logical(grpsubset(:,i))}]);
%         gchilds{i} = find(any(obj.groups([nchilds{logical(grpsubset(:,i))}],gsubsetinds),2));
    gchilds = find(any(obj.groups(nchilds,gsubsetinds),1));
end

function gsiblings = groupSiblings(obj,grpinds,gsubsetinds)
    
    if nargin < 3
        gsubsetinds = 1:size(obj.groups,2);
    end
    
    nodeinds = find(any(obj.groups(:,grpinds),2));
    
    nsiblings = obj.siblings(nodeinds); %#ok<FNDSB>
    
%         gsiblings{i} = unique([nsiblings{grpsubset(:,i)}]);
%         gsiblings{i} = find(any(obj.groups([nsiblings{logical(grpsubset(:,i))}],gsubsetinds),2));
    gsiblings = find(any(obj.groups(nsiblings,gsubsetinds),2));
    
end

function b = balancedchildlevels(obj)
    
    for i=1:size(obj.adjacency(1))
        childinds = find(obj.adjacency(i,:));
        if not(isempty(childinds)) && not(all(obj.level(childinds) == obj.level(childinds(1))))
            b = false;
            return;
        end
    end
    
    b = true;
end

function createImplicitRelgroups(obj)
    
    synctolerance = 0.1;
    
    allrels = obj.rels(1:numel(obj.rels));
    for i=1:size(obj.groups,2)
        nodeinds = find(obj.groups(:,i));
        if numel(nodeinds) > 1 && all(nodeinds > numel(obj.elms))
            
            relinds = nodeinds - numel(obj.elms);
            relvals = [allrels(relinds).value];
            relvars = [allrels(relinds).variance];
            
            if not(all(strcmp({allrels(relinds).type},allrels(relinds(1)).type)))
                error('Not all relationships in a group are of the same type');
            end
            
            if allrels(relinds(1)).iscircular
                grprelval = circ_mean(relvals,[],2);
            else
                grprelval = mean(relvals);
            end
            grprelvar = mean(sqrt(relvars))^2; % average of the std. deviations
            
            % add dummy group node
            grpnodeind = obj.addGrpnodes(struct(...
                'value',grprelval,...
                'variance',grprelvar));
            grpnodeind = grpnodeind + obj.grpnodeoffset;
            
            % add reldiff=0 relationships between all group members and the group node
            for j=1:numel(nodeinds)
                if allrels(relinds(j)).iscircular
                    rel = RelvalRelationship.create('relDirdiff',relvals(j),grprelval);
                else
                    rel = RelvalRelationship.create('relDiff',relvals(j),grprelval);
                end
                grpmemberresidual = rel.value^2 / rel.variance;
                if grpmemberresidual > synctolerance
                    error('Not all group members have similar values.');
                end
                obj.addGrprels(rel,nodeinds(j),grpnodeind);
            end
        end
    end
    
    error('Need to re-implement.');
end

function removeImplicitRelgroups(obj)
    % also removes all nodes dependent on the group nodes
%     grpnodeoffset = numel(obj.elms)+numel(obj.elmrels)+numel(obj.metarels);
%     obj.removeNodes(grpnodeoffset+1:grpnodeoffset+numel(obj.grpnodes))
    obj.removeGrpnodes(1:numel(obj.grpnodes));
end

% function val = checkConsistency(obj)
% 
%     val = true;
%     
%     % no outgoing edges for element vertices
%     val = val && nnz(obj.adjacency(1:numel(obj.elms),:)) == 0;
%     
%     % exactly two outgoing edges for elmrel vertices, one source and one
%     % target edge, both to element vertices
%     elmreloffset = numel(obj.elms);
%     [indi,srcind] = find(obj.adjacency(elmreloffset+1:elmreloffset+numel(obj.elmrels),:) < 0);
%     val =  val && all(indi == 1:numel(obj.elmrels));
%     [indi,tgtind] = find(obj.adjacency(elmreloffset+1:elmreloffset+numel(obj.elmrels),:) > 0);
%     val = val && all(indi == 1:numel(obj.elmrels));
%     val = val && all(srcind <= numel(obj.elms)) && all(tgtind <= numel(obj.elms));
%     
%     % exactly two outgoing edges for metarel vertices, one source and one
%     % target edge, both to elmrel vertices or both to metarel vertices
%     metareloffset = numel(obj.elms) + numel(obj.elmrels);
%     [indi,srcind] = find(obj.adjacency(metareloffset+1:metareloffset+numel(obj.metarels),:) < 0);
%     val =  val && all(indi == 1:numel(obj.metarels));
%     [indi,tgtind] = find(obj.adjacency(metareloffset+1:metareloffset+numel(obj.metarels),:) > 0);
%     val =  val && all(indi == 1:numel(obj.metarels));
%     val = val && all(...
%         (srcind > elmreloffset & srcind <= metareloffset & ...
%          tgtind > elmreloffset & tgtind <= metareloffset) | ...
%         (srcind > metareloffset & srcind <= metareloffset+numel(obj.metarels) & ...
%          tgtind > metareloffset & tgtind <= metareloffset+numel(obj.metarels)));
%     
% 	% graph needs to be acyclic (need to check because of metarels)
%     val = val && graphisdag(obj.adjacency);
% end

function recomputeRelationships(obj,activerelinds)
    if nargin < 2 || isempty(activerelinds)
        activerelinds = 1 : size(obj.adjacency,1)-numel(obj.elms);
    end
        
    nnodes = numel(obj.nodes);
    
    n2relinds = obj.node2relinds;
    eoffset = obj.elmoffset;

    elements = obj.elms;   
    relations = obj.rels;    
    ereloffset = obj.elmreloffset;
    mreloffset = obj.metareloffset;
    
    % also queue ancestors that have not been computed yet
    descendantinds = obj.descendants(obj.rel2nodeinds(activerelinds),obj.rel2nodeinds(not(relations.hasValue)));
    queuedmask = false(1,nnodes);
    queuedmask([obj.rel2nodeinds(activerelinds),descendantinds]) = true;
    
    srcind = zeros(1,nnodes);
    tgtind = zeros(1,nnodes);
    srcind(obj.rel2nodeinds) = obj.relsrcind;
    tgtind(obj.rel2nodeinds) = obj.reltgtind;

    elements = num2cell(elements); % for performance (necessary?)
    relations = num2cell(relations); % for performance (necessary?)

    % recompute all queued relationships in the correct order
    while(any(queuedmask))
        nodemask = srcind > 0 & tgtind > 0; % has source and target (not an element)
        nodemask(nodemask) = ...
            queuedmask(nodemask) & ...
            not(queuedmask(srcind(nodemask))) & ...
            not(queuedmask(tgtind(nodemask)));

        if not(any(nodemask))
            error('The pattern graph may have cycles or disconnected components.');
        end

        nodeinds = find(nodemask);
        for i=1:numel(nodeinds)
            if nodeinds(i) > mreloffset
                relations{n2relinds(nodeinds(i))}.compute(...
                    relations{n2relinds(srcind(nodeinds(i)))}.value,...
                    relations{n2relinds(srcind(nodeinds(i)))}.variance,...
                    relations{n2relinds(tgtind(nodeinds(i)))}.value,...
                    relations{n2relinds(tgtind(nodeinds(i)))}.variance,...
                    true,true,false);
            elseif nodeinds(i) > ereloffset
                relations{n2relinds(nodeinds(i))}.compute(...
                    elements{srcind(nodeinds(i))-eoffset},...
                    elements{tgtind(nodeinds(i))-eoffset},...
                    false,... % userelvar
                    true,true,false);
            else
                error('An element node should not have child nodes.')
            end
        end

        queuedmask(nodemask) = false;
    end
end

end

methods(Static)
    
function cinds = childs_s(adjacency,nodeinds,subsetinds)
    nodemask = logical(sparse(ones(1,numel(nodeinds)),nodeinds,ones(1,numel(nodeinds)),1,size(adjacency,1)));
    
    if nargin >= 3
        subsetmask = logical(sparse(ones(1,numel(subsetinds)),subsetinds,ones(1,numel(subsetinds)),1,size(adjacency,1)));
        hassubsetmask = true;
    else
        hassubsetmask = false;
    end
    
    cmask = logical(nodemask*abs(adjacency));

    if hassubsetmask
        cmask = cmask & subsetmask;
    end
    
    cinds = find(cmask);
end

function pinds = parents_s(adjacency,nodeinds,subsetinds)
    nodemask = logical(sparse(ones(1,numel(nodeinds)),nodeinds,ones(1,numel(nodeinds)),1,size(adjacency,1)))';
    
    if nargin >= 3
        subsetmask = logical(sparse(ones(1,numel(subsetinds)),subsetinds,ones(1,numel(subsetinds)),1,size(adjacency,1)));
        hassubsetmask = true;
    else
        hassubsetmask = false;
    end
    
    pmask = logical(abs(adjacency)*nodemask);

    if hassubsetmask
        pmask = pmask & subsetmask;
    end
    
    pinds = find(pmask);
end

function sinds = siblings_s(adjacency,nodeinds,varargin)
    pinds = PatternGraph.parents_s(adjacency,nodeinds,varargin{:});
    sinds = PatternGraph.childs_s(adjacency,pinds,varargin{:});
end
    
function dinds = descendants_s(adjacency,nodeinds,subsetinds)
    nodemask = logical(sparse(ones(1,numel(nodeinds)),nodeinds,ones(1,numel(nodeinds)),1,size(adjacency,1)));
    dmask = logical(sparse(1,size(adjacency,1)));
    
    if nargin >= 3
        subsetmask = logical(sparse(ones(1,numel(subsetinds)),subsetinds,ones(1,numel(subsetinds)),1,size(adjacency,1)));
        hassubsetmask = true;
    else
        hassubsetmask = false;
    end
    
    while any(nodemask)
        nodemask = logical(nodemask*abs(adjacency));
        
        if hassubsetmask
            nodemask = nodemask & subsetmask;
        end
        
        nodemask(dmask) = false; % has already been visited
        
        dmask = dmask | nodemask;
    end
    
    dinds = find(dmask);
end

function ainds = ancestors_s(adjacency,nodeinds,subsetinds)
    nodemask = logical(sparse(ones(1,numel(nodeinds)),nodeinds,ones(1,numel(nodeinds)),1,size(adjacency,1)))';
    amask = logical(sparse(1,size(adjacency,1)))';
    
    if nargin >= 3
        subsetmask = logical(sparse(ones(1,numel(subsetinds)),subsetinds,ones(1,numel(subsetinds)),1,size(adjacency,1)));
        hassubsetmask = true;
    else
        hassubsetmask = false;
    end
    
    while any(nodemask)
        nodemask = logical(abs(adjacency)*nodemask);
        
        if hassubsetmask
            nodemask = nodemask & subsetmask;
        end
        
        nodemask(amask) = false; % has already been visited
        
        amask = amask | nodemask;
    end
    
    ainds = find(amask');
end

function val = elmrelsrcind_s(adjacency,nelms,nelmrels)
    offset = nelms;
    [ind,val] = find(adjacency(offset+1:offset+nelmrels,:) < 0);
    val(ind) = val;
    val = val';
end

function val = elmreltgtind_s(adjacency,nelms,nelmrels)
    offset = nelms;
    [ind,val] = find(adjacency(offset+1:offset+nelmrels,:) > 0);
    val(ind) = val;
    val = val';
end

function val = metarelsrcind_s(adjacency,nelms,nelmrels,nmetarels)
    offset = nelms + nelmrels;
    [ind,val] = find(adjacency(offset+1:offset+nmetarels,:) < 0);
    val(ind') = val;
    val = val';
end

function val = metareltgtind_s(adjacency,nelms,nelmrels,nmetarels)
    offset = nelms + nelmrels;
    [ind,val] = find(adjacency(offset+1:offset+nmetarels,:) > 0);
    val(ind') = val;
    val = val';
end
   
function val = grprelsrcind_s(adjacency,nelms,nelmrels,nmetarels,ngrpnodes,ngrprels)
    offset = nelms + nelmrels + nmetarels + ngrpnodes;
    [ind,val] = find(adjacency(offset+1:offset+ngrprels,:) < 0);
    val(ind') = val;
    val = val';
end

function val = grpreltgtind_s(adjacency,nelms,nelmrels,nmetarels)
    offset = nelms + nelmrels + nmetarels + ngrpnodes;
    [ind,val] = find(adjacency(offset+1:offset+ngrprels,:) > 0);
    val(ind') = val;
    val = val';
end

% todo: implement PatternImporter/Exporter that reads/writes augmented svg files
% for sparse matrices, store nonzero indices (i,j) and values (i,j and v in
% each in an attribute that contains a list of all values, check that lists have same length)
function obj = loadobj(obj)
    if isstruct(obj)
        error('Saved version of the pattern graph seems to be too outdated.');
%         objstruct = obj;
%         obj = PatternGraph;
%         obj.adjacency = objstruct.adjacency;
%         obj.elms = objstruct.elms; % elements are not owned (they are owned by SceneGroup)
%         obj.elmrels = objstruct.elmrels;
%         obj.metarels = objstruct.metarels;
%         obj.level = objstruct.level;
%         obj.groups = objstruct.groups;
    end
    
    needrecompute = false;
    wasupdated = false;
    
    if not(isa(obj.elms,'ScenePolygonset'))
        if isa(obj.elms,'ScenePolygon')
            % legacy: convert from polygons to polygonset
            for i=1:numel(obj.elms)
                oldelm = obj.elms(i);
                obj.elms(i) = ScenePolygonset(obj.elms(i));
                obj.elms(i).defineSize([obj.elms(i).radius;obj.elms(i).radius]); % so it is uniform (otherwise information gets lost when transforming to uniform scale pose in solver)
                delete(oldelm(isvalid(oldelm)));
            end
            needrecompute = true;
            wasupdated = true;
        else
            error('Invalid type of scene elements.');
        end
    end
    
    for i=1:numel(obj.elms)
        elmsize = obj.elms(i).size;
        if elmsize(1) ~= elmsize(2);
            elmrad = obj.elms(i).radius;
            obj.elms(i).defineSize([elmrad;elmrad]);
            needrecompute = true;
            wasupdated = true;
        end
    end
    
    
    if numel(obj.level) ~= size(obj.adjacency,1);
        % compute level of all nodes
        obj.level = zeros(1,size(obj.adjacency,1));
        nodeinds = 1:numel(obj.elms);
        nodemask = logical(sparse(ones(1,numel(nodeinds)),nodeinds,ones(1,numel(nodeinds)),1,size(obj.adjacency,1)))';
        amask = logical(sparse(1,size(obj.adjacency,1)))';

        lvl = 0;
        while any(nodemask)
            nodemask = logical(abs(obj.adjacency)*nodemask);
            lvl = lvl+1;

            nodemask(amask) = false;

            obj.level(nodemask) = lvl;

            amask = amask | nodemask;
        end
        
        wasupdated = true;
    end
    
    if size(obj.fixedposeinds,2) ~= numel(obj.nodes)
        obj.fixedposeinds = sparse(6,numel(obj.nodes));
        wasupdated = true;
    end
    
    if any(size(obj.hierarchy) ~= [numel(obj.nodes),numel(obj.nodes)])
        obj.hierarchy = sparse(numel(obj.nodes),numel(obj.nodes));
        wasupdated = true;
    end
    
    if numel(obj.hlevel) ~= numel(obj.nodes)
        obj.hlevel = zeros(1,numel(obj.nodes));
        wasupdated = true;
    end
    
    if size(obj.instgroups,1) ~= numel(obj.nodes)
        obj.instgroups = sparse(numel(obj.nodes),0);
        wasupdated = true;
    end
    
    obj.dirty; % otherwise nodes might get loaded twice, with different (cloned) objects in the different p_nodes, p_rels, etc.
    
    if needrecompute
        obj.recomputeRelationships;
    end
    
    if wasupdated
        warning('PatternGraph has been updated from older version, recommend re-saving.');
    end
end

end

methods
    
% should be non-static
function obj = saveobj(obj)
    obj.dirty; % otherwise nodes might get loaded twice, with different (cloned) objects in the different p_nodes, p_rels, etc.
end

end

end
