function transform = readsvgtransform(xmlnode)
    transform = eye(3);
    if xmlnode.hasAttribute('transform')
        transformSequenceStr = char(xmlnode.getAttribute('transform'));
        if not(isempty(transformSequenceStr))
            transformStr = regexp(transformSequenceStr, ...
                '[\d\w]*([^\)]*\)','match');
            for i=1:numel(transformStr)
                transformType = regexp(transformStr{i},'[\d\w]*(','match');
                transformType = transformType{1}(1:end-1);
                tElms = regexp(transformStr{i},'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?','match');    
                tElms = str2double(tElms);
                t = eye(3);
                if strcmp(transformType,'matrix')
                    t([1,2,4,5,7,8]) = tElms;
                elseif strcmp(transformType,'translate')
                    if numel(tElms) == 1
                        t(7) = tElms;
                    else
                        t([7,8]) = tElms;
                    end
                elseif strcmp(transformType,'scale')
                    if numel(tElms) == 1
                        t(1) = tElms;
                    else
                        t([1,5]) = tElms;
                    end
                    transform = transform * t;
                elseif strcmp(transformType,'rotate')
                    tElms(1) = tElms(1) * (2*pi/360);
                    t([1,2,4,5]) = [...
                        cos(tElms(1)),sin(tElms(1)),...
                        -sin(tElms(1)),cos(tElms(1))];
                    if numel(tElms) > 1
                        tpre = eye(3);
                        tpost = eye(3);
                        tpre([7,8]) = -tElms([2,3]); % translate to rotation center
                        tpost([7,8]) = tElms([2,3]); % translate back
                        t = tpost * t * tpre;
                    end
                elseif strcmp(transformType,'skewX')
                    tElms(1) = tElms(1) * (2*pi/360);
                    t(4) = tan(tElms);
                elseif strcmp(transformType,'skewY')
                    tElms(1) = tElms(1) * (2*pi/360);
                    t(2) = tan(tElms);
                else
                    error('Unkown transfomation type.');
                end
                transform = transform * t;
            end
        end
    end
end
