% function graphics2svg(gobj,filename,...)
% function graphics2svg(gobj,svgnode,...)
function graphics2svg(gobj,in2,recursive,includehidden,transform,...
    canvasmin,canvasmax,scale,shift,includetags,excludetags,removeoutsidecanvas)
    if nargin < 3 || isempty(recursive)
        recursive = true;
    end
    
    if nargin < 4 || isempty(includehidden)
        includehidden = false;
    end
    
    if nargin < 5 || isempty(transform)
        transform = eye(4);
    end
    
    if nargin < 8 || isempty(scale)
        scale = 1;
    end
    
	if nargin < 9 || isempty(shift)
        shift = [0;0];
	end
    
    if nargin < 10
        includetags = {};
    end
    
    if nargin < 11
        excludetags = {};
    end
    
    if nargin < 11 || isempty(removeoutsidecanvas)
        removeoutsidecanvas = false;
    end
    
    if not(isgraphics(gobj))
        error('Invalid graphics handle.');
    end
    
    if nargin < 6 || isempty(canvasmin) || isempty(canvasmax)
        [canvasmin,canvasmax] = boundingbox(gobj);
    end
    canvasmin = (canvasmin+shift).*scale;
    canvasmax = (canvasmax+shift).*scale;
    
    filename = '';
    if ischar(in2)
        % create svg file
        filename = in2;
        
        doc = com.mathworks.xml.XMLUtils.createDocument('svg');
        doc.setDocumentURI(['file:/',filename]);

        parentnode = doc.getDocumentElement;
        parentnode.setAttribute('version','1.1');
        parentnode.setAttribute('xmlns','http://www.w3.org/2000/svg');
        parentnode.setAttribute('xmlns:xlink','http://www.w3.org/1999/xlink');
        parentnode.setAttribute('x',[num2str(canvasmin(1),10),'px']);
        parentnode.setAttribute('y',[num2str(canvasmin(2),10),'px']);
        parentnode.setAttribute('width',[num2str(canvasmax(1) - canvasmin(1),10),'px']);
        parentnode.setAttribute('height',[num2str(canvasmax(2) - canvasmin(2),10),'px']);
        parentnode.setAttribute('viewBox',...
            [num2str(canvasmin(1),10),' ',num2str(canvasmin(2),10),' ',...
            num2str(canvasmax(1) - canvasmin(1),10),' ',...
            num2str(canvasmax(2) - canvasmin(2),10)]);
    else
        parentnode = in2;
    end
    
    type = get(gobj,'Type');
    
    if strcmp(type,'axes')
        % do nothing (axes labels are hidden children of the axes)
    elseif strcmp(type,'hggroup')
        % do nothing (svg group has already been created)
    elseif strcmp(type,'hgtransform')
        % do nothing (todo: transform polylines)
        transform = transform * gobj.Matrix;
    elseif strcmp(type,'patch')
        patch2svg(gobj,parentnode,transform,canvasmin,canvasmax,scale,shift,removeoutsidecanvas);
    elseif strcmp(type,'line')
        line2svg(gobj,parentnode,transform,canvasmin,canvasmax,scale,shift,removeoutsidecanvas);
    elseif strcmp(type,'image') || strcmp(type,'surface')
        image2svg(gobj,parentnode,transform,canvasmin,canvasmax,scale,shift,removeoutsidecanvas);
    else
        warning(['Unsupported graphics type ''',type,''', skipping']);
    end

    if recursive
        if includehidden
            childs = allchild(gobj);
        else
            childs = get(gobj,'Children');
        end
        
        childs(not(isgraphics(childs))) = [];
        
        childs = childs(end:-1:1); % to get correct z-ordering
        
        % include/exclude childs with given tags
        if not(isempty(includetags)) && not(isempty(childs))
            mask = ismember(get(childs,'Tag'),includetags);
            childs(not(mask)) = [];
        end
        if not(isempty(excludetags)) && not(isempty(childs))
            mask = ismember(get(childs,'Tag'),excludetags);
            childs(mask) = [];
        end
        
        if not(isempty(childs))
            if isempty(filename) % not for the root
                % group childs
                childgroupnode = parentnode.getOwnerDocument.createElement('g');
            else
                childgroupnode = parentnode;
            end
            canvasminorig = canvasmin./scale - shift;
            canvasmaxorig = canvasmax./scale - shift;
            for i=1:numel(childs)

                graphics2svg(...
                    childs(i),childgroupnode,...
                    recursive,includehidden,transform,...
                    canvasminorig,canvasmaxorig,scale,shift,...
                    includetags,excludetags,removeoutsidecanvas);
            end
            if isempty(filename)
                parentnode.appendChild(childgroupnode);
            end
        end
    end
    
    if not(isempty(filename))
        xmlwrite(filename,doc);
    end
end

function patch2svg(gobj,parentnode,transform,canvasmin,canvasmax,scale,shift,removeoutsidecanvas)
    faces = get(gobj,'Faces')';
    
    if isempty(faces)
        return;
    end
    
    vertices = gobj.Vertices';
    if size(vertices,1) == 2
        vertices = [vertices;zeros(1,size(vertices,2))];
    end
    vertices = transform*[vertices;ones(1,size(vertices,2))];
    vertices = bsxfun(@rdivide,vertices(1:2,:),vertices(4,:)); % homogenize and only take xy coordinates
    vertices = vertices(1:2,:);
    cdata = get(gobj,'FaceVertexCData');
    linewidth = get(gobj,'LineWidth');
    edgecolormode = get(gobj,'EdgeColor');
    facecolormode = get(gobj,'FaceColor');
    
    % get edges
    edges = cell2mat(cellfun(@(x) [x(:),x([2:end,1])]',...
        mat2cell(faces,size(faces,1),ones(1,size(faces,2))),'UniformOutput',false));
    edgefaces = cell2mat(cellfun(@(x,y) ones(1,numel(x)).*y,...
        mat2cell(faces,size(faces,1),ones(1,size(faces,2))),...
        num2cell(1:size(faces,2)),'UniformOutput',false));
    nanedges = isnan(vertices(1,edges(1,:))) | isnan(vertices(1,edges(2,:)));
    nanfaceinds = unique(edgefaces(nanedges));
    nanfaces = false(1,size(faces,2));
    nanfaces(nanfaceinds) = true;
%     edges(:,nanedges) = [];
%     edgefaces(nanedges) = [];
    
    [edgefaces_s,perm] = sort(edgefaces,'ascend');
    intervals = find(diff(edgefaces_s));
    intervals = [1,intervals+1;intervals,size(edges,2)];
    faceedges = cell(1,size(faces,2));
    for i=1:size(faces,2)
        faceedges{i} = perm(intervals(1,i):intervals(2,i));
    end
    
    % convert cdata to rgb
    if isempty(cdata)
        % use black if no color is given
        cdata = [0,0,0];
    elseif size(cdata,2) == 1
        cdatamapping = get(gobj,'CDataMapping');
        fig = ancestor(gobj,'figure');
        cmap = get(fig,'Colormap');
        if strcmp(cdatamapping,'scaled')
            axes = ancestor(gobj,'axes');
            cdata = (cdata-min(get(axes,'CLim')))./...
                (max(get(axes,'CLim'))-min(get(axes,'CLim'))); % scale to [0,1]
            cdata = min(1,max(0,cdata));
            cdata = cdata.*(size(cmap,1)-1) + 1; % scale to [1,nc]
        end
        cdata = ind2rgb(round(cdata),cmap);
        cdata = reshape(cdata,[],3);
    elseif size(cdata,2) ~= 3
        error('Invalid format for patch CData.');
    end
    
    
    % get one color for each edge
    edgecolor = zeros(3,size(edges,2));
    if not(ischar(edgecolormode))
        edgecolor = edgecolormode(ones(1,size(edges,2)),:)';
        edgemode = repmat({'polygon'},1,size(edges,2));
    elseif strcmp(edgecolormode,'none')
        edgecolor = zeros(3,size(edges,2));
        edgemode = repmat({'none'},1,size(edges,2));
    elseif strcmp(edgecolormode,'flat') || strcmp(edgecolormode,'interp')
        
        if strcmp(edgecolormode,'interp')
            warning('Interpolated colors not yet implemented for patch edges, using flat coloring instead.');
        end
        
        if size(cdata,1) == 1
            edgecolor(1,:) = cdata(1,1);
            edgecolor(2,:) = cdata(1,2);
            edgecolor(3,:) = cdata(1,3);
            edgemode = repmat({'polygon'},1,size(edges,2));
        elseif size(cdata,1) == size(faces,2)
            edgecolor(1,:) = cdata(edgefaces,1)';
            edgecolor(2,:) = cdata(edgefaces,1)';
            edgecolor(3,:) = cdata(edgefaces,1)';
            edgemode = repmat({'polygon'},1,size(edges,2));
        elseif size(cdata,1) == size(vertices,2)
            edgecolor(1,:) = cdata(edges(1,:),1)';
            edgecolor(2,:) = cdata(edges(1,:),2)';
            edgecolor(3,:) = cdata(edges(1,:),3)';
            edgemode = repmat({'line'},1,size(edges,2));
        else
            error('Invalid FaceVertexCData format for patch.')
        end
    end
    
    % get one color for each face
    facecolor = zeros(3,size(faces,2));
    if not(ischar(facecolormode))
        facecolor = facecolormode(ones(1,size(faces,2)),:)';
        facemode = repmat({'polygon'},1,size(faces,2));
    elseif strcmp(facecolormode,'none')
        facecolor = zeros(3,size(faces,2));
        facemode = repmat({'none'},1,size(faces,2));
    elseif strcmp(facecolormode,'flat') || strcmp(facecolormode,'interp')
        if strcmp(facecolormode,'interp')
            warning('Interpolated colors not yet implemented for patch faces, using flat coloring instead.');
        end
        
        if size(cdata,1) == 1
            facecolor(1,:) = cdata(1,1);
            facecolor(2,:) = cdata(1,2);
            facecolor(3,:) = cdata(1,3);
            facemode = repmat({'polygon'},1,size(faces,2));
        elseif size(cdata,1) == size(faces,2)
            facecolor(1,:) = cdata(:,1)';
            facecolor(2,:) = cdata(:,2)';
            facecolor(3,:) = cdata(:,3)';
            facemode = repmat({'polygon'},1,size(faces,2));
        elseif size(cdata,1) == size(vertices,2)
            facecolor(1,:) = cdata(faces(1,:),1)';
            facecolor(2,:) = cdata(faces(1,:),2)';
            facecolor(3,:) = cdata(faces(1,:),3)';
            facemode = repmat({'polygon'},1,size(faces,2));
        else
            error('Invalid FaceVertexCData format for patch.')
        end
    end
    
    facemode(nanfaces) = repmat({'none'},1,sum(nanfaces));
    mask = nanfaces(edgefaces) & strcmp(edgemode,'polygon');
    edgemode(mask) = repmat({'polyline'},1,sum(mask));
    
%     % create group for svg objects representing the patch (if more than one)    
%     if numel(svgtypes) > 1
%         patchgroupnode = parentnode.getOwnerDocument.createElement('g');
%     else
%         patchgroupnode = parentnode;
%     end
    
	svgtypes = {};
    svgcoords = {};
    svgattr = {};
    
    if size(faces,2) > 1
        facegroupnode = parentnode.getOwnerDocument.createElement('g');
        parentnode.appendChild(facegroupnode);
    else
        facegroupnode = parentnode;
    end
    
    % create svg geometry
    markervertinds = [];
    for i=1:size(faces,2)
        vertinds = faces(:,i);
        vertinds(isnan(vertinds)) = [];
        faceverts = vertices(:,vertinds);
        
        canvasminorig = canvasmin./scale - shift;
        canvasmaxorig = canvasmax./scale - shift;
        if removeoutsidecanvas && ...
           (any(faceverts(1,:) < canvasminorig(1)) || any(faceverts(2,:) < canvasminorig(2)) || ...
            any(faceverts(1,:) > canvasmaxorig(1)) || any(faceverts(2,:) > canvasmaxorig(2)))
            % remove if any part of the face is outside the canvas 
            continue;
        end
        
        markervertinds = [markervertinds,vertinds]; %#ok<AGROW>
        
        if strcmp(facemode{i},'polygon') || strcmp(edgemode{i},'polygon')
            
            % create svg polygon
            svgtypes{end+1} = 'polygon'; %#ok<AGROW>
            svgcoords{end+1} = swapy(...
                bsxfun(@plus,faceverts,shift).*scale,canvasmin,canvasmax); %#ok<AGROW>
            svgattr{end+1} = containers.Map('KeyType','char','ValueType','char'); %#ok<AGROW>
            
            if strcmp(facemode{i},'polygon')
                svgattr{end}('fill') = ['rgb(',...
                    num2str(facecolor(1,i)*100),'%,',...
                    num2str(facecolor(2,i)*100),'%,',...
                    num2str(facecolor(3,i)*100),'%)'];
            elseif strcmp(facemode{i},'none')
                svgattr{end}('fill') = 'none';
            end
            
            if strcmp(edgemode{i},'polygon')
                firstedge = faceedges{i}(1);
                svgattr{end}('stroke') = ['rgb(',...
                    num2str(edgecolor(1,firstedge)*100),'%,',...
                    num2str(edgecolor(2,firstedge)*100),'%,',...
                    num2str(edgecolor(3,firstedge)*100),'%)'];
                svgattr{end}('stroke-width') = [num2str(linewidth.*0.1),'%'];
            elseif strcmp(edgemode{i},'none') 
                svgattr{end}('stroke') = 'none';
            end
        end
        
        if strcmp(edgemode{i},'polyline')
            
            % create svg polylines
            [vertsx,vertsy] = polysplit(faceverts(1,:),faceverts(2,:));
            for j=1:numel(vertsx)
                firstedge = faceedges{i}(1);
                svgtypes{end+1} = 'polyline'; %#ok<AGROW>
                svgcoords{end+1} = swapy(...
                    bsxfun(@plus,[vertsx{j};vertsy{j}],shift).*scale,canvasmin,canvasmax); %#ok<AGROW>
                svgattr{end+1} = containers.Map('KeyType','char','ValueType','char'); %#ok<AGROW>
                svgattr{end}('fill') = 'none';
                svgattr{end}('stroke') = ['rgb(',...
                    num2str(edgecolor(1,firstedge)*100),'%,',...
                    num2str(edgecolor(2,firstedge)*100),'%,',...
                    num2str(edgecolor(3,firstedge)*100),'%)'];
                svgattr{end}('stroke-width') = [num2str(linewidth.*0.1),'%'];
            end
            
        elseif strcmp(edgemode{i},'line')
            
            % create svg lines
%             segmentedges = polysplit(faceedges{i},faceedges{i});
            segmentedges = faceedges{i};
            segmentedges(nanedges) = nan;
            segmentedges = polysplit(segmentedges,segmentedges);
            
            if numel(segmentedges) > 1
                segmentgroupnode = parentnode.getOwnerDocument.createElement('g');
                facegroupnode.appendChild(segmentgroupnode);
            else
                segmentgroupnode = facegroupnode;    
            end
            
            for k=1:numel(segmentedges)
                if numel(segmentedges{k}) > 1
                    edgegroupnode = parentnode.getOwnerDocument.createElement('g');
                    segmentgroupnode.appendChild(edgegroupnode);
                else
                    edgegroupnode = segmentgroupnode;
                end
                segsvgtypes = {};
                segsvgcoords = {};
                segsvgattr = {};
                for j=1:size(segmentedges{k},2)
                    segsvgtypes{end+1} = 'line'; %#ok<AGROW>
                    segsvgcoords{end+1} = swapy(...
                        bsxfun(@plus,vertices(:,edges(1:2,segmentedges{k}(j))'),shift).*scale,canvasmin,canvasmax); %#ok<AGROW>
                    segsvgattr{end+1} = containers.Map('KeyType','char','ValueType','char'); %#ok<AGROW>
                    segsvgattr{end}('fill') = 'none';
                    segsvgattr{end}('stroke') = ['rgb(',...
                        num2str(edgecolor(1,segmentedges{k}(j))*100),'%,',...
                        num2str(edgecolor(2,segmentedges{k}(j))*100),'%,',...
                        num2str(edgecolor(3,segmentedges{k}(j))*100),'%)'];
                    segsvgattr{end}('stroke-width') = [num2str(linewidth.*0.1),'%'];
                end
                writesvggeometry(segsvgtypes,segsvgcoords,segsvgattr,edgegroupnode);
            end
        end
        
    end
    
    writesvggeometry(svgtypes,svgcoords,svgattr,facegroupnode);
    
%     if facegroupnode ~= parentnode
%         parentnode.appendChild(facegroupnode);
%     end
    
    for i=1:numel(svgattr)
        if isvalid(svgattr{i})
            delete(svgattr{i});
        end
    end
    
    markerpositions = vertices(:,markervertinds);
    marker2svg(gobj,markerpositions,parentnode,canvasmin,canvasmax,scale,shift);
end

function line2svg(gobj,parentnode,transform,canvasmin,canvasmax,scale,shift,removeoutsidecanvas)
    if not(strcmp(get(gobj,'LineStyle'),'none'))
        svgtypes = {};
        svgcoords = {};
        svgattr = {};    

        color = get(gobj,'Color');
        linewidth = get(gobj,'LineWidth');
        
        if isempty(gobj.ZData)
            [vertsx,vertsy] = polysplit(gobj.XData,gobj.YData);
            verts = cellfun(@(x,y) [x,y,zeros(numel(x),1)]',vertsx,vertsy,'UniformOutput',false);
        else
            [vertsx,vertsy,vertsz] = polysplit3D(gobj.XData,gobj.YData);
            verts = cellfun(@(x,y,z) [x,y,z]',vertsx,vertsy,vertsz,'UniformOutput',false);
        end
        
        verts = transform*[verts;ones(1,size(verts,2))];
        verts = bsxfun(@rdivide,verts(1:2,:),verts(4,:)); % homogenize and only take xy coordinates
        markerpositions = zeros(2,0);
        for j=1:numel(verts)
            canvasminorig = canvasmin./scale - shift;
            canvasmaxorig = canvasmax./scale - shift;
            if removeoutsidecanvas && ...
               (any(verts{j}(1,:) < canvasminorig(1)) || any(verts{j}(2,:) < canvasminorig(2)) || ...
                any(verts{j}(1,:) > canvasmaxorig(1)) || any(verts{j}(2,:) > canvasmaxorig(2)))
                % remove if any part of the line outside the canvas 
                continue;
            end
            
            svgtypes{end+1} = 'polyline'; %#ok<AGROW>
            svgcoords{end+1} = swapy(...
                bsxfun(@plus,verts{j},shift).*scale,canvasmin,canvasmax); %#ok<AGROW>
            svgattr{end+1} = containers.Map('KeyType','char','ValueType','char'); %#ok<AGROW>
            svgattr{end}('stroke') = ['rgb(',...
                num2str(color(1)*100),'%,',...
                num2str(color(2)*100),'%,',...
                num2str(color(3)*100),'%)'];
            svgattr{end}('fill') = 'none';
            svgattr{end}('stroke-width') = [num2str(linewidth.*0.1),'%'];
            markerpositions = [markerpositions,verts{j}]; %#ok<AGROW>
        end

        % create group for svg objects representing the patch (if more than one)    
        if numel(svgtypes) > 1
            childgroupnode = parentnode.getOwnerDocument.createElement('g');
        else
            childgroupnode = parentnode;
        end

        writesvggeometry(svgtypes,svgcoords,svgattr,childgroupnode);

        if numel(svgtypes) > 1
            parentnode.appendChild(childgroupnode);
        end

        for i=1:numel(svgattr)
            if isvalid(svgattr{i})
                delete(svgattr{i});
            end
        end
    end
    
%     markerpositions = [get(gobj,'XData');get(gobj,'YData')];
    marker2svg(gobj,markerpositions,parentnode,canvasmin,canvasmax,scale,shift);
end

function marker2svg(gobj,positions,parentnode,canvasmin,canvasmax,scale,shift)
    
    type = get(gobj,'Marker');
    edgecolor = get(gobj,'MarkerEdgeColor');
    facecolor = get(gobj,'MarkerFaceColor');    

    if strcmp(type,'none') || (strcmp(edgecolor,'none') && strcmp('facecolor','none'))
        return;
    end
    
    % create geometry for single marker
    % just use filled circles for now
    if strcmp(facecolor,'none')
        facecolor = edgecolor;
    end
    
    canvasdiag = sqrt(sum((canvasmin-canvasmax).^2,1)) / scale;
    
    svgtypes = {};
    svgcoords = {};
    svgattr = {};
    svgtypes{end+1} = 'circle';
    svgcoords{end+1} = [[0;0],[canvasdiag*0.002;0]]; % center and radius
    svgattr{end+1} = containers.Map('KeyType','char','ValueType','char');
    svgattr{end}('stroke') = 'none';
    svgattr{end}('fill') = ['rgb(',...
            num2str(facecolor(1)*100),'%,',...
            num2str(facecolor(2)*100),'%,',...
            num2str(facecolor(3)*100),'%)'];
        
    
    % translate to all vertices
    positions = swapy(bsxfun(@plus,positions,shift).*scale,canvasmin,canvasmax);
    if not(isempty(svgtypes))
        svgtypes = svgtypes(:);
        svgcoords = svgcoords(:);
        svgattr = svgattr(:);
        svgtypes = repmat(svgtypes,1,size(positions,2));
        svgcoords = repmat(svgcoords,1,size(positions,2));
        svgattr = repmat(svgattr,1,size(positions,2));
        % copy attribute maps (are handles)
        for i=2:size(positions,2)
            for j=1:size(svgattr,1)
                svgattr{j,i} = containers.Map(svgattr{j,1}.keys,svgattr{j,1}.values);
            end
        end
        for i=1:size(positions,2)
            transformstr = sprintf('translate(%.30g,%.30g)',positions(:,i));
            for j=1:size(svgattr,1)
                svgattr{j,i}('transform') = transformstr;
            end
        end
        svgtypes = svgtypes(:);
        svgcoords = svgcoords(:);
        svgattr = svgattr(:);
    end
    
    % create group for svg objects representing the patch (if more than one)    
    if numel(svgtypes) > 1
        childgroupnode = parentnode.getOwnerDocument.createElement('g');
    else
        childgroupnode = parentnode;
    end
    
    writesvggeometry(svgtypes,svgcoords,svgattr,childgroupnode);
    
    if numel(svgtypes) > 1
        parentnode.appendChild(childgroupnode);
    end
    
    for i=1:numel(svgattr)
        if isvalid(svgattr{i})
            delete(svgattr{i});
        end
    end
end

function image2svg(gobj,parentnode,transform,canvasmin,canvasmax,scale,shift,removeoutsidecanvas)
    svgimagecoords = {};
    svgimagergba = {};
    svgimageattr = {};    

    [imgmin,imgmax] = boundingbox(gobj);
    
    imgmin = transform * [imgmin;0;1];
    imgmax = transform * [imgmax;0;1];
    imgmin = imgmin(1:2)./imgmin(4);
    imgmax = imgmax(1:2)./imgmax(4);

    canvasminorig = canvasmin./scale - shift;
    canvasmaxorig = canvasmax./scale - shift;
    if removeoutsidecanvas && ...
       (any(imgmin(1) < canvasminorig(1)) || any(imgmin(2) < canvasminorig(2)) || ...
        any(imgmax(1) > canvasmaxorig(1)) || any(imgmax(2) > canvasmaxorig(2)))
        % skip image if it is not completely inside the canvas
        return;
    end
    
    rgbadata = get(gobj,'CData');
    if size(rgbadata,3) == 1
        cdatamapping = get(gobj,'CDataMapping');
        fig = ancestor(gobj,'figure');
        cmap = get(fig,'Colormap');
        if strcmp(cdatamapping,'scaled')
            axes = ancestor(gobj,'axes');
            rgbadata = (rgbadata-min(get(axes,'CLim')))./...
                (max(get(axes,'CLim'))-min(get(axes,'CLim'))); % scale to [0,1]
            rgbadata = min(1,max(0,rgbadata));
            rgbadata = rgbadata.*(size(cmap,1)-1) + 1; % scale to [1,nc]
        end
        rgbadata = ind2rgb(round(rgbadata),cmap);
    elseif size(rgbadata,3) ~= 3
        error('Invalid format for image CData.');
    end

    adata = get(gobj,'AlphaData');
    if numel(adata) == 1
        adata = ones(size(rgbadata,1),size(rgbadata,2),1) .* adata;
    end
    adatamapping = get(gobj,'AlphaDataMapping');
    if strcmp(adatamapping,'direct') || strcmp(adatamapping,'scaled')
        fig = ancestor(gobj,'figure');
        amap = get(fig,'Alphamap');
        if strcmp(adatamapping,'scaled')
            axes = ancestor(gobj,'axes');
            adata = (adata-min(get(axes,'ALim')))./...
                (max(get(axes,'ALim'))-min(get(axes,'ALim'))); % scale to [0,1]
            adata = min(1,max(0,adata));
            adata = adata.*(size(amap,1)-1) + 1; % scale to [1,nc]
        end
        adata = amap(round(adata));
    end
    rgbadata = cat(3,rgbadata,adata);

    svgimagecoords{end+1} = swapy(bsxfun(@plus,[...
        imgmin,...
        [imgmax(1);imgmin(2)],...
        [imgmin(1);imgmax(2)]],shift).*scale,...
        canvasmin,canvasmax);
    svgimagergba{end+1} = rgbadata;
    svgimageattr{end+1} = containers.Map('KeyType','char','ValueType','char');
    
    writesvgimages(svgimagecoords,svgimagergba,svgimageattr,parentnode);
    
    for i=1:numel(svgimageattr)
        if isvalid(svgimageattr{i})
            delete(svgimageattr{i});
        end
    end
end

function [bbmin,bbmax] = boundingbox(gobj)
    type = get(gobj,'Type');    

    if strcmp(type,'axes')
        xlim = get(gobj,'XLim');
        ylim = get(gobj,'YLim');
        bbmin = [xlim(1);ylim(1)];
        bbmax = [xlim(2);ylim(2)];
    elseif strcmp(type,'patch') || strcmp(type,'line')
        xdata = get(gobj,'XData');
        ydata = get(gobj,'YData');
        bbmin = [min(xdata);min(ydata)];
        bbmax = [max(xdata);max(ydata)];
    elseif strcmp(type,'image') || strcmp(type,'surface')
        xdata = get(gobj,'XData');
        ydata = get(gobj,'YData');
        cdata = get(gobj,'CData');
        pixelspacing = [(max(xdata)-min(xdata))/(size(cdata,2)-1);...
                        (max(ydata)-min(ydata))/(size(cdata,1)-1)];
        bbmin = [min(xdata);min(ydata)] - pixelspacing./2;
        bbmax = [max(xdata);max(ydata)] + pixelspacing./2;
    else
        error('Unknown graphics type.')
    end
end

function points = swapy(points,canvasmin,canvasmax)
    % mirror points in y-direction (in svg, the coordinate origin is in the
    % upper left)
    points(2,:) = canvasmax(2) - (points(2,:)-canvasmin(2));
end
