% saveSvg(filename,type,coords)
% saveSvg(filename,type,coords,attr)
% saveSvg(filename,type,coords,attr,canvasmin,canvasmax)
function saveSvg(filename,type,coords,varargin)

    attr = [];
    canvasmin = [];
    canvasmax = [];
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1
        attr = varargin{1};
    elseif numel(varargin) == 3
        attr = varargin{1};
        canvasmin = varargin{2};
        canvasmin = varargin{3};
    else
        error('Invalid number of arguments.');
    end
    
    if isempty(attr)
        attr = cell(1,numel(type));
    end
    
    if isempty(canvasmin)
        if isempty(coords)
            canvasmin = zeros(2,1);
        else
            canvasmin = min([coords{:}],[],2);
        end
    end
    if isempty(canvasmax)
        if isempty(coords)
            canvasmax = zeros(2,1);
        else
            canvasmax = max([coords{:}],[],2);
        end
    end
    
    doc = com.mathworks.xml.XMLUtils.createDocument('svg');
    doc.setDocumentURI(['file:/',filename]);
    
    node = doc.getDocumentElement;
    node.setAttribute('version','1.1');
    node.setAttribute('xmlns','http://www.w3.org/2000/svg');
    node.setAttribute('xmlns:xlink','http://www.w3.org/1999/xlink');
    node.setAttribute('x',[sprintf('%.30g',canvasmin(1)),'px']);
    node.setAttribute('y',[sprintf('%.30g',canvasmin(2)),'px']);
    node.setAttribute('width',[sprintf('%.30g',canvasmax(1) - canvasmin(1)),'px']);
    node.setAttribute('height',[sprintf('%.30g',canvasmax(2) - canvasmin(2)),'px']);
    node.setAttribute('viewBox',sprintf('%.30g ',[canvasmin',(canvasmax-canvasmin)']));

    writesvggeometry(type,coords,attr,node);
    
    xmlwrite(filename,doc);
end
