% in the first group is the geometry without group
function [type,coords,transforms,attr,groups] = readsvggeometry(parentnode,discretize,maxerror)

    type = {};
    coords = {};
    transforms = zeros(3,3,0);
    attr = {};
    groups = sparse([]);
    
    parentnodequeue = parentnode; % to avoid recursion (which is capped at 500 by default in matlab)
    
    groupind = 1;
    while not(isempty(parentnodequeue))
        parentnode = parentnodequeue(1);
        parentnodequeue = parentnodequeue(2:end);
        
        nodelist = parentnode.getChildNodes();
        for i=1:nodelist.getLength()
            node = nodelist.item(i-1);
            if node.getNodeType() == node.ELEMENT_NODE
                nodetype = char(node.getTagName());
                if strcmp(nodetype,'g')
                    
                    parentnodequeue(end+1) = node; %#ok<AGROW>
                    
                elseif strcmp(nodetype,'path')
                    
                    [subpathcoords,pathtransform,pathattr] = readPath(node);
                    % iterate over subpaths
                    for j=1:numel(subpathcoords)
                        type{end+1} = 'path'; %#ok<AGROW>
                        coords{end+1} = subpathcoords{j}; %#ok<AGROW>
                        transforms(:,:,end+1) = pathtransform; %#ok<AGROW>
                        if not(isempty(pathattr))
                            attr{end+1} = containers.Map(pathattr.keys,pathattr.values); %#ok<AGROW>
                        else
                            attr{end+1} = containers.Map('KeyType',pathattr.KeyType,'ValueType',pathattr.ValueType); %#ok<AGROW>
                        end
                        groups(end+1,groupind) = 1; %#ok<SPRIX,AGROW>
                    end
                    
                elseif strcmp(nodetype,'polyline')
                    
                    type{end+1} = 'polyline'; %#ok<AGROW>
                    [coords{end+1},transforms(:,:,end+1),attr{end+1}] = readPolyline(node); %#ok<AGROW>
                    groups(end+1,groupind) = 1; %#ok<SPRIX,AGROW>
                
                elseif strcmp(nodetype,'polygon')
                
                    type{end+1} = 'polygon'; %#ok<AGROW>
                    [coords{end+1},transforms(:,:,end+1),attr{end+1}] = readPolygon(node); %#ok<AGROW>
                    groups(end+1,groupind) = 1; %#ok<SPRIX,AGROW>
                
                elseif strcmp(nodetype,'line')
                    
                    type{end+1} = 'line'; %#ok<AGROW>
                    [coords{end+1},transforms(:,:,end+1),attr{end+1}] = readLine(node); %#ok<AGROW>
                    groups(end+1,groupind) = 1; %#ok<SPRIX,AGROW>
                    
                elseif strcmp(nodetype,'rect')
                    
                    type{end+1} = 'rect'; %#ok<AGROW>
                    [coords{end+1},transforms(:,:,end+1),attr{end+1}] = readRect(node); %#ok<AGROW>
                    groups(end+1,groupind) = 1; %#ok<SPRIX,AGROW>
                    
                elseif strcmp(nodetype,'circle')
                    
                    type{end+1} = 'circle'; %#ok<AGROW>
                    [coords{end+1},transforms(:,:,end+1),attr{end+1}] = readCircle(node); %#ok<AGROW>
                    groups(end+1,groupind) = 1; %#ok<SPRIX,AGROW>
                    
                elseif strcmp(nodetype,'ellipse')
                    
                    type{end+1} = 'ellipse'; %#ok<AGROW>
                    [coords{end+1},transforms(:,:,end+1),attr{end+1}] = readEllipse(node); %#ok<AGROW>
                    groups(end+1,groupind) = 1; %#ok<SPRIX,AGROW>

                end
            end
        end
        
        groupind = groupind + 1;
    end

    if nargin >= 3 && discretize
        [polylines,polylinesclosed] = discretizesvggeometry(type,coords,transforms,maxerror);
        for i=1:numel(type)
            if polylinesclosed(i)
                type{i} = 'polygon'; %#ok<AGROW>
            else
                type{i} = 'polyline'; %#ok<AGROW>
            end

            coords{i} = polylines{i}; %#ok<AGROW>
            
            transforms(:,:,i) = eye(3);
        end
    end
end

function [coords,transform,attr] = readRect(node)
    
    x = sscanf(char(node.getAttribute('x')),'%f');
    y = sscanf(char(node.getAttribute('y')),'%f');
    width = sscanf(char(node.getAttribute('width')),'%f');
    height = sscanf(char(node.getAttribute('height')),'%f');
    coords = [x,width;y,height];
    
    if node.hasAttribute('rx') || node.hasAttribute('ry')
        warning('Rounded rectangles not supported, converting the recatangle to non-rounded.');
    end

    transform = readsvgtransform(node);

    attr = containers.Map('KeyType','char','ValueType','char');
    nodeattributes = node.getAttributes();
    nodeattributes.removeNamedItem('x');
    nodeattributes.removeNamedItem('y');
    nodeattributes.removeNamedItem('width');
    nodeattributes.removeNamedItem('height');
    if not(isempty(nodeattributes.getNamedItem('rx')))
        nodeattributes.removeNamedItem('rx');
    end
    if not(isempty(nodeattributes.getNamedItem('ry')))
        nodeattributes.removeNamedItem('ry');
    end
    if not(isempty(nodeattributes.getNamedItem('transform')))
        nodeattributes.removeNamedItem('transform');
    end
    for i=1:nodeattributes.getLength()
        attribute = nodeattributes.item(i-1); 
        attr(char(attribute.getNodeName())) = char(attribute.getNodeValue());
    end
end

function [coords,transform,attr] = readCircle(node)
    
    cx = sscanf(char(node.getAttribute('cx')),'%f');
    cy = sscanf(char(node.getAttribute('cy')),'%f');
    r = sscanf(char(node.getAttribute('r')),'%f');
    coords = [cx,r;cy,r];

    transform = readsvgtransform(node);

    attr = containers.Map('KeyType','char','ValueType','char');
    nodeattributes = node.getAttributes();
    nodeattributes.removeNamedItem('cx');
    nodeattributes.removeNamedItem('cy');
    nodeattributes.removeNamedItem('r');
    if not(isempty(nodeattributes.getNamedItem('transform')))
        nodeattributes.removeNamedItem('transform');
    end
    for i=1:nodeattributes.getLength()
        attribute = nodeattributes.item(i-1); 
        attr(char(attribute.getNodeName())) = char(attribute.getNodeValue());
    end
end

function [coords,transform,attr] = readEllipse(node)
    
    cx = sscanf(char(node.getAttribute('cx')),'%f');
    cy = sscanf(char(node.getAttribute('cy')),'%f');
    rx = sscanf(char(node.getAttribute('rx')),'%f');
    ry = sscanf(char(node.getAttribute('ry')),'%f');
    coords = [cx,rx;cy,ry];

    transform = readsvgtransform(node);

    attr = containers.Map('KeyType','char','ValueType','char');
    nodeattributes = node.getAttributes();
    nodeattributes.removeNamedItem('cx');
    nodeattributes.removeNamedItem('cy');
    nodeattributes.removeNamedItem('rx');
    nodeattributes.removeNamedItem('ry');
    if not(isempty(nodeattributes.getNamedItem('transform')))
        nodeattributes.removeNamedItem('transform');
    end
    for i=1:nodeattributes.getLength()
        attribute = nodeattributes.item(i-1); 
        attr(char(attribute.getNodeName())) = char(attribute.getNodeValue());
    end
end

function [coords,transform,attr] = readLine(node)
    
    x1 = sscanf(char(node.getAttribute('x1')),'%f');
    y1 = sscanf(char(node.getAttribute('y1')),'%f');
    x2 = sscanf(char(node.getAttribute('x2')),'%f');
    y2 = sscanf(char(node.getAttribute('y2')),'%f');
    coords = [x1,x2;y1,y2];

    transform = readsvgtransform(node);

    attr = containers.Map('KeyType','char','ValueType','char');
    nodeattributes = node.getAttributes();
    nodeattributes.removeNamedItem('x1');
    nodeattributes.removeNamedItem('y1');
    nodeattributes.removeNamedItem('x2');
    nodeattributes.removeNamedItem('y2');
    if not(isempty(nodeattributes.getNamedItem('transform')))
        nodeattributes.removeNamedItem('transform');
    end
    for i=1:nodeattributes.getLength()
        attribute = nodeattributes.item(i-1); 
        attr(char(attribute.getNodeName())) = char(attribute.getNodeValue());
    end
end

function [coords,transform,attr] = readPolyline(node)
    
    polylineStr = char(node.getAttribute('points'));    
    tokens = regexp(polylineStr,'[a-zA-Z]|([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)','match');
    coords = reshape(str2double(tokens),2,[]);

    transform = readsvgtransform(node);

    attr = containers.Map('KeyType','char','ValueType','char');
    nodeattributes = node.getAttributes();
    nodeattributes.removeNamedItem('points');
    if not(isempty(nodeattributes.getNamedItem('transform')))
        nodeattributes.removeNamedItem('transform');
    end
    for i=1:nodeattributes.getLength()
        attribute = nodeattributes.item(i-1); 
        attr(char(attribute.getNodeName())) = char(attribute.getNodeValue());
    end
end

function [coords,transform,attr] = readPolygon(node)
    % same as readPolyline
    
    polylineStr = char(node.getAttribute('points'));    
    tokens = regexp(polylineStr,'[a-zA-Z]|([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)','match');
    coords = reshape(str2double(tokens),2,[]);

    transform = readsvgtransform(node);

    attr = containers.Map('KeyType','char','ValueType','char');
    nodeattributes = node.getAttributes();
    nodeattributes.removeNamedItem('points');
    if not(isempty(nodeattributes.getNamedItem('transform')))
        nodeattributes.removeNamedItem('transform');
    end
    for i=1:nodeattributes.getLength()
        attribute = nodeattributes.item(i-1); 
        attr(char(attribute.getNodeName())) = char(attribute.getNodeValue());
    end
end

function [coords,transform,attr] = readPath(node)
    % convert the entire path to bezier spline segments
	
    pathStr = char(node.getAttribute('d'));
%     disp(pathStr);

    tokens = regexp(pathStr,'[a-zA-Z]|([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)','match');

    si = 0;
    coords = {};

    currentPoint = zeros(2,1);
    subpathStart = zeros(2,1);
    lastControl = [];
    
    snapdist = 1.0e-08;

    i=1;
    while i <= numel(tokens)
        token = tokens{i};
        i = i + 1;
        if token == 'M' || token == 'm' % start new subpath
%             disp('M or m');
            lastControl = [];
            si = si+1;
            if token == 'M' % absolute
%                 disp('M');
                subpathStart = [sscanf(tokens{i},'%f');sscanf(tokens{i+1},'%f')];
                i = i + 2;
            else            % relative
%                 disp('m');
                subpathStart = currentPoint + [sscanf(tokens{i},'%f');sscanf(tokens{i+1},'%f')];
                i = i + 2;
            end
            currentPoint = subpathStart;
%             disp('start');
%             disp(subpathStart);
            coords{si} = [subpathStart,nan(2,1)]; %#ok<AGROW>

            lastpoint = currentPoint;
            while i <= numel(tokens) && not(isempty(sscanf(tokens{i},'%f')))
                p1 = lastpoint;
                if token == 'M' % absolute
%                     disp('ML');
                    p4 = [sscanf(tokens{i},'%f');sscanf(tokens{i+1},'%f')];
                    i = i + 2;
                else            % relative
%                     disp('ml');
                    p4 = lastpoint + [sscanf(tokens{i},'%f');sscanf(tokens{i+1},'%f')];
                    i = i + 2;
                end
                if norm(p1 - p4) > snapdist % don't write line segments with zero length
                    coords{si} = [coords{si},p4,nan(2,1)]; %#ok<AGROW>
                end
                lastpoint = p4;
            end
            currentPoint = lastpoint;
        elseif token == 'Z' || token == 'z' % straight line segment that closes the subpath
            %             disp('Z or z');
            lastControl = [];
            p1 = currentPoint;
            p4 = subpathStart;
%             if norm(p1 - p4) > snapdist % z is also encountered after the path was already closed
%                 coords{si} = [coords{si},p4]; %#ok<AGROW> % no NaN at end if path is closed
%             end
            if norm(p1 - p4) <= snapdist
                % do not close path if first and last point are already the
                % same (cannot remove point at start or end, otherwise
                % bezier segment might loose a point)
                coords{si}(:,end-1) = p4; %#ok<AGROW>
%                 coords{si} = [coords{si},nan(2,1)]; %#ok<AGROW>
            else
                coords{si}(:,end) = [];  %#ok<AGROW> % no NaN at end if path is closed
            end
            currentPoint = subpathStart;
        elseif token == 'L' || token == 'l' || token == 'H' || token == 'h' || ... % straight line segment
               token == 'V' || token == 'v' 

            lastControl = []; 
            lastpoint = currentPoint;
            while i <= numel(tokens) && not(isempty(sscanf(tokens{i},'%f')))
                p1 = lastpoint;
                if token == 'L' % absolute line
%                     disp('L');
                    p4 = [sscanf(tokens{i},'%f');sscanf(tokens{i+1},'%f')];
                    i = i + 2;
                elseif token == 'l'  % relative line
%                     disp('l');
                    p4 = lastpoint + [sscanf(tokens{i},'%f');sscanf(tokens{i+1},'%f')];
                    i = i + 2;
                elseif token == 'H'  % absolute horizontal line
%                     disp('H');
                    p4 = [sscanf(tokens{i},'%f');lastpoint(2)];
                    i = i + 1;
                elseif token == 'h'  % relative horizontal line
%                     disp('h');
                    p4 = lastpoint + [sscanf(tokens{i},'%f');0];
                    i = i + 1;
                elseif token == 'V'  % absolute vertical line
%                     disp('V');
                    p4 = [lastpoint(1);sscanf(tokens{i},'%f')];
                    i = i + 1;
                elseif token == 'v'  % relative vertical line
%                     disp('v');
                    p4 = lastpoint + [0;sscanf(tokens{i},'%f')];
                    i = i + 1;
                end
                if norm(p1 - p4) > snapdist % don't write line segments with zero length
                    coords{si} = [coords{si},p4,nan(2,1)]; %#ok<AGROW>
                end
                lastpoint = p4;
            end
            currentPoint = lastpoint;
        elseif token == 'C' || token == 'c' || token == 'S' || token == 's' % cubic bezier curve
            if isempty(lastControl)
                lastControl = currentPoint;
            end
            lastpoint = currentPoint;
            while i <= numel(tokens) && not(isempty(sscanf(tokens{i},'%f')))
                p1 = lastpoint;
                if token == 'C' % absolute curve
%                     disp('C');
                    p2 = [sscanf(tokens{i},'%f');sscanf(tokens{i+1},'%f')];
                    p3 = [sscanf(tokens{i+2},'%f');sscanf(tokens{i+3},'%f')];
                    p4 = [sscanf(tokens{i+4},'%f');sscanf(tokens{i+5},'%f')];
                    i = i + 6;
                elseif token == 'c' % relative curve
%                     disp('c');
                    p2 = lastpoint + [sscanf(tokens{i},'%f');sscanf(tokens{i+1},'%f')];
                    p3 = lastpoint + [sscanf(tokens{i+2},'%f');sscanf(tokens{i+3},'%f')];
                    p4 = lastpoint + [sscanf(tokens{i+4},'%f');sscanf(tokens{i+5},'%f')];
                    i = i + 6;
                elseif token == 'S' % absolute smooth curve (first control point is last control point mirrored on last point)
%                     disp('S');
                    p2 = lastpoint + (lastpoint-lastControl);
                    p3 = [sscanf(tokens{i},'%f');sscanf(tokens{i+1},'%f')];
                    p4 = [sscanf(tokens{i+2},'%f');sscanf(tokens{i+3},'%f')];
                    i = i + 4;
                elseif token == 's' % relative smooth curve (first control point is last control point mirrored on last point)
%                     disp('s');
                    p2 = lastpoint + (lastpoint-lastControl);
                    p3 = lastpoint + [sscanf(tokens{i},'%f');sscanf(tokens{i+1},'%f')];
                    p4 = lastpoint + [sscanf(tokens{i+2},'%f');sscanf(tokens{i+3},'%f')];
                    i = i + 4;
                end
%                 disp([p1,p2,p3,p4]);
                if norm(p1 - p2) > snapdist || norm(p2 - p3) > snapdist || norm(p3 - p4) > snapdist % don't write curve segments with zero length
                    coords{si} = [coords{si},p2,p3,p4,nan(2,1)]; %#ok<AGROW>
%                         subpathbezierinds{si}(:,end+1) = (size(subpaths{si},2)-3:size(subpaths{si},2))'; %#ok<AGROW>
                end
                lastpoint = p4;
                lastControl = p3;
            end
            currentPoint = lastpoint;
        else
            error(['unsupported path command: ',token]);
        end
    end
    
    transform = readsvgtransform(node);

    attr = containers.Map('KeyType','char','ValueType','char');
    nodeattributes = node.getAttributes();
    nodeattributes.removeNamedItem('d');
    if not(isempty(nodeattributes.getNamedItem('transform')))
        nodeattributes.removeNamedItem('transform');
    end
    for i=1:nodeattributes.getLength()
        attribute = nodeattributes.item(i-1); 
        attr(char(attribute.getNodeName())) = char(attribute.getNodeValue());
    end
end

% function transform = readTransform(node)
%     transform = eye(2);
%     translate = [0;0];
%     if node.hasAttribute('transform')
%         transformStr = char(node.getAttribute('transform'));
%         if not(isempty(transformStr))
%             matrixStr = regexp(transformStr, 'matrix([^\)]*\)','match');
%             if not(isempty(matrixStr))
%                 matrixElms = regexp(matrixStr,'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?','match');    
%                 matrixElms = str2double(matrixElms{1});
%                 transform(:) = matrixElms(1:4);
%                 translate(:) = matrixElms(5:6);
%             else
%                 error('unsupported transform given in svg file');
%             end
%         end
%         transform = [transform,translate;[0,0,1]];
%     else
%         transform = eye(3);
%     end
% end
