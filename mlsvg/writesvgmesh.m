function nodes = writesvgmeshes(coords,meshstruct,attr,parentnode)
    
    nodes = {};
    
    for i=1:numel(coords)
%         if coords{i}(2,2) ~= coords{i}(2,1) || ...
%            coords{i}(1,3) ~= coords{i}(1,1)
%             error('Writing rotated or otherwise oddly transformed images not yet implemented.');
%         end
        
%         % get coordinates of image
%         x = coords{i}(1,3);
%         y = coords{i}(2,3);
%         w = coords{i}(1,2) - coords{i}(1,3);
%         h = coords{i}(2,1) - coords{i}(2,3);
        
        nodes{end+1} = writeMesh(meshstruct{i},parentnode); %#ok<AGROW>
        
        % write additional attributes
        keys = attr{i}.keys;
        vals = attr{i}.values;
        for j=1:numel(keys)
            nodes{end}.setAttribute(keys{j},vals{j});
        end
    end
end

function node = writeMesh(meshstruct,parentnode) %#ok<INUSL>
    node = parentnode.getOwnerDocument.createElement('meshmodel'); 
%     node.setAttribute('x',sprintf('%.30g',x));
%     node.setAttribute('y',sprintf('%.30g',y));
%     node.setAttribute('width',sprintf('%.30g',w));
%     node.setAttribute('height',sprintf('%.30g',h));

    % save to temporary file
    tempfilename = [tempname,'.mat'];
    save(tempfilename, 'meshstruct', '-mat' );
    
%     imwrite(rgba(end:-1:1,:,1:3),tempfilename,'Alpha',rgba(end:-1:1,:,4));
    
    % read image bytes from temporary file
    fid = fopen(tempfilename,'rb');
    bytes = fread(fid);
    fclose(fid);
    
    % delete temporary file
    delete(tempfilename);
    
    % base64 encode image bytes
    encoder = org.apache.commons.codec.binary.Base64;    
    base64string = char(encoder.encode(bytes))';
    
    node.setAttribute('xlink:href',['data:model/matlabmesh;base64,',base64string]);
    
    parentnode.appendChild(node);
end
