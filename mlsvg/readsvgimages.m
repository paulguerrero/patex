function [coords,rgba,transforms,attr] = readsvgimages(parentnode,transformed)
    
    coords = {};
    transforms = zeros(3,3,0);
    rgba = {};
    attr = {};

    % read images
    imageNodeList = parentnode.getElementsByTagName('image');
    for p = 1:imageNodeList.getLength()
        imageNode = imageNodeList.item(p-1);
        [imgcoords,imgrgba,imgtransforms,imgattr] = ...
            readImage(imageNode);
        if not(isempty(imgcoords))
            coords{end+1} = imgcoords; %#ok<AGROW>
            rgba{end+1} = imgrgba; %#ok<AGROW>
            transforms(:,:,end+1) = imgtransforms; %#ok<AGROW>
            attr{end+1} = imgattr; %#ok<AGROW>
        end
    end

    if transformed
        % apply transform to all coordinates
        for i=1:numel(coords)
            coords{i} = transform(coords{i},transforms(:,:,i)); %#ok<AGROW>
            transforms(:,:,i) = eye(3);
        end
    end
end

function [coords,rgba,transform,attr] = readImage(node)
    
    if node.hasAttribute('x')
        x = sscanf(char(node.getAttribute('x')),'%f');
    else
        x = 0;
    end
    if node.hasAttribute('y')
        y = sscanf(char(node.getAttribute('y')),'%f');
    else
        y = 0;
    end
    width = sscanf(char(node.getAttribute('width')),'%f');
    height = sscanf(char(node.getAttribute('height')),'%f');
    img = char(node.getAttribute('xlink:href'));
    if strcmp(img(1:5),'data:')

        % check MIME type (should be something like data:image/png;base64)
        [startind,endind,mimetype] = ...
            regexp(img,'data:(\w*)/(\w*);','start','end','tokens','once');
        
        if startind ~= 1 || not(strcmp(mimetype{1},'image'))
        	error('Invalid MIME type for embedded image.');
        end
        imageformat = mimetype{2};
        if numel(img) < endind+6 || not(strcmp(img(endind+1:endind+6),'base64'))
            error('Can only read embedded images that are encoded in base64.');
        end
        
        % get only base64 string (remove MIME specifier and separating
        % character)
        img = img(endind+8:end);
        
        % decode to bytes (decoder returns int8, but fwrite expects uint8)
        decoder = org.apache.commons.codec.binary.Base64;    
        bytes = typecast(decoder.decode(int8(img)),'uint8');
        
        % save image bytes to temporary file
        tempfilename = [tempname,'.',imageformat];
        fid = fopen(tempfilename,'wb');
        fwrite(fid,bytes);
        fclose(fid);
        
        % read image from temporary file
        [rgba,map,a] = imread(tempfilename,imageformat);
        
        % delete temporary file
        delete(tempfilename);
    else
        % get the path of the svg file, the image filename should be
        % relative to that path
        svgFilename = char(node.getOwnerDocument().getDocumentURI());
        if not(isempty(svgFilename))
            if numel(svgFilename) >= 6 && strcmp(svgFilename(1:6),'file:/')
                svgFilename = svgFilename(7:end);
            end
            svgFilepath = fileparts(svgFilename);
            filename = [svgFilepath,'/',img];
        
            [rgba,map,a] = imread(filename);
        else
            warning('Can''t find SVG filename, but need to find an image path relative to SVG filename, skipping image.');
            coords = [];
            rgba = [];
            transform = [];
            attr = {};
            return;
        end
    end
        
    if not(isempty(map))
        % for indexed colors
        rgba = im2double(rgba,'indexed');    
        rgba = ind2rgb(rgba,map);
    else
        % for non-indexed colors
        rgba = im2double(rgba);
    end
    if size(rgba,3) == 1
        % for intensity images
        rgba = cat(3,rgba(:,:,ones(1,3)),ones(size(rgba,1),size(rgba,2)));
    elseif size(rgba,3) ~= 3
        error('Unsupported number of channels.');
    end
    if not(isempty(a))
        % alpha channel available
        a = im2double(a);
    else
        % no alpha channel (use alpha = 1 everywhere)
        a = ones(size(rgba,1),size(rgba,2));
    end
    rgba = cat(3,rgba,a);

    % flip up/down
    rgba = rgba(end:-1:1,:,:);
	
    % set x,y and width,height to describe the extent of the pixel centers,
    % not the extent of the pixel areas
    pixsize = [width/size(rgba,2);height/size(rgba,1)];
    x = x + pixsize(1)/2;
    y = y + pixsize(2)/2;
    width = width - pixsize(1);
    height = height - pixsize(2);
    
    % store three image corners (image might be rotated in the transform)
    coords = [[x;y],[x+width;y],[x;y+height]];
    
    transform = readsvgtransform(node);

    attr = containers.Map('char','char');
    nodeattributes = node.getAttributes();
    if not(isempty(nodeattributes.getNamedItem('x')))
        nodeattributes.removeNamedItem('x');
    end
    if not(isempty(nodeattributes.getNamedItem('y')))
        nodeattributes.removeNamedItem('y');
    end
    nodeattributes.removeNamedItem('width');
    nodeattributes.removeNamedItem('height');
    nodeattributes.removeNamedItem('xlink:href');
    if not(isempty(nodeattributes.getNamedItem('transform')))
        nodeattributes.removeNamedItem('transform');
    end
    for i=1:nodeattributes.getLength()
        attribute = nodeattributes.item(i-1); 
        attr(char(attribute.getNodeName())) = char(attribute.getNodeValue());
    end
end

function points = transform(points,transform)
    points = transform * [points;ones(1,size(points,2))];
    points = points(1:2,:) ./ points([3,3],:);
end
