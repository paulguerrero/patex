function nodes = writesvggeometry(type,coords,attr,parentnode)
    
    nodes = {};
    
    for i=1:numel(type)
        if strcmp(type{i},'rect')
            if size(coords{i},2) >= 3
                nodes{end+1} = writeRect(...
                    coords{i}(1,1),coords{i}(2,1),...
                    coords{i}(1,2),coords{i}(2,2),...
                    parentnode,...
                    coords{i}(1,2),coords{i}(2,3)); %#ok<AGROW>
            else
                nodes{end+1} = writeRect(...
                    coords{i}(1,1),coords{i}(2,1),...
                    coords{i}(1,2),coords{i}(2,2),parentnode); %#ok<AGROW>
            end
        elseif strcmp(type{i},'circle')
            nodes{end+1} = writeCircle(...
                coords{i}(1,1),coords{i}(2,1),...
                coords{i}(1,2),...
                parentnode); %#ok<AGROW>
        elseif strcmp(type{i},'ellipse')
            nodes{end+1} = writeEllipse(...
                coords{i}(1,1),coords{i}(2,1),...
                coords{i}(1,2),coords{i}(2,2),...
                parentnode); %#ok<AGROW>
        elseif strcmp(type{i},'line')
            nodes{end+1} = writeLine(...
                coords{i}(1,1),coords{i}(2,1),...
                coords{i}(1,2),coords{i}(2,2),...
                parentnode); %#ok<AGROW>
        elseif strcmp(type{i},'polyline')
            nodes{end+1} = writePolyline(coords{i},parentnode); %#ok<AGROW>
        elseif strcmp(type{i},'polygon')
            nodes{end+1} = writePolygon(coords{i},parentnode); %#ok<AGROW>
        elseif strcmp(type{i},'path')
            nodes{end+1} = writePath(coords{i},parentnode); %#ok<AGROW>
        else
            error('Unknown svg shape.');
        end
        
        % write additional attributes
        if not(isempty(attr{i}))
            keys = attr{i}.keys;
            vals = attr{i}.values;
            for j=1:numel(keys)
                nodes{end}.setAttribute(keys{j},vals{j});
            end
        end
    end
end

function node = writeRect(x,y,w,h,parentnode,rx,ry)
    node = parentnode.getOwnerDocument.createElement('rect'); 
    node.setAttribute('x',sprintf('%.30g',x));
    node.setAttribute('y',sprintf('%.30g',y));
    node.setAttribute('w',sprintf('%.30g',w));
    node.setAttribute('h',sprintf('%.30g',h));
        
    if nargin >= 7
        node.setAttribute('rx',sprintf('%.30g',rx));
        node.setAttribute('ry',sprintf('%.30g',ry));
    end
    
    parentnode.appendChild(node);
end

function node = writeCircle(cx,cy,r,parentnode)
    node = parentnode.getOwnerDocument.createElement('circle'); 
    node.setAttribute('cx',sprintf('%.30g',cx));
    node.setAttribute('cy',sprintf('%.30g',cy));
    node.setAttribute('r',sprintf('%.30g',r));
    
    parentnode.appendChild(node);
end

function node = writeEllipse(cx,cy,rx,ry,parentnode)
    node = parentnode.getOwnerDocument.createElement('ellipse'); 
    node.setAttribute('cx',sprintf('%.30g',cx));
    node.setAttribute('cy',sprintf('%.30g',cy));
    node.setAttribute('rx',sprintf('%.30g',rx));
    node.setAttribute('ry',sprintf('%.30g',ry));
    
    parentnode.appendChild(node);
end

function node = writeLine(x1,y1,x2,y2,parentnode)
    node = parentnode.getOwnerDocument.createElement('line'); 
    node.setAttribute('x1',sprintf('%.30g',x1));
    node.setAttribute('y1',sprintf('%.30g',y1));
    node.setAttribute('x2',sprintf('%.30g',x2));
    node.setAttribute('y2',sprintf('%.30g',y2));
    
    parentnode.appendChild(node);
end

function node = writePolyline(points,parentnode)
    pointstr = [];
    for j=1:size(points,2)
        pointstr = [pointstr,sprintf('%.30g',points(1,j)),',',sprintf('%.30g',points(2,j)),' ']; %#ok<AGROW>
    end
    pointstr(end) = [];

    node = parentnode.getOwnerDocument.createElement('polyline'); 
    node.setAttribute('points',pointstr);
    parentnode.appendChild(node);
end

function node = writePolygon(points,parentnode)
    pointstr = [];
    for j=1:size(points,2)
        pointstr = [pointstr,sprintf('%.30g',points(1,j)),',',sprintf('%.30g',points(2,j)),' ']; %#ok<AGROW>
    end
    pointstr(end) = [];

    node = parentnode.getOwnerDocument.createElement('polygon'); 
    node.setAttribute('points',pointstr);
    parentnode.appendChild(node);
end

function node = writePath(points,parentnode)
    % Write a path composed of straight line segmens and (cubic) bezier
    % spline segments, the format is the same that would be returned by
    % readsvggeometry: a nan-separated 2xn vector of point coordinates,
    % where the first component is the starting point, and for each
    % following component 1 point creates a straight line segment and 3
    % points create a bezier segment (both include the last point of the
    % previous component). The path is closed with an additonal line
    % between end- and start point if the last entry of the coordinate
    % vector is NaN.
    
    node = parentnode.getOwnerDocument.createElement('path');     

	[x,y] = polysplit(points(1,:),points(2,:));
    
    pathstr = '';
    if not(isempty(x))
        pathstr = [pathstr,'M',...
            sprintf('%.30g',x{1}),',',...
            sprintf('%.30g',y{1})];
        
        for i=2:numel(x)
            if numel(x{i}) == 1
                pathstr = [pathstr,' L',...
                    sprintf('%.30g',x{i}),',',...
                    sprintf('%.30g',y{i})]; %#ok<AGROW>
            elseif numel(x{i}) == 3
                pathstr = [pathstr,' C',...
                    sprintf('%.30g',x{i}(1)),',',...
                    sprintf('%.30g',y{i}(1)),' ',...
                    sprintf('%.30g',x{i}(2)),',',...
                    sprintf('%.30g',y{i}(2)),' ',...
                    sprintf('%.30g',x{i}(3)),',',...
                    sprintf('%.30g',y{i}(3))]; %#ok<AGROW>
            else
                error('Invalid format for path coordinates.');
            end
        end
        
        if not(isnan(points(1,end)))
            pathstr = [pathstr,' Z'];
        end
    end
    
    node.setAttribute('d',pathstr);
    parentnode.appendChild(node);
end
