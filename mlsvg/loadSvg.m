% [type,coords,transforms] = loadSvg(filename)
% [type,coords,transforms] = loadSvg(filename,discretize)
% [type,coords,transforms] = loadSvg(filename,discretize,maxerror)
% [type,coords,transforms,attr] = loadSvg(...)
% [type,coords,transforms,attr,canvasmin,canvasmax] = loadSvg(...)
%
% if discretize is true, all types are polygon or polyline and all
% transforms are identity
% maxerror in multiples of the bounding box diagonal of each shape
function [type,coords,transforms,attr,canvasmin,canvasmax] = loadSvg(filename,discretize,maxerror)

    if nargin < 2 || isempty(discretize)
        discretize = true;
    end
    
    if nargin < 3 || isempty(maxerror)
        maxerror = 0.001;
    end

    fp = fopen(filename,'rt');
	filestr = fread(fp,inf,'*char')';
	fclose(fp);
	filestr = regexprep(filestr, '<!DOCTYPE [^>]*>','','once','ignorecase');

	xmldoc = xmlreadstring(filestr);
	svgnodelist = getDOMChildElements(xmldoc,'svg');
	if isempty(svgnodelist)
		error('The xml document does not appear to be an svg file.');
	end
	svgnode = svgnodelist(end);

    [type,coords,transforms,attr] = readsvggeometry(svgnode,discretize,maxerror);
    
    canvasmin = [];
    canvasmax = [];
    if svgnode.hasAttribute('viewBox')
        viewboxStr = char(svgnode.getAttribute('viewBox'));    
        tokens = regexp(viewboxStr,'[a-zA-Z]|([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)','match');
        viewbox = reshape(str2double(tokens),2,[]);
        canvasmin = viewbox(1:2)';
        canvasmax = canvasmin + viewbox(3:4)';
    end

end

