function [coords,meshstruct,transforms,attr] = readsvgmeshes(parentnode)
    
    coords = {};
    transforms = zeros(3,3,0);
    meshstruct = {};
    attr = {};

    % read images
    meshNodeList = parentnode.getElementsByTagName('meshmodel');
    for p = 1:meshNodeList.getLength()
        meshNode = meshNodeList.item(p-1);
        [coords{end+1},meshstruct{end+1},transforms(:,:,end+1),attr{end+1}] = ...
            readMesh(meshNode); %#ok<AGROW>
    end

%     if transformed
%         % apply transform to all coordinates
%         for i=1:numel(coords)
%             coords{i} = transform(coords{i},transforms(:,:,i)); %#ok<AGROW>
%             transforms(:,:,i) = eye(3);
%         end
%     end
end

function [coords,meshstruct,transform,attr] = readMesh(node)
    
    meshdata = char(node.getAttribute('xlink:href'));
    if strcmp(meshdata(1:5),'data:')

        % check MIME type (should be something like data:model/matlabmesh;base64)
        [startind,endind,mimetype] = ...
            regexp(meshdata,'data:(\w*)/(\w*);','start','end','tokens','once');
        
        if startind ~= 1 || not(strcmp(mimetype{1},'model')) || not(strcmp(mimetype{2},'matlabmesh'))
        	error('Invalid MIME type for embedded mesh.');
        end
        if numel(meshdata) < endind+6 || not(strcmp(meshdata(endind+1:endind+6),'base64'))
            error('Can only read embedded meshes that are encoded in base64.');
        end
        
        % get only base64 string (remove MIME specifier and separating
        % character)
        meshdata = meshdata(endind+8:end);
        
        % decode to bytes (decoder returns int8, but fwrite expects uint8)
        decoder = org.apache.commons.codec.binary.Base64;    
        bytes = typecast(decoder.decode(int8(meshdata)),'uint8');
        
        % save mesh bytes to temporary file
        tempfilename = [tempname,'.mat'];
        fid = fopen(tempfilename,'wb');
        fwrite(fid,bytes);
        fclose(fid);
        
        % read image from temporary file
        s = load(tempfilename,'-mat');
        meshstruct = s.meshstruct;
        
        % delete temporary file
        delete(tempfilename);
    end
        
    % mesh has no coordinates (todo: mesh should have a possible transform,
    % stored as the four bounding box corners, or alternatively as bounding
    % box center position, three angles and a scaling)
    coords = [];
    
    transform = readsvgtransform(node);

    attr = containers.Map('char','char');
    nodeattributes = node.getAttributes();
    nodeattributes.removeNamedItem('xlink:href');
    if not(isempty(nodeattributes.getNamedItem('transform')))
        nodeattributes.removeNamedItem('transform');
    end
    for i=1:nodeattributes.getLength()
        attribute = nodeattributes.item(i-1); 
        attr(char(attribute.getNodeName())) = char(attribute.getNodeValue());
    end
end

% function points = transform(points,transform)
%     points = transform * [points;ones(1,size(points,2))];
%     points = points(1:2,:) ./ points([3,3],:);
% end
