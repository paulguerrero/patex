% converts all shapes to polylines (with a given error bound)
function [points,closed] = discretizesvggeometry(type,coords,transforms,maxerror,abserror)
    if nargin < 5
        abserror = false;
    end

    if not(abserror)
        % compute approximate bounding box, the error bound is relative to
        % the extent of the geometry (bounding box diagonal)
        
        bbmin = nan(2,1);
        bbmax = nan(2,1);

        
        for i=1:numel(type)
            if strcmp(type{i},'rect')
                bbmin = min(bbmin,coords{i}(:,1));
                bbmax = max(bbmax,coords{i}(:,1)+coords{i}(:,2));
            elseif strcmp(type{i},'circle')
                bbmin = min(bbmin,coords{i}(:,1)-coords{i}([1,1],2));
                bbmax = max(bbmax,coords{i}(:,1)+coords{i}([1,1],2));
            elseif strcmp(type{i},'ellipse')
                bbmin = min(bbmin,coords{i}(:,1)-coords{i}(:,2));
                bbmax = max(bbmax,coords{i}(:,1)+coords{i}(:,2));
            elseif strcmp(type{i},'line')
                bbmin = min(bbmin,min(coords{i}(:,1:2),[],2));
                bbmax = max(bbmax,max(coords{i}(:,1:2),[],2));
            elseif strcmp(type{i},'polyline')
                bbmin = min(bbmin,min(coords{i},[],2));
                bbmax = max(bbmax,max(coords{i},[],2));
            elseif strcmp(type{i},'polygon')
                bbmin = min(bbmin,min(coords{i},[],2));
                bbmax = max(bbmax,max(coords{i},[],2));
            elseif strcmp(type{i},'path')
                % not entirely exact => just max or min of control points, but
                % ok for now
                bbmin = min(bbmin,min(coords{i},[],2));
                bbmax = max(bbmax,max(coords{i},[],2));
            else
                error('Unknown svg shape.');
            end
        end
    
        bbdiag = sqrt(sum((bbmax-bbmin).^2,1));
        absmaxerror = maxerror * bbdiag;
    end
    
    % discretize geometry
    points = {};
    closed = [];
    for i=1:numel(type)
        if strcmp(type{i},'rect')
            [points{end+1},closed(end+1)] = discretizeRect(coords{i},absmaxerror); %#ok<AGROW>
        elseif strcmp(type{i},'circle')
            [points{end+1},closed(end+1)] = discretizeCircle(coords{i},absmaxerror); %#ok<AGROW>
        elseif strcmp(type{i},'ellipse')
            [points{end+1},closed(end+1)] = discretizeEllipse(coords{i},absmaxerror); %#ok<AGROW>
        elseif strcmp(type{i},'line')
            [points{end+1},closed(end+1)] = discretizeLine(coords{i},absmaxerror); %#ok<AGROW>
        elseif strcmp(type{i},'polyline')
            [points{end+1},closed(end+1)] = discretizePolyline(coords{i},absmaxerror); %#ok<AGROW>
        elseif strcmp(type{i},'polygon')
            [points{end+1},closed(end+1)] = discretizePolygon(coords{i},absmaxerror); %#ok<AGROW>
        elseif strcmp(type{i},'path')
            [points{end+1},closed(end+1)] = discretizePath(coords{i},absmaxerror); %#ok<AGROW>
        else
            error('Unknown svg shape.');
        end
        
        % apply transform
        points{i} = transform(points{i},transforms(:,:,i)); %#ok<AGROW>
    end
end

function [points,closed] = discretizeRect(coords,~)
    x = coords(1,1);
    y = coords(2,1);
    width = coords(1,2);
    height = coords(2,2);
%     points = [x,x+width,x+width,x,x;...
%               y,y,y+height,y+height,y];
    points = [x,x+width,x+width,x;...
              y,y,y+height,y+height];
	closed = true;
end

function [points,closed] = discretizeCircle(coords,maxerror)
    cx = coords(1,1);
    cy = coords(2,1);
    r = coords(1,2);

    [points(1,:),points(2,:)] = polygonCircle(cx,cy,r,maxerror,'maxerror');
    closed = true;
end

function [points,closed] = discretizeEllipse(coords,maxerror)
%     error('Ellipse discretization not yet implemented.');
    
    cx = coords(1,1);
    cy = coords(2,1);
    rx = coords(1,2);
    ry = coords(2,2);
    
    [points(1,:),points(2,:)] = polygonEllipse(cx,cy,rx,ry,maxerror,'maxerror');
    
    closed = true;
end

function [points,closed] = discretizeLine(coords,~)
    x1 = coords(1,1);
    y1 = coords(2,1);
    x2 = coords(1,2);
    y2 = coords(2,2);
    
    points = [x1,x2;y1,y2];
    closed = false;
end

function [points,closed] = discretizePolyline(coords,~)
    points = coords;
    closed = false;
end

function [points,closed] = discretizePolygon(coords,~)
    points = coords;
    closed = true;
end

function [points,closed] = discretizePath(coords,maxerror)
    [x,y] = polysplit(coords(1,:),coords(2,:));
    points = zeros(2,0);
    for i=1:numel(x)
        if numel(x{i}) == 3 && not(isempty(points))
            
            controlpoints = [points(:,end),[x{i};y{i}]];
            bsegpoints = discretizeBezierSegment(...
                controlpoints(:,1),controlpoints(:,2),...
                controlpoints(:,3),controlpoints(:,4),...
                maxerror);
            points = [points,bsegpoints(:,2:end)]; %#ok<AGROW>
        elseif numel(x{i}) == 1
            points(:,end+1) = [x{i};y{i}]; %#ok<AGROW>
        else
            error('Invalid number of points in a path segment.');
        end
    end
    
    closed = not(isnan(coords(1,end)));
end

function points = discretizeBezierSegment(p1,p2,p3,p4,maxerror)
    samplet = [0,1];
    points = bezier3(p1,p2,p3,p4,samplet);
    seginds = 1;
    while true
        
        % compute distance of bezier curve at midpoint
        midpointsx = zeros(3,numel(seginds));
        midpointsy = zeros(3,numel(seginds));
        
        midsamplet = (samplet(seginds)'   * [0.75,0.5,0.25])'+...
                     (samplet(seginds+1)' * [0.25,0.5,0.75])';

        midpoints = bezier3(p1,p2,p3,p4,midsamplet(1,:));
        midpointsx(1,:) = midpoints(1,:);
        midpointsy(1,:) = midpoints(2,:);
        
        midpoints = bezier3(p1,p2,p3,p4,midsamplet(2,:));
        midpointsx(2,:) = midpoints(1,:);
        midpointsy(2,:) = midpoints(2,:);
        
        midpoints = bezier3(p1,p2,p3,p4,midsamplet(3,:));
        midpointsx(3,:) = midpoints(1,:);
        midpointsy(3,:) = midpoints(2,:);
        
        error = inf(3,numel(seginds));
        for i=1:numel(seginds)
            [error(:,i),~] = pointPolylineDistance_mex(...
                midpointsx(:,i)',midpointsy(:,i)',...
                points(1,[seginds(i),seginds(i)+1]),...
                points(2,[seginds(i),seginds(i)+1]));
        end
        [error,maxind] = max(error,[],1);
        inds = sub2ind(size(midsamplet),maxind,1:size(midsamplet,2));
        midsamplet = midsamplet(inds);
        midpoints = [midpointsx(inds);midpointsy(inds)];
        
%         sqerror = sum((linmidpoints-midpoints).^2,1);
        
        if max(error) <= maxerror
            break;
        end
        
        midsamplet = midsamplet(error > maxerror);
        midpoints = midpoints(:,error > maxerror);
        
        % insert midpoints into the list of samples
        [samplet,perm] = sort([samplet,midsamplet],'ascend');
        
        newind = zeros(1,numel(perm));
        newind(perm) = 1:numel(perm);
        seginds = newind(size(points,2)+1:end);
        seginds = unique([seginds,seginds-1]);
        
        points = [points,midpoints]; %#ok<AGROW>
        points = points(:,perm);
        
        % new segments to check are segments before and after the inserted
        % points

    end
end

function points = transform(points,transform)
    points = transform * [points;ones(1,size(points,2))];
    points = points(1:2,:) ./ points([3,3],:);
end
