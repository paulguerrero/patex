% function over a subspace of pose space, sampled on a grid
classdef PosespaceSamplegrid < handle % & matlab.mixin.Copyable
    
properties
    posemin = [];
    posemax = [];
    
    val = [];
    
    % pose dimensions that are not sampled
    posx = nan;
    posy = nan;
    posz = nan;
    angleu = nan;
    anglev = nan;
    anglew = nan;
    scale = nan;
    mir = nan;
    
    % variance of pose dimensions (scalar value if sampled on grid)
    posxvar = nan;
    posyvar = nan;
    poszvar = nan;
    angleuvar = nan;
    anglevvar = nan;
    anglewvar = nan;
    scalevar = nan;
    mirvar = nan;
    
    posxdim = [];
    posydim = [];
    poszdim = [];
    angleudim = [];
    anglevdim = [];
    anglewdim = [];
    scaledim = [];
    mirdim = [];
    
    addinfo = struct;
end

methods

% PosespaceSamplegrid()
% PosespaceSamplegrid(copyobj)
% PosespaceSamplegrid(posemin,posemax,res,pose,posevar)
% PosespaceSamplegrid(posemin,posemax,val,pose,posevar)
% PosespaceSamplegrid(...,nameaddinfopairs)
% pose and posevar are structs with fields:
% x,y,z,au,av,aw,s,m for 3D poses
% x,y,a,s,m for 2D poses
% If values are given for any of these elements, the corresponding
% pose dimension is fixed and not part of the grid. Empty [] means the pose
% dimension is part of the grid. NaN means the dimension is not part of the
% pose (neither grid nor fixed).
function obj = PosespaceSamplegrid(posemin,posemax,val,pose,posevar,varargin)
    if abs(nargin) == 0
        % do nothing
    elseif abs(nargin) == 1
        dims = num2cell(size(posemin));
        obj(dims{:}) = PosespaceSamplegrid; % array constructor
        obj.copyFrom(posemin);
    elseif abs(nargin) >= 5
        obj.posemin = posemin;
        obj.posemax = posemax;
        
        if isvector(val) && numel(val) == numel(posemin)
            % construct from given resolution and posemin/max
            res = val;
%             obj.val = cell(numel(res),1);
%             for i=1:numel(res)
%                 obj.val{i} = linspace(obj.posemin(i),obj.posemax(i),res(i));
%             end
            
            res_c = num2cell(res);
            obj.val = zeros(res_c{:});
%             obj.val = ndgrid(obj.val{:});
        else
            obj.val = val;
        end
        
        if isfield(pose,'z')
            % pose values
            obj.posx = pose.x;
            obj.posy = pose.y;
            obj.posz = pose.z;
            obj.angleu = pose.au;
            obj.anglev = pose.av;
            obj.anglew = pose.aw;
            obj.scale = pose.s;
            obj.mir = pose.m;
            
            % pose variances
            if not(isempty(posevar))
                obj.posxvar = posevar.x;
                obj.posyvar = posevar.y;
                obj.poszvar = posevar.z;
                obj.angleuvar = posevar.au;
                obj.anglevvar = posevar.av;
                obj.anglewvar = posevar.aw;
                obj.scalevar = posevar.s;
                obj.mirvar = posevar.m;
            end
        else
            % pose values
            obj.posx = pose.x;
            obj.posy = pose.y;
            obj.angleu = pose.a;
            obj.scale = pose.s;
            obj.mir = pose.m;
            
            % pose variances
            if not(isempty(posevar))
                obj.posxvar = posevar.x;
                obj.posyvar = posevar.y;
                obj.angleuvar = posevar.a;
                obj.scalevar = posevar.s;
                obj.mirvar = posevar.m;
            end
        end
        
        
        subspacedims = 0;
        if isempty(obj.posx)
            obj.posxdim = subspacedims+1;
            subspacedims = subspacedims+1;
        end
        if isempty(obj.posy)
            obj.posydim = subspacedims+1;
            subspacedims = subspacedims+1;
        end
        if isempty(obj.posz)
            obj.poszdim = subspacedims+1;
            subspacedims = subspacedims+1;
        end
        if isempty(obj.angleu)
            obj.angleudim = subspacedims+1;
            subspacedims = subspacedims+1;
        end
        if isempty(obj.anglev)
            obj.anglevdim = subspacedims+1;
            subspacedims = subspacedims+1;
        end
        if isempty(obj.anglew)
            obj.anglewdim = subspacedims+1;
            subspacedims = subspacedims+1;
        end
        if isempty(obj.scale)
            obj.scaledim = subspacedims+1;
            subspacedims = subspacedims+1;
        end
        if isempty(obj.mir)
            obj.mirdim = subspacedims+1;
            subspacedims = subspacedims+1;
        end
        
        subspacedims = max(subspacedims,1);
        if (isempty(obj.posx) && numel(obj.posxvar)~=1) || ...
           (isempty(obj.posy) && numel(obj.posyvar)~=1) || ...
           (isempty(obj.posz) && numel(obj.poszvar)~=1) || ...
           (isempty(obj.angleu) && numel(obj.angleuvar)~=1) || ...
           (isempty(obj.anglev) && numel(obj.anglevvar)~=1) || ...
           (isempty(obj.anglew) && numel(obj.anglewvar)~=1) || ...
           (isempty(obj.scale) && numel(obj.scalevar)~=1) || ...
           (isempty(obj.mir) && numel(obj.mirvar)~=1)
            error('Pose variance and pose do not match.');
        end
        if (subspacedims == 1 && not(isrow(obj.val))) || ...
           (subspacedims > 2 && ndims(obj.val) ~= subspacedims)
            error('Invalid size for pose subspace.');
        end
        if isempty(obj.mir) && size(obj.val,ndims(obj.val)) ~= 2
            error('Sampled mirroring dimension must have 2 elements.');
        end
        if numel(obj.posemin) ~= subspacedims || numel(obj.posemax) ~= subspacedims
            error('Pose min or pose max does not have the correct size.');
        end
    
        if abs(nargin) >= 6
            
            if numel(varargin) == 1 && isstruct(varargin{1})
                obj.addinfo = varargin{1};
            else
                obj.addinfo = nvpairs2struct(varargin);
            end
            
            % check for NaNs
            nvnames = fieldnames(obj.addinfo);
            for i=1:numel(nvnames)
                if any(isnan(obj.addinfo.(nvnames{i})))
                    error('NaNs are not allowed for additional info.');
                end
            end
            
%             nargs = 0;
%             while numel(varargin) >= nargs+1
%                 if numel(varargin) < nargs+2
%                     error('Invalid number of parameters, expected (name,addinfo) pairs.');
%                 end
%                 if not(ischar(varargin{nargs+1}))
%                     error('Invalid parameters, expected (name,addinfo) pairs.');
%                 end
%                 if any(isnan(varargin{nargs+2}))
%                     error('Nans are not allowed for additional info.');
%                 end
% 
%                 obj.addinfo.(varargin{nargs+1}) = varargin{nargs+2};
%                 nargs = nargs+2;
%             end
        end
    else
        error('Invalid number of arguments, must be 0 or >=5.');
    end
end

function copyFrom(obj,obj2)
    for i=1:numel(obj)
        o = obj(i);
        o2 = obj2(i);
        
        o.posemin = o2.posemin;
        o.posemax = o2.posemax;

        o.val = o2.val;

        % pose dimensions that are not sampled
        o.posx = o2.posx;
        o.posy = o2.posy;
        o.posz = o2.posz;
        o.angleu = o2.angleu;
        o.anglev = o2.anglev;
        o.anglew = o2.anglew;
        o.scale = o2.scale;
        o.mir = o2.mir;

        % variance of pose dimensions (scalar value if sampled on grid)
        o.posxvar = o2.posxvar;
        o.posyvar = o2.posyvar;
        o.poszvar = o2.poszvar;
        o.angleuvar = o2.angleuvar;
        o.anglevvar = o2.anglevvar;
        o.anglewvar = o2.anglewvar;
        o.scalevar = o2.scalevar;
        o.mirvar = o2.mirvar;

        o.posxdim = o2.posxdim;
        o.posydim = o2.posydim;
        o.poszdim = o2.poszdim;
        o.angleudim = o2.angleudim;
        o.anglevdim = o2.anglevdim;
        o.anglewdim = o2.anglewdim;
        o.scaledim = o2.scaledim;
        o.mirdim = o2.mirdim;

        o.addinfo = o2.addinfo;
    end
end

function obj2 = clone(obj)
    obj2 = PosespaceSamplegrid(obj);
end

function [p,pvar] = getPoses(obj,gridcoords)
    if nargin < 2
        samples = obj.getSamples;
    else
        samples = obj.getSamples(gridcoords);
    end
    p = samples.pose;
    pvar = samples.posevar;
end

% gridcoords must be integers
function s = getSamples(obj,gridcoords)
    
    if nargin < 2
        gridcoords = cell(ndims(obj.val),1);
        for i=1:ndims(obj.val)
            gridcoords{i} = 1:size(obj.val,i);
        end

        [gridcoords{:}] = ndgrid(gridcoords{:});
        gridcoords = cell2mat(cellfun(@(x) x(:)',gridcoords,'UniformOutput',false));
    end
    
    % get both grid indices and grid coordinates (subs)
    if size(gridcoords,1) == 1
        % input is grid indices
        gridinds = gridcoords;
        if size(obj.val,1) > 1
            gridcoords = cell(size(obj.val,1),1);
            [gridcoords{:}] = ind2sub(size(obj.val),gridinds);
            gridcoords = cell2mat(gridcoords);
        end
    else
        % input is grid coordinates
        gridinds = mat2cell(gridcoords,ones(1,size(gridcoords,1)),size(gridcoords,2));
        gridinds = sub2ind(size(obj.val),gridinds{:});        
    end
    
    posx_ = obj.getSamplePose(obj.posxdim,gridcoords,obj.posx,gridinds);
    posy_ = obj.getSamplePose(obj.posydim,gridcoords,obj.posy,gridinds);
    posz_ = obj.getSamplePose(obj.poszdim,gridcoords,obj.posz,gridinds);
    angleu_ = obj.getSamplePose(obj.angleudim,gridcoords,obj.angleu,gridinds);
    anglev_ = obj.getSamplePose(obj.anglevdim,gridcoords,obj.anglev,gridinds);
    anglew_ = obj.getSamplePose(obj.anglewdim,gridcoords,obj.anglew,gridinds);
    scale_ = obj.getSamplePose(obj.scaledim,gridcoords,obj.scale,gridinds);
    mir_ = obj.getSamplePose(obj.mirdim,gridcoords,obj.mir,gridinds);
    
    posxvar_ = obj.getSamplePosevar(obj.posxvar,gridinds);
    posyvar_ = obj.getSamplePosevar(obj.posyvar,gridinds);
    poszvar_ = obj.getSamplePosevar(obj.poszvar,gridinds);
    angleuvar_ = obj.getSamplePosevar(obj.angleuvar,gridinds);
    anglevvar_ = obj.getSamplePosevar(obj.anglevvar,gridinds);
    anglewvar_ = obj.getSamplePosevar(obj.anglewvar,gridinds);
    scalevar_ = obj.getSamplePosevar(obj.scalevar,gridinds);
    mirvar_ = obj.getSamplePosevar(obj.mirvar,gridinds);

    addinfo_ = obj.addinfo;
    addinfonames = fieldnames(obj.addinfo);
    for i=1:numel(addinfonames)
        addinfo_.(addinfonames{i}) = addinfo_.(addinfonames{i})(gridinds);
    end
    
    pose = struct(...
        'x',posx_,...
        'y',posy_,...
        'z',posz_,...
        'au',angleu_,...
        'av',anglev_,...
        'aw',anglew_,...
        's',scale_,...
        'm',mir_);
    
    posevar = struct(...
        'x',posxvar_,...
        'y',posyvar_,...
        'z',poszvar_,...
        'au',angleuvar_,...
        'av',anglevvar_,...
        'aw',anglewvar_,...
        's',scalevar_,...
        'm',mirvar_);
    
    s = PosespaceSampleset(obj.val(gridinds),pose,posevar,addinfo_);
end

% function maxProject(obj,posedim)
%     
%     if strcmp(posedim,'posx')
%         removedim = obj.posxdim;
%         
%         vals = linspace(obj.posemin(removedim),obj.posemax(removedim),size(obj.val,removedim));
%         [~,maxinds] = obj.maxProject_singledim(removedim);
%         obj.posx = vals(maxinds);
%         
%         obj.posxdim = [];
%         obj.posydim = obj.posydim - 1;
%         obj.poszdim = obj.poszdim - 1;
%         obj.angleudim = obj.angleudim - 1;
%         obj.anglevdim = obj.anglevdim - 1;
%         obj.anglewdim = obj.anglewdim - 1;
%         obj.scaledim = obj.scaledim - 1;
%         obj.mirdim = obj.mirdim - 1;
%     elseif strcmp(posedim,'posy')
%         removedim = obj.posydim;
%         
%         vals = linspace(obj.posemin(removedim),obj.posemax(removedim),size(obj.val,removedim));
%         [~,maxinds] = obj.maxProject_singledim(removedim);
%         obj.posy = vals(maxinds);
%         
%         obj.posydim = [];
%         obj.poszdim = obj.poszdim - 1;
%         obj.angleudim = obj.angleudim - 1;
%         obj.anglevdim = obj.anglevdim - 1;
%         obj.anglewdim = obj.anglewdim - 1;
%         obj.scaledim = obj.scaledim - 1;
%         obj.mirdim = obj.mirdim - 1;
%     elseif strcmp(posedim,'angle')
%         removedim = obj.angleudim;
%         
%         vals = linspace(obj.posemin(removedim),obj.posemax(removedim),size(obj.val,removedim));
%         [~,maxinds] = obj.maxProject_singledim(removedim);
%         obj.angleu = vals(maxinds);
%         
%         obj.angleudim = [];
%         obj.scaledim = obj.scaledim - 1;
%         obj.mirdim = obj.mirdim - 1;
%     elseif strcmp(posedim,'scale')
%         removedim = obj.scaledim;
%         
%         vals = linspace(obj.posemin(removedim),obj.posemax(removedim),size(obj.val,removedim));
%         [~,maxinds] = obj.maxProject_singledim(removedim);
%         obj.scale = vals(maxinds);
%         
%         obj.scaledim = [];
%         obj.mirdim = obj.mirdim - 1;
%     elseif strcmp(posedim,'mir')
%         removedim = obj.mirdim;
%         
%         vals = [false,true];
%         [~,maxinds] = obj.maxProject_singledim(removedim);
%         obj.mir = vals(maxinds);
%         
%         obj.mirdim = [];
%     else
%         error('Unrecognized dimension');
%     end
%     
%     obj.posemin(removedim) = [];
%     obj.posemax(removedim) = [];
% end
% 
% function [maxvals,maxinds] = maxProject_singledim(obj,removedim)
%     if numel(removedim) ~= 1
%         error('Invalid maxproject dimension.');
%     end
%     
%     [maxvals,maxinds] = max(obj.val,[],removedim);
% 
%     % get the linear indices of the elements that are the maximum in
%     % each slice
%     [smaxind,slicelininds] = sort(maxinds(:));
%     linindto = [find(diff(smaxind),'ascend'),numel(lindinds)];
%     linindfrom = [1,linindto+1];
%     linindrange = zeros(2,size(obj.val,removedim));
%     linindrange(:,smaxind(linindfrom)) = [linindfrom;linindto];
% 
%     clear linindto linindfrom;
% 
%     alllininds = reshape(1:numel(obj.val),size(obj.val));
% 
%     newsize = size(obj.val);
%     newsize(removedim) = [];
%     newlininds = nan(newsize);
%     for i=1:size(obj.val,removedim)
%         slice = arrayslice(alllininds,removedim,i);
%         inds = slicelininds(linindrange(1,i):linindrange(2,i));
%         newlininds(inds) = slice(inds);
%     end
% 
%     obj.val = obj.val(newlininds);
% 
%     if not(isempty(obj.pos))
%         obj.pos = obj.pos(newlininds);
%     end
%     if not(isempty(obj.angleu))
%         obj.pos = obj.angleu(newlininds);
%     end
%     if not(isempty(obj.scale))
%         obj.scale = obj.scale(newlininds);
%     end
%     if not(isempty(obj.mir))
%         obj.mir = obj.mir(newlininds);
%     end
%     
%     addinfonames = fieldnames(obj.addinfo);
%     for i=1:numel(addinfonames)
%         obj.addinfo.(addinfonames{i}) = obj.addinfo.(addinfonames{i})(newlininds);
%     end
% end

function hasAddinfo(obj,name)
    isfield(obj.addinfo,name);
end

function newAddinfo(obj,name,val)
    obj.addinfo.(name) = val;
end

function p = gridlen2poselen(obj,glen,dim)
    % -1 because there are size(grid,dim)-1 spaces between grid points (which have length 1)
    p = (glen ./ (size(obj.val,dim)-1)) .* ... % from [0,size(grid,dim)-1] to [0,1]
        (obj.posemax(dim)-obj.posemin(dim));    % from [0,1] to [0,posemax(dim)-posemin(dim)]
        
end

function p = gridind2pose(obj,gind,dim)
    % -1 to get grid distance from first grid point
    p = obj.gridlen2poselen(gind-1,dim) + ... % from [1,size(grid,dim)] to [0,posemax(dim)-posemin(dim)]
        obj.posemin(dim);                     % from [0,posemax(dim)-posemin(dim)] to [posemin(dim),posemax(dim)]
end

function glen = poselen2gridlen(obj,poselen,dim)
    glen = (poselen ./ (obj.posemax(dim)-obj.posemin(dim))) .* ... % from [0,posemax(dim)-posemin(dim)] to [0,1]
           (size(obj.val,dim)-1);                                 % from [0,1] to [0,size(grid,dim)-1]
end

function gind = pose2gridind(obj,poseval,dim)
    gind = round(...
        obj.poselen2gridlen(poseval - obj.posemin(dim),dim) + ... % from [posemin(dim),posemax(dim)] to [0,size(grid,dim)-1]
        1);                                                   % from [0,size(grid,dim)-1] to [1,size(grid,dim)]
end

end

methods(Access=protected)

function p = getSamplePose(obj,dim,coords,poselist,inds)
    if isnan(poselist)
        p = nan;
    elseif isempty(poselist)
        p = obj.gridind2pose(coords(dim,:),dim);
    else
        p = poselist(inds);
    end
end

function p = getSamplePosevar(obj,posevarlist,inds) %#ok<INUSL>
    if isnan(posevarlist)
        p = nan;
    elseif numel(posevarlist) == 1
        p = ones(1,numel(inds)) .* posevarlist;
    else
        p = posevarlist(inds);
    end
end

end

end
