% n: normal, p: point on plane (default: xy-plane)
function m = mouseposOnPlane(ax,n,p)
    if nargin < 2
        n = [0;0;1];
    end
    if nargin < 3
        p = [0;0;0];
    end
    
    mpos = get(ax,'CurrentPoint')';
%     disp(mpos(:,1));
%     disp(mpos(:,2));
    m = linePlaneIntersection(n,p,mpos(:,1),mpos(:,2));
%     disp(m);
end
