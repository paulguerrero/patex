% poses must be: [x;y;size;orientation]
function gobjs = show2DPoses(poses,gobjs,parent,stackz,color,linewidth)
    if numel(gobjs) > 1
        error('Invalid format for handle.')
    end

    if nargin < 4 || isempty(stackz)
        stackz = 0;
    end
    
    if nargin < 5 || isempty(color)
        color = [0;0;1];
    end
    
    if nargin < 6 || isempty(linewidth)
        linewidth = 1;
    end

    if not(iscell(poses))
        if isempty(poses)
            poses = {};
        else
            poses = {poses};
        end
    end
    
    if size(color,2) == 1 && numel(pts) ~= 1
        color = color(:,ones(1,numel(pts)));
    end
    
    if numel(linewidth) == 1 && numel(pts) ~= 1
        linewidth = linewidth(:,ones(1,numel(pts)));
    end
    
    if numel(stackz) == 1 && numel(pts) ~= 1
        stackz = stackz(:,ones(1,numel(pts)));
    end

    if numel(gobjs) ~= numel(poses) || not(all(isgraphics(gobjs(:))))
        delete(gobjs(isgraphics(gobjs)));
        gobjs = gobjects(1,numel(poses));
        
        [~,gobjinds] = sort(stackz,'ascend');
        
        for i=gobjinds
            gobjs(i) = patch([0,0],[0,0],0,...
                'Parent',parent(i),...
                'FaceColor','none',...
                'Visible','off');
            gobjs(i).addprop('Stackz');
            gobjs(i).Stackz = stackz(i);
        end
        
        % update z-stack of all parents
        updateZStack(unique(parent));
        
        % add SetMethod (not earlier so the zstack does not updated
        % repeatedly when creating the objects)
        for i=1:numel(gobjs)
            gobjs(i).findprop('Stackz').SetMethod = @changeStackz;
        end
    end
    
    % show poses as circles (for scale) with an arrow (for direction)
    for j=1:numel(poses)
        
        xydata = zeros(2,0);
        for i=1:size(poses{j},2)
            [cx,cy] = polygonCircle(poses{j}(1,i),poses{j}(2,i),poses{j}(4,i)*0.5,64);
            xydata(:,end+1:end+numel(cx)+2) = [...
                cx([1:end,1]),nan;...
                cy([1:end,1]),nan]; 

            dir = zeros(2,1);
            [dir(1),dir(2)] = pol2cart(poses{j}(3,i),1);
            ahsize = poses{j}(4,i) * 0.4;
            dirsize = poses{j}(4,i);
            ah = arrowhead2D(poses{j}(1:2,i)+dir.*dirsize,dir,ahsize);

            xydata(:,end+1:end+size(ah,2)+4) = [poses{j}(1:2,i),poses{j}(1:2,i)+dir.*dirsize,nan(2,1),ah,nan(2,1)];
        end

        % parent might have changed, but only set when necessary
        if get(gobjs(j),'Parent') ~= parent(j)
            set(gobjs(j),'Parent',parent(j));
        end
        set(gobjs(j),...
            'EdgeColor',color(:,i)',...
            'LineWidth',linewidth(i),...
            'XData',xydata(1,:),...
            'YData',xydata(2,:),...
            'Visible','on');
    end
end
