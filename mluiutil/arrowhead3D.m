function ah = arrowhead3D(p,dir,tipsize)
    ah = [[-tipsize;tipsize/2;0],...
          [0;0;0],...
          [-tipsize;-tipsize/2;0],...
          nan(3,1),...
          [-tipsize;0;tipsize/2],...
          [0;0;0],...
          [-tipsize;0;-tipsize/2]];
    
    % normalize dir
	dir = dir ./ sqrt(sum(dir.^2,1));
    
    % get rotation
    a = dot([1;0;0],dir);
    if 1-abs(a) < 0.00001
        axis = [0;0;1];
    else
        axis = cross(dir,[1;0;0]);
    end
    angle = acos(a);
    rotmat = quat2dcm(angleaxis2quat(angle,axis)')';
    
    % transform arrowhead
    ah = bsxfun(@plus,rotmat * ah,p);
end
