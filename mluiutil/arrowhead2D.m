function ah = arrowhead2D(p,dir,tipsize)
    ah = [-tipsize,0,-tipsize;...
           tipsize/2,0,-tipsize/2];
    [angle,~] = cart2pol(dir(1,:),dir(2,:));
    ah = transform2D(ah,[1;1],angle,p);
end
