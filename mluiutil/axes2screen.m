% screenscale: how much an object at target plane (parallel to screen)
% would be scaled to get to screen size (if DataAspectRatio is [1,1,1]) -
% otherwise it gets complicated
% 
% not very accurate (e.g. borders are not taken into account)
function [screenpos,screenscale] = axes2screen(gnode,axespos)
    
%     modelscaling = 1;
%     model2world = [];
%     if strcmp(ax.Type,'hgtransform')    
%         [model2world,ax] = gmodel2world(ax);
%         modelscaling = det(model2world)^(1/4); % det measures the change in hypervolume, need change in lengths (assume isotropic scaling)
%     end

    [m,~,~,~,camscale,gnode] = axesCameraMatrix(gnode);
    
    axespos(end+1:3,:) = 0;
    axespos(4,:) = 1; % homogeneous coordinate
    
    % computes a coordinate transformation T = [xo,yo,rx,ry] that
    % relates normalized axes coordinates [xa,ya] of point [xo,yo]
    % to its screen coordinate [xs,ys] (in the root units) by:
    %     xs = xo + rx * xa
    %     ys = yo + ry * ya
    % Note: this is a modified internal function within moveptr()
    % Get axes normalized position in figure
    currentunits = gnode.Units;
    gnode.Units = 'normalized';
    T = gnode.Position;
    gnode.Units = currentunits;
%     T = getPos(ax,'normalized');
    % Loop all the way up the hierarchy to the root
    % Note: this fixes a bug in Matlab 7's moveptr implementation
    parent = gnode.Parent;
    while ~isempty(parent)
        % Transform normalized axis coords -> parent coords
        if isequal(parent,groot)
            gr = groot;
            parentPos = gr.ScreenSize;  % Save screen units
        else
            currentunits = parent.Units;
            parent.Units = 'normalized';
            parentPos = parent.Position;
            parent.Units = currentunits;
%             parentPos = getPos(parent, 'normalized'); % Norm units
        end
        T(1:2) = parentPos(1:2) + parentPos(3:4) .* T(1:2);
        T(3:4) = parentPos(3:4) .* T(3:4);
        parent = parent.Parent;
    end
    
%     if not(all(ax.CameraUpVector == [0,1,0]))
%         error('Camera Up Vector must be pointing upwards.');
%     end
    
    % T now contains the pixel position of the axes position rectangle
    
    viewportpos = m*axespos;
    viewportpos = bsxfun(@rdivide,viewportpos,viewportpos(4,:)); % homogenize
    
    % viewportpos is now the position of the point in coordinates that are
    % normalized by cameraViewAngle (points exactly at CameraViewAngle are
    % -1 or 1)
    
    % since the camera view angle seems to coincide with the smallest side
    % of the axes position rectangle
    screenpos = bsxfun(@plus,...
        bsxfun(@times,viewportpos(1:2,:),min(T(3:4))./2),...
        T(1:2)' + T(3:4)'./2);
    
    screenscale = mean(camscale) .* min(T(3:4))./2;
    screenscale = repmat(screenscale,1,size(screenpos,2));
    
%     normaxespos = axes2normaxes(ax,axespos);
%     screenpos = [T(1) + T(3) .* normaxespos(1,:);...
%                  T(2) + T(4) .* normaxespos(2,:)];
end
