% gobjs = showFaces(verts)
% gobjs = showFaces(verts,gobjs)
% gobjs = showFaces(verts,gobjs,parent)
% gobjs = showFaces(verts,gobjs,parent,nvpairs)
% same as showFaces, but each set of vertices is one face
% -------------------------------------------------------------------------
% - verts: cell array with each cell containing a cell array of component
% vertices; or a cell array of patch vertices, or directly the vertics of
% a single patch. Vertices of a patch or component are 2xn or 3xn matrices.
% - gobjs: empty to create new patches, existing patches to update
% - parent: parent of the patches (e.g. axes)
% -------------------------------------------------------------------------
% available name-value parameters:
% - fvcolors: color per component, per vertex or per face
% - fvalpha: alpha per component, per vertex or per face
% - facecolor: single color, color per patch, 'none', 'flat', or 'interp'
% - edgecolor: single color, color per patch, 'none', 'flat', or 'interp'
% - facealpha: single alpha value, alpha per patch, 'none', 'flat' or 'interp'
% - edgealpha: single alpha value, alpha per patch, 'none', 'flat' or 'interp'
% - vnormals: normal per vertex or 'auto' for all patches or per patch
% - fnormals: normal per face or 'auto' for all patches or per patch
% -------------------------------------------------------------------------
% the following reflective properties are only set when creating a new
% patch (not when updating an existing patch):
% - facelighting: 'none', 'flat' or 'gouraud'
% - edgelighting: 'none', 'flat' or 'gouraud'
% - materialprops: (for details see patch properties) struct with
%       AmbientStrength
%       DiffuseStrength
%       SpecularStrength
%       SpecularColorReflectance
%       SpecularExponent
function gobjs = showFaces(verts,gobjs,parent,varargin)
    
    if abs(nargin) < 2
        gobjs = gobjects(1,0);
    end
    if abs(nargin) < 3
        parent = gobjects(1,0);
    end    

    % generate faces (one face per patch or per component)
    if iscell(verts)
        
        % if filled
        faces = cell(1,numel(verts));
        for j=1:numel(verts)
            if iscell(verts{j})
                maxnverts = max(cellfun(@(x) size(x,2),verts{j}));
                faces{j} = cell(1,numel(verts{j}));
                faces{j} = nan(maxnverts,numel(verts{j}));
                for i=1:numel(verts{j})
                    nverts = size(verts{j}{i},2);
                    faces{j}(1:nverts,i) = (1:nverts)';
                end
            else
                faces{j} = (1:size(verts{j},2))';
            end
        end
        
        % otherwise
        
    else
        faces = (1:size(verts,2))';
    end
    
    gobjs = showPatches(verts,faces,gobjs,parent,varargin{:});
end
