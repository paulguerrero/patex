function resetCamera(ax,fit,xlim,ylim,zlim)
    
    if nargin < 2 || isempty(fit)
        fit = 'both';
    end
    
    margin = 1.1;
    if nargin < 3 || isempty(xlim)
        xcenter = (ax.XLim(1) + ax.XLim(2)) / 2;
        xsize = (ax.XLim(2) - ax.XLim(1))/2;
        xlim = [xcenter - xsize*margin, xcenter + xsize*margin];
    end
    if nargin < 4 || isempty(ylim)
        ycenter = (ax.YLim(1) + ax.YLim(2)) / 2;
        ysize = (ax.YLim(2) - ax.YLim(1)) / 2;
        ylim = [ycenter - ysize*margin, ycenter + ysize*margin];
    end
    if nargin < 5 || isempty(zlim)
        zcenter = (ax.ZLim(1) + ax.ZLim(2)) / 2;
        zsize = (ax.ZLim(2) - ax.ZLim(1)) / 2;
        zlim = [zcenter - zsize*margin, zcenter + zsize*margin];
    end

%     viewport = ax.Parent;
    viewport = ax;
    viewportunits = viewport.Units;
    viewport.Units = 'points';
    viewportpos = viewport.Position;
    viewport.Units = viewportunits;
    viewportaspect = viewportpos(3) / viewportpos(4);
    
    limboxaspect = (xlim(2)-xlim(1)) / (ylim(2)-ylim(1));
    
    if strcmp(fit,'y') || (strcmp(fit,'both') && viewportaspect > limboxaspect)
        % camera distance limited by y extent of limits box (limbox will fill viewport in y direction)
        yextent = ylim(2)-ylim(1);
        xextent = yextent * viewportaspect;
    else
        % camera distance limited by x extent of limits box (limbox will fill viewport in x direction)
        xextent = xlim(2)-xlim(1);
        yextent = xextent / viewportaspect;
    end

    camdir = [0;0;1];
    newcamtargetdist = (min(xextent,yextent)*0.5) / (tan(ax.CameraViewAngle*0.5*(pi/180)));
    camtargetpos = (sum([xlim;ylim;zlim],2).*0.5);
    set(ax,...
        'CameraTarget',camtargetpos',...
        'CameraPosition',(camtargetpos+camdir.*newcamtargetdist)',...
        'CameraUpVector',[0,1,0]);
    
    viewport.Units = viewportunits;
end
