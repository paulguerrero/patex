function p = gproperties(varargin)

     p = struct(...
        'stackz',0,...
        'fvcolor',{{}},...
        'fvalpha',{{}},...
        'facecolor',[1;1;0.75],...
        'edgecolor',[0.5;0.5;0.5],...
        'facealpha',1.0,...
        'edgealpha',1.0,...
        'vnormals','auto',...
        'fnormals','auto',...
        'facelighting','flat',...
        'edgelighting','flat',...
        'edgewidth',1,...
        'edgestyle','-',...
        'markersymbol','none',...
        'markeredgecolor','none',...
        'markerfacecolor','none',...
        'markersize',6,...
        'pickableparts','none',...
        'hittest','off',...
        'visible','on',...
        'lightingbugWorkaround','off',...
        'AmbientStrength',0.3,...        
        'DiffuseStrength',0.6,...
        'SpecularStrength',0,...
        'SpecularColorReflectance',1,...
        'SpecularExponent',10);

    if not(isempty(varargin))
        p = nvpairs2struct(varargin,p);
    end

end
