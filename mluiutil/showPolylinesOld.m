function gobjs = showPolylinesOld(polylines,gobjs,parent,stackz,arrowsize,color,linewidth,aheads)
    
    if nargin < 4 || isempty(stackz)
        stackz = 0;
    end
    
    if nargin < 6 || isempty(color)
        color = [0;0;0];
    end

    if nargin < 7 || isempty(linewidth)
        linewidth = 1;
    end

    if nargin < 8 || isempty(aheads)
        aheads = false;
    end
    
    if isempty(polylines)
        polylines = {};
    else
        if not(iscell(polylines))
            polylines = {polylines};
        end
        for i=1:numel(polylines)
            if not(iscell(polylines{i}))
                polylines{i} = matrixsplit(polylines{i},2);
            end
        end
    end
    
    if numel(parent) == 1 && numel(polylines) > 1
        parent = parent(1,ones(1,numel(polylines)));
    end
    
    if not(iscell(color))
        if size(color,2) == 1 || numel(polylines) == 1
            % given as single color for all polylines or
            % as color per verts of the single polyline
            color = {color};
            if numel(color) == 1 && numel(polylines) ~= 1
                % one cell per polyline
                color = color(1,ones(1,numel(polylines)));
            end
        else
            % given as array with one entry per polyline
            color = mat2cell(color,...
                size(color,1),ones(1,size(color,2)));
        end
    end
    
%     if not(iscell(linewidth))
%         if size(linewidth,2) == 1
%             % given as single value for all polylines 
%             linewidth = {linewidth};
%             if numel(linewidth) == 1 && numel(polylines) ~= 1
%                 % one cell per polyline
%                 linewidth = linewidth(1,ones(1,numel(polylines)));
%             end
%         else
%             % given as array with one entry per polyline
%             linewidth = mat2cell(linewidth,...
%                 size(linewidth,1),ones(1,size(linewidth,2)));
%         end
%     end
    
    if numel(linewidth) == 1 && numel(polylines) ~= 1
        linewidth = linewidth(1,ones(1,numel(polylines)));
    end
    
    if numel(stackz) == 1 && numel(polylines) ~= 1
        stackz = stackz(1,ones(1,numel(polylines)));
    end
    
    if aheads
        ahsize = arrowsize/5;
    end
    
    % create new patches if necessary
    if numel(gobjs) ~= numel(polylines) || not(all(isgraphics(gobjs(:))))
        delete(gobjs(isgraphics(gobjs)));
        gobjs = gobjects(1,numel(polylines));
        
        [~,gobjinds] = sort(stackz,'ascend');
        
        for i=gobjinds
            gobjs(i) = patch([0,0],[0,0],0,...
                'Parent',parent(i),...
                'FaceColor','none',...
                'Visible','off');
            gobjs(i).addprop('Stackz');
            gobjs(i).Stackz = stackz(i);
        end
        
        % update z-stack of all parents
        updateZStack(unique(parent));
        
        % add SetMethod (not earlier so the zstack does not updated
        % repeatedly when creating the objects)
        for i=1:numel(gobjs)
            gobjs(i).findprop('Stackz').SetMethod = @changeStackz;
        end
    end
    
    % polylines
    for j=1:numel(polylines)
        xyzdata = zeros(3,0);
        cdata = zeros(3,0);
        
        if size(color{j},2) == 1
            % one color for the entire polyline group
            cdata = color{j};
            colortype = 1;
        elseif size(color{j},2) == numel(polylines{j})
            % one color for each polyline in the group
            colortype = 2;
        elseif size(color{j},2) == size([polylines{j}{:}],2)
            % one color for each vertex
            colortype = 3;
        else
            error('Invalid color specification.');
        end
        
        vertoffset = 0;
        for i=1:numel(polylines{j})
            verts = polylines{j}{i};
            nverts = size(verts,2);
            if size(verts,1) == 2
                verts = [verts;zeros(1,nverts)]; %#ok<AGROW>
            end
            
            xyzdata = [xyzdata,verts,nan(3,1)]; %#ok<AGROW>
            if colortype == 2
                cdata = [cdata,color{j}(:,ones(1,nverts+1).*i)]; %#ok<AGROW>
            elseif colortype == 3
                cdata = [cdata,color{j}(:,[vertoffset+1:vertoffset+nverts,vertoffset+1])]; %#ok<AGROW>
            end
            if aheads
                if nverts > 1
                    dir = verts(:,end) - verts(:,end-1);
                else
                    dir = [1;0;0];
                end
                ah = arrowhead3D(verts(:,end),dir,ahsize);
                xyzdata = [xyzdata,ah,nan(3,1)]; %#ok<AGROW>
                if colortype == 2
                    cdata = [cdata,color{j}(:,ones(1,size(ah,2)+1).*i)]; %#ok<AGROW>
                elseif colortype == 3
                    cdata = [cdata,color{j}(:,ones(1,size(ah,2)+1).*vertoffset+nverts)]; %#ok<AGROW>
                end
            end
            vertoffset = vertoffset + nverts;
        end
        
        % parent might have changed, but only set when necessary
        if get(gobjs(j),'Parent') ~= parent(j)
            set(gobjs(j),'Parent',parent(j));
        end
        
        if colortype == 1
            set(gobjs(j),...
                'XData',xyzdata(1,:),...
                'YData',xyzdata(2,:),...
                'ZData',xyzdata(3,:),...
                'EdgeColor',cdata',...
                'LineWidth',linewidth(j),...
                'Visible','on');
        else
            set(gobjs(j),...
                'XData',xyzdata(1,:),...
                'YData',xyzdata(2,:),...
                'ZData',xyzdata(3,:),...
                'EdgeColor','flat',...
                'FaceVertexCData',cdata',...
                'LineWidth',linewidth(j),...
                'Visible','on');
        end
        
    end
    
end
