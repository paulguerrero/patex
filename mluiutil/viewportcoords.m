% get coordinates of 3D point in viewport
function [vp_pix,vp_norm,vp_ax,vpsize_pix] = viewportcoords(p,ax)
    
    % get camera parameters
    campos = get(ax,'CameraPosition')';
    camtarget = get(ax,'CameraTarget')';
    camviewangle = get(ax,'CameraViewAngle');
    camup = get(ax,'CameraUpVector')';
    camprojectiontype = get(ax,'Projection');
    camdir = camtarget-campos;
    camdist = sqrt(sum(camdir.^2,1));
    camdir = camdir ./ camdist;

    % get size of viewport in pixels
    parent = get(ax,'Parent');
    parentunits = get(parent,'Units');
%     if not(strcmp(get(parent,'Units'),'pixels'))
    set(parent,'Units','pixels');
%     end
    parentpos = get(parent,'Position')';
    vpsize_pix = parentpos(3:4);
    [minvpsize_pix,mindim] = min(vpsize_pix);
    [maxvpsize_pix,maxdim] = max(vpsize_pix);
    
    % get size of viewport in axes units
    vpsize_ax = zeros(2,1);
    vpsize_ax(mindim) = 2*tan((camviewangle*(pi/180))/2)*camdist;
    vpsize_ax(maxdim) = vpsize_ax(mindim) * (maxvpsize_pix / minvpsize_pix);
    
    % get intersection of view vector through points with viewport plane
    if strcmp(camprojectiontype,'orthographic')
        vp = linePlaneIntersection(camdir,camtarget,p,bsxfun(@plus,p,camdir));
    elseif strcmp(camprojectiontype,'perspective')
        vp = linePlaneIntersection(camdir,camtarget,campos,p);
    else
        error('Unkown projection type.');
    end
    
    % get basis vectors of 2D viewport coordinate system
    vporigin_ax = camtarget;
    vpxaxis_ax = cross(camdir,camup);
    vpyaxis_ax = cross(vpxaxis_ax,camdir);
    
    % project to 2D basis and convert to different units
    vp_ax = [vpxaxis_ax';vpyaxis_ax'] * bsxfun(@minus,vp,vporigin_ax); % in axes units
    vp_norm = bsxfun(@plus,bsxfun(@times,vp_ax,1./vpsize_ax),[0.5;0.5]); % normalized
    vp_pix = bsxfun(@times,vp_norm,vpsize_pix); % in pixels
    
    set(parent,'Units',parentunits);
end
