function m = mouseScreenpos
    rootunits = get(0,'Units');
    set(0,'Units','pixels');
    m = get(0,'PointerLocation')';
    set(0,'Units',rootunits);
end
