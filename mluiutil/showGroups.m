function gobjs = showGroups(matrix,gobjs,parent,varargin)

    if abs(nargin) < 2 || isempty(gobjs)
        gobjs = gobjects(1,0);
    end
    if abs(nargin) < 3 || isempty(parent)
        parent = gca;
    end
    
    if not(iscell(matrix))
        matrix = {matrix};
    end
    
    if numel(parent) == 1 && numel(matrix) > 1
        parent = parent(1,ones(1,numel(matrix)));
    end
    
    options = struct(...
        'visible','on');
    options = nvpairs2struct(varargin,options);
    
    if not(iscell(options.visible)) 
        if ischar(options.visible) || size(options.visible,2) == 1
            % one value for all groups
            options.visible = {options.visible};
            if numel(matrix) ~= 1
                options.visible = options.visible(:,ones(1,numel(matrix)));
            end
        else
            % one value per group given as array
            options.visible = mat2cell(options.visible,...
                size(options.visible,1),ones(1,size(options.visible,2)));
        end
    end
    
    % create new gobject placeholders if necessary
    if numel(gobjs) ~= numel(matrix) % || not(all(isgraphics(gobjs(:))))
        delete(gobjs(isvalid(gobjs)));
        gobjs = gobjects(1,numel(matrix));
    end
        
    newgobjinds = find(not(isgraphics(gobjs(:)')));
    if not(isempty(newgobjinds))
        
        % create graphics objects
        for i=newgobjinds
            gobjs(i) = hgtransform(...
                'Parent',parent(i),...
                'Visible','off');
        end
        
    end
    
    [parent,~,gparentind] = unique(parent);
    
    for j=1:numel(matrix) % over all groups
        
        % only pass parameters that have changed
        additionalargs = cell(1,0);
        if parent(gparentind(j)) ~= gobjs(j).Parent
            additionalargs(:,end+1:end+2) = {'Parent',parent(gparentind(j))};
        end
        if numel(matrix{j}) ~= numel(gobjs(j).Matrix) || ...
           any(matrix{j}(:) ~= gobjs(j).Matrix(:))
            additionalargs(:,[end+1,end+2]) = {'Matrix',matrix{j}};
        end
        if numel(options.visible{j}) ~= numel(gobjs(j).Visible) || ...
           any(options.visible{j} ~= gobjs(j).Visible)
            additionalargs(:,[end+1,end+2]) = {'Visible',options.visible{j}};
        end
        
        if not(isempty(additionalargs))
            set(gobjs(j),...
                additionalargs{:});
        end
            
    end
end
