function [normaxespos] = axes2normaxes(ax, axespos)
	xlim = get(ax,'XLim');
    
    if strcmpi(get(ax,'XScale'), 'log')
        normaxespos(1,:) = (log2(axespos(1,:)) - log2(xlim(1))) ./ diff(log2(xlim));
    else
        normaxespos(1,:) = (axespos(1,:)-xlim(1)) / diff(xlim);
    end
    
    ylim = get(ax,'YLim');
	if strcmpi(get(ax,'YScale'), 'log')
        normaxespos(2,:) = (log2(axespos(2,:)) - log2(ylim(1))) ./ diff(log2(ylim));
	else
        normaxespos(2,:) = (axespos(2,:)-ylim(1)) / diff(ylim);
	end
    
end
