function gobjs = showPoints(pts,gobjs,parent,varargin) % stackz,markeredgecolor,markerfacecolor)
    
    if iscell(pts)
        faces = cell(1,numel(pts));
        for i=1:numel(pts)
            faces{i} = (1:size(pts{i},2))';
        end
    else
        faces = (1:size(pts,2))';
    end
    
    if abs(nargin) < 2
        gobjs = gobjects(1,0);
    end
    if abs(nargin) < 3
        parent = gobjects(1,0);
    end
    
    gobjs = showPatches(pts,faces,gobjs,parent,...
        'markersymbol','o',... % default for points
        'facecolor','none',... % default for points
        'edgecolor','none',... % default for points
        'markerfacecolor',[0;0;0],... % default for points
        varargin{:});
end
