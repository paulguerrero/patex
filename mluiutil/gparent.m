% type could be e.g. 'axes','figure' or 'panel'
function gp = gparent(g,type,includeself)
    
    if nargin < 3 || isempty(includeself)
        includeself = false;
    end

    gp = gobjects(1,0);

    if not(includeself)
        if isgraphics(g)
            g = g.Parent; % get direct parent
        else
            return;
        end
    end
    
    while isgraphics(g)
        if strcmp(g.Type,type)
            gp = g;
            break;
        end

        g = g.Parent;
    end
end
