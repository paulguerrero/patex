function menus = figureMenus(fig)
    % create menus struct
    menus = struct;
    for i=1:numel(fig.Children)
        if strcmp(fig.Children(i).Type,'uimenu')
            menus.(fig.Children(i).Label) = fig.Children(i);
        end
    end
end
