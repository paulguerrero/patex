function updateZStack(gobj)
    for i=1:numel(gobj)
        % get z values (all childs without are assumed to have z value 0)
        allchilds = gobj(i).Children;
        allstackz = zeros(1,numel(allchilds));

        % get stackz from all childs (need to do loop because it is a dynamic
        % property)
        haszinds = find(isprop(allchilds,'Stackz'));
        if numel(haszinds) >= 2
            allstackz(haszinds) = cell2mat(get(allchilds(haszinds),'Stackz'));
        else
            allstackz(haszinds) = get(allchilds(haszinds),'Stackz');
        end

        % change ordering of children if they are not already ordered correctly
        if any(diff(allstackz) < 0) 
            [~,perm] = sort(allstackz,'ascend');
            gobj(i).Children = allchilds(perm');
        end
    end
end
