% mapped to a quad
% all values in img must be in [0,1] (including alpha values)
function gobjs = showImages(img,imgmin,imgmax,gobjs,parent,varargin)
    
    if abs(nargin) < 4 || isempty(gobjs)
        gobjs = gobjects(1,0);
    end
    if abs(nargin) < 5 || isempty(parent)
        parent = gca;
    end       

    if not(iscell(img))
        img = {img};
    end
    if not(iscell(imgmin))
        imgmin = {imgmin};
    end
    if not(iscell(imgmax))
        imgmax = {imgmax};
    end
    
    if numel(imgmin) ~= numel(img) || numel(imgmax) ~= numel(img)
        error('Input sizes do not match.');
    end
    
    if numel(parent) == 1 && numel(img) > 1
        parent = parent(1,ones(1,numel(img)));
    end
    
    options = struct(...
        'stackz',0);
    options = nvpairs2struct(varargin,options);
    
    if not(iscell(options.stackz)) 
        if size(options.stackz,2) == 1
            % one value for all patches
            options.stackz = {options.stackz};
            if numel(img) > 1
                options.stackz = options.stackz(:,ones(1,numel(img)));
            end
        else
            % one value per patch given as array
            options.stackz = mat2cell(options.stackz,...
                size(options.stackz,1),ones(1,size(options.stackz,2)));
        end
    end
    
    if numel(imgmin) ~= numel(img) || ...
       numel(imgmax) ~= numel(img) || ...
       numel(options.stackz) ~= numel(img)
        error('Input sizes do not match.');
    end
    
    if numel(gobjs) ~= numel(img) || not(all(isgraphics(gobjs(:))))
        delete(gobjs(isgraphics(gobjs)));
        gobjs = gobjects(1,numel(img));
        
        for i=1:numel(options.stackz)
            if isempty(options.stackz{i})
                options.stackz{i} = 0;
            end
        end
        [~,gobjinds] = sort([options.stackz{:}],'ascend');
        
        for i=gobjinds
            gobjs(i) = surface([0,1],[0,1],ones(2,2).*0.0001,...
                'Parent',parent(i),...
                'FaceColor','texturemap',...
                'FaceAlpha','texture',...
                'EdgeColor','none',...
                'CDataMapping','direct',...
                'AlphaDataMapping','none',...
                'FaceLighting','none',...
                'BackFaceLighting','unlit',...
                'EdgeLighting','none',...
                'HitTest','off',...
                'PickableParts','none',...
                'Visible','off');
            gobjs(i).addprop('Stackz');
            gobjs(i).Stackz = options.stackz{i};
        end
        
        % update z-stack of all parents
        updateZStack(unique(parent));
        
        % add SetMethod (not earlier so the zstack does not updated
        % repeatedly when creating the objects)
        for i=1:numel(gobjs)
            gobjs(i).findprop('Stackz').SetMethod = @changeStackz;
        end
    end
        
    for j=1:numel(img)
        
        additionalargs = cell(1,0);
        xdata = [imgmin{j}(1),imgmax{j}(1)];
        ydata = [imgmin{j}(2),imgmax{j}(2)];
        if numel(imgmin{j}) >= 3
            additionalargs(:,end+1:end+2) = {'ZData',ones(2,2).*imgmin{j}(3)};
        end
        
        % parent might have changed, but only set when necessary
        if parent(j) ~= gobjs(j).Parent
            additionalargs(:,end+1:end+2) = {'Parent',parent(j)};
        end
        
        % imgmin and imgmax are given as minimum and maximum coordinates of the
        % grid points (= pixel centers). In contrast to image(), surface()
        % draws the image so that the minimum pixel corner (not center) is
        % at min and the maximum pixel corner at max. Adjust the min and
        % max given to surface to account for that.
        gridresx = size(img{j},2);
        gridresy = size(img{j},1);
        gridspacing = (imgmax{j}(1:2)-imgmin{j}(1:2)) ./ [gridresx-1;gridresy-1];
        xdata(1) = xdata(1)-gridspacing(1)/2;
        xdata(2) = xdata(2)+gridspacing(1)/2;
        ydata(1) = ydata(1)-gridspacing(2)/2;
        ydata(2) = ydata(2)+gridspacing(2)/2;
        
        if size(img{j},3) == 1 % indexed color
            cmapsize = size(colormap(gparent(gobjs(j),'axes')),1);
            amapsize = numel(alphamap(gparent(gobjs(j),'axes')));
            set(gobjs(j),...
                'XData',xdata,...
                'YData',ydata,...
                'CData',img{j}.*(cmapsize-1)+1,...
                'AlphaData',ones(size(img{j})).*(amapsize-1)+1,...    
                'AlphaDataMapping','direct',...
                additionalargs{:},...
                'Visible','on');
        elseif size(img{j},3) == 2 % indexed color + alpha
            cmapsize = size(colormap(gparent(gobjs(j),'axes')),1);
            amapsize = numel(alphamap(gparent(gobjs(j),'axes')));
            set(gobjs(j),...
                'XData',xdata,...
                'YData',ydata,...
                'CData',img{j}(:,:,1).*(cmapsize-1)+1,...
                'AlphaData',img{j}(:,:,2).*(amapsize-1)+1,...
                'AlphaDataMapping','direct',...
                additionalargs{:},...
                'Visible','on');
        elseif size(img{j},3) == 3 % rgb color
            set(gobjs(j),...
                'XData',xdata,...
                'YData',ydata,...
                'CData',img{j},...
                'AlphaData',ones(size(img{j},1),size(img{j},2)),...
                'AlphaDataMapping','none',...
                additionalargs{:},...
                'Visible','on');
        elseif size(img{j},3) == 4 % rgb color + alpha
            set(gobjs(j),...
                'XData',xdata,...
                'YData',ydata,...
                'CData',img{j}(:,:,1:3),...
                'AlphaData',img{j}(:,:,4),...
                'AlphaDataMapping','none',...
                additionalargs{:},...
                'Visible','on');
        else
            error('Unknown format for image.')
        end
    end
end
