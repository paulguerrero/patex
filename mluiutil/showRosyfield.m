function [gnode,gfield] = showRosyfield(nodes,rfield,gnode,gfield,parent,...
        compasslen,nodecolor,fieldcolor,stackz)
    
    if isempty(nodes) || isempty(rfield)
        return;
    end
    
    if nargin < 9 || isempty(stackz)
        stackz = 0;
    end

    if isempty(gnode) || not(isgraphics(gnode))
        gnode = line(0,0,...
            'Parent',parent,...
            'LineStyle','none',...
            'Marker','o',...
            'MarkerFaceColor',nodecolor,...
            'MarkerEdgeColor','none',...
            'Visible','off');
        gnode.addprop('Stackz');
        gnode.Stackz = stackz;
        
        updateZStack(parent);
        
        gnode.findprop('Stackz').SetMethod = @changeStackz;
    end

    if isempty(gfield) || not(isgraphics(gfield))
        gfield = line(0,0,...
            'Parent',parent,...
            'Color',fieldcolor,...
            'LineWidth',1,...
            'Visible','off');
        gfield.addprop('Stackz');
        gfield.Stackz = stackz-0.001;
        
        updateZStack(parent);
        
        gfield.findprop('Stackz').SetMethod = @changeStackz;
    end

    nrosy = 1;
    rosyangles = ((2*pi)/nrosy).*(0:nrosy-1);
    dirverts = zeros(2,numel(rosyangles));
    [dirverts(1,:),dirverts(2,:)] = pol2cart(rosyangles,ones(1,numel(rosyangles)));
    dirverts = dirverts .* compasslen;
    compassverts = [];
    for i=1:numel(rosyangles)
        compassverts(:,end+(1:2)) = [[0;0],dirverts(:,i)]; %#ok<AGROW>
    end
    compassverts(:,end+1) = [0;0];
    compassverts(:,end+1) = nan; % so everything can be drawn using one line with gaps (faster)

    [angle,len] = cart2pol(rfield(1,:),rfield(2,:));

    len = repmat(len,size(compassverts,2),1);
    len = len(:)';
    angle = repmat(angle,size(compassverts,2),1);
    angle = angle(:)';
    posX = repmat(nodes(1,:),size(compassverts,2),1);
    posX = posX(:)';
    posY = repmat(nodes(2,:),size(compassverts,2),1);
    posY = posY(:)';
    verts = transform2D(repmat(compassverts,1,size(nodes,2)), ...
        [len;len],...
        angle, ...
        [posX;posY]);

    clear len angle posX posY;

    set(gfield,...
        'XData',verts(1,:),...
        'YData',verts(2,:),...
        'Visible','on');
    set(gnode,...
        'XData',nodes(1,:),...
        'YData',nodes(2,:),...
        'Visible','on'); % 'ZData',ones(1,size(nodes,2)).*-1,
end
