% input: hgtransform (or axes)
function [t,g] = gmodel2world(g)
    t = eye(4);
    while not(strcmp(g.Type,'axes'))
        t = g.Matrix * t;
        g = g.Parent;
    end
end
