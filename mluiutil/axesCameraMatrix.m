% world coordinates to normalized view frustum coordinates in
% [-1,-1,0],[1,1,1]
% x,y are -1 or 1 at view angle, or for orthogonal
% projections at the intersection of the view angle with the far plane at
% camera target depth
% z is 0 for camera position an 1 for camera target depth
% camerascale: how much an object at target plane (parallel to screen)
% would be scaled by the camera matrix (if DataAspectRatio is [1,1,1]) -
% otherwise it gets complicated
function [m,proj,world2cam,model2world,camerascale,ax] = axesCameraMatrix(ax)

    % camera view in -z
    % camera up in +y
    % cross(view,up) in +x
    
    if strcmp(ax.Type,'hgtransform')
        [model2world,ax] = gmodel2world(ax);
    else
        model2world = [];
    end
        
    viewdir = (ax.CameraTarget - ax.CameraPosition)';
    viewlen = sqrt(sum(viewdir.^2,1));
    viewdir = viewdir ./ viewlen;
    updir = ax.CameraUpVector' ./ sqrt(sum(ax.CameraUpVector.^2,2));
    rightdir = cross(viewdir,updir);

    world2camrot = eye(4);
    world2camrot(1:3,1:3) = [rightdir';updir';-viewdir'];
    
    world2camtrans = eye(4);
    world2camtrans(1:3,4) = -ax.CameraPosition;
    
    world2camscale = diag([ax.DataAspectRatio,1]);
    
    world2cam = world2camrot * world2camscale * world2camtrans;

    if strcmp(ax.Projection,'orthographic')
        
        viewxyextent = tan(ax.CameraViewAngle*0.5*(pi/180))*viewlen;
        
        proj = eye(4);
        proj(1,1) = 1/viewxyextent;
        proj(2,2) = 1/viewxyextent;
        proj(3,3) = 1/viewlen;

    elseif strcmp(ax.Projection,'projection')
        
        viewxyangle = tan(ax.CameraViewAngle*0.5*(pi/180));
        
        warning('Check that the projection matrix is right.');
        % todo: I don't think this is quite right yet
        
        proj = eye(4);
        proj(1,1) = 1/viewxyangle;
        proj(2,2) = 1/viewxyangle;
        proj(3,3) = 1/viewlen;
        proj(4,3) = 1;
        proj(4,4) = 0;
        
    else
        error('Unknown camera projection.');
    end
    
    m = proj * world2cam;
    camerascale = [proj(1,1);proj(2,2)];
    
    % todo: for perspective projection, camscale is different for different
    % depths
    
    if not(isempty(model2world))
        m = m * model2world;
        % det measures the change in hypervolume, need change in lengths
        % (assume isotropic scaling)
        camerascale = camerascale * det(model2world)^(1/4);
    end
    
end
