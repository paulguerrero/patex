function handle = updateGTransforms(tmats,handle,handleinds,parent,stackz)
    
    if isempty(handleinds)
        handleinds = 1:size(tmats,3);
        
        % remove superflous handles
        removeinds = size(tmats,3)+1:size(handle,2);
        delete(handle(removeinds(isgraphics(handle(:,removeinds)))))
        handle(removeinds) = [];
    end
    
    if nargin < 5
        stackz = [];
    end
    
    if numel(handle) < max(handleinds)
        handle = [handle,gobjects(1,max(handleinds)-numel(handle))];
    end
    
    for i=find(not(isgraphics(handle(handleinds))))
        handle(handleinds(i)) = hgtransform('Parent',parent(i));
        handle(handleinds(i)).addprop('Stackz').SetMethod = @changeStackz;
        if not(isempty(stackz))
            handle(handleinds(i)).Stackz = stackz;
        else
            handle(handleinds(i)).Stackz = 0;
        end
    end
    
    for j=1:numel(handleinds)
        % parent might have changed, but only set when necessary
        if get(handle(handleinds(j)),'Parent') ~= parent(j)
            set(handle(handleinds(j)),'Parent',parent(j));
        end
        
        % stackz might have changed, but only set when necessary
        if not(isempty(stackz))
            if not(isprop(handle(handleinds(j)),'Stackz'))
                handle(handleinds(j)).addprop('Stackz').SetMethod = @changeStackz;
                handle(handleinds(j)).Stackz = stackz;
            end
            if handle(handleinds(j)).Stackz ~= stackz
                handle(handleinds(j)).Stackz = stackz;
            end
        end
        
        set(handle(handleinds(j)),'Matrix',tmats(:,:,j));
    end
end
