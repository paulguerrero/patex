classdef PatternExporter < handle

% todo: similar to OptreeExporter
% also export relationships
    
properties(Access = protected)
    fplanexporter = SceneExporter.empty;
end

methods

function obj = PatternExporter(fplan)
    if nargin == 0
        % do nothing
    elseif nargin >= 1 && nargin <= 2
        obj.rootnodes = rootnodes;
        
        if nargin >= 2
            obj.fplan = fplan;
        end
    else
        error('Invalid arguments.');
    end
end


end

end
