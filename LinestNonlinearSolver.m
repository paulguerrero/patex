classdef LinestNonlinearSolver < StandardPatternSolver
    
properties(SetAccess=protected)
    % last solution
    solutionsubstateinds = cell(1,0);
    solutionactiveconsrelinds = cell(1,0);
    solutionactivefixnodeinds = cell(1,0);
    solutionactiveregnodeinds = cell(1,0);
    solutioncombs = [];
    
    % diagnostic information for paper
    nltime = [];
end

properties(Access=protected)
    visfunc = [];
end

methods(Static)

function s = defaultSettings
	s = StochasticLinearSolver.defaultSettings;
end
    
end

methods

% obj = LinestNonlinearSolver
% obj = LinestNonlinearSolver(obj2)
% obj = LinestNonlinearSolver(pgraph)
function obj = LinestNonlinearSolver(varargin)
    superarginds = zeros(1,0);
    if numel(varargin) == 1 && isa(varargin{1},'PatternGraph')
        superarginds = 1;
    end
    
    obj@StandardPatternSolver(varargin{superarginds});
    varargin(superarginds) = [];
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'LinestNonlinearSolver')
        obj.copyFrom(varargin{1});
    else
        error('Invalid arguments.');
    end
end

function delete(obj) %#ok<INUSD>
    % do nothing
end

function copyFrom(obj,obj2)
    obj.copyFrom@StandardPatternSolver(obj2);
    
    obj.solutionsubstateinds = obj2.solutionsubstateinds;
    obj.solutionactiveconsrelinds = obj2.solutionactiveconsrelinds;
    obj.solutionactivefixnodeinds = obj2.solutionactivefixnodeinds;
    obj.solutionactiveregnodeinds = obj2.solutionactiveregnodeinds;
    obj.solutioncombs = obj2.solutioncombs;
    
    obj.nltime = obj2.nltime;
end

function obj2 = clone(obj)
    obj2 = LinestNonlinearSolver(obj);
end

function clear(obj)
    obj.clear@StandardPatternSolver;
    
    obj.solutionsubstateinds = cell(1,0);
    obj.solutionactiveconsrelinds = cell(1,0);
    obj.solutionactivefixnodeinds = cell(1,0);
    obj.solutionactiveregnodeinds = cell(1,0);
    obj.solutioncombs = [];
end

function setPatterngraph(obj,pgraph)
    obj.setPatterngraph@StandardPatternSolver(pgraph);
end

function prepare(obj,newelmposes,newrelvals)

    obj.prepare@StandardPatternSolver(newelmposes,newrelvals);
    
    obj.solutionsubstateinds = cell(1,0);
    obj.solutionactiveconsrelinds = cell(1,0);
    obj.solutionactivefixnodeinds = cell(1,0);
    obj.solutionactiveregnodeinds = cell(1,0);
    obj.solutioncombs = [];
end

% [selmposes,srelvals,scost] = solve(obj,newelmposes,newrelvals,editedelminds,linsolver)
% [selmposes,srelvals,scost] = solve(obj,newelmposes,newrelvals,editedelminds)
% [selmposes,srelvals,scost] = solve(obj,linsolver)
% [selmposes,srelvals,scost] = solve(obj,linsolver,linsolversolinds)
% [selmposes,srelvals,scost] = solve(...,nvpairs)
function [selmposes,srelvals,scost] = solve(obj,varargin)

    disp('**** non-linear solve ****');
    
    obj.clear;
    
    if numel(varargin) >= 1 && isa(varargin{1},'StochasticLinearSolver')
        % use existing solution
        
        lsolver = varargin{1}.clone;
        varargin(1) = [];
        obj.setPatterngraph(lsolver.pgraph);

        eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        eposes(1:obj.settings.elmposedim,:) = reshape(lsolver.newelmposes,obj.settings.elmposedim,numel(obj.pgraph.elms));
        newelmposes = PatternSolver.sim2affinepose(eposes);
        
        newrelvals = lsolver.newrelvals;

        if isnumeric(varargin{1})
            lsolversolinds = varargin{1};
            varargin(1) = [];
        else
            lsolversolinds = 1:size(lsolver.solutionstate,2);
        end
        
        optionalargs = struct(...
            'fixedelminds',zeros(1,0));
        [~,additionalargmask] = nvpairs2struct(varargin,optionalargs,false,0);
        varargin(additionalargmask) = [];
        
        obj.settings = nvpairs2struct(varargin,obj.settings);
        
    elseif numel(varargin) >= 3 && isnumeric(varargin{1})
        % solve anew
        
        newelmposes = varargin{1};
        newrelvals = varargin{2};
        editedelminds = varargin{3};
        varargin(1:3) = [];
        
        if isempty(newelmposes) || isempty(newrelvals)
            error('Pattern without elements or without relationships.');
        end

        if numel(varargin) >= 1 && isa(varargin{1},'StochasticLinearSolver')
            lsolver = varargin{1}.clone;
            varargin(1) = [];
            obj.setPatterngraph(lsolver.pgraph);
        else
            lsolver = StochasticLinearSolver(obj.pgraph);
        end
        
        optionalargs = struct(...
            'fixedelminds',zeros(1,0));
        [optionalargs,additionalargmask] = nvpairs2struct(varargin,optionalargs,false,0);
        varargin(additionalargmask) = [];
        
        obj.settings = nvpairs2struct(varargin,obj.settings);
        
        linsolversettings = struct2nvpairs(obj.settings);
        linsolveradditionalargs = struct2nvpairs(optionalargs);
        lsolver.setVisfunction(obj.visfunc);
        lsolver.solve(newelmposes,newrelvals,editedelminds,linsolveradditionalargs{:},linsolversettings{:});
        
        lsolversolinds = 1:size(lsolver.solutionstate,2);
    else
        error('Invalid arguments.');
    end
    
    obj.prepare(newelmposes,newrelvals);
    
    obj.solutioncost = zeros(size(lsolver.solutioncost(:,lsolversolinds)));
    obj.solutionstate = zeros(size(lsolver.solutionstate(:,lsolversolinds)));
    obj.solutionstatevar = zeros(size(lsolver.solutionstatevar(:,lsolversolinds)));
    obj.solutionsubstateinds = lsolver.solutionsubstateinds(lsolversolinds);
    obj.solutionactiveconsrelinds = lsolver.solutionactiveconsrelinds(lsolversolinds);
    obj.solutionactivefixnodeinds = lsolver.solutionactivefixnodeinds(lsolversolinds);
    obj.solutionactiveregnodeinds = lsolver.solutionactiveregnodeinds(lsolversolinds);
    obj.solutioncombs = lsolver.solutioncombs(:,lsolversolinds);
    
    for i=1:numel(lsolversolinds)
        
        % pick linear solution as start state or start from scratch
%         startstate = lsolver.solutionstartstate(:,lsolversolinds(i)); % scratch
        startstate = lsolver.solutionstate(:,lsolversolinds(i)); % linear

        startstatevar = lsolver.solutionstatevar(:,lsolversolinds(i));
        substateinds = lsolver.solutionsubstateinds{lsolversolinds(i)};
       
        % define lower and upper bounds for the state
        lb = -inf(size(startstate));
        ub = inf(size(startstate));
        
        % starting close to the lower or upper bound seems to be a bad idea
        % (slow convergence, even if the starting point = solution for this
        % subspace of the domain) and setting the lower/upper bound to 0/1
        % makes matlab think the initial state is out of bounds and set it
        % somewhere betweent the bounds which gives better convergence

        lb(obj.nodefixorig2stateinds) = 0;
        ub(obj.nodefixorig2stateinds) = 1;
        lb(obj.nodefixnew2stateinds) = 0;
        ub(obj.nodefixnew2stateinds) = 1;


        if strcmp(obj.settings.visualize,'all')
            ofun = @(substate,optimValues,solverflag) obj.outfun(...
                startstate,startstatevar,substate,substateinds,optimValues.iteration,optimValues.fval);
        else
            ofun = [];
        end
        
        if any(strcmp(obj.settings.visualize,{'all','result'}))
            obj.outfun(startstate,startstatevar,startstate(substateinds),substateinds);
        end
        
        objfun = @(x) obj.evalobjective_vecnorm(...
            startstate,startstatevar,...
            x,substateinds,...
            obj.solutionactiveconsrelinds{i},obj.solutionactivefixnodeinds{i},obj.solutionactiveregnodeinds{i},...
            obj.settings.consweight,obj.settings.fixweight,obj.settings.regweight,...
            obj.settings.regw_relfixed,obj.settings.regw_elmgrpschanged,obj.settings.regw_relgrpschanged,obj.settings.regw_grpsbroken);
        
        % problem: when doing the manual edits, the groups are broken and
        % when fixing the members of the gropen groups to the original
        % values the different members are fixed to different values =>
        % should not be fixed the original values in the first place?
        % but why are the linear solutions different?
        % => this is an example where the linear approximation is not very
        % accurate, when moving the center rose downwards, the distance
        % between the rose and the leafs decreases fast at first, but the
        % decrease slows down as the rose gets between the leafs (0 when it is exactly in between). 
        % But the linear approximation assumes the decrease is constant,
        % and if it would be, the position in between the leafs would preserve the
        % original distances between leafs and rose
        
        if isempty(substateinds)
            bestsubstate = startstate(substateinds);
            objectiveval = 0;
        else
            if all(isinf(lb(substateinds))) && all(isinf(ub(substateinds)))
                tic;
                [bestsubstate,objectiveval] = fminunc(...
                    objfun,...
                    startstate(substateinds),...
                    optimoptions(@fminunc,...
                    'Algorithm','trust-region',...'quasi-newton',...
                    'MaxFunEvals',100000,... % matlab default is 100*number of variables
                    'MaxIter',30,...
                    'TolFun',1e-4,... % matlab default is 1e-6
                    'TolX',1e-10,... % default for interior point is 1e-10
                    'GradObj','on',...
                    'Diagnostics','off',...
                    'Display','final',...%'off',... 
                    'FunValCheck','off',...
                    'OutputFcn',ofun,...
                    'DerivativeCheck','off'));
                obj.nltime = toc;
                disp(['Elapsed time is ',num2str(obj.nltime), ' seconds.']);
            else
                % Matlab's fmincon
                [bestsubstate,objectiveval] = fmincon(...
                    objfun,...
                    startstate(substateinds),...
                    [],[],[],[],lb(substateinds),ub(substateinds),[],...
                    optimoptions(@fmincon,...
                    'Algorithm','interior-point',...
                    'MaxFunEvals',100000,... % matlab default is 100*number of variables
                    'MaxIter',200,...
                    'TolFun',1e-6,... % matlab default is 1e-6
                    'TolX',1e-10,... % default for interior point is 1e-10
                    'GradObj','on',...
                    'Diagnostics','off',...
                    'Display','final',...%'off',... 
                    'FunValCheck','off',...
                    'OutputFcn',ofun,...
                    'DerivativeCheck','off'));
            end
        end
        
        % store solution
        obj.solutioncost(:,i) = objectiveval;
        obj.solutionstate(:,i) = startstate;
        obj.solutionstate(substateinds,i) = bestsubstate;
        obj.solutionstatevar(:,i) = startstatevar;
        
        if any(strcmp(obj.settings.visualize,{'all','result'}))
            regcostrels = obj.evalobjective_vecnorm(...
                        obj.solutionstate(:,i),obj.solutionstatevar(:,i),...
                        obj.solutionstate(substateinds,i),substateinds,...
                        obj.solutionactiveconsrelinds{i},obj.solutionactivefixnodeinds{i},obj.solutionactiveregnodeinds{i},...
                        0,0,obj.settings.regweight,...
                        0,0,obj.settings.regw_relgrpschanged,0);
            regcostelm = obj.evalobjective_vecnorm(...
                        obj.solutionstate(:,i),obj.solutionstatevar(:,i),...
                        obj.solutionstate(substateinds,i),substateinds,...
                        obj.solutionactiveconsrelinds{i},obj.solutionactivefixnodeinds{i},obj.solutionactiveregnodeinds{i},...
                        0,0,obj.settings.regweight,...
                        0,obj.settings.regw_elmgrpschanged,0,0);
            regcostgrp = obj.evalobjective_vecnorm(...
                        obj.solutionstate(:,i),obj.solutionstatevar(:,i),...
                        obj.solutionstate(substateinds,i),substateinds,...
                        obj.solutionactiveconsrelinds{i},obj.solutionactivefixnodeinds{i},obj.solutionactiveregnodeinds{i},...
                        0,0,obj.settings.regweight,...
                        0,0,0,obj.settings.regw_grpsbroken);
            disp(['reg. (rel./elm./grps.): ',num2str(regcostrels),' / ',num2str(regcostelm),' / ',num2str(regcostgrp)]);
            
            obj.outfun(...
                obj.solutionstate(:,i),...
                obj.solutionstatevar(:,i),...
                obj.solutionstate(substateinds,i),...
                substateinds);
        end
    end
    
    % sort by ascending cost
    [~,perm] = sort(obj.solutioncost,'ascend');
    obj.solutioncombs = obj.solutioncombs(:,perm);
    obj.solutioncost = obj.solutioncost(perm);
    obj.solutionstate = obj.solutionstate(:,perm);
    obj.solutionstatevar = obj.solutionstatevar(:,perm);
    obj.solutionsubstateinds = obj.solutionsubstateinds(perm);
    obj.solutionactiveconsrelinds = obj.solutionactiveconsrelinds(perm);
    obj.solutionactivefixnodeinds = obj.solutionactivefixnodeinds(perm);
    obj.solutionactiveregnodeinds = obj.solutionactiveregnodeinds(perm);

    % get element poses and relationship values from states
    obj.solutionelmposes = cell(1,size(obj.solutionstate,2));
    obj.solutionrelvals = cell(1,size(obj.solutionstate,2));
    for i=1:size(obj.solutionstate,2)
        eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        eposes(1:obj.settings.elmposedim,:) = reshape(obj.solutionstate(obj.elmposestateinds(:),i),obj.settings.elmposedim,numel(obj.pgraph.elms));
        eposes = PatternSolver.sim2affinepose(eposes);
        obj.solutionelmposes{i} = eposes;
        obj.solutionrelvals{i} = obj.solutionstate(obj.relstateinds,i)';
    end

    % set outputs
    selmposes = obj.solutionelmposes;
    srelvals = obj.solutionrelvals;
    scost = obj.solutioncost;

    delete(lsolver(isvalid(lsolver)));
    
    disp('done');
end

function setVisfunction(obj,func)
    obj.visfunc = func;
end

function clearFunctions(obj)
    % needed to save the solver
    obj.visfunc = [];
    obj.relfs = cell(1,0);
end

function stop = outfun(obj,fullstate,fullstatevar,substate,substateinds,iter,fval) %#ok<INUSL>
    state = fullstate;
    state(substateinds) = substate;
    
    if nargin >= 7
        disp(['*** iteration ',num2str(iter),':']);
        disp(['    objective function value: ',num2str(fval)]);
    end
    
    if not(isempty(obj.visfunc))
        elmposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        
        elmposes(1:obj.settings.elmposedim,:) = reshape(state(obj.elmposestateinds),obj.settings.elmposedim,[]);
        
        nodefixorig = state(obj.nodefixorig2stateinds);
        nodefixnew = state(obj.nodefixnew2stateinds);
        
        elmposes = PatternSolver.sim2affinepose(elmposes);
        
        stop = obj.visfunc(...
            obj,...
            elmposes,...
            nodefixorig,...
            nodefixnew);
        
    else
        stop = false;
    end
end

end

end
