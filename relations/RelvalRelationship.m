classdef (Abstract) RelvalRelationship < GeomRelationship
    
methods

% % obj = RelvalRelationship(rval1,rval2,nvpairs)
% % obj = RelvalRelationship(rval1,rvar1,rval2,rvar2,nvpairs)
    
% obj = RelvalRelationship()
% obj = RelvalRelationship(copyobject)
% obj = RelvalRelationship(nvpairs)
% obj = RelvalRelationship(inputs,options,nvpairs)
% obj = RelvalRelationship(rval1,rval2,nvpairs)
% obj = RelvalRelationship(rval1,rvar1,rval2,rvar2,nvpairs)
% obj = RelvalRelationship(value,variance,weight,nvpairs)
function obj = RelvalRelationship(varargin)
    
    if numel(varargin) >= 2 && all(cellfun(@(x) isnumeric(x),varargin(1:2))) && (numel(varargin) == 2 || not(isnumeric(varargin{3})))
        % create 'inputs' argument
        varargin = [{varargin(1:2)},varargin(3:end)];
        options = struct(...
            );
        [options,nvmask] = nvpairs2struct(varargin(2:end),options,false,0,0);
        varargin(nvmask) = [];
        varargin = [varargin(1),{struct2cell(options)},varargin(2:end)];
    elseif numel(varargin) >= 3 && all(cellfun(@(x) isnumeric(x),varargin(1:3))) && (numel(varargin) == 3 || not(isnumeric(varargin{4})))
        % do nothing, arguments are already in correct format
    elseif numel(varargin) >= 4 && all(cellfun(@(x) isnumeric(x),varargin(1:4))) && (numel(varargin) == 4 || not(isnumeric(varargin{5})))
        varargin = [{varargin(1:4)},varargin(5:end)];
        options = struct(...
            );
        [options,nvmask] = nvpairs2struct(varargin(2:end),options,false,0,0);
        varargin(nvmask) = [];
        varargin = [varargin(1),{struct2cell(options)},varargin(2:end)];
    end
    
    obj = obj@GeomRelationship(varargin{:});
end

% % compute(obj,inputs,options,compvar,compweight,compgrad)
% % compute(obj,rval1,rval2,compvar,compweight,compgrad)
% % compute(obj,rval1,rvar1,rval2,rvar2,compvar,compweight,compgrad)

% Input must be n sets of relationship values for n-ary relationships. One
% cell for each set, containing an mx1x2 array, where m is the number of
% output relationship values computed and (:,:,1) is the input value,
% (:,:,2) is the input variance (second dimension is reserved in case we
% want have more values per input relationship and to be consistent with a
% possible update of PoseFeatureRelationship)
% compute(obj,inputs,userelvar,compvar,compweight,compgrad)
function compute(obj,varargin)
    
    if numel(varargin) == 5
        % do nothing
    elseif numel(varargin) == 5
        varargin = [{varargin(1:2)},{cell(0,1)},varargin(3:end)];
    elseif numel(varargin) == 7
        varargin = [{{cat(3,varargin{1},varargin{2}),cat(3,varargin{3},varargin{4})}},{cell(0,1)},varargin(5:end)];
    else
        error('Invalid inputs');
    end
    
    inputs = varargin{1};
    for i=1:numel(inputs)
        if size(inputs{i},3) == 1
            inputs{i} = cat(3,inputs{i},zeros(size(inputs{i})));
        end
        
        if size(inputs{i},2) ~= 1
            error('Invalid input format.');
        end
        if size(inputs{i},3) ~= 2
            error('Invalid input format.');
        end
    end
    varargin{1} = inputs;
    
    obj.compute@GeomRelationship(varargin{:});
end
    
end

methods(Static)

function applicablemask = isapplicable(reltypenames,inputrels)
    
    % get relationship types
    rtypes = GeomRelationshipType.fromName(reltypenames);
    
    if not(iscell(inputrels))
        inputrels = {inputrels};
    end
    if not(isrow(inputrels))
        inputrels = inputrels(:)';
    end
    
    % true if relname i applicable to input relationship j
    applicablemask = true(numel(rtypes),numel(inputrels));
    
    if isempty(rtypes)
        return;
    end
    
    if any(cellfun(@(x) numel(x) > 1,[rtypes.inputtype]))
        error('Multiple applicable input types currently not supported.');
    end
    
    return;
    
    % todo: finish (what should be the applicable categories? E.g. is 'rel'
    % a wildcard for any kind of relationship? how about a relationship
    % only applicable to circular relationships?)
    
    % only RelvalRelationships
    for i=1:numel(rtypes)
        applicablemask(i,:) = any(strcmp(superclasses(rtypes(i).classname),'RelvalRelationship'));
    end
    
    relsym = [rtypes.issymmetric]';
    relnary = [rtypes.nary]';
    
    % get feature type names for all relationship types
    relireltypestr = cell(numel(rtypes),1);
%     allrelinds = 
    for i=1:numel(rtypes)
        if not(any(applicablemask(i,:)))
            relireltypestr{i} = '';
        else
            relireltypestr{i} = [rtypes(i).inputtype{:}];
            
            if relsym(i)
                relireltypestr{i} = sort(relireltypestr{i});
            end
            
            relireltypestr{i} = [relireltypestr{i}{:}];
        end
    end
    
%     [relftypenames,sym] = GeomRelationship.featureTypenames(rtypes,true);
%     relftypenames = relftypenames';
%     sym = sym';
%     nary = cellfun(@(x) numel(x), relftypenames);
%     relftypestr = cellfun(@(x) [x{:}],relftypenames,'UniformOutput',false);
    
    % get feature types of all feature n-tuples
    ftypestr = cell(1,numel(inputrels));
    ftypestr_sorted = cell(1,numel(inputrels));
    ntuple = cellfun(@(x) numel(x),inputrels);
    for f=1:numel(inputrels)
        % remove relations for other feature types
%         ftypenames = SceneFeature.typenames([feature{f}.type]);
        ftypes = SceneFeatureType.fromName({inputrels{f}.type});
        ftypenames = {ftypes.name};
        ftypestr{f} = [ftypenames{:}];

        % sort feature type names before comparing to
        % relation type names of symmetric relations
        % (because order does not matter)
        ftypenames = sort(ftypenames);
        ftypestr_sorted{f} = [ftypenames{:}];
        
        ntuple(f) = numel(inputrels{f});
    end
    
    % find pairs (n-ary reltype,inputrel n-tuple) where n matches
    applicablemask = applicablemask & bsxfun(@eq,relnary,ntuple);
    
    % find pairs (n-ary reltype,inputrel n-tuple) where the relation types
    % match (sorted for symmetric, preserving the order for non-symmetric)
    if any(relsym)
        applicablemask(relsym,:) = applicablemask(relsym,:) & ...
            bsxfun(@(i,j) strcmp(relireltypestr(i),ftypestr_sorted(j)),find(relsym),1:numel(ftypestr_sorted));
    end
    if any(not(relsym))
        applicablemask(not(relsym),:) = applicablemask(not(relsym),:) & ...
            bsxfun(@(i,j) strcmp(relireltypestr(i),ftypestr(j)),find(not(relsym)),1:numel(ftypestr));
    end
end
    
end
    
methods(Static,Abstract)
    [value,variance,weight,gradient] = compute_s(pose,posevar,feature,userelvar);
end

end
