classdef GeomRelationshipLevelset

properties
    % level sets
    position = [];
    postype = []; % (0:open polyline, 1:closed polyline, 2:polygon)
    angle = [];
    scale = [];
    mirrored = []; % (true/false)
    weight = [];
    tolerance = []; % in positional dimension
    angletolerance = []; % in angular dimension
    relname = []; % name of the relationship type
end

methods
    
function obj = GeomRelationshipLevelset(...
        position,postype,angle,scale,mirrored,weight,...
        tolerance,angletolerance,relname)
	
    obj.position = position;
    obj.postype = postype;
    obj.angle = angle;
    obj.scale = scale;
    obj.mirrored = mirrored;
    obj.weight = weight;
    obj.tolerance = tolerance;
    obj.angletolerance = angletolerance;
    obj.relname = relname;
end
    
end

end
