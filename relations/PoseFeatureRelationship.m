classdef (Abstract) PoseFeatureRelationship < GeomRelationship
    
methods

% obj = PoseFeatureRelationship()
% obj = PoseFeatureRelationship(copyobject)
% obj = PoseFeatureRelationship(nvpairs)
% obj = PoseFeatureRelationship(inputs,options,nvpairs)
% obj = PoseFeatureRelationship(pose,feature,nvpairs)
% obj = PoseFeatureRelationship(pose,posevar,feature,nvpairs)
% obj = PoseFeatureRelationship(value,variance,weight,nvpairs)
function obj = PoseFeatureRelationship(varargin)
    
    if numel(varargin) >= 2 && isa(varargin{2},'SceneFeature')
        % create 'inputs' argument
        varargin = [{varargin(1:2)},varargin(3:end)];
        options = struct(...
            'userelvar',[]);
        [options,nvmask] = nvpairs2struct(varargin(2:end),options,false,0,0);
        varargin(nvmask) = [];
        varargin = [varargin(1),{struct2nvpairs(options)},varargin(2:end)];
    elseif numel(varargin) >= 3 && isa(varargin{3},'SceneFeature')
        % create 'inputs' argument
        varargin = [{varargin(1:3)},varargin(4:end)];
        options = struct(...
            'userelvar',[]);
        [options,nvmask] = nvpairs2struct(varargin(2:end),options,false,0,0);
        varargin(nvmask) = [];
        varargin = [varargin(1),{struct2nvpairs(options)},varargin(2:end)];
    end
    
    obj = obj@GeomRelationship(varargin{:});
end
    
% compute(obj,inputs,options,compvar,compweight,compgrad)
% compute(obj,pose,feature,userelvar,compvar,compweight,compgrad)
% compute(obj,pose,posevar,feature,userelvar,compvar,compweight,compgrad)
function compute(obj,varargin)
    
    if numel(varargin) == 5
        % do nothing
    elseif numel(varargin) == 6
        varargin = [{varargin(1:2)},{varargin(3)},varargin(4:end)];
    elseif numel(varargin) == 7
        varargin = [{varargin(1:3)},{varargin(4)},varargin(5:end)];
    else
        error('Invalid inputs');
    end
    
    inputs = varargin{1};
    if numel(inputs) == 2
        inputs = [inputs(1),{zeros(size(inputs{1}))},inputs(2)];
    end
    varargin{1} = inputs;
    
    feature = varargin{1}(3);
    if numel(feature) ~= numel(obj.inputtype)-1 || ...
       not(all(cellfun(@(x,y) isa(x,y),num2cell(feature),obj.inputtype(2:end))))
        error('Invalid type of feature.');
    end
    
    obj.compute@GeomRelationship(varargin{:});
end
    
end

methods(Static)

function applicablemask = isapplicable(reltypenames,feature)
    
    % get relationship types
    rtypes = GeomRelationshipType.fromName(reltypenames);
    
    if any(cellfun(@(x) numel(x) > 1,[rtypes.inputtype]))
        error('Multiple applicable input types currently not supported.');
    end
    
    if not(iscell(feature))
        feature = {feature};
    end
    if not(isrow(feature))
        feature = feature(:)';
    end
    
    % true if relname i applicable to feature j
    applicablemask = true(numel(rtypes),numel(feature));
    
    % only PoseFeatureRelationships
    for i=1:numel(rtypes)
        applicablemask(i,:) = any(strcmp(superclasses(rtypes(i).classname),'PoseFeatureRelationship'));
    end
    
    relsym = [rtypes.issymmetric]';
    relnary = [rtypes.nary]';
    
    % get feature type names for all relationship types
    relftypestr = cell(numel(rtypes),1);
    for i=1:numel(rtypes)
        if not(any(applicablemask(i,:)))
            relftypestr{i} = '';
        else
            relftypestr{i} = [rtypes(i).inputtype{2:end}];
            
            if relsym(i)
                relftypestr{i} = sort(relftypestr{i});
            end
            
            relftypestr{i} = [relftypestr{i}{:}];
        end
    end
    
%     [relftypenames,sym] = GeomRelationship.featureTypenames(rtypes,true);
%     relftypenames = relftypenames';
%     sym = sym';
%     nary = cellfun(@(x) numel(x), relftypenames);
%     relftypestr = cellfun(@(x) [x{:}],relftypenames,'UniformOutput',false);
    
    % get feature types of all feature n-tuples
    ftypestr = cell(1,numel(feature));
    ftypestr_sorted = cell(1,numel(feature));
    ntuple = cellfun(@(x) numel(x),feature);
    for f=1:numel(feature)
        % remove relations for other feature types
%         ftypenames = SceneFeature.typenames([feature{f}.type]);
        ftypes = SceneFeatureType.fromName({feature{f}.type});
        ftypenames = {ftypes.name};
        ftypestr{f} = [ftypenames{:}];

        % sort feature type names before comparing to
        % relation type names of symmetric relations
        % (because order does not matter)
        ftypenames = sort(ftypenames);
        ftypestr_sorted{f} = [ftypenames{:}];
    end
    
    % find pairs ((n-1)-ary reltype,feature n-tuple) where n matches
    applicablemask = applicablemask & bsxfun(@eq,relnary-1,ntuple);
    
    % find pairs ((n-1)-ary reltype,feature n-tuple) where the feature types
    % match (sorted for symmetric, preserving the order for non-symmetric)
    if any(relsym)
        applicablemask(relsym,:) = applicablemask(relsym,:) & ...
            bsxfun(@(i,j) strcmp(relftypestr(i),ftypestr_sorted(j)),find(relsym),1:numel(ftypestr_sorted));
    end
    if any(not(relsym))
        applicablemask(not(relsym),:) = applicablemask(not(relsym),:) & ...
            bsxfun(@(i,j) strcmp(relftypestr(i),ftypestr(j)),find(not(relsym)),1:numel(ftypestr));
    end
end
    
end
    
methods(Static,Abstract)
    [value,variance,weight,gradient] = compute_s(pose,posevar,feature,userelvar);
end

end
