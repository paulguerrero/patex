% Class is abstract, use GeomRelationship.create factory method to create an
% instance of a relationship type.
% Relationship value variance depends on two factors:
% - The uncertainty of absolute placement: how inexact the absolute user
% positioning is, i.e. the error function of absolute placement. The
% user won't place objects pixel-exact and usually might have just as
% well clicked the pixel next to the actual pixel (and even if the
% placement is pixel-exact, the pixels themselves limit the precision).
% - The uncertainty of relative placement: how inexact the user places
% an object relative to another object. This usually depends on the
% type of relation and on the relation value, e.g. when the user places
% an object at a distance from some other object, this distance gets
% more inexact as the distance increases.
% => These uncertainties are modeled as variance of the relationship
% values.

classdef (Abstract) GeomRelationship < handle & matlab.mixin.Heterogeneous

properties
    value = [];
    variance = [];
    weight = [];
    gradient = [];
end

properties(SetAccess=protected)
    type = [];
end

properties(Dependent,SetAccess=protected)
    % the static functions are re-defined as dependent properties here,
    % because otherwise they could not be called on heterogeneous arrays
    inputtype;
    nary;
    iscircular;
    issymmetric;
    isboolean;
end

methods
    
% obj = GeomRelationship()
% obj = GeomRelationship(copyobject)
% obj = GeomRelationship(nvpairs)
% obj = GeomRelationship(inputs,options,nvpairs)
% obj = GeomRelationship(value,variance,weight,nvpairs)
function obj = GeomRelationship(varargin)

%    reltype = GeomRelationshipType.fromClassname(class(obj));
%    obj.type = reltype.name;
    
    obj.type = obj.name_s;
    
    if nargin == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'GeomRelationship')
        obj2 = varargin{1};
        obj.copyFrom(obj2);
    else

        baseoptions = struct(...
            'computevariance',true,...
            'computeweight',true,...
            'computegradient',false);
        [baseoptions,nvmask] = nvpairs2struct(varargin,baseoptions,false,0,0);
        varargin(nvmask) = [];

        if numel(varargin) == 0 
            % do nothing
        elseif numel(varargin) == 2
            obj.compute(varargin{1},varargin{2},...
                baseoptions.computevariance,...
                baseoptions.computeweight,...
                baseoptions.computegradient);
        elseif numel(varargin) == 3
            obj.value = varargin{1};
            obj.variance = varargin{2};
            obj.weight = varargin{3};
        else
            error('Invalid arguments.');
        end
    end
end

function copyFrom(obj,obj2)
    obj.value = obj2.value;
    obj.variance = obj2.variance;
    obj.weight = obj2.weight;
    obj.gradient = obj2.gradient;

    obj.type = obj2.type;
end

function compute(obj,inputs,options,compvar,compweight,compgrad)
    
    if nargin < 4 || isempty(compvar)
        compvar = true;
    end
    if nargin < 5 || isempty(compweight)
        compweight = true;
    end
    if nargin < 6 || isempty(compgrad)
        compgrad = false;
    end

    if compgrad
        [obj.value,obj.variance,obj.weight,obj.gradient] = obj.compute_s(inputs{:},options{:});
    elseif compweight
        [obj.value,obj.variance,obj.weight] = obj.compute_s(inputs{:},options{:});
        obj.gradient = [];
    elseif compvar
        [obj.value,obj.variance] = obj.compute_s(inputs{:},options{:});
        obj.weight = [];
        obj.gradient = [];
    else
        [obj.value] = obj.compute_s(inputs{:},options{:});
        obj.variance = [];
        obj.weight = [];
        obj.gradient = [];
    end
end

% When called on a (possibly heterogeneous) array of objects, the get
% method will not pass the entire array in obj, but rather iteratively
% call the get method for each object in the array. Therefore, when
% calling a static method, the method of the actual class of each
% object is called, not the method of the superclass of the
% heterogeneous array.
function val = get.inputtype(obj)
    val = obj.inputtype_s;
end
function val = get.nary(obj)
    val = numel(obj.inputtype);
end
function val = get.iscircular(obj)
    val = obj.iscircular_s;
end
function val = get.issymmetric(obj)
    val = obj.issymmetric_s;
end
function val = get.isboolean(obj)
    val = obj.isboolean_s;
end

% % indices (ind) of relatinoships with the given names (relnames) in the
% % given set of relationships (obj)
% function inds = relinds(obj,relnames)
%     [~,inds] = ismember(relnames,{obj.relname});
% end

end

methods(Static)
    
% Create geometric relatinoships of the given types.
% elements = create(relnames,nvpairs)
% elements = create(relnames,...,nvpairs) % ... are the constructor inputs
% elements = create(relnames,value,variance,weight,nvpairs)
function rels = create(relnames,varargin)
    if not(iscell(relnames))
        relnames = {relnames};
    end
    
    rels = cell(1,numel(relnames));
    for i=1:numel(relnames)
        rels{i} = GeomRelationshipType.create(relnames{i},varargin{:});
    end
    rels = [rels{:}];
end
    
% function [typenames,issymmetric] = featureTypenames(rtypes,sortsymmetric)
%     
%     if nargin < 2
%         sortsymmetric = false;
%     end
%     
%     if sortsymmetric || nargout >= 2
%         issymmetric = [rtypes.issymmetric];
%     end
%     
%     typenames = cell(1,numel(rtypes));
%     for i=1:numel(rtypes)
%         if any(cellfun(@(x) numel(x),rtypes(i).inputtype) ~= 1)
%             error('Multiple applicable feature types currently not supported.');
%         end
%         typenames{i} = cellfun(@(x) x(1),rtypes(i).inputtype);
%         
%         if sortsymmetric && issymmetric(i)
%             typenames{i} = sort(typenames{i});
%         end
%     end
% end

% function [types,objinds] = groupByType(rels)
%     if isempty(rels)
%         types = zeros(1,0);
%         objinds = cell(1,0);
%     else
%         types = [rels.type]; % can't use class(rels), since it would return the superclass of the heterogeneous array
%         if any(types ~= types(1))
%             [types,~,objtypeinds] = unique(types);
%             objinds = array2cell_mex(1:numel(rels),objtypeinds',numel(types),2);
%         else
%             types = types(1);
%             objinds = {1:numel(rels)};
%         end
%     end
% end
    
end

methods(Sealed)

% need sealing so i can call these methods on heterogenous arrays of
% geometric relationship elements
function b = eq(obj,obj2)
    b = obj.eq@handle(obj2);
end

function b = ne(obj,obj2)
    b = obj.ne@handle(obj2);
end

function b = hasValue(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        o = obj(i);
        b(i) = not(isempty(o.value));
    end
end

end

methods(Static,Abstract)
    ftype = inputtype_s;
    isc = iscircular_s;
    iss = issymmetric_s;
    isf = isboolean_s;
%     
%     isa = isapplicable;
end

end
