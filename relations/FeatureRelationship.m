classdef (Abstract) FeatureRelationship < GeomRelationship
    
methods

% obj = FeatureRelationship()
% obj = FeatureRelationship(copyobject)
% obj = FeatureRelationship(nvpairs)
% obj = FeatureRelationship(inputs,options,nvpairs)
% obj = FeatureRelationship(feat1,feat2,nvpairs)
% obj = FeatureRelationship(value,variance,weight,nvpairs)
function obj = FeatureRelationship(varargin)
    
    if numel(varargin) >= 2 && isa(varargin{1},'SceneFeature')
        % create 'inputs' argument
        varargin = [{varargin(1:2)},varargin(3:end)];
        options = struct(...
            'userelvar',[]);
        [options,nvmask] = nvpairs2struct(varargin(2:end),options,false,0,0);
        varargin(nvmask) = [];
        varargin = [varargin(1),{struct2cell(options)},varargin(2:end)];
    end
    
    obj = obj@GeomRelationship(varargin{:});
end
    
% compute(obj,inputs,options,compvar,compweight,compgrad)
% compute(obj,feat1,feat2,userelvar,compvar,compweight,compgrad)
function compute(obj,varargin)
    
    if numel(varargin) == 5
        % do nothing
    elseif numel(varargin) == 6
        varargin = [{varargin(1:2)},{varargin(3)},varargin(4:end)];
    else
        error('Invalid inputs');
    end
    
    inputs = varargin{1};
    if numel(inputs) ~= numel(obj.inputtype) || ...
       not(all(cellfun(@(x,y) any(strcmp(x.type,y)),inputs,obj.inputtype)))
        error('Invalid input feature types.');
    end
    
    obj.compute@GeomRelationship(varargin{:});
end
    
end

methods(Static)

 
% applicablemask = isapplicable(reltypenames,feature)
function applicablemask = isapplicable(reltypenames,features)
    
    % get relationship types
    rtypes = GeomRelationshipType.fromName(reltypenames);
    
    if not(iscell(features))
        features = {features};
    end
    if not(isrow(features))
        features = features(:)';
    end
    
    % true if relname i applicable to feature j
    applicablemask = true(numel(rtypes),numel(features));
    
    if isempty(rtypes)
        return;
    end
    
%     if any(cellfun(@(x) numel(x) > 1,[rtypes.inputtype]))
%         error('Multiple applicable input types currently not supported.');
%     end
    
    % only FeatureRelationships
    for i=1:numel(rtypes)
        applicablemask(i,:) = any(strcmp(superclasses(rtypes(i).classname),'FeatureRelationship'));
    end
    
    relsym = [rtypes.issymmetric]';
    relnary = [rtypes.nary]';
    
    ntuple = cellfun(@(x) numel(x),features);
    
    % find pairs (reltype,feature tuple) pairs that where the length of the
    % tuple matches the number of inputs of the reltype
    applicablemask = applicablemask & bsxfun(@eq,relnary,ntuple);
    
    % symmetric relationships: all inputs must have the same allowed input
    % types, so find (reltype,feature tuples) pairs where each feature
    % type in the tuple is allowed in the first input
    if any(relsym)
        relsyminds = find(relsym);
        
        % check that all symmetric relationships have equal input types
        relinputftypes = cell(numel(relsyminds),1);
        for r=1:numel(relsyminds)
            if not(any(applicablemask(relsyminds(r),:)))
                relinputftypes{r} = {};
            else
                reltype = rtypes(relsyminds(r));
                if not(all( cellfun(@(x) all(strcmp(x,reltype.inputtype{1})),reltype.inputtype) ))
                    error('In a symmetric relationship, all input types should be equal.');
                end
                relinputftypes{r} = reltype.inputtype{1};
            end
        end
        
        ftypes = cell(1,numel(features));
        for f=1:numel(features)
            ftypes{f} = {features{f}.type};
        end
        
        for i=1:numel(relinputftypes)
            for j=1:numel(ftypes)
                applicablemask(relsyminds(i),j) = applicablemask(relsyminds(i),j) & ...
                    all(ismember(ftypes{j},relinputftypes{i}));
            end
        end
%         applicablemask(relsyminds,:) = applicablemask(relsyminds,:) & ...
%             bsxfun(@(i,j) all(ismember(ftypes{j},relinputftypes{i})),(1:numel(relinputftypes))',1:numel(ftypes));
    end
    
    % non-symmetric relationships: find (reltype,feature tuples) pairs
    % where each feature type in the tuple is allowed in the corresponding
    % input
    if any(not(relsym))
        notrelsyminds = find(not(relsym));
        
        maxrelnary = max(relnary);
        
        relinputftypes = cell(maxrelnary,numel(notrelsyminds));
        relnarymask = false(maxrelnary,numel(notrelsyminds));
        for r=1:numel(notrelsyminds)
            reltype = rtypes(notrelsyminds(r));
            relinputftypes(1:numel(reltype.inputtype),r) = reltype.inputtype;
            relnarymask(1:numel(reltype.inputtype),r) = true;
        end
        
        ftypes = cell(maxrelnary,numel(features));
        fnarymask = false(maxrelnary,numel(features));
        for f=1:numel(features)
            ftypes(1:numel(features{f}),f) = {features{f}.type};
            fnarymask(1:numel(features{f}),f) = true;
        end
        
        % iterate to a=1:maxnary and only compare the reltypes and
        % features tuples that have that at least a inputs/features
        for a=1:numel(maxrelnary)
            rinds = find(relnarymask(a,:));
            finds = find(fnarymask(a,:));
            
            for i=rinds
                for j=finds
                    applicablemask(notrelsyminds(i),j) = applicablemask(relsyminds(i),j) & ...
                        all(ismember(ftypes{a,j},relinputftypes{a,i}));
                end
            end
%             
%             applicablemask(notrelsyminds(rinds),:) = applicablemask(notrelsyminds(rinds),:) & ...
%                 bsxfun(@(i,j) all(ismember(ftypes{a,j},relinputftypes{a,i})),rinds',finds);
        end
    end
    

    
%     for i=1:maxrelnary
%         mask = relnary >= i;
%         
%         if any(relsym)
%             rtypes(i).
%             applicablemask = applicablemask &
%         end
%     end
%     
%     % get feature type names for all relationship types
%     relftypestr = cell(numel(rtypes),1);
%     for i=1:numel(rtypes)
%         if not(any(applicablemask(i,:)))
%             relftypestr{i} = '';
%         else
%             relftypestr{i} = [rtypes(i).inputtype{:}];
%             
%             if relsym(i)
%                 relftypestr{i} = sort(relftypestr{i});
%             end
%             
%             relftypestr{i} = [relftypestr{i}{:}];
%         end
%     end
%     
% %     [relftypenames,sym] = GeomRelationship.featureTypenames(rtypes,true);
% %     relftypenames = relftypenames';
% %     sym = sym';
% %     nary = cellfun(@(x) numel(x), relftypenames);
% %     relftypestr = cellfun(@(x) [x{:}],relftypenames,'UniformOutput',false);
%     
%     % get feature types of all feature n-tuples
%     ftypestr = cell(1,numel(features));
%     ftypestr_sorted = cell(1,numel(features));
%     ntuple = cellfun(@(x) numel(x),features);
%     for f=1:numel(features)
%         % remove relations for other feature types
% %         ftypenames = SceneFeature.typenames([feature{f}.type]);
%         ftypes = SceneFeatureType.fromName({features{f}.type});
%         ftypenames = {ftypes.name};
%         ftypestr{f} = [ftypenames{:}];
% 
%         % sort feature type names before comparing to
%         % relation type names of symmetric relations
%         % (because order does not matter)
%         ftypenames = sort(ftypenames);
%         ftypestr_sorted{f} = [ftypenames{:}];
%     end
%     
% 
%     
%     % find pairs ((n-1)-ary reltype,feature n-tuple) where the feature types
%     % match (sorted for symmetric, preserving the order for non-symmetric)
%     if any(relsym)
%         applicablemask(relsym,:) = applicablemask(relsym,:) & ...
%             bsxfun(@(i,j) strcmp(relftypestr(i),ftypestr_sorted(j)),find(relsym),1:numel(ftypestr_sorted));
%     end
%     if any(not(relsym))
%         applicablemask(not(relsym),:) = applicablemask(not(relsym),:) & ...
%             bsxfun(@(i,j) strcmp(relftypestr(i),ftypestr(j)),find(not(relsym)),1:numel(ftypestr));
%     end
end
    
end
    
methods(Static,Abstract)
    [value,variance,weight,gradient] = compute_s(feat1,feat2,userelvar);
end

end
