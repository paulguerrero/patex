classdef PolygonBoundarydist < PoseFeatureRelationship

methods

function obj = PolygonBoundarydist(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = reltype.PolygonBoundarydist(obj);
end

end

methods(Static)

function n = name_s
    n = 'polygonBoundarydist';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygon'}};
end
function ac = angletype_s
    ac = 'PolygonBoundaryangle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    
    if nargout >= 4
        [d,~,~,cpx,cpy] = pointPolylineDistance_mex(...
                p(1,:),p(2,:),...
                feature.verts(1,[1:end,1]),feature.verts(2,[1:end,1]));

    else
        [d,~,~,~,~] = pointPolylineDistance_mex(...
                p(1,:),p(2,:),...
                feature.verts(1,[1:end,1]),feature.verts(2,[1:end,1]));
    end

    mask = pointInPolygon_mex(...
        p(1,:),p(2,:),...
        feature.verts(1,:),feature.verts(2,:));
    
    val = d;
    val(mask) = -val(mask);
    
    if nargout >= 2
        % relative variance increases as distance gets larger,
        % use std. deviaton of 10 percent of the distance, but keep absolute
        % variance as minimum
        if userelvar
            var = max(sqrt(pvar(1,:)),abs(d).*0.1).^2;
        else
            var = pvar(1,:);
        end
    end
    
    if nargout >= 3
        w = zeros(1,size(p,2));
        if any(mask)
            w(mask) = 1 - max(0,min(1,-val(mask)./(feature.depth)));
        end
        w(not(mask)) = max(0.05,1 - max(0,min(1,val(not(mask))./(feature.radius*2))));
    %     w(not(mask)) = max(0.05,1 - max(0,min(1,val(not(mask))./(feature.radius*5)))); % here
    end

    if nargout >= 4
        cpdir = bsxfun(@minus,p(1:2,:),[cpx;cpy]);
        cpdir = cpdir ./ d;
        cpdir(:,mask) = -cpdir(:,mask); % points inside the polygon
        grad = [...
            cpdir(1,:);...
            cpdir(2,:);...
            zeros(size(val));...
            zeros(size(val))]; % changing the scale does not affect the value of this relationship function
    end
    
end

% levelset in the position and mirrored [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
    lsetpoly = PolygonBoundarydist.boundarydistLevelsetPolygon(feature,relval);

    p = [lsetpoly,lsetpoly];
    ptype = ones(1,numel(p));
%     pm = zeros(1,numel(obj.isodistpoly));
    pm = [zeros(1,numel(lsetpoly));ones(1,numel(lsetpoly))];

    if relval < 0
        pvar = relvar; % approx. gaussian with linear falloff
    else
        pvar = relvar*relval.^2;
    end
    pvar = pvar(ones(1,numel(p)));
end

function val = boundarydistLevelsetPolygon(feature,dist)
    [x,y] = polygonOffset(...
        feature.verts(1,:),feature.verts(2,:),dist,32);

    val = {};
    for i=1:numel(x)
        if not(isempty(x{i})) && not(isempty(x{i}{1}))
            val{end+1} = [x{i}{1};y{i}{1}]; %#ok<AGROW> % ignore holes, only take outer contour
        end
    end
end

end
    
end
