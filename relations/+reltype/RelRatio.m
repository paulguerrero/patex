classdef RelRatio < RelvalRelationship

methods
    
function obj = RelRatio(varargin)
	obj@RelvalRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = reltype.RelRatio(obj);
end

end
    
methods(Static)

function n = name_s
    n = 'relRatio';
end
function ft = inputtype_s
    ft = {{'rel'},{'rel'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = false;
end
function ib = isboolean_s
    ib = false;
end

function [val,var,w,grad] = compute_s(rval1,rval2)
    if nargout == 1
        val = reltype.RelRatio.compute_o(rval1,rval2);
    elseif nargout == 2
        [val,var] = reltype.RelRatio.compute_o(rval1,rval2);
    elseif nargout == 3
        [val,var] = reltype.RelRatio.compute_o(rval1,rval2);
        w = ones(size(val));
    elseif nargout == 4
        [val,var,grad] = reltype.RelRatio.compute_o(rval1,rval2);
        w = ones(size(val));
    end
end

function [val,var,grad] = compute_o(rval1,rval2)

    val = rval1(:,:,1)./rval2(:,:,1);
    
    if nargout >= 2
%         var = (val .* 0.05).^2;
%         var = max((val .* 0.05).^2,0.05.^2);
        var = (rval2(:,:,1) .* 0.1).^2;
    end
    
    if nargout >= 3
        dval_drval1 = 1./rval2(:,:,1);
        dval_drval2 = rval1(:,:,1) ./ -rval2(:,:,2).^2;
        grad = cat(3,dval_drval1,dval_drval2);
    end
end

end
    
end

