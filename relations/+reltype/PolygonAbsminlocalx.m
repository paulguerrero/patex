classdef PolygonAbsminlocalx < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polygonAbsminlocalx';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygon'}};
end
function ac = angletype_s
    ac = 'PolygonLocalxangle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolygonAbsminlocalx(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolygonAbsminlocalx(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    obbox = feature.orientedbbox;
    origin = obbox(:,1);
    xaxisextent = sqrt(sum(obbox(:,2).^2,1));
    xaxis = obbox(:,2) ./ xaxisextent;

    val = xaxis' * bsxfun(@minus,p(1:2,:),origin);

    if nargout >= 2
        % std. deviation proportional to axis extent
    %     var = max(pvar(1,:),(xaxisextent*0.05)^2);
    %     var = max(pvar(1,:),(xaxisextent*0.2)^2);
        if userelvar
    %         var = max((sqrt(pvar(1,:))./xaxisextent).^2,0.2^2); % <= variance of normalized value, not of actual position
            var = max(sqrt(pvar(1,:)).^2,0.05^2); % <= variance of normalized value, not of actual position
        else
            var = sqrt(pvar(1,:)).^2; % <= variance of normalized value, not of actual position
        end
    end
    
%     xaxis_gpu = gpuArray(xaxis);
%     origin_gpu = gpuArray(origin);
%     p_gpu = gpuArray(p);
%     val_gpu = xaxis_gpu' * bsxfun(@minus,p_gpu(1:2,:),origin_gpu);
%     
%     % std. deviation proportional to axis extent
%     pvar_gpu = gpuArray(pvar);
%     xaxisextent_gpu = gpuArray(xaxisextent);
%     var_gpu = max(pvar_gpu(1,:),(xaxisextent_gpu*0.2)^2);
%     
%     val = gather(val_gpu);
%     var = gather(var_gpu);
    
    if nargout >= 3
        w = ones(1,size(p,2)); % todo: less important if somewhere in the center
    end
    
    if nargout >= 4
        grad = [...
            xaxis;...
            zeros(1,size(val,2));...
            zeros(1,size(val,2))];
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
    error('Not yet implemented.');
end

end
    
end
