classdef PolygonLocalxangle < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polygonLocalxangle';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygon'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolygonLocalxangle(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolygonLocalxangle(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar) %#ok<INUSD>
    obbox = feature.orientedbbox;
%     xaxisextent = sqrt(sum(localbbox(:,2).^2,1));
    xaxis = obbox(:,2);
    xaxisangle = cart2pol(xaxis(1),xaxis(2));
    
    val = mod(p(3,:)-xaxisangle + pi,2*pi)-pi;
    
    if nargout >= 2
        % just use absolute variance
        var = pvar(2,:);
    end
    
    if nargout >= 3
        w = ones(1,size(p,2));
    end
    
    if nargout >= 4
        grad = [...
            zeros(1,size(val,2));...
            zeros(1,size(val,2));...
            ones(1,size(val,2));...
            zeros(1,size(val,2))];
    end
end

function [a,avar] = angle_levelset_posmline(feature,relval,relvar,p,ptype,pm) %#ok<INUSL>
    if pm == 1
        relval = sign(relval).*(pi-abs(relval));
    end
    
    if ptype == 0 % open (at segments)
        obbox = feature.orientedbbox;
        xaxis = obbox(:,2);
        xaxisangle = cart2pol(xaxis(1),xaxis(2));

        a = mod(relval+xaxisangle + pi,2*pi)-pi; % just one segment 
        avar = relvar;
    else
        error('Invalid type of levelset.');
    end
end

function a = angle_levelset_posmpoints(feature,localxangle,p,m)
    if m == 1
        localxangle = sign(localxangle).*(pi-abs(localxangle));
    end
    
    obbox = feature.orientedbbox;
    xaxis = obbox(:,2);
    xaxisangle = cart2pol(xaxis(1),xaxis(2));

    a = localxangle+xaxisangle; % just one segment 
    a = mod(a+pi,2*pi)-pi;
    a = a(ones(1,size(p,2)));
end

end
    
end
