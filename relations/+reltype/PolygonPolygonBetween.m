classdef PolygonPolygonBetween < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polygonPolygonBetween';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygon'},{'polygon'}};
end
function ac = angletype_s
    ac = 'PolygonPolygonBetweenangle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s
    is = false;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolygonPolygonBetween(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolygonPolygonBetween(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    
    % distance to first object
    [d1,~,~,cx1,cy1] = pointPolylineDistance_mex(...
            p(1,:),p(2,:),...
            feature(1).verts(1,[1:end,1]),feature(1).verts(2,[1:end,1]));
    val1 = d1;
%     mask = pointInPolygon_mex(...
%         p(1,:),p(2,:),...
%         feature(1).verts(1,:),feature(1).verts(2,:));
    
%     val1(mask) = -val1(mask);
    
    % distance to second object
    [d2,~,~,cx2,cy2] = pointPolylineDistance_mex(...
            p(1,:),p(2,:),...
            feature(2).verts(1,[1:end,1]),feature(2).verts(2,[1:end,1]));
    val2 = d2;        
%     mask = pointInPolygon_mex(...
%         p(1,:),p(2,:),...
%         feature(2).verts(1,:),feature(2).verts(2,:));
    
%     val2(mask) = -val2(mask);
    
%     val = abs(val1-val2);

    val = val1-val2; % temp
    
    directdist = sqrt((cx1-cx2).^2 + (cy1-cy2).^2);
    if directdist > 0
        val = val./directdist;
    else
        val = 0;
    end
    
%     % todo: addtionally compare distance between two closest points sum of
%     % two distances d. val = val * sum(dist) / directdist
%     directdist = sqrt((cx1-cx2).^2 + (cy1-cy2).^2);
%     minratio = 0.9;
%     val = val .* (max(0,directdist ./ (d1+d2) - minratio) ./ (1-minratio));
    % => this is not the weight, it is the value of the relation
    
    if nargout >= 2
        % relative variance increases as distance gets larger,
        % use std. deviaton of 10 percent of the distance, but keep absolute
        % variance as minimum
        if userelvar
            var = max(sqrt(pvar(1,:)),min(abs(val1),abs(val2))*0.3).^2;
        else
            var = pvar(1,:);
        end

        % to bring the variance into function value space (account for scaling
        % of the function value with directdist)
        var = sqrt(var) ./ directdist.^2;
    end
    
    if nargout >= 3
        % maximum weight if exactly between, lowest weight if at boundary
        w = 1 - max(0,min(1,val./(abs(val1)+abs(val2))));
    end
    
    if nargout >= 4
        error('Gradient not yet implemented for this relationship.');
        
%        grad = [...
%            
%            zeros(1,size(val,2));...
%            zeros(1,size(val,2))];
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar) %#ok<STOUT,INUSD>
    error('Levelset not implemented.');
end

end
    
end
