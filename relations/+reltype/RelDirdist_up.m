classdef RelDirdist_up < RelvalRelationship

methods

function obj = RelDirdist_up(varargin)
	obj@RelvalRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = reltype.RelDirdist_up(obj);
end

end

methods(Static)

function n = name_s
    n = 'relDirdist_up';
end
function ft = inputtype_s
    ft = {{'rel'},{'rel'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function [val,var,w,grad] = compute_s(rval1,rval2)

    diff = smod(rval2(:,:,1),-pi,pi) - smod(rval1(:,:,1),-pi,pi);
    val = abs(smod(diff-pi/2,-pi,pi));

    if nargout >= 2
        var = ones(size(val)) .* (pi/16)^2;
    end
    
    if nargout >= 3
        w = ones(size(val));
    end
    
    if nargout >= 4
        warning('Gradient for this relationship function not implemented yet.');
        grad = zeros(4,size(val,2));
        
%         mask = smod(diff,-pi,pi) < 0;
%         
%         grad1 = zeros(size(diff));
%         grad2 = zeros(size(diff));
% 
%         % rval2 smaller than rval1, increase rval1 to increase distance
%         grad1(mask) = 1;
%         grad2(mask) = -1;
% 
%         % rval2 larger than rval1, decrease rval1 to increase distance
%         grad1(not(mask)) = -1;
%         grad2(not(mask)) = 1;
% 
%         grad = cat(3,grad1,grad2);
    end
end

function [val,var,grad] = compute_o(rval1,rval2)

    diff = smod(rval2(:,:,1),-pi,pi) - smod(rval1(:,:,1),-pi,pi);
    val = abs(smod(diff-pi/2,-pi,pi));
    
    if nargout >= 2
        var = ones(size(val)) .* (pi/16)^2;
    end
    
    if nargout >= 3
        warning('Gradient for this relationship function not implemented yet.');
        grad = zeros(4,size(val,2));
        
%         mask = smod(diff,-pi,pi) < 0;
% 
%         grad1 = zeros(size(val));
%         grad2 = zeros(size(val));
%         
%         % rval2 smaller than rval1, increase rval1 to increase distance
%         grad1(mask) = 1;
%         grad2(mask) = -1;
% 
%         % rval2 larger than rval1, decrease rval1 to increase distance
%         grad1(not(mask)) = -1;
%         grad2(not(mask)) = 1;
% 
%         grad = cat(3,grad1,grad2);
    end
end

end
    
end

