classdef PolygonpartCenteraxisangle < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polygonpartCenteraxisangle';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygonpart'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolygonpartCenteraxisangle(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolygonpartCenteraxisangle(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar) %#ok<INUSD>
    
    [~,psegind,~,~,~] = pointPolylineDistance_mex(...
        p(1,:),p(2,:),feature.centeraxis(1,:),feature.centeraxis(2,:));
    dsegvec = feature.centeraxis(:,2:end) - feature.centeraxis(:,1:end-1);
    
    [axisangle,~] = cart2pol(dsegvec(1,psegind),dsegvec(2,psegind));
    val = p(3,:)-axisangle;
    val = mod(val+pi,2*pi)-pi;
    
	if nargout >= 2
        % just use absolute variance
        var = pvar(2,:);
    end
    
    if nargout >= 3
        w = ones(1,size(p,2));
    end
    
    if nargout >= 4
        error('Gradient not yet implemented for this relationship.');
    end
end

function [a,avar] = angle_levelset_posmline(feature,relval,relvar,p,ptype,pm) %#ok<INUSL>
%     caxisangle = obj.value('centeraxisangle');

    if pm == 1
        relval = sign(relval)*(pi-abs(relval));
% 	elseif psym >= 2
%         symcenteraxisangle = sign(centeraxisangle)*(pi-abs(centeraxisangle));
%         symangleoffset = symcenteraxisangle-centeraxisangle;
%         symangleoffset = mod(symangleoffset+pi,2*pi)-pi;
%         psym = 2+max(0,min(1,(symangleoffset+pi)/(2*pi)));
% %         psym = 2+max(0,min(1,(symcenteraxisangle+pi)/(2*pi)));
    end
    
    if ptype(end) == 1 % closed
        dsegvec = p(:,[2:end,1]) - p(:,1:end);
    else % open
        dsegvec = p(:,2:end) - p(:,1:end-1);
    end

    [axisangle,~] = cart2pol(dsegvec(1,:),dsegvec(2,:));
    a = relval+axisangle;
    a = mod(a{end}+pi,2*pi)-pi;
    avar = relvar;
end

function a = angle_levelset_posmpoints(feature,doorareaangle,p,m) %#ok<INUSD>
    error('not implented yet');
end

end
    
end
