classdef PointDist < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'pointDist';
end
function ft = inputtype_s
    ft = {{'pose'},{'point'}};
end
function ac = angletype_s
    ac = 'PointAngle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = PointDist(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PointDist(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
           
    relpos = bsxfun(@minus,p(1:2,:),feature.centroid(1:2,:));
    val = sqrt(sum(relpos.^2,1));
           
    if nargout >= 2
        % relative variance increases as distance gets larger,
        % use std. deviaton of 10 percent of the distance, but keep absolute
        % variance as minimum
        if userelvar
            var = max(sqrt(pvar(1,:)),abs(val).*0.1).^2;
        else
            var = pvar(1,:);
        end
    end
    
    if nargout >= 3
    %     w = max(0,1 - max(0,min(1,d/0.8))); % lmhere
        w = max(0,min(10,1 ./ max(0,min(1,val./0.8))) - 1)./10; % lmhere
    end
    
    if nargout >= 4
        grad = [...
            relpos(1)./val;...
            relpos(2)./val;...
            zeros(1,size(val,2));...
            zeros(1,size(val,2))];
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
    res = 64;
    angles = linspace(0,2*pi,res+1);
    angles(end) = [];
    dists = ones(size(angles)).*abs(relval);
    [p(1,:),p(2,:)] = pol2cart(angles,dists);
    p = [p(1,:) + feature.centroid(1);...
         p(2,:) + feature.centroid(2)];
    ptype = 1;
    pm = 0;
    pvar = relvar;
end

end
    
end
