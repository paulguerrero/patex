classdef PolygonCentroiddir < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polygonCentroiddir';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygon'}};
end
function ac = angletype_s
    ac = 'PolygonCentroidangle';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolygonCentroiddir(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolygonCentroiddir(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
%     if not(obj.hasRelation('boundarydist'))
%         bdist = obj.computeBoundarydist;
%     else
%         bdist = obj.value('boundarydist');
%     end
    
    relpos = bsxfun(@minus,p(1:2,:),feature.center(1:2,:));
    [dirangle,sqdist] = cart2pol(relpos(1,:),relpos(2,:));
    sqdist = sqdist.^2;
%     [dirangle,~] = cart2pol(vec(1),vec(2));
%     val = dirangle-p(3,:);
    val = smod(dirangle-feature.orientation,-pi,pi);
    
    if nargout >= 2
        % absolute variance can get large at small distances to the centroid
    %     sqdist = sum(vec.^2,1);
        var = zeros(1,size(p,2));
        mask = pvar(1,:) >= sqdist;
        var(mask) = (pi/4)^2;
        if not(all(mask))
            var(not(mask)) = min(pi/4, asin(sqrt( pvar(1,not(mask))./sqdist(not(mask)) )) ).^2;
        end
    %     if obj.pointvar >= sqdist;
    %         var = (pi/4)^2;
    %     else
    %         var = min(pi/4,asin(sqrt(obj.pointvar/sqdist)))^2;
    %     end
        
        % relative variance limits the exactness of the placement as the
        % distance to the centroid increases (variance remains constant in the
        % angular domain)
        if userelvar
            var = max(var,(pi/16)^2);
        end
    end
    
    if nargout >=3
        w = zeros(1,size(p,2));
        mask = pointInPolygon_mex(...
            p(1,:),p(2,:),...
            feature.verts(1,:),feature.verts(2,:));
        mask = not(mask);
        if any(mask)
            [bdistd,~,~,~,~] = pointPolylineDistance_mex(...
                    p(1,mask),p(2,mask),...
                    feature.verts(1,[1:end,1]),feature.verts(2,[1:end,1]));
            w(mask) = max(0,min(10,1 ./ max(0,min(1,bdistd/0.8))) - 1)./10; % lmhere
    %         w(mask) = max(0,min(2,1 ./ max(0,min(1,bdistd/(feature.radius*2)))))./1; % lmhere
            maxdist = feature.radius*2;
            w(mask) = 1 - min(1, max(0,bdistd)./maxdist );
        end
    end
    
    if nargout >= 4
        grad = [...
            -relpos(2,:) ./ sqdist;... % solution to d(atan2(p(2)-featpos(2),p(1)-featpos(1))) / d(p(1))
            relpos(1,:) ./ sqdist;... % solution to d(atan2(p(2)-featpos(2),p(1)-featpos(1))) / d(p(2))
            zeros(size(val));...  % angle has no influence on relationship function value
            zeros(size(val))]; % changing the scale does not affect the value of this relationship function
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
    dirangle = relval+feature.orientation;
    dirangle = mod(dirangle+pi,2*pi)-pi;
    
    [dirx,diry] = pol2cart(dirangle,1);
    dir = [dirx;diry];
%     len = feature.radius*2;
    len = feature.radius*3; % lmhere original
%     len = 0.8; % lmhere
    
    p = {[feature.center,feature.center+ dir .* len]};
    ptype = 0;
    pm = 0;
    pvar = 0.01; % = 0.25 tolerance
    
    if feature.symmetries(1) || feature.symmetries(2)
        [anglex,angley] = pol2cart(feature.orientation,1);
        angledir = [anglex;angley];
        angleorthdir = [-angley;anglex];
        diry = dot(dir,angledir);
        dirx = dot(dir,angleorthdir);

        if feature.symmetries(1)
            % symmetry around the axis parallel to the angle direction
            symdir = -dirx.*angleorthdir + diry.*angledir;
            p = [p,[feature.center,feature.center+symdir .* len]];
            ptype(end+1) = 0;
            pm(end+1) = 1;
            pvar(end+1) = 0.01; % = 0.25 tolerance
        end
        if feature.symmetries(2)
            % symmetry around the axis orthogonal to the angle direction
            symdir = dirx.*angleorthdir - diry.*angledir;
            p = [p,[feature.center,feature.center+symdir .* len]];
            ptype(end+1) = 0;
            pm(end+1) = 1;
            pvar(end+1) = 0.01; % = 0.25 tolerance
        end
        if feature.symmetries(1) && feature.symmetries(2)
            % both symmetries
            symdir = -dirx.*angleorthdir - diry.*angledir;
            p = [p,[feature.center,feature.center+symdir .* len]];
            ptype(end+1) = 0;
            pm(end+1) = 0;
            pvar(end+1) = 0.01; % = 0.25 tolerance
        end
    end
    
    % Real tolerance should change depending on the position on the level
    % set. Currently not possible => use constant tolerance
end

end
    
end
