classdef SegmentArclen < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'segmentArclen';
end
function ft = inputtype_s
    ft = {{'pose'},{'segment'}};
end
function ac = angletype_s
    ac = 'SegmentArclenangle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = SegmentArclen(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = SegmentArclen(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar) %#ok<INUSD>
%     if isempty(obj.cornerextent)
%         obj.cornerextent = obj.computeCornerextent;
%     end
    segverts = feature.vertices;
    segarclen = feature.arclen;

    [d,si,f,~,~] = pointPolylineDistance_mex(...
            p(1,:),p(2,:),...
            segverts(1,:),segverts(2,:));
        
    val = (segarclen(si).*(1-f) + segarclen(si+1).*f) ./ segarclen(end);
    
    if nargout >= 2
        % just use normalized absolute variance
        var = pvar(1,:) ./ segarclen(end)^2;
    end
    
    if nargout >= 3
        w = 1-max(0,min(1,d ./ (feature.parentelement.depth*0.75)));
        
        w = min(1,w.*1.2); % constant 1 near the border
        
        % todo: when outside range, set arclen to 'distance projected to
        % first/last edge in the segment = segment si' / total arclength, so
        % the values continue smoothly and don't stop at 0 or 1
        
    %     w = w * (1-max(0,min(1,abs(val-0.5)*2)));
        % also less towards the ends of the segment    
        % function that is 0 at the ends of the segment and 1 at the center and
        % a falloff from 1 that starts ~= at 0.4/0.6 and becomes zero at 0.1/0.9
    %     w = w * max(0,min(1,((1-(abs(val-0.5).*2)).*1.66-0.33)));
        % 1 everywhere except a short falloff to 0 at the edges
        w = w .* max(0,min(1,((1-(abs(val-0.5).*2)).*100)));
        % todo: cornercell diameter as maximum distance here
    %     w = 0; % temp
    end
    
    if nargout >= 4
        tangentvec = bsxfun(@times,segverts(:,si+1)-segverts(:,si),ones(size(val)));
        tangentvec = tangentvec ./ (segarclen(si+1) - segarclen(si)); % normalize
        grad = [...
            tangentvec(1) / segarclen(end);... % since it is the normalized arclength
            tangentvec(2) / segarclen(end);... % since it is the normalized arclength
            zeros(size(val));...  % angle has no influence on relationship function value
            zeros(size(val))]; % changing the scale does not affect the value of this relationship function
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)

%     segmentarclen = obj.value('arclen');
    
    relval = max(0,min(1,relval)); % clamp rel value, might be < 0 or > 1 because of expectation maximization learning

    maxextent = feature.parentelement.depth*0.75;

    segverts = feature.vertices;
    segarclen = feature.arclen;
    
    
    % first part of the isoline
    abssegarclen = relval * segarclen(end);
    
    si = find(segarclen <= abssegarclen,1,'last');
    if si==numel(segarclen)
        si = si-1;
    end
    f = (abssegarclen - segarclen(si)) / (segarclen(si+1)-segarclen(si));
    
    segpos = segverts(:,si).*(1-f) + segverts(:,si+1).*f;
    
    isodir = segverts(:,si+1) - segverts(:,si);
    isodir = [-isodir(2);isodir(1)];
    isodir = isodir ./ sqrt(sum(isodir.^2,1));
    
    p = {[segpos - isodir .* 0.00001, segpos - isodir .* maxextent],...
         [segpos + isodir .* 0.00001, segpos + isodir .* maxextent]};
    
    
    % second part of the isoline at the other end of the segment
    abssegarclen = (1-relval) * segarclen(end);
    
    si = find(segarclen <= abssegarclen,1,'last');
    if si==numel(segarclen)
        si = si-1;
    end
    f = (abssegarclen - segarclen(si)) / (segarclen(si+1)-segarclen(si));
    
    segpos = segverts(:,si).*(1-f) + segverts(:,si+1).*f;
    
    isodir = segverts(:,si+1) - segverts(:,si);
    isodir = [-isodir(2);isodir(1)];
    isodir = isodir ./ sqrt(sum(isodir.^2,1));
    
    p = [p,[segpos - isodir .* 0.00001, segpos - isodir .* maxextent],...
           [segpos + isodir .* 0.00001, segpos + isodir .* maxextent]];
    % small offset to avoid numeric issues if the segment starts
    % exactly at the border
    
    % faster: store max. length per arclength (like length of inset paths)
    % intersect with parent skeleton to determine max. length of level sets
    skeleton = feature.parentelement.skeleton;
    skeledges =  skeleton.edges(:,not(skeleton.prunededgemask));
    skeletonsegs = [skeleton.verts(:,skeledges(1,:));...
                    skeleton.verts(:,skeledges(2,:))];
    psegs = cell2mat(cellfun(@(x) x(:),p,'UniformOutput',false));
    [intersects,ip,~,t1] = linesegLinesegIntersection2D_mex(...
        psegs(1:2,:),psegs(3:4,:),...
        skeletonsegs(1:2,:),skeletonsegs(3:4,:));
    for i=1:numel(p)
        intersectinds = find(intersects(i,:)>0);
        if not(isempty(intersectinds))
%             [~,minind] = min(intersects.intNormalizedDistance1To2(i,intersectinds));
            [~,minind] = min(t1(i,intersectinds));
            ix = ip(i,intersectinds(minind),1);
            iy = ip(i,intersectinds(minind),2);
%             ix = intersects.intMatrixX(i,intersectinds(minind));
%             iy = intersects.intMatrixY(i,intersectinds(minind));
            
            p{i}(:,2) = [ix;iy];
        end
    end
    
    ptype = [0,0,0,0];
    pm = [0,1,1,0];
    pvar = relvar * feature.arclen(end)^2;
    pvar = pvar(ones(1,4));
%     psym = [2,2,2,2]; % temp
    
    % todo: max until intersection with real medial axis (primary segments
    % only - store in object)
end

end
    
end
