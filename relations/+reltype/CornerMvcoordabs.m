classdef CornerMvcoordabs < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'cornerMvcoordabs';
end
function ft = inputtype_s
    ft = {{'pose'},{'corner'}};
end
function ac = angletype_s
    ac = 'CornerAngle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = CornerMvcoordabs(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = CornerMvcoordabs(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    
%     c = feature.parentelement.center;
    v1 = feature.previouscornervertex.pos;    
    v2 = feature.pos;
    v3 = feature.nextcornervertex.pos;
    
    pv1 = v1-p;
    pv2 = v2-p;
    pv3 = v3-p;
    
    pv1l = sqrt(sum(pv1.^2,1));
    pv2l = sqrt(sum(pv2.^2,1));
    pv3l = sqrt(sum(pv3.^2,1));
    
    % normalize
    pv1 = bsxfun(@rdivide,pv1,pv1l);
    pv2 = bsxfun(@rdivide,pv2,pv2l);
    pv3 = bsxfun(@rdivide,pv3,pv3l);
    
    % unnormalized mean value coordinates
    val = (tan(acos(sum(pv1.*pv2,1))./2) + tan(acos(sum(pv2.*pv3,1))./2)) ./ pv2l;
    
    if nargout >= 2
        % relative variance increases as distance gets larger,
        % use std. deviaton of 10 percent of the distance, but keep absolute
        % variance as minimum
        if userelvar
            var = max(sqrt(pvar(1,:)),abs(d).*0.1).^2;
        else
            var = pvar(1,:);
        end
    end
    
    if nargout >= 3
        w = ones(1,size(p,2));
        
%         if any(mask)
%             w(mask) = 1 - max(0,min(1,-val(mask)./(feature.depth)));
%         end
%         w(not(mask)) = max(0.05,1 - max(0,min(1,val(not(mask))./(feature.size*2))));
    %     w(not(mask)) = max(0.05,1 - max(0,min(1,val(not(mask))./(feature.size*5)))); % here
    end

    if nargout >= 4
        error('Gradient not implemented for this relationship type.');
    end
    
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
    error('Levelset not implemented for this relationship type.');
end

end
    
end
