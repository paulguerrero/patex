classdef PolygonPolygonOrientationdist < FeatureRelationship

methods

function obj = PolygonPolygonOrientationdist(varargin)
	obj@FeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = reltype.PolygonPolygonOrientationdist(obj);
end

end
    
methods(Static)

function n = name_s
    n = 'polygonPolygonOrientationdist';
end
function ft = inputtype_s
    ft = {{'polygon','polygonset'},{'polygon','polygonset'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function [val,var,w,grad] = compute_s(feat1,feat2,userelvar)
    f1pos = PatternSolver.affine2simpose([feat1.pose2D]);
    f2pos = PatternSolver.affine2simpose([feat2.pose2D]);
    
    if nargout == 1
        val = reltype.PolygonPolygonOrientationdist.compute_o(f1pos,f2pos);
    elseif nargout == 2
        [val,var] = reltype.PolygonPolygonOrientationdist.compute_o(f1pos,f2pos);
    elseif nargout == 3
        [val,var] = reltype.PolygonPolygonOrientationdist.compute_o(f1pos,f2pos);
        w = ones(size(val));
    elseif nargout == 4
        [val,var,grad] = reltype.PolygonPolygonOrientationdist.compute_o(f1pos,f2pos);
        w = ones(size(val));
    end
end

% input poses are similarity transformation poses
% additional required properties of the feature can be given in the 3rd and
% 4th argument
function [val,var,grad] = compute_o(f1pose,f2pose,f1props,f2props)
    
    diff = smod( f1pose(3,:) - f2pose(3,:) ,-pi,pi);
    val = abs(diff);
    
    if nargout >= 2
        var = ones(size(val)) .* (pi/16)^2;
    end
    
    if nargout >= 3
        mask = diff >= 0;
        dval_df1rot(mask) = 1;
        dval_df1rot(not(mask)) = -1;
        
        % on the vector of input poses of both features
        grad = cat(3,...
            [zeros(2,size(val,2));dval_df1rot;zeros(size(f1pose,1)-3,size(val,2))],...
            [zeros(2,size(val,2));-dval_df1rot;zeros(size(f1pose,1)-3,size(val,2))]);
    end
end

end
    
end
