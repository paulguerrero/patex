classdef PolygonAbsmaxlocaly < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polygonAbsmaxlocaly';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygon'}};
end
function ac = angletype_s
    ac = 'PolygonLocalyangle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolygonAbsmaxlocaly(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolygonAbsmaxlocaly(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    obbox = feature.orientedbbox;
    origin = obbox(:,1) + obbox(:,3);
    yaxisextent = sqrt(sum(obbox(:,3).^2,1));
    yaxis = obbox(:,3) ./ yaxisextent; % squared so projected values are in [0..1]
    
    val = yaxis' * bsxfun(@minus,p(1:2,:),origin);
    
    if nargout >= 2
        % std. deviation proportional to axis extent
        if userelvar
            var = max(sqrt(pvar(1,:)).^2,0.05^2);
        else
            var = sqrt(pvar(1,:)).^2;
        end
    end
    
    if nargout >= 3
        w = ones(1,size(p,2)); % todo: less important if somewhere in the center
    end
    
    if nargout >= 4
        grad = [...
            yaxis;...
            zeros(1,size(val,2));...
            zeros(1,size(val,2))];
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
    error('Not yet implemented.');
end

end
    
end
