classdef RelDirdist < RelvalRelationship

methods

function obj = RelDirdist(varargin)
	obj@RelvalRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = reltype.RelDirdist(obj);
end

end

methods(Static)

function n = name_s
    n = 'relDirdist';
end
function ft = inputtype_s
    ft = {{'rel'},{'rel'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = false; % distance is not circular (only difference is)
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function [val,var,w,grad] = compute_s(rval1,rval2)
    if nargout == 1
        val = reltype.RelDirdist.compute_o(rval1,rval2);
    elseif nargout == 2
        [val,var] = reltype.RelDirdist.compute_o(rval1,rval2);
    elseif nargout == 3
        [val,var] = reltype.RelDirdist.compute_o(rval1,rval2);
        w = ones(size(val));
    elseif nargout == 4
        [val,var,grad] = reltype.RelDirdist.compute_o(rval1,rval2);
        w = ones(size(val));
    end
end

function [val,var,grad] = compute_o(rval1,rval2)

    diff = smod(rval1(:,:,1)-rval2(:,:,1),-pi,pi);
    val = abs(diff);
    
    if nargout >= 2
        var = ones(size(val)) .* (pi/16)^2;
%         var = ones(size(val)) .* (pi/8)^2;
    end
    
    if nargout >= 3
        mask = diff >= 0;
        dval_drval1(mask) = 1;
        dval_drval1(not(mask)) = -1;
        
        % dval_drval2 = -dval_drval1
        grad = cat(3,dval_drval1,-dval_drval1);
    end
end

end
    
end

