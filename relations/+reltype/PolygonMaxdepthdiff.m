classdef PolygonMaxdepthdiff < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polygonMaxdepthdiff';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygon'}};
end
function ac = angletype_s
    ac = 'PolygonBoundaryangle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolygonMaxdepthdiff(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolygonMaxdepthdiff(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    [d,~,~,~,~] = pointPolylineDistance_mex(...
            p(1,:),p(2,:),...
            feature.verts(1,[1:end,1]),...
            feature.verts(2,[1:end,1]));

    mask = pointInPolygon_mex(...
        p(1,:),p(2,:),...
        feature.verts(1,:),feature.verts(2,:));
    
    d(mask) = -d(mask);
        
%         if inpoly(obj.point',feature.verts')
%         d = -d;
%     end
%     val = max(0,feature.depth + d);
    
    absval = max(0,feature.depth + d); 
    val = absval./feature.depth; % normalized
    
    if nargout >= 2
        % assuming the user knows where the maximum depth is, the relative
        % variance increases away from the maximum depth (use std. deviation of
        % 10 percent of the distance to maximum depth), but keep absolute
        % variance as minimum
        if userelvar
            var = (max(sqrt(pvar(1,:)),absval.*0.1)./feature.depth).^2;
        else
            var = (sqrt(pvar(1,:))./feature.depth).^2;
        end
    end
    
    if nargout >= 3
        w = max(0,min(1,(feature.depth - val) ./ feature.depth));
    end
    
    if nargout >= 4
        error('Gradient not yet implemented for this relationship.');
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
    bdist = -feature.depth + relval*feature.depth; 

    lsetpoly = PolygonBoundarydist.boundarydistLevelsetPolygon(feature,bdist);
    
    p = lsetpoly;
    ptype = ones(1,numel(lsetpoly));
    pm = zeros(1,numel(lsetpoly));
    pvar = relvar * feature.depth^2;
    pvar = pvar(ones(1,numel(lsetpoly)));
end

end
    
end
