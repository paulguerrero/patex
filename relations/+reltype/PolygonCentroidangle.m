classdef PolygonCentroidangle < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polygonCentroidangle';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygon'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolygonCentroidangle(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolygonCentroidangle(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar) %#ok<INUSD>
    relpos = bsxfun(@minus,p(1:2,:),feature.center(1:2,:));
    [dirangle,dist] = cart2pol(relpos(1),relpos(2));
    
    val = smod(p(3,:)-dirangle,-pi,pi);
    
    if nargout >= 2
        % just use absolute variance
        var = pvar(2,:);
    end
    
    if nargout >= 3
        w = ones(1,size(p,2));
    end
    
    if nargout >= 4
        distsq = dist.^2;
        grad = [...
            relpos(2) ./ distsq;... % solution to d(atan2(p(2)-centroid(2),p(1)-centroid(1))) / d(p(1))
            -relpos(1) ./ distsq;... % solution to d(atan2(p(2)-centroid(2),p(1)-centroid(1))) / d(p(2))
            ones(size(val));...  % angle has 1:1 influence on relationship function value => dval / d(p(3)) = 1
            zeros(size(val))]; % changing the scale does not affect the value of this relationship function
    end
end

function [a,avar] = angle_levelset_posmline(feature,relval,relvar,p,ptype,pm)
%     centroidangle = obj.value('centroidangle');
    
    if pm == 1
        relval = -relval;
%     elseif pm >= 2
%         symcentroidangle = -centroidangle;
%         symangleoffset = symcentroidangle-centroidangle;
%         symangleoffset = mod(symangleoffset+pi,2*pi)-pi;
%         pm = 2+max(0,min(1,(symangleoffset+pi)/(2*pi)));
% %         pm = 2+max(0,min(1,(symcentroidangle+pi)/(2*pi)));
    end
    
    if ptype == 1 % closed (at segments)
        dirs = p-feature.center(:,ones(1,size(p,2)));
        dirs = dirs + dirs(:,[2:end,1]); % to get dirs at segment centers
    elseif ptype == 0 % open (at segments)
        dirs = p-feature.center(:,ones(1,size(p,2)));
        dirs = dirs(:,1:end-1) + dirs(:,2:end); % to get dirs at segment centers
    else % filled (at vertices)
        dirs = p-feature.center(:,ones(1,size(p,2)));
    end
    
    [dirangles,~] = cart2pol(dirs(1,:),dirs(2,:));
    a = dirangles+relval;
    a = mod(a+pi,2*pi)-pi;
    avar = relvar;
end

function a = angle_levelset_posmpoints(feature,centroidangle,p,m)
    if m == 1
        centroidangle = -centroidangle;
    end
    
    [dirangle,~] = cart2pol(...
        p(1,:)-feature.center(1),...
        p(2,:)-feature.center(2));
    
    a = centroidangle+dirangle;
    a = mod(a+pi,2*pi)-pi;
end

end
    
end
