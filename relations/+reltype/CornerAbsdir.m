classdef CornerAbsdir < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'cornerAbsdir';
end
function ft = inputtype_s
    ft = {{'pose'},{'corner'}};
end
function ac = angletype_s
    ac = 'CornerAngle';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = CornerAbsdir(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = CornerAbsdir(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    relpos = bsxfun(@minus,p(1:2,:),feature.pos);
    [val,dist] = cart2pol(relpos(1,:),relpos(2,:));    
    
    if nargout >= 2
        % absolute variance can get large at small distances to the centroid
        var = zeros(1,size(p,2));
        sqdist = dist.^2;
        mask = pvar(1,:) >= sqdist;
        var(mask) = (pi/4)^2;
        if not(all(mask))
            var(not(mask)) = min(pi/4,asin(sqrt(pvar(1,not(mask))./sqdist(not(mask))))).^2;
        end
        
        % relative variance limits the exactness of the placement as the
        % distance to the centroid increases (variance remains constant in the
        % angular domain)
        if userelvar
            var = max(var,(pi/16)^2);
        end
    end
    
    if nargout >= 3
        maxlength = 2;  % max. 2 meters
        cornerextent = min(maxlength,feature.extent);
        w = (1-max(0,min(1,dist ./ (cornerextent*2))));
    end
    
    if nargout >= 4
        distsq = dist.^2;
        grad = [...
            -relpos(2) ./ distsq;... % solution to d(atan2(p(2)-featpos(2),p(1)-featpos(1))) / d(p(1))
            relpos(1) ./ distsq;... % solution to d(atan2(p(2)-featpos(2),p(1)-featpos(1))) / d(p(2))
            zeros(size(val));...  % angle has no influence on relationship function value
            zeros(size(val))]; % changing the scale does not affect the value of this relationship function
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar) %#ok<INUSD>
    [dirx,diry] = pol2cart(relval,1);
    dir = [dirx;diry];
    
    maxlength = 2;  % max. 2 meters
    cornerextent = min(maxlength,feature.extent);
    
    p = [feature.pos,feature.pos + dir .* cornerextent];
    p = {p,p}; % mirrored is the same because angle is absolute
    ptype = [0,0];
    pm = [0,1];
    pvar = [0.01,0.01]; % = 0.25 tolerance
end

end
    
end
