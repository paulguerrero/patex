classdef CornerReldir < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'cornerReldir';
end
function ft = inputtype_s
    ft = {{'pose'},{'corner'}};
end
function ac = angletype_s
    ac = 'CornerAngle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = CornerReldir(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = CornerReldir(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    dir = bsxfun(@minus,p(1:2,:),feature.pos(1:2,:));
    [dirangle,cdist] = cart2pol(dir(1,:),dir(2,:));
    edge1angle = cart2pol(feature.edge1(1),feature.edge1(2));
    edge2angle = cart2pol(feature.edge2(1),feature.edge2(2));
    
    dirangle = mod(dirangle - edge2angle,2*pi);
    edge1angle = mod(edge1angle - edge2angle,2*pi);
    
    val = zeros(1,size(p,2));
    mask = dirangle < edge1angle;
    
    % point is outside polygon
    val(mask) = -(dirangle(mask) ./ edge1angle);
    
    % point is inside polygon
    dirangle(not(mask)) = 2*pi - dirangle(not(mask));
    val(not(mask)) = dirangle(not(mask)) ./ (2*pi-edge1angle);
    
    if nargout >= 2
        % absolute variance can get large at small distances to the centroid
        var = zeros(1,size(p,2));
        sqdist = cdist.^2;
        mask2 = pvar(1,:) >= sqdist;
        var(mask2) = (pi/4)^2;
        if not(all(mask2))
            var(not(mask2)) = min(pi/4,asin(sqrt(pvar(1,not(mask2))./sqdist(not(mask2))))).^2;
        end
        
        % relative variance limits the exactness of the placement as the
        % distance to the centroid increases (variance remains constant in the
        % angular domain)
        if userelvar
            var = max(var,(pi/16)^2);
        end
        
        % point is outside polygon
        var(mask) = var(mask) ./ edge1angle^2;
        
        % point is inside polygon
        var(not(mask)) = var(not(mask)) ./ (2*pi-edge1angle)^2;
    end
    
    if nargout >= 3
        maxlength = 2;  % max. 2 meters
        cornerextent = min(maxlength,feature.extent);
        w = (1-max(0,min(1,cdist ./ (cornerextent*2)))) .* (1 - abs(abs(val)-0.5).*2); % last term difference to angle exactly between two corner edges
    end
    
    if nargout >= 4
        error('Gradient not yet implemented for this relationship.');
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar) %#ok<INUSD>
    
% 	if isempty(obj.cornerextent)
%         obj.cornerextent = obj.computeCornerextent;
%     end
    
    maxlength = 2;  % max. 2 meters
    cornerextent = min(maxlength,feature.extent); % neworig
%     cornerextent = min(maxlength,feature.extent)*2; % newhere
    
%     cornerreldir = obj.value('reldir');
    
    edge1angle = cart2pol(feature.edge1(1),feature.edge1(2));
    edge2angle = cart2pol(feature.edge2(1),feature.edge2(2));
    
    edge1angle = mod(edge1angle - edge2angle,2*pi);

    if relval < 0
        % angle outside polygon
        dirangle = -relval * edge1angle;
    else
        % angle inside polygon
        dirangle = relval * (2*pi - edge1angle);
        dirangle = 2*pi-dirangle;
    end
    dirangle = mod(dirangle + edge2angle,2*pi);
    if dirangle > pi
        dirangle = dirangle-2*pi;
    end
    [dirx,diry] = pol2cart(dirangle,1);
    dir = [dirx;diry];
    p = [feature.pos,feature.pos + dir .* cornerextent];
    ptype = 0;
    pm = 0;
    pvar = 0.01; % = 0.25 tolerance
    
    % same thing with mirrored angle
    if relval < 0
        % angle outside polygon
        dirangle = (-1-relval) * edge1angle;
    else
        % angle inside polygon
        dirangle = (1-relval) * (2*pi - edge1angle);
        dirangle = 2*pi-dirangle;
    end
    dirangle = mod(dirangle + edge2angle,2*pi);
    if dirangle > pi
        dirangle = dirangle-2*pi;
    end
    [dirx,diry] = pol2cart(dirangle,1);
    dir = [dirx;diry];
    
    p = {p,[feature.pos,feature.pos + dir .* cornerextent]};
    ptype = [ptype,0];
    pm = [pm,1];
    pvar = [pvar,0.01]; % = 0.25 tolerance
    
    % todo: second point of p at intersection with corner cell
end

end
    
end
