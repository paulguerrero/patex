classdef PolygonPolygonBetweenangle < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polygonPolygonBetweenangle';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygon'},{'polygon'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s
    is = false;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolygonPolygonBetweenangle(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolygonPolygonBetweenangle(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar) %#ok<INUSD>

    refangle = PolygonPolygonBetweenangle.computeRefangle(feature,p);
    
    val = p(3,:)-refangle;
    val = mod(val+pi,2*pi)-pi;

    if nargout >= 2
        % just use absolute variance
        var = pvar(2,:);
    end
    
    if nargout >= 3
        w = ones(1,size(p,2));
    end
    
    if nargout >= 4
        error('Gradient not yet implemented for this relationship.');
    end
end

function [a,avar] = angle_levelset_posmline(feature,relval,relvar,p,ptype,pm) %#ok<STOUT,INUSD>
    error('Levelset not implemented.');
end

% angle for pose p that is missing the angle that results in the given
% relationship value to the given features (or is at least as similar as
% possible)
function a = angle_levelset_posmpoints(feature,boundaryangle,p,m)
    % this results in the values of the angle levelset at the given
    % positions
    refangle = PolygonPolygonBetweenangle.computeRefangle(feature,p);
    
    if m == 1
        boundaryangle = sign(boundaryangle)*(pi-abs(boundaryangle));
    end
    
    a = boundaryangle+refangle;
    a = mod(a+pi,2*pi)-pi;
end

function refangle = computeRefangle(feature,p)
    % hulls must be cw!
    % reference frame (rf) for one object is always aligned so that 0 is
    % the boundary direction for a clockwise boundary
    % reference frame for the pair of objects is mean(rf1,-rf2)
    
    % feature 1
    % compute the segment of the point p on the dilated polygon
    [~,psegind,f,cpx,cpy] = pointPolylineDistance_mex(...
        p(1,:),p(2,:),...
        feature(1).verts(1,[1:end,1]),...
        feature(1).verts(2,[1:end,1]));
    refangle1 = zeros(1,size(p,2));
    % todo: (also in computeAngle) instead of pointInPolygon, make dot
    % product with segment, should be > 0 (also on corners - 0 means
    % degenerate 180 degrees corner - has to be outside, turn 90 deg. clockwise)
    %
    % if closest point is corner, rotate direction to closest point by 90
    % degrees so it points in clockwise boundary direction (so turn angle
    % to closest point 90 degrees clockwise (negative) if point is inside
    % polygon and counterclockwise (positive) if outside polygon)
    mask = f == 0 | f == 1;
    if any(mask)
        maskinds = find(mask);
        in = pointInPolygon_mex(p(1,mask),p(2,mask),...
            feature(1).verts(1,:),feature(1).verts(2,:));
        refangle1(:,maskinds) = cart2pol(...
            cpx(maskinds) - p(1,maskinds),...
            cpy(maskinds) - p(2,maskinds));
        refangle1(maskinds(in)) = mod(refangle1(maskinds(in))-pi/2+pi,2*pi)-pi;
        refangle1(maskinds(not(in))) = mod(refangle1(maskinds(not(in)))+pi/2+pi,2*pi)-pi;
    end
    % if closest point is on a line segment, use direction of line segment
    % (hull must be guaranteed to be clockwise)
    if not(all(mask))
        dsegvec = feature(1).verts(:,[2:end,1]) - ...
                  feature(1).verts;
        refangle1(not(mask)) = cart2pol(...
            dsegvec(1,psegind(not(mask))),dsegvec(2,psegind(not(mask))));
    end
    
    % feature 2
    % compute the segment of the point p on the dilated polygon
    [~,psegind,f,cpx,cpy] = pointPolylineDistance_mex(...
        p(1,:),p(2,:),...
        feature(1).verts(1,[1:end,1]),...
        feature(1).verts(2,[1:end,1]));
    refangle2 = zeros(1,size(p,2));
    % todo: (also in computeAngle) instead of pointInPolygon, make dot
    % product with segment, should be > 0 (also on corners - 0 means
    % degenerate 180 degrees corner - has to be outside, turn 90 deg. clockwise)
    %
    % if closest point is corner, rotate direction to closest point by 90
    % degrees so it points in clockwise boundary direction (so turn angle
    % to closest point 90 degrees clockwise (negative) if point is inside
    % polygon and counterclockwise (positive) if outside polygon)
    mask = f == 0 | f == 1;
    if any(mask)
        maskinds = find(mask);
        in = pointInPolygon_mex(p(1,mask),p(2,mask),...
            feature(1).verts(1,:),feature(1).verts(2,:));
        refangle2(:,maskinds) = cart2pol(...
            cpx(maskinds) - p(1,maskinds),...
            cpy(maskinds) - p(2,maskinds));
        refangle2(maskinds(in)) = mod(refangle2(maskinds(in))-pi/2+pi,2*pi)-pi;
        refangle2(maskinds(not(in))) = mod(refangle2(maskinds(not(in)))+pi/2+pi,2*pi)-pi;
    end
    % if closest point is on a line segment, use direction of line segment
    % (hull must be guaranteed to be clockwise)
    if not(all(mask))
        dsegvec = feature(1).verts(:,[2:end,1]) - ...
                  feature(1).verts;
        refangle2(not(mask)) = cart2pol(...
            dsegvec(1,psegind(not(mask))),dsegvec(2,psegind(not(mask))));
    end
    
    % refangle is mean of dir(refangle1) and -dir(refangle2)
    [r1x,r1y] = pol2cart(refangle1,ones(size(refangle1)));
    [r2x,r2y] = pol2cart(refangle2,ones(size(refangle2)));
    refangle = cart2pol((r1x-r2x)./2,(r1y-r2y)./2);
end

end
    
end
