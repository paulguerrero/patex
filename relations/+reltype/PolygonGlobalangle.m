classdef PolygonGlobalangle < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polygonGlobalangle';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygon'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s
    is = false;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolygonGlobalangle(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolygonGlobalangle(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar) %#ok<INUSD>

    val = p(3,:); % function value = angle

    if nargout >= 2
        % just use absolute variance
        var = pvar(2,:);
    end
    
    if nargout >= 3
        w = ones(1,size(p,2));
    end
    
    if nargout >= 4
        grad = [...
            zeros(1,size(val,2));...
            zeros(1,size(val,2));...
            ones(1,size(val,2));...
            zeros(1,size(val,2))];
    end
end

function [a,avar] = angle_levelset_posmline(feature,relval,relvar,p,ptype,pm) %#ok<STOUT,INUSD>
    error('Levelset not implemented.');
end

% angle for pose p that is missing the angle that results in the given
% relationship value to the given features (or is at least as similar as
% possible)
function a = angle_levelset_posmpoints(feature,globalangle,p,m) %#ok<INUSD,INUSL>
    a = globalangle;
end

end
    
end
