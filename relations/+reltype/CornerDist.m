classdef CornerDist < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'cornerDist';
end
function ft = inputtype_s
    ft = {{'pose'},{'corner'}};
end
function ac = angletype_s
    ac = 'CornerAngle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = CornerDist(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = CornerDist(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    relpos = bsxfun(@minus,p(1:2,:),feature.pos(1:2,:));
    val = sqrt(sum(relpos.^2,1));
    
    if nargout >= 2
        % relative variance increases as distance gets larger,
        % use std. deviaton of 10 percent of the distance, but keep absolute
        % variance as minimum
        if userelvar
            var = max(sqrt(pvar(1,:)),abs(val).*0.1).^2;
        else
            var = pvar(1,:);
        end
    end
    
    if nargout >= 3
        maxlength = 2;  % max. 2 meters
        cornerextent = min(maxlength,feature.extent);
        w = 1-max(0,min(1,val ./ cornerextent)); % neworig
%         w = 1-max(0,min(1,val ./ (cornerextent.*2))); % newhere
        % todo: cornercell diameter as maximum distance here
%         w = 0; % temp
    end
    
    if nargout >= 4
        grad = [...
            relpos(1)./val;...
            relpos(2)./val;...
            zeros(1,size(val,2));...
            zeros(1,size(val,2))];
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)

%     cornerdist = obj.value('dist');

    res = 64;
    angles = linspace(0,2*pi,res+1);
    angles(end) = [];
    dists = ones(size(angles)).*relval;
    [circlex,circley] = pol2cart(angles,dists);    

    p = [circlex+feature.pos(1);circley+feature.pos(2)];
    p = {p,p};
    ptype = [1,1];
    pm = [0,1];
    pvar = [relvar,relvar];
end

end
    
end
