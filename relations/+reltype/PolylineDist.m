classdef PolylineDist < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polylineDist';
end
function ft = inputtype_s
    ft = {{'pose'},{'polyline'}};
end
function ac = angletype_s
    ac = 'PolylineAngle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolylineDist(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolylineDist(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    if nargout >= 4
        [val,~,~,cpx,cpy] = pointPolylineDistance_mex(...
                p(1,:),p(2,:),...
                feature.verts(1,:),feature.verts(2,:));
    else
        [val,~,~,~,~] = pointPolylineDistance_mex(...
                p(1,:),p(2,:),...
                feature.verts(1,:),feature.verts(2,:));
    end
    
    if nargout >= 2
        % relative variance increases as distance gets larger,
        % use std. deviaton of 10 percent of the distance, but keep absolute
        % variance as minimum
        if userelvar
            var = max(sqrt(pvar(1,:)),abs(val).*0.1).^2;
        else
            var = pvar(1,:);
        end
    end
    
	if nargout >= 3
        w = max(0,1 - max(0,min(1,val))); 
    end
    
    if nargout >= 4
        cpdir = bsxfun(@minus,p(1:2,:),[cpx;cpy]);
        cpdir = cpdir ./ sqrt(sum(cpdir.^2,1));
        grad = [...
            cpdir(1);... % gradient is normalized direction from closest point
            cpdir(2);... % gradient is normalized direction from closest point
            zeros(size(val));...  % changing the angle does not affect the value of this relationship function
            zeros(size(val))]; % changing the scale does not affect the value of this relationship function
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
%     bdist = obj.value('dist');
	
    lsetpoly = PolylineDist.plinedistLevelsetPolygon(feature,relval);

    p = [lsetpoly,lsetpoly];
    ptype = ones(1,numel(p));
    pm = [zeros(1,numel(lsetpoly));ones(1,numel(lsetpoly))];
    pvar = relvar(ones(1,numel(p)));
end

function val = plinedistLevelsetPolygon(feature,dist)
    
    [x,y] = polylineOffset(...
        feature.verts(1,:),feature.verts(2,:),dist);
    
    val = {};
    for i=1:numel(x)
        if not(isempty(x{i})) && not(isempty(x{i}{1}))
            val{end+1} = [x{i}{1};y{i}{1}]; %#ok<AGROW> % ignore holes, only take outer contour
        end
    end
end

end
    
end
