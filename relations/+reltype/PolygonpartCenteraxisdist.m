classdef PolygonpartCenteraxisdist < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polygonpartCenteraxisdist';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygonpart'}};
end
function ac = angletype_s
    ac = 'PolygonpartCenteraxisangle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolygonpartCenteraxisdist(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolygonpartCenteraxisdist(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)

    [val,si,~,ix,iy] = pointPolylineDistance_mex(...
        p(1,:),p(2,:),feature.centeraxis(1,:),feature.centeraxis(2,:));
    % check if p is on left or right side of closest segment of center axis
    centeraxisorth = feature.centeraxis(:,si+1)-feature.centeraxis(:,si);
    centeraxisorth = [-centeraxisorth(2,:);centeraxisorth(1,:)];

    mask = sum(centeraxisorth.*(p-[ix;iy]),1)<0;
    val(mask) = -val(mask);
%     if dot(centeraxisorth,obj.point-[ix;iy])<0
%         d = -d; % negative if on right side
%     end
%     val = d;
    
    if nargout >= 2
        % relative variance increases as distance gets larger,
        % use std. deviaton of 10 percent of the distance, but keep absolute
        % variance as minimum
        if userelvar
            var = max(sqrt(pvar(1,:)),abs(val).*0.1).^2;
        else
            var = pvar(1,:);
        end
    end
    
    if nargout >= 3
        w = ones(1,size(p,2));
    end
    
    if nargout >= 4
        error('Gradient not yet implemented for this relationship.');
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
    error('not implented yet');
end

end
    
end
