classdef EdgeDistangle < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'edgeDistangle';
end
function ft = inputtype_s
    ft = {{'pose'},{'edge'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = EdgeDistangle(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = EdgeDistangle(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    
    % compute the segment of the point p on the dilated polygon
    [dist,~,f,cpx,cpy] = pointPolylineDistance_mex(...
        p(1,:),p(2,:),...
        feature.vertices(1,:),...
        feature.vertices(2,:));

    relpos = [p(1,:) - cpx; p(2,:) - cpy];
    
    % reference frame for angle is always aligned so that 0 is the boundary
    % direction for a clockwise boundary
    
    % rotate direction to closest point by 90 degrees so it points in
    % clockwise boundary direction (so turn angle to closest point 90
    % degrees  counterclockwise (positive) - point is always on the outside)
    refangle = cart2pol(-relpos(1),-relpos(2));
    refangle = smod(refangle+pi/2,-pi,pi);
    
    val = p(3,:)-refangle;
    val = mod(val+pi,2*pi)-pi;

    if nargout >= 2
        % just use absolute variance
        var = pvar(2,:);
    end
    
    if nargout >= 3
        w = ones(1,size(p,2));
    end
    
    if nargout >= 4
        % since segments are piecewise linear, the gradient
        % of the relative angle is only non-zero
        % if the closest point is a vertex of the segment
        mask = f == 0 | f == 1;
        
        if any(mask)
            distsq = dist.^2;
            dx(mask) = relpos(2,mask) ./ distsq(mask);... % solution to d(atan2(p(2)-cpy,p(1)-cpx)) / d(p(1))
            dy = -relpos(1,mask) ./ distsq(mask);... % solution to d(atan2(p(2)-cpy,p(1)-cpx)) / d(p(2))            
        end
        
        nmask = not(mask);
        if any(nmask)
            dx(nmask) = 0;
            dy(nmask) = 0;
        end
       
        grad = [...
            dx;...
            dy;...
            ones(1,size(val,2));...
            zeros(1,size(val,2))];
    end
end

function [a,avar] = angle_levelset_posmline(feature,relval,relvar,p,ptype,pm) %#ok<INUSL>
%     segangle = obj.value('distangle');

    if pm == 1
        relval = sign(relval)*(pi-abs(relval));
    end
    
    if ptype == 1 % closed
        dsegvec = p(:,[2:end,1]) - p(:,1:end);
    else % open
        dsegvec = p(:,2:end) - p(:,1:end-1);    
    end

    [axisangle,~] = cart2pol(dsegvec(1,:),dsegvec(2,:));
    a = relval+axisangle;
    a = mod(a+pi,2*pi)-pi;
    avar = relvar;
end

function a = angle_levelset_posmpoints(feature,segdistangle,p,m)
    % this results in the values of the angle levelset at the given
    % positions
    
    if m == 1
        segdistangle = sign(segdistangle)*(pi-abs(segdistangle));
    end
    
    % todo: buffer the closest points?
    % or compute in cuda
    
    % compute the segment of the point p on the dilated polygon
    [~,~,~,cpx,cpy] = pointPolylineDistance_mex(...
        p(1,:),p(2,:),...
        feature.vertices(1,:),...
        feature.vertices(2,:));
    
    % or do distance transform if the p are guaranteed to be in a regular
    % grid

    % reference frame for angle is always aligned so that 0 is the boundary
    % direction for a clockwise boundary
    
    % rotate direction to closest point by 90 degrees so it points in
    % clockwise boundary direction (so turn angle to closest point 90
    % degrees  counterclockwise (positive) - point is always on the
    % outside)
    refangle = cart2pol(...
        cpx - p(1,:),...
        cpy - p(2,:));
    refangle = mod(refangle+pi/2+pi,2*pi)-pi;
    
    a = segdistangle+refangle;
    a = mod(a+pi,2*pi)-pi;

%     % just use absolute variance
%     var = pvar(2,:);
%     
%     w = ones(1,size(p,2));
end

end
    
end
