classdef PointDir < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'pointDir';
end
function ft = inputtype_s
    ft = {{'pose'},{'point'}};
end
function ac = angletype_s
    ac = 'PointAngle';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = PointDir(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PointDir(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    relpos = bsxfun(@minus,p(1:2,:),feature.position(1:2,:));
    [dirangle,dist] = cart2pol(relpos(1,:),relpos(2,:));
    val = smod(dirangle-feature.orientation,-pi,pi);
    
    if nargout >= 2
        % absolute variance can get large at small distances to the point
        var = zeros(1,size(p,2));
        sqdist = dist.^2;
        mask = pvar(1,:) >= sqdist;
        var(mask) = (pi/4)^2;
        var(not(mask)) = min(pi/4,asin(sqrt(pvar(1,:)./sqdist))).^2;
    %     if obj.pointvar >= sqdist;
    %         var = (pi/4)^2;
    %     else
    %         var = min(pi/4,asin(sqrt(obj.pointvar/sqdist)))^2;
    %     end
    
        % relative variance limits the exactness of the placement as the
        % distance to the point increases (variance remains constant in the
        % angular domain)
        if userelvar
            var = max(var,(pi/16)^2);
        end
    end
    
    if nargout >= 3
    %     w = max(0,1 - max(0,min(1,dist/0.8))); % lmhere
    %     dist = sqrt(sum(bsxfun(@minus,p,feature.pos).^2,1));    
        w = max(0,min(10,1 ./ max(0,min(1,dist./0.8))) - 1)./10; % lmhere
    end
    
    if nargout >= 4
        grad = [...
            -relpos(2) ./ sqdist;... % solution to d(atan2(p(2)-featpos(2),p(1)-featpos(1))) / d(p(1))
            relpos(1) ./ sqdist;... % solution to d(atan2(p(2)-featpos(2),p(1)-featpos(1))) / d(p(2))
            zeros(size(val));...  % angle has no influence on relationship function value
            zeros(size(val))]; % changing the scale does not affect the value of this relationship function
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
    dirangle = relval+feature.orientation;
    dirangle = mod(dirangle+pi,2*pi)-pi;
    
    [dirx,diry] = pol2cart(dirangle,1);
    dir = [dirx;diry];
    len = 0.8; % lmhere
    
    p = {[feature.position,feature.position+ dir .* len]};
    ptype = 0;
    pm = 0;
    pvar = 0.01; % = 0.25 tolerance
end

end
    
end
