classdef RelDiff < RelvalRelationship

methods
    
function obj = RelDiff(varargin)
	obj@RelvalRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = reltype.RelDiff(obj);
end

end
    
methods(Static)

function n = name_s
    n = 'relDiff';
end
function ft = inputtype_s
    ft = {{'rel'},{'rel'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = false;
end
function ib = isboolean_s
    ib = false;
end

function [val,var,w,grad] = compute_s(rval1,rval2)
    if nargout == 1
        val = reltype.RelDiff.compute_o(rval1,rval2);
    elseif nargout == 2
        [val,var] = reltype.RelDiff.compute_o(rval1,rval2);
    elseif nargout == 3
        [val,var] = reltype.RelDiff.compute_o(rval1,rval2);
        w = ones(size(val));
    elseif nargout == 4
        [val,var,grad] = reltype.RelDiff.compute_o(rval1,rval2);
        w = ones(size(val));
    end
end

function [val,var,grad] = compute_o(rval1,rval2)
    
    val = rval1(:,:,1)-rval2(:,:,1);
    
    if nargout >= 2
%         var = (val .* 0.05).^2;
%         var = max((val .* 0.05).^2,0.05.^2);
        var = ones(size(val)) .* 0.4.^2;
        
        % todo: var = rval1(:,:,2) + rval2(:,:,2); (sum of variances)
        % or: user-defined variance that should not be overriden by the
        % child variances?
    end
    
    if nargout >= 3
        grad = cat(3,ones(size(val)),-ones(size(val)));
    end
end

end
    
end

