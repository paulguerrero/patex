classdef PolygonPolygonCentroiddirdist_up < FeatureRelationship

methods

function obj = PolygonPolygonCentroiddirdist_up(varargin)
	obj@FeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = reltype.PolygonPolygonCentroiddirdist_up(obj);
end

end
    
methods(Static)

function n = name_s
    n = 'polygonPolygonCentroiddirdist_up';
end
function ft = inputtype_s
    ft = {{'polygon'},{'polygon'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function [val,var,w,grad] = compute_s(feat1,feat2,userelvar)
%     if not(obj.hasRelation('boundarydist'))
%         bdist = obj.computeBoundarydist;
%     else
%         bdist = obj.value('boundarydist');
%     end

    f1pos = [feat1.position];
    f2pos = [feat2.position];
    f2f1vec = f1pos-f2pos;
    
    [dirangle,dist] = cart2pol(f2f1vec(1,:),f2f1vec(2,:));
    feat2orientation = [feat2.orientation];
    val = smod(dirangle-feat2orientation,-pi,pi);
    val = abs(smod(val-pi/2,-pi,pi));
    
    if nargout >= 2
        % var = sqrt(sum(f2f1vec.^2,1)) .* 0.05;
        var = ones(size(val)) .* ((2*pi)/128);
    end
    
    if nargout >= 3
        w = ones(size(val));
    end
    
    if nargout >= 4
        warning('Gradient for this relationship function not implemented yet.');
        grad = zeros(4,size(val,2));
        
%         distsq = dist.^2;
%         
%         % rotate f2f1vec so feat2orientation is at zero (rotate to local coordinate system of feat2)
%         rotmat = [...
%             cos(-feat2orientation),-sin(-feat2orientation);...
%             sin(-feat2orientation),cos(-feat2orientation)];
%             
%         f2f1vec_rot = rotmat * f2f1vec;
%         
%         f2f1vec_rot(1,:) = f2f1vec(1,:) .* cos(-feat2orientation) + f2f1vec(2,:) .* -sin(-feat2orientation);
%         f2f1vec_rot(2,:) = f2f1vec(1,:) .* sin(-feat2orientation) + f2f1vec(2,:) .* cos(-feat2orientation);
%         
%         % for position: partial derivatives of atan2 at rotated f2f1vec
%         % (negative for feat2, since only the relative position is relevant)
%         % for orientation: zero for feat1, -1 for feat2 (as feat2 orientation increases = rotates counterclockwise, orientation to feat1 decreases)
%         % for scale: zero
%         
%         posgrad = [...
%             -f2f1vec_rot(2,:) ./ distsq;...
%             f2f1vec(1,:) ./ distsq];
%         
%         % rotate gradient back
%         posgrad = f2f1vec_rot' * posgrad;
%         
%         grad1 = [posgrad;zeros(1,size(val,2));zeros(1,size(val,2))];
%         grad2 = [-posgrad;-ones(1,size(val,2));zeros(1,size(val,2))];
%         grad = cat(3,grad1,grad2);
    end
end

function [val,var,grad] = compute_o(pose1,pose2)
%     if not(obj.hasRelation('boundarydist'))
%         bdist = obj.computeBoundarydist;
%     else
%         bdist = obj.value('boundarydist');
%     end

    f1pos = pose1(1:2,:);
    f2pos = pose2(1:2,:);
    f2f1vec = f1pos-f2pos;
    
    [dirangle,dist] = cart2pol(f2f1vec(1,:),f2f1vec(2,:));
%     feat2orientation = [feat2.orientation];
    feat2orientation = 0; % for now
    val = smod(dirangle-feat2orientation,-pi,pi);
    val = abs(smod(val-pi/2,-pi,pi));
    
    if nargout >= 2
%         var = ones(size(val)) .* ((2*pi)/128);
        var = ones(size(val)) .* (pi/16)^2;
    end
    
    if nargout >= 3
        warning('Gradient for this relationship function not implemented yet.');
        grad = zeros(4,size(val,2));
        
%         distsq = dist.^2;
%         
%         % rotate f2f1vec so feat2orientation is at zero (rotate to local coordinate system of feat2)
%         rotmat = [...
%             cos(-feat2orientation),-sin(-feat2orientation);...
%             sin(-feat2orientation),cos(-feat2orientation)];
%             
%         f2f1vec_rot = rotmat * f2f1vec;
%         
%         f2f1vec_rot(1,:) = f2f1vec(1,:) .* cos(-feat2orientation) + f2f1vec(2,:) .* -sin(-feat2orientation);
%         f2f1vec_rot(2,:) = f2f1vec(1,:) .* sin(-feat2orientation) + f2f1vec(2,:) .* cos(-feat2orientation);
%         
%         % for position: partial derivatives of atan2 at rotated f2f1vec
%         % (negative for feat2, since only the relative position is relevant)
%         % for orientation: zero for feat1, -1 for feat2 (as feat2 orientation increases = rotates counterclockwise, orientation to feat1 decreases)
%         % for scale: zero
%         
%         posgrad = [...
%             -f2f1vec_rot(2,:) ./ distsq;...
%             f2f1vec(1,:) ./ distsq];
%         
%         % rotate gradient back
%         posgrad = rotmat' * posgrad;
%         
%         grad1 = [posgrad;zeros(1,size(val,2));zeros(1,size(val,2))];
%         grad2 = [-posgrad;-ones(1,size(val,2));zeros(1,size(val,2))];
%         grad = cat(3,grad1,grad2);
    end
end

end
    
end
