classdef SegmentArclenangle < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'segmentArclenangle';
end
function ft = inputtype_s
    ft = {{'pose'},{'segment'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = SegmentArclenangle(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = SegmentArclenangle(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar) %#ok<INUSD>
    
    segverts = feature.vertices;

    [~,si,~,cx,cy] = pointPolylineDistance_mex(...
            p(1,:),p(2,:),...
            segverts(1,:),segverts(2,:));
        
    
    orthsegvec = segverts(:,si+1)-segverts(:,si);
    orthsegvec = [-orthsegvec(2,:);orthsegvec(1,:)]; % segment should alway point towards the point
    
    mask = sum((p(1:2,:)-[cx;cy]).*orthsegvec,1) < 0;
    orthsegvec(:,mask) = -orthsegvec(:,mask);
%     if dot(p-[cx;cy],orthsegvec) < 0
%         orthsegvec = -orthsegvec;
%     end
    
    [axisangle,~] = cart2pol(orthsegvec(1),orthsegvec(2));
    val = p(3,:)-axisangle;
    val = mod(val+pi,2*pi)-pi;
    
    if nargout >= 2
        % just use absolute variance
        var = pvar(2,:);
    end
    
    if nargout >= 3
        w = ones(1,size(p,2));
    end
    
    if nargout >= 4
        grad = [...
            zeros(size(val));... % does not change with position (segment direction stays the same)
            zeros(size(val));... % does not change with position (segment direction stays the same)
            ones(size(val));...
            zeros(size(val))]; % changing the scale does not affect the value of this relationship function
    end
end

function [a,avar] = angle_levelset_posmline(feature,relval,relvar,p,ptype,pm) %#ok<INUSL>
%     segarclenangle = obj.value('arclenangle');
    
    if pm == 1
        relval = -relval;
% 	elseif psym >= 2
%         symsegmentarclenangle = -segmentarclenangle;
%         symangleoffset = symsegmentarclenangle-segmentarclenangle;
%         symangleoffset = mod(symangleoffset+pi,2*pi)-pi;
%         psym = 2+max(0,min(1,(symangleoffset+pi)/(2*pi)));
% %         psym = 2+max(0,min(1,(symsegmentarclenangle+pi)/(2*pi)));
    end

    if ptype == 1 % closed
        dsegvec = p(:,[2:end,1]) - p(:,1:end);
    else % open
        dsegvec = p(:,2:end) - p(:,1:end-1);    
    end
    
    [axisangle,~] = cart2pol(dsegvec(1,:),dsegvec(2,:));
    a = relval+axisangle;
    a = mod(a+pi,2*pi)-pi; 
    avar = relvar;
end

function a = angle_levelset_posmpoints(feature,segarclenangle,p,m)
%     segarclenangle = obj.value('arclenangle');
    
    sarclenangle = ones(1,size(p,2)).*segarclenangle;
    mask = m==1;
    sarclenangle(mask) = -sarclenangle(mask);
%     if m == 1
%         segarclenangle = -segarclenangle;
% % 	elseif psym >= 2
% %         symsegmentarclenangle = -segmentarclenangle;
% %         symangleoffset = symsegmentarclenangle-segmentarclenangle;
% %         symangleoffset = mod(symangleoffset+pi,2*pi)-pi;
% %         psym = 2+max(0,min(1,(symangleoffset+pi)/(2*pi)));
% % %         psym = 2+max(0,min(1,(symsegmentarclenangle+pi)/(2*pi)));
%     end
    
    segverts = feature.vertices;
%     arrayfun(@(x) size(x.vertices,2),features.vertices);
%     segverts = [feature.vertices];
    

    [~,si,~,cx,cy] = pointPolylineDistance_mex(...
            p(1,:),p(2,:),...
            segverts(1,:),segverts(2,:));
        
    orthsegvec = segverts(:,si+1)-segverts(:,si);
    orthsegvec = [-orthsegvec(2,:);orthsegvec(1,:)]; % segment should alway point towards the point
    
    mask = sum((p(1:2,:)-[cx;cy]).*orthsegvec,1) < 0;
    orthsegvec(:,mask) = -orthsegvec(:,mask);
    
    [axisangle,~] = cart2pol(orthsegvec(1,:),orthsegvec(2,:));
    a = sarclenangle+axisangle;
    a = mod(a+pi,2*pi)-pi;
end

end
    
end
