classdef SegmentSymarclen < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'segmentSymarclen';
end
function ft = inputtype_s
    ft = {{'pose'},{'segment'}};
end
function ac = angletype_s
    ac = 'SegmentArclenangle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = SegmentSymarclen(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = SegmentSymarclen(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar) %#ok<INUSD>
%     if isempty(obj.cornerextent)
%         obj.cornerextent = obj.computeCornerextent;
%     end
    segverts = feature.vertices;
    segarclen = feature.arclen;

    [d,si,f,~,~] = pointPolylineDistance_mex(...
            p(1,:),p(2,:),...
            segverts(1,:),segverts(2,:));
        
    val = (segarclen(si).*(1-f) + segarclen(si+1).*f) ./ segarclen(end);
    
    % make value symmetric on the segment
    mask = val > 0.5;
    val(mask) = 1-val(mask);
    
    if nargout >= 2
        % just use normalized absolute variance
        var = pvar(1,:) ./ segarclen(end)^2;
    end
    
    if nargout >= 3
        w = 1-max(0,min(1,d ./ (feature.parentelement.depth*0.75)));
        w = min(1,w.*1.2); % constant 1 near the border
        
        % todo: when outside range, set arclen to 'distance projected to
        % first/last edge in the segment = segment si' / total arclength, so
        % the values continue smoothly and don't stop at 0 or 1
        
%         w = w * (1-max(0,min(1,abs(val-0.5)*2)));
        % also less towards the ends of the segment    
        % function that is 0 at the ends of the segment and 1 at the center and
        % a falloff from 1 that starts ~= at 0.4/0.6 and becomes zero at 0.1/0.9
%         w = w * max(0,min(1,((1-(abs(val-0.5).*2)).*1.66-0.33)));
        % 1 everywhere except a short falloff to 0 at the edges
        w = w .* max(0,min(1,((1-(abs(val-0.5).*2)).*100)));
        % todo: cornercell diameter as maximum distance here
    %     w = 0; % temp
    end
    
    if nargout >= 4
        tangentvec = bsxfun(@times,segverts(:,si+1)-segverts(:,si),ones(size(val)));
        tangentvec = tangentvec ./ (segarclen(si+1) - segarclen(si));
        tangentvec(:,mask) = -tangentvec(:,mask);
        grad = [...
            tangentvec(1);...
            tangentvec(2);...
            zeros(size(val));...  % angle has no influence on relationship function value
            zeros(size(val))]; % changing the scale does not affect the value of this relationship function
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar) %#ok<STOUT,INUSD>
    error('Levelsets not implemented for this relation type.');
end

end
    
end
