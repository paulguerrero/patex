classdef RelDirdiff < RelvalRelationship

methods

function obj = RelDirdiff(varargin)
	obj@RelvalRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = reltype.RelDirdiff(obj);
end

end

methods(Static)

function n = name_s
    n = 'relDirdiff';
end
function ft = inputtype_s
    ft = {{'rel'},{'rel'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = false;
end
function ib = isboolean_s
    ib = false;
end

function [val,var,w,grad] = compute_s(rval1,rval2)
    if nargout == 1
        val = reltype.RelDirdiff.compute_o(rval1,rval2);
    elseif nargout == 2
        [val,var] = reltype.RelDirdiff.compute_o(rval1,rval2);
    elseif nargout == 3
        [val,var] = reltype.RelDirdiff.compute_o(rval1,rval2);
        w = ones(size(val));
    elseif nargout == 4
        [val,var,grad] = reltype.RelDirdiff.compute_o(rval1,rval2);
        w = ones(size(val));
    end
end

function [val,var,grad] = compute_o(rval1,rval2)

    val = smod( rval1(:,:,1)-rval2(:,:,1), -pi,pi);
    
    if nargout >= 2
        var = ones(size(val)) .* (pi/16)^2;
%         var = ones(size(val)) .* (pi/8)^2;
    end
    
    if nargout >= 3
        grad = cat(3,ones(size(val)),-ones(size(val)));
    end
end

end
    
end

