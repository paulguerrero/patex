classdef PolygonBoundaryangle < PoseFeatureRelationship

methods

function obj = PolygonBoundaryangle(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = reltype.PolygonBoundaryangle(obj);
end

end

methods(Static)

function n = name_s
    n = 'polygonBoundaryangle';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygon'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar) %#ok<INUSD>
    
    % hull must be cw!
    
    % compute the segment of the point p on the dilated polygon
    [dist,psegind,f,cpx,cpy] = pointPolylineDistance_mex(...
        p(1,:),p(2,:),...
        feature.verts(1,[1:end,1]),...
        feature.verts(2,[1:end,1]));

    refangle = zeros(1,size(p,2));
    
    relpos = [p(1,:) - cpx; p(2,:)-cpy];
    
    % reference frame for angle is always aligned so that 0 is the boundary
    % direction for a clockwise boundary
    
    % if closest point is corner, rotate direction to closest point by 90
    % degrees so it points in clockwise boundary direction (so turn angle
    % to closest point 90 degrees clockwise (negative) if point is inside
    % polygon and counterclockwise (positive) if outside polygon)
    mask = f == 0 | f == 1;
    if any(mask)
        maskinds = find(mask);
        in = pointInPolygon_mex(p(1,mask),p(2,mask),...
            feature.verts(1,:),feature.verts(2,:));
        refangle(:,maskinds) = cart2pol(-relpos(1,maskinds), -relpos(2,maskinds));
        refangle(maskinds(in)) = mod(refangle(maskinds(in))-pi/2+pi,2*pi)-pi;
        refangle(maskinds(not(in))) = mod(refangle(maskinds(not(in)))+pi/2+pi,2*pi)-pi;
    end
    
    % if closest point is on a line segment, use direction of line segment
    % (hull must be guaranteed to be clockwise)
    nmask = not(mask);
    if any(nmask)
        dsegvec = feature.verts(:,[2:end,1]) - ...
                  feature.verts;
        refangle(nmask) = cart2pol(...
            dsegvec(1,psegind(nmask)),dsegvec(2,psegind(nmask)));
    end
    
    val = p(3,:)-refangle;
    val = mod(val+pi,2*pi)-pi;

    if nargout >= 2
        % just use absolute variance
        var = pvar(2,:);
    end
    
    if nargout >= 3
        w = ones(1,size(p,2));
    end
    
    if nargout >= 4
        % since polygon boundaries are piecewise linear, the gradient
        % of the relative angle is only non-zero
        % if the closest point is a vertex of the boundary
        if any(mask)
            distsq = dist.^2;
            dx(mask) = relpos(2,mask) ./ distsq(mask);... % solution to d(atan2(p(2)-cpy,p(1)-cpx)) / d(p(1))
            dy = -relpos(1,mask) ./ distsq(mask);... % solution to d(atan2(p(2)-cpy,p(1)-cpx)) / d(p(2))            
        end
        
        if any(nmask)
            dx(nmask) = 0;
            dy(nmask) = 0;
        end
       
        grad = [...
            dx;...
            dy;...
            ones(1,size(val,2));...
            zeros(1,size(val,2))];
    end
end

% angle levelset in a pose subspace defined by a line in the position and mirrored [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
function [a,avar] = angle_levelset_posmline(feature,relval,relvar,p,ptype,pm) %#ok<INUSL>
%     boundaryangle = obj.value('boundaryangle');

    if pm == 1
        relval = sign(relval)*(pi-abs(relval));
%     elseif pm >= 2
%         symboundaryangle = sign(boundaryangle)*(pi-abs(boundaryangle));
%         symangleoffset = symboundaryangle-boundaryangle;
%         symangleoffset = mod(symangleoffset+pi,2*pi)-pi;
%         pm = 2+max(0,min(1,(symangleoffset+pi)/(2*pi)));
%         pm = 2+max(0,min(1,(symboundaryangle+pi)/(2*pi)));
   end
    
    if ptype == 1 % closed
        dsegvec = p(:,[2:end,1]) - p(:,1:end);
    else % open
        dsegvec = p(:,2:end) - p(:,1:end-1);    
    end

    [axisangle,~] = cart2pol(dsegvec(1,:),dsegvec(2,:));
    a = relval+axisangle;
    a = mod(a+pi,2*pi)-pi;
    avar = relvar;
end

% angle levelset in a pose subspaces defined by several points in the position and mirrored [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
function a = angle_levelset_posmpoints(feature,boundaryangle,p,m)
    % this results in the values of the angle levelset at the given
    % lines in pose space - orientation (position,size and mirrored)
    
    if m == 1
        boundaryangle = sign(boundaryangle)*(pi-abs(boundaryangle));
    end
    
    % todo: buffer the closest points?
    % or compute in cuda
    
    % hull must be cw!
    
    % compute the segment of the point p on the dilated polygon
    [~,psegind,f,cpx,cpy] = pointPolylineDistance_mex(...
        p(1,:),p(2,:),...
        feature.verts(1,[1:end,1]),...
        feature.verts(2,[1:end,1]));

    refangle = zeros(1,size(p,2));
    
    % reference frame for angle is always aligned so that 0 is the boundary
    % direction for a clockwise boundary
    
    % todo: (also in computeAngle) instead of pointInPolygon, make dot
    % product with segment, should be > 0 (also on corners - 0 means
    % degenerate 180 degrees corner - has to be outside, turn 90 deg. clockwise)
    
    % if closest point is corner, rotate direction to closest point by 90
    % degrees so it points in clockwise boundary direction (so turn angle
    % to closest point 90 degrees clockwise (negative) if point is inside
    % polygon and counterclockwise (positive) if outside polygon)
    mask = f == 0 | f == 1;
    if any(mask)
        maskinds = find(mask);
        in = pointInPolygon_mex(p(1,mask),p(2,mask),...
            feature.verts(1,:),feature.verts(2,:));
        refangle(:,maskinds) = cart2pol(...
            cpx(maskinds) - p(1,maskinds),...
            cpy(maskinds) - p(2,maskinds));
        refangle(maskinds(in)) = mod(refangle(maskinds(in))-pi/2+pi,2*pi)-pi;
        refangle(maskinds(not(in))) = mod(refangle(maskinds(not(in)))+pi/2+pi,2*pi)-pi;
    end
    
    % if closest point is on a line segment, use direction of line segment
    % (hull must be guaranteed to be clockwise)
    if not(all(mask))
        dsegvec = feature.verts(:,[2:end,1]) - ...
                  feature.verts;
        refangle(not(mask)) = cart2pol(...
            dsegvec(1,psegind(not(mask))),dsegvec(2,psegind(not(mask))));
    end
    
    a = boundaryangle+refangle;
    a = mod(a+pi,2*pi)-pi;

%     % just use absolute variance
%     var = pvar(2,:);
%     
%     w = ones(1,size(p,2));
end

end
    
end
