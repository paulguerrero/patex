classdef PolygonPolygonCentroiddist < FeatureRelationship

methods

function obj = PolygonPolygonCentroiddist(varargin)
	obj@FeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = reltype.PolygonPolygonCentroiddist(obj);
end

end
    
methods(Static)

function n = name_s
    n = 'polygonPolygonCentroiddist';
end
function ft = inputtype_s
    ft = {{'polygon','polygonset'},{'polygon','polygonset'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function [val,var,w,grad] = compute_s(feat1,feat2,userelvar) %#ok<INUSD>
    f1pos = PatternSolver.affine2simpose([feat1.pose2D]);
    f2pos = PatternSolver.affine2simpose([feat2.pose2D]);
    
    if nargout == 1
        val = reltype.PolygonPolygonCentroiddist.compute_o(f1pos,f2pos);
    elseif nargout == 2
        [val,var] = reltype.PolygonPolygonCentroiddist.compute_o(f1pos,f2pos);
    elseif nargout == 3
        [val,var] = reltype.PolygonPolygonCentroiddist.compute_o(f1pos,f2pos);
        w = ones(size(val));
    elseif nargout == 4
        [val,var,grad] = reltype.PolygonPolygonCentroiddist.compute_o(f1pos,f2pos);
        w = ones(size(val));
    end
end

% input poses are similarity transformation poses
% additional required properties of the feature can be given in the 3rd and
% 4th argument
function [val,var,grad] = compute_o(f1pose,f2pose,f1props,f2props)
%     f1pos = pose1(1:2,:);
%     f2pos = pose2(1:2,:);
    diff = f1pose(1:2,:) - f2pose(1:2,:);
    val = sqrt(sum(diff.^2,1));
    
    if nargout >= 2
        var = ones(size(val)) .* 0.4.^2; % todo: get variance from input function variance
    end
    
    if nargout >= 3
        % on the vector of input poses of both features
        mask = val ~= 0;
        dval_df1pose(:,mask) = bsxfun(@rdivide,diff(:,mask),val(mask));
        mask = not(mask);
        dval_df1pose(1,mask) = 0;
        dval_df1pose(2,mask) = 0;
        dval_df1pose = [dval_df1pose;zeros(size(f1pose,1)-2,size(val,2))];
        % dval_dpose2 = -dval_dpose1
        dval_df2pose = [-dval_df1pose(1:2,:);zeros(size(f1pose,1)-2,size(val,2))];
        
        
        grad = cat(3,dval_df1pose,dval_df2pose);
    end
end

end
    
end
