classdef PolylineAngle < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polylineAngle';
end
function ft = inputtype_s
    ft = {{'pose'},{'polyline'}};
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolylineAngle(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolylineAngle(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar) %#ok<INUSD>
    % compute the segment of the point p on the dilated polygon
    [dist,~,f,cpx,cpy] = pointPolylineDistance_mex(...
        p(1,:),p(2,:),...
        feature.verts(1,:),...
        feature.verts(2,:));
        
    relpos = [p(1,:) - cpx; p(2,:) - cpy];
    
    % reference frame for angle is always aligned so that 0 is the boundary
    % direction for a clockwise boundary
    
    % rotate direction to closest point by 90 degrees so it points in
    % clockwise boundary direction (so turn angle to closest point 90
    % degrees  counterclockwise (positive) - point is always on the outside)
    refangle = cart2pol(-relpos(1),-relpos(2));
    refangle = smod(refangle+pi/2,-pi,pi);
    
    val = p(3,:)-refangle;
    val = mod(val+pi,2*pi)-pi;

    if nargout >= 2
        % just use absolute variance
        var = pvar(2,:);
    end
    
    if nargout >= 3
        w = ones(1,size(p,2));
    end
    
    if nargout >= 4
        % since polylines are piecewise linear, the gradient
        % of the relative angle is only non-zero
        % if the closest point is a vertex of the polyline
        mask = f == 0 | f == 1;
        
        if any(mask)
            distsq = dist.^2;
            dx(mask) = relpos(2,mask) ./ distsq(mask);... % solution to d(atan2(p(2)-cpy,p(1)-cpx)) / d(p(1))
            dy = -relpos(1,mask) ./ distsq(mask);... % solution to d(atan2(p(2)-cpy,p(1)-cpx)) / d(p(2))            
        end
        
        nmask = not(mask);
        if any(nmask)
            dx(nmask) = 0;
            dy(nmask) = 0;
        end
       
        grad = [...
            dx;...
            dy;...
            ones(1,size(val,2));...
            zeros(1,size(val,2))];
    end
end

function [a,avar] = angle_levelset_posmline(feature,relval,relvar,p,ptype,pm) %#ok<INUSL>

    if pm == 1
        relval = sign(relval)*(pi-abs(relval));
    end
    
    if ptype == 1 % closed
        dsegvec = p(:,[2:end,1]) - p(:,1:end);
    else % open
        dsegvec = p(:,2:end) - p(:,1:end-1);    
    end

    [axisangle,~] = cart2pol(dsegvec(1,:),dsegvec(2,:));
    a = relval+axisangle;
    a = mod(a+pi,2*pi)-pi;
    avar = relvar;
end

function a = angle_levelset_posmpoints(feature,segdistangle,p,m)
    error('not implented yet');
end

end
    
end
