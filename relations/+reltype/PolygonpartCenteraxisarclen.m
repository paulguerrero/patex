classdef PolygonpartCenteraxisarclen < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polygonpartCenteraxisarclen';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygonpart'}};
end
function ac = angletype_s
    ac = 'PolygonpartCenteraxisangle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function obj = PolygonpartCenteraxisarclen(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolygonpartCenteraxisarclen(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar) %#ok<INUSD>

    [~,si,f,~,~] = pointPolylineDistance_mex(...
        p(1,:),p(2,:),feature.centeraxis(1,:),feature.centeraxis(2,:));
    arclen = polylineArclen(feature.centeraxis,false);

    val = (arclen(si).*(1-f) + arclen(si+1).*f) ./ arclen(end);
    
    if nargout >= 2
        % just use normalized absolute variance
        var = pvar(1,:) ./ arclen(end)^2;
    end
    
    if nargout >= 3
        w = ones(1,size(p,2));
    end
    
    if nargout >= 4
        error('Gradient not yet implemented for this relationship.');
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
    error('not implented yet');
end

end
    
end
