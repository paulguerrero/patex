classdef PolygonPolygonDirCentroiddirDist < FeatureRelationship

methods

function obj = PolygonPolygonDirCentroiddirDist(varargin)
	obj@FeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = reltype.PolygonPolygonDirCentroiddirDist(obj);
end

end
    
methods(Static)

function n = name_s
    n = 'polygonPolygonDirCentroiddirDist';
end
function ft = inputtype_s
    ft = {{'polygon','polygonset'},{'polygon','polygonset'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function [val,var,w,grad] = compute_s(feat1,feat2,userelvar)
    f1pos = PatternSolver.affine2simpose([feat1.pose2D]);
    f2pos = PatternSolver.affine2simpose([feat2.pose2D]);
    
    if nargout == 1
        val = reltype.PolygonPolygonDirCentroiddirDist.compute_o(f1pos,f2pos);
    elseif nargout == 2
        [val,var] = reltype.PolygonPolygonDirCentroiddirDist.compute_o(f1pos,f2pos);
    elseif nargout == 3
        [val,var] = reltype.PolygonPolygonDirCentroiddirDist.compute_o(f1pos,f2pos);
        w = ones(size(val));
    elseif nargout == 4
        [val,var,grad] = reltype.PolygonPolygonDirCentroiddirDist.compute_o(f1pos,f2pos);
        w = ones(size(val));
    end
end

% input poses are similarity transformation poses
% additional required properties of the feature can be given in the 3rd and
% 4th argument
function [val,var,grad] = compute_o(f1pose,f2pose,f1props,f2props)
%     f1pos = f1pos(1:2,:);
%     f2pos = f2pos(1:2,:);
    diff = f1pose(1:2,:) - f2pose(1:2,:);
    
    [centroiddirangle,dist] = cart2pol(diff(1,:),diff(2,:));

%     feat2orientation = f2pose(3,:);
%     val = smod(dirangle-feat2orientation,-pi,pi);
    
%     val = centroiddirangle;
    
    feat1orientation = f1pose(3,:);
    diffval = smod(feat1orientation-centroiddirangle,-pi,pi);
    val = abs(diffval);
    
    if nargout >= 2
%         var = ones(size(val)) .* ((2*pi)/128);
        var = ones(size(val)) .* (pi/16)^2;
%         var = ones(size(val)) .* (pi/8)^2;
        % todo: when changing this to something non-constant, gradient
        % estimation might need some updates, since it currently assumes
        % the circular range of a circular objetive function value (which
        % is normalized by the variance) is the same in x0+delta as it is
        % in x0 (where it is computed)
    end
    
    if nargout >= 3
        % 1 for first input (as first orientation increases = angle
        % increases), zero for second input (orientation of second feature
        % does not change the angle)
        
        % for position: partial derivatives of atan2 of difference vector
        % (negative for second input)
        % for orientation: 1 for first input (as first orientation
        % increases = angle increases), zero for second input (orientation
        % of second feature does not change the angle)
        % feat2orientation)
        % for scale: zero
        
        distsq = dist.^2;
        
        % negative gradient of the centroid direction, since we subtract
        % the centroid direction
        dval_df1pose = [...
            diff(2,:) ./ distsq;...
            -diff(1,:) ./ distsq;...
            ones(1,size(val,2));...
            zeros(size(f1pose,1)-3,size(val,2))];
        dval_df2pose = [...
            -dval_df1pose(1:2,:);...
            zeros(size(f1pose,1)-2,size(val,2))];
        
        % if the value would be negative if not for the abs, the gradient
        % points in the other direction
        mask = diffval < 0;
        dval_df1pose(:,mask) = -dval_df1pose(:,mask);
        dval_df2pose(:,mask) = -dval_df2pose(:,mask);

        grad = cat(3,dval_df1pose,dval_df2pose);
    end
end

end
    
end
