classdef PolygonPolygonSizediff < FeatureRelationship

methods

function obj = PolygonPolygonSizediff(varargin)
	obj@FeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = reltype.PolygonPolygonSizediff(obj);
end

end
    
methods(Static)

function n = name_s
    n = 'polygonPolygonSizediff';
end
function ft = inputtype_s
    ft = {{'polygon','polygonset'},{'polygon','polygonset'}};
end
function ac = angletype_s
    ac = '';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function [val,var,w,grad] = compute_s(feat1,feat2,userelvar)
    f1pos = PatternSolver.affine2simpose([feat1.pose2D]);
    f2pos = PatternSolver.affine2simpose([feat2.pose2D]);
    
    if nargout == 1
        val = reltype.PolygonPolygonSizediff.compute_o(f1pos,f2pos);
    elseif nargout == 2
        [val,var] = reltype.PolygonPolygonSizediff.compute_o(f1pos,f2pos);
    elseif nargout == 3
        [val,var] = reltype.PolygonPolygonSizediff.compute_o(f1pos,f2pos);
        w = ones(size(val));
    elseif nargout == 4
        [val,var,grad] = reltype.PolygonPolygonSizediff.compute_o(f1pos,f2pos);
        w = ones(size(val));
    end
end

% input poses are similarity transformation poses
% additional required properties of the feature can be given in the 3rd and
% 4th argument
function [val,var,grad] = compute_o(f1pose,f2pose,f1props,f2props)
    
    diff = f1pose(4,:) - f2pose(4,:);
    
%     diff = f1pose(4,:) ./ f2pose(4,:);
    
    val = diff;
    
    if nargout >= 2
        var = ones(size(val)) .* 0.2.^2; % todo: get variance from input function variance
    end
    
    if nargout >= 3
        % on the vector of input poses of both features
        
%         dval_df1size = 1 ./ f2pose(4,:);
%         dval_df2size = - f1pose(4,:) ./ f2pose(4,:).^2;
%         
%         grad = cat(3,...
%             [zeros(3,size(val,2));dval_df1size;zeros(size(f1pose,1)-4,size(val,2))],...
%             [zeros(3,size(val,2));dval_df2size;zeros(size(f1pose,1)-4,size(val,2))]);
        
        grad = cat(3,...
            [zeros(3,size(val,2));ones(1,size(val,2));zeros(size(f1pose,1)-4,size(val,2))],...
            [zeros(3,size(val,2));-ones(1,size(val,2));zeros(size(f1pose,1)-4,size(val,2))]);
    end
end

end
    
end
