classdef PolygonBoundarynormal < PoseFeatureRelationship

methods

function obj = PolygonBoundarynormal(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = reltype.PolygonBoundarynormal(obj);
end

end

methods(Static)

function n = name_s
    n = 'polygonBoundarynormal';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygon'}};
end
function ac = angletype_s
    ac = 'PolygonBoundaryangle';
end
function ic = iscircular_s
    ic = true;
end
function is = issymmetric_s % nary = 1 means it is symmetric
    is = true;
end
function ib = isboolean_s
    ib = false;
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    
    % compute weighted average of normals along the polygon boundary
    % weight function is a 2d gaussian centered at the pose with std. dev.
    % krad
    
    bbdiag = sqrt(sum((feature.boundingboxmax - feature.boundingboxmin).^2,1));
    krad = bbdiag * 0.1;
    
    v = feature.verts;

    edgelen = sqrt(sum((v(:,[2:end,1]) - v).^2,1));
    % get distance to lines colinear with the edges
    [dist2line,~,distalongline] = pointLineDistance2D(p(1:2,:),v,v(:,[2:end,1]));
    
    d1 = -distalongline;
    d2 = bsxfun(@plus,d1,edgelen');
    
    clear distalongline;
    
    % weight function along each edge is a 1d gaussian with a height defined
    % by a gaussian of the distance between line and pose
    a = exp(-(dist2line.^2 ./ (2.*krad.^2)));
    
    clear dist2line;
    
    % weights for each edge (line integral of a gaussian centered at the pose along the edge)
    % indefinite integral of a gaussian is (sqrt(pi/2) .* krad) .* (-a) .* erf((-x)./(sqrt(2).*krad))
    w = ...
        (sqrt(pi/2) .* krad) .* (-a) .* erf((-d2)./(sqrt(2).*krad)) - ...
        (sqrt(pi/2) .* krad) .* (-a) .* erf((-d1)./(sqrt(2).*krad));
    
    clear a d1 d2;
    
    % weighted average orientation of edge normals
    n = polylineNormals(v,true,false); % point outwards if polygon is cw (should be)
    sumw = sum(w,1);
    [val,nlen] = cart2pol(...
        sum(bsxfun(@times,n(1,:)',w),1) ./ sumw,...
        sum(bsxfun(@times,n(2,:)',w),1) ./ sumw);
    
    clear n w sumw;
    
    if nargout >= 2
        if userelvar
            % use constant variance for now
            var = ones(size(val)).*(pi/16)^2;
        else
            % use constant variance for now
            var = ones(size(val)).*(pi/16)^2;
        end
    end
    
    if nargout >= 3
        [d,~,~,~,~] = pointPolylineDistance_mex(...
                p(1,:),p(2,:),...
                v(1,[1:end,1]),v(2,[1:end,1]));
        mask = pointInPolygon_mex(...
            p(1,:),p(2,:),...
            v(1,:),v(2,:));
        w = zeros(1,size(p,2));
        if any(mask)
            w(mask) = 1 - max(0,min(1,-d(mask)./(feature.depth)));
        end
        w(not(mask)) = max(0.05,1 - max(0,min(1,d(not(mask))./(feature.radius*2))));
    %     w(not(mask)) = max(0.05,1 - max(0,min(1,val(not(mask))./(feature.radius*5)))); % here
    
%         w = w .* nlen; % factor in certainty of normal
        
        w = nlen; % just use certainty of normal
    end

    if nargout >= 4
        error('Gradient not yet implemented for this relationship type.');
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar)
    error('Level sets not yet implemented for this relationship type.');
end

end
    
end
