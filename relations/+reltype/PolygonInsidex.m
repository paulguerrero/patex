classdef PolygonInsidex < PoseFeatureRelationship

methods(Static)

function n = name_s
    n = 'polygonInsidex';
end
function ft = inputtype_s
    ft = {{'pose'},{'polygon'}};
end
function ac = angletype_s
    ac = 'PolygonGlobalangle';
end
function ic = iscircular_s
    ic = false;
end
function is = issymmetric_s
    is = false;
end
function ib = isboolean_s
    ib = true;
end

function obj = PolygonInsidex(varargin)
	obj@PoseFeatureRelationship(varargin{:});
end

function obj2 = clone(obj)
    obj2 = PolygonInsidex(obj);
end

function [val,var,w,grad] = compute_s(p,pvar,feature,userelvar)
    
    bbmin = feature.boundingboxmin;
    bbmax = feature.boundingboxmax;
    val = double(p(1,:)>bbmin(1) & p(1,:)<bbmax(1));
    
    if nargout >= 2
    %     var = pvar(1,:);
        var = 0.001; % some small variance
    end
    
    if nargout >= 3
        w = ones(1,size(p,2));
    end
    
    if nargout >= 4
        grad = zeros(4,size(val,2)); % zero gradient everywhere (except at discontinuities)
    end
end

function [p,ptype,pm,pvar] = posm_levelset(feature,relval,relvar) %#ok<STOUT,INUSD>
    error('Levelset not implemented.');
end

end
    
end
