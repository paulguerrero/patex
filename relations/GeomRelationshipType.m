% mapping between classes and relationship types is bijective
% use a separate class to describe the relationship type instead of static
% methods for two reasons:
% 1) performance: since I often have to get the class from the type name
% (e.g. to get infos about the relationship type or to create a new
% relationship), I would have to call str2func(classname(type)) each time
% to get a handle to the required function and then call it, which takes
% about 4x as long as getting the handle once, storing it and then just
% using the stored handle.
% 2) mapping between name and classname: Need some way to map between
% these 2 synonyms for relationship types. Name is the human-readable name,
% classname is the name of the class
classdef GeomRelationshipType
    
properties(SetAccess=protected)
    classname = '';
end

properties(Dependent,SetAccess=private)
    name;
    inputtype;
    nary;
%     angletype;
    iscircular;
    issymmetric;
    isboolean;
end

properties(Access=protected)
    % to speed up access (so I don't have to call str2func with the classname every time)
    constructorhandle = function_handle.empty;
    namehandle = function_handle.empty;
    inputtypehandle = function_handle.empty;
%     angletypehandle = function_handle.empty;
    iscircularhandle = function_handle.empty;
    issymmetrichandle = function_handle.empty;
    isbooleanhandle = function_handle.empty;
end

methods

function obj = GeomRelationshipType(classname)

    if nargin == 0
        % do noting (for empty list)
    elseif nargin == 1

        obj.classname = classname;

        % check if the given classname is a class derived from
        % GeomRelationship and has all necessary static functions
        % (the latter is guaranteed when being derived from
        % GeomRelationship, but it is only checked when the
        % class is actually instantiated or when the static functions
        % are called, so better to check right away)

        if not(exist(obj.classname,'class'))
            error(['Cannot find the class ''',obj.classname,'''.']);
        end
        if not(any(strcmp(...
                'GeomRelationship',...
                superclasses(obj.classname))))
            error('Class is not a GeomRelationship.');
        end

        classmethods = methods(obj.classname);

        if not(all(ismember({...
                'name_s',...
                'inputtype_s',...
                'iscircular_s'...
                'issymmetric_s'...
                'isboolean_s'...
                },classmethods)));
            error('Class does not have all necessary static functions.');
        end

        obj.constructorhandle = str2func([obj.classname]);
        obj.namehandle = str2func([obj.classname,'.name_s']);
        obj.inputtypehandle = str2func([obj.classname,'.inputtype_s']);
        obj.iscircularhandle = str2func([obj.classname,'.iscircular_s']);
        obj.issymmetrichandle = str2func([obj.classname,'.issymmetric_s']);
        obj.isbooleanhandle = str2func([obj.classname,'.isboolean_s']);

%         if ismember('angletype_s',classmethods)
%             obj.angletypehandle = str2func([obj.classname,'.angleclass_s']);
%         end
    else
        error('Invalid arguments.');
    end
end

function val = get.name(obj)
    val = obj.namehandle();
end
function val = get.inputtype(obj)
    val = obj.inputtypehandle();
end
function val = get.nary(obj)
    val = numel(obj.inputtype);
end
function val = get.iscircular(obj)
    val = obj.iscircularhandle();
end
function val = get.issymmetric(obj)
    val = obj.issymmetrichandle();
end
function val = get.isboolean(obj)
    val = obj.isbooleanhandle();
end
% function val = get.angletype(obj)
%     if isempty(obj.angletype)
%         val = '';
%     else
%         val = obj.angletypehandle();
%     end
% end

function instance = createInstance(obj,varargin)
    instance = obj.constructorhandle(varargin{:});
end

end

methods(Static)

% factory methods (create relation element of correct class given a relation name)

% Feature class name and feature types are not the same,
% e.g. there are relations to segments, which will always work on edges
% as well, but you may want to have relations that are applied to
% segments only, this is done through the applicable feature types.
function [name2type,classname2type] = library
%     persistent id2type_singleton;
    persistent name2type_singleton;
    persistent classname2type_singleton;

    if not(isa(name2type_singleton,'containers.Map'))
%         id2type_singleton = containers.Map('KeyType','double','ValueType','any');
        name2type_singleton = containers.Map('KeyType','char','ValueType','any');
        classname2type_singleton = containers.Map('KeyType','char','ValueType','any');

        % add all known relation types (all classe in the 'reltype' package)
        reltypeclasses = meta.package.fromName('reltype').ClassList;
        reltypeclassnames = {reltypeclasses.Name};
        reltypeclassnames = sort(reltypeclassnames); % to keep the order consistent
        for i=1:numel(reltypeclassnames)
            GeomRelationshipType.addReltype(reltypeclassnames{i});
        end
    end
    
%     id2type = id2type_singleton;
    name2type = name2type_singleton;
    classname2type = classname2type_singleton;
end

function addReltype(classname)
    if isempty(classname)
        error('Empty class name.');
    end
    
    [name2type,classname2type] = GeomRelationshipType.library;
    
    if any(strcmp(classname,classname2type.keys))
        error(['The relationship ',classname,' is already in the library.']);
    end
    
%     existingids = id2type.keys;
%     existingids = [existingids{:}];
%     newid = missingint(existingids);
    reltype = GeomRelationshipType(classname);
    typename = reltype.name;
    if isempty(typename)
        delete(reltype(isvalid(reltype)));
        error('Relationship type name must not be empty.');
    end
    if any(strcmp(typename,name2type.keys))
        delete(reltype(isvalid(reltype)));
        error(['The relationship ',typename,' is already in the library.']);
    end
    
%     id2type(reltype.id) = reltype; %#ok<NASGU>
    name2type(reltype.name) = reltype; %#ok<NASGU>
    classname2type(reltype.classname) = reltype; %#ok<NASGU>
end

function removeReltype(classname)
    if isempty(classname)
        error('Empty class name.');
    end
    
    [name2type,classname2type] = GeomRelationshipType.library;
    reltype = classname2type(classname);
    name2type.remove(reltype.name);
    classname2type.remove(reltype.classname);
end

function reltype = fromClassname(classname)
    if isempty(classname)
        reltype = GeomRelationshipType.empty;
        return;
    end
    
    [~,classname2type] = GeomRelationshipType.library;

    if not(all(classname2type.isKey(classname)))
        error('Relation type not found.');
    end
    if iscell(classname)
        reltype = classname2type.values(classname);
        reltype = [reltype{:}];
    else
        reltype = classname2type(classname);
    end
end

function reltype = fromName(name)
    if isempty(name)
        reltype = GeomRelationshipType.empty;
        return;
    end
    
    [name2type,~] = GeomRelationshipType.library;

    if not(all(name2type.isKey(name)))
        error('Relation type not found.');
    end
    if iscell(name)
        reltype = name2type.values(name);
        reltype = [reltype{:}];
    else
        reltype = name2type(name);
    end
end

% function reltype = fromID(id)
%     if isempty(id)
%         reltype = GeomRelationshipType.empty;
%         return;
%     elseif numel(id) > 1
%         id = num2cell(id);
%     end
%     
%     [id2type,~,~] = GeomRelationshipType.library;
% 
%     if not(all(id2type.isKey(id)))
%         error('Relation type not found.');
%     end
%     if iscell(id)
%         reltype = id2type.values(id);
%         reltype = [reltype{:}];
%     else
%         reltype = id2type(id);
%     end
% end

% elm = create(relname,varargin)
function elm = create(relname,varargin)
    % create from given relation name
    reltype = GeomRelationshipType.fromName(relname);
    elm = reltype.createInstance(varargin{:});
end

end

end
