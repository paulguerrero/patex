% difference between relatinoship values with gaussian falloff (between 0
% for min. and 1 for max. difference)
function reldiff = normreldiff(mean1,sdev1,mean2,sdev2,circular)

    if circular
        reldiff = 1 - exp(-(smod(mean1-mean2,-pi,pi)).^2 ./ (2.*(sdev1+sdev2).^2));
    else
        reldiff = 1 - exp(-(mean1-mean2).^2 ./ (2.*(sdev1+sdev2).^2));
    end
end
