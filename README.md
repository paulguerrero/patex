# PATEX Demonstration #

Explore variations of given patterns with known structure. This is a small proof-of-concept editor based on [PATEX: Exploring Pattern Variations](http://geometry.cs.ucl.ac.uk/projects/2016/pattern-variations/).

## Setup ##

This tool requires Matlab version >= 2015b. It was tested on Windows 64 bit. It should also work on other platforms (including Unix and Mac), but was not tested there. Some recompilation may be required on these platforms (you will be asked when starting the tool). A Matlab-compatible compilation environment for your platform may then be necessary, for details go [here](http://mathworks.com/support/compilers/R2016a/index.html).

* Clone or download code into a folder.
* Download dataset available [here](http://geometry.cs.ucl.ac.uk/projects/2016/image-relationships/paper_docs/GuerreroEtAl_PATEX_SIGG16_data.zip) and place it in a folder called 'data' on the same level as the code folder (the tool expects to find a '../data' folder relative to the code folder)
* Open Matlab, navigate to the code folder and type 'restartEditor'. (At this point you may be asked to recompile if you are NOT running Windows 64 bit).

## Usage ##

### Basic Functionality ###

Use the navigate button on the top left (or press 1) to toggle navigation mode. In navigation mode, you can pan with the left mouse button and zoom with the right mouse button (or ctrl + left mouse button); reset the camera by pressing 'c'.

Select a top-level composite element by clicking on it (only when not in navigation mode), double click to select sub-parts of the composite element (similar to working with Adobe Illustrator path groups).

Drag an element to move it, rotate elements by holding '*r*' while dragging and scale elements by holding '*s*' while dragging. Holding '*x*' snaps rotations to discrete steps and movements to discrete directions from the original position.

![doc2.PNG](https://bitbucket.org/repo/GqLj8B/images/3962150298-doc2.PNG)

Show/hide elements of the UI (e.g. relationships).

![doc4.PNG](https://bitbucket.org/repo/GqLj8B/images/4216089809-doc4.PNG)

Switch between the two interaction approaches:  Autocomplete and Variation Exploration.
(The other two editors are for testing solvers and editing the pattern graph.)


### Variation Exploration Editor ###

![doc5.PNG](https://bitbucket.org/repo/GqLj8B/images/3144404385-doc5.PNG)

In this editor, individual patterns are modified in the left half of the interface and exploration takes place in the right half.

After each edit to the pattern on the left, a row gets added to the right with a list of possible variations. You can scroll through the variations in a row with the right mouse button (or ctrl + left mouse button). Double clicking a variation lets you edit the variation. Previous variations are stored, so it is possible to backtrack and select a different variation at any point. Scroll up and down through previous operations with the middle mouse button (or shift + left mouse button). 


### Autocomplete Editor ###

![doc6.PNG](https://bitbucket.org/repo/GqLj8B/images/716185250-doc6.PNG)

In the autocomplete editor, variations are overlayed directly on the canvas, so there is no need for a separate exploration panel, but it is slightly harder to get a good overview of multiple variations.

After editing a pattern element, a large set of variations is found. Moving a second element brings up one *autocomplete target* for each variation (orange dots). Dragging the element close to a target previews the variation as overlay on the canvas without interrupting the workflow. Releasing the element while hovering over an autocomplete target accepts the corresponding variation.

When pressing space, the snapping points are removed, so elements can be placed without snapping. Pressing escape while dragging an element moves it back to its original position.

## Contact ##

For any questions or comments, please contact paul.guerrero (a) ucl.ac.uk