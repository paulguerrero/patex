% three lists used extensively throughout the solvers:
% - pattern graph nodes
% - relationship nodes (a subset of the patterngraph nodes) and
% - state variables

classdef PatternSolver < handle

properties(SetAccess=protected)
    pgraph = PatternGraph.empty; % solvers never change the pattern graph!
    
    settings = struct;
end

% properties(Access=protected)
properties(SetAccess=protected)
    % all of these values are computed from the pattern graph in prepare()
    % and are only valid for the current state of the pattern graph
    
    % state vector (and state vector variance) of the pattern graph
    patternstate = zeros(1,0);
    patternstatevar = zeros(1,0);
    
    % indices of the nodes in the state
    state2nodeinds = zeros(1,0);
    node2stateinds = cell(1,0);
    
    % indices of the node fixed to original value indicators in the state
    state2nodefixoriginds = zeros(1,0);
    nodefixorig2stateinds = zeros(1,0);
    
    % indices of the node fixed to new value indicators in the state
    state2nodefixnewinds = zeros(1,0);
    nodefixnew2stateinds = zeros(1,0);
    
    % indices of the group fixed indicators in the state
    state2grpfixinds = zeros(1,0);
    grpfix2stateinds = zeros(1,0);
    
    % indices of the element poses in the state
    elmposestateinds = zeros(1,0); % one column for each element
    % and flag indicating which dimensions of the pose are circular
    elmposecircular = false(2,0); % true for the part of the poses of elements that is circular
    
    % indices of the relationship values in the state
    relstateinds = zeros(1,0);
    % and flag indicating which relationship values are circular
    relcircular = false(1,0);
    
    statecircular = false(0,1);
    
    % all relationships grouped according to type (for fast evaluation)
    relfs = cell(1,0); % pointers to functions, each function computes the values for all relationships of one type (to reduce the number of function calls)
    relfinput1inds = cell(1,0); % indices in the state vector of the first input of each function (each cell: one column for each individual relationship in the function, number of rows is dimension of each input to a relationship)
    relfinput2inds = cell(1,0); % indices in the state vector of the second input of each function (each cell: one column for each individual relationship in the function, number of rows is dimension of each input to a relationship)
    relfoutputinds = cell(1,0); % indices in the state vector of the output of each function (each cell: one column for each individual relationship in the function, one row)
    relfcircular = false(1,0); % true if function has angles as output
    relftype = cell(1,0); % function type name
    relfrelinds = cell(1,0); % index of the relationship in the list of relationships
    
    % information about groups of elements and relationships
    elmgrpelminds = cell(1,0); % elements of each element group
    relgrprelinds = cell(1,0); % relatinoships of each relationship group
    relgrpcircular = false(1,0); % is relationship group circular
    elmgrpinds = false(1,0); % which of the groups are element groups
    relgrpinds = false(1,0); % which of the groups are relationships groups
    
    % for efficiency, precompute the parents, childs and siblings of each
    % node
    parentnodeinds = cell(1,0);
    childnodeinds = cell(1,0);
    siblingnodeinds = cell(1,0);
    
    elmposevar = zeros(0,1);
end

methods(Static)

function s = defaultSettings
	s = struct(...
        'elmposedim',4,...
        'synctolerance',0.1,...
        'relgrouptype','change'); % 'change' or 'value' => relationship groups can either be relationships that change the same or that have the same value
    
%     'elmposevar',[0.4;0.4;pi/8].^2,...%[0.4;0.4;pi/16].^2,...
end
    
end

methods
    
% obj = PatternSolver
% obj = PatternSolver(pgraph)
function obj = PatternSolver(varargin)
    obj.settings = obj.defaultSettings;
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1
        obj.setPatterngraph(varargin{1});
    else
        error('Invalid arguments.');
    end
end

function setSettings(obj,settings)
    obj.settings = settings;
end

function copyFrom(obj,obj2)
    
    obj.pgraph = obj2.pgraph;
    
%     obj.settings.elmposedim = obj2.elmposedim;
%     obj.elmposevar = obj2.elmposevar;
%     
%     obj.settings.synctolerance = obj2.synctolerance;
    
    obj.settings = obj2.settings;
    
    obj.patternstate = obj2.patternstate;
    obj.patternstatevar = obj2.patternstatevar;
    
    obj.state2nodeinds = obj2.state2nodeinds;
    obj.node2stateinds = obj2.node2stateinds;
    
    obj.state2nodefixoriginds = obj2.state2nodefixoriginds;
    obj.nodefixorig2stateinds = obj2.nodefixorig2stateinds;
    
    obj.state2nodefixnewinds = obj2.state2nodefixnewinds;
    obj.nodefixnew2stateinds = obj2.nodefixnew2stateinds;
    
    obj.state2grpfixinds = obj2.state2grpfixinds;
    obj.grpfix2stateinds = obj2.grpfix2stateinds;  
    
    obj.elmposestateinds = obj2.elmposestateinds;
    obj.elmposecircular = obj2.elmposecircular;
    
    obj.relstateinds = obj2.relstateinds;
    obj.relcircular = obj2.relcircular;
    
    obj.statecircular = obj2.statecircular;
    
    obj.relfs = obj2.relfs;
    obj.relfinput1inds = obj2.relfinput1inds;
    obj.relfinput2inds = obj2.relfinput2inds;
    obj.relfoutputinds = obj2.relfoutputinds;
    obj.relfcircular = obj2.relfcircular;
    obj.relftype = obj2.relftype;
    obj.relfrelinds = obj2.relfrelinds;
    
    obj.elmgrpelminds = obj2.elmgrpelminds;
    obj.relgrprelinds = obj2.relgrprelinds;
    obj.relgrpcircular = obj2.relgrpcircular;
    obj.elmgrpinds = obj2.elmgrpinds;
    obj.relgrpinds = obj2.relgrpinds;
    
    obj.parentnodeinds = obj2.parentnodeinds;
    obj.childnodeinds = obj2.childnodeinds;
    obj.siblingnodeinds = obj2.siblingnodeinds;
end

function clear(obj)
    obj.patternstate = zeros(1,0);
    obj.patternstatevar = zeros(1,0);
    
    obj.state2nodeinds = zeros(1,0);
    obj.node2stateinds = cell(1,0);
    
    obj.state2nodefixoriginds = zeros(1,0);
    obj.nodefixorig2stateinds = zeros(1,0);
    
    obj.state2nodefixnewinds = zeros(1,0);
    obj.nodefixnew2stateinds = zeros(1,0);
    
    obj.state2grpfixinds = zeros(1,0);
    obj.grpfix2stateinds = zeros(1,0);    
    
    obj.elmposestateinds = zeros(1,0);
    obj.elmposecircular = false(2,0);
    
    obj.relstateinds = zeros(1,0);
    obj.relcircular = false(1,0);
    
    obj.statecircular = false(0,1);
    
    obj.relfs = cell(1,0);
    obj.relfinput1inds = cell(1,0);
    obj.relfinput2inds = cell(1,0);
    obj.relfoutputinds = cell(1,0);
    obj.relfcircular = false(1,0);
    obj.relftype = cell(1,0);
    obj.relfrelinds = cell(1,0);
    
    obj.elmgrpelminds = cell(1,0);
    obj.relgrprelinds = cell(1,0);
    obj.relgrpcircular = false(1,0);
    obj.elmgrpinds = false(1,0);
    obj.relgrpinds = false(1,0);
    
    obj.parentnodeinds = cell(1,0);
    obj.childnodeinds = cell(1,0);
    obj.siblingnodeinds = cell(1,0);
end

function clonePatterngraph(obj)
    % temp to store the pattern graph
    if not(isempty(obj.pgraph))
        obj.pgraph = obj.pgraph.clone;
    end
end

function setPatterngraph(obj,pgraph)
    if numel(pgraph) ~= numel(obj.pgraph) || not(all(pgraph == obj.pgraph))
        obj.clear;

        obj.pgraph = pgraph;
        obj.elmposevar = obj.affine2simpose(pgraph.elmposevar);
        obj.elmposevar = obj.elmposevar(1:obj.settings.elmposedim,:);
    end
end

% set up data structure for fast state evaluation
% (sort relationships by type and get elements)
function reltypeind = prepare(obj)
    
    [obj.patternstate,obj.patternstatevar] = PatternSolver.compute_patternstate(obj.pgraph,obj.settings.elmposedim,obj.elmposevar);
    
    obj.state2nodeinds = PatternSolver.compute_state2nodeinds(obj.pgraph,obj.settings.elmposedim);
    obj.node2stateinds = PatternSolver.compute_node2stateinds(obj.pgraph,obj.settings.elmposedim);
    
    [obj.state2nodefixoriginds,obj.state2nodefixnewinds] = PatternSolver.compute_state2nodefixinds(obj.pgraph,obj.settings.elmposedim);
    [obj.nodefixorig2stateinds,obj.nodefixnew2stateinds] = PatternSolver.compute_nodefix2stateinds(obj.pgraph,obj.settings.elmposedim);
    
    obj.state2grpfixinds = obj.compute_state2grpfixinds(obj.pgraph,obj.settings.elmposedim);
    obj.grpfix2stateinds = obj.compute_grpfix2stateinds(obj.pgraph,obj.settings.elmposedim);
    
    [obj.relftype,~,reltypeind] = unique({obj.pgraph.rels.type}); % same type also means same class
    obj.relfs = cell(1,numel(obj.relftype));
    obj.relfinput1inds = cell(1,numel(obj.relftype));
    obj.relfinput2inds = cell(1,numel(obj.relftype));
    obj.relfoutputinds = cell(1,numel(obj.relftype));
    obj.relfrelinds = cell(1,numel(obj.relftype));
    for i=1:numel(obj.pgraph.rels)
        
        % todo: add a reationships if it or any of its ancestors has a target value
        
        if isempty(obj.relfs{reltypeind(i)})
            obj.relfs{reltypeind(i)} = @(x,y) obj.pgraph.rels(i).compute_o(x,y);
        end
        
        input1nodeind = find(obj.pgraph.adjacency(obj.pgraph.rel2nodeinds(i),:) == -1);
        input2nodeind = find(obj.pgraph.adjacency(obj.pgraph.rel2nodeinds(i),:) == 1);
        outputnodeind = obj.pgraph.rel2nodeinds(i);
        if numel(input1nodeind)~=1 || numel(input2nodeind)~=1
            error('Invalid pattern graph, a relationship does not have exacty one source and target.');
        end
        
        input1stateinds = obj.node2stateinds(input1nodeind);
        input2stateinds = obj.node2stateinds(input2nodeind);
        outputstateind = obj.node2stateinds(outputnodeind);

        obj.relfinput1inds{reltypeind(i)}(:,end+1) = input1stateinds{1};
        obj.relfinput2inds{reltypeind(i)}(:,end+1) = input2stateinds{1};
        obj.relfoutputinds{reltypeind(i)}(:,end+1) = outputstateind{1};
        obj.relfrelinds{reltypeind(i)}(:,end+1) = i;
    end
    
    obj.relfcircular = false(1,numel(obj.relftype));
    for i=1:numel(obj.relftype)
        rtype = GeomRelationshipType.fromName(obj.relftype{i});
        obj.relfcircular(i) = rtype.iscircular;
    end
    
    % for elements
    elms = obj.pgraph.elms;

    obj.elmposestateinds = obj.node2stateinds(obj.pgraph.elmoffset+1 : obj.pgraph.elmoffset+numel(elms));
    obj.elmposestateinds = [obj.elmposestateinds{:}];
    obj.elmposecircular = logical([0;0;1;0]);
    obj.elmposecircular = repmat(obj.elmposecircular(1:obj.settings.elmposedim),1,numel(elms));
    
    obj.relstateinds = cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds});
    obj.relcircular = [obj.pgraph.rels.iscircular];
    
    obj.statecircular = false(size(obj.patternstate));
    obj.statecircular(obj.relstateinds) = obj.relcircular;
    obj.statecircular(obj.elmposestateinds(:)) = obj.elmposecircular(:);
    
    % for safety
    if numel(obj.relstateinds) ~= numel(obj.relcircular)
        error('Some relatinoships appear to have multiple entries in the state?');
    end
    
    
    % for groups of elements and relationships
    obj.elmgrpelminds = cell(1,0);
    obj.relgrprelinds = cell(1,0);
    obj.relgrpcircular = false(1,0);
    obj.elmgrpinds = zeros(1,0);
    obj.relgrpinds = zeros(1,0);
    for i=1:size(obj.pgraph.groups,2)
        grpnodeinds = find(obj.pgraph.groups(:,i))';
        
        grpelminds = grpnodeinds-obj.pgraph.elmoffset;
        if not(isempty(grpelminds)) && all(grpelminds > 0 & grpelminds <= numel(obj.pgraph.elms))
            obj.elmgrpelminds{end+1} = grpelminds;
%             obj.egroupmask(i) = true;
            obj.elmgrpinds(end+1) = i;
        end
        
        grprelinds = obj.pgraph.node2relinds(grpnodeinds);
        if not(isempty(grprelinds)) && not(any(isnan(grprelinds)))
            obj.relgrprelinds{end+1} = grprelinds;
            obj.relgrpcircular(end+1) = obj.pgraph.rels(grprelinds(1)).iscircular;
%             obj.rgroupmask(i) = true;
            obj.relgrpinds(end+1) = i;
        end
    end
    
    % compute parents, childs and siblings of each node
    obj.parentnodeinds = cell(1,numel(obj.pgraph.nodes));
    obj.childnodeinds = cell(1,numel(obj.pgraph.nodes));
    obj.siblingnodeinds = cell(1,numel(obj.pgraph.nodes));
    for i=1:numel(obj.pgraph.nodes)
        obj.parentnodeinds{i} = obj.pgraph.parents(i)';
        obj.childnodeinds{i} = obj.pgraph.childs(i);
        obj.siblingnodeinds{i} = obj.pgraph.siblings(i);
        obj.siblingnodeinds{i}(obj.siblingnodeinds{i} == i) = []; % remove node itself
    end
end

end

methods(Static)

function [nodefixoriginds,nodefixnewinds] = compute_state2nodefixinds(pgraph,elmposedim)
    
    selmoffset = 0;
    selmreloffset = selmoffset + numel(pgraph.elms) * elmposedim;
    smetareloffset = selmreloffset + numel(pgraph.elmrels);
    sgrpnodeoffset = smetareloffset + numel(pgraph.metarels);
    sgrpreloffset = sgrpnodeoffset + numel(pgraph.grpnodes);
    snodefixorigoffset = sgrpreloffset + numel(pgraph.grprels);
    snodefixnewoffset = snodefixorigoffset + numel(pgraph.nodes);
    sgrpfixoffset = snodefixnewoffset + numel(pgraph.nodes);
    statedim = sgrpfixoffset + size(pgraph.groups,2);
    
    nodefixoriginds = nan(1,statedim);
    nodefixnewinds = nan(1,statedim);
    
    nodefixoriginds(snodefixorigoffset+1:snodefixorigoffset+numel(pgraph.nodes)) = 1:numel(pgraph.nodes);
    nodefixnewinds(snodefixnewoffset+1:snodefixnewoffset+numel(pgraph.nodes)) = 1:numel(pgraph.nodes);
end

function [stateindsorig,stateindsnew] = compute_nodefix2stateinds(pgraph,elmposedim)
    
    selmoffset = 0;
    selmreloffset = selmoffset + numel(pgraph.elms) * elmposedim;
    smetareloffset = selmreloffset + numel(pgraph.elmrels);
    sgrpnodeoffset = smetareloffset + numel(pgraph.metarels);
    sgrpreloffset = sgrpnodeoffset + numel(pgraph.grpnodes);
    snodefixorigoffset = sgrpreloffset + numel(pgraph.grprels);
    snodefixnewoffset = snodefixorigoffset + numel(pgraph.nodes);
    
    stateindsorig = (snodefixorigoffset+1 : snodefixorigoffset+numel(pgraph.nodes));
    stateindsnew = (snodefixnewoffset+1 : snodefixnewoffset+numel(pgraph.nodes));
end

function grpfixinds = compute_state2grpfixinds(pgraph,elmposedim)
    selmoffset = 0;
    selmreloffset = selmoffset + numel(pgraph.elms) * elmposedim;
    smetareloffset = selmreloffset + numel(pgraph.elmrels);
    sgrpnodeoffset = smetareloffset + numel(pgraph.metarels);
    sgrpreloffset = sgrpnodeoffset + numel(pgraph.grpnodes);
    snodefixorigoffset = sgrpreloffset + numel(pgraph.grprels);
    snodefixnewoffset = snodefixorigoffset + numel(pgraph.nodes);
    sgrpfixoffset = snodefixnewoffset + numel(pgraph.nodes);
    statedim = sgrpfixoffset + size(pgraph.groups,2);
    
    grpfixinds = nan(1,statedim);
    
    grpfixinds(sgrpfixoffset+1:sgrpfixoffset+size(pgraph.groups,2)) = 1:size(pgraph.groups,2);
end

function stateinds = compute_grpfix2stateinds(pgraph,elmposedim)
    selmoffset = 0;
    selmreloffset = selmoffset + numel(pgraph.elms) * elmposedim;
    smetareloffset = selmreloffset + numel(pgraph.elmrels);
    sgrpnodeoffset = smetareloffset + numel(pgraph.metarels);
    sgrpreloffset = sgrpnodeoffset + numel(pgraph.grpnodes);
    snodefixorigoffset = sgrpreloffset + numel(pgraph.grprels);
    snodefixnewoffset = snodefixorigoffset + numel(pgraph.nodes);
    sgrpfixoffset = snodefixnewoffset + numel(pgraph.nodes);
    
    stateinds = (sgrpfixoffset+1 : sgrpfixoffset+size(pgraph.groups,2));
end
    
function nodeinds = compute_state2nodeinds(pgraph,elmposedim)
    
    selmoffset = 0;
    selmreloffset = selmoffset + numel(pgraph.elms) * elmposedim;
    smetareloffset = selmreloffset + numel(pgraph.elmrels);
    sgrpnodeoffset = smetareloffset + numel(pgraph.metarels);
    sgrpreloffset = sgrpnodeoffset + numel(pgraph.grpnodes);
    snodefixorigoffset = sgrpreloffset + numel(pgraph.grprels);
    snodefixnewoffset = snodefixorigoffset + numel(pgraph.nodes);
    sgrpfixoffset = snodefixnewoffset + numel(pgraph.nodes);
    statedim = sgrpfixoffset + size(pgraph.groups,2);
    
    stateinds = 1 : statedim;
    
    nodeinds = nan(1,statedim);
    
    offset = selmoffset;
    mask = stateinds > offset & stateinds <= offset + numel(pgraph.elms) * elmposedim;
    if any(mask)
        nodeinds(mask) = floor(((stateinds(mask)-offset)-1) / elmposedim)+1 + pgraph.elmoffset;
    end
    
    offset = selmreloffset;
    mask = stateinds > offset & stateinds <= offset + numel(pgraph.elmrels);
    if any(mask)
        nodeinds(mask) = (stateinds(mask)-offset) + pgraph.elmreloffset;
    end
    
    offset = smetareloffset;
    mask = stateinds > offset & stateinds <= offset + numel(pgraph.metarels);
    if any(mask)
        nodeinds(mask) = (stateinds(mask)-offset) + pgraph.metareloffset;
    end
    
    offset = sgrpnodeoffset;
    mask = stateinds > offset & stateinds <= offset + numel(pgraph.grpnodes);
    if any(mask)
        nodeinds(mask) = (stateinds(mask)-offset) + pgraph.grpnodeoffset;
    end
    
    offset = sgrpreloffset;
    mask = stateinds > offset & stateinds <= offset + numel(pgraph.grprels);
    if any(mask)
        nodeinds(mask) = (stateinds(mask)-offset) + pgraph.grpreloffset;
    end
    
    % nodefixorig state indices map to nan
end

function stateinds = compute_node2stateinds(pgraph,elmposedim)
    
    selmoffset = 0;
    selmreloffset = selmoffset + numel(pgraph.elms) * elmposedim;
    smetareloffset = selmreloffset + numel(pgraph.elmrels);
    sgrpnodeoffset = smetareloffset + numel(pgraph.metarels);
    sgrpreloffset = sgrpnodeoffset + numel(pgraph.grpnodes);
    
    nodeinds = 1 : numel(pgraph.nodes);
    stateinds = num2cell(nan(1,numel(nodeinds)));
    
    offset = pgraph.elmoffset;
    mask = nodeinds > offset & nodeinds <= offset + numel(pgraph.elms);
    if any(mask)
        stateinds(mask) = mat2cell(...
            bsxfun(@plus,(1:elmposedim)',(nodeinds(mask)-(offset+1)).*elmposedim)+selmoffset,...
            elmposedim,ones(1,sum(mask)));
    end
    
    offset = pgraph.elmreloffset;
    mask = nodeinds > offset & nodeinds <= offset + numel(pgraph.elmrels);
    if any(mask)
        stateinds(mask) = num2cell((nodeinds(mask)-offset) + selmreloffset);
    end
    
    offset = pgraph.metareloffset;
    mask = nodeinds > offset & nodeinds <= offset + numel(pgraph.metarels);
    if any(mask)
        stateinds(mask) = num2cell((nodeinds(mask)-offset) + smetareloffset);
    end
    
    offset = pgraph.grpnodeoffset;
    mask = nodeinds > offset & nodeinds <= offset + numel(pgraph.grpnodes);
    if any(mask)
        stateinds(mask) = num2cell((nodeinds(mask)-offset) + sgrpnodeoffset);
    end
    
    offset = pgraph.grpreloffset;
    mask = nodeinds > offset & nodeinds <= offset + numel(pgraph.grprels);
    if any(mask)
        stateinds(mask) = num2cell((nodeinds(mask)-offset) + sgrpreloffset);
    end
end

function [state,statevar] = compute_patternstate(pgraph,elmposedim,elmposevar)
    elmpose = [pgraph.elms.pose];
    elmpose = elmpose(1:elmposedim,:);
    
    nodefixorigvals = ones(numel(pgraph.nodes),1);
    nodefixorigvals(pgraph.elmoffset+1 : pgraph.elmoffset+numel(pgraph.elms)) = 0;
    nodefixnewvals = zeros(numel(pgraph.nodes),1);
    
    % fix all relation groups with more than one member
    grpfixvals = zeros(size(pgraph.groups,2),1);
    relgrps = pgraph.groups(pgraph.rel2nodeinds,:);
    mask = sum(relgrps~=0,1) > 1;
    grpfixvals(mask) = 1;
    
    state = cat(1,...
        elmpose(:),...
        cat(1,pgraph.elmrels.value),...
        cat(1,pgraph.metarels.value),...
        cat(1,pgraph.grpnodes.value),...
        cat(1,pgraph.grprels.value),...
        nodefixorigvals,...
        nodefixnewvals,...
        grpfixvals);
    statevar = cat(1,...
        repmat(elmposevar,numel(pgraph.elms),1),...
        cat(1,pgraph.elmrels.variance),...
        cat(1,pgraph.metarels.variance),...
        cat(1,pgraph.grpnodes.variance),...
        cat(1,pgraph.grprels.variance),...
        zeros(size(nodefixorigvals)),...
        zeros(size(nodefixnewvals)),...
        zeros(size(grpfixvals)));
end

function selmpose = affine2simpose(elmpose)
    % non-uniform to uniform size (taking the max. of both) and no mirrored
    selmpose = [elmpose(1:3,:);max(elmpose(4,:),elmpose(5,:))];
end

function aelmpose = sim2affinepose(elmpose)
    % uniform to non-uniform soze and mirrored
    aelmpose = [elmpose([1:3,4,4],:);zeros(1,size(elmpose,2))];
end
    
end

methods(Abstract)
    solve(obj);
end

end
