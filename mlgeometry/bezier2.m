% BEZIER2 Evaluate a cubic bezier spline.
%   val = BEZIER2(p1,p2,p3,p4,t) evaluates the quadratic bezier spline
%   defined by the 3 control points p1,p2,p3 at the arclength
%   parameters t.
function val = bezier2(p1,p2,p3,t)
    
    if not(isvector(p1)) || not(isvector(t))
        error('control points and arclength parameters must be vectors');
    end
    
    if min(t) < 0 || max(t) > 1
        error('t must be in [0,1]');
    end
    
    if iscolumn(t)
        t = t';
    end
    
    if isrow(p1)
        p1 = p1';
        p2 = p2';
        p3 = p3';
    end
    
    val =  p1 * ((1-t).^2) + p2 * (2.*t.*(1-t)) + p3 * (t.^2);
end