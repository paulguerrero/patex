% bpose is the pose of the coordinate system that bbmin and bbmax is in (in
% the global coordinate system)
% bmind is the minimum distance, bmaxd the farthest distance on the box to
% the line, bmint and bmaxt are the corresponding normalized distances
% along the line segemnt (from the start)
function [bmind,bmint,bmaxd,bmaxt] = linesegBoxDistance3D(...
    linesegstart,linesegend,bmin,bmax,bpose)

    if size(bpose,1) < 7
        bpose(4,:) = 1;
        bpose(5,:) = 0;
        bpose(6,:) = 0;
        bpose(7,:) = 0;
    end
    if size(bpose,1) < 9
        bpose(8,:) = 1;
        bpose(9,:) = 1;
        bpose(10,:) = 1;
    end
    if size(bpose,1) < 11
        bpose(11,:) = 0;
    end
    
    % can't transform line segments, because then distances would not be
    % right because of scaling (would need to scale back by transforming
    % the line between the closest points back, otherwise non-uniform
    % scaling wouldn't work)
%     % to local coordinte systems of boxes
%     linesegstart = transform3Dinv(linesegstart,bpose(8:10,:),bpose(4:7,:),bpose(1:3,:),bpose(11,:));
%     linesegend = transform3Dinv(linesegend,bpose(8:10,:),bpose(4:7,:),bpose(1:3,:),bpose(11,:));
    
    [bverts,bfaces] = trimeshAACuboid(bmin,bmax);
    bbvertindoffsets = reshape((0:size(bmin,2)-1).*8,1,1,[]);
    bfaces = bsxfun(@plus,reshape(bfaces,3,12,[]),bbvertindoffsets);
    bfaces = reshape(bfaces,3,[]);
    
    bpose = reshape(repmat(bpose,8,1),size(bpose,1),[]); % same number as vertices
    
    bverts = transform3D(bverts,bpose(8:10,:),bpose(4:7,:),bpose(1:3,:),bpose(11,:));
    
    [bmind,bmint] = linesegTriangleDistance3D_mex(...
        linesegstart,linesegend,...
        bverts(:,bfaces(1,:)),...
        bverts(:,bfaces(2,:)),...
        bverts(:,bfaces(3,:)));
    bmind = reshape(bmind,12,[]);
    bmint = reshape(bmint,12,[]);
    [bmind,ind] = min(bmind,[],1);
    bmint = bmint(sub2ind(size(bmint),ind,1:size(bmint,2)));

    if nargout >= 3
        % unlike minimum distance, maximum distance is always at a vertex
        % of the bounding box
        [bmaxd,bmaxt] = pointLinesegDistance3D_mex(bverts,linesegstart,linesegend);
        bmaxd = reshape(bmaxd,8,[]);
        bmaxt = reshape(bmaxt,8,[]);
        [bmaxd,ind] = max(bmaxd,[],1);
        bmaxt = bmaxt(sub2ind(size(bmaxt),ind,1:size(bmaxt,2)));
    end
end
