% v1 : d x 1
% v2 : d x n
% weight : d x 1 or d x n
% method : string
function [d,delm,usedparams] = weighteddist(v1,v2,weights,method,varargin)

    if isempty(weights)
        weights = ones(size(v2));
    end
    
    if size(v1,2) ~= size(v2,2)
        v1 = bsxfun(@times,v1,ones(1,size(v2,2)));
    end
    if size(weights,2) ~=  size(v2,2)
        weights = bsxfun(@times,weights,ones(1,size(v2,2)));
    end
    
    usedparams = 0;
    
    if strcmp(method,'chisquare')
        % --- Chi-square distance:
        % = L2 distance, but instead of the absolute squared difference of
        % bins, the squared difference relative to the number of entries in the
        % template bins
        delm = ((v2-v1).^2 ./ v2) .* weights;
        d = 0.5 .* sum(delm,1);

    elseif strcmp(method,'chisquare2')
        % --- Chi-square distance(like in shape context paper):
        delm = ((v2-v1).^2 ./ (v2+v1)) .* weights;
        d = 0.5 .* sum(delm,1);
        
    elseif strcmp(method,'L2dist')
        
        delm = (v2-v1).^2 .* weights;
        d = sqrt(sum(delm,1));
   
    elseif strcmp(method,'L1dist')
        
        delm = abs(v2-v1) .* weights;
        d = sum(delm,1);
        
    elseif strcmp(method,'SL0dist')
        % additional input argument: variance of gaussians (scalar, d x 1 or d x n)
        usedparams = 1;
        
        if numel(varargin{1}) ~= 1 && size(varargin{1}) == 1
            varargin{1} = bsxfun(@times,varargin{1},ones(1,size(v2,2)));
        end
        delm = 1 - exp((v1-v2).^2 ./ (2.*varargin{1}));
        d = sum(delm,1);
   
    elseif strcmp(method,'EMD')
        % additional input argument: ground distance between bins
        usedparams = 2;
        
        % extra mass penalty means that additional earth that has to be
        % transported to the histogram or away from it travels the distance
        % given in the penalty. Setting it to -1 means the distance is the
        % maximum of any distance between bins.
        if numel(varargin) < 2 || isempty(varargin{2})
            masspen = -1;
        else
            masspen = varargin{2};
        end
        
        d = zeros(1,size(v2,2));
        delm = zeros(size(v2));
        if nargout >= 2
            for i=1:size(v2,2)
                if mod(i,100) == 0
                    disp(i);
                end
                [d(i),F] = emd_hat_gd_metric_mex(v1(:,i),v2(:,i),varargin{1},masspen,2);
                delm(:,i) = max(v1(:,i),v2(:,i))-diag(F); % flow not going to the same bin
            end
        else
            for i=1:size(v2,2)
                d(i) = emd_hat_gd_metric_mex(v1(:,i),v2(:,i),varargin{1},masspen,1);
            end
        end
    else
        error('Unknown distance method.');
    end
end
