function [x,y] = polygonCircle(cx,cy,r,in4,mode,startangle)
    if nargin < 4 || isempty(in4)
        nsamples = 64;
    else
        if nargin < 5 || isempty(mode)
            mode = 'samplecount';
        end
        if strcmp(mode,'samplecount')
            nsamples = in4;
        elseif strcmp(mode,'maxerror')
            maxangle = acos((r-in4)/r)*2;
            nsamples = max(3,ceil((2*pi)/maxangle));
        else
            error('Unkown polygon circle mode.');
        end
    end
    
    if nargin < 6 || isempty(startangle)
        startangle = 0;
    end

    angles = linspace(startangle,startangle+2*pi,nsamples+1);
    [x,y] = pol2cart(angles(1:end-1),abs(r));
    x = x + cx;
    y = y + cy;
end
