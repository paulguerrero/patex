function [curv,alen,originds] = polylineCurvature(poly,closed,errbound,additionalsamples)

    if closed
        nextp = [poly(:,2:end),poly(:,1)];
        lastp = [poly(:,end),poly(:,1:end-1)];
    else
        nextp = [poly(:,2:end),poly(:,end).*2-poly(:,end-1)];
        lastp = [poly(:,1).*2-poly(:,2),poly(:,1:end-1)];
    end

    if nargin < 4
        additionalsamples = false;
    end
    
    if nargin < 3
        tolastp = lastp-poly;
        tonextp = nextp-poly;
        
        % compute curvature and normal
        tolastp_mag = sqrt(tolastp(1,:).^2 + tolastp(2,:).^2);
        tonextp_mag = sqrt(tonextp(1,:).^2 + tonextp(2,:).^2);
        tolastp = tolastp ./ repmat(tolastp_mag,2,1);
        tonextp = tonextp ./ repmat(tonextp_mag,2,1);
        curv = (tolastp+tonextp) .* repmat((2./(tolastp_mag+tonextp_mag)),2,1);
        curv = sqrt(curv(1,:).^2 + curv(2,:).^2);

        orth = [tolastp(2,:);-tolastp(1,:)];

        % sign of the curvature and normal: dot of tonextp with vector orthonormal to tolastp
        s = sign(dot(tonextp,orth,1));
        s(s==0) = 1;
        curv = curv .* s;
        
        if nargout > 1
            alen = polylineArclen(poly,closed);
        end
    else
        % fit osculating circles
        [r,c,type] = fitcircle(lastp,poly,nextp, errbound);
        
        curv = 1./r;
        curv(isinf(r)) = 0; % just to be safe

        if additionalsamples
            if not(closed)
                error('Additional samples not yet implemented for open polylines.');
            end

            subd1 = type == 2 | type == 4;
            subd2 = type == 2 | type == 3;
            p1 = lastp;
            p2 = poly;
            p3 = nextp;

            % segend = [p21(:,subd1),p23(:,subd2)];
            % sc = [c(:,subd1),c(:,subd2)]-[p2(:,subd1),p2(:,subd2)];
            segs = [[p2(:,subd1);p1(:,subd1)],[p2(:,subd2);p3(:,subd2)]];
            sc = [c(:,subd1),c(:,subd2)];
            sr = [r(subd1),r(subd2)];

            ip = nan(2,size(segs,2));
            t = nan(1,size(segs,2));
            for i=1:size(segs,2)
            %     [ip1,ip2,t1,t2] = linesegCircleIntersection([0;0],segend(:,i),sc(:,i),sr(i));
                [ip1,ip2,t1,t2] = linesegCircleIntersection(segs(1:2,i),segs(3:4,i),sc(:,i),sr(i));
                if t1 > t2 && not(isnan(ip1(1)))
                    ip(:,i) = ip1;
                    t(i) = t1;
                elseif not(isnan(ip2(1)))
                    ip(:,i) = ip2;
                    t(i) = t2;
                end
            end
            % ip = ip + [p2(:,subd1),p2(:,subd2)];
            % reverse t in segments that have been reversed relative to the polygon
            % boundary winding
            t(1:sum(subd1)) = 1-t(1:sum(subd1));

            sinds = [find(subd1)-1,find(subd2)];
            sinds(sinds<1) = sinds(sinds<1) + size(poly,2);
            sinds(sinds>size(poly,2)) = sinds(sinds>size(poly,2)) - size(poly,2);
            sinds = sinds + t;

            nanmask = isnan(t);
            sinds(nanmask) = [];
            ip(:,nanmask) = [];
    %         t(nanmask) = [];

            newpoly = [poly,ip];
            newcurv = [curv,zeros(1,size(ip,2))];
            originds = [1:size(poly,2),nan(1,size(ip,2))];
            [~,perm] = sort([1:size(poly,2),sinds],'ascend');
            newpoly = newpoly(:,perm);
            curv = newcurv(perm);
            originds = originds(perm);
            alen = polylineArclen(newpoly,closed);
        else
            if nargout > 1
                alen = polylineArclen(poly,closed);
            end
        end
    end

end
