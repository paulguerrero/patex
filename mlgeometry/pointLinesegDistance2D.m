function [d,f,px,py] = pointLinesegDistance2D(x,y,x1,y1,x2,y2)

    if nargin ~= 4 && nargin ~= 6
        error('Must provide exactly 4 or 6 inputs.'),
    end
    
    if nargout ~= 1 && nargout ~= 4
        error('Must provide exactly 1 or 4 outputs.'),
    end
    
    if nargin == 4
        x2 = x1(2:end);
        y2 = y1(2:end);
        x1(end) = [];
        y1(end) = [];
    end
    
    d = zeros(numel(x),numel(x1));
    
    if nargout > 1
        f = zeros(numel(x),numel(x1));
        px = zeros(numel(x),numel(x1));
        py = zeros(numel(x),numel(x1));
    end
    
    for i=1:numel(x)
        % test if point (x,y) projected to the line is outside the segment x1,x2
        o1m = (x2 == x1 & y2 == y1) | ...                    % degenerate linesegment (zero length)
              ((x2-x1).*(x(i)-x1) + (y2-y1).*(y(i)-y1) < 0); %(p1 to p) projected to the segment (p1 to p2)  
        o2m = (x2-x1).*(x(i)-x2) + (y2-y1).*(y(i)-y2) > 0;   %(p2 to p) projected to the segment (p1 to p2)  
        im = not(o1m | o2m);

        ad = inf(1,numel(x1));
        ad(im) = abs((x2(im)-x1(im)).*(y1(im)-y(i)) - (y2(im)-y1(im)).*(x1(im)-x(i))) ./ ...
                sqrt((x2(im)-x1(im)).^2 + (y2(im)-y1(im)).^2);
        ad(o1m) = sqrt((x(i)-x1(o1m)).^2 + (y(i)-y1(o1m)).^2);
        ad(o2m) = sqrt((x(i)-x2(o2m)).^2 + (y(i)-y2(o2m)).^2);

        d(i,:) = ad;

        if nargout > 1
            % im case
            f(i,im) = ((x2(im)-x1(im)).*(x(i)-x1(im)) + (y2(im)-y1(im)).*(y(i)-y1(im))) ./ ...
                      ((x2(im)-x1(im)).^2             + (y2(im)-y1(im)).^2);
            px(i,im) = x1(im).*(1-f(i,im)) + x2(im).*f(i,im);
            py(i,im) = y1(im).*(1-f(i,im)) + y2(im).*f(i,im);
            
            % o1m case
            f(i,o1m) = 0;
            px(i,o1m) = x1(o1m);
            py(i,o1m) = y1(o1m);
            
            % o2m case
            f(i,o2m) = 1;
            px(i,o2m) = x2(o2m);
            py(i,o2m) = y2(o2m);
        end
    end
 
end
