function I = polygonRasterization3(I,x,y,z,v,zcircular)
    
    % todo if ciruclar: vertices +/- (size(I,3)-1) so that
    % - all vertices in an interval no larger than (size(I,3)-1)
    % - area of polygon minimal
    % in triangle rast. when setting I values, mod the coord. with
    % (size(I,3)-1) before rounding and using as array index

    if nargin < 6 || isempty(zcircular)
        zcircular = false;
    end
    
    % construct in 2D and attach 3rd coordinate later, triangles do
    % not really need to be delaunay, just covering the polygon
    % polygon projection to (x,y) is assumed to be a valid 2D polygon
    tri = delaunayTriangulation(x',y',[1:numel(x);[2:numel(x),1]]');
    inds = find(tri.isInterior);
    
    for i=inds'
        % find the normal of the triangle and do standard 2D rasterization
        % of the triangle in the coordinate plane of maximum extent
        I = triangleRasterization3(I,...
            x(tri.ConnectivityList(i,:)),...
            y(tri.ConnectivityList(i,:)),...
            z(tri.ConnectivityList(i,:)),...
            v(:,tri.ConnectivityList(i,:)),...
            zcircular);
    end
end
