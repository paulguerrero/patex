% adjacency: adjacency matrix of directed acyclic graph, where nonzero
% entries (i,j) indicate an edge from i to j (and i is considered the
% parent of j)
% nodeinds: all descendants of these nodes are returned
function dinds = graphDescendants(adjacency,nodeinds)
    nodemask = logical(sparse(ones(1,numel(nodeinds)),nodeinds,ones(1,numel(nodeinds)),1,size(adjacency,1)));
    dmask = logical(sparse(1,size(adjacency,1)));
    
    while any(nodemask)
        nodemask = logical(nodemask*abs(adjacency));
        
        nodemask(dmask) = false; % has already been visited
        
        dmask = dmask | nodemask;
    end
    
    dinds = find(dmask);
end
