% p: 2 x 1 or 2 x n matrix of 1 or n 2-dimensional points
% or 6 x 1 or 6 x n matrix of 1 or n 6-dimensional poses in 2D space (2 position, 1 angle, 2 scale, 1 mirror)
% order: inverse translate, inverse rotate, inverse scale, mirror (= inverse mirror)
% scale: 2 x n or 2 x 1 matrix of 2-dimensional scale vectors
% angle: 1 x n or 1 x 1 matrix of angles (in radians)
% trans: 2 x n or 2 x 1 matrix of 2-dimensional translations
% optional: mirror: 1 x n or 1 x 1 matrix of mirroring flags
% (mirroring is about x axis: y = -y)
function tp = transform2Dinv(p, scale, angle, trans, mirror)

    tp = bsxfun(@times,ones(1,size(scale,2)),p);

    tp(1:2,:) = bsxfun(@minus,p(1:2,:),trans);

    % matrix multiplication written out (so i can multiply n matrices with n vectors)
    cosangle = cos(-angle);
    sinangle = sin(-angle);
    invscale = 1./scale;
    tp(1:2,:) = [(invscale(1,:).*cosangle).*tp(1,:) + (invscale(1,:).*-sinangle).*tp(2,:); ...
                 (invscale(2,:).*sinangle).*tp(1,:) + (invscale(2,:).* cosangle).*tp(2,:)];
      
    if size(tp,1) >= 3
        tp(3,:) = smod(bsxfun(@plus,tp(3,:),-angle),-pi,pi);
    end
    
    if size(tp,1) >= 5
        tp(4:5,:) = bsxfun(@times,tp(4:5,:),invscale);
    end

    if nargin >= 5 && any(mirror)
        % mirror about x axis
        tp(2,:) = tp(2,:).*(logical(mirror).*-2+1);
        
        if size(tp,1) >= 3
            % negative angle (because dir = x-axis is 0 angle)
            tp(3,:) = tp(3,:).*(logical(mirror).*-2+1);
        end

        if size(tp,1) >= 6
            tp(6,:) = bsxfun(@xor,tp(6,:),mirror);
        end
    end
end
