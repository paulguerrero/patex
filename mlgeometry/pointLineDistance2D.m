% should work for any number of dimensions
% t is normalized, tabs unnormalized
function [d,t,tabs] = pointLineDistance(p,p1,p2)
    lineseglen = sqrt(sum((p2-p1).^2,1))';
    
    zmask = lineseglen == 0;
    nzmask = ~zmask;
    linesegdir = zeros(size(p1,2),size(p1,1));
    linesegdir(nzmask,:) = bsxfun(@rdivide,(p2(:,nzmask)-p1(:,nzmask))',lineseglen);

    tabs = zeros(size(p1,2),size(p,2));
    for i=1:size(p,1)
        tabs = tabs + ...
            bsxfun(@times,...
                bsxfun(@minus,p(i,:),p1(i,:)'),...
                linesegdir(:,i));
    end
    
    d = zeros(size(p1,2),size(p,2));
    for i=1:size(p,1)
        cpi = bsxfun(@plus,...
            bsxfun(@times,tabs,linesegdir(:,i)),...
            p1(i,:)');
        d = d + bsxfun(@minus,p(i,:),cpi).^2;
    end
    d = sqrt(d);
    
    if nargout >= 2
        t = bsxfun(@rdivide,tabs,lineseglen);
    end
    
%     tabs = zeros(size(p1,2),size(p,2));
%     d = zeros(size(p1,2),size(p,2));
%     t = zeros(size(p1,2),size(p,2));    
%     for i=1:size(p1,2)
%         tabs(i,:) = sum( bsxfun(@times,bsxfun(@minus,p,p1(:,i)),linesegdir(i,:)'), 1); % distance from p1 to closest point on line (projection of p-p1 to line)
%         cp = bsxfun(@plus,p1(:,i),bsxfun(@times,linesegdir(i,:)',tabs(i,:))); % closest point on line
%         d(i,:) = sqrt(sum((p-cp).^2,1)); % distance between p and closest point on line
%         t(i,:) = bsxfun(@times,tabs(i,:),1./lineseglen(i));
%     end
end
