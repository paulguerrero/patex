% this assumes faces are ccw when viewed from the front
function fn = trimeshFaceNormals(verts,faces)
    e1 = verts(:,faces(2,:)) - verts(:,faces(1,:));
    e2 = verts(:,faces(3,:)) - verts(:,faces(1,:));
    fn = cross(e1, e2, 1);
    fn = bsxfun(@times,fn,1./sqrt(sum(fn.^2,1)));
end
