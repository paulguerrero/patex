#include "mex.h"

#include<vector>

#include "mexutils.h"
#include "geom.h"

// itype = polygonPolygonIntersection_mex(poly1,poly2)
// poly1 is first set of polygons, either as 2xn vertices or cell array of vertices
// poly2 is second set of polygons, either as 2xn vertices or cell array of vertices
// itype: intersection type:
// 0: completely separate
// 1: 1 completely inside 2
// 2: 2 completely inside 1
// 3: intersecting

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    if(nrhs != 2) {
        mexErrMsgTxt("Two inputs required.");
    }
    
    mwSize dim ,n, nl1, nl2;
    
    mwIndex inputind = 0;

    // read polygon set 1
    std::vector<std::vector<double> > p1;
    if (mxIsClass(prhs[inputind],"cell")) {
        readDoubleNDVectorsetCellArray(prhs[inputind],2,p1);
    } else {
        p1.resize(1);
        readDoubleNDVectorset(prhs[inputind],2,p1[0]);
    }
    inputind++;
    
    // read polygon set 2
    std::vector<std::vector<double> > p2;
    if (mxIsClass(prhs[inputind],"cell")) {
        readDoubleNDVectorsetCellArray(prhs[inputind],2,p2);
    } else {
        p2.resize(1);
        readDoubleNDVectorset(prhs[inputind],2,p2[0]);
    }
    inputind++;
    
    if(nlhs != 1) {
        mexErrMsgTxt("One output required.");
    }
    
    std::vector<Intersectiontype> itype(p1.size()*p2.size());
    polygonsetPolygonsetIntersection2D(
            p1.begin(),p1.end(),
            p2.begin(),p2.end(),
            itype.begin(),itype.end());
    
    // write outputs
    plhs[0] = mxCreateDoubleMatrix(p1.size(),p2.size(),mxREAL);
    std::copy(itype.begin(),itype.end(),mxGetPr(plhs[0]));
    itype.clear();
}
