// function d = pointLinesegDistance2D_mex(p,segstart,segend)
// function [d,f] = pointLinesegDistance2D_mex(p,segstart,segend)

#include "mex.h"

#include "geom.h"

#include "mexutils.h"

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    // check number of inputs and outputs
    if(nrhs != 3) {
        mexErrMsgTxt("Three inputs required.");
    }
    if(nlhs < 1 || nlhs > 2) {
        mexErrMsgTxt("One or two outputs required.");
    }
    
    mwSize dim ,n, np, ns;
    
    // read points
    double* p;
    readDoubleMatrix(prhs[0], p, dim, np);
    if (dim != 2) {
        mexErrMsgTxt("Points must be two-dimensional.");
    }
    
    // read line segment start vertices
    double* ss;
    readDoubleMatrix(prhs[1], ss, dim, ns);
    if (dim != 2) {
        mexErrMsgTxt("Line segments must be two-dimensional.");
    }
    
    // read line segment end vertices
    mwSize nes;
    double* se;
    readDoubleMatrix(prhs[2], se, dim, nes);
    if (dim != 2) {
        mexErrMsgTxt("Line segments must be two-dimensional.");
    }
    if (nes != ns) {
        mexErrMsgTxt("Different number of start- and end vertices.");
    }
    
    // allocate outputs
    double* d = NULL;
    double* f = NULL;
    plhs[0] = mxCreateDoubleMatrix(np,ns, mxREAL);
    d = mxGetPr(plhs[0]);
    if (nlhs >= 2) {
        plhs[1] = mxCreateDoubleMatrix(np,ns, mxREAL);
        f = mxGetPr(plhs[1]);
    }

    // do distance calculations
    Vector pv(0,0,0);
    Lineseg ls(Vector(0,0,0),Vector(0,0,0));
    for (mwIndex j=0; j<ns; ++j) {
        
        ls.P0.x = ss[j*2];
        ls.P0.y = ss[j*2+1];
        
        ls.P1.x = se[j*2];
        ls.P1.y = se[j*2+1];
        
        for (mwIndex i=0; i<np; ++i) {
        
            pv.x = p[i*2];
            pv.y = p[i*2+1];

            if (f != NULL) {
                d[j*np+i] = pointLinesegDistance2D(
                    pv,ls,f[j*np+i]);
            } else {
                double fdummy;
                d[j*np+i] = pointLinesegDistance2D(
                    pv,ls,fdummy);
            }
        }
    }

}
