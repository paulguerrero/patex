% order: mirror, scale, rotate
% scale: 3 x n or 3 x 1 matrix of 3-dimensional scale vectors
% angle: 4 x n or 4 x 1 matrix of quaternions
% optional: mirror: 1 x n or 1 x 1 matrix of mirroring flags
% (mirroring is about xz plane: y = -y)
% returns 3 x 3 x n 3D normal transformation matrices
function m = transformNormalMat3D(scale, angle, mirror)

    scale = reshape(scale,3,1,[]);
%     angle = reshape(angle,4,1,[]);
    
    if nargin >= 3
        mirror = reshape(mirror,1,1,[]);
        mir = (logical(mirror).*-2+1);
    else
        mir = ones(1,1,size(scale,3));
    end    

    % matrix multiplication written out (so i can multiply n matrices with n vectors)
    rot = permute(quat2dcm(angle'),[2,1,3]);
    invscale = 1./scale;
    m = [rot(1,1,:).*invscale(1,:,:), rot(1,2,:).*invscale(2,:,:).*mir, rot(1,3,:).*invscale(3,:,:); ...
         rot(2,1,:).*invscale(1,:,:), rot(2,2,:).*invscale(2,:,:).*mir, rot(2,3,:).*invscale(3,:,:); ...
         rot(3,1,:).*invscale(1,:,:), rot(3,2,:).*invscale(2,:,:).*mir, rot(3,3,:).*invscale(3,:,:)];
end
