function [bbmin,bbmax,diag] = pointsetBoundingbox(p)
    if iscell(p)
        bbmin = min(cell2mat(cellfun(@(x) min(x,[],2),p,'UniformOutput',false)),[],2);
        bbmax = max(cell2mat(cellfun(@(x) max(x,[],2),p,'UniformOutput',false)),[],2);
    else
        bbmin = min(p,[],2);
        bbmax = max(p,[],2);
    end
    if nargout > 2
        diag = sqrt(sum((bbmax-bbmin).^2,1));
    end
end
