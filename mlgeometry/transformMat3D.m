% order: mirror, scale, rotate, translate
% scale: 3 x n or 3 x 1 matrix of 3-dimensional scale vectors
% angle: 4 x n or 4 x 1 matrix of quaternions
% trans: 3 x n or 3 x 1 matrix of 3-dimensional translations
% optional: mirror: 1 x n or 1 x 1 matrix of mirroring flags
% (mirroring is about xz plane: y = -y)
% returns 4 x 4 x n 3D transformation matrices
function m = transformMat3D(scale, angle, trans, mirror)

    scale = reshape(scale,3,1,[]);
%     angle = reshape(angle,4,1,[]);
    trans = reshape(trans,3,1,[]);
    
    if nargin >= 4
        mirror = reshape(mirror,1,1,[]);
        mir = (logical(mirror).*-2+1);
    else
        mir = ones(1,1,size(scale,3));
    end

    % matrix multiplication written out (so i can multiply n matrices with n vectors)
    rot = permute(quat2dcm(angle'),[2,1,3]);
    m = [rot(1,1,:).*scale(1,:,:), rot(1,2,:).*scale(2,:,:).*mir, rot(1,3,:).*scale(3,:,:), trans(1,:,:); ...
         rot(2,1,:).*scale(1,:,:), rot(2,2,:).*scale(2,:,:).*mir, rot(2,3,:).*scale(3,:,:), trans(2,:,:); ...
         rot(3,1,:).*scale(1,:,:), rot(3,2,:).*scale(2,:,:).*mir, rot(3,3,:).*scale(3,:,:), trans(3,:,:); ...
         zeros(1,3,size(scale,3)), ones(1,1,size(scale,3))];
end
