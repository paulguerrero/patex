function [x,y] = polygonEllipse(cx,cy,rx,ry,in4,mode)
    if nargin < 5
        nsamples = 64;
    else
        if nargin < 6 || isempty(mode)
            mode = 'samplecount';
        end
        if strcmp(mode,'samplecount')
            nsamples = in4;
        elseif strcmp(mode,'maxerror')
            % the maximum error when sampling with equal polar angle is the
            % same as for a circle with radius max(rx,ry) (?)
            rmax = max(rx,ry);
            maxangle = acos((rmax-in4)/rmax)*2;
            nsamples = max(3,ceil((2*pi)/maxangle));
        else
            error('Unkown polygon circle mode.');
        end
    end
    
    angles = linspace(0,2*pi,nsamples+1);
	angles(end) = [];
    [x,y] = pol2cart(angles,1);
    x = x.*rx;
    y = y.*ry;

    x = x + cx;
    y = y + cy;
end
