#include "mex.h"

#include<vector>

#include "mexutils.h"
#include "geom.h"

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    if(nrhs != 6) {
        mexErrMsgTxt("Six inputs required.");
    }
    if(nlhs != 1) {
        mexErrMsgTxt("One output required.");
    }
    
    mwSize dim ,n, nt1, nt2;
    
    // check first triangle set arguments
    doubleMatrixSize(prhs[0], dim, nt1);
    double* t1p1 = mxGetPr(prhs[0]);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    doubleMatrixSize(prhs[1], dim, n);
    double* t1p2 = mxGetPr(prhs[1]);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    if (n != nt1) {
        mexErrMsgTxt("Point sets for the first triangle vertices do not match.");
    }
    
    doubleMatrixSize(prhs[2], dim, n);
    double* t1p3 = mxGetPr(prhs[2]);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    if (n != nt1) {
        mexErrMsgTxt("Point sets for the first triangle vertices do not match.");
    }
    
    // check second triangle set arguments
    doubleMatrixSize(prhs[3], dim, nt2);
    double* t2p1 = mxGetPr(prhs[3]);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    doubleMatrixSize(prhs[4], dim, n);
    double* t2p2 = mxGetPr(prhs[4]);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    if (n != nt2) {
        mexErrMsgTxt("Point sets for the first triangle vertices do not match.");
    }
    doubleMatrixSize(prhs[5], dim, n);
    double* t2p3 = mxGetPr(prhs[5]);
    if (dim != 3) {
        mexErrMsgTxt("Input must be three-dimensional.");
    }
    if (n != nt2) {
        mexErrMsgTxt("Point sets for the first triangle vertices do not match.");
    }
    
    // create first triangle set
    std::vector<Triangle> triset1(nt1);
    for (mwIndex i=0; i<nt1; i++) {
        triset1[i].P0.x = t1p1[i*3];
        triset1[i].P0.y = t1p1[i*3+1];
        triset1[i].P0.z = t1p1[i*3+2];
        
        triset1[i].P1.x = t1p2[i*3];
        triset1[i].P1.y = t1p2[i*3+1];
        triset1[i].P1.z = t1p2[i*3+2];
        
        triset1[i].P2.x = t1p3[i*3];
        triset1[i].P2.y = t1p3[i*3+1];
        triset1[i].P2.z = t1p3[i*3+2];
    }
    
    // create second triangle set
    std::vector<Triangle> triset2(nt2);
    for (mwIndex i=0; i<nt2; i++) {
        triset2[i].P0.x = t2p1[i*3];
        triset2[i].P0.y = t2p1[i*3+1];
        triset2[i].P0.z = t2p1[i*3+2];
        
        triset2[i].P1.x = t2p2[i*3];
        triset2[i].P1.y = t2p2[i*3+1];
        triset2[i].P1.z = t2p2[i*3+2];
        
        triset2[i].P2.x = t2p3[i*3];
        triset2[i].P2.y = t2p3[i*3+1];
        triset2[i].P2.z = t2p3[i*3+2];
    }
    
    // run function
    bool intersect = trianglesetTrianglesetIntersection3D(triset1,triset2);
    
    // write output
    plhs[0] = mxCreateLogicalScalar(intersect);
}
