function [x,y] = bary2cart(b1,b2,x1,x2,x3,y1,y2,y3)
    x = x1.*b1 + x2.*b2 + (1-(b1+b2)) .* x3;
    y = y1.*b1 + y2.*b2 + (1-(b1+b2)) .* y3;
end
