% p: 3 x n matrix of n 3-dimensional normals
% order: inverse rotate, inverse scale, mirror (= inverse mirror)
% scale: 3 x n or 3 x 1 matrix of 3-dimensional scale vectors
% angle: 4 x n or 4 x 1 matrix of quaternions
% optional: mirror: 1 x n or 1 x 1 matrix of mirroring flags
% (mirroring is about xz plane: y = -y)
function tn = transformNormal3Dinv(n, scale, angle, mirror)

    % matrix multiplication written out (so i can multiply n matrices with n vectors)
    invrot = permute(permute(quat2dcm(quatconj(angle')),[2,1,3]),[1,3,2]);
    tn = [(scale(1,:).*invrot(1,:,1)).*n(1,:) + (scale(1,:).*invrot(1,:,2)).*n(2,:) + (scale(1,:).*invrot(1,:,3)).*n(3,:); ...
          (scale(2,:).*invrot(2,:,1)).*n(1,:) + (scale(2,:).*invrot(2,:,2)).*n(2,:) + (scale(2,:).*invrot(2,:,3)).*n(3,:); ...
          (scale(3,:).*invrot(3,:,1)).*n(1,:) + (scale(3,:).*invrot(3,:,2)).*n(2,:) + (scale(3,:).*invrot(3,:,3)).*n(3,:)];
      
    if nargin >= 4 && any(mirror)
        % mirror about xz plane
        tn(2,:) = tn(2,:).*(logical(mirror).*-2+1);
    end
      
    % re-normalize
    tn = bsxfun(@times,tn,1./sqrt(sum(tn.^2,1)));
end
