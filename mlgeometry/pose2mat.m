function m = pose2mat(targetpose,currentpose,uniformscale)
    % if no current pose is given, assume it is:
    %    currentpose = [...
    %        0;0;... % position
    %        0;...   % rotation
    %        1;...   % scale
    %        0];     % mirrored
    
    if nargin < 3
        uniformscale = false;
    end
    
    % convert to non-uniform scale format
    if uniformscale
        if size(targetpose,1) == 8 || size(targetpose,1) == 9
            targetpose = targetpose([1:7,8,8,8,9:end],:);
        elseif size(targetpose,1) == 4 || size(targetpose,1) == 5
            targetpose = targetpose([1:3,4,4,5:end],:);
        else
            error('Invalid pose format.');
        end
        if nargin >= 3 && not(isempty(currentpose))
            if size(currentpose,1) == 8 || size(currentpose,1) == 9
                currentpose = currentpose([1:7,8,8,8,9:end],:);
            elseif size(currentpose,1) == 4 || size(currentpose,1) == 5
                currentpose = currentpose([1:3,4,4,5:end],:);
            else
                error('Invalid pose format.');
            end
        end
    end
   
    if nargin >= 2 && not(isempty(currentpose))
        if size(targetpose,1) ~= size(currentpose,1)
            error('Pose format does not match for current and target poses.');
        end
        
        % avoid problems with scaling if both sizes are close to zero
        mask = currentpose(4,:) < eps('double')*100 & targetpose(4,:) < eps('double')*100;
        if any(mask)
            currentpose(4,mask) = 1;
            targetpose(4,mask) = 1;
        end
        
        if size(currentpose,1) == 11
            % pose with mirroring
            minv = transformMat3Dinv(currentpose(8:10,:), currentpose(4:7,:), currentpose(1:3,:), currentpose(11,:));
        elseif size(currentpose,1) == 10
            minv = transformMat3Dinv(currentpose(8:10,:), currentpose(4:7,:), currentpose(1:3,:));
        elseif size(currentpose,1) == 6
            % pose with mirroring
            minv = transformMat2Dinv(currentpose([4;4],:), currentpose(3,:), currentpose(1:2,:), currentpose(6,:));
        elseif size(currentpose,1) == 5
            minv = transformMat2Dinv(currentpose([4;4],:), currentpose(3,:), currentpose(1:2,:));
        else
            error('Invalid pose format.');
        end
    end
    
    if size(targetpose,1) == 11
        % pose with mirroring
        m = transformMat3D(targetpose(8:10,:), targetpose(4:7,:), targetpose(1:3,:), targetpose(11,:));
    elseif size(targetpose,1) == 10
        m = transformMat3D(targetpose(8:10,:), targetpose(4:7,:), targetpose(1:3,:));
    elseif size(targetpose,1) == 6
        % pose with mirroring
        m = transformMat2D(targetpose(4:5,:), targetpose(3,:), targetpose(1:2,:), targetpose(6,:));
    elseif size(targetpose,1) == 5
        m = transformMat2D(targetpose(4:5,:), targetpose(3,:), targetpose(1:2,:));
    else
        error('Invalid pose format.');
    end
    
    if nargin >= 2 && not(isempty(currentpose))
        for i=1:size(m,3)
            m(:,:,i) = m(:,:,i) * minv(:,:,i);
        end
    end
end
