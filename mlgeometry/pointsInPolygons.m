function finds = pointsInPolygons(points,faces,verts)
    % compute bounding boxes for each face
    vertsx = verts(:,1);
    vertsy = verts(:,2);
    bmin = [min(vertsx(faces),[],2),min(vertsy(faces),[],2)]';
    bmax = [max(vertsx(faces),[],2),max(vertsy(faces),[],2)]';
    clear vertsx vertsy;
	
    % compute candidate points for each face and candidate faces (that have
    % at least one candidate)
    candidatepinds = cell(1,size(faces,1));
    for i=1:size(faces,1)
        candidatepinds{i} = find(points(1,:) >= bmin(1,i) & points(1,:) <= bmax(1,i) & ...
                                 points(2,:) >= bmin(2,i) & points(2,:) <= bmax(2,i));
    end
    candidatefinds = find(cellfun(@(x)not(isempty(x)),candidatepinds));

    % check candidate points in polygon for each candidate face
    % (iterate over faces because inpoly can only do a face at a time)
    pointinpoly = sparse(size(points,2),size(faces,1));
    for i=candidatefinds
%         edges = [faces(i,1:end);
%                  faces(i,[2:end,1])];
%         pointinpoly(candidatepinds{i},i) = inpoly(points(:,candidatepinds{i})',verts,edges');%,TOL)

        pointinpoly(candidatepinds{i},i) = pointInPolygon_mex(...
            points(1,candidatepinds{i}),points(2,candidatepinds{i}),...
            verts(1,faces(i,:)),verts(2,faces(i,:)));
    end
    
    % for each point, get index of the first face it is in
    finds = zeros(1,size(points,2));
    for i=1:size(points,2)
        ind = find(pointinpoly(i,:) > 0,1,'first');
        if not(isempty(ind))
            finds(i) = ind;
        end
    end
end
