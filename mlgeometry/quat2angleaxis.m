% from http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToAngle/
% (Martin John Baker)
function [angle,axis] = quat2angleaxis(quat)
    
    quat = quatnormalize(quat')';
    angle = 2 * acos(quat(1,:));
    sinhalfangle = sqrt(1-quat(1,:).^2);
    
    % default axis: [1;0;0]
    axis = [ones(1,size(quat,2));zeros(2,size(quat,2))];
    
    % use default axis for angles below eps (axis direction is not important)
    mask = find(sinhalfangle >= 0.000001);
    
    % axis for all angle above eps
    axis(:,mask) = bsxfun(@times,quat(2:4,mask),1./sinhalfangle(:,mask));
end
