#include "mex.h"
#include <limits>
#include <math.h>

// function [d,si,f,px,py] = pointPolylineDistance(x,y,x1,y1,x2,y2)

void pointPolylineDistance_mex(const double* px, const double* py,
                               const double* xv1, const double* yv1,
                               const double* xv2, const double* yv2,
                               const mwSize np, const mwSize nv,
                               double* d, double* si, double* f, double* cpx, double* cpy)
{
    mwIndex i,j;
    
    const double* xvs = xv1;
    const double* xve = xv2;
    const double* yvs = yv1;
    const double* yve = yv2;
    mwSize nsegs = nv;
    mwIndex minseg;
    double a,dsq,mindsq;
    
    // int for booleans
    int out1, out2, minout1, minout2;
    int alloutputs = f != NULL && cpx != NULL && cpy != NULL;
    
    double xs, xe, ys, ye, x, y;
    
    if (xv2 == NULL || yv2 == NULL) { // vertices are specified in the continous format
        nsegs = nv-1;
        xve = xv1+1;
        yve = yv1+1;
    }

    for(i=0;i<np;i++) {
        x = px[i];
        y = py[i];
        mindsq = std::numeric_limits<double>::max();
        minseg = 0;
        minout1 = 0;
        minout2 = 0;
        for(j=0;j<nsegs;j++) {
            xs = xvs[j];
            xe = xve[j];
            ys = yvs[j];
            ye = yve[j];
            
            if (xe==xs && ye==ys) { // degenerate segment (zero length)
                out1 = 1;
                out2 = 0;
            } else {
                out1 = ((xe-xs)*(x-xs)) + ((ye-ys)*(y-ys)) < 0; //(ps to p) projected to the segment (ps to pe)
                out2 = ((xe-xs)*(x-xe)) + ((ye-ys)*(y-ye)) > 0; //(pe to p) projected to the segment (ps to pe)  
            }
            
            if (out1 == 0 && out2 == 0) { 
                a = fabs(((xe-xs)*(ys-y)) - ((ye-ys)*(xs-x)));
                dsq =  (a*a)/(((xe-xs)*(xe-xs)) + ((ye-ys)*(ye-ys)));
            } else if(out1 > 0) {
                dsq = ((x-xs)*(x-xs)) + ((y-ys)*(y-ys));
            } else {
                dsq = ((x-xe)*(x-xe)) + ((y-ye)*(y-ye));
            }
            
            if (dsq < mindsq) {
                mindsq = dsq;
                minseg = j;
                minout1 = out1;
                minout2 = out2;
            }
        }
        
        d[i] = sqrt(mindsq);
        si[i] = (double) minseg;
        
        if (alloutputs > 0) {
            if (minout1 == 0 && minout2 == 0) {
                xs = xvs[minseg];
                xe = xve[minseg];
                ys = yvs[minseg];
                ye = yve[minseg];
                
                a = 
                f[i] = (((xe-xs)*(x-xs)) + ((ye-ys)*(y-ys))) /
                       (((xe-xs)*(xe-xs)) + ((ye-ys)*(ye-ys)));
                cpx[i] = (xs*(1.0-f[i])) + (xe*f[i]);
                cpy[i] = (ys*(1.0-f[i])) + (ye*f[i]);
            } else if (minout1 > 0) {
                f[i] = 0.0;
                cpx[i] = xvs[minseg];
                cpy[i] = yvs[minseg];
            } else {
                f[i] = 1.0;
                cpx[i] = xve[minseg];
                cpy[i] = yve[minseg];
            }
        }
    }
}

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    mwIndex i;
    
    const double* px,* py,* xv1,* yv1;
    double* xv2,* yv2;
    mwSize np,nv;
    double* d,* si,* f,* cpx,* cpy;
    
    //  check for proper number of arguments
    if(nrhs != 4 && nrhs != 6) 
        mexErrMsgTxt("Four or six inputs required.");
    
    // validate inputs
    np = mxGetN(prhs[0]);
    if (np < 0 || mxGetN(prhs[1]) != np || mxGetM(prhs[0]) != 1 || mxGetM(prhs[1]) != 1) {
        mexErrMsgTxt("Invalid dimension for points array.");
    }
    px = mxGetPr(prhs[0]);
    py = mxGetPr(prhs[1]);
    
    nv = mxGetN(prhs[2]);
    if (nv <= 0 || mxGetN(prhs[3]) != nv || mxGetM(prhs[2]) != 1 || mxGetM(prhs[3]) != 1) {
        mexErrMsgTxt("Invalid dimension for first vertices array.");
    }
    xv1 = mxGetPr(prhs[2]);
    yv1 = mxGetPr(prhs[3]);
    
    if(nrhs==6) {
        if (mxGetN(prhs[4]) != nv || mxGetN(prhs[5]) != nv ||
            mxGetM(prhs[4]) != 1 || mxGetM(prhs[5]) != 1) {
            mexErrMsgTxt("Invalid dimension for second vertices array.");
        }
        xv2 = mxGetPr(prhs[4]);
        yv2 = mxGetPr(prhs[5]);
    } else {
        if (nv <= 1) {
            mexErrMsgTxt("At least two vertices must be given.");
        }
        xv2 = NULL;
        yv2 = NULL;
    }
    
    //mexPrintf("isarea: %d, n: %d, nsp: %d\n",isarea,n,nsp);
    
    if(nlhs != 2 && nlhs != 5) 
        mexErrMsgTxt("Two or five outputs required.");    

    // create outputs
    plhs[0] = mxCreateDoubleMatrix(1,np, mxREAL);
    plhs[1] = mxCreateDoubleMatrix(1,np, mxREAL);
    if (nlhs == 5) {
        plhs[2] = mxCreateDoubleMatrix(1,np, mxREAL);
        plhs[3] = mxCreateDoubleMatrix(1,np, mxREAL);
        plhs[4] = mxCreateDoubleMatrix(1,np, mxREAL);
    }
    
    d = mxGetPr(plhs[0]);
    si = mxGetPr(plhs[1]);
    if (nlhs == 5) {
        f = mxGetPr(plhs[2]);
        cpx = mxGetPr(plhs[3]);
        cpy = mxGetPr(plhs[4]);
    } else {
        f = NULL;
        cpx = NULL;
        cpy = NULL;
    }
    
    pointPolylineDistance_mex(px, py,
                              xv1, yv1,
                              xv2, yv2,
                              np, nv,
                              d, si, f, cpx, cpy);
                    
    // convert from c indexing to matlab indexing (start at 1)
    for (i=0; i<np; i++) {
        si[i] = si[i] + 1.0;
    }
}
