% WARNING! 'I' must not be shared with any other variable!!
function I = polygonRasterization(I,x,y,v,piptest,op)
    
    if nargin < 5 || isempty(piptest)
        piptest = 'crossing';
    end
    
    if nargin < 6 || isempty(op)
        op = [];
    end

    % triangulate the polygon => this also determines how the values are
    % interpolated across the polygon
    warning('off','MATLAB:delaunayTriangulation:ConsConsSplitWarnId'); % so self-intersecting polygons don't create a warning
    warning('off','MATLAB:delaunayTriangulation:DupPtsConsUpdatedWarnId'); % so polygons with duplicate points don't create a warning
    tri = delaunayTriangulation(x',y',[1:numel(x);[2:numel(x),1]]');
    warning('on','MATLAB:delaunayTriangulation:ConsConsSplitWarnId');
    warning('on','MATLAB:delaunayTriangulation:DupPtsConsUpdatedWarnId');
    
    % find triangles inside the polygon (depends on mode)
    if strcmp(piptest,'crossing');
        inds = find(tri.isInterior);
    elseif strcmp(piptest,'winding')
%         tricentroidsx = sum(reshape(tri.Points(tri.ConnectivityList,1),[],3),2)./3;
%         tricentroidsy = sum(reshape(tri.Points(tri.ConnectivityList,2),[],3),2)./3;
        
        incenters = tri.incenter((1:size(tri.ConnectivityList,1))');
        % inpolygon determines in/out status using the winding number
        inds = find(inpolygon(incenters(:,1),incenters(:,2),x,y));
    else
        error('Unknown point in polygon test.');
    end
    
    % rasterize triangles inside the polygon
    if size(v,2)==1
        for i=inds'
            I = triangleRasterization(I,...
                tri.Points(tri.ConnectivityList(i,:),1)',...
                tri.Points(tri.ConnectivityList(i,:),2)',...
                [v,v,v],op);
        end
%         I(1) = I(1); % safety to unshare I (this seems to work in R2014b)
%         triangleRasterization2D_mex(I,...
%             tri.Points(tri.ConnectivityList(inds,1),:)',...
%             tri.Points(tri.ConnectivityList(inds,2),:)',...
%             tri.Points(tri.ConnectivityList(inds,3),:)',...
%             v);

        % constant value, no need to intepolate, just check if each pixel
        % center is inside the polygon
    else
        if size(v,2) ~= size(tri.Points,1)
            % The polygon self-intersects and additional points were added.
            % It is not clear how to fill the overlapping parts of the polygon.
            error('Only constant values supported for self-intersecting polygons.');
        end
        for i=inds'
            I = triangleRasterization(I,...
                tri.Points(tri.ConnectivityList(i,:),1)',...
                tri.Points(tri.ConnectivityList(i,:),2)',...
                v(:,tri.ConnectivityList(i,:)),op);
        end
%         I(1) = I(1); % safety to unshare I (this seems to work in R2014b)        
%         triangleRasterization2D_mex(I,...
%             tri.Points(tri.ConnectivityList(inds,1),:)',...
%             tri.Points(tri.ConnectivityList(inds,2),:)',...
%             tri.Points(tri.ConnectivityList(inds,3),:)',...
%             v(:,tri.ConnectivityList(inds,:)));
    end
end
