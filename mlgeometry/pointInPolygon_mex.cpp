// in = pointInPolygon_mex(px,py,v)
// in = pointInPolygon_mex(px,py,vx,vy)
// in = pointInPolygon_mex(...,piptype)
// px,py are the x and y coordinates of the points, each 1 x n (where n is number of points)
// v are the polygon vertices as 2 x m array (where m is number of polygon vertices), or cell array thereof for multiple polygons
// vx,vy are the x and y coordinates of the polygon vertices, each 1 x m (where m is number of polygon vertices), or cell array thereof for multiple polygons
// in is a 1 x n logical matrix indicating if a point is inside the polygon if polygons were not given as cell array
//    or a n x l logical matrix (where l is the number of polygons) if polygons were given as cell array
// piptype (optional) is the type of point in polygon test, either 'crossing' (default) or 'winding'

#include "mex.h"

#include<vector>
// #include <algorithm>

#include "mexutils.h"
#include "geom.h"

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{   
    // check number of inputs
    if (nrhs < 3 || nrhs > 5) {
        mexErrMsgTxt("Three to five inputs required.");
    }
    
    mwIndex inputind = 0;

    // get point x components
    std::vector<double> px;
    readDoubleNDVectorset(prhs[inputind],1,px);
    ++inputind;
    
    // get point y components
    std::vector<double> py;
    readDoubleNDVectorset(prhs[inputind],1,py);
    ++inputind;
    
    // read polygon(s)
    bool separatecomps = false;
    bool cellpolys = false;
    std::vector<std::vector<double> > v;
    std::vector<std::vector<double> > vx;
    std::vector<std::vector<double> > vy;
    if (inputind+1 >= nrhs || mxIsClass(prhs[inputind+1],"char")) {
        // polygon x and y components in 2xn matrix
        if (mxIsClass(prhs[inputind],"cell")) {
            readDoubleNDVectorsetCellArray(prhs[inputind],2,v);
            cellpolys = true;
        } else {
            v.resize(1);
            readDoubleNDVectorset(prhs[inputind],2,v[0]);
            cellpolys = false;
        }
        ++inputind;
        
        separatecomps = false;
    
    } else {
        // polygons x and y components in two separate inputs vx and vy
        if (mxIsClass(prhs[inputind],"cell")) {
            readDoubleNDVectorsetCellArray(prhs[inputind],2,vx);
            cellpolys = true;
        } else {
            vx.resize(1);
            readDoubleNDVectorset(prhs[inputind],1,vx[0]);
            cellpolys = false;
        }
        ++inputind;
        
        if (mxIsClass(prhs[inputind],"cell")) {
            readDoubleNDVectorsetCellArray(prhs[inputind],2,vy);
        } else {
            vy.resize(1);
            readDoubleNDVectorset(prhs[inputind],1,vy[0]);
        }
        ++inputind;
        
        // check validity
        if (vx.size() != vy.size()) {
            mexErrMsgTxt("Invalid polygons.");
        }
        std::vector<std::vector<double> >::const_iterator vxi,vyi;
        for (vxi=vx.begin(), vyi=vy.begin(); vxi!=vx.end() && vyi!=vy.end(); ++vxi, ++vyi) {
            if (vxi->size() != vyi->size()) {
                mexErrMsgTxt("Invalid polygons.");
            }
        }
        
        separatecomps = true;
    }
    
    // read optional point in polygon test parameter
    Pointinpolytest piptest = PIP_CROSSING;
    if (inputind < nrhs) {
        std::string piptest_str;
        readString(prhs[inputind],piptest_str);
        if (piptest_str.compare("crossing") == 0) {
            piptest = PIP_CROSSING;
        } else if (piptest_str.compare("winding") == 0) {
            piptest = PIP_WINDING;
        } else {
            mexErrMsgTxt("Unknown point in polygon test type.");
        }
        ++inputind;
    }
    
    if(nlhs != 1) {
        mexErrMsgTxt("Exactly one output required.");
    }
    
    // initialize output
    std::vector<bool> in;
    if (separatecomps) {
        if (!cellpolys) {
            // for legacy reasons, return in as row if polygons were not given as cell array
            plhs[0] = mxCreateLogicalMatrix(vx.size(),px.size());
        } else {
            plhs[0] = mxCreateLogicalMatrix(px.size(),vx.size());
        }
        in.resize(px.size()*vx.size());
    } else {
        if (!cellpolys) {
            // for legacy reasons, return in as row if polygons were not given as cell array
            plhs[0] = mxCreateLogicalMatrix(v.size(),px.size());
        } else {
            plhs[0] = mxCreateLogicalMatrix(px.size(),v.size());
        }
        in.resize(px.size()*v.size());
    }
    
    // return if points or polygons are empty
    if (in.empty()) {
        return;
    }
    
    // run test
    if (separatecomps) {
        poinsetInPolygonset2D(
                px.begin(),px.end(),py.begin(),py.end(),
                vx.begin(),vx.end(),vy.begin(),vy.end(),
                in.begin(), in.end(),
                piptest);
    } else {
        poinsetInPolygonset2D(
                px.begin(),px.end(),py.begin(),py.end(),
                v.begin(),v.end(),
                in.begin(), in.end(),
                piptest);
    }
    
    // copy to output and convert from bool to mxLogical
    mxLogical* plhs_p = mxGetLogicals(plhs[0]);
    std::vector<bool>::const_iterator ini = in.begin();
    for (; ini!=in.end(); ++ini, ++plhs_p) {
        *plhs_p = (mxLogical) *ini;
    }
}
