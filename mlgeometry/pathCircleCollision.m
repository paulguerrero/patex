function t = pathCircleCollision(ps,pe,center,radius)

    if size(ps,2) ~= 1
        error('only one line segment at a time supported');
    end	

    t = nan(1,size(center,2));
    
    if isempty(center)
        return;
    end

    segvec = pe-ps;
    seglen = sqrt(sum(segvec.^2,1));        

    [id,~,it,~,~] = pointPolylineDistance_mex(...
        center(1,:),center(2,:),...
        ps(1),ps(2),pe(1),pe(2));
    
    imask = id <= radius;
    
    % if the trajectory endpoint is inside the closest point add back the distance from the trajectory endpoint to the closest point
    it = it(imask);
    id = id(imask);
    icenters = center(:,imask);
    iradii = radius(imask);
    mask = it == 1;
    if any(mask)
        segnormal = [-segvec(2),segvec(1)]./seglen;
        endpointvec = [pe(1) - icenters(1,mask);...
                       pe(2) - icenters(2,mask)];
        orthdist = abs(endpointvec(1,:).*segnormal(1) + endpointvec(2,:).*segnormal(2));
        endpointdist = sqrt(sum(endpointvec.^2,1));
        it(mask) = it(mask) - (sqrt(iradii(mask).^2-orthdist.^2) - sqrt(endpointdist.^2-orthdist.^2)) ./ seglen;
    end
    
    mask = not(mask);
    if any(mask)
        it(mask) = it(mask) - sqrt(iradii(mask).^2-id(mask).^2) ./ seglen;
    end
    
    t(imask) = it;
end
