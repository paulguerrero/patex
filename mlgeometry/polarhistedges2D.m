function [angularedges,radialedges] = polarhistedges2D(abinpos,rbinpos,radialstart)
    
    if nargin < 3 || isempty(radialstart)
        radialstart = 0;
    end
    
    binadist = smod(diff(abinpos),-pi,pi);
    if any(binadist - binadist(1) > 0.000001)
        error('Angular bin positions must be distributed uniformly on the circle');
    end
    % start of the edges
    angularedges = abinpos-(smod(abinpos(2)-abinpos(1),-pi,pi))*0.5;
    
    % making use of the fact that the first bin starts at radialstart
    % next edge is at same distance from center as previous edge
    radialedges = nan(1,numel(rbinpos+1));
    radialedges(1) = radialstart;
    for i=1:numel(rbinpos)
        radialedges(i+1) = rbinpos(i)+rbinpos(i)-radialedges(i);
    end
end