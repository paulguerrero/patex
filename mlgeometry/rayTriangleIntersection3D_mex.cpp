/* Ray-Triangle Intersection Test Routines          */
/* Different optimizations of my and Ben Trumbore's */
/* code from journals of graphics tools (JGT)       */
/* http://www.acm.org/jgt/                          */
/* by Tomas Moller, May 2000                        */

#include "mex.h"

#include <math.h>
#include <algorithm>

#define EPSILON 0.000001
#define CROSS(dest,v1,v2) \
          dest[0]=v1[1]*v2[2]-v1[2]*v2[1]; \
          dest[1]=v1[2]*v2[0]-v1[0]*v2[2]; \
          dest[2]=v1[0]*v2[1]-v1[1]*v2[0];
#define DOT(v1,v2) (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])
#define SUB(dest,v1,v2) \
          dest[0]=v1[0]-v2[0]; \
          dest[1]=v1[1]-v2[1]; \
          dest[2]=v1[2]-v2[2]; 

/* code rewritten to do tests on the sign of the determinant */
/* the division is before the test of the sign of the det    */
/* and one CROSS has been moved out from the if-else if-else */
int intersect_triangle3(double orig[3], double dir[3],
			double vert0[3], double vert1[3], double vert2[3],
			double *t, double *u, double *v)
{
   double edge1[3], edge2[3], tvec[3], pvec[3], qvec[3];
   double det,inv_det;

   /* find vectors for two edges sharing vert0 */
   SUB(edge1, vert1, vert0);
   SUB(edge2, vert2, vert0);

   /* begin calculating determinant - also used to calculate U parameter */
   CROSS(pvec, dir, edge2);

   /* if determinant is near zero, ray lies in plane of triangle */
   det = DOT(edge1, pvec);

   /* calculate distance from vert0 to ray origin */
   SUB(tvec, orig, vert0);
   inv_det = 1.0 / det;
   
   CROSS(qvec, tvec, edge1);
      
   if (det > EPSILON)
   {
      *u = DOT(tvec, pvec);
      if (*u < 0.0 || *u > det)
	 return 0;
            
      /* calculate V parameter and test bounds */
      *v = DOT(dir, qvec);
      if (*v < 0.0 || *u + *v > det)
	 return 0;
      
   }
   else if(det < -EPSILON)
   {
      /* calculate U parameter and test bounds */
      *u = DOT(tvec, pvec);
      if (*u > 0.0 || *u < det)
	 return 0;
      
      /* calculate V parameter and test bounds */
      *v = DOT(dir, qvec) ;
      if (*v > 0.0 || *u + *v < det)
	 return 0;
   }
   else return 0;  /* ray is parallell to the plane of the triangle */

   *t = DOT(edge2, qvec) * inv_det;
   (*u) *= inv_det;
   (*v) *= inv_det;

   return 1;
}

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    // inputs
    
    //double* vert1 = NULL;
    //double* vert2 = NULL;
    //double* vert3 = NULL;
    
    //double* intersect;
    //double* t;
    //double* u;
    //double* v;
    
    //mwSize ntris;
    
    //mwIndex i;
    
    // validate and format inputs
    if (nrhs != 5) {
        mexErrMsgTxt("Five inputs required.");
    }
    
    //mexPrintf("0\n");
    
    // first input: origin
    if (mxGetNumberOfDimensions(prhs[0]) != 2) {
        mexErrMsgTxt("Invalid number of dimensions for origin.");
    }
    if (mxGetM(prhs[0]) != 3 || mxGetN(prhs[0]) != 1) {
        mexErrMsgTxt("Invalid size for origin.");
    }
    //memcpy(&orig[0],mxGetPr(prhs[0]),3*sizeof(double));
    double orig[3];
    std::copy(mxGetPr(prhs[0]),mxGetPr(prhs[0])+3,orig);
    
    //mexPrintf("1\n");
    
    // second input: direction
    if (mxGetNumberOfDimensions(prhs[1]) != 2) {
        mexErrMsgTxt("Invalid number of dimensions for direction.");
    }
    if (mxGetM(prhs[1]) != 3 || mxGetN(prhs[1]) != 1) {
        mexErrMsgTxt("Invalid size for direction.");
    }
    //memcpy(&dir[0],mxGetPr(prhs[1]),3*sizeof(double));
    double dir[3];
    std::copy(mxGetPr(prhs[1]),mxGetPr(prhs[1])+3,dir);
    
    //mexPrintf("2\n");
    
    // third input: first triangle vertices
    if (mxGetNumberOfDimensions(prhs[2]) != 2) {
        mexErrMsgTxt("Invalid number of dimensions for triangle vertex coordinates.");
    }
    if (mxGetM(prhs[2]) != 3 || mxGetN(prhs[2]) < 1) {
        mexErrMsgTxt("Vertices are empty or have incorrect size.");
    }
    mwSize ntris = mxGetN(prhs[2]);
    //vert1 = mxCalloc(ntris*3,sizeof(double));
    //memcpy(&vert1[0],mxGetPr(prhs[2]),ntris*3*sizeof(double));
    double* vert1 = new double[ntris*3];
    std::copy(mxGetPr(prhs[2]),mxGetPr(prhs[2])+ntris*3,vert1);
    
    //mexPrintf("3 - ntris: %d\n",ntris);
    
    // fourth input: second triangle vertices
    if (mxGetNumberOfDimensions(prhs[3]) != 2) {
        mexErrMsgTxt("Invalid number of dimensions for triangle vertex coordinates.");
    }
    if (mxGetM(prhs[3]) != 3 || mxGetN(prhs[3]) != ntris) {
        mexErrMsgTxt("Vertices have incorrect size.");
    }
    //vert2 = mxCalloc(ntris*3,sizeof(double));
    //memcpy(&vert2[0],mxGetPr(prhs[3]),ntris*3*sizeof(double));
    double* vert2 = new double[ntris*3];
    std::copy(mxGetPr(prhs[3]),mxGetPr(prhs[3])+ntris*3,vert2);
    
    //mexPrintf("4\n");
    
    // fifth input: third triangle vertices
    if (mxGetNumberOfDimensions(prhs[4]) != 2) {
        mexErrMsgTxt("Invalid number of dimensions for triangle vertex coordinates.");
    }
    if (mxGetM(prhs[4]) != 3 || mxGetN(prhs[4]) != ntris) {
        mexErrMsgTxt("Vertices have incorrect size.");
    }
    //vert3 = mxCalloc(ntris*3,sizeof(double));
    //memcpy(&vert3[0],mxGetPr(prhs[4]),ntris*3*sizeof(double));
    double* vert3 = new double[ntris*3];
    std::copy(mxGetPr(prhs[4]),mxGetPr(prhs[4])+ntris*3,vert3);

    
    //mexPrintf("5\n");
    
    // check number of outputs
    if (nlhs != 4) {
        mexErrMsgTxt("Four outputs required.");
    }
    
    /*mexPrintf("ntris: %d\n",ntris);*/
    /*mexPrintf("orig: (%f,%f,%f)\n",orig[0],orig[1],orig[2]);*/
    /*mexPrintf("dir: (%f,%f,%f)\n",dir[0],dir[1],dir[2]);*/
    /*mexPrintf("first vert1: (%f,%f,%f)\n",vert1[0],vert1[1],vert1[2]);*/
    /*mexPrintf("first vert2: (%f,%f,%f)\n",vert2[0],vert2[1],vert2[2]);*/
    /*mexPrintf("first vert3: (%f,%f,%f)\n",vert3[0],vert3[1],vert3[2]);*/
    
    // allocate outputs
    double* intersect = new double[ntris];
    double* t = new double[ntris];
    double* u = new double[ntris];
    double* v = new double[ntris];
    
    // call function for all triangles
    for (mwIndex i=0; i<ntris; i++) {
        intersect[i] = (double) intersect_triangle3(orig,dir,&vert1[i*3],&vert2[i*3],&vert3[i*3],&t[i],&u[i],&v[i]);
    }
    
    delete[] vert1;
    delete[] vert2;
    delete[] vert3;
    
    //mxFree(vert1);
    //mxFree(vert2);
    //mxFree(vert3);

    // first output: intersect (1 if there is an intersection, 0 otherwise)
    plhs[0] = mxCreateDoubleMatrix(1,ntris, mxREAL);
    //memcpy(mxGetPr(plhs[0]),intersect,ntris*sizeof(double));
    std::copy(intersect,intersect+ntris,mxGetPr(plhs[0]));
    
    // second output: t (distance from origin)
    plhs[1] = mxCreateDoubleMatrix(1,ntris, mxREAL);
    //memcpy(mxGetPr(plhs[1]),t,ntris*sizeof(double));
    std::copy(t,t+ntris,mxGetPr(plhs[1]));
    
    // third output: u (first coordinate on triangle)
    plhs[2] = mxCreateDoubleMatrix(1,ntris, mxREAL);
    //memcpy(mxGetPr(plhs[2]),u,ntris*sizeof(double));
    std::copy(u,u+ntris,mxGetPr(plhs[2]));
    
    // fourth output: v (second coordinate on triangle)
    plhs[3] = mxCreateDoubleMatrix(1,ntris, mxREAL);
    //memcpy(mxGetPr(plhs[3]),v,ntris*sizeof(double));
    std::copy(v,v+ntris,mxGetPr(plhs[3]));

    delete[] intersect;
    delete[] t;
    delete[] u;
    delete[] v;
    
    //mxFree(intersect);
    //mxFree(t);
    //mxFree(u);
    //mxFree(v);
}
