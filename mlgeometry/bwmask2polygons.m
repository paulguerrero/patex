% polygons = bwmask2polygons(bwmask)
% polygons = bwmask2polygons(bwmask,options ...)
function polygons = bwmask2polygons(bwmask,varargin)
    
    options = struct(...
        'connectivity',8,...        
        'removenoise',false,...
        'noisemaxarea',5e-5,... % maximum area that is considered to be noise (given as fraction of the total number of image pixels)
        'cutholes',false,...
        'normalize',false,...
        'flipy',true);
    
    options = nvpairs2struct(varargin,options);

    if options.removenoise    
        % remove small regions
        
        filtersize = round(sqrt(options.noisemaxarea * numel(bwmask)) * 2);
        if filtersize >= 1
            bwmask = medfilt2(bwmask,[filtersize,filtersize],'symmetric'); 
        end
        areasize = round(options.noisemaxarea * numel(bwmask));
        if areasize >= 1
            % remove small islands
            bwmask = bwareaopen(bwmask,areasize);

            % remove small holes
            holemask = imfill(bwmask, 'holes') & not(bwmask);
            holemask = holemask & not(bwareaopen(holemask,areasize)); 
            bwmask = bwmask | holemask;
        end
    end

    % swtich x,y and flip y to get to the standard coordinate system
    [polygons,~,~,adjacency] = bwboundaries(bwmask,options.connectivity);
    for i=1:numel(polygons)
        polygons{i} = polygons{i}';
        polygons{i} = polygons{i}([2,1],:);
        if options.flipy
            polygons{i}(2,:) = size(bwmask,1) - (polygons{i}(2,:)-1);
        end
    end

    polymask = not(any(adjacency,2)); % root polygons
    newpolys = cell(1,0);
    while any(polymask)

        polyA = adjacency(:,polymask);

        % cut open holes in all polygons
        p = polygons(polymask)';
        if options.cutholes
            for i=1:numel(p)
                holemask = polyA(:,i);
                if any(holemask)
                    p{i} = [p(i),polygons(holemask)'];
                    p{i} = [p{i};repmat({nan(2,1)},1,size(p{i},2))];
                    p{i} = [p{i}{:}];
                    p{i}(:,end) = []; % remove trainling nans
                end
                [x,y] = polycut2(p{i}(1,:),p{i}(2,:));
                p{i} = [x,y]';
            end
        else
            for i=1:numel(p)
                holemask = polyA(:,i);
                if any(holemask)
                    p{i} = [p(i),polygons(holemask)'];
                end
            end
        end
        newpolys = [newpolys,p]; %#ok<AGROW>

        polymask = any(adjacency(:,any(adjacency(:,polymask),2)),2); % repeat for polygons inside the holes
    end

    polygons = newpolys;


    if options.normalize
        % convert to [0,1] in both coordinates
        for i=1:numel(polygons)
            polygons{i} = bsxfun(@rdivide,polygons{i}-1,[size(bwmask,2);size(bwmask,1)]-1);
        end
    end

end
