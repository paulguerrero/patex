% edges should be a sparse matrix where entry (i,j) denotes the number of
% half-edges from vertex i to vertex j
function [isconsistent,loopedges,duplicatehalfedges,borderedges] = trimeshHalfedgeConsistency(edges)
    [loopedges(:,1),loopedges(:,2)] = find(diag(edges));
    loopedges = loopedges';
    
    [duplicatehalfedges(:,1),duplicatehalfedges(:,2)] = find(edges>1);
    duplicatehalfedges = duplicatehalfedges';
    
    [borderedges(:,1),borderedges(:,2)] = find(edges > 0 & (edges + edges') == 1);
    borderedges = borderedges';
    
    isconsistent = isempty(loopedges) && isempty(duplicatehalfedges) && isempty(borderedges);
end
