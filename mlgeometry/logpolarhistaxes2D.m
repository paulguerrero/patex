function [angularpos,radialpos] = logpolarhistaxes2D(nangular,nradial,minrad,maxrad,lin)
    
    if nangular < 2 || nradial < 1
        error('Too few bins, need at least 2 angular slices and 1 radial ring');
    end
    
    if nargin < 3 || isempty(minrad)
        minrad = 0;
    end
    
    if nargin < 3 || isempty(maxrad)
        maxrad = 1;
    end
    
    if nargin < 5 || isempty(lin)
        lin = 0;
    end
    
    % angular positions
    angularpos = linspace(0,2*pi,nangular+1);
    angularpos = angularpos(1:end-1);
    
    % radial positions
    radialedges = linlogspace(0,10,nradial+1,lin,true).*((maxrad-minrad)/10) + minrad;
    radialpos = (radialedges(1:end-1)+radialedges(2:end)).*0.5;
end
