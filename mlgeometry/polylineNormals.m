% point to the right if polyline segment goes from top to bottom
% if polyline is cw polygon, this means normals point outwards
function normals = polylineNormals(poly,closed,vertex)
    
    if nargin < 3
        vertex = true;
    end
    
    if nargin < 2
        closed = false;
    end

    % compute segment normals
    if closed
        normals = [-(poly(2,[2:end,1])-poly(2,:)); ...
                     poly(1,[2:end,1])-poly(1,:)];
    else
        normals = [-(poly(2,2:end)-poly(2,1:end-1)); ...
                     poly(1,2:end)-poly(1,1:end-1)];
    end
    
    len = sqrt(sum(normals.^2,1));
    normals = normals ./ len([1,1],:); % normals of degenerate segments will be nan
    
    if vertex
        % compute vertex normals
        if closed
            normals = normals(:,[end,1:end-1]) + normals;
        else
            normals = [...
                normals(:,1),...
                normals(:,1:end-1) + normals(:,2:end),...
                normals(:,end)];
        end

        % re-normalize
        len = sqrt(sum(normals.^2,1));
        mask = len < eps(mean(len))*100;
%         normals(:,not(mask)) = normals(:,not(mask)) ./ len([1,1],not(mask));
        if any(mask)
            % for 180 degree corners, use the direction from the adjacent
            % vertices to the current vertex as normal
            inds = find(mask);
            indsbefore = mod(inds-2,size(poly,2))+1; % (inds-1)-1
            indsafter = mod(inds,size(poly,2))+1; % (inds+1)-1
            normals(:,mask) = (poly(:,inds)-poly(:,indsbefore)) + ...
                              (poly(:,inds)-poly(:,indsafter));
            len(mask) = sqrt(sum(normals(:,mask).^2,1));
            
            % vertices that still have zero normal lengths are degenerate
            % vertices, both adjacent edges must have zero length, the
            % normals of these vertices will be NaN
        end
        normals = normals ./ len([1,1],:);
    end
end
