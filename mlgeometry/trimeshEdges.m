function e = trimeshEdges(faces,nverts)
%     e = [...
%         faces(:,[1,2]);faces(:,[2,3]);faces(:,[3,1]);...
%         faces(:,[1,3]);faces(:,[3,2]);faces(:,[2,1])];
    e = [faces([1,2],:),faces([2,3],:),faces([3,1],:)];
    e = sparse(e(1,:)',e(2,:)',ones(1,size(e,2))',nverts,nverts);
end
