% polyonLineIntersection(poly,p,angle)
% polyonLineIntersection(poly,p,dir)
function [ix,iy,si,lf,sf] = polylineLineIntersection(polyline,p,dir)

    ix = cell(1,size(p,2));
    iy = cell(1,size(p,2));
    if nargout>2
        si = cell(1,size(p,2));
    end
    if nargout>3
        lf = cell(1,size(p,2));
    end
    if nargout>4
        sf = cell(1,size(p,2));
    end

    [pbbmin,pbbmax,pbbdiag] = pointsetBoundingbox(polyline);

    % add numerical safety margin to bounding box
    pbbmin = pbbmin - [pbbdiag;pbbdiag]*0.1;
    pbbmax = pbbmax + [pbbdiag;pbbdiag]*0.1;

    if size(dir,1) == 1
        angle = dir;
        [x,y] = pol2cart(angle,1); 
        dir = [x;y];
    end

    if any(all(dir == 0,1))
        error('zero direction found');
    end
    
    pbbmin = pbbmin(:,ones(1,size(p,2)));
    pbbmax = pbbmax(:,ones(1,size(p,2)));
    pbbsize = pbbmax-pbbmin;
    
    [~,maxdircoord] = max(abs(dir),[],1);
    inds = sub2ind(size(p),maxdircoord,1:size(p,2));
    ftomin = (pbbmin(inds)-p(inds)) / dir(inds);
    fdiag = (pbbmax(inds)-pbbmin(inds)) / dir(inds);
    
%     %     ftomin = min((,[],1);
%     ftomin = (pbbmin(:,ones(1,size(p,2)))-p) ./ dir;
%     [~,minind] = min(abs(ftomin),[],1);
%     ftomin = ftomin(sub2ind(size(ftomin),minind,1:size(ftomin,2)));
%     pbbdiagvec = pbbmax-pbbmin;
% %     fdiag = min(pbbdiagvec(:,ones(1,size(p,2))) ./ dir,[],1);
%     fdiag = pbbdiagvec(:,ones(1,size(p,2))) ./ dir;
%     [~,minind] = min(abs(fdiag),[],1);
%     fdiag = fdiag(sub2ind(size(fdiag),minind,1:size(fdiag,2)));

    linestart = p+dir.*ftomin([1,1],:);

    [intersects,ip] = linesegLinesegIntersection2D_mex(...
        polyline(:,1:end-1),polyline(:,2:end),...
        linestart,linestart+dir.*fdiag([1,1],:));
    ipx = ip(:,:,1);
    ipy = ip(:,:,2);
    clear ip;
    
    for i=1:size(p,2)
        isegs = find(intersects(:,i)>0);
        ix{i} = ipx(isegs,i);
        iy{i} = ipy(isegs,i);
        if nargout>2
            si{i} = isegs;
        end
        if nargout>3
            error('todo: compute normalized distance.');
            lf{i} = (intersects.intNormalizedDistance2To1(isegs,i)/fdiag) - ftomin;
        end
        if nargout>4
            error('todo: compute normalized distance.');
            sf{i} = intersects.intNormalizedDistance1To2(isegs,i);
        end
    end
end
