function [r,c,type] = fitcircle(p1,p2,p3,errbound)
    
    c = nan(2,size(p1,2));
    r = zeros(1,size(p1,2));

    % compute normals pointing outwards (to the convex side)
    n21 = [-(p2(2,:)-p1(2,:));p2(1,:)-p1(1,:)];
    n21 = bsxfun(@times, n21, 1./sqrt(sum(n21.^2,1)));
    n23 = [-(p3(2,:)-p2(2,:));p3(1,:)-p2(1,:)];
    n23 = bsxfun(@times, n23, 1./sqrt(sum(n23.^2,1)));
%     errbound = 0.1;

    p23 = p3-p2;
    p21 = p1-p2;    
    p21len = sqrt(sum((p21).^2,1));
    p23len = sqrt(sum((p23).^2,1));
    angledivider = bsxfun(@times,p1-p2,1./p21len) + ...
                   bsxfun(@times,p3-p2,1./p23len);
    angledivider = bsxfun(@times,angledivider,1./sqrt(sum(angledivider.^2,1)));
    mask = dot(n21,angledivider,1) > 0;
    n21(:,mask) = -n21(:,mask);
    mask = dot(n23,angledivider,1) > 0;
    n23(:,mask) = -n23(:,mask);
    unsolvedmask = not(all(angledivider == 0,1));


    % do non-linear optimization to find the circle center that maximizes
    % the circle radius with six nonlinear inequality constraints:
    % - constrain circle to be inside error bound of first segments
    % - constrain circle to be inside error bound of second segments
    % - constrain circle to not contain p1
    % - constrain circle to not contain p2
    % - constrain circle to be on concave side of segment1
    % - constrain circle to be on concave side of segment2
    
%     fmincon doesn't seem to find the best minima
%     for i=1:size(p1,2)
%         c(:,i) = fmincon(...
%             @(x)-sqrt(sum((p2-x).^2,1)),... % objective function to minimize
%             p2,...                          % starting position
%             [],[],[],[],[],[],...
%             @(x) deal(...
%                 [sqrt(sum((p2-x).^2)) - dot(p2-x,n12) - errbound;... 
%                  sqrt(sum((p2-x).^2)) - dot(p2-x,n23) - errbound;...
%                  sqrt(sum((p2-x).^2)) - sqrt(sum((p1-x).^2));...
%                  sqrt(sum((p2-x).^2)) - sqrt(sum((p3-x).^2));...
%                  -dot(p2-x,n12);...
%                  -dot(p2-x,n23)],... % inequality constraints
%                  []));
% 	
%         % compute circle radius
%         r(i) = sqrt(sum((p2-c).^2,1));
%     end

    % check for colinear points (inf. radius)
    triarea = 0.5.*abs((p1(1)-p3(1)).*(p2(2)-p1(2)) - (p1(1)-p2(1)).*(p3(2)-p1(2)));
    mask = triarea == 0;
    r(mask) = inf;
    c(:,mask) = nan;
    unsolvedmask(mask) = false;
    
    type = zeros(1,numel(unsolvedmask));

    % circle is defined by three points:
    % p2, and the limit of two of the first four constraints

    % (the two concave side constraints are always satisfied?)
    
    % first try going to the limit of the neighbouring points constraint
    % and see if the other two constraints are satisfied
    % (compute circumcenter of all consecutive triples of points)
    A = p1(:,unsolvedmask);
    B = p2(:,unsolvedmask);
    C = p3(:,unsolvedmask);
    D = 2.*(A(1,:).*(B(2,:)-C(2,:)) + ...
            B(1,:).*(C(2,:)-A(2,:)) + ...
            C(1,:).*(A(2,:)-B(2,:)));
    cx = (sum(A.^2,1).*(B(2,:)-C(2,:)) + ...
          sum(B.^2,1).*(C(2,:)-A(2,:)) + ...
          sum(C.^2,1).*(A(2,:)-B(2,:)))./D;
    cy = (sum(A.^2,1).*(C(1,:)-B(1,:)) + ...
          sum(B.^2,1).*(A(1,:)-C(1,:)) + ...
          sum(C.^2,1).*(B(1,:)-A(1,:)))./D;
    r(unsolvedmask) = sqrt(sum((p2(:,unsolvedmask) - [cx;cy]).^2,1));
    c(:,unsolvedmask) = [cx;cy];
    
    wasunsolved = unsolvedmask;
    
    % find circles with unsatisfied constraints
    unsolvedmask = unsolvedmask & ...
        (sqrt(sum((p2-c).^2)) - dot(p2-c,n21) > errbound*1.0001 | ...
         sqrt(sum((p2-c).^2)) - dot(p2-c,n23) > errbound*1.0001 | ...
         sqrt(sum((p2-c).^2)) > sqrt(sum((p1-c).^2)).*1.0001 | ...
         sqrt(sum((p2-c).^2)) > sqrt(sum((p3-c).^2)).*1.0001);
    
%     subd1 = false(1,numel(unsolvedmask));
%     subd2 = false(1,numel(unsolvedmask));
    type(wasunsolved & not(unsolvedmask)) = 1;
    
    if any(unsolvedmask)
        % if the constraints are unsatisfied, try going to the limit of the
        % error bound constraints first and see if the neighbor point
        % constraints are satisfied
        angles = (pi-abs(polylineAngles(p2,true)));
        r(unsolvedmask) = errbound ./ (1-sin(angles(unsolvedmask)./2));
        c(:,unsolvedmask) = bsxfun(@times,r(unsolvedmask),angledivider(:,unsolvedmask)) + p2(:,unsolvedmask);

        wasunsolved = unsolvedmask;

        % find circles with unsatisfied constraints
        unsolvedmask = unsolvedmask & ...
            (sqrt(sum((p2-c).^2)) - dot(p2-c,n21) > errbound*1.0001 | ...
             sqrt(sum((p2-c).^2)) - dot(p2-c,n23) > errbound*1.0001 | ...
             sqrt(sum((p2-c).^2)) > sqrt(sum((p1-c).^2)).*1.0001 | ...
             sqrt(sum((p2-c).^2)) > sqrt(sum((p3-c).^2)).*1.0001);

%         subd1(wasunsolved & not(unsolvedmask)) = true;
%         subd2(wasunsolved & not(unsolvedmask)) = true;
        type(wasunsolved & not(unsolvedmask)) = 2;
    end

    
    % remaining cases: mixture of neighboring point and error boundary
    % constraints
    
    if any(unsolvedmask)
        % 1st point constraint & 2nd segment boundary error constraint
        % solve r = a /(cos(angles-asin(1-(errbound/r)))):
        a = p21len(unsolvedmask)/2;
        ra = (2 .*(-sqrt(...
            2.*a.*errbound.*sin(angles(unsolvedmask)).*cos(angles(unsolvedmask)).^2 + ...
            errbound.^2.*cos(angles(unsolvedmask)).^2) - ...
            a.*sin(angles(unsolvedmask)) - ...
            errbound)) ./ ...
            (cos(2 .* angles(unsolvedmask))-1);
    %     rb = (2 .*(sqrt(...
    %         2.*a.*errbound.*sin(angles(unsolvedmask)).*cos(angles(unsolvedmask)).^2 + ...
    %         errbound.^2.*cos(angles(unsolvedmask)).^2) - ...
    %         a.*sin(angles(unsolvedmask)) - ...
    %         errbound)) ./ ...
    %         (cos(2 .* angles(unsolvedmask))-1);
        r(unsolvedmask) = ra;
        b = sqrt(r(unsolvedmask).^2 - a.^2);
        c(:,unsolvedmask) = p2(:,unsolvedmask) + p21(:,unsolvedmask)./2 + ...
            bsxfun(@times,-n21(:,unsolvedmask),b);

        wasunsolved = unsolvedmask;

        % find circles with unsatisfied constraints
        unsolvedmask = unsolvedmask & ...
            (sqrt(sum((p2-c).^2)) - dot(p2-c,n21) > errbound*1.0001 | ...
             sqrt(sum((p2-c).^2)) - dot(p2-c,n23) > errbound*1.0001 | ...
             sqrt(sum((p2-c).^2)) > sqrt(sum((p1-c).^2)).*1.0001 | ...
             sqrt(sum((p2-c).^2)) > sqrt(sum((p3-c).^2)).*1.0001);

%         subd2(wasunsolved & not(unsolvedmask)) = true;
        type(wasunsolved & not(unsolvedmask)) = 3;
    end
    
    if any(unsolvedmask)
        % 2nd point constraint & 1st segment boundary error constraint
        % solve r = a /(cos(angles-asin(1-(errbound/r)))):
        a = p23len(unsolvedmask)/2;
        ra = (2.*(-sqrt(...
            2.*a.*errbound.*sin(angles(unsolvedmask)).*cos(angles(unsolvedmask)).^2 + ...
            errbound.^2.*cos(angles(unsolvedmask)).^2) - ...
            a.*sin(angles(unsolvedmask)) - ...
            errbound)) ./ ...
            (cos(2 .* angles(unsolvedmask))-1);
    %     rb = (2.*(sqrt(...
    %         2.*a.*errbound.*sin(angles(unsolvedmask)).*cos(angles(unsolvedmask)).^2 + ...
    %         errbound.^2.*cos(angles(unsolvedmask)).^2) - ....
    %         a.*sin(angles(unsolvedmask)) - ...
    %         errbound)) ./ ...
    %         (cos(2 .* angles(unsolvedmask))-1);
        r(unsolvedmask) = ra;
        b = sqrt(r(unsolvedmask).^2 - a.^2);
        c(:,unsolvedmask) = p2(:,unsolvedmask) + p23(:,unsolvedmask)./2 + ...
            bsxfun(@times,-n23(:,unsolvedmask),b);

        wasunsolved = unsolvedmask;

        % find circles with unsatisfied constraints
        unsolvedmask = unsolvedmask & ...
            (sqrt(sum((p2-c).^2)) - dot(p2-c,n21) > errbound*1.0001 | ...
             sqrt(sum((p2-c).^2)) - dot(p2-c,n23) > errbound*1.0001 | ...
             sqrt(sum((p2-c).^2)) > sqrt(sum((p1-c).^2)).*1.0001 | ...
             sqrt(sum((p2-c).^2)) > sqrt(sum((p3-c).^2)).*1.0001);

%         subd1(wasunsolved & not(unsolvedmask)) = true;
        type(wasunsolved & not(unsolvedmask)) = 4;
    end
    
    if any(unsolvedmask)
        error('last two cases not implemented yet.');

        % todo: last two cases:
        % 1st point constraint & 1st boundary constraint
        % 2nd point constraint & 2nd boundary constraint
        
%         type(wasunsolved & not(unsolvedmask)) = 5;
%         type(wasunsolved & not(unsolvedmask)) = 6;
    end
    
    % todo: maybe do every case for all points and then take the radius and
    % center of the case that satisfies constraints and has largest radius?
    % (otherwise there could be cases where one of the earlier cases
    % satisfies constraints but has smaller radius than one of the later
    % cases that also satisfies constraints?)
end

function ip = intersectSegments(segend,c,r)
    ip = zeros(2,0);
    for i=1:size(segend,2)
        [ip1,ip2,t1,t2] = linesegCircleIntersection([0;0],segend(:,i),c(:,i),r(i));
        if t1 > t2 && not(isnan(ip1(1)))
            ip(:,end+1) = ip1; %#ok<AGROW>
        else not(isnan(ip2(1)))
            ip(:,end+1) = ip2; %#ok<AGROW>
        end
    end
end
