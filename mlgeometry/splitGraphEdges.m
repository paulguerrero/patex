function edges = splitGraphEdges(splitedgeinds,splitpos,splitvertinds,edges)
    
    if isempty(splitedgeinds)
        return;
    end
    if not(isrow(splitedgeinds))
        splitedgeinds = splitedgeinds';
    end
    if not(isrow(splitpos))
        splitpos = splitpos';
    end
    [~,perm] = sortrows([splitedgeinds',splitpos']);
    splitedgeinds = splitedgeinds(perm);
%     splitpos = splitpos(perm);
    splitvertinds = splitvertinds(perm);
    edgeinds = find(diff(splitedgeinds)~=0);
    edgeinds = [[1,edgeinds+1];[edgeinds,numel(splitedgeinds)]];
    for i=1:size(edgeinds,2)
        edges = [edges,...
            [edges(1,splitedgeinds(edgeinds(1,i))),splitvertinds(edgeinds(1,i):edgeinds(2,i));...
             splitvertinds(edgeinds(1,i):edgeinds(2,i)),edges(2,splitedgeinds(edgeinds(1,i)))]]; %#ok<AGROW>
    end
    edges(:,splitedgeinds) = [];
end
