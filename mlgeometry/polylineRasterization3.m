% grid nodes assumed to be at integer coordinates
function [I,pixcoords] = polylineRasterization3(I,x,y,z,v,vperseg,zcircular)
    
    if nargin < 6 || isempty(vperseg)
        vperseg = false(size(v,1),1);
    end
    
    if nargin < 7 || isempty(zcircular)
        zcircular = false;
    end

    if not(iscell(v))
        v = {v};
    end

    % dda
    x = round(x);
    y = round(y);
    z = round(z);
    
    if numel(x) == 1
        if x(1) >= 1 && x(1) <= size(I,2) && ...
           y(1) >= 1 && y(1) <= size(I,1) && ...
           z(1) >= 1 && z(1) <= size(I,3)
            I(y(1),x(1),z(1),:) = cellfun(@(x) x(1),v);
            if nargout > 1
                pixcoords = [x(1);y(1);z(1)];
            end
        end
        return;
    end

    dx = abs(x(2:end)-x(1:end-1));
    dy = abs(y(2:end)-y(1:end-1));
    dz = abs(z(2:end)-z(1:end-1));
    
    if zcircular
        % circular z domain (like angle)
        % in a circular domain, the uppermost pixel = the the lowermost
        % pixel, e.g. -pi and pi is same value for circular domain
        mask = dz > (size(I,3)-1)/2;
        dz(mask) = (size(I,3)-1) - dz(mask);
    end
    
	m = max([dx;dy;dz],[],1);
    
%     mask = abs(dx)>=abs(dy);
%     m(mask) = abs(dx(mask));
%     m(not(mask)) = abs(dy(not(mask)));
    
    if x(1) >= 1 && x(1) <= size(I,2) && ...
	   y(1) >= 1 && y(1) <= size(I,1) && ...
       z(1) >= 1 && z(1) <= size(I,3)
        I(y(1),x(1),z(1),:) = cellfun(@(x) x(1),v);
        if nargout > 1
            pixcoords = [x(1);y(1);z(1)];
        end
    end
    for i=2:numel(x)
        sx = round(linspace(x(i-1),x(i),m(i-1)+1));
        sy = round(linspace(y(i-1),y(i),m(i-1)+1));
%         sz = round(linspace(z(i-1),z(i),m(i-1)+1));
        
        if zcircular && abs(z(i)-z(i-1)) > (size(I,3)-1)/2
            
            if z(i-1) > z(i)
                sz = linspace(z(i-1),z(i)+(size(I,3)-1),m(i-1)+1);
%                 mask = sz > size(I,3);
%                 sz(mask) = sz(mask) - (size(I,3)-1);
            else
                sz = linspace(z(i-1),z(i)-(size(I,3)-1),m(i-1)+1);
            end
            sz = mod(sz-1,(size(I,3)-1))+1;
            sz = round(sz);
        else
            sz = round(linspace(z(i-1),z(i),m(i-1)+1));
        end
        
        % clipping
        clipmask = sx >= 1 & sx <= size(I,2) & ...
                   sy >= 1 & sy <= size(I,1) & ...
                   sz >= 1 & sz <= size(I,3);
        clipmask(1) = false; % don't draw first point of polyline segment
        
        if sum(clipmask) > 0
            for j=1:numel(v)
                if vperseg(j)
                    sv = ones(1,m(i-1)+1).*v{j}(i-1);
                else
                    sv = linspace(v{j}(i-1),v{j}(i),m(i-1)+1);    
                end
                
                I(sub2ind(size(I),sy(clipmask),sx(clipmask),sz(clipmask),...
                    ones(1,sum(clipmask)).*j)) = sv(clipmask);
                if nargout > 1
                    pixcoords = [pixcoords,[sx(clipmask);sy(clipmask);sz(clipmask)]]; %#ok<AGROW>
                end
            end
        end
    end
end
 