% s: shape contexts
% sp: shape context positions
% sa: shape context angles
% ss: shape context scales (radii)
function [s,sp,sa,ss,distbinedges,anglebinedges] = polylineShapecontexts(x,y,closed,varargin)
    
    if isempty(x) || isempty(y) || isempty(closed)
        return;
    end

    if not(iscell(x))
        x = {x};
    end
    if not(iscell(y))
        y = {y};
    end
    
    [~,~,bbdiag] = pointsetBoundingbox([cell2mat(x);cell2mat(y)]);

    % input validation & default values
    params = inputParser;
    params.addOptional('sx',[],@(x) isnumeric(x) || isempty(x));
    params.addOptional('sy',[],@(x) isnumeric(x) || isempty(x));
    params.addOptional('sa',[],@(x) isnumeric(x) || isempty(x));
    params.addOptional('ss',[],@(x) isnumeric(x) || isempty(x));
    params.addParamValue('shapecontextspacing',bbdiag*0.02,@(x) isnumeric(x) && isscalar(x));
    params.addParamValue('scaleres',5,@(x) isnumeric(x) && isscalar(x));
    params.addParamValue('maxscale',bbdiag*0.2,@(x) isnumeric(x) && isscalar(x));
    params.addParamValue('samplespacing',-1,@(x) isnumeric(x) && isscalar(x));
    params.addParamValue('distres',8,@(x) isnumeric(x) && isscalar(x));
    params.addParamValue('angleres',16,@(x) isnumeric(x) && isscalar(x));
    params.parse(varargin);
    
    params = params.Results;
    
    % shape context positions & angles
    if isempty(params.sx) || isempty(params.sy) || isempty(params.sa)
        sp = [];
        sa = [];
        for i=1:numel(x)
            sp = [sp,polylineResample([x{i};y{i}],params.shapecontextspacing,'spacing',closed(i),true)]; %#ok<AGROW>
            normals = -polylineNormals(sp,closed(i));
            sa = [sa,cart2pol(normals(1,:),normals(2,:))]; %#ok<AGROW>
        end
    else
        sa = params.sa;
        params.sa = [];
        params.sx = [];
        params.sy = [];
    end
    
    % shape context scales
    if isempty(params.ss)
        ss = linexpspace(0,10,params.scaleres+1,0.1).*(params.maxscale/10);
        ss = ss(2:end);
        nss = numel(ss);
        ss = repmat(ss,1,size(sp,2));
        sp = reshape(repmat(sp,nss,1),2,[]);
        sa = reshape(repmat(sa,nss,1),1,[]);
    else
        ss = params.ss;
        params.ss = [];
    end
    
    if numel(sa) ~= size(sp,2) || numel(ss) ~= size(sp,2)
        error('Invalid shape context pose format.')
    end
    
    % polyline sample spacing
    if params.samplespacing <= 0
        params.samplespacing = max(bbdiag*0.001,min(ss)*0.5);
    end
    
    % polyline sample positions & arclengths
    samples = cell(size(x));
    samplearclen = cell(size(x));
    for i=1:numel(x)
        samples{i} = polylineResample([x{i};y{i}],params.samplespacing,'spacing',closed(i),true);
        samplearclen{i} = diff(polylineArclen(samples{i},closed(i)));
        if closed(i)
            samplearclen{i} = (samplearclen{i}([end,1:end-1])+samplearclen{i}) / 2;
        else
            samplearclen{i} = [...
                samplearclen{i}(1),...
                (samplearclen{i}(1:end-1)+samplearclen{i}(2:end))/2,...
                samplearclen{i}(end)];    
        end
    end
    samples = cell2mat(samples);
    samplearclen = cell2mat(samplearclen);
    
    % bin edges for a shape context in default position (0 angle and size 1)
    distbinedges = linexpspace(0,1,params.distres+1,0.9);
    anglebinedges = linspace(-pi,pi,params.angleres+1);
    
    % accumulate points to create shape contexts
    s = zeros(params.distres,params.angleres,size(sp,2));
    for i=1:size(sp,2)
        mask = (samples(1,:)-sp(1,i)).^2 + ...
               (samples(2,:)-sp(2,i)).^2 <= ss(i).^2;
        
        [coordsa,coordsd] = cart2pol(...
            samples(1,mask) - sp(1,i),...
            samples(2,mask) - sp(2,i));
        
        [~,dbin] = histc(coordsd./ss(i),distbinedges);
        [~,abin] = histc(mod(coordsa-sa(i)+pi,2*pi)-pi,anglebinedges);
        
        s(:,:,i) = accumarray([dbin;abin]',samplearclen(mask),[size(s,1),size(s,2)]);
    end
end
