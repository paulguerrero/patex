function intersectmask = trimeshTrimeshIntersection(verts1,faces1,verts2,faces2,bbmin1,bbmax1,bbmin2,bbmax2)
    
    if not(iscell(verts1))
        verts1 = {verts1};
        faces1 = {faces1};
    end
    
    if not(iscell(verts2))
        verts2 = {verts2};
        faces2 = {faces2};
    end
    
    if nargin < 5 || isempty(bbmin1)
        bbmin1 = zeros(3,numel(verts1));
        bbmax1 = zeros(3,numel(verts1));
        for i=1:numel(verts1)
            bbmin1(:,i) = min(verts1{i},[],2);
            bbmax1(:,i) = max(verts1{i},[],2);
        end
    end
    
    if nargin < 7 || isempty(bbmin2)
        bbmin2 = zeros(3,numel(verts2));
        bbmax2 = zeros(3,numel(verts2));
        for i=1:numel(verts2)
            bbmin2(:,i) = min(verts2{i},[],2);
            bbmax2(:,i) = max(verts2{i},[],2);
        end
    end
    
    % check for overlapping boundingboxes
    intersectmask = ...
        bsxfun(@lt,bbmin2(1,:),bbmax1(1,:)') & bsxfun(@lt,bbmin2(2,:),bbmax1(2,:)') & bsxfun(@lt,bbmin2(3,:),bbmax1(3,:)') & ...
        bsxfun(@gt,bbmax2(1,:),bbmin1(1,:)') & bsxfun(@gt,bbmax2(2,:),bbmin1(2,:)') & bsxfun(@gt,bbmax2(3,:),bbmin1(3,:)');
    
    for i=1:numel(verts1)
        for j=1:numel(verts2)
            if intersectmask(i,j)
                intersectmask(i,j) = trianglesetTrianglesetIntersection3D_cgal_mex(...
                    verts1{i}(:,faces1{i}(1,:)),verts1{i}(:,faces1{i}(2,:)),verts1{i}(:,faces1{i}(3,:)),...
                    verts2{j}(:,faces2{j}(1,:)),verts2{j}(:,faces2{j}(2,:)),verts2{j}(:,faces2{j}(3,:)));        
            end
        end
    end

end
