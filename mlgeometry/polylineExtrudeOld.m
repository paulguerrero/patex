% face normals will only point to the ouside if polyline is cw
% faceseginds: the index of the polyline edges corresponding to each face
% Multiple boundaries can be given as cell array, in that case the first
% boundary must be the outer boundary and all remaining boundaries holes.
function [verts,faces,faceedgeinds,vertpointinds,varargout] = polylineExtrude(p,extrvec,closed,varargin)
    
    if size(p,2) < 2
        error('Polyline must have at least two vertices.');
    end
   

    if abs(nargin) < 3 || isempty(closed)
        closed = false;
    end
    
    varargout = cell(1,0);
    
    settings = struct(...
        'topcap','flat',...
        'bottomcap','flat',...
        'downset',0,...
        'topcapheight',0,...
        'bottomcapheight',0,...
        'computemeshprops',false,...
        'innerfaces',false,...
        'cornervertinds',zeros(1,0),...
        'polylineOrientation',0,...    
        'matbasename','defaultmaterial',...
        'sidematname',{cell(1,0)});
    settings = nvpairs2struct(varargin,settings);
    
    % extend coordinates to 3D if necessary (assume they lie in the xy plane)
    if size(p,1) == 2
        p = [p;zeros(1,size(p,2))];
    end
    
    % create extrusion vertices
    verts = [bsxfun(@plus,p,-extrvec.*settings.downset),bsxfun(@plus,p,extrvec)];
    vertpointinds = [1:size(p,2),1:size(p,2)];
    
    origvertinds = 1:size(verts,2)/2;
    extrvertinds = size(verts,2)/2+1 : size(verts,2);

    % create extrusion faces
    if closed
        faces = [...
            origvertinds;...
            extrvertinds([2:end,1]);...
            origvertinds([2:end,1])];
        faceedgeinds = origvertinds;
        
        faces = [faces;[...
            origvertinds;...
            extrvertinds;...
            extrvertinds([2:end,1])]];
        faceedgeinds = [faceedgeinds;origvertinds];
        
        faces = reshape(faces,3,[]);
        faceedgeinds = faceedgeinds(:)';
    else
        faces = [...
            origvertinds(1:end-1);...
            extrvertinds(2:end);...
            origvertinds(2:end)];
        faceedgeinds = origvertinds(1:end-1);
        
        faces = [faces;[...
            origvertinds(1:end-1);...
            extrvertinds(1:end-1);...
            extrvertinds(2:end)]];
        faceedgeinds = [faceedgeinds;origvertinds(1:end-1)];
        
        faces = reshape(faces,3,[]);
        faceedgeinds = faceedgeinds(:)';
    end
    
    % add top cap
    if closed && not(strcmp(settings.topcap,'none'))
        nfaces = size(faces,2);
        nverts = size(verts,2);
        if dot(extrvec,[0;0;1]) < 0
            capnormal = [0;0;-1];
        else
            capnormal = [0;0;1];
        end
        nvpairs = {};
        if strcmp(settings.topcap,'simpleroof')
            nvpairs = {'height',settings.topcapheight};
        end
        capborderpolyvinds = extrvertinds;
        [verts,faces] = addcap(verts,faces,capborderpolyvinds,capnormal,settings.topcap,nvpairs{:});
        faceedgeinds = [faceedgeinds,ones(1,size(faces,2)-nfaces).*-1];
        vertpointinds = [vertpointinds,ones(1,size(verts,2)-nverts).*-1];
    end
    
    % add bottom cap
    if closed && not(strcmp(settings.bottomcap,'none'))
        nfaces = size(faces,2);
        nverts = size(verts,2);
        if dot(extrvec,[0;0;1]) < 0
            capnormal = [0;0;1];
        else
            capnormal = [0;0;-1];
        end
        nvpairs = {};
        if strcmp(settings.bottomcap,'simpleroof')
            nvpairs = {'height',settings.bottomcapheight};
        end
        capborderpolyvinds = origvertinds([1,end:-1:2]); % reverse winding, so polygon points downwards
        [verts,faces] = addcap(verts,faces,capborderpolyvinds,capnormal,settings.bottomcap,nvpairs{:});
        newfaceinds = nfaces+1:size(faces,2);
        faces(:,newfaceinds) = faces([1,3,2],newfaceinds); % flip faces for the bottom cap
        faceedgeinds = [faceedgeinds,ones(1,size(faces,2)-nfaces).*-2];
        vertpointinds = [vertpointinds,ones(1,size(verts,2)-nverts).*-2];
    end
    
    % polylines are ccw when viewed from the front (which faces direction
    % [0;0;1]), so if the extrusion goes in in the other direction,
    % the faces have to change vertex ordering to remain ccw
    if xor(dot(extrvec,[0;0;1]) < 0, settings.innerfaces)
        faces = faces([1,3,2],:);
    end
    
    % mesh properties
    if settings.computemeshprops
        meshprops = struct;
        meshprops.fareas = triangleArea(verts(:,faces(1,:)),verts(:,faces(2,:)),verts(:,faces(3,:)));
        meshprops.fnormals = trimeshFaceNormals(verts,faces);

        if isempty(settings.cornervertinds)
            segvertinds = {[1:size(p,2),1]};
        else
            cornervertinds = sort(settings.cornervertinds,'ascend');
            if closed
                nvertangles = size(p,2);
                segvertinds = cellfun(@(x,y)...
                    {iif(x<y,x:y,[x:nvertangles,1:y])},...
                    num2cell(cornervertinds),num2cell(cornervertinds([2:end,1])));
            else
                nvertangles = size(p,2)-2;
                segvertinds = cellfun(@(x,y)...
                    {iif(x<y,x:y,[x:nvertangles,1:y])},...
                    num2cell(cornervertinds(1:end-1)),num2cell(cornervertinds(2:end)));
            end

        end
        
        meshprops.texcoords = zeros(2,0);
        meshprops.facetexcoordinds = zeros(size(faces));
        meshprops.vnormals = zeros(3,0);
        meshprops.facevnormalinds = zeros(size(faces));
        meshprops.facematerialind = zeros(1,size(faces,2));
        meshprops.facesmoothgroupind = zeros(1,size(faces,2));
        meshprops.materialnames = cell(1,0);

        % for side faces
        sidefacemask = faceedgeinds > 0;
        vinds = unique(faces(:,sidefacemask));
        newvinds(vinds) = 1:numel(vinds);

        alen = polylineArclen(p,closed);
        if settings.innerfaces
            alen = alen(end)-alen;
        end

        extrveclen = sqrt(sum(extrvec.^2,1));
        if extrveclen < 0.0000001
            extrvec_norm = [0;0;1];
        else
            extrvec_norm = extrvec ./ extrveclen;
        end
        texcoords = [...
            alen(vertpointinds(vinds));...
            extrvec_norm'*(verts(:,vinds)-p(:,vertpointinds(vinds)))];

        offset = size(meshprops.texcoords,2);
        meshprops.texcoords(:,end+1:end+size(texcoords,2)) = texcoords;
        meshprops.facetexcoordinds(:,sidefacemask) = newvinds(faces(:,sidefacemask))+offset;

        if closed
            % different texture coordinates for vertices that are at the end of
            % the basecurve
            endfaceinds = find(faceedgeinds == size(p,2));
            f = faces(:,endfaceinds);
            ftc = meshprops.facetexcoordinds(:,endfaceinds);
            fvinds = find(vertpointinds(f) == 1);
            [tcinds,~,fvtcinds] = unique(ftc(fvinds));
            offset = size(meshprops.texcoords,2);
            meshprops.texcoords(:,end+1:end+numel(tcinds)) = [...
                alen(ones(1,numel(tcinds)).*end);...
                meshprops.texcoords(2,tcinds)];
            ftc(fvinds) = fvtcinds+offset;
            meshprops.facetexcoordinds(:,endfaceinds) = ftc;
        end

        for i=1:numel(segvertinds)
            faceinds = find(ismember(faceedgeinds,segvertinds{i}(1:end-1)));
            if isempty(faceinds)
                continue;
            end

            offset = size(meshprops.vnormals,2);

            [v,f] = submesh(verts,faces,faceinds);
            vnormals = trimeshVertexNormals(v,f,'faceavg_area',meshprops.fnormals(:,faceinds),meshprops.fareas(faceinds));
            facevnormalinds = f;

            meshprops.vnormals(:,end+1:end+size(vnormals,2)) = vnormals;
            meshprops.facevnormalinds(:,faceinds) = facevnormalinds+offset;

            if numel(settings.sidematname) >= i
                matname = [settings.sidematname{i},'_side'];
            else
                matname = [settings.matbasename,'_side'];
            end
            matind = find(strcmp(matname,meshprops.materialnames),1,'first');
            if isempty(matind)
                meshprops.materialnames{:,end+1} = matname;
                matind = numel(meshprops.materialnames);
            end
            meshprops.facematerialind(:,faceinds) = matind;
            if isempty(meshprops.facesmoothgroupind)
                sgind = 1;
            else
                sgind = max(meshprops.facesmoothgroupind)+1;
            end
            meshprops.facesmoothgroupind(:,faceinds) = sgind;
        end

        % for cap faces
        for i=1:2
            if i==1
                capfacemask = faceedgeinds == -1;
                captype = settings.topcap;
            else
                capfacemask = faceedgeinds == -2;
                captype = settings.bottomcap;
            end

            if any(capfacemask)
                vinds = unique(faces(:,capfacemask));
                newvinds(vinds) = 1:numel(vinds);

                obbox = pointsetOrientedBoundingbox(p(1:2,:),settings.polylineOrientation);
                origin = obbox(:,1)+extrvec(1:2); % basecurve moved in the basecurve plane by the extrude vector
                xaxis = obbox(:,2);
                yaxis = obbox(:,3);
                xaxis = xaxis./sqrt(sum(xaxis.^2,1));
                yaxis = yaxis./sqrt(sum(yaxis.^2,1));

                texcoords = [xaxis';yaxis'] * bsxfun(@minus,verts(1:2,vinds),origin);

                offset = size(meshprops.texcoords,2);
                meshprops.texcoords(:,end+1:end+size(texcoords,2)) = texcoords;
                meshprops.facetexcoordinds(:,capfacemask) = newvinds(faces(:,capfacemask))+offset;

                if strcmp(captype,'flat')
                    capfaceinds = find(capfacemask);
                    offset = size(meshprops.vnormals,2);

                    [v,f] = submesh(verts,faces,capfaceinds);
                    vnormals = trimeshVertexNormals(v,f,'faceavg_area',meshprops.fnormals(:,capfaceinds),meshprops.fareas(capfaceinds));
                    facevnormalinds = f;

                    meshprops.vnormals(:,end+1:end+size(vnormals,2)) = vnormals;
                    meshprops.facevnormalinds(:,capfaceinds) = facevnormalinds+offset;

                    if i==1
                        matname = [settings.matbasename,'_top_flat'];
                    else
                        matname = [settings.matbasename,'_bottom_flat'];
                    end
                    matind = find(strcmp(matname,meshprops.materialnames),1,'first');
                    if isempty(matind)
                        meshprops.materialnames{:,end+1} = matname;
                        matind = numel(meshprops.materialnames);
                    end
                    meshprops.facematerialind(:,capfaceinds) = matind;
                    if isempty(meshprops.facesmoothgroupind)
                        sgind = 1;
                    else
                        sgind = max(meshprops.facesmoothgroupind)+1;
                    end
                    meshprops.facesmoothgroupind(:,capfaceinds) = sgind;
                elseif strcmp(captype,'simpleroof')
                    capfaceinds = find(capfacemask);
                    offset = size(meshprops.vnormals,2);

                    % separate vertex normals for each face
                    vnormals = [meshprops.fnormals(:,capfaceinds);...
                                meshprops.fnormals(:,capfaceinds);...
                                meshprops.fnormals(:,capfaceinds)];
                    vnormals = reshape(vnormals,3,[]);
                    normalinds = reshape(1:size(vnormals,2),3,[]);
                    meshprops.vnormals(:,end+1:end+size(vnormals,2)) = vnormals;
                    meshprops.facevnormalinds(:,capfaceinds) = normalinds+offset;

                    if i==1
                        matname = [settings.matbasename,'_top_angled'];
                    else
                        matname = [settings.matbasename,'_bottom_angled'];
                    end
                    matind = find(strcmp(matname,meshprops.materialnames),1,'first');
                    if isempty(matind)
                        meshprops.materialnames{:,end+1} = matname;
                        matind = numel(meshprops.materialnames);
                    end
                    meshprops.facematerialind(:,capfaceinds) = matind;
                    meshprops.facesmoothgroupind(:,capfaceinds) = 0; % 0 for no smoothing group
                else
                    error('Unknown cap type.');
                end

            end
        end
        
        varargout{end+1} = segvertinds;
        varargout{end+1} = meshprops;
    end
end

function [v,f] = submesh(verts,faces,faceinds)
    f = faces(:,faceinds);
    vinds = unique(f(:));
    newvinds(vinds) = (1:numel(vinds))';
    f = newvinds(f);
    v = verts(:,vinds);
end

function [verts,faces] = addcap(verts,faces,capborderpolyvinds,capnormal,captype,varargin)
            
    if isempty(capborderpolyvinds)
        return;
    end
    
    settings = struct(...
        'height',0);
    settings = nvpairs2struct(varargin,settings);

    if strcmp(captype,'flat')
        % dt seems to generate ccw triangles
        dt = delaunayTriangulation(verts(1:2,capborderpolyvinds)',...
            [1:numel(capborderpolyvinds);[2:numel(capborderpolyvinds),1]]');
        faces = [faces,capborderpolyvinds(dt.ConnectivityList(dt.isInterior,:)')];
    elseif strcmp(captype,'simpleroof')
        
        roofheight = settings.height;
        roofbaseheight = verts(3,capborderpolyvinds(1));
        
        % move cap border vertices by a small random vector in the plane of
        % the cap to avoid numeric issues with the skeleton
        bverts = verts(1:2,capborderpolyvinds);
        [~,~,capbbdiag] = pointsetBoundingbox(bverts);
        bverts = bverts + rand(size(bverts)).*(capbbdiag*0.0000001);
        verts(1:2,capborderpolyvinds) = bverts;
        
        % use straight line skeleton for the roof
        [sverts,sheight,~,~,sfaces] = polygonStraightSkeleton_mex(verts(1:2,capborderpolyvinds));
        
        % compute height manually (cgal sometimes returns negative values
        % for no good reason)
%         maxheight = max(sheight);
%         sheight = zeros(1,size(sverts,2));
        newvertinds = numel(capborderpolyvinds)+1:size(sverts,2);
        if not(isempty(newvertinds))
            mask = sheight(newvertinds) < 0;
            [sheight(newvertinds(mask)),~] = pointPolylineDistance_mex(...
                sverts(1,newvertinds(mask)),...
                sverts(2,newvertinds(mask)),...
                verts(1,capborderpolyvinds),...
                verts(2,capborderpolyvinds));
        end
        
        
        % triangulate faces
        hasdegeneracies = false;
        stris = zeros(3,0);
        for i=1:numel(sfaces)
            dt = delaunayTriangulation(sverts(1:2,sfaces{i})',...
                [1:numel(sfaces{i});[2:numel(sfaces{i}),1]]');
            facetris = dt.ConnectivityList(dt.isInterior,:)';
            sfaces{i} = sfaces{i}';
            
            % in degenerate cases dt generates additional vertices, add
            % those vertices to the list
            additionalvinds = unique(facetris(facetris > numel(sfaces{i})));
            if not(isempty(additionalvinds))
                hasdegeneracies = true;
                break;
%                 warning('Degnerate polygons created by the roof straight skeleton, mesh is probably not consistent anymore.');
%                 sfaces{i}(additionalvinds) = size(sverts,2)+1:size(sverts,2)+numel(additionalvinds);
%                 newverts = dt.Points(additionalvinds,:)';
%                 [~,minind] = min(pdist2(newverts',sverts'),[],2);
%                 newheight = sheight(minind');
%                 sverts(:,end+1:end+numel(additionalvinds)) = newverts;
%                 sheight(:,end+1:end+numel(additionalvinds)) = newheight;
            end
            
            stris = [stris,sfaces{i}(facetris)]; %#ok<AGROW>
        end
        
        if hasdegeneracies
            % straight line skeleton has degeneracies, try inner voronoi
            % diagram instead
            
            [sx,sy,sheight,binds,~,~,sfaces] = ...
                polygonInnerVoronoiDiagram(verts(1,capborderpolyvinds),verts(2,capborderpolyvinds),'distance');
            if numel(binds) ~= numel(capborderpolyvinds) || not(all(binds == 1:numel(capborderpolyvinds)))
                error('Skeleton vertices not sorted correctly.');
            end
            sverts = [sx;sy];
            
            % triangulate faces
            hasdegeneracies = false;
            stris = zeros(3,0);
            for i=1:numel(sfaces)
                dt = delaunayTriangulation(sverts(1:2,sfaces{i})',...
                    [1:numel(sfaces{i});[2:numel(sfaces{i}),1]]');
                facetris = dt.ConnectivityList(dt.isInterior,:)';
                sfaces{i} = sfaces{i}';

                % in degenerate cases dt generates additional vertices, add
                % those vertices to the list
                additionalvinds = unique(facetris(facetris > numel(sfaces{i})));
                if not(isempty(additionalvinds))
                    hasdegeneracies = true;
                    break;
                end
                
                stris = [stris,sfaces{i}(facetris)]; %#ok<AGROW>
            end
            
        end
        
        if hasdegeneracies
            % inner voronoi diagram has degenerarcies as well, use flat cap
            % instead
            
            dt = delaunayTriangulation(verts(1:2,capborderpolyvinds)',...
                [1:numel(capborderpolyvinds);[2:numel(capborderpolyvinds),1]]');
            faces = [faces,capborderpolyvinds(dt.ConnectivityList(dt.isInterior,:)')];
        else
            sheight = sheight .* (roofheight/max(sheight));
            sverts = [sverts;ones(1,size(sverts,2)).*roofbaseheight];
            sverts = sverts + bsxfun(@times,capnormal,sheight);

            skel2meshvertind = zeros(1,size(sverts,2));
            skel2meshvertind(1:numel(capborderpolyvinds)) = capborderpolyvinds;
            skel2meshvertind(numel(capborderpolyvinds)+1:end) = size(verts,2)+1 : size(verts,2)+size(sverts,2)-numel(capborderpolyvinds);

            verts = [verts,sverts(:,numel(capborderpolyvinds)+1:end)];
            faces = [faces,skel2meshvertind(stris)];
        end
    else
        error('Unkown cap type.');
    end
    
end
