function pdiff = posediff(dstpose,srcpose)
    if size(srcpose,1) == 11
        % 3d pose
        pdiff = zeros(11,1);
        pdiff(1:3) = dstpose(1:3) - srcpose(1:3);
        pdiff(4:7) = quatmultiply(dstpose(4:7)',quatconj(srcpose(4:7)'))';
        pdiff(8:10) = dstpose(8:10) ./ srcpose(8:10);
        pdiff(11) = dstpose(11) ~= srcpose(11);
    elseif size(pose,1) == 6
        % 2d pose
        error('Not yet implemented.');
    else
        error('Unknown pose type.');
    end
end
