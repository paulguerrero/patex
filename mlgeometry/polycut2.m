function [xv,yv] = polycut2(xp1,yp1)
%POLYCUT2 Remove polygon holes by connecting contours and holes.
%
%   [lat2,long2] = POLYCUT2(lat,long) connects the contour and holes of
%   polygons. In contrast to POLYCUT, POLYCUT2 finds a output polygon with
%   smaller total contour length and avoids cases where the cuts interesect
%   with the polygon contours. Polygons are input as NaN-delimited vectors,
%   or as cell arrays containing individual polygons in each element with
%   the outer face separated from the subsequent inner faces by NaNs.
%   Multiple polygons outputs are separated by NaNs.
%
%   See also POLYCUT, POLYBOOL, POLYSPLIT, POLYJOIN.
%
% Written by:  P. Guerrero

xv = zeros(0,1);
yv = zeros(0,1);

if isempty(xp1) && isempty(yp1)
    return;
end

% convert inputs to cell
if not(iscell(xp1))
    xp1 = {xp1};
    yp1 = {yp1};
end

for n=1:length(xp1)
    
    % bring to correct format: one cell containing a column vector per contour
    if not(iscolumn(xp1{n}))
        xp1{n} = xp1{n}(:);
    end
    if not(iscolumn(yp1{n}))
        yp1{n} = yp1{n}(:);
    end
    [xc,yc] = polysplit(xp1{n},yp1{n});
    
    % remove duplicate vertices at the start/end
    for i=1:numel(xc)
        if xc{i}(1) == xc{i}(end) && yc{i}(1) == yc{i}(end)
            xc{i}(end) = [];
            yc{i}(end) = [];
        end
    end
    
    % bring contours to correct winding order
    [xc{1},yc{1}] = poly2cw(xc{1},yc{1}); % outer contours clockwise
    if numel(xc) >= 1
        [xc(2:end),yc(2:end)] = poly2cw(xc(2:end),yc(2:end)); % inner contours counter-clockwise
    end
    
    if isempty(xc)
        
        % no contours
        x = zeros(0,1);
        y = zeros(0,1);
        
    elseif numel(xc) == 1
        
        % no holes
        x = xc{1};
        y = yc{1};
   
    else

        % ***** polygon cut algorithm *****
        % changed from polycut: record the distances to all contours that
        % have been computed and add the cut between any visited and any
        % unvisited contour that has the shortest distance (same number of
        % distance computations, but somewhat more memory)
        
        nc = numel(xc);
        
        % making matrices sparse does not really pay off, it would just
        % half the memory more or less and make the method slower
        dists = zeros(nc,nc);
        vfrom = zeros(nc,nc);
        vto = zeros(nc,nc);
        
        cuts = false(nc,nc);
        
        visited = zeros(1,0);
        unvisited = 1:nc;
        
        polyind = 1;
        
        % Starting with the contour polygon, find the shortest distance
        % between any visited and any unvisited polygon and connect the
        % corresponding polygons. Repeat until all polygons have been
        % visited.
        for k=1:nc-1
            
            unvisited(unvisited == polyind) = [];
            visited(end+1) = polyind; %#ok<AGROW>
           
            x1 = xc{polyind};
            y1 = yc{polyind};
            for m=1:numel(unvisited)
                x2 = xc{unvisited(m)};
                y2 = yc{unvisited(m)};
             
                distsq = bsxfun(@minus,x1,x2').^2 + bsxfun(@minus,y1,y2').^2;
                
                [dist,minvto] = min(distsq,[],2);
                [mindist,minvfrom] = min(dist);
                minvto = minvto(minvfrom);
                mindist = sqrt(mindist);
                
                vfrom(polyind,unvisited(m)) = minvfrom;
                vto(polyind,unvisited(m)) = minvto;
                dists(polyind,unvisited(m)) = mindist;
            end
            
            [dist,unvisitedind] = min(dists(visited,unvisited),[],2);
            [~,visitedind] = min(dist);
            unvisitedind = unvisited(unvisitedind(visitedind));
            visitedind = visited(visitedind);
            
            cuts(visitedind,unvisitedind) = true;
            
            polyind = unvisitedind;
        end
        
        clear dists;
        
        % only keep entries for the chosen cuts
        vfrom = vfrom .* cuts;
        vto = vto .* cuts;
        
        % create second half of half-edges representing the cuts
        vfrom_t = vfrom;
        vfrom = vfrom + vto';
        vto = vto + vfrom_t';
        
        clear cuts vfrom_t;
        
        x = zeros(1,0);
        y = zeros(1,0);
        polyind = 1;
        vin = 1;
        while nnz(vfrom(polyind,:)) > 0 % while there are outgoing edges
            
            % get the next outgoing edge along the polygon contour
            polyinds2 = find(vfrom(polyind,:));
            [~,minind] = min(mod(vfrom(polyind,polyinds2) - (vin+1), numel(xc{polyind})));
            polyind2 = polyinds2(minind);
            vout = vfrom(polyind,polyind2);
            
            % add vertices between the incoming edge we arrived on and next
            % outgoing edge to the output contour
            if vin >= vout
                x = [x;xc{polyind}([vin:end,1:vout])]; %#ok<AGROW>
                y = [y;yc{polyind}([vin:end,1:vout])]; %#ok<AGROW>
            else
                x = [x;xc{polyind}(vin:vout)]; %#ok<AGROW>
                y = [y;yc{polyind}(vin:vout)]; %#ok<AGROW>
            end

            % move along outgoing edge to next polygon contour
            % and remove the outgoing edge (we won't be needing it anymore)
            vin = vto(polyind,polyind2);
            vfrom(polyind,polyind2) = 0;
            vto(polyind,polyind2) = 0;
            polyind = polyind2;
        end
        
        x = [x;xc{polyind}([vin:end,1])]; %#ok<AGROW>
        y = [y;yc{polyind}([vin:end,1])]; %#ok<AGROW>
        
    end

    xv = [xv; x; nan]; %#ok<AGROW>
    yv = [yv; y; nan]; %#ok<AGROW>

    clear x y polystruc;
end

% remove trailing nan
xv(end) = [];  yv(end) = [];

end
