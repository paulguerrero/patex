function [verts,edges] = mergeGraphVertices(mergefrom,mergeto,verts,edges)
    
    % connect all edges connected to mergefrom to mergeto instead
    vindmap = (1:size(verts,2))';
    vindmap(mergefrom) = mergeto;
    edges = vindmap(edges);
    
    % remove vertices and update edges the reflect the new vertex indices
    vindmap = nan(1,size(verts,2))';
    mask = true(1,size(verts,2));
    mask(mergefrom) = false;
    verts(:,mergefrom) = [];
    vindmap(mask) = 1:size(verts,2);
    edges = vindmap(edges);
end
