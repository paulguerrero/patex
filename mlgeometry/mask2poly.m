function polys = mask2poly(BW)

    [B,~,~,A] = bwboundaries(BW);
    rinds = zeros(1,0);
    polys = cell(1,0);
    polyholes = cell(1,0);
    while(not(isempty(B)))
        for i=1:length(B),
            if(~sum(A(i,:)))
                polys{:,end+1} = B{i}'; %#ok<AGROW>
                polyholes{:,end+1} = cell(1,0); %#ok<AGROW>
                rinds(:,end+1) = i; %#ok<AGROW>
%                 boundary = B{i};
%                 plot(boundary(:,2),...
%                     boundary(:,1),'r','LineWidth',2);
                for j=find(A(:,i))'
                    polyholes{:,end}{:,end+1} = B{j}';
                    rinds(:,end+1) = j; %#ok<AGROW>
%                     boundary = B{j};
%                     plot(boundary(:,2),...
%                         boundary(:,1),'g','LineWidth',2);
                end
            end
        end
        B(rinds) = [];
        A(rinds,:) = [];
        A(:,rinds) = [];
        rinds = zeros(1,0);
    end
    hold off;

    % offset by half a pixel outside
    polysx = cell(1,0);
    polysy = cell(1,0);
    polyholesx = cell(1,0);
    polyholesy = cell(1,0);
    for i=1:numel(polys)
        x = cell(1,1+numel(polyholes{i}));
        y = cell(1,1+numel(polyholes{i}));
        x{1} = polys{i}(1,1:end-1); % last = first vertex
        y{1} = polys{i}(2,1:end-1); % last = first vertex
        for j=1:numel(polyholes{i})
            x{j+1} = polyholes{i}{j}(1,1:end-1); % last = first vertex
            y{j+1} = polyholes{i}{j}(2,1:end-1); % last = first vertex
        end
        
        % handle countours with two points or less
        for j=1:numel(x)
            if numel(x{j}) == 2
                % add vertex at the center of the single edge
                x{j} = [x{j}(1),sum(x{j}).*0.5,x{j}(2)];
                y{j} = [y{j}(1),sum(y{j}).*0.5,y{j}(2)];
            elseif numel(x{j}) == 1
                x{j} = x{j}([1,1,1]);
                y{j} = y{j}([1,1,1]);
            elseif numel(x{j}) == 0
                warning('Empty countour found.');
                continue;
            end
        end
        
        % do offsets (outer and inner contours need to be offset by half a
        % pixel outerwards)
        % just use the first component, ignore the others (should not have
        % multiple components with less than half a pixel offset)
        [xo,yo] = polygonOffset(x{1},y{1},0.49,16);
        if numel(xo) ~= 1 || numel(xo{1}) ~= 1
            warning('ignoring multiple components during offsetting.');
        end
        polysx(:,end+1) = xo{1}(1); %#ok<AGROW>
        polysy(:,end+1) = yo{1}(1); %#ok<AGROW>
        polysx(:,end+1) = xo{1}(1); %#ok<AGROW>
        polysy(:,end+1) = yo{1}(1); %#ok<AGROW>
        polyholesx{:,end+1} = cell(1,0); %#ok<AGROW>
        polyholesy{:,end+1} = cell(1,0); %#ok<AGROW>
        for j=2:numel(x)
            [xo,yo] = polygonOffset(x{j},y{j},0.49,16);
            if numel(xo) ~= 1 || numel(xo{1}) ~= 1
                warning('ignoring multiple components during offsetting.');
            end
            polyholesx{end}(:,end+1) = xo{1}(1);
            polyholesy{end}(:,end+1) = yo{1}(1);
        end
    end

    % simplify
    for i=1:numel(polysx)
        p = polylineSimplify([polysx{i}',polysy{i}'],0.5)';
        polysx{i} = p(1,:);
        polysy{i} = p(2,:);
        for j=1:numel(polyholes{i})
            polyholes{i}{j} = polylineSimplify(polyholes{i}{j}',0.5)';
            p = polylineSimplify([polyholesx{i}',polyholesy{i}'],0.5)';
            polyholesx{i} = p(1,:);
            polyholesy{i} = p(2,:);
        end
    end

    % cut to remove holes
    polys = cell(1,numel(polysx));
    for i=1:numel(polysx)
    %     polys{i}(:,end+1) = nan;
    %     for j=1:numel(polyholes{i})
    %         polyholes{i}{j}(:,end+1) = nan;
    %     end
    %     polys{i} = [polys{i},polyholes{i}{:}];
        [x,y] = polycut([polysx(i),polyholesx{i}],[polysy(i),polyholesy{i}]);
        polys{i} = [x';y'];
    end

end
