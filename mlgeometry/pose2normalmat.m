function m = pose2normalmat(targetpose,currentpose,uniformscale)
    % if no current pose is given, assume it is:
    %    currentpose = [...
    %        0;0;0;...   % position
    %        1;0;0;0;... % rotation (identity quaternion)
    %        1;1;1;...   % scale
    %        0];         % mirrored
    
    if nargin < 3
        uniformscale = false;
    end
    
    % convert to non-uniform scale format
    if uniformscale
        if size(targetpose,1) == 8 || size(targetpose,1) == 9
            targetpose = targetpose([1:7,8,8,8,9:end],:);
        else
            error('Invalid pose format.');
        end
        if nargin >= 3 && not(isempty(currentpose))
            if size(currentpose,1) == 8 || size(currentpose,1) == 9
                currentpose = currentpose([1:7,8,8,8,9:end],:);
            else
                error('Invalid pose format.');
            end
        end
    end
    
    if nargin >= 3 && not(isempty(currentpose))
        if size(targetpose,1) ~= size(currentpose,1)
            error('Pose format does not match for current and target poses.');
        end

        % avoid problems with scaling if both sizes are close to zero
        if size(currentpose,1) == 11 || size(currentpose,1) == 10
            mask = bsxfun(@and,abs(currentpose(8:10,:)) < eps('double')*100,abs(targetpose(8:10,:)) < eps('double')*100);
            if any(mask)
                % need to expand if there are not the same number of both
                % poses
                if size(currentpose,2) < size(targetpose,2)
                    if size(currentpose,2) ~= 1
                        error('Pose counts do not match.');
                    end
                    currentpose = currentpose(:,ones(1,size(targetpose,2)));
                end
                if size(targetpose,2) < size(currentpose,2)
                    if size(targetpose,2) ~= 1
                        error('Pose counts do not match.');
                    end
                    targetpose = targetpose(:,ones(1,size(currentpose,2)));
                end
                
                currentpose(8,mask(1,:)) = 1;
                targetpose(8,mask(1,:)) = 1;
                currentpose(9,mask(2,:)) = 1;
                targetpose(9,mask(2,:)) = 1;
                currentpose(10,mask(3,:)) = 1;
                targetpose(10,mask(3,:)) = 1;
            end
        else
            error('Invalid pose format.');
        end
        
        if size(currentpose,1) == 11
            % pose with mirroring
            minv = transformNormalMat3Dinv(currentpose(8:10,:), currentpose(4:7,:), currentpose(11,:));
        elseif size(currentpose,1) == 10
            minv = transformNormalMat3Dinv(currentpose(8:10,:), currentpose(4:7,:));
        else
            error('Invalid pose format.');
        end
        
    end
    
    if size(targetpose,1) == 11
        % pose with mirroring
        m = transformNormalMat3D(targetpose(8:10,:), targetpose(4:7,:), targetpose(11,:));
    elseif size(currentpose,1) == 10
        m = transformNormalMat3D(targetpose(8:10,:), targetpose(4:7,:));
    else
        error('Invalid pose format.');        
    end
    
    if nargin >= 3 && not(isempty(currentpose))
        for i=1:size(m,3)
            m(:,:,i) = m(:,:,i) * minv(:,:,i);
        end
    end
end
