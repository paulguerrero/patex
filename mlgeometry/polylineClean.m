% [p,ind] = polylineClean(p,polyclosed,minseglen)
% [x,y,ind] = polylineClean(x,y,polyclosed,minseglen)
function varargout = polylineClean(varargin)

    cellinput = true;
    if not(iscell(varargin{1}))
        varargin{1} = varargin(1);
        if numel(varargin) >= 4
            varargin{2} = varargin(2);
        end
        cellinput = false;
    end

    if numel(varargin) == 4
        x = varargin{1};
        y = varargin{2};
        polyclosed = varargin{3};
        minseglen = varargin{4};
        varargout = cell(1,3);
    elseif numel(varargin) == 3
        x = cell(1,numel(varargin{1}));
        y = cell(1,numel(varargin{1}));
        for i=1:numel(varargin{1})
            x{i} = varargin{1}{i}(1,:);
            y{i} = varargin{1}{i}(2,:);
        end
        polyclosed = varargin{2};
        minseglen = varargin{3};
        varargout = cell(1,2);
    else
        error('Invalid arguments.');
    end

    i = 1;
    c = 1;
    minseglensq = minseglen^2;
    ind = zeros(1,0);
    while i <= numel(x)
        if numel(x{i}) < 2
            x(i) = [];
            y(i) = [];
            polyclosed(i) = [];
        else
            if polyclosed(i)
                x{i}(:,end+1) = x{i}(:,1);
                y{i}(:,end+1) = y{i}(:,1);
            end
            j = 1;
            while j < numel(x{i})
                seglensq = (x{i}(j+1) - x{i}(j))^2 + (y{i}(j+1) - y{i}(j))^2;                
                if seglensq < minseglensq
                    x{i}(j+1) = [];
                    y{i}(j+1) = [];
                else
                    j = j + 1;
                end
            end
            if polyclosed(i) && not(isempty(x{i}))
                x{i}(:,end) = [];
                y{i}(:,end) = [];
            end
            if numel(x{i}) < 2
                x(i) = [];
                y(i) = [];
                polyclosed(i) = [];
            else
                ind(i) = c;
                i = i + 1;
            end
        end
        c = c + 1;
    end
    
    if numel(varargin) == 3
        varargout{1} = cell(1,numel(x));
        for i=1:numel(x)
            varargout{1}{i} = [x{i};y{i}];
        end
        varargout{2} = ind;
    else
        varargout{1} = x;
        varargout{2} = y;
        varargout{3} = ind;
    end
    
    if not(cellinput)
        if numel(varargin) == 3
            varargout{1} = [varargout{1}{:}];
        else
            varargout{1} = [varargout{1}{:}];
            varargout{2} = [varargout{2}{:}];
        end
    end
    
end
