function alen = polylineArclen(poly,closed)
    if isempty(poly)
        alen = zeros(1,0);
        return;
    end

    if closed
        alen = sqrt((poly(1,[2:end,1])-poly(1,:)).^2 + ...
                    (poly(2,[2:end,1])-poly(2,:)).^2);
    else
        alen = sqrt((poly(1,2:end)-poly(1,1:end-1)).^2 + ...
                    (poly(2,2:end)-poly(2,1:end-1)).^2);    
    end
    alen = [0,cumsum(alen)];
end
