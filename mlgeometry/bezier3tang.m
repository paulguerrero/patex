% BEZIER3CURV Tangent of a cubic bezier spline.
%   val = BEZIER3CURV(p1,p2,p3,p4,t) returns the tangent of the cubic
%   bezier spline defined by the 4 control points p1,p2,p3,p4 at the
%   arclength parameter t
function val = bezier3tang(p1,p2,p3,p4,t)
  
    %%    
    if not(isvector(p1)) || not(isvector(t))
        error('control points and arclength parameters must be vectors');
    end
    
    if min(t) < 0 || max(t) > 1
        error('t must be in [0,1]');
    end
    
    dim = numel(p1);
    n = numel(t);
    
    if iscolumn(t)
        t = t';
    end
    
    if isrow(p1)
        p1 = p1';
        p2 = p2';
        p3 = p3';
        p4 = p4';
    end
    
    %%
    % de casteljau
    p10 = repmat(p1,1,n) + (p2 - p1) * t;
	p11 = repmat(p2,1,n) + (p3 - p2) * t;
	p12 = repmat(p3,1,n) + (p4 - p3) * t;
	p20 = p10 + (p11 - p10) .* repmat(t,dim,1);  
	p21 = p11 + (p12 - p11) .* repmat(t,dim,1);

	p20p21 = p21 - p20;
	norm = repmat(sqrt(sum(p20p21.^2,1)),dim,1);
    val = p20p21 ./ norm;
    
    % first and second parametric derivative 
    dx = -3.*(-1+t).^2.*p1(1) + 3.*(-1+t).*(-1+3.*t).*p2(1) + 3.*(2-3.*t).*t.*p3(1) + 3.*t.^2.*p4(1);
    dy = -3.*(-1+t).^2.*p1(2) + 3.*(-1+t).*(-1+3.*t).*p2(2) + 3.*(2-3.*t).*t.*p3(2) + 3.*t.^2.*p4(2);
       
end