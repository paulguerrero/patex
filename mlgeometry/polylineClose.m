function [poly,c] = polylineClose(poly)
    
    if not(iscell(poly))
        poly = {poly};
        wascell = false;
    else
        wascell = true;
    end

    c = cellfun(@(x) ...
        size(x,2) > 1 && ...
        norm(x(:,1)-x(:,end)) < eps(norm(max(x,[],2)-min(x,[],2)))*100, ...
        poly);
    
    for i=1:numel(poly)
        if c(i)
            poly{i}(:,end) = [];
        end
    end
    
    if not(wascell)
        poly = poly{1};
    end
end
