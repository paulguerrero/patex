% outer boundary must be ccw and hole boundaries cw
% cut lines must not intersect any other points on the polygon (outer or
% inner) boundaries
% all faces should be in ccw order
function [cx,cy,faces] = polygonCut(x,y,e1,e2,ei1,f1,ei2,f2)
    
    % general idea: build mesh where each face is a component, bounded by
    % either the polygon boundary or a cut line. Build this mesh with
    % half-edges, so each half-edge belongs to exactly one face=component
    if isempty(ei1)
        faces = {1:numel(x)};
        cx = x;
        cy = y;
        return;
    end

%     polyextent = sqrt((max(x)-min(x)).^2 + (max(y)-min(y)).^2);
    
    % add intersection points of cuts with boundary and add half-edges
    % (only one half-edge per edge, in ccw for boundary or cw for hole)
    firstvertmask = f1==0;
    vi1(firstvertmask) = e1(ei1(firstvertmask));

    secondvertmask = f1==1;
    vi1(secondvertmask) = e2(ei1(secondvertmask));
    
    seginds1 = find(not(firstvertmask | secondvertmask));
    c1x = x(e1(ei1(seginds1))) + (x(e2(ei1(seginds1))) - x(e1(ei1(seginds1)))) .* f1(seginds1);
    c1y = y(e1(ei1(seginds1))) + (y(e2(ei1(seginds1))) - y(e1(ei1(seginds1)))) .* f1(seginds1);
    vi1(seginds1) = numel(x)+1:numel(x)+numel(c1x);
    x = [x,c1x];
    y = [y,c1y];
    
    firstvertmask = f2==0;
    vi2(firstvertmask) = e1(ei2(firstvertmask));

    secondvertmask = f2==1;
    vi2(secondvertmask) = e2(ei2(secondvertmask));
    
    seginds2 = find(not(firstvertmask | secondvertmask));
    c2x = x(e1(ei2(seginds2))) + (x(e2(ei2(seginds2))) - x(e1(ei2(seginds2)))) .* f2(seginds2);
    c2y = y(e1(ei2(seginds2))) + (y(e2(ei2(seginds2))) - y(e1(ei2(seginds2)))) .* f2(seginds2);
    vi2(seginds2) = numel(x)+1:numel(x)+numel(c2x);
    x = [x,c2x];
    y = [y,c2y];
    
    newe1 = [];
    newe2 = [];
    deleinds = [];
    for i=1:numel(e1)
        sinds1 = seginds1(ei1(seginds1) == i);
        sinds2 = seginds2(ei2(seginds2) == i);
        sinds = [sinds1,sinds2];
        if not(isempty(sinds));
            f = [f1(sinds1),f2(sinds2)];
            vi = [vi1(sinds1),vi2(sinds2)];
%             ei = [vi1(sinds1),ei2(sinds2)];
            [~,perm] = sort(f,'ascend');
%             sinds = sinds(perm);
            vi = vi(perm);
%             ei = ei(perm);
            newe1 = [newe1,e1(i),vi]; %#ok<AGROW>
            newe2 = [newe2,vi,e2(i)]; %#ok<AGROW>
            deleinds(end+1) = i; %#ok<AGROW>
        end
        
    end
    
%     e1 = [e1,e1(ei1(seginds1)),vi1(seginds1),e1(ei2(seginds2)),vi2(seginds2)];
%     e2 = [e2,vi1(seginds1),e2(ei1(seginds1)),vi2(seginds2),e2(ei2(seginds2))];
%     e1(ei1(seginds1)) = [];
%     e2(ei1(seginds1)) = [];
%     e1(ei2(seginds2)) = [];
%     e2(ei2(seginds2)) = [];
    e1 = [e1,newe1];
    e2 = [e2,newe2];
    e1(deleinds) = [];
    e2(deleinds) = [];
   
    clear ei1 f1 ei2 f2; % these have not been updated and are not correct anymore
    
    % compute intersections of cuts with other cuts
    intersects = linesegLinesegIntersection2D_mex(...
        [x(vi1);y(vi1)],[x(vi2);y(vi2)],...
        [x(vi1);y(vi1)],[x(vi2);y(vi2)]);
    
    error('todo: get the required info from new intersects format.');
    
    intersects.intAdjacencyMatrix(logical(eye(size(intersects.intAdjacencyMatrix)))) = 0;
    % remove non-intersections
    intersects.intNormalizedDistance1To2(not(intersects.intAdjacencyMatrix)) = inf;
    % remove self-intersections
    intersects.intNormalizedDistance1To2(logical(eye(size(intersects.intAdjacencyMatrix)))) = inf;
    % remove intersections exactly at the vertices
    mask = intersects.intNormalizedDistance1To2 < 0.000001 | intersects.intNormalizedDistance1To2 > 0.999999;
    intersects.intNormalizedDistance1To2(mask) = inf;
    
    ivi = zeros(size(intersects.intNormalizedDistance1To2));
    % add half-edges between intersection points on each cut line
    % (two half-edges per edge)
    for i=1:numel(vi1)
        % sort intersections by distance
        [f,perm] = sort(intersects.intNormalizedDistance1To2(i,:),'ascend');
        perm(isinf(f)) = [];
        if not(isempty(perm))
            mask = perm>=i;
            ix = intersects.intMatrixX(i,perm(mask));
            iy = intersects.intMatrixY(i,perm(mask));
            ivi(i,perm(mask)) = numel(x)+1:numel(x)+numel(ix);
            x = [x,ix]; %#ok<AGROW>
            y = [y,iy]; %#ok<AGROW>
            vinds = zeros(1,numel(perm));
            vinds(mask) = ivi(i,perm(mask));
            vinds(not(mask)) = ivi(perm(not(mask)),i);
    %         mask = ivi(i,perm) == 0;
    %         ivi(i,perm(mask)) = numel(x)+1:numel(x)+sum(mask);
    %         ivi(i,perm) = numel(x)+1:numel(x)+numel(ix);
    %         mask = ivi(perm,i) == 0;
    %         ivi(perm(mask),i) = ivi(i,perm(mask));
    %         x = [x,ix(mask)]; %#ok<AGROW>
    %         y = [y,iy(mask)]; %#ok<AGROW>
        else
            vinds = [];
        end
        e1 = [e1,vi1(i),vinds,vi2(i),vinds(end:-1:1)]; %#ok<AGROW>
        e2 = [e2,vinds,vi2(i),vinds(end:-1:1),vi1(i)]; %#ok<AGROW>
    end
    
    % build connectivity data structure
    vedges = cell(1,numel(x));
    
%     vedgeleaving = cell(1,numel(x));
%     vneighbours = cell(1,numel(x));
    for i=1:numel(e1)
        vedges{e1(i)}(end+1) = i;
%         vedgeleaving{e1(i)}(end+1) = true;
%         vneighbours{e1(i)}(end+1) = e1(i);
        
%         vedges{e2(i)}(end+1) = i;
%         vedgeleaving{e2(i)}(end+1) = false;
%         vneighbours{e2(i)}(end+1) = e2(i);
    end
    
    twinedge = zeros(1,numel(e1));
    for i=1:numel(x)
        for j=1:numel(vedges{i})
            ind = find(e2(vedges{e2(vedges{i}(j))}) == i);
            if numel(ind) > 1
                error('more than one twin edge found');
            elseif numel(ind) == 1
                twinedge(vedges{i}(j)) = vedges{e2(vedges{i}(j))}(ind);
            else
                twinedge(vedges{i}(j)) = 0;
            end
        end
    end
    
    % extract faces
    faces = {};
    cx = x;
    cy = y;
    outstandingedgemask = true(1,numel(e1));
    while any(outstandingedgemask)

        faces{end+1} = []; %#ok<AGROW>
        eind = find(outstandingedgemask,1,'first');
 
        while not(isempty(eind))
            if not(outstandingedgemask(eind))
                error('arrived at same edge twice'); % debug
            end
            outstandingedgemask(eind) = false;
            faces{end}(end+1) = e1(eind);
        
            leavingedges = vedges{e2(eind)};
            leavingedges(leavingedges == twinedge(eind)) = [];
            
            edgedir = [x(e2(eind)) - x(e1(eind));...
                       y(e2(eind)) - y(e1(eind))];
            edgedir = edgedir ./ sqrt(sum(edgedir.^2,1));
            orthedgedir = [-edgedir(2);edgedir(1)];
            leavingedgedirs = [x(e2(leavingedges)) - x(e1(leavingedges));...
                               y(e2(leavingedges)) - y(e1(leavingedges))];
            leavingedgedirs = leavingedgedirs ./ repmat(sqrt(sum(leavingedgedirs.^2,1)),2,1);
            
            mask = orthedgedir(1,:).*leavingedgedirs(1,:) + ...
                   orthedgedir(2,:).*leavingedgedirs(2,:) < 0;
            cosangle = edgedir(1,:).*leavingedgedirs(1,:) + edgedir(2,:).*leavingedgedirs(2,:);
            cosangle(mask) = (1-cosangle(mask))+1;
        
            % cosangle should now be function where -1 = leftmost angle and
            % 3 = rightmost angle
        
            [~,minangleind] = min(cosangle);
            eind = leavingedges(minangleind);
            if e1(eind) == faces{end}(1)
                eind = [];
            end
        end
%         
%         cx{end+1} = x(faces{end}); %#ok<AGROW>
%         cy{end+1} = y(faces{end}); %#ok<AGROW>
    end
    
end
