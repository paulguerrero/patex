% [poly2,param2,origvertinds] = polylineResample(poly, spacing)
% [poly2,param2,origvertinds] = polylineResample(poly, spacing, 'spacing')
% [poly2,param2,origvertinds] = polylineResample(poly, amount, 'amount')
% [poly2,param2,origvertinds] = polylineResample(..., closed)
% [poly2,param2,origvertinds] = polylineResample(..., closed, exact)
% [poly2,param2,origvertinds] = polylineResample(..., closed, exact, param)
function [poly2,param2,origvertinds,edgeinds] = polylineResample(poly, in2, mode, closed, exact, param)

    if nargin < 3
        mode = 'spacing';
    end
    
    if nargin < 4
        closed = false;
    end
    
    if nargin < 5
        exact = false;
    end
    
    if nargin < 6
        param = polylineArclen(poly,closed);
    end
	
    if nargout > 1
        param2 = zeros(size(param,1),0);
    end
    if nargout > 2
        origvertinds = [];
    end
    
    if strcmp(mode,'amount')
        if in2 < 2
            error('Number of samples needs to be >= 2.')
        end
        sampledist = param(1,end)/(in2-1);
    elseif strcmp(mode,'spacing')
        sampledist = in2;
    else
        error('Mode needs to be either ''amount'', or ''spacing''.')
    end
    
    if exact
        % distribute points with arclength spacing as close to n as
        % possible, but keep old vertices, so the polyline does not change
        % shape at all
        poly2 = zeros(2,0);
        edgeinds = zeros(1,0);
        dparam = diff(param,1,2);
        for i=1:size(dparam,2)
            vi1 = i;
            vi2 = mod(i+1-1,size(poly,2))+1;
            npseg = round(dparam(1,i)/sampledist)+1;
            if nargout > 2
                origvertinds(end+1) = size(poly2,2)+1; %#ok<AGROW>
            end
            if npseg >=3
                f = linspace(0,1,npseg);
                f = f(1:end-1);
                if nargout > 1
                    param2 = [param2,...
                              param(:,vi1) + dparam(:,i).*f]; %#ok<AGROW>
                end
                poly2 = [poly2,...
                    [poly(1,vi1).*(1-f) + poly(1,vi2).*f;...
                     poly(2,vi1).*(1-f) + poly(2,vi2).*f]]; %#ok<AGROW>
                if nargout >= 4
                    edgeinds = [edgeinds,ones(1,numel(f)).*vi1]; %#ok<AGROW>
                end
            else
                poly2 = [poly2,poly(:,vi1)]; %#ok<AGROW>
                if nargout >= 4
                    edgeinds = [edgeinds,vi1]; %#ok<AGROW>
                end
                if nargout > 1
                    param2 = [param2,param(:,vi1)]; %#ok<AGROW>
                end
            end

        end
        if not(closed)
            if nargout > 1
                param2(:,end+1) = param(:,end);
            end
            if nargout > 2
                origvertinds(end+1) = size(poly2,2)+1;
            end
            poly2(:,end+1) = poly(:,end); % add missing endpoint
            if nargout >= 4
                edgeinds(end+1) = edgeinds(end);
            end
        end
        
    else
        % distribute N points equally spaced
        % (min. 3 points so polygons do not become degenerate)
        np = max(3,round(param(1,end)/sampledist)+1);
        pos = linspace(param(1,1), param(1,end), np);
        
        if closed
            poly(:,end+1) = poly(:,1);
        end
        
        % remove duplicate parameter values
        [~,ind] = unique(param(1,:),'sorted');
        param = param(1,ind);
        poly = poly(:,ind);
        if numel(param) == 1
            % must be a point, treat coordinates as constant over all parameter values
            param = [param-1,param+1];
            poly = [poly,poly];
        end
        
        poly2 = interp1(param',poly',pos','linear','extrap')';

%         if closed
%             poly2 = interp1(param',poly(:,[1:end,1])',pos','linear','extrap')';
%         else
%             poly2 = interp1(param',poly',pos','linear','extrap')';
%         end
        
        if nargout > 1
%            param2 = pos;
			param2 = interp1(param',param',pos','linear','extrap');
        end
        if nargout > 2
            origvertinds = [];
        end
        if nargout >= 4
            edgeinds = max(1,min(numel(param)-1,floor(interp1(param',(1:numel(param))',pos','linear','extrap')')));
        end
    end

end
