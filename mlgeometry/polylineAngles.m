% how much angles at vertices deviate from 180 degrees
% positive if to the left when polyline direction goes from bottom to top
% if polyline is ccw polygon, this means positive for convex angles
function angles = polylineAngles(poly,closed)
    
    if closed
        segs = diff(poly(:,[1:end,1]),1,2);
    else
        segs = diff(poly,1,2);
    end
    
    seglen = sqrt(sum(segs.^2,1));
    segs = segs ./ seglen([1,1],:);
    
    if closed
        angles = acos(max(-1,min(1,dot(segs(:,[end,1:end-1]),segs))));
    else
        angles = acos(max(-1,min(1,dot(segs(:,1:end-1),segs(:,2:end)))));
    end
    
    orthsegs = [segs(2,:);-segs(1,:)];
    
    if closed
        s = dot(segs(:,[end,1:end-1]),orthsegs);
    else
        s = dot(segs(:,1:end-1),orthsegs(:,2:end));    
    end
    
    mask = s<0;
    angles(mask) = -angles(mask);
end
