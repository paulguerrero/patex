function [centerx,centery] = rectangleArray(rectsize,arraycenter,arraywidth,arrayheight,mode,varargin)

    if strcmp(mode,'count')
        if numel(varargin) ~= 2
            error('Must pass count in x and y direction as arguments');
        end
        arraycountx = varargin{1};
        arraycounty = varargin{2};
        
        nxmax = floor(arraywidth / rectsize(1));
        nymax = floor(arrayheight / rectsize(2));
        
        nx = min(arraycountx,nxmax);
        ny = min(arraycounty,nymax);

    elseif strcmp(mode,'spacing')
        if numel(varargin) ~= 2
            error('Must pass spacing in x and y direction as arguments');
        end
        arrayspacingx = varargin{1};
        arrayspacingy = varargin{2};
        
        nx = floor((arraywidth+arrayspacingx) / (rectsize(1)+arrayspacingx));
        ny = floor((arrayheight+arrayspacingy) / (rectsize(2)+arrayspacingy));
    else
        error('Unknown array mode.');
    end
    
    % determine array x- and y-positions
    if nx == 1
        centerx = arraycenter(1);
    else
        centerx = linspace(...
            arraycenter(1)-arraywidth/2 + rectsize(1)/2,...
            arraycenter(1)+arraywidth/2 - rectsize(1)/2, nx);
    end

    if ny == 1
        centery = arraycenter(2);
    else
        centery = linspace(...
            arraycenter(2)-arrayheight/2 + rectsize(2)/2,...
            arraycenter(2)+arrayheight/2 - rectsize(2)/2, ny);
    end

    [centery,centerx] = ndgrid(centery,centerx);
end
