function [curv, arclen, normal, curvint] = polylineDiffProperties(points, closed, errbound)

% max bound on error of curve segments to actual curve => constrains
% curvature estimation
% compute osculating circle max distance from actual segments, shoould not
% be larger than given error bounds (using only local information of three
% points, the curve segment interpolating the three points with smallest
% possible maximum curvature is the osculating circle)
% => use curvature of osculating circle of max. radius instead

% do noise pre-smoothing (takes too long right now)    
%     if circular
%         nextp = [points(:,2:end),points(:,1)];
%         lastp = [points(:,end),points(:,1:end-1)];
%     else
%         nextp = [points(:,2:end),points(:,end).*2-points(:,end-1)];
%         lastp = [points(:,1).*2-points(:,2),points(:,1:end-1)];
%     end
%     
%     tolastp = lastp-points;
%     tonextp = nextp-points;
% 
%     % compute the arclength of each segment
%     arclen = sqrt(tonextp(1,:).^2 + tonextp(2,:).^2);
%     arclen = [0,cumsum(arclen)];
%     if not(circular)
%         arclen = arclen(1:end-1);
%     end
%     
% 
%     if circular
%         % convert arclength to 
%         rad = min(noisesmoothrad, arclen(end)); % 100
%         xvals = [arclen(1:end-1)-arclen(end),arclen(1:end-1),arclen(1:end-1)+arclen(end)];
%         sx = smooth(xvals,[points(1,:),points(1,:),points(1,:)],rad,'lowess');
%         sy = smooth(xvals,[points(2,:),points(2,:),points(2,:)],rad,'lowess');
%         realinds = size(points,2)+1:size(points,2)*2;
%         points = [sx(realinds)';sy(realinds)'];
%     else
%         sx = smooth(arclen,points(1,:),noisesmoothrad,'lowess');
%         sy = smooth(arclen,points(2,:),noisesmoothrad,'lowess');
%         points = [sx';sy'];
%     end
    
    if nargin < 3
        errbound = 0;
    end


    if closed
        nextp = [points(:,2:end),points(:,1)];
        lastp = [points(:,end),points(:,1:end-1)];
    else
        nextp = [points(:,2:end),points(:,end).*2-points(:,end-1)];
        lastp = [points(:,1).*2-points(:,2),points(:,1:end-1)];
    end
    
    tolastp = lastp-points;
    tonextp = nextp-points;

    % compute the arclength of each segment
    arclen = sqrt(tonextp(1,:).^2 + tonextp(2,:).^2);
    arclen = [0,cumsum(arclen)];
    if not(closed)
        arclen = arclen(1:end-1);
    end
    
%     % compute circumcenter of all consecutive triples of points
%     A = lastp;
%     B = points;
%     C = nextp;
%     D = 2.*(A(1,:).*(B(2,:)-C(2,:)) + ...
%             B(1,:).*(C(2,:)-A(2,:)) + ...
%             C(1,:).*(A(2,:)-B(2,:)));
%     cx = (sum(A.^2,1).*(B(2,:)-C(2,:)) + ...
%           sum(B.^2,1).*(C(2,:)-A(2,:)) + ...
%           sum(C.^2,1).*(A(2,:)-B(2,:)))./D;
%     cy = (sum(A.^2,1).*(C(1,:)-B(1,:)) + ...
%           sum(B.^2,1).*(A(1,:)-C(1,:)) + ...
%           sum(C.^2,1).*(B(1,:)-A(1,:)))./D;
%       
%     % compute radius 
%     r = sqrt(sum((points - [cx;cy]).^2,1));
% %     a = sqrt(sum((B-A).^2,1));
% %     b = sqrt(sum((C-B).^2,1));
% %     c = sqrt(sum((A-C).^2,1));
% %     r2 = (a.*b.*c)./sqrt((a+b+c).*(-a+b+c).*(a-b+c).*(a+b-c));
%       
%     tolastd = zeros(1,size(points,2));
%     tonextd = zeros(1,size(points,2));
% 	for i=1:size(points,2)
%         [tolastd(i),~] = pointPolylineDistance(cx(i),cy(i),[lastp(1,i),points(1,i)],[lastp(2,i),points(2,i)]);
%         [tonextd(i),~] = pointPolylineDistance(cx(i),cy(i),[points(1,i),nextp(1,i)],[points(2,i),nextp(2,i)]);
% 	end
%     
%     mask1 = r > tolastd+errbound;
%     mask2 = r > tonextd+errbound;
    
   
    % compute curvature and normal
    tolastp_mag = sqrt(tolastp(1,:).^2 + tolastp(2,:).^2);
    tonextp_mag = sqrt(tonextp(1,:).^2 + tonextp(2,:).^2);
    tolastp = tolastp ./ repmat(tolastp_mag,2,1);
    tonextp = tonextp ./ repmat(tonextp_mag,2,1);
    curv = (tolastp+tonextp) .* repmat((2./(tolastp_mag+tonextp_mag)),2,1);
    normal = curv;
    curv = sqrt(curv(1,:).^2 + curv(2,:).^2);

    orth = [tolastp(2,:);-tolastp(1,:)];
    mask = curv == 0;
    normal(:,mask) = orth(:,mask);
    mask = not(mask);
    normal(:,mask) = normal(:,mask) ./ repmat(curv(mask),2,1);
    
    % sign of the curvature and normal: dot of tonextp with vector orthonormal to tolastp
    s = sign(dot(tonextp,orth,1));
    s(s==0) = 1;
    curv = curv .* s;
    normal = normal .* -repmat(s,2,1);

    % curvature integral = sum(curvature * d(arclen))
    if nargout >= 4
        if closed
            diffarclen = diff(arclen);
            diffarclen = (diffarclen + diffarclen([end,1:end-1])) ./ 2;
        else
            diffarclen = diff(arclen);
            diffarclen = [diffarclen(1),(diffarclen(1:end-1)+diffarclen(2:end))./2,diffarclen(end)];
        end
        curvint = [0,cumsum(abs(curv) .* diffarclen)];
%         curvintadjusted = [0,cumsum(max(abs(curv),mincurv) .* diffarclen)];
        totalcurvint = curvint(end);
%         totalcurvintadjusted = curvintadjusted(end);
        curvint = (curvint(1:end-1)+curvint(2:end)).*0.5;
%         curvintadjusted = (curvintadjusted(1:end-1)+curvintadjusted(2:end)).*0.5;
        if closed
            curvint = [curvint-curvint(1),totalcurvint];
%             curvintadjusted = [curvintadjusted-curvintadjusted(1),totalcurvintadjusted];
        end
%         if not(circular)
%             curvint = curvint(1:end-1);
%         end
    end
end
