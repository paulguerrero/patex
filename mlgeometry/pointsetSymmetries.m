% p: point set
% c: centroid (symmetries about this point will be checked)
% a: angle (multiples of this angle an mirror symmetries using this angle will be checked)
% w: weights of the points (e.g. the arclength)
% symmetries checked:
% - 2 mirror (orthogonal to angle and along angle)
% - 3 rotational (90,180,270 deg.)
function s = pointsetSymmetries(p,c,a,w)
    
%     [px,py] = polysplit(p(1,:),p(2,:));
%     p = cellfun(@(x,y) [x;y],px,py,'UniformOutput',false);

    % format input
    if not(iscell(p))
        if any(isnan(p))
            [px,py] = polysplit(p(1,:),p(2,:));
            p = cellfun(@(x,y) [x;y],px,py,'UniformOutput',false);
            p = p';
        else
            p = {p};    
        end
    end
    
    if nargin >= 4
        if not(iscell(w))
            if any(isnan(w))
                [w,~] = polysplit(w,w);
                w = w';
            else
                w = {w};
            end
        end
    else
        w = cell(size(p));
        for i=1:numel(p)
            w{i} = ones(1,size(p{i},2));
        end
    end
    
    p = cell2mat(p);
    w = cell2mat(w);
    
%     bbdiags = zeros(1,numel(p));
%     for i=1:numel(p)
%         bbdiags(i) = norm([max(p{i}(1,:));max(p{i}(2,:))] - ...
%                           [min(p{i}(1,:));min(p{i}(2,:))]);
%     end
    [~,~,bbdiag] = pointsetBoundingbox(p);
    thresh = (bbdiag * 0.02)^2;
%     thresh = bbdiag * 0.05;
    
    % use squared distance as measure
%     thresh = 1;
    s = false(5,1);
    
    % check mirror symmetries
    rot = [cos(-a),-sin(-a);...
           sin(-a),cos(-a)];
	mirx = [-1,0;
             0,1];
	miry = [1,0;
            0,-1];
    
    p = p - c(:,ones(1,size(p,2)));
    sumw = sum(w);
        
    tp = (rot' * miry * rot) * p;
    [d,~] = pointPolylineDistance_mex(tp(1,:),tp(2,:),p(1,:),p(2,:));
    if sum(d.^2.*w)/sumw < thresh
        s(1) = true;
    end
    
    tp = (rot' * mirx * rot) * p;
    [d,~] = pointPolylineDistance_mex(tp(1,:),tp(2,:),p(1,:),p(2,:));
%     if sum(d.*w)/sumw < thresh
	if sum(d.^2.*w)/sumw < thresh
        s(2) = true;
	end
    
    rot = [cos(pi/2),-sin(pi/2);...
           sin(pi/2),cos(pi/2)];
    tp = rot * p;
    [d,~] = pointPolylineDistance_mex(tp(1,:),tp(2,:),p(1,:),p(2,:));
    if sum(d.^2.*w)/sumw < thresh
        s(3) = true;
    end
    
    rot = [cos(pi),-sin(pi);...
           sin(pi),cos(pi)];
    tp = rot * p;
    [d,~] = pointPolylineDistance_mex(tp(1,:),tp(2,:),p(1,:),p(2,:));
    if sum(d.^2.*w)/sumw < thresh
        s(4) = true;
    end
    
    rot = [cos(pi*(3/2)),-sin(pi*(3/2));...
           sin(pi*(3/2)),cos(pi*(3/2))];
    tp = rot * p;
    [d,~] = pointPolylineDistance_mex(tp(1,:),tp(2,:),p(1,:),p(2,:));
    if sum(d.^2.*w)/sumw < thresh
        s(5) = true;
    end
end
