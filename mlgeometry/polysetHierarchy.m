% parent-child relationship between polygons
% polys are given as cell array of polygon vertices (each 2xn)
% the given polygons must not intersect
% a(i,j) == 1 if i is child of j (i is inside j and not inside any of the childs of j)
function a = polysetHierarchy(polys)

    a = sparse(numel(polys),numel(polys));
    
    areas = zeros(1,numel(polys));
    for i=1:numel(polys)
        areas(i) = polyarea(polys{i}(1,:),polys{i}(2,:));
    end
    
    % order by polygon area
    [~,perm] = sort(areas,'descend');
    polys = polys(perm);
    
    for i=2:numel(polys)
        parentinds = findParentpolys(polys(1:i-1),a(1:i-1,1:i-1),polys{i});
        a(i,parentinds) = 1; %#ok<SPRIX>
    end

    % back to original order
    a(perm,:) = a;
    a(:,perm) = a;
end

function parentinds = findParentpolys(polys,a,querypoly)
    rootpolyinds = find(not(any(a,2)));
    activeinds = rootpolyinds;
    
    parentinds = zeros(1,0);
    while not(isempty(activeinds))
        
        newactiveinds = zeros(1,0);
        for i=1:numel(activeinds)
            
            rel = polygonContainmentRelation(...
                querypoly(1,:),querypoly(2,:),...
                polys{activeinds(i)}(1,:),polys{activeinds(i)}(2,:));
            
            
            if rel == 1
                achildinds = find(a(:,activeinds(i)))';
                aparentinds = find(a(activeinds(i),:));
                
                parentinds = [parentinds,activeinds(i)]; %#ok<AGROW>
                parentinds(ismember(parentinds,aparentinds)) = [];
                
                newactiveinds = [newactiveinds,achildinds]; %#ok<AGROW>
            end
        end
        activeinds = newactiveinds;
        
    end
end

