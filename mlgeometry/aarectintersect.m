% pairwise intersections of two sets of axis-aligned 2d rectangles
% [iminx,iminy,imaxx,imaxy,itype] = aarectintersect(r1min,r1max,r2min,r2max)
% [iminx,iminy,imaxx,imaxy,itype] = aarectintersect(r1min,r1max,r2min,r2max,ind1,ind2)
% corresponding entry in itype is 2 if rectangles are intersecting, 1 if
% rectangles touching and 0 otherwise
function [iminx,iminy,imaxx,imaxy,itype] = aarectintersect(r1min,r1max,r2min,r2max,ind1,ind2)

    if size(r1min,1) ~= 2 || size(r1max,1) ~= 2 || ...
       size(r2min,1) ~= 2 || size(r2max,1) ~= 2
        error('Input rectangle min/max corners must be given as 2XN arrays');
    end

    if nargin == 5 || ...
       nargin >= 6 && (ndims(ind1) ~= ndims(ind2) || any(size(ind1) ~= size(ind2)))
        error('Given indices must have same shape and size.');
    end
    
    if nargin < 6 || isempty(ind1)
        % all pairwise intersections
        iminx = bsxfun(@max,r1min(1,:)',r2min(1,:));
        iminy = bsxfun(@max,r1min(2,:)',r2min(2,:));
        imaxx = bsxfun(@min,r1max(1,:)',r2max(1,:));
        imaxy = bsxfun(@min,r1max(2,:)',r2max(2,:));
    else
        % intersections the given pairs
        iminx = max(r1min(1,ind1(:)),r2min(1,ind2(:)));
        iminy = max(r1min(2,ind1(:)),r2min(2,ind2(:)));
        imaxx = min(r1max(1,ind1(:)),r2max(1,ind2(:)));
        imaxy = min(r1max(2,ind1(:)),r2max(2,ind2(:)));
        
        iminx = reshape(iminx,size(ind1));
        iminy = reshape(iminy,size(ind1));
        imaxx = reshape(imaxx,size(ind1));
        imaxy = reshape(imaxy,size(ind1));
    end
    
    if nargout >= 5
        itype = zeros(size(iminx));
        imask = imaxx >= iminx & imaxy >= iminy;
        itype(imask) = 2; % intersecting
        itype(imask & (imaxx == iminx | imaxy == iminy)) = 1; % touching
    end
end
