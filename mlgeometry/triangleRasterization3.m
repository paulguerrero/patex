function I = triangleRasterization3(I,x,y,z,v,zcircular)
    
    if nargin < 6 || isempty(zcircular)
        zcircular = false;
    end

    if zcircular
        warning('triangleRasterization3:provisory',...
            ['triangleRasterization in circular domain not implemented, ',...
             'rasterizing triangle as if in non-circular domain']);
    end

    % find the triangle normal
    normal = cross([x(2);y(2);z(2)] - [x(1);y(1);z(1)],...
                   [x(3);y(3);z(3)] - [x(1);y(1);z(1)]);
	normal = normal ./ sqrt(sum(normal.^2,1));
    
    Iextent = [size(I,2);size(I,1);size(I,3)];
    
    % shuffle dimensions so that the smallest gradient dimension is z
    % (= dimension of maximum normal component)
    [~,mindim] = max(normal);
    dims = [1;2;3];
    dims(mindim) = [];
    
    verts = [x;y;z];
    x = verts(dims(1),:);
    y = verts(dims(2),:);
    z = verts(mindim,:);
    Iextent = Iextent([dims;mindim]);
    
    
    [bbmin,bbmax] = pointsetBoundingbox([x;y]);
    
    
    bbmin = max(1,floor(bbmin));
    bbmax = [min(Iextent(1),ceil(bbmax(1)));...
             min(Iextent(2),ceil(bbmax(2)))];
         
    [px,py] = meshgrid(bbmin(1):bbmax(1),bbmin(2):bbmax(2));
    px = px(:)';
    py = py(:)';
    bc = [((y(2)-y(3)).*(px-x(3)) + (x(3)-x(2)).*(py-y(3))) ./ ...
          ((y(2)-y(3))*(x(1)-x(3)) + (x(3)-x(2))*(y(1)-y(3))); ...
          ((y(3)-y(1)).*(px-x(3)) + (x(1)-x(3)).*(py-y(3))) ./ ...
          ((y(2)-y(3))*(x(1)-x(3)) + (x(3)-x(2))*(y(1)-y(3))); ...
          ones(1,numel(px))];
	bc(3,:) = 1-(bc(1,:)+bc(2,:));
    
    mask = bc(1,:) >= 0 & bc(1,:) <= 1 & ...
           bc(2,:) >= 0 & bc(2,:) <= 1 & ...
           bc(3,:) >= 0 & bc(3,:) <= 1;
    
       
    px = repmat(px(mask),size(v,1),1);
    py = repmat(py(mask),size(v,1),1);
    pz = max(1,min(Iextent(3),round(...
        z(1)*bc(1,mask) + ...
        z(2)*bc(2,mask) + ...
        z(3)*bc(3,mask))));
    pz = repmat(pz,size(v,1),1);
    pc = repmat((1:size(v,1))',1,size(px,2));
    vals = v(:,1)*bc(1,mask) + ...
           v(:,2)*bc(2,mask) + ...
           v(:,3)*bc(3,mask);

% 	I(sub2ind(size(I),py(mask),px(mask))) = v(:,1)*bc(1,mask) + ...
%                                             v(:,2)*bc(2,mask) + ...
%                                             v(:,3)*bc(3,mask);

    % shuffle dimensions back
    pcoords = zeros(3,numel(px));
    pcoords(dims(1),:) = px(:)';
    pcoords(dims(2),:) = py(:)';
    pcoords(mindim,:) = pz(:)';
                                        
% 	I(sub2ind(size(I),py(:),px(:),pc(:))) = vals(:);
    I(sub2ind(size(I),pcoords(2,:),pcoords(1,:),pcoords(3,:),pc(:)')) = vals(:);
end
