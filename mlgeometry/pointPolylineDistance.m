function [d,si,f,px,py] = pointPolylineDistance(x,y,x1,y1,x2,y2)

    if nargin ~= 4 && nargin ~= 6
        error('Must provide exactly 4 or 6 inputs.'),
    end
    
    if nargout ~= 2 && nargout ~= 5
        error('Must provide exactly 2 or 5 outputs.'),
    end
    
    if nargin == 4
        x2 = x1(2:end);
        y2 = y1(2:end);
        x1(end) = [];
        y1(end) = [];
    end
    
    d = zeros(1,numel(x));
    si = zeros(1,numel(x));
    
    if nargout > 2
        f = zeros(1,numel(x));
        px = zeros(1,numel(x));
        py = zeros(1,numel(x));
    end
    
    for i=1:numel(x)
        % test if point (x,y) projected to the line is outside the segment x1,x2
        o1m = (x2 == x1 & y2 == y1) | ...                    % degenerate linesegment (zero length)
              ((x2-x1).*(x(i)-x1) + (y2-y1).*(y(i)-y1) < 0); %(p1 to p) projected to the segment (p1 to p2)  
        o2m = (x2-x1).*(x(i)-x2) + (y2-y1).*(y(i)-y2) > 0;   %(p2 to p) projected to the segment (p1 to p2)  
        im = not(o1m | o2m);

        ad = inf(1,numel(x1));
        ad(im) = abs((x2(im)-x1(im)).*(y1(im)-y(i)) - (y2(im)-y1(im)).*(x1(im)-x(i))) ./ ...
                sqrt((x2(im)-x1(im)).^2 + (y2(im)-y1(im)).^2);
        ad(o1m) = sqrt((x(i)-x1(o1m)).^2 + (y(i)-y1(o1m)).^2);
        ad(o2m) = sqrt((x(i)-x2(o2m)).^2 + (y(i)-y2(o2m)).^2);

        [d(i),si(i)] = min(ad);

        if nargout > 2
            if im(si(i))
                f(i) = ((x2(si(i))-x1(si(i)))*(x(i)-x1(si(i))) + (y2(si(i))-y1(si(i)))*(y(i)-y1(si(i)))) / ...
                       ((x2(si(i))-x1(si(i)))^2                + (y2(si(i))-y1(si(i)))^2);
                px(i) = x1(si(i)).*(1-f(i)) + x2(si(i)).*f(i);
                py(i) = y1(si(i)).*(1-f(i)) + y2(si(i)).*f(i);
            elseif o1m(si(i))
                f(i) = 0;
                px(i) = x1(si(i));
                py(i) = y1(si(i));
            else
                f(i) = 1;
                px(i) = x2(si(i));
                py(i) = y2(si(i));    
            end
        end
    end
 
end
