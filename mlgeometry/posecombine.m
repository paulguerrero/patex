function pose = posecombine(pose,pose2)
    if size(pose,1) == 11
        % 3d pose
        pose(1:3) = pose(1:3)+ pose2(1:3);
        pose(4:7) = quatmultiply(pose2(4:7)',pose(4:7)')';
        pose(8:10) = pose(8:10) .* pose2(8:10);
        pose(11) = xor(pose(11),pose2(11));
    elseif size(pose,1) == 6
        % 2d pose
        error('Not yet implemented.');
    else
        error('Unknown pose type.');
    end
end
