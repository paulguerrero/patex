% p: 3 x 1 or 3 x n matrix of 1 or n 3-dimensional points
% or 7 x 1 or 7 x n matrix of 1 or n 7-dimensional poses in 3D space (3 position, 4 angle)
% or 10 x 1 or 10 x n matrix of 1 or n 10-dimensional poses in 3D space (3 position, 4 angle, 3 scale)
% or 11 x 1 or 11 x n matrix of 1 or n 11-dimensional poses in 3D space (3 position, 4 angle, 3 scale, 1 mirror)
% order: inverse translate, inverse rotate, inverse scale, mirror (= inverse mirror)
% scale: 3 x n or 3 x 1 matrix of 3-dimensional scale vectors
% angle: 4 x n or 4 x 1 matrix of quaternions
% trans: 3 x n or 3 x 1 matrix of 3-dimensional translations
% optional: mirror: 1 x n or 1 x 1 matrix of mirroring flags
% (mirroring is about xz plane: y = -y)
function tp = transform3Dinv(p, scale, angle, trans, mirror)
    
    tp = bsxfun(@times,ones(1,size(scale,2)),p);

    tp(1:3,:) = bsxfun(@minus,tp(1:3,:),trans);

    % matrix multiplication written out (so i can multiply n matrices with n vectors)
    invangle = quatconj(angle');
    invrot = permute(permute(quat2dcm(invangle),[2,1,3]),[1,3,2]); % inner permute (=transpose of each matrix) because quat2dcm returns the coordinate frame corresponding to the quaternion, not the rotation matrix
    invscale = 1./scale;
    tp(1:3,:) = [(invscale(1,:).*invrot(1,:,1)).*tp(1,:) + (invscale(1,:).*invrot(1,:,2)).*tp(2,:) + (invscale(1,:).*invrot(1,:,3)).*tp(3,:); ...
                 (invscale(2,:).*invrot(2,:,1)).*tp(1,:) + (invscale(2,:).*invrot(2,:,2)).*tp(2,:) + (invscale(2,:).*invrot(2,:,3)).*tp(3,:); ...
                 (invscale(3,:).*invrot(3,:,1)).*tp(1,:) + (invscale(3,:).*invrot(3,:,2)).*tp(2,:) + (invscale(3,:).*invrot(3,:,3)).*tp(3,:)];
      
    if size(tp,1) >= 7
        tp(4:7,:) = quatmultiply(invangle,tp(4:7,:)')';
%         tp(4:7,:) = quatmultiply(tp(4:7,:)',invangle)';
    end
    
    if size(tp,1) >= 10
        tp(8:10,:) = bsxfun(@times,tp(8:10,:),invscale);
    end

    if nargin >= 5 && any(mirror)
        % mirror about xz plane (quaternion rotation remains the same)
        tp(2,:) = tp(2,:).*(logical(mirror).*-2+1);
        
        if size(tp,1) >= 7
            % rotate 180 degrees about x-axis
            tp(4:7,logical(mirror)) = quatmultiply(angleaxis2quat(pi,[1;0;0])',tp(4:7,logical(mirror))')';
            error('mirroring not yet implemented');
            % todo: 180 deg. rot. about x is probably not correct, e.g. when
            % orientation direction is almost in xz plane (the mirroring
            % plane)'
        end

        if size(tp,1) >= 11
            tp(11,:) = bsxfun(@xor,tp(11,:),mirror);
        end
    end
    
end
