#include "mex.h"

#include<vector>

#include "mexutils.h"
#include "geom.h"


// [itype,ip0,ip1,t0,t1] = linesegLinesegIntersection2D_mex(lp1,lp2)
// all outputs except itype are optional
// itype: intersection type, 0 no intersection, 1 intersection at a point, 2 intersection at a line segment (lines are colinear)
// ip0: intersection point (itype == 1) or startpoint of intersection line segment (itype == 2)
// ip1: endpoint of the intersection line segment (itype == 2)
// t0: normalized distance to intersection along line segment 1
// t1: normalized distance to intersection along line segment 2

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    if(nrhs != 4) {
        mexErrMsgTxt("Four inputs required.");
    }
    if(nlhs < 1 || nlhs > 5) {
        mexErrMsgTxt("One to five outputs required.");
    }
    
    mwSize dim ,n, nl1, nl2;
    
    // read lineseg1
    std::vector<double> l1p1;
    std::vector<double> l1p2;
    readDoubleMatrix(prhs[0], l1p1, dim, nl1);
    if (dim != 2) {
        mexErrMsgTxt("Input must be two-dimensional.");
    }
    
    readDoubleMatrix(prhs[1], l1p2, dim, n);
    if (dim != 2) {
        mexErrMsgTxt("Input must be two-dimensional.");
    }
    if (n != nl1) {
        mexErrMsgTxt("Input size does not match.");
    }
    
    // read lineseg2
    std::vector<double> l2p1;
    std::vector<double> l2p2;
    readDoubleMatrix(prhs[2], l2p1, dim, nl2);
    if (dim != 2) {
        mexErrMsgTxt("Input must be two-dimensional.");
    }
    
    readDoubleMatrix(prhs[3], l2p2, dim, n);
    if (dim != 2) {
        mexErrMsgTxt("Input must be two-dimensional.");
    }
    if (n != nl2) {
        mexErrMsgTxt("Input size does not match.");
    }
        
    // compute intersections
    mwSize nelm = nl1*nl2;
    std::vector<double> itype(nelm,0);
    std::vector<double> i0(nelm*2,0);
    std::vector<double> i1(nelm*2,0);
    if (nl1 >= 1 && nl2 >= 1) {
        mwIndex ind;
        Lineseg lseg1;
        Lineseg lseg2;
        Vector ip0;
        Vector ip1;
        for (mwIndex l2i=0; l2i<nl2; l2i++) {
            for (mwIndex l1i=0; l1i<nl1; l1i++) {
                
                ind = l1i*dim;
                lseg1.P0 = Vector(l1p1[ind],l1p1[ind+1],l1p1[ind+2]);
                lseg1.P1 = Vector(l1p2[ind],l1p2[ind+1],l1p2[ind+2]);

                ind = l2i*dim;
                lseg2.P0 = Vector(l2p1[ind],l2p1[ind+1],l2p1[ind+2]);
                lseg2.P1 = Vector(l2p2[ind],l2p2[ind+1],l2p2[ind+2]);
                
                ind = l2i*nl1+l1i;
                itype[ind] = (double) linesegLinesegIntersection2D(lseg1,lseg2,ip0,ip1);
                
                if (itype[ind] == 1) {
                    i0[ind] = ip0.x;
                    i0[ind+nelm] = ip0.y;
                } else if (itype[ind] == 2) {
                    i0[ind] = ip0.x;
                    i0[ind+nelm] = ip0.y;
                    
                    i1[ind] = ip1.x;
                    i1[ind+nelm] = ip1.y;
                }
            }
        }
    }
    
    // write outputs
    plhs[0] = mxCreateDoubleMatrix(nl1,nl2,mxREAL);
    std::copy(itype.begin(),itype.end(),mxGetPr(plhs[0]));
    itype.clear();
    
    if (nlhs >= 2) {
        const mwSize dims[]={nl1,nl2,2};
        plhs[1] = mxCreateNumericArray(3,dims,mxDOUBLE_CLASS,mxREAL);
        std::copy(i0.begin(),i0.end(),mxGetPr(plhs[1]));
    }
    
    if (nlhs >= 3) {
        const mwSize dims[]={nl1,nl2,2};
        plhs[2] = mxCreateNumericArray(3,dims,mxDOUBLE_CLASS,mxREAL);
        std::copy(i1.begin(),i1.end(),mxGetPr(plhs[2]));
    }

    if (nlhs >= 4) {
        std::vector<double> t0(nelm,0);
        for (mwIndex l1i=0; l1i<nl1; l1i++) {
            mwIndex l1ind = l1i*dim;
            double dx = l1p2[l1ind]   - l1p1[l1ind];
            double dy = l1p2[l1ind+1] - l1p1[l1ind+1];
            double seglen = sqrt(dx*dx+dy*dy);
            
            for (mwIndex l2i=0; l2i<nl2; l2i++) {
                mwIndex ind = l2i*nl1+l1i;
                double dx = i0[ind]      - l1p1[l1ind];
                double dy = i0[ind+nelm] - l1p1[l1ind+1];
                t0[ind] = sqrt(dx*dx+dy*dy) / seglen;
            }
        }
        plhs[3] = mxCreateDoubleMatrix(nl1,nl2,mxREAL);
        std::copy(t0.begin(),t0.end(),mxGetPr(plhs[3]));
    }
    
    if (nlhs >= 5) {
        std::vector<double> t1(nelm,0);
        for (mwIndex l2i=0; l2i<nl2; l2i++) {
            mwIndex l2ind = l2i*dim;
            double dx = l2p2[l2ind]   - l2p1[l2ind];
            double dy = l2p2[l2ind+1] - l2p1[l2ind+1];
            double seglen = sqrt(dx*dx+dy*dy);
            
            for (mwIndex l1i=0; l1i<nl1; l1i++) {    
                mwIndex ind = l2i*nl1+l1i;
                double dx = i0[ind]      - l2p1[l2ind];
                double dy = i0[ind+nelm] - l2p1[l2ind+1];
                t1[ind] = sqrt(dx*dx+dy*dy) / seglen;
            }
        }
        plhs[4] = mxCreateDoubleMatrix(nl1,nl2,mxREAL);
        std::copy(t1.begin(),t1.end(),mxGetPr(plhs[4]));
    }
}

