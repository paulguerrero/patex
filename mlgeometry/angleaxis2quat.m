function quat = angleaxis2quat(angle,axis)
    
    % normalize axis
    axis = bsxfun(@times,axis,1./sqrt(sum(axis.^2,1)));

    halfangle = angle./2;
    sinhalfangle = sin(halfangle);

    quat = zeros(4,numel(angle));
    quat(1,:) = cos(halfangle);
    quat(2:4,:) = bsxfun(@times,axis,sinhalfangle);
end
