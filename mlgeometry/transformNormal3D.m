% p: 3 x n matrix of n 3-dimensional normals
% order: mirror, scale, rotate
% scale: 3 x n or 3 x 1 matrix of 3-dimensional scale vectors
% angle: 4 x n or 4 x 1 matrix of quaternions
% optional: mirror: 1 x n or 1 x 1 matrix of mirroring flags
% (mirroring is about xz plane: y = -y)
function tn = transformNormal3D(n, scale, angle, mirror)

    tn = n;

    if nargin >= 4 && any(mirror)
        % mirror about xz plane
        tn(2,:) = tn(2,:).*(logical(mirror).*-2+1);
    end    

    % matrix multiplication written out (so i can multiply n matrices with n vectors)
    rot = permute(permute(quat2dcm(angle'),[2,1,3]),[1,3,2]);
    invscale = 1./scale;
    tn = [(rot(1,:,1).*invscale(1,:)).*tn(1,:) + (rot(1,:,2).*invscale(2,:)).*tn(2,:) + (rot(1,:,3).*invscale(3,:)).*tn(3,:); ...
          (rot(2,:,1).*invscale(1,:)).*tn(1,:) + (rot(2,:,2).*invscale(2,:)).*tn(2,:) + (rot(2,:,3).*invscale(3,:)).*tn(3,:); ...
          (rot(3,:,1).*invscale(1,:)).*tn(1,:) + (rot(3,:,2).*invscale(2,:)).*tn(2,:) + (rot(3,:,3).*invscale(3,:)).*tn(3,:)];
      
    % re-normalize
    tn = bsxfun(@times,tn,1./sqrt(sum(tn.^2,1)));
end
