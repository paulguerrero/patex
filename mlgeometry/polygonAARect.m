% vertices of axis-aligned rectangle(s). For multiple rectangles,
% the vertices are appended (there are always 4 vertices).
%
% order of vertices:
% 4----------------3 
% |                |
% |                |       y
% |                |       |
% 1----------------2       *--x

function verts = polygonAARectangle(rmin,rmax)
    
    verts = [...
        rmin;...
        [rmin(1:2,:);rmax(3,:)];...
        [rmin(1,:);rmax(2:3,:)];...
        [rmin(1,:);rmax(2,:);rmin(3,:)];...
        [rmax(1,:);rmin(2:3,:)];...
        [rmax(1:2,:);rmin(3,:)];...
        rmax
        [rmax(1,:);rmin(2,:);rmax(3,:)]];
        
    verts = reshape
end
