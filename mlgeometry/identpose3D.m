function p = identpose3D
    p = [...
       0;0;0;...   % position
       1;0;0;0;... % rotation
       1;1;1;...   % scale
       0];      
end
