% face normals will only point to the ouside if polyline is cw
% Multiple boundaries can be given as cell array, in that case the first
% boundary must be the outer boundary and all remaining boundaries holes.
% meshparts: high-level parts of the mesh:
%   - basevertinds: indices of vertices at the base of the mesh
%   - extrvertinds: indices of vertices at extrusion height
%   - vertpointinds: point index in one of the given boundaries for each mesh vertex (-1 for top cap, -2 for bottom cap)
%   - vertboundaryinds: boundary index for each mesh vertex (-1 for top cap, -2 for bottom cap)
%   - faceedgeinds: edge index in one of the given boundaries for each mesh face (-1 for top cap, -2 for bottom cap)
%   - faceboundaryinds: boundary index for each mesh face (-1 for top cap, -2 for bottom cap)
%   - segpointinds: all point indices in one of the given boundaries for each segment (=part of the boundary without sharp corners)
%   - segboundaryinds: boundary index for each segment (=part of the boundary without sharp corners)
% meshprops: will only be computed if settings.computemeshprops == true:
%   - fareas: face areas
%   - fnormals: face normals
%   - texcoords: texture coordinate array
%   - facetexcoordinds: indices into the texture coordinate array for each face
%   - vnormals: vertex normals array
%   - facevnormalinds: indices into the vertex normals array for each face
%   - facematerialind: material group index for each face
%   - facesmoothgroupind: smoothing group index for each face
%   - materialnames: material names (one for each material group)
function [verts,faces,meshparts,meshprops] = polylineExtrude(p,extrvec,closed,varargin)
    
    if not(iscell(p))
        p = {p};
    end
    
    if numel(p) > 1
        a = polysetHierarchy(p);

        if not(all(a(2:end,1))) || any(any(a(:,2:end))) || a(1,1)
            error('Not all inner boundaries are directly inside the outer boundary.');
        end
    end
    
    for i=1:numel(p)
        if size(p{i},2) < 2
            error('Polyline must have at least two vertices.');
        end
    end
    
    if (norm(extrvec)) == 0
        error('The extrude vector has to be non-zero.');
    end
    
    if abs(nargin) < 3 || isempty(closed)
        closed = true;
    end
    
    if numel(p)>1 && not(closed)
        error('When giving hole boundaries, the outer boundary must be closed.');
    end
    
    settings = struct(...
        'topcap','flat',...
        'bottomcap','flat',...
        'downset',0,...
        'topcapheight',0,...
        'bottomcapheight',0,...
        'computemeshprops',false,...
        'innerfaces',false,...
        'cornervertinds',{cell(1,numel(p))},...
        'polylineOrientation',0,...    
        'matbasename','defaultmaterial',...
        'sidematname',{cell(1,0)});
    settings = nvpairs2struct(varargin,settings);
    
    if not(iscell(settings.cornervertinds))
        settings.cornervertinds = {settings.cornervertinds};
    end
    if isempty(settings.cornervertinds)
        settings.cornervertinds = cell(1,numel(p));
    end
    
    % extend coordinates to 3D if necessary (assume they lie in the xy plane)
    for i=1:numel(p)
        if size(p{i},1) == 2
            p{i} = [p{i};zeros(1,size(p{i},2))];
        end
    end
    
    % compute vertices, their point and boundary indices
    verts = [bsxfun(@plus,[p{:}],-extrvec.*settings.downset),bsxfun(@plus,[p{:}],extrvec)];
    meshparts.vertpointinds = [1:size(verts,2)/2,1:size(verts,2)/2];
    meshparts.vertpointinds = cellfun(@(x) 1:size(x,2),p,'UniformOutput',false);
    meshparts.vertpointinds = [meshparts.vertpointinds{:}];
    meshparts.vertpointinds = [meshparts.vertpointinds,meshparts.vertpointinds];
    meshparts.vertboundaryinds = cellfun(@(x,y) ones(1,size(x,2)).*y,p,num2cell(1:numel(p)),'UniformOutput',false);
    meshparts.vertboundaryinds = [meshparts.vertboundaryinds{:}];
    meshparts.vertboundaryinds = [meshparts.vertboundaryinds,meshparts.vertboundaryinds];
    
    meshparts.basevertinds = 1:size(verts,2)/2;
    meshparts.extrvertinds = size(verts,2)/2+1:size(verts,2);
    
    % compute faces, their edge and boundary indices
    faces = zeros(3,0);
    meshparts.faceedgeinds = zeros(1,0);
    meshparts.faceboundaryinds = zeros(1,0);
    vertoffset = 0;
    for i=1:numel(p)
        
        boundarybasevertinds = vertoffset+1 : vertoffset+size(p{i},2);
        boundaryextrvertinds = boundarybasevertinds + size(verts,2)/2;

        % create extrusion faces
        if closed
            boundaryfaces = [...
                boundarybasevertinds;...
                boundaryextrvertinds([2:end,1]);...
                boundarybasevertinds([2:end,1])];
            boundaryfaceedgeinds = 1:size(p{i},2);

            boundaryfaces = [boundaryfaces;[...
                boundarybasevertinds;...
                boundaryextrvertinds;...
                boundaryextrvertinds([2:end,1])]]; %#ok<AGROW>
            boundaryfaceedgeinds = [boundaryfaceedgeinds;1:size(p{i},2)]; %#ok<AGROW>

            boundaryfaces = reshape(boundaryfaces,3,[]);
            boundaryfaceedgeinds = boundaryfaceedgeinds(:)';
        else
            boundaryfaces = [...
                boundarybasevertinds(1:end-1);...
                boundaryextrvertinds(2:end);...
                boundarybasevertinds(2:end)];
            boundaryfaceedgeinds = 1:size(p{i},2)-1;

            boundaryfaces = [boundaryfaces;[...
                boundarybasevertinds(1:end-1);...
                boundaryextrvertinds(1:end-1);...
                boundaryextrvertinds(2:end)]]; %#ok<AGROW>
            boundaryfaceedgeinds = [boundaryfaceedgeinds;1:size(p{i},2)-1]; %#ok<AGROW>

            boundaryfaces = reshape(boundaryfaces,3,[]);
            boundaryfaceedgeinds = boundaryfaceedgeinds(:)';
        end
        
        % reverse face winding for holes
        if i>1
            boundaryfaces = boundaryfaces([1,3,2],:);
        end
        
        faces = [faces,boundaryfaces]; %#ok<AGROW>
        meshparts.faceedgeinds = [meshparts.faceedgeinds,boundaryfaceedgeinds];
        meshparts.faceboundaryinds = [meshparts.faceboundaryinds,ones(1,size(boundaryfaces,2)).*i];
        
        vertoffset = vertoffset + size(p{i},2);
    end
    
    % add top cap
    if closed && not(strcmp(settings.topcap,'none'))
        nfaces = size(faces,2);
        nverts = size(verts,2);
        if dot(extrvec,[0;0;1]) < 0
            capnormal = [0;0;-1];
        else
            capnormal = [0;0;1];
        end
        nvpairs = {};
        if strcmp(settings.topcap,'simpleroof')
            nvpairs = {'height',settings.topcapheight};
        end
        capborderpolyvinds = array2cell_mex(...
            meshparts.extrvertinds,...
            meshparts.vertboundaryinds(meshparts.extrvertinds),...
            numel(p),2);
        [verts,faces] = addcap(verts,faces,capborderpolyvinds,capnormal,settings.topcap,nvpairs{:});
        
        meshparts.faceedgeinds = [meshparts.faceedgeinds,ones(1,size(faces,2)-nfaces).*-1];
        meshparts.faceboundaryinds = [meshparts.faceboundaryinds,ones(1,size(faces,2)-nfaces).*-1]; % faces do not belong to a single boundary
        meshparts.vertpointinds = [meshparts.vertpointinds,ones(1,size(verts,2)-nverts).*-1];
        meshparts.vertboundaryinds = [meshparts.vertboundaryinds,ones(1,size(verts,2)-nverts).*-1]; % vertices do not belong to any boundary
    end
    
    % add bottom cap
    if closed && not(strcmp(settings.bottomcap,'none'))
        nfaces = size(faces,2);
        nverts = size(verts,2);
        if dot(extrvec,[0;0;1]) < 0
            capnormal = [0;0;1];
        else
            capnormal = [0;0;-1];
        end
        nvpairs = {};
        if strcmp(settings.bottomcap,'simpleroof')
            nvpairs = {'height',settings.bottomcapheight};
        end
        capborderpolyvinds = array2cell_mex(...
            meshparts.basevertinds([1,end:-1:2]),...
            meshparts.vertboundaryinds(meshparts.extrvertinds([1,end:-1:2])),...
            numel(p),2); % reverse winding, so polygon points downwards
        [verts,faces] = addcap(verts,faces,capborderpolyvinds,capnormal,settings.bottomcap,nvpairs{:});
        newfaceinds = nfaces+1:size(faces,2);
        faces(:,newfaceinds) = faces([1,3,2],newfaceinds); % flip faces for the bottom cap
        
        meshparts.faceedgeinds = [meshparts.faceedgeinds,ones(1,size(faces,2)-nfaces).*-2];
        meshparts.faceboundaryinds = [meshparts.faceboundaryinds,ones(1,size(faces,2)-nfaces).*-2]; % faces do not belong to a single boundary
        meshparts.vertpointinds = [meshparts.vertpointinds,ones(1,size(verts,2)-nverts).*-2];
        meshparts.vertboundaryinds = [meshparts.vertboundaryinds,ones(1,size(verts,2)-nverts).*-2]; % vertices do not belong to any boundary
    end
    
    % polylines are ccw when viewed from the front (which faces direction
    % [0;0;1]), so if the extrusion goes in in the other direction,
    % the faces have to change vertex ordering to remain ccw
    if xor(dot(extrvec,[0;0;1]) < 0, settings.innerfaces)
        faces = faces([1,3,2],:);
    end
    
    % compute segment point, boundary and corner indices
    meshparts.segpointinds = cell(1,0);
    meshparts.segboundaryinds = zeros(1,0);
    meshparts.segcornerinds = zeros(1,0);
    for i=1:numel(p)
        if isempty(settings.cornervertinds{i})
            meshparts.segpointinds = [meshparts.segpointinds,{[1:size(p{i},2),1]}];
            meshparts.segboundaryinds = [meshparts.segboundaryinds,i];
            meshparts.segcornerinds = [meshparts.segcornerinds,nan];
        else
            [cornervertinds,perm] = sort(settings.cornervertinds{i},'ascend');
            boundarysegcornerinds = 1:numel(settings.cornervertinds{i});
            meshparts.segcornerinds = [meshparts.segcornerinds,boundarysegcornerinds(perm)];
            if closed
                nangles = size(p{i},2);
                meshparts.segpointinds = [meshparts.segpointinds,cellfun(@(x,y)...
                    {iif(x<y,x:y,[x:nangles,1:y])},...
                    num2cell(cornervertinds),num2cell(cornervertinds([2:end,1])))];
                meshparts.segboundaryinds = [meshparts.segboundaryinds,ones(1,numel(cornervertinds)).*i];
            else
                nangles = size(p{i},2)-2;
                meshparts.segpointinds = [meshparts.segpointinds,cellfun(@(x,y)...
                    {iif(x<y,x:y,[x:nangles,1:y])},...
                    num2cell(cornervertinds(1:end-1)),num2cell(cornervertinds(2:end)))];
                meshparts.segboundaryinds = [meshparts.segboundaryinds,ones(1,numel(cornervertinds)-1).*i];
            end
        end
    end
    
    % mesh properties
    if settings.computemeshprops
        meshprops = struct;
        meshprops.fareas = triangleArea(verts(:,faces(1,:)),verts(:,faces(2,:)),verts(:,faces(3,:)));
        meshprops.fnormals = trimeshFaceNormals(verts,faces);

        meshprops.texcoords = zeros(2,0);
        meshprops.facetexcoordinds = zeros(size(faces));
        meshprops.vnormals = zeros(3,0);
        meshprops.facevnormalinds = zeros(size(faces));
        meshprops.facematerialind = zeros(1,size(faces,2));
        meshprops.facesmoothgroupind = zeros(1,size(faces,2));
        meshprops.materialnames = cell(1,0);

        % ----- mesh properties for side faces -----
        
        extrveclen = sqrt(sum(extrvec.^2,1));
        if extrveclen < 0.0000001
            extrvec_norm = [0;0;1];
        else
            extrvec_norm = extrvec ./ extrveclen;
        end
        
        vertalen = nan(1,size(verts,2));
        boundarylen = zeros(1,numel(p));
        for i=1:numel(p)
            alen = polylineArclen(p{i},closed);
            if xor(settings.innerfaces,i>1)
                alen = alen(end)-alen;
            end
            
            boundarylen(i) = alen(end);
            mask = meshparts.vertboundaryinds == i;
            vertalen(mask) = alen(meshparts.vertpointinds(mask));
        end
        
        sidefacemask = meshparts.faceedgeinds > 0;
        sidefaceinds = find(sidefacemask);
        vinds = unique(faces(:,sidefacemask));
        newvinds(vinds) = 1:numel(vinds);

        % texture coordinates: u: arclength, v: distance along extrusion vector
        vertpoints = zeros(size(verts));
        for i=1:numel(p)
            mask = meshparts.vertboundaryinds == i;
            vertpoints(:,mask) = p{i}(:,meshparts.vertpointinds(mask));
        end
        texcoords = [...
            vertalen(vinds);...
            extrvec_norm'*(verts(:,vinds)-vertpoints(:,vinds))];

        vertoffset = size(meshprops.texcoords,2);
        meshprops.texcoords(:,end+1:end+size(texcoords,2)) = texcoords;
        meshprops.facetexcoordinds(:,sidefacemask) = newvinds(faces(:,sidefacemask))+vertoffset;

        if closed
            % different texture coordinates for vertices that are at the end of
            % the basecurve
            pvertcount = cellfun(@(x) size(x,2),p);
            endfaceinds = sidefaceinds(meshparts.faceedgeinds(sidefacemask) == pvertcount(meshparts.faceboundaryinds(sidefacemask)));
            f = faces(:,endfaceinds);
            ftc = meshprops.facetexcoordinds(:,endfaceinds);
            fvinds = find(meshparts.vertpointinds(f) == 1);
            [~,uniqueinds,fvtcinds] = unique(ftc(fvinds));
            tcinds = ftc(fvinds(uniqueinds));
            vinds = f(fvinds(uniqueinds));
            
            vertoffset = size(meshprops.texcoords,2);
            meshprops.texcoords(:,end+1:end+numel(tcinds)) = [...
                boundarylen(meshparts.vertboundaryinds(vinds));...% alen(ones(1,numel(tcinds)).*end);...
                meshprops.texcoords(2,tcinds)];
            
            ftc(fvinds) = fvtcinds+vertoffset;
            meshprops.facetexcoordinds(:,endfaceinds) = ftc;
        end
        
        % vertex normals: face normals averaged by face area inside the same segment
        % material groups: a common 'side' material, or a given material (settings.sidematname) for each side
        % smoothing groups: a new smoothing group for each side
        for i=1:numel(meshparts.segpointinds)
            faceinds = find(ismember(meshparts.faceedgeinds,meshparts.segpointinds{i}(1:end-1)) & meshparts.faceboundaryinds==meshparts.segboundaryinds(i));
            if isempty(faceinds)
                continue;
            end
            
            % vertex normals
            [v,f] = submesh(verts,faces,faceinds);
            vnormals = trimeshVertexNormals(v,f,'faceavg_area',meshprops.fnormals(:,faceinds),meshprops.fareas(faceinds));

            vertoffset = size(meshprops.vnormals,2);
            meshprops.vnormals(:,end+1:end+size(vnormals,2)) = vnormals;
            meshprops.facevnormalinds(:,faceinds) = f+vertoffset;

            % material names & material groups
            if numel(settings.sidematname) >= i
                matname = [settings.sidematname{i},'_side'];
            else
                matname = [settings.matbasename,'_side'];
            end
            matind = find(strcmp(matname,meshprops.materialnames),1,'first');
            if isempty(matind)
                meshprops.materialnames{:,end+1} = matname;
                matind = numel(meshprops.materialnames);
            end
            meshprops.facematerialind(:,faceinds) = matind;

            % smoothing groups
            if isempty(meshprops.facesmoothgroupind)
                sgind = 1;
            else
                sgind = max(meshprops.facesmoothgroupind)+1;
            end
            meshprops.facesmoothgroupind(:,faceinds) = sgind;
        end

        % ----- mesh properties for cap faces -----
        for i=1:2
            if i==1
                capfacemask = meshparts.faceedgeinds == -1;
                captype = settings.topcap;
            else
                capfacemask = meshparts.faceedgeinds == -2;
                captype = settings.bottomcap;
            end

            if any(capfacemask)
                vinds = unique(faces(:,capfacemask));
                newvinds(vinds) = 1:numel(vinds);

                % texcoords: coordinates with origin in oriented bounding
                % box corner and u/v along x/y-axis of bounding box
                obbox = pointsetOrientedBoundingbox(p{1}(1:2,:),settings.polylineOrientation);
                if i==1
                    % basecurve moved in the basecurve plane by the extrude vector
                    origin = obbox(:,1)+extrvec(1:2);
                else
                    % basecurve moved in the basecurve plane by downset vector
                    origin = obbox(:,1)-extrvec(1:2).*settings.downset;
                end
                xaxis = obbox(:,2);
                yaxis = obbox(:,3);
                xaxis = xaxis./sqrt(sum(xaxis.^2,1));
                yaxis = yaxis./sqrt(sum(yaxis.^2,1));

                texcoords = [xaxis';yaxis'] * bsxfun(@minus,verts(1:2,vinds),origin);
                vertoffset = size(meshprops.texcoords,2);
                meshprops.texcoords(:,end+1:end+size(texcoords,2)) = texcoords;
                meshprops.facetexcoordinds(:,capfacemask) = newvinds(faces(:,capfacemask))+vertoffset;

                if strcmp(captype,'flat')
                    capfaceinds = find(capfacemask);

                    % vertex normals: face normals averaged by face area
                    % (face normals should all be the same)
                    [v,f] = submesh(verts,faces,capfaceinds);
                    vnormals = trimeshVertexNormals(v,f,'faceavg_area',meshprops.fnormals(:,capfaceinds),meshprops.fareas(capfaceinds));
                    vertoffset = size(meshprops.vnormals,2);
                    meshprops.vnormals(:,end+1:end+size(vnormals,2)) = vnormals;
                    meshprops.facevnormalinds(:,capfaceinds) = f+vertoffset;

                    % material names & material groups: extended base name
                    if i==1
                        matname = [settings.matbasename,'_top_flat'];
                    else
                        matname = [settings.matbasename,'_bottom_flat'];
                    end
                    matind = find(strcmp(matname,meshprops.materialnames),1,'first');
                    if isempty(matind)
                        meshprops.materialnames{:,end+1} = matname;
                        matind = numel(meshprops.materialnames);
                    end
                    meshprops.facematerialind(:,capfaceinds) = matind;
                    
                    % new smoothing group
                    if isempty(meshprops.facesmoothgroupind)
                        sgind = 1;
                    else
                        sgind = max(meshprops.facesmoothgroupind)+1;
                    end
                    meshprops.facesmoothgroupind(:,capfaceinds) = sgind;
                    
                elseif strcmp(captype,'simpleroof')
                    capfaceinds = find(capfacemask);

                    % separate vertex normals for each face
                    vnormals = [meshprops.fnormals(:,capfaceinds);...
                                meshprops.fnormals(:,capfaceinds);...
                                meshprops.fnormals(:,capfaceinds)];
                    vnormals = reshape(vnormals,3,[]);
                    normalinds = reshape(1:size(vnormals,2),3,[]);
                    vertoffset = size(meshprops.vnormals,2);
                    meshprops.vnormals(:,end+1:end+size(vnormals,2)) = vnormals;
                    meshprops.facevnormalinds(:,capfaceinds) = normalinds+vertoffset;

                    % material names & material groups: extended base name
                    if i==1
                        matname = [settings.matbasename,'_top_angled'];
                    else
                        matname = [settings.matbasename,'_bottom_angled'];
                    end
                    matind = find(strcmp(matname,meshprops.materialnames),1,'first');
                    if isempty(matind)
                        meshprops.materialnames{:,end+1} = matname;
                        matind = numel(meshprops.materialnames);
                    end
                    meshprops.facematerialind(:,capfaceinds) = matind;
                    
                    % no smoothing group
                    meshprops.facesmoothgroupind(:,capfaceinds) = 0; % 0 for no smoothing group
                else
                    error('Unknown cap type.');
                end

            end
        end
    end
end

function [v,f] = submesh(verts,faces,faceinds)
    f = faces(:,faceinds);
    vinds = unique(f(:));
    newvinds(vinds) = (1:numel(vinds))';
    f = newvinds(f);
    v = verts(:,vinds);
end

function [verts,faces] = addcap(verts,faces,capborderpolyvinds,capnormal,captype,varargin)
            
    if isempty(capborderpolyvinds)
        return;
    end
    
    settings = struct(...
        'height',0);
    settings = nvpairs2struct(varargin,settings);
    
    vertoffset = 0;
    edges = zeros(2,0);
    for i=1:numel(capborderpolyvinds)
        edges = [edges,[...
            vertoffset+1:vertoffset+numel(capborderpolyvinds{i});...
            [vertoffset+2:vertoffset+numel(capborderpolyvinds{i}),vertoffset+1]]]; %#ok<AGROW>
        vertoffset = vertoffset + numel(capborderpolyvinds{i});
    end
    
    allcapborderpolyvinds = [capborderpolyvinds{:}]';

    if strcmp(captype,'flat')
        % dt seems to generate ccw triangles
        
        dt = delaunayTriangulation(...
            verts(1:2,allcapborderpolyvinds)',edges');
        faces = [faces,allcapborderpolyvinds(dt.ConnectivityList(dt.isInterior,:)')];
        
    elseif strcmp(captype,'simpleroof')
        
        roofheight = settings.height;
        roofbaseheight = verts(3,capborderpolyvinds{1}(1));
        
        % move cap border vertices by a small random vector in the plane of
        % the cap to avoid numeric issues with the skeleton
        bverts = verts(1:2,allcapborderpolyvinds);
        [~,~,capbbdiag] = pointsetBoundingbox(bverts);
        bverts = bverts + rand(size(bverts)).*(capbbdiag*0.0000001);
        verts(1:2,allcapborderpolyvinds) = bverts;
        
        boundaryverts = cell(1,numel(capborderpolyvinds));
        for i=1:numel(capborderpolyvinds)
            boundaryverts{i} = verts(1:2,capborderpolyvinds{i});
        end
        
        % use straight line skeleton for the roof
        [sverts,sheight,~,~,sfaces] = polygonStraightSkeleton_mex(boundaryverts);
        
        % compute height manually (cgal sometimes returns negative values
        % for no good reason)
        newvertinds = numel(allcapborderpolyvinds)+1:size(sverts,2);
        if not(isempty(newvertinds))
            mask = sheight(newvertinds) < 0;
            sheight(newvertinds(mask)) = inf;
            for i=1:numel(capborderpolyvinds)
                [h,~] = pointPolylineDistance_mex(...
                    sverts(1,newvertinds(mask)),...
                    sverts(2,newvertinds(mask)),...
                    verts(1,capborderpolyvinds{i}),...
                    verts(2,capborderpolyvinds{i}));
                sheight(newvertinds(mask)) = min(sheight(newvertinds(mask)),h);                
            end
        end
        
        % triangulate faces
        hasdegeneracies = false;
        stris = zeros(3,0);
        for i=1:numel(sfaces)
            dt = delaunayTriangulation(sverts(1:2,sfaces{i})',...
                [1:numel(sfaces{i});[2:numel(sfaces{i}),1]]');
            facetris = dt.ConnectivityList(dt.isInterior,:)';
            sfaces{i} = sfaces{i}';
            
            % in degenerate cases dt generates additional vertices, add
            % those vertices to the list
            additionalvinds = unique(facetris(facetris > numel(sfaces{i})));
            if not(isempty(additionalvinds))
                hasdegeneracies = true;
                break;
%                 warning('Degnerate polygons created by the roof straight skeleton, mesh is probably not consistent anymore.');
%                 sfaces{i}(additionalvinds) = size(sverts,2)+1:size(sverts,2)+numel(additionalvinds);
%                 newverts = dt.Points(additionalvinds,:)';
%                 [~,minind] = min(pdist2(newverts',sverts'),[],2);
%                 newheight = sheight(minind');
%                 sverts(:,end+1:end+numel(additionalvinds)) = newverts;
%                 sheight(:,end+1:end+numel(additionalvinds)) = newheight;
            end
            
            stris = [stris,sfaces{i}(facetris)]; %#ok<AGROW>
        end
        
        if hasdegeneracies
            % straight line skeleton has degeneracies, try inner voronoi
            % diagram instead
            
            if numel(capborderpolyvinds) > 1
                error('Extrusion roof has degeneracies.');
            end
            
            [sx,sy,sheight,binds,~,~,sfaces] = ...
                polygonInnerVoronoiDiagram(verts(1,capborderpolyvinds{1}),verts(2,capborderpolyvinds{1}),'distance');
            if numel(binds) ~= numel(capborderpolyvinds{1}) || not(all(binds == 1:numel(capborderpolyvinds{1})))
                error('Skeleton vertices not sorted correctly.');
            end
            sverts = [sx;sy];
            
            % triangulate faces
            hasdegeneracies = false;
            stris = zeros(3,0);
            for i=1:numel(sfaces)
                dt = delaunayTriangulation(sverts(1:2,sfaces{i})',...
                    [1:numel(sfaces{i});[2:numel(sfaces{i}),1]]');
                facetris = dt.ConnectivityList(dt.isInterior,:)';
                sfaces{i} = sfaces{i}';

                % in degenerate cases dt generates additional vertices, add
                % those vertices to the list
                additionalvinds = unique(facetris(facetris > numel(sfaces{i})));
                if not(isempty(additionalvinds))
                    hasdegeneracies = true;
                    break;
                end
                
                stris = [stris,sfaces{i}(facetris)]; %#ok<AGROW>
            end
            
        end
        
        if hasdegeneracies
            % inner voronoi diagram has degenerarcies as well, use flat cap
            % instead
            
            allcapborderpolyvinds = [capborderpolyvinds{:}];
            dt = delaunayTriangulation(...
                verts(1:2,allcapborderpolyvinds)',edges');
            faces = [faces,allcapborderpolyvinds(dt.ConnectivityList(dt.isInterior,:)')];
        else
            sheight = sheight .* (roofheight/max(sheight));
            sverts = [sverts;ones(1,size(sverts,2)).*roofbaseheight];
            sverts = sverts + bsxfun(@times,capnormal,sheight);

            skel2meshvertind = zeros(1,size(sverts,2));
            skel2meshvertind(1:numel(allcapborderpolyvinds)) = allcapborderpolyvinds;
            skel2meshvertind(numel(allcapborderpolyvinds)+1:end) = size(verts,2)+1 : size(verts,2)+size(sverts,2)-numel(allcapborderpolyvinds);

            verts = [verts,sverts(:,numel(allcapborderpolyvinds)+1:end)];
            faces = [faces,skel2meshvertind(stris)];
        end
    else
        error('Unkown cap type.');
    end
    
end
