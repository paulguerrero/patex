function [I,sI,type]=linePlaneIntersection(n,V0,P0,P1)
%linePlaneIntersection computes the intersection of a plane and a segment(or
%a straight line)
% Inputs: 
%       n: normal vector of the Plane 
%       V0: any point that belongs to the Plane 
%       P0: end point 1 of the segment P0P1
%       P1:  end point 2 of the segment P0P1
%
%Outputs:
%      I    is the point of interection 
%     type is an indicator:
%      0 => disjoint (no intersection)
%      1 => the plane intersects P0P1 in the unique point I
%      2 => the segment lies in the plane
%      3 => the intersection lies outside the segment P0<->P1
%
% Example:
% Determine the intersection of following the plane x+y+z+3=0 with the segment P0P1:
% The plane is represented by the normal vector n=[1 1 1]
% and an arbitrary point that lies on the plane, ex: V0=[1 1 -5]
% The segment is represented by the following two points
% P0=[-5 1 -1]
%P1=[1 2 3]   
% [I,check]=plane_line_intersect([1 1 1],[1 1 -5],[-5 1 -1],[1 2 3]);

%This function is written by :
%                             Nassim Khaled
%                             Wayne State University
%                             Research Assistant and Phd candidate
%If you have any comments or face any problems, please feel free to leave
%your comments and i will try to reply to you as fast as possible.

P0 = bsxfun(@times,P0,ones(1,size(n,2)));
P1 = bsxfun(@times,P1,ones(1,size(n,2)));
V0 = bsxfun(@times,V0,ones(1,size(P0,2)));
n = bsxfun(@times,n,ones(1,size(P0,2)));

nI = max(size(P0,2),size(n,2));
I = zeros(3,nI);
type = zeros(1,nI);

% I=[0 0 0];
u = P1-P0;
w = P0-V0;
% D = dot(n,u);
D = sum(n.*u,1);
% N = -dot(n,w);
N = -sum(n.*w,1);

mask1 = abs(D) < 10^-7; % parallel to the plane
mask2 = N==0;

type(mask1 & mask2) = 2; % The segment lies in plane
type(mask1 & not(mask2)) = 0; % The segment is parallel to plane and not in the plane

% check=0;
% if abs(D) < 10^-7        % The segment is parallel to plane
%     if N == 0           
%         check=2;
%         return
%     else
%         check=0;       %no intersection
%         return
%     end
% end

%compute the intersection parameter
mask1 = not(mask1);
sI = zeros(1,nI);
sI(mask1) = N(mask1) ./ D(mask1);
I(:,mask1) = P0(:,mask1)+ bsxfun(@times,sI(:,mask1),u(:,mask1));

mask2 = (sI < 0 | sI > 1);
type(mask1 & mask2) = 3; % The intersection point lies outside the segment
type(mask1 & not(mask2)) = 1; % The intersection point lies inside the segment

% if (sI < 0 || sI > 1)
%     check= 3;          %The intersection point  lies outside the segment, so there is no intersection
% else
%     check=1;
% end
