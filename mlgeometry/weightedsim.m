% v1 : d x 1
% v2 : d x n
% weight : d x 1 or d x n
% method : string
% additional name,value parameters
function [s,selm] = weightedsim(v1,v2,weights,method,varargin)

    if isempty(weights)
        weights = ones(size(v2));
    end
    
    if size(v1,2) ~= size(v2,2)
        v1 = bsxfun(@times,v1,ones(1,size(v2,2)));
    end
    if size(weights,2) ~=  size(v2,2)
        weights = bsxfun(@times,weights,ones(1,size(v2,2)));
    end
    
    if iscell(method)
        distmethod = method{1};
        distweightmethod = method{2};
    elseif strcmp(method,'negativeL2dist')
        distmethod = 'L2dist';
        distweightmethod = 'negative';
    elseif strcmp(method,'negativeL1dist')
        distmethod = 'L1dist';
        distweightmethod = 'negative';
    elseif strcmp(method,'product')
        selm = v1.*v2.*w;
        s = sum(selm,1);
        return;
    elseif strcmp(method,'gaussianL1dist')
        distmethod = 'L1dist';
        distweightmethod = 'gaussian';
    elseif strcmp(method,'gaussianL2dist')
        distmethod = 'L2dist';
        distweightmethod = 'gaussian';
    elseif strcmp(method,'SL0sim')
        % additional input argument: variance of gaussians (scalar, d x 1 or d x n)
        if numel(varargin{1}) ~= 1 && size(varargin{1}) == 1
            varargin{1} = bsxfun(@times,varargin{1},ones(1,size(v2,2)));
        end
        selm = exp((v1-v2).^2 ./ (2.*varargin{1}));
        s = sum(selm,1);
        return;
    elseif strcmp(method,'negativeEMD')
        distmethod = 'EMD';
        distweightmethod = 'negative';
    else
        error('Unknown similarity method.');
    end
    
    [d,selm,usedparams] = weighteddist(v1,v2,weights,distmethod,varargin{:});
    
    if strcmp(distweightmethod,'negative')
        s = -d;
    elseif strcmp(dist2simmethod,'gaussian')
        % additional input argument: variance of gaussians (scalar or 1 x n)
        s = exp(-d.^2 ./ (2.*varargin{usedparams+1}));
    elseif strcmp(dist2simmethod,'inverse')
        s = 1./d;
    elseif strcmp(dist2simmethod,'inversesquared')
        s = 1./d.^2;
    elseif strcmp(dist2simmethod,'none')
        % do nothing
    else
        error('Unknown distance weighting method.');
    end
end
