function [sx,sy,st,binds,efrom,eto,faces] = polygonInnerVoronoiDiagram(px,py,timetype,res)

    if any(isnan(px)) || any(isnan(py))
        error('polygons with holes or multiple components not supported.');
    end
    
%     if ispolycw(px,py)
%         [px,py] = poly2ccw(px,py);
%     end
    
%     polygonextent = sqrt((max(px)-min(px)).^2 + (max(py)-min(py)).^2);
    if nargin < 4
%         res = 100000/polygonextent;
        res = 1000;
    end
    
    % check if there are numerical problems with the polygon
    [bbmax,bbmin] = pointsetBoundingbox([px;py]);

    maxcoord = max(max(abs([bbmax,bbmin])));

    % leave room for about 7 bit for larger values, that leaves
    % about 25 bits for precision (or 24 because signed),
    % that are about 7 significant decimal digits
    scale = (double(intmax('int32') / 128.0)) / maxcoord;
    ipx = round(px.*scale);
    ipy = round(py.*scale);

    minedgelen = min(sqrt(sum(diff([ipx([1:end,1]);ipy([1:end,1])],1,2).^2,1)));
%         while mindiff < 10 && size(ipx,2) > 3
%             mergeind1 = mindiffind;
%             mergeind2 = mod(mindiffind,size(ipx,2))+1;
%             ipx(mergeind1) = (ipx(mergeind1) + ipx(mergeind2))/2;
%             ipy(mergeind1) = (ipy(mergeind1) + ipy(mergeind2))/2;
%             ipx(mergeind2) = [];
%             ipy(mergeind2) = [];
%             px(mergeind1) = (px(mergeind1) + px(mergeind2))/2;
%             py(mergeind1) = (py(mergeind1) + py(mergeind2))/2;
%             px(mergeind2) = [];
%             py(mergeind2) = [];
% 
%             [mindiff,mindiffind] = min(sqrt(sum(diff([ipx([1:end,1]);ipy([1:end,1])],1,2).^2,1)));
%         end
% 
%     if minedgelen < 10
%         error('Numeric issues with this polygon, vertices are too close together.');
%     end
    
    segsx = [ipx;ipx([2:end,1])];
    segsy = [ipy;ipy([2:end,1])];
    minedgedist = inf;
    for i=1:numel(ipx)
        nonadjacentsegmask = true(1,size(segsx,2));
        nonadjacentsegmask(smod([i-1,i],1,numel(ipx)+1)) = false;
        [d,~] = pointPolylineDistance_mex(ipx(i),ipy(i),...
            segsx(1,nonadjacentsegmask),segsy(1,nonadjacentsegmask),...
            segsx(2,nonadjacentsegmask),segsy(2,nonadjacentsegmask));
        if d<minedgedist
            minedgedist = d;
        end
    end
    
    mindist = min(minedgedist,minedgelen);
    
    np = numel(px);
    
%     % no shuffling
%     if mindist < 10
%         error('Numeric issues with this polygon, vertices are too close to other vertices or to non-adjacent edges.');
%     end
%     [sx,sy,efrom,eto,etype] = polygonVoronoiDiagram_mex(px,py,np,res);
    

    % shuffle with a small random vector to avoid numeric issues
    if mindist < 30
        error('Numeric issues with this polygon, vertices are too close to other vertices or to non-adjacent edges.');
    end
    randlen = 15/scale;
    spx = px + (rand(size(px))-0.5).*(randlen*2); % random integers in (-randlen,randlen)
    spy = py + (rand(size(px))-0.5).*(randlen*2); % random integers in (-randlen,randlen)
    [sx,sy,efrom,eto,etype] = polygonVoronoiDiagram_mex(spx,spy,np,res);
    sx(1:np) = px(1:np); % back to original non-shuffled vertices
    sy(1:np) = py(1:np); % back to original non-shuffled vertices
    

    
%     % initial matching of skeleton vertices to boundary vertices
%     dists = pdist2([sx;sy]',[px;py]');
%     [d,binds] = min(dists,[],1);
%     binds(d>mean(mean(dists))*0.000001) = -1;
%     spinds = ones(1,numel(sx)).*-1;
%     spinds(binds(binds>0)) = find(binds>0);
    
%     % find ray edges that start or end inside the polygon
%     % and split them into one edge between inside and boundary and one edge
%     % between boundary and inf
%     raytoinds = find(efrom <= 0 & eto > 0);
%     rayfrominds = find(eto <= 0 & efrom > 0);
%     raytoinds(stype(eto(raytoinds)) <= 0) = [];
%     rayfrominds(stype(efrom(rayfrominds)) <= 0) = [];
%     ininfeinds = [raytoinds,rayfrominds];
%     if not(isempty(ininfeinds))
%         mask = efrom(ininfeinds) <= 0;
%         svinds(mask) = eto(ininfeinds(mask));
%         mask = eto(ininfeinds) <= 0;
%         svinds(mask) = efrom(ininfeinds(mask));
% 
% %         dists = pdist2([sx(svinds);sy(svinds)]',[px;py]');
% %         remainingpinds = 1:numel(px);
%         for i=1:numel(svinds)
%             % find boundary points the skeleton vertex does not alrady have an edge to
%             neighboureinds = find(efrom == svinds(i) | eto == svinds(i));
%             neighbourvinds = unique([efrom(neighboureinds),eto(neighboureinds)]);
%             neighbourvinds(neighbourvinds<=0) = [];
%             neighbourvinds = spinds(neighbourvinds);
%             neighbourvinds(neighbourvinds<=0) = [];
%             freepinds = 1:numel(px);
%             freepinds(neighbourvinds) = [];
%             
%             [~,minind] = min(dists(svinds(i),freepinds));
% %             pind = remainingpinds(freepinds(minind));
%             pind = freepinds(minind);
%             
%             sx = [sx,px(pind)]; %#ok<AGROW>
%             sy = [sy,py(pind)]; %#ok<AGROW>
%             stype = [stype,0]; %#ok<AGROW>
%             efrom = [efrom,efrom(ininfeinds(i)),numel(sx)]; %#ok<AGROW>
%             eto = [eto,numel(sx),eto(ininfeinds(i))]; %#ok<AGROW>
% %             dists(:,ind) = [];
% %             remainingpinds(ind) = [];
%         end
%         efrom(ininfeinds) = [];
%         eto(ininfeinds) = [];
%     end
    
    
%     % remove edges to infinity
%     validmask = efrom > 0 & eto > 0;
%     efrom = efrom(validmask);
%     eto = eto(validmask);
%     
%     % find edges with one point inside and one point outside the polygon
%     % these edges go through a polygon vertex
%     % split these edges so they have a vertex at the boundary
%     inouteinds = find(stype(efrom) < 0 & stype(eto) > 0 | ...
%                       stype(efrom) > 0 & stype(eto) < 0);
%     if not(isempty(inouteinds))
%         [d,neighboureinds] = pointPolylineDistance_mex(...
%             px,py,...
%             sx(efrom(inouteinds)),sy(efrom(inouteinds)),...
%             sx(eto(inouteinds)),sy(eto(inouteinds)));
%         pinds = 1:numel(px);
%         [~,perm] = sort(d,'ascend'); % sort so points with smallest distance are first
%         neighboureinds = neighboureinds(perm);
%         pinds = pinds(perm);
%         [~,ia] = unique(neighboureinds,'first'); % take point closest to the edge (first one for each edge has smallest distance)
%         neighboureinds = neighboureinds(ia);
%         pinds = pinds(ia);
%         if numel(inouteinds) ~= numel(pinds) || any(neighboureinds-(1:numel(inouteinds))) % check if all edges have points
%             error('Could not split inside/outside edge, no boundary vertex found');
%         end
%         sx = [sx,px(pinds)];
%         sy = [sy,py(pinds)];
%         stype = [stype,zeros(1,numel(pinds))];
%         pinds = numel(sx)-numel(pinds)+1:numel(sx);
%         efrom = [efrom,efrom(inouteinds),pinds];
%         eto = [eto,pinds,eto(inouteinds)];
%         efrom(inouteinds) = [];
%         eto(inouteinds) = [];
%     end

    
    % remove vertices and edges outside the polygon
%     outsidevertmask = stype < 0;
%     outsidevertinds = find(outsidevertmask);
%     outsideedgemask = ismember(efrom,outsidevertinds) | ismember(eto,outsidevertinds);
%     outsideedgemask = ismember(efrom,outsidevertinds) | ismember(eto,outsidevertinds);
    outsideedgemask = etype < 0;
    
    clear etype;

    efrom(outsideedgemask) = [];
    eto(outsideedgemask) = [];
%     etype(outsideedgemask) = [];
    
    outsidevertmask = true(1,numel(sx));
    outsidevertmask(efrom) = false;
    outsidevertmask(eto) = false;

    old2newmap = ones(1,numel(sx)).*-1;
    sx(outsidevertmask) = [];
    sy(outsidevertmask) = [];
    
    if isempty(sx)
        error('polygon has no medial axis (may be too thin)')
%         [cx,cy] = polygonCentroid(px,py);
%         sx = [px,cx];
%         sy = [py,cy];
%         st = [zeros(1,numel(px)),0.00001];
%         efrom = 1:numel(px);
%         eto = ones(1,numel(efrom)).*numel(sx);
%         binds = 1:numel(px);
%         return;
    end
    
%     stype(outsidevertmask) = [];
    old2newmap(not(outsidevertmask)) = 1:numel(sx);
    efrom = old2newmap(efrom);
    eto = old2newmap(eto);
    
%     % remove degenerate egdes (edges that are too small)
%     for i=1:10000
%         elen = sqrt((sx(eto)-sx(efrom)).^2 + (sy(eto)-sy(efrom)).^2);
%         mask = elen < eps(polygonextent)*1000;
%         
%         
%     end


       
    % find skeleton boundary edges
%     dists = pdist2([sx;sy]',[px;py]');
%     [d,binds] = min(dists,[],1);
%     if any(d>mean(mean(dists))*0.000001)
%         error('could not find skeleton edge adjacent to boundary vertex');
%     end
% %     binds = find(stype == 0);
% %     innervertinds = find(stype > 0);
%     
%     
%     if not(isempty(setdiff(binds,find(stype==0)))) || ...
%        not(isempty(setdiff(find(stype==0),binds)))
%         error('boundary vertices not identified correctly');
%     end
    
    binds = 1:numel(px);
    
    innervertinds = 1:numel(sx);
    innervertinds(binds) = [];
    
    % compute distance of each inner skeleton vertex to the boundary
    sd = zeros(1,numel(sx));
    [d,~] = pointPolylineDistance_mex(...
        sx(innervertinds),sy(innervertinds),...
        px,py,px([2:end,1]),py([2:end,1]));
    sd(innervertinds) = d;
    
    % compute skeleton edge time
    if strcmp(timetype,'morph')
        % compute skeleton edge lengths
        elen = sqrt((sx(eto) - sx(efrom)).^2 + (sy(eto) - sy(efrom)).^2);
        
        maxspeed = 5; % max speed along skeleton edge in multiples of the normal border offset speed
        [st,scenter,sctime] = skeletonContraction(sd,efrom,eto,elen,maxspeed);%,sx,sy);

        clear elen;
    
        % split edge containing skeleton center into two edges (if center is
        % not on a skeleton vertex)
        sceind = scenter(1);
        scf = scenter(2);
        if scf ~= 0 && scf ~= 1
            % compute skeleton center position
            scx = sx(efrom(sceind))*(1-scf) + sx(eto(sceind))*scf;
            scy = sy(efrom(sceind))*(1-scf) + sy(eto(sceind))*scf;
            
            % add vertex at skeleton center
            sx(end+1) = scx;
            sy(end+1) = scy;
            st(end+1) = sctime;

            % add edges two new edges to skeleton center
            efrom(end+1) = efrom(sceind);
            eto(end+1) = numel(sx);
            efrom(end+1) = eto(sceind);
            eto(end+1) = numel(sx);

            % remove old edge containing skeleton center
            efrom(sceind) = [];
            eto(sceind) = [];
        end
    elseif strcmp(timetype,'distance');
        st = sd;
    else
        error('unknown time type');
    end
    
    if nargout >= 7
        if not(ispolycw(px,py))
            error('Polygon is not cw.');
        end
        
        % boundary is cw so add boundary edges reversed, so they form
        % half-edges of the ccw faces
        
        % add boundary edges
        ebfrom = binds;
        ebto = binds([2:end,1]);
        
        edges = sparse(...
            [efrom,eto,ebto],...
            [eto,efrom,ebfrom],...
            ones(1,numel(efrom)*2+numel(ebfrom)),...
            numel(sx),numel(sx));
        
%         faces = cell(1,numel(binds));
%         for i=1:numel(binds)
%             faces{i} = [binds(i),binds(smod(i+1,1,numel(binds)+1))];

        c = 1;
        faces = cell(1,0);
        while true
            
            [from,to] = find(edges > 0,1,'first');
            if isempty(from)
                % done, no more edges found
                break;
            end
            
            faces{c} = [from,to];
            edges(from,to) = -1; %#ok<SPRIX> % does not change non-zero pattern
            
            while true
                outgoing = find(edges(faces{c}(end),:) > 0);
                outgoing(outgoing == faces{c}(end-1)) = []; % remove edge back
                if numel(outgoing) > 1
                    edgeangle = atan2(...
                        sy(faces{c}(end))-sy(faces{c}(end-1)),...
                        sx(faces{c}(end))-sx(faces{c}(end-1)));
                    outgoingedgeangles = atan2(...
                        sy(outgoing)-sy(faces{c}(end)),...
                        sx(outgoing)-sx(faces{c}(end)));
                    [~,ind] = max(smod(outgoingedgeangles-edgeangle,-pi,pi)); % take edge with right-most turn (boudary is cw)
                elseif numel(outgoing) == 1
                    ind = 1;
                else
                    error('Dead-end edge found, this should not be possible in a skeleton.');
                end
                
                if outgoing(ind) == faces{c}(1)
                    edges(faces{c}(end),faces{c}(1)) = -1; %#ok<SPRIX>
                    break;
                end
                
                faces{c}(end+1) = outgoing(ind);
                
                edges(faces{c}(end-1),faces{c}(end)) = -1; %#ok<SPRIX> % does not change non-zero pattern

                if numel(faces{c}) > numel(efrom)
                    error('Could not close skeleton face.');
                    % should not happen
                end
            end
            
%             % make face ccw
%             faces{c} = faces{c}([1,end:-1:2]);
            
            if numel(faces) > numel(efrom)
                error('Could not find all skeleton faces.');
                % should not happen
            end
            
            c = c + 1;
        end
    end
end
