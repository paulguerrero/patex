% p: 2 x 1 or 2 x n matrix of 1 or n 2-dimensional points
% or 6 x 1 or 6 x n matrix of 1 or n 6-dimensional poses in 2D space (2 position, 1 angle, 2 scale, 1 mirror)
% order: mirror, scale, rotate, translate
% scale: 2 x n or 2 x 1 matrix of 2-dimensional scale vectors
% angle: 1 x n or 1 x 1 matrix of angles (in radians)
% trans: 2 x n or 2 x 1 matrix of 2-dimensional translations
% optional: mirror: 1 x n or 1 x 1 matrix of mirroring flags
% (mirroring is about x axis: y = -y)
function tp = transform2D(p, scale, angle, trans, mirror)
    
    tp = bsxfun(@times,ones(1,size(scale,2)),p);

    if nargin >= 5 && any(mirror)
        % mirror about x axis
        tp(2,:) = tp(2,:).*(logical(mirror).*-2+1);
        
        if size(tp,1) >= 3
            % negative angle (because dir = x-axis is 0 angle)
            tp(3,:) = tp(3,:).*(logical(mirror).*-2+1);
        end
        
        if size(tp,1) >= 6
            tp(6,:) = bsxfun(@xor,tp(6,:),mirror);
        end
    end
    
    if size(tp,1) >= 5
        tp(4:5,:) = bsxfun(@times,tp(4:5,:),scale);
    end
    
    if size(tp,1) >= 3
        tp(3,:) = smod(bsxfun(@plus,tp(3,:),angle),-pi,pi);
    end

    % matrix multiplication written out (so i can multiply n matrices with n vectors)
    cosangle = cos(angle);
    sinangle = sin(angle);
    tp(1:2,:) = [(cosangle.*scale(1,:)).*tp(1,:) + (-sinangle.*scale(2,:)).*tp(2,:); ...
                 (sinangle.*scale(1,:)).*tp(1,:) + ( cosangle.*scale(2,:)).*tp(2,:)];
      
    tp(1:2,:) = bsxfun(@plus,tp(1:2,:),trans);
end
