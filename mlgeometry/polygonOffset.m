% POLYGONOFFSET Offset or inset a polygon.
%   [ox,oy] = POLYGONOFFSET(x,y,offset) offsets the polygon defined by x
%   and y by an amount equal to offset. For a polygon with N vertices,
%   x and y each must be 1xN vectors of the x and y coordinates of each
%   vertex. x and y can contain multiple polygon contours, either separated
%   by NaNs or each contour in a separate cell.
%   The contours should form a single simple polygon, possibly with holes.
%   The output ox and oy are 1xM cell arrays, where M is the number of
%   output polygons. Each cell contains an 1xL cell array, where L is the
%   number of contours of the output polygon. The first contour is always
%   the outer contour, the remaining contours are inner contours (holes).
%
%   [ox,oy] = POLYGONOFFSET(x,y,offset,res) additionally specifies the
%   resolution of the rounded offset around corners in the output
%   polygons. res is the number of samples per 360 degrees (default: 64).
%
%   See also POLYGONMINKOWSKISUM
function [ox,oy] = polygonOffset(x,y,offset,res)

    if nargin < 4
        res = 64;
    end
    
    if offset == 0
        if not(iscell(x))
            [x,y] = polysplit(x,y);
        end
        ox = {x};
        oy = {y};
        return;
    end
    
    angles = linspace(2*pi,0,res+1); % clockwise for outer contour
    angles(end) = [];
    dists = ones(size(angles)).*abs(offset);
    [circlex,circley] = pol2cart(angles,dists);
    
    [ox,oy] = polygonMinkowskiSum(circlex,circley,x,y,offset>=0);
end
