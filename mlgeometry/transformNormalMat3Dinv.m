% order: inverse rotate, inverse scale, mirror (= inverse mirror)
% scale: 3 x n or 3 x 1 matrix of 3-dimensional scale vectors
% angle: 4 x n or 4 x 1 matrix of quaternions
% optional: mirror: 1 x n or 1 x 1 matrix of mirroring flags
% (mirroring is about xz plane: y = -y)
% returns 3 x 3 x n 3D normal transformation matrices
function m = transformNormalMat3Dinv(scale, angle, mirror)

    scale = reshape(scale,3,1,[]);
%     angle = reshape(angle,4,1,[]);
    
    if nargin >= 3
        mirror = reshape(mirror,1,1,[]);
        mir = (logical(mirror).*-2+1);
    else
        mir = ones(1,1,size(scale,3));
    end   

    % matrix multiplication written out (so i can multiply n matrices with n vectors)
    invrot = permute(quat2dcm(quatconj(angle')),[2,1,3]);
    m = [     scale(1,:,:).*invrot(1,1,:),      scale(1,:,:).*invrot(1,2,:),      scale(1,:,:).*invrot(1,3,:); ...
         mir.*scale(2,:,:).*invrot(2,1,:), mir.*scale(2,:,:).*invrot(2,2,:), mir.*scale(2,:,:).*invrot(2,3,:); ...
              scale(3,:,:).*invrot(3,1,:),      scale(3,:,:).*invrot(3,2,:),      scale(3,:,:).*invrot(3,3,:)];
end
