function [ox,oy] = polygonMaskBoundary(x,y,closed,mx,my)
    
    if not(iscell(mx))
        mx = {mx};
        my = {my};
    end
    
    % remove empty components
    mx(cellfun(@(a) isempty(a),mx)) = [];
    
    if isempty(x) || isempty(mx)
        ox = [];
        oy = [];
        return;
    end
    
    p = [x;y];
    mp = cellfun(@(a,b) [a;b],mx,my,'UniformOutput',false);
    
    % check if bounding boxes intersect
    [pbbmin,pbbmax] = pointsetBoundingbox(p);
    [maskbbmin,maskbbmax] = pointsetBoundingbox(mp);
    if any(pbbmax < maskbbmin) || any(pbbmin > maskbbmax)
        ox = [];
        oy = [];
        return;
    end
    
    % compute edges
    ncomp = cellfun(@(a) size(a,2),mp);
    if any(ncomp<3)
        error('Invalid component with < 3 vertices found.');
    end
    ncomp = cumsum(ncomp);
    ncomp = [[1,ncomp(1:end-1)+1];ncomp];
    medges = [];
    for i=1:size(ncomp,2)
        si = ncomp(1,i);
        ei = ncomp(2,i);
        medges = [medges,[si:ei;[si+1:ei,si]]]; %#ok<AGROW>
    end
    mp = cell2mat(mp);
    
    if closed
        psegs = [p;p(:,[2:end,1])];
    else
        psegs = [p(:,1:end-1);p(:,2:end)];
    end
    
%     intersects = linesegLinesegIntersect(...
%         psegs',...
%         [mp;mp(:,[2:end,1])]');
    [intersects,ip] = linesegLinesegIntersection2D_mex(...
        psegs(1:2,:),psegs(3:4,:),...
        mp(:,medges(1,:)),mp(:,medges(2,:)));
    
    if not(any(any(intersects>0)))
        testpoint = p(:,1);

        testpointin = false;
        for k=1:numel(mx)
           if pointInPolygon_mex(testpoint(1),testpoint(2),mx{k},my{k})
               testpointin = true;
               break;
           end
        end
        
%         if inpoly(testpoint',mp',medges')
        if testpointin
            % all are inside the mask
            ox = p(1,:);
            oy = p(2,:);
        else
            % all are outside the mask
            ox = [];
            oy = [];
        end
    else
        [hullseg,pseg] = find(intersects'>0);
        hullseg = hullseg';
        pseg = pseg';
        inds = sub2ind(size(intersects),pseg,hullseg);
        ix = ip(:,:,1);
        ix = ix(inds);
        iy = ip(:,:,2);
        iy = iy(inds);
        error('todo: compute normalized distance 1 to 2 from intersection points.');
        psegf = intersects.intNormalizedDistance1To2(inds);
        
        clear hullseg;
        
        if not(closed)
            % remove intersection for first/last segment touching the mask
            mask = pseg == size(psegs,2) & psegf >= 1;
            pseg(mask) = [];
            psegf(mask) = [];
            ix(mask) = [];
            iy(mask) = [];
            mask = pseg == 1 & psegf <= 0;
            pseg(mask) = [];
            psegf(mask) = [];
            ix(mask) = [];
            iy(mask) = [];
        end
        
        % convert intersection at end of one segment to intersection at
        % start of next
        mask = psegf == 1;
        psegf(mask) = 0;
        pseg(mask) = mod(pseg(mask),size(p,2))+1;
        
        % remove duplicate intersections
        [~,uniqueinds] = unique([pseg;psegf]','rows');
        pseg = pseg(uniqueinds);
        psegf = psegf(uniqueinds);
        ix = ix(uniqueinds);
        iy = iy(uniqueinds);
        
        
        if not(closed)
            % add first point as an intersection
            pseg = [1,pseg];
            psegf = [0,psegf];
            ix = [p(1,1),ix];
            iy = [p(2,1),iy];
            
            % add last point as an intersection
            pseg = [pseg,size(psegs,2)];
            psegf = [psegf,1];
            ix = [ix,p(1,end)];
            iy = [iy,p(2,end)];
            
            npseg = numel(pseg)-1;
            
%             % add first point
%             o = p(:,1);
        else
            npseg = numel(pseg);
        end
        
        o = zeros(2,0);
        for i=1:npseg
            % compute a testpoint to see if the next polyline piece is
            % inside the boundary of the second polygon
            nexti = mod(i,numel(pseg))+1;
            if pseg(nexti) == pseg(i) && psegf(i) <= psegf(nexti)
                f = (psegf(i) + psegf(nexti)) * 0.5;
            else
                f = (psegf(i) + 1) * 0.5;
            end
            testpoint = psegs(1:2,pseg(i)) .* (1-f) + psegs(3:4,pseg(i)) .* f;
            
            testpointin = false;
            for k=1:numel(mx)
               if pointInPolygon_mex(testpoint(1),testpoint(2),mx{k},my{k})
                   testpointin = true;
                   break;
               end
            end
%             if inpoly(testpoint',mp',medges')
            if testpointin
                % if polyline segment inside: add all points until the next
                % intersection
                firstaddind = mod(pseg(i),size(p,2))+1;
                lastaddind = pseg(nexti);
                
                if lastaddind < firstaddind && ...
                     (pseg(nexti) < pseg(i) || ...
                     (pseg(nexti) == pseg(i) && psegf(nexti) < psegf(i)))
                    addinds = [firstaddind:size(p,2),1:lastaddind];
                elseif lastaddind > firstaddind && ...
                     (pseg(nexti) == pseg(i) && psegf(nexti) > psegf(i))
                    addinds = [];
                else
                    addinds = firstaddind:lastaddind;
                end
                
                % remove start of segment of next intersection if the
                % intersection is exactly at the start point anyway (the
                % next intersection will be added later or was already added)
                if psegf(nexti) == 0 && not(isempty(addinds))
                    addinds(end) = [];
                end
                
                o = [o,[ix(i);iy(i)],p(:,addinds)]; %#ok<AGROW>
            else
                % if polyline segment not inside: add intersection point
                % and handle the case that the boundary of the mask
                % only touches the boundary of the polygon in
                % this intersection point (in this case do not add the
                % intersection point)
                if (closed && isempty(o)) || (not(isempty(o)) && not(isnan(o(1,end))))
                    o = [o,[ix(i);iy(i)],nan(2,1)]; %#ok<AGROW>
                end
            end
        end
        
        if closed
            % start point may still need to be removed if boundary of mask only
            % touches the polygon boundary at the first intersection point
            if size(o,2) >= 2 && isnan(o(1,end)) && isnan(o(1,2))
                % remove start point and following nan (there is still the nan
                % at the last position)
                o(:,[1,2]) = [];
            end
            
            % a set of adjacent points (without nans in between) may still be
            % wrapped from end to beginning of output 
            if any(isnan(o(1,:))) && not(isnan(o(1,1))) && not(isnan(o(1,end)))
                lastnanind = find(isnan(o(1,:)),1,'last');
                o = [o(:,lastnanind+1:end),o(:,1:lastnanind)];
            end
        else
            % add last point
%             if isempty(o) || not(isnan(o(1,end)))
            if not(isempty(o)) && not(isnan(o(1,end)))
                o(:,end+1) = p(:,end);
            end
        end
        
%         % testing:
%         mask = not(isnan(o(1,:)));
%         [d,~] = pointPolylineDistance_mex(o(1,mask),o(2,mask),mp(1,[1:end,1]),mp(2,[1:end,1]));
%         mask = not(inpoly(o(:,mask)',mp')') & d > 0.001;
        
        ox = o(1,:);
        oy = o(2,:);    
    end
end
