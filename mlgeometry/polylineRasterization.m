% grid nodes assumed to be at integer coordinates
function [I,pixcoords] = polylineRasterization(I,x,y,v,vperseg)
    
    if nargin < 5
        vperseg = false(numel(v),1);
    end

    if not(iscell(v))
        v = {v};
    end

    % dda
    x = round(x);
    y = round(y);
    
    if numel(x) == 1
        if x(1) >= 1 && x(1) <= size(I,2) && ...
           y(1) >= 1 && y(1) <= size(I,1)
            I(y(1),x(1),:) = cellfun(@(x) x(1),v);
            if nargout > 1
                pixcoords = [x(1);y(1)];
            end
        end
        return;
    end

    dx = x(2:end)-x(1:end-1);
    dy = y(2:end)-y(1:end-1);
    
	mask = abs(dx)>=abs(dy);
    m(mask) = abs(dx(mask));
    m(not(mask)) = abs(dy(not(mask)));
    
    if x(1) >= 1 && x(1) <= size(I,2) && ...
	   y(1) >= 1 && y(1) <= size(I,1)
        I(y(1),x(1),:) = cellfun(@(x) x(1),v);
        if nargout > 1
            pixcoords = [x(1);y(1)];
        end
    end
    for i=2:numel(x)
        sx = round(linspace(x(i-1),x(i),m(i-1)+1));
        sy = round(linspace(y(i-1),y(i),m(i-1)+1));
        
        % clipping
        clipmask = sx >= 1 & sx <= size(I,2) & ...
                   sy >= 1 & sy <= size(I,1);
        clipmask(1) = false; % don't draw first point of polyline segment
        
        if sum(clipmask) > 0
            for j=1:numel(v)
                if vperseg(j)
                    sv = ones(1,m(i-1)+1).*v{j}(i-1);
                else
                    sv = linspace(v{j}(i-1),v{j}(i),m(i-1)+1);    
                end
                
                I(sub2ind(size(I),sy(clipmask),sx(clipmask),ones(1,sum(clipmask)).*j)) = sv(clipmask);
                if nargout > 1
                    pixcoords = [pixcoords,[sx(clipmask);sy(clipmask)]]; %#ok<AGROW>
                end
            end
        end
    end
end
 