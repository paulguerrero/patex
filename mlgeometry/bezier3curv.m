% BEZIER3CURV Curvature of a cubic bezier spline.
%   val = BEZIER3CURV(p1,p2,p3,p4,t) returns the curvature of the
%   2-dimensional cubic bezier spline defined by the 4 control points
%   p1,p2,p3,p4 at the arclength parameter t
%   val = BEZIER3CURV(p1,p2,p3,p4,t,signed) additionally specifies if the
%   curvature returned should be signed or unsigned. Default is unsigned.
function curv = bezier3curv(p1,p2,p3,p4,t,varargin)
  

if isempty(varargin)
    signedcurv = false;
elseif numel(varargin) > 1 || not(isscalar(varargin{1}))
    error('Only one optional argument allowed. Must be logical true or false.');
else
    signedcurv = logical(varargin{1});
end


%%
if not(isvector(p1)) || not(isvector(t))
    error('control points and arclength parameters must be vectors');
end

if min(t) < 0 || max(t) > 1
    error('t must be in [0,1]');
end

%     dim = numel(p1);
%     n = numel(t);
% 
%     if dim ~= 2
%       error('the control points must be 2-dimensional')
%     end
%     
%     if iscolumn(t)
%         t = t';
%     end
%     
%     if isrow(p1)
%         p1 = p1';
%         p2 = p2';
%         p3 = p3';
%         p4 = p4';
%     end
%     
%     %%
%     sp1 = bezier3(p1,p2,p3,p4, t);
%   
% 	%%
%     % first de casteljau to find the four control points of an identical
%     % curve starting at t and going to either p1 or p4 (depending what is
%     % farther away - for numerical stability)
% 	mask = t > 0.5;
%     repmask = repmat(mask,dim,1);
%     
%     t1 = t(mask);
%     t2 = t(not(mask));
%     
%     sp2 = zeros(size(sp1));
%     sp3 = zeros(size(sp1));
%     
%     %%
%     % t > 0.5    
%     p10 = repmat(p1,1,numel(t1)) + (p2 - p1) * t1;
% 	p11 = repmat(p2,1,numel(t1)) + (p3 - p2) * t1;
%         
% 	p20 = p10 + (p11 - p10) .* repmat(t1,dim,1);
% 
%     sp2(repmask) = p20;
% 	sp3(repmask) = p10;
% 
% 	%%
%     % t < 0.5
% 	p11 = repmat(p2,1,numel(t2)) + (p3 - p2) * t2;
% 	p12 = repmat(p3,1,numel(t2)) + (p4 - p3) * t2;
% 
% 	p21 = p11 + (p12 - p11) .* repmat(t2,dim,1);
% 
% 	sp2(not(repmask)) = p21;
% 	sp3(not(repmask)) = p12;
% 
% 	%%
%     % then curvature at starting point of new curve (easier to compute at
% 	% starting point)
% 	sp1sp2 = sp2 - sp1;
% 	sp2sp3 = sp3 - sp2;
% 	a = sqrt(sum(sp1sp2.^2,1));
%   
% 	sp1sp2OrthNorm = [sp1sp2(2,:);-sp1sp2(1,:)];
% 	sp1sp2OrthNorm = sp1sp2OrthNorm ./ repmat(a,dim,1);
% 	h = abs(dot(sp1sp2OrthNorm, sp2sp3, 1));
% 
% 	curv = (2/3) .* (h./(a.^2));
%   
% % 	%%
% % 	% make curvature negative if concave (with respect to the inside of the
% % 	% shape)
% % 	sp1sp3 = sp3 - sp1;
% % 	sp1sp2_vec3 = [sp1sp2;zeros(1,n)];
% % 	sp1sp3_vec3 = [sp1sp3;zeros(1,n)];
% % 	indicator = cross(sp1sp2_vec3, sp1sp3_vec3,1);
% % 	indicator = indicator(3,:);
% %   
% % 	ncmask = not((indicator > 0 & mask) | (indicator <= 0 & not(mask)));
% % 	curv(ncmask) = -curv(ncmask);
% % %   if (indicator(3) > 0 && t > 0.5) || (indicator(3) <= 0 && t <= 0.5)
% % %     val = curv;
% % %   else
% % %     val = -curv;
% % %   end

% first and second parametric derivative 
dx = -3.*(-1+t).^2.*p1(1) + 3.*(-1+t).*(-1+3.*t).*p2(1) + 3.*(2-3.*t).*t.*p3(1) + 3.*t.^2.*p4(1);
dy = -3.*(-1+t).^2.*p1(2) + 3.*(-1+t).*(-1+3.*t).*p2(2) + 3.*(2-3.*t).*t.*p3(2) + 3.*t.^2.*p4(2);

ddx = (6-6.*t).*p1(1) + 6.*(-2+3.*t).*p2(1) + (6-18.*t).*p3(1) + 6.*t.*p4(1);
ddy = (6-6.*t).*p1(2) + 6.*(-2+3.*t).*p2(2) + (6-18.*t).*p3(2) + 6.*t.*p4(2);

% curvature of a parametric curve:
% curv = ((dx.^2 + dy.^2).^(3/2));
if signedcurv
    curv = (dx.*ddy - dy.*ddx) ./ ((dx.^2 + dy.^2).^(3/2));
else
    curv = abs(dx.*ddy - dy.*ddx) ./ ((dx.^2 + dy.^2).^(3/2));
end

% curv = abs(18 .* (-(-1 + t).^2 .* p1(1) + p2(1) + t .* ((-4 + 3 .* t) .* p2(1) + (2 - 3 .* t) .* p3(1) + t .* p4(1))) ...
%               .* (p1(2) - t .* p1(2) - 2 .* p2(2) + p3(2) + t .* (3 .* p2(2) - 3 .* p3(2) + p4(2))) ...
%           -18 .* (p1(1) - t .* p1(1) - 2 .* p2(1) + p3(1) + t .* (3 .* p2(1) - 3 .* p3(1) + p4(1))) ...
%               .* (-(-1 + t).^2 .* p1(2) + p2(2) + t .* ((-4 + 3 .* t) .* p2(2) + (2 - 3 .* t) .* p3(2) + t .* p4(2)))) ...
%        ./ (27 .*((-(-1 + t).^2 .* p1(1) + p2(1) + t .* ((-4 + 3 .* t) .* p2(1) + (2 - 3 .* t) .* p3(1) + t .* p4(1))).^2 ...
%                + (-(-1 + t).^2 .* p1(2) + p2(2) + t .* ((-4 + 3 .* t) .* p2(2) + (2 - 3 .* t) .* p3(2) + t .* p4(2))).^2).^(3/2));

end