% [x,y] = polygonInset(px,py,inset,type,insettype,originx,originy,angleres)
% [x,y] = polygonInset(insetpaths,inset,insettype)
function [x,y] = polygonInset(px,py,inset,type,insettype,origintype,in1,in2,in3)

    if size(px,1) == 6
        pathstart = px(1:2,:);
        pathend  = px(3:4,:);
        pathstarttime = px(5,:);
        pathendtime = px(6,:);
        
        origintraj = py;
        
        if nargin < 4
            insettype = 'normalized';
        else
            if not(ischar(type))
                error('fourth argument must be the inset type');
            end
            insettype = type;
        end
    else
        if nargin < 9
            in3 = [];
        end
        if nargin < 8
            in2 = [];
        end
        if nargin < 7
            in1 = [];
        end
        if nargin < 6
            origintype = [];
        end
        if nargin < 5
            insettype = 'normalized';
        end
        if nargin < 4
            type = 'morph';
        end
        
        [pathstart,pathend,pathstarttime,pathendtime,origintraj] = ...
            polygonInsetPaths(px,py,type,origintype,in1,in2,in3);
    end
    
    if strcmp(insettype,'absolute')
        time = inset;
    elseif strcmp(insettype,'normalized')
        maxendtime = max(pathendtime);
        time = inset*maxendtime;
    else
        error('unknown inset type');
    end
    
    % find origin path for the given time
    ind = find(time >= origintraj(1,:) & time <= origintraj(2,:),1,'last');
    originpathind = origintraj(3,ind);
    
    % shuffle paths so that the origin path is the first one
    pathstart = pathstart(:,[originpathind:end,1:originpathind-1]);
    pathend = pathend(:,[originpathind:end,1:originpathind-1]);
    pathstarttime = pathstarttime([originpathind:end,1:originpathind-1]);
    pathendtime = pathendtime([originpathind:end,1:originpathind-1]);
    
    % compute inset
    f = (time-pathstarttime) ./ (pathendtime - pathstarttime);
    activepathmask = f >= 0 & f < 1;
    f = f(activepathmask);
    
    x = pathstart(1,activepathmask) .* (1-f) + ...
        pathend(1,activepathmask) .* f;
    y = pathstart(2,activepathmask) .* (1-f) + ...
        pathend(2,activepathmask) .* f;
end
