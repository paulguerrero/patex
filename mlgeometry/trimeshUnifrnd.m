% v: 3xn vertices
% f: 3xm faces (indices into the vertex list)
% n: sample count
% fareas: face areas (optional, otherwise they are computed in this
% function)
function [r,facechoice] = trimeshUnifrnd(v,f,n,fareas)
    if nargin < 4 || isempty(fareas)
        fareas = triangleArea(v(:,f(1,:)),v(:,f(2,:)),v(:,f(3,:)));
    end
    
    r = zeros(3,0);
    
    if n == 0
        return;
    end
    
    if isempty(f)
        r = nan(3,n);
        return;
    end
    
    surfarea = sum(fareas);
    
    if surfarea <= 0
        % mesh has zero surface area (maybe each face is a line segment, or
        % a point), pick random for now
        % todo: pick faces according to maximum edge length
        facecdf = ones(1,size(faces,2))./size(faces,2);
    else
        facecdf = cumsum(fareas) ./ surfarea;
    end
    
    ur = unifrnd(0,1,n,1);
    facechoice = zeros(1,n);
    for l=1:n
        facechoice(l) = find(ur(l) < facecdf,1,'first');
    end
    
    v1 = v(:,f(1,facechoice));
    v1v2 = v(:,f(2,facechoice)) - v1;
    v1v3 = v(:,f(3,facechoice)) - v1;
    
    % uniform random samples inside parallelogram
    r = unifrnd(0,1,2,n);
    
    % map points on other half of parallelogram back to triangle
    mask = sum(r,1)>1; 
    r(:,mask) = 1-r(:,mask);
    
    % sample triangle
    r = v1 + bsxfun(@times,r(1,:),v1v2) + bsxfun(@times,r(2,:),v1v3);
end
