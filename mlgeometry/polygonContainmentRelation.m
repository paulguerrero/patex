% input: x1,y1 must be a polygon with a single contour,
%        x2,y2 can be multiply-connected polygons with holes
% output:
% 0: completely separate
% 1: 1 completely inside 2
% 2: 2 completely inside 1
% 3: intersecting
function rel = polygonContainmentRelation(x1,y1,x2,y2)
    % intersect boundingboxes
    bb1min = min([x1;y1],[],2);
    bb1max = max([x1;y1],[],2);
    
    bb2min = min([x2;y2],[],2);
    bb2max = max([x2;y2],[],2);
    
    if any(bb1min > bb2max) || any(bb1max < bb2min)
        rel = 0;
        return;
    end
    
    % get segments from nan-separated polygons
    nanmask = isnan(x2);
    if any(nanmask)
        naninds = find(nanmask);
        segs = zeros(4,numel(x2)-numel(naninds));
        segs(1,:) = x2(not(nanmask));
        segs(2,:) = y2(not(nanmask));
        inds = 1:numel(x2);
        inds([naninds,end+1]) = inds([1,naninds+1]);
        mask = true(size(inds));
        mask([1,naninds+1]) = false;
        segs(3,:) = x2(inds(mask));
        segs(4,:) = y2(inds(mask));
    else
        segs = [x2;y2;x2([2:end,1]);y2([2:end,1])];
    end
    
%     [sx2,sy2] = polysplit(x2,y2);    
%     segs = cell2mat(cellfun(@(x,y) [x;y;x([2:end,1]);y([2:end,1])],sx2',sy2','UniformOutput',false));
    
    intersects = linesegLinesegIntersection2D_mex(...
        [x1;y1],[x1([2:end,1]);y1([2:end,1])],...
        segs(1:2,:),segs(3:4,:));
    
    if any(intersects(:)>0)
        rel = 3;
    elseif inpolygon(x1(1),y1(1),x2,y2)
        rel = 1;
    elseif inpoly([x2(1),y2(1)],[x1;y1]')
        rel = 2;
    else
        rel = 0;
    end
end
