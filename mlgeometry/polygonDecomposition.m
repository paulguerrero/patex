% the first numel(x) vertices of cx and cy should be the same as x and y
% (without the nans)
function [cx,cy,faces] = polygonDecomposition(x,y,geolib)
    
    % decompose into outer boundary and holes
    [polycompx,polycompy] = polysplit(x,y);
    bx = polycompx{1};
    by = polycompy{1};
    hx = polycompx(2:end);
    hy = polycompy(2:end);
    clear polycompx polycompy;
    
    [bx,by] = poly2ccw(bx,by);
    if numel(bx) < 3
        error('degenerate boundary');
    end
    binds = 1:numel(bx);
    bindsbefore = circshift(binds,[0,1]);
    bindsafter = circshift(binds,[0,-1]);
%     bvertnormals = polylineNormals([bx;by],true);
    bedgenormals = polylineNormals([bx;by],true,false);
    bangles = polylineAngles([bx;by],true);
    hinds = cell(1,numel(hx));
    hindsbefore = cell(1,numel(hx));
    hindsafter = cell(1,numel(hx));
%     hvertnormals = cell(1,numel(hx));
    hedgenormals = cell(1,numel(hx));
    hangles = cell(1,numel(hx));
    offset = numel(binds);
    for i=1:numel(hx)
        [hx{i},hy{i}] = poly2cw(hx{i},hy{i});
        if numel(hx{i}) < 3
            error('degenerate hole');
        end
        hinds{i} = offset+1:offset+numel(hx{i});
        hindsbefore{i} = circshift(hinds{i},[0,1]);
        hindsafter{i} = circshift(hinds{i},[0,-1]);
%         hvertnormals{i} = polylineNormals([hx{i};hy{i}],true);
        hedgenormals{i} = polylineNormals([hx{i};hy{i}],true,false);
        hangles{i} = polylineAngles([hx{i};hy{i}],true);
        offset = offset + numel(hx{i});
    end
    x = [bx,cell2mat(hx)];
    y = [by,cell2mat(hy)];
    indsbefore = [bindsbefore,cell2mat(hindsbefore)];
    indsafter = [bindsafter,cell2mat(hindsafter)];
%     vertnormals = [bvertnormals,cell2mat(hvertnormals)];
    edgenormals = [bedgenormals,cell2mat(hedgenormals)];
    angles = [bangles,cell2mat(hangles)];
    
    clear bx by hx hy bindsbefore bindsafter hindsbefore hindsafter ...
          bvertnormals hvertnormals bedgenormals hedgenormals bangles hangles;
    
    % parameters (thresholds)
    polyextent = sqrt((max(x(binds))-min(x(binds))).^2 + (max(y(binds))-min(y(binds))).^2);
%     concthreshold = 0.15;%0.05;
    concthreshold = 0.2;%0.05;
    snapthresh = pi/16; % maximum angle deviation of split line to snap to a vertex
%     snappointthresh = pi/16; % minimum angle of a point to be the target of a snap
    samepointthresh = polyextent*0.00001;
%     splitqualitythresh = 0.2; % assuming it is normalized in [0,1]
    splitqualitythresh = 0.1; % assuming it is normalized in [0,1]
    
    conc = zeros(1,numel(x));
       
    % compute geodesic distances to convex hull
    hulldists = polygonConvexHullDistance(x(binds),y(binds),geolib);
    
    nhulldists = hulldists ./ polyextent;
    conc(binds) = computeConcavity(nhulldists,angles(binds));
    
    % compute concavity at each point
    for i=1:numel(hinds)
        % compute concavity of each point
        dists = squareform(pdist([x(hinds{i});y(hinds{i})]'));
        [antipdists,~] = max(dists,[],1);
%         angles = polylineAngles([x(hinds{i});y(hinds{i})],true);
        nantipdists = antipdists ./ polyextent;
        conc(hinds{i}) = computeConcavity(nantipdists,angles(hinds{i}));
    end
    
    concvertinds = find(conc >= concthreshold);
    
    if isempty(concvertinds)
        cx = x;
        cy = y;
        faces = {1:numel(x)};
        return;
    end
    
    % todo: find direction to closest point on boundary
    
    % compute split directions (todo: maybe include directions 90 deg. to
    % tangents)
    splittangentsbefore = [x(concvertinds)-x(indsbefore(concvertinds));y(concvertinds)-y(indsbefore(concvertinds))];
    splittangentsbefore = splittangentsbefore ./ repmat(sqrt(sum(splittangentsbefore.^2,1)),2,1);
    splittangentsafter = [x(concvertinds)-x(indsafter(concvertinds));y(concvertinds)-y(indsafter(concvertinds))];
    splittangentsafter = splittangentsafter ./ repmat(sqrt(sum(splittangentsafter.^2,1)),2,1);
    splitnormalsbefore = edgenormals(:,indsbefore(concvertinds));
    splitnormalsafter = edgenormals(:,concvertinds);

    
%     splitvertnormal = vertnormals(:,candidateinds);
%     splitdir = [splittangentsbefore;splittangentsafter];
%     splitdir = [splittangentsbefore;splittangentsafter;splitvertnormal];
    splitdir = [splittangentsbefore;splittangentsafter;splitnormalsbefore;splitnormalsafter];    
%     splitdir = [splittangentsbefore;splittangentsafter;splitnormalsbefore;splitnormalsafter;closestvertdirs];
    splitdir = reshape(splitdir,2,[]);
    splitstart = repmat([concvertinds;indsbefore(concvertinds);indsafter(concvertinds)],4,1);
%     splitstart = repmat([concvertinds;indsbefore(concvertinds);indsafter(concvertinds)],5,1);
    splitstart = reshape(splitstart,3,[]);
    
    
    
    %  find closest candidate index
    dists = squareform(pdist([x(concvertinds);y(concvertinds)]'));
    dists = dists + diag(inf(1,numel(concvertinds)));
    for i=1:numel(concvertinds)
        % remove distances to vertices directly adjacent (would result in
        % the split being collinear to an edge)
        lasti = mod(i-2,numel(concvertinds))+1;
        nexti = mod(i,numel(concvertinds))+1;
        if concvertinds(lasti) == indsbefore(concvertinds(i))
            dists(lasti,i) = inf;
            dists(i,lasti) = inf;
        end
        if concvertinds(nexti) == indsafter(concvertinds(i))
            dists(nexti,i) = inf;
            dists(i,nexti) = inf;
        end
    end
    if not(isempty(dists))
        [mindist,closestvind] = min(dists,[],1);
        mask = not(isinf(mindist));
        closestvertdirs = [x(concvertinds(closestvind(mask)))-x(concvertinds(mask));...
                           y(concvertinds(closestvind(mask)))-y(concvertinds(mask))];
        closestvertdirs = closestvertdirs ./ repmat(sqrt(sum(closestvertdirs.^2,1)),2,1);
        splitdir = [splitdir,closestvertdirs];
        splitstart = [splitstart,...
            [concvertinds(mask);indsbefore(concvertinds(mask));indsafter(concvertinds(mask))]];
    end
    
    % remove splits that point to the outside of the polygon
    isholevert = true(1,numel(x));
    isholevert(binds) = false;
    isholevert = isholevert(splitstart(1,:));
    
    tangentsbefore = [x(splitstart(2,:))-x(splitstart(1,:));y(splitstart(2,:))-y(splitstart(1,:))];
    tangentsafter = [x(splitstart(3,:))-x(splitstart(1,:));y(splitstart(3,:))-y(splitstart(1,:))];
    
    [anglesbefore,~] = cart2pol(tangentsbefore(1,:),tangentsbefore(2,:));
    [anglesafter,~] = cart2pol(tangentsafter(1,:),tangentsafter(2,:));
    [splitangles,~] = cart2pol(splitdir(1,:),splitdir(2,:));
    maxinsideangles = anglesbefore - anglesafter;
    mask = maxinsideangles < 0;
    maxinsideangles(mask) = 2*pi + maxinsideangles(mask);
    maxinsideangles = maxinsideangles - pi/32; % do not take splits almost collinear to the adjacent edges
    mask = maxinsideangles < 0;
    maxinsideangles(mask) = 2*pi + maxinsideangles(mask);
    
    splitangles = splitangles - anglesafter;
    mask = splitangles < 0;
    splitangles(mask) = 2*pi + splitangles(mask);
    
    splitangles(not(isholevert)) = splitangles(not(isholevert)) ./ maxinsideangles(not(isholevert));
    splitangles(isholevert) = (splitangles(isholevert)-maxinsideangles(isholevert))./(2*pi-maxinsideangles(isholevert));
    
    normangletol(not(isholevert)) = (pi/32)./maxinsideangles(not(isholevert));
    normangletol(isholevert) = (pi/32)./(2*pi-maxinsideangles(isholevert));
    outsidemask = splitangles <= normangletol | splitangles >= 1-normangletol;
    
%     normangletol = (pi/32)/(2*pi-maxinsideangles(isholevert));
%     outsidemask(isholevert) = splitangles(isholevert) <= normangletol | splitangles(isholevert) >= 1-normangletol;

    splitstart(:,outsidemask) = [];
    splitdir(:,outsidemask) = [];
% 
%     splitstart(:,outsidemask & not(isholevert)) = [];
%     splitdir(:,outsidemask & not(isholevert)) = [];
%     
%     splitstart(:,not(outsidemask) & isholevert) = [];
%     splitdir(:,not(outsidemask) & isholevert) = [];
    

    splitstartconc = conc(splitstart(1,:));
    
    clear splitvertnormal splittangentsbefore splittangentsafter closestvertdirs;
    
    % find intersection of split lines with polygon boundaries
    intersects = linesegLinesegIntersection2D_mex(...
        [x;y],[x(indsafter);y(indsafter)],...
        [x(splitstart(1,:));...
         y(splitstart(1,:))],...
        [x(splitstart(1,:))+splitdir(1,:).*(polyextent*2);...
         y(splitstart(1,:))+splitdir(2,:).*(polyextent*2)]);
    mask = intersects>0;
    
    error('todo: get the required info from new intersects format.');
    
    intersects.intNormalizedDistance2To1(not(mask)) = nan;
    ownseginds = sub2ind(size(intersects.intNormalizedDistance2To1),indsbefore(splitstart(1,:)),1:size(splitstart,2));
    ownseginds = [ownseginds,sub2ind(size(intersects.intNormalizedDistance2To1),splitstart(1,:),1:size(splitstart,2))];
    intersects.intNormalizedDistance2To1(ownseginds) = nan;
    [firstintersectdist,firstintersectsegind] = min(intersects.intNormalizedDistance2To1,[],1);
    firstintersectsegf = intersects.intNormalizedDistance1To2(...
        sub2ind(size(intersects.intNormalizedDistance1To2),firstintersectsegind,1:numel(firstintersectsegind)));
    firstintersectdist = firstintersectdist .* (polyextent*2);
    
    % remove split lines that don't hit the boundary
    mask = isnan(firstintersectdist);
    splitstart(:,mask) = [];
    splitdir(:,mask) = [];
    splitstartconc(mask) = [];
%     firstintersectdist(mask) = [];
    firstintersectsegind(mask) = [];
    firstintersectsegf(mask) = [];
%     splittype(mask) = [];
    
    intersectends = [-firstintersectsegf;firstintersectsegind;indsafter(firstintersectsegind)];    

    clear firstintersectdist;
    
    
    % search for best snapping vertices of each split line
    snapvertinds = concvertinds;
    tosnapindmap = zeros(1,size(splitstart,2));
    tosnapindmap(snapvertinds) = 1:numel(snapvertinds);
    [dists,f,~,~] = pointLinesegDistance2D(x(snapvertinds),y(snapvertinds),...
        x(splitstart(1,:)),y(splitstart(1,:)),...
        x(splitstart(1,:))+splitdir(1,:).*(polyextent*2),...
        y(splitstart(1,:))+splitdir(2,:).*(polyextent*2));
    inds = find(tosnapindmap(splitstart(1,:)) > 0);
    ownvertinds = sub2ind(size(dists),tosnapindmap(splitstart(1,inds)),inds);
    dists(ownvertinds) = nan;
    f(f==0) = nan;
    f = f.*(polyextent*2);
    angledev = atan(dists./f);
    f(angledev > snapthresh) = nan;
    [bestsnapdist,bestsnappointind] = min(f,[],1);
    bestsnappointind = snapvertinds(bestsnappointind);
    
    clear splitdir;
    
    % find split lines to a snap point that do not intersect the polygon
    % boundary
    snapinds = find(not(isnan(bestsnapdist)));
    snapmask = false(1,size(splitstart,2));
    snapends = [];
    if not(isempty(snapinds))
        intersects = linesegLinesegIntersection2D_mex(...
            [x;y],[x(indsafter);y(indsafter)],...
            [x(splitstart(1,snapinds));...
             y(splitstart(1,snapinds))],...
            [x(bestsnappointind(snapinds));...
             y(bestsnappointind(snapinds))]);
        mask = intersects>0;
        
        error('todo: get the required info from new intersects format.');
        
        intersects.intNormalizedDistance2To1(not(mask)) = nan;
        % two segments at start of split line
        ownseginds = sub2ind(size(intersects.intAdjacencyMatrix),indsbefore(splitstart(1,snapinds)),1:numel(snapinds));
        ownseginds = [ownseginds,sub2ind(size(intersects.intAdjacencyMatrix),splitstart(1,snapinds),1:numel(snapinds))];
        % two segments at end of split line
        ownseginds = [ownseginds,sub2ind(size(intersects.intAdjacencyMatrix),indsbefore(bestsnappointind(snapinds)),1:numel(snapinds))];
        ownseginds = [ownseginds,sub2ind(size(intersects.intAdjacencyMatrix),bestsnappointind(snapinds),1:numel(snapinds))];
        intersects.intAdjacencyMatrix(ownseginds) = 0;
        freepathinds = not(any(logical(intersects.intAdjacencyMatrix),1));
        snapinds = snapinds(freepathinds);
    end
    
    % evaluate quality of both splits (snap and intersection with segment)
    % and take the one with higher quality
    if not(isempty(snapinds))
        snapends = [bestsnappointind(snapinds);indsbefore(bestsnappointind(snapinds));indsafter(bestsnappointind(snapinds))];
        
        endpointquality = evaluateSplit(...
            splitstart(:,[snapinds,snapinds]),...
            [snapends,intersectends(:,snapinds)],...
            splitstartconc([snapinds,snapinds]),...
            [conc(bestsnappointind(snapinds)),zeros(1,numel(snapinds))],...
            x,y);
        snapendquality = endpointquality(1:numel(snapinds));
        segendquality = endpointquality(numel(snapinds)+1:end);

        usesnapmask = snapendquality >= segendquality;

        % if intersection is the same point as snap point, use the snap point
        snapendpoints = [x(snapends(1,:));y(snapends(1,:))];
        segendpoints =  [x(intersectends(2,snapinds)) .* (1+intersectends(1,snapinds)) + x(intersectends(3,snapinds)) .* -intersectends(1,snapinds);...
                         y(intersectends(2,snapinds)) .* (1+intersectends(1,snapinds)) + y(intersectends(3,snapinds)) .* -intersectends(1,snapinds)];
        samepointmask = sqrt(sum((snapendpoints - segendpoints).^2,1)) < samepointthresh;
        usesnapmask = usesnapmask | samepointmask;
        snapmask(snapinds(usesnapmask)) = true;
    end
    
    % use either snap points or intersections with the boundary as split
    % end points
    if sum(snapmask) > 0
%         splitend(:,mask) = snapends(:,snapendquality >= segendquality);
        splitend(:,snapmask) = snapends(:,usesnapmask);
%         [bestsnappointind(mask);indsbefore(bestsnappointind(mask));indsafter(bestsnappointind(mask))];
        splitendconc(:,snapmask) = conc(splitend(1,snapmask));
        splitendpoints(:,snapmask) = [x(splitend(1,snapmask));y(splitend(1,snapmask))];
    end
    notsnapmask = not(snapmask);
    if sum(notsnapmask) > 0
%         splitend(:,mask) = [-firstintersectsegf(mask);firstintersectsegind(mask);indsafter(firstintersectsegind(mask))];
        splitend(:,notsnapmask) = intersectends(:,notsnapmask);
        splitendconc(:,notsnapmask) = 0;
        splitendpoints(:,notsnapmask) = ...
            [x(splitend(2,notsnapmask)) .* (1+splitend(1,notsnapmask)) + ...
             x(splitend(3,notsnapmask)) .* -splitend(1,notsnapmask);...
             y(splitend(2,notsnapmask)) .* (1+splitend(1,notsnapmask)) + ...
             y(splitend(3,notsnapmask)) .* -splitend(1,notsnapmask)];
    end
    
    clear bestsnapdist bestsnappointind firstintersectdist firstintersectsegind firstintersectsegf;
        
    % evaluate split line quality
    splitquality = evaluateSplit(splitstart,splitend,splitstartconc,splitendconc,x,y);
    clear splitstartconc splitendconc 
    
    % remove split lines that have a very low quality
    mask = splitquality < splitqualitythresh;
    splitstart(:,mask) = [];
    splitend(:,mask) = [];
    splitquality(mask) = [];
    splitendpoints(:,mask) = [];
    
    % sort splits by quality (highest first)
    [~,perm] = sort(splitquality,'descend');
    splitstart = splitstart(:,perm);
    splitend = splitend(:,perm);
    splitquality = splitquality(perm);
    splitendpoints = splitendpoints(:,perm);
    
    % remove duplicate split lines (remove the duplicate with lower quality)
    dists = pdist2([x(splitstart(1,:));y(splitstart(1,:))]',splitendpoints');
    dists2 = pdist2([x(splitstart(1,:));y(splitstart(1,:))]',[x(splitstart(1,:));y(splitstart(1,:))]');
    dists3 = pdist2(splitendpoints',splitendpoints');
    [ind1,ind2] = find(...
        dists'+dists < samepointthresh*2 | ...
        (dists2 < samepointthresh & dists3 < samepointthresh)); % endpoint<->startpoint and startpoint<->endpoint < threshold
    mask = ind2 <= ind1;
    ind1(mask) = [];
    ind2(mask) = [];
    mask = splitquality(ind1) <= splitquality(ind2);
    duplicateinds = [ind1(mask)',ind2(not(mask))'];
    splitstart(:,duplicateinds) = [];
    splitend(:,duplicateinds) = [];
    splitquality(duplicateinds) = [];
    splitendpoints(:,duplicateinds) = [];
    
    
    % greedily remove those splitlines that don't conserve min. angle to
    % other splitlines of the same vertex
    % minimum angle to all other splitlines of the same vertex
    vsplits = cell(1,numel(x));
    for i=1:size(splitstart,2)
        vsplits{splitstart(1,i)}(end+1) = i;
        if splitend(1,i) > 0
            vsplits{splitend(1,i)}(end+1) = i;
        end
    end
    
    cosminangle = cos(pi/4+pi/16);
    concvertmask = false(1,numel(x));
    concvertmask(concvertinds) = true;
    removemask = false(1,size(splitstart,2));
    
    splitdir = splitendpoints - [x(splitstart(1,:));y(splitstart(1,:))];
    splitdir = splitdir ./ repmat(sqrt(sum(splitdir.^2,1)),2,1);
    
    for i=size(splitstart,2):-1:1
        
        startvertsplitinds = vsplits{splitstart(1,i)};
        startvertsplitinds(startvertsplitinds == i) = [];
        startvertsplitinds(removemask(startvertsplitinds)) = [];
        startok = concvertmask(splitstart(1,i)) || startvertsplitinds > 0;
        
        if splitend(1,i) <= 0
            endvertsplitinds = [];
        else
            endvertsplitinds = vsplits{splitend(1,i)};
        end
        endvertsplitinds(endvertsplitinds == i) = [];
        endvertsplitinds(removemask(endvertsplitinds)) = [];
        endok = splitend(1,i) <= 0 ||  concvertmask(splitend(1,i)) || endvertsplitinds > 0;
        
        
        if startok && endok
            startvertsplitleaving = splitstart(1,startvertsplitinds) == splitstart(1,i);
            if splitend(1,i) <= 0
                endvertsplitleaving = zeros(1,0);
            else
                endvertsplitleaving = splitstart(1,endvertsplitinds) == splitend(1,i);
            end
            startvertsplitleaving = (startvertsplitleaving-0.5).*2;
            endvertsplitleaving = (endvertsplitleaving-0.5).*2;
            
            if any(splitdir(1,i)*splitdir(1,startvertsplitinds).*startvertsplitleaving + ...
                   splitdir(2,i)*splitdir(2,startvertsplitinds).*startvertsplitleaving > cosminangle) || ...
               any(-splitdir(1,i)*splitdir(1,endvertsplitinds).*endvertsplitleaving + ...
                   -splitdir(2,i)*splitdir(2,endvertsplitinds).*endvertsplitleaving > cosminangle)
                
                removemask(i) = true;
            end
        end
    end
    
%     splitvertinds = unique([splitstart(1,:),splitend(1,splitend(1,:)>0)]);
%     
%     for i=1:numel(splitvertinds)
%         indsfrom = find(splitstart(1,:) == splitvertinds(i));
%         indsto = find(splitend(1,:) == splitvertinds(i));
%         inds = [indsfrom,indsto];
%         if numel(inds) > 1
% %             isstartvert = not(isempty(indsfrom));
%             
%             sdir = [splitendpoints(:,indsfrom) - [x(splitstart(1,indsfrom));y(splitstart(1,indsfrom))],...
%                    [x(splitstart(1,indsto));y(splitstart(1,indsto))] - splitendpoints(:,indsto)];
%             sdir = sdir ./ repmat(sqrt(sum(sdir.^2,1)),2,1);
% 
%             squality = splitquality(inds);
%             [~,perm] = sort(squality,'ascend');
%             inds = inds(perm);
%             sdir = sdir(:,perm);
%             for j=1:numel(inds)-1
%                 if any(sdir(1,j)*sdir(1,j+1:end) + ...
%                        sdir(2,j)*sdir(2,j+1:end) > cosminangle)
%                     removemask(inds(j)) = true;
%                 end
%             end
%         end
%     end
    splitstart(:,removemask) = [];
    splitend(:,removemask) = [];
%     splitquality(removemask) = [];
    splitendpoints(:,removemask) = [];

    clear splitquality splitendpoints;
    
    % split the polygon along the split lines
    e1 = 1:numel(x);
    e2 = indsafter(e1);
    mask = splitend(1,:) >= 1;
    ei2(mask) = splitend(1,mask);
    f2(mask) = 0;
    ei2(not(mask)) = splitend(2,not(mask));
    f2(not(mask)) = -splitend(1,not(mask));
    ei1 = splitstart(1,:);
    f1 = zeros(1,size(splitstart,2));
    [cx,cy,faces] = polygonCut(x,y,e1,e2,ei1,f1,ei2,f2);
    
	% todo: remove long splits compared to sqrt(cell area) where the two
	% merged cells result in a convex cell (or nearly convex, with a small
	% concave value)
end

function conc = computeConcavity(nhulldists,angles)
    conc = (log(nhulldists.*1000+1)./log(1001)) .* max(0,angles./-pi);
end

function quality = evaluateSplit(splitstart,splitend,splitstartconc,splitendconc,x,y)
    
    if isempty(splitstart)
        quality = [];
        return;
    end

    mask = splitend(1,:) >= 1;
    if sum(mask) > 0
        splitendpoints(:,mask) = [x(splitend(1,mask));y(splitend(1,mask))];
    end
    mask = not(mask);
    if sum(mask) > 0
        splitendpoints(:,mask) =  [x(splitend(2,mask)) .* (1+splitend(1,mask)) + x(splitend(3,mask)) .* -splitend(1,mask);...
                                   y(splitend(2,mask)) .* (1+splitend(1,mask)) + y(splitend(3,mask)) .* -splitend(1,mask)];
    end
        
    splitdir = splitendpoints - [x(splitstart(1,:));y(splitstart(1,:))];
    splitlength = sqrt(sum(splitdir.^2,1));
    splitdir = splitdir ./ repmat(splitlength,2,1);    
    
    % angle weights at start of split line
    splittangentbefore = [x(splitstart(2,:))-x(splitstart(1,:));y(splitstart(2,:))-y(splitstart(1,:))];
    splittangentbefore = splittangentbefore ./ repmat(sqrt(sum(splittangentbefore.^2,1)),2,1);
    splittangentafter = [x(splitstart(3,:))-x(splitstart(1,:));y(splitstart(3,:))-y(splitstart(1,:))];
    splittangentafter = splittangentafter ./ repmat(sqrt(sum(splittangentafter.^2,1)),2,1);

    startanglequality = evaluateSplitAngle(splitdir,splittangentbefore,splittangentafter);

    % angle weights at end of split line
    splitdir = -splitdir;
    splittangentbefore = [x(splitend(2,:))-splitendpoints(1,:);y(splitend(2,:))-splitendpoints(2,:)];
    splittangentbefore = splittangentbefore ./ repmat(sqrt(sum(splittangentbefore.^2,1)),2,1);
    splittangentafter = [x(splitend(3,:))-splitendpoints(1,:);y(splitend(3,:))-splitendpoints(2,:)];
    splittangentafter = splittangentafter ./ repmat(sqrt(sum(splittangentafter.^2,1)),2,1);

    endanglequality = evaluateSplitAngle(splitdir,splittangentbefore,splittangentafter);

%     tangentbeforeweight = abs((acos(max(0,min(1,abs(dot(splitdir,splittangentbefore)))))/(pi/2) - 0.5) * 2); % 0 at 45 degrees 1 at multiples of 90 degrees
%     tangentafterweight = abs((acos(max(0,min(1,abs(dot(splitdir,splittangentafter)))))/(pi/2) - 0.5) * 2);    
    
    % combined quality
    meansplitlen = mean(splitlength);
    quality = sqrt(startanglequality .* endanglequality) .* (1./(splitlength./meansplitlen)) .* sqrt(splitstartconc .* max(splitstartconc*0.75,splitendconc));
%     quality = sqrt(startanglequality .* endanglequality) .* (1./(splitlength./meansplitlen)) .* max(splitstartconc,splitendconc);
end

function quality = evaluateSplitAngle(splitdir,splittangentbefore,splittangentafter)
    
    % 0..1 for minangle 0..90 deg. 1 for over 90 deg. bonus for 90 deg. or
    % 180 deg. alignment with tangents

    bonustantolerance = pi/16;
    bonusnormtolerance = pi/8;
    bonustanmagnitude = 0.3;
    bonusnormmagnitude = 0.1;

    tbeforeangle = acos(max(-1,min(1,dot(splitdir,splittangentbefore))));
    tafterangle = acos(max(-1,min(1,dot(splitdir,splittangentafter))));
    minangle = min(tbeforeangle,tafterangle);
    tbeforealignmentbonus = max(0,max((bonusnormtolerance-abs(tbeforeangle - pi/2)) .* (bonusnormmagnitude/bonusnormtolerance),...
                                      (bonustantolerance-abs(tbeforeangle - pi)) .* (bonustanmagnitude/bonustantolerance)));
    tafteralignmentbonus = max(0,max((bonusnormtolerance-abs(tafterangle - pi/2)) .* (bonusnormmagnitude/bonusnormtolerance),...
                                     (bonustantolerance-abs(tafterangle - pi)) .* (bonustanmagnitude/bonustantolerance)));
                                 
	
	quality = min(1,minangle./(pi/2)) + max(tbeforealignmentbonus,tafteralignmentbonus);
    quality = quality ./ (1+max(bonustanmagnitude,bonusnormmagnitude)); % normalize
end

