% order: inverse translate, inverse rotate, inverse scale, mirror (= inverse mirror)
% scale: 2 x n or 2 x 1 matrix of 2-dimensional scale vectors
% angle: 1 x n or 1 x 1 matrix of angles (in radians)
% trans: 2 x n or 2 x 1 matrix of 2-dimensional translations
% optional: mirror: 1 x n or 1 x 1 matrix of mirroring flags
% (mirroring is about x axis: y = -y)
% returns 3 x 3 x n 2D transformation matrices
function m = transformMat2Dinv(scale, angle, trans, mirror)
    
    scale = reshape(scale,2,1,[]);
    angle = reshape(angle,1,1,[]);
    trans = reshape(trans,2,1,[]);
    
    if nargin >= 4
        mirror = reshape(mirror,1,1,[]);
        mir = (logical(mirror).*-2+1);
    else
        mir = ones(1,1,size(scale,3));
    end
    
    transmat = repmat(eye(3,3),1,1,size(scale,3));
    transmat(1,3,:) = -trans(1,1,:);
    transmat(2,3,:) = -trans(2,1,:);
    
    % matrix multiplication written out (so i can multiply n matrices with n vectors)
    cosangle = cos(-angle);
    sinangle = sin(-angle);
    invscale = 1./scale;
    m = [[     invscale(1,:,:).*cosangle ,      invscale(1,:,:).*-sinangle;...
          mir.*invscale(2,:,:).*sinangle , mir.*invscale(2,:,:).* cosangle], zeros(2,1,size(angle,3));...
         zeros(1,2,size(scale,3)), ones(1,1,size(scale,3))];
     
	for i=1:size(scale,3)
        m(:,:,i) =  m(:,:,i) * transmat(:,:,i);
	end
end
