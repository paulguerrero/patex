% BEZIER3 Evaluate a cubic bezier spline.
%   val = BEZIER3(p1,p2,p3,p4,t) evaluates the cubic bezier spline
%   defined by the 4 control points p1,p2,p3,p4 at the arclength
%   parameters t.
function val = bezier3(p1,p2,p3,p4,t)
    
    if not(isvector(p1)) || not(isvector(t))
        error('control points and arclength parameters must be vectors');
    end
    
    if min(t) < 0 || max(t) > 1
        error('t must be in [0,1]');
    end
    
    if iscolumn(t)
        t = t';
    end
    
    if isrow(p1)
        p1 = p1';
        p2 = p2';
        p3 = p3';
        p4 = p4';
    end
    
    val =  p1 * ((1-t).^3) + p2 * (3.*t.*(1-t).^2) + p3 * (3.*t.^2.*(1-t)) + p4 * (t.^3);
end