% find intersections, create additional vertices at intersections
% and create a graph of all vertices and connecting edges
function [verts,edges] = polylineSet2Graph(poly,closed,vertexsnapdist,edgesnapdist)
	
    if not(iscell(poly))
        poly = {poly};
    end
    
    if nargin < 3
        vertexsnapdist = 0;
    end
    
    if nargin < 4
        edgesnapdist = vertexsnapdist;
    end
    
    % remove polylines with just one vertex
    poly(cellfun(@(x) size(x,2) < 2,poly)) = [];
    
    % check for empty graph
    if isempty(poly)
        verts = zeros(2,0);
        edges = zeros(2,0);
        return;
    end
    
    % create edges
    edges = [];
    verts = [];
    for i=1:numel(poly)
        if closed(i)
            edges = [edges,size(verts,2)+[1:size(poly{i},2);[2:size(poly{i},2),1]]]; %#ok<AGROW>
        else
            edges = [edges,size(verts,2)+[1:size(poly{i},2)-1;2:size(poly{i},2)]]; %#ok<AGROW>
        end
        verts = [verts,poly{i}]; %#ok<AGROW>
    end
    
    % resolve edge intersections
    [verts,edges] = intersectEmbeddedGraphEdges(verts,edges,vertexsnapdist,edgesnapdist);
end
