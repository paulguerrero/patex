% adjacency: adjacency matrix of directed acyclic graph, where nonzero
% entries (i,j) indicate an edge from i to j (and i is considered the
% parent of j)
% nodeinds: all ancestors of these nodes are returned
function ainds = graphAncestors(adjacency,nodeinds)
    nodemask = logical(sparse(ones(1,numel(nodeinds)),nodeinds,ones(1,numel(nodeinds)),1,size(adjacency,1)));
    amask = logical(sparse(1,size(adjacency,1)));
    
    while any(nodemask)
        nodemask = logical(abs(adjacency)*nodemask);
        
        nodemask(amask) = false; % has already been visited
        
        amask = amask | nodemask;
    end
    
    ainds = find(amask);
end
