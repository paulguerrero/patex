function [verts,faces] = meshPolargrid2D(apos,rpos,centerpos,minres)

    if nargin < 3 || size(centerpos,1) == 0
        centerpos = [0;0];
    end    

    if nargin < 4 || isempty(minres)
        minres = 64;
    end
    
    resf = ceil(minres/numel(apos));
    if isempty(centerpos)
        verts = zeros(2,0);
        faces = zeros((resf+1)*2,0);
        return;
    end
    
    binadist = smod(diff(apos),-pi,pi);
    if any(binadist - binadist(1) > 0.000001)
        error('Angular positions must be distributed uniformly on the circle');
    end

    % create vertices
    res = numel(apos) * resf;
    verts = zeros(2,res*numel(rpos));
    vindoffset = 0;
    for i=1:numel(rpos)
        % create vertices in one edge ring
        % (vertices are ordered counterclockwise, starting at (radius,0))
        % first bin starts at aedges(end), so rotate vertices by that
        % amount
        vinds = vindoffset+1 : vindoffset+res;
        [verts(1,vinds),verts(2,vinds)] = polygonCircle(...
            0,0,...
            rpos(i),res,'samplecount',apos(1));
        vindoffset = vindoffset+res; 
    end

    % create faces
    faces = zeros((resf+1)*2,numel(apos)*(numel(rpos)-1));
    vindoffset = 0;
    faceind = 1;
    for i=1:numel(rpos)-1
        for j=1:numel(apos)
            faces(:,faceind) = [...
                smod(vindoffset+1:vindoffset+resf+1,(i-1)*res+1,i*res+1),...
                smod(vindoffset+res+resf+1:-1:vindoffset+res+1,i*res+1,(i+1)*res+1)];
            vindoffset = vindoffset + resf;
            faceind = faceind + 1;
        end
    end
    
    % move vertices to positions of all descriptors
    if size(centerpos,2) > 1
        ndescverts = size(verts,2);
        verts = [...
            reshape(bsxfun(@plus,verts(1,:)',centerpos(1,:)),1,[]);...
            reshape(bsxfun(@plus,verts(2,:)',centerpos(2,:)),1,[])];
        faces = reshape(...
            bsxfun(@plus,faces,reshape(0:ndescverts:ndescverts*size(centerpos,2)-1,1,1,[])),...
            size(faces,1),size(faces,2)*size(centerpos,2));
    elseif size(centerpos,2) == 1
        verts(1,:) = verts(1,:) + centerpos(1);
        verts(2,:) = verts(2,:) + centerpos(2);
    end
    
end