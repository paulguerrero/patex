function poly = polylineSetWinding(poly,c,winding)
    arrayinput = false;
    if not(iscell(poly))
        arrayinput = true;
        poly = mat2cell(poly,size(poly,1),size(poly,2));
    end
    
    if strcmp(winding,'ccw')
        ccw = true;
    elseif strcmp(winding,'cw')
        ccw = false;
    else
        error('winding order must be either ''ccw'' or ''cw''.');
    end
   
    % reverse polylines with points in the wrong order
    for i=1:numel(poly)
        if c(i)
            segments = diff(poly{i}(:,[1:end,1]),1,2);
        else
            segments = diff(poly{i},1,2);
        end
        seglen = sqrt(segments(1,:).^2+segments(2,:).^2);
        mask = seglen > 0;
        segments = segments(:,mask);
        seglen = seglen(mask);
%         if c(i)
%             segments = [segments,segments(:,1)]; %#ok<AGROW>
%             seglen = [seglen,seglen(1)]; %#ok<AGROW>
%         end
        segments = segments ./ repmat(seglen,2,1);
        if c(i)
            a = acos(max(-1,min(1,dot(segments,segments(:,[2:end,1]),1))));
            orth = [-segments(2,:);segments(1,:)];
            s = sign(max(-1,min(1,dot(segments(:,[2:end,1]),orth,1))));
        else
            a = acos(max(-1,min(1,dot(segments(:,1:end-1),segments(:,2:end),1))));
            orth = [-segments(2,1:end-1);segments(1,1:end-1)];
            s = sign(max(-1,min(1,dot(segments(:,2:end),orth,1))));
        end
        a = a .* s; 
        
        if ccw && sum(a) < 0 || not(ccw) && sum(a) >= 0
            poly{i} = poly{i}(:,end:-1:1);
        end
    end
    
    if arrayinput
        poly = cell2mat(poly);
    end
end
