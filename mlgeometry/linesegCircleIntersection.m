function [ip1,ip2,t1,t2] = linesegCircleIntersection(p1,p2,center,radius)

    if size(p1,2) ~= 1
        error('only one line segment at a time supported');
    end
    
    ip1 = nan(2,size(center,2));
    ip2 = nan(2,size(center,2));
    
    t1 = nan(1,size(center,2));
    t2 = nan(1,size(center,2));
    
    if isempty(center)
        return;
    end
    
    segvec = p2-p1;
    seglen = sqrt(sum(segvec.^2,1));        
    
    if seglen == 0
        % p1 == p2, line segment is actually a point,
        % can only intersect if the point lies exactly on the circle boundary
        mask = sqrt(sum((p1-center).^2,1)) == radius;
        t1(mask) = 0;
        ip1(1,mask) = p1(1);
        ip1(2,mask) = p1(2);
        return;
    end
    
    segnormal = [-segvec(2),segvec(1)]./seglen;
    
    % projection of vector to from center to closest point on line to segment normal
    % = projection of vector from center to any point on line to segment normal
    p1c = bsxfun(@minus,p1,center);
    c2cpproj = p1c(1,:).*segnormal(1) + p1c(2,:).*segnormal(2);
    imask = abs(c2cpproj) < radius;
    c2cpproj = c2cpproj(imask);
    
    if not(any(imask))
        return;
    end
    
    % half the length of the line interval inside the circle
    lineinterval = sqrt(radius(imask).^2-c2cpproj(imask).^2);
    % closest point on line
    cp = [center(1,imask) + segnormal(1).*c2cpproj;...
          center(2,imask) + segnormal(2).*c2cpproj];
	% vector closest point to border of circle (in direction to second point)
    cp2border = [lineinterval.*(segvec(1)/seglen);...
                 lineinterval.*(segvec(2)/seglen)];
    
    % two intersection points of the line with the circle
	ip1(:,imask) = cp+cp2border;
    ip2(:,imask) = cp-cp2border;
    
    % position of the intersection points on the line segment
    % (0 at p0 ... 1 at p1)
    t1 = ((ip1(1,:)-p1(1)).*segvec(1)+(ip1(2,:)-p1(2)).*segvec(2))./seglen.^2;
    t2 = ((ip2(1,:)-p1(1)).*segvec(1)+(ip2(2,:)-p1(2)).*segvec(2))./seglen.^2;
    
    ip1(:,t1<0 | t1>1) = nan;
    ip2(:,t2<0 | t2>1) = nan;
end
