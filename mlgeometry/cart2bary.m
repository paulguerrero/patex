function [b1,b2,b3] = cart2bary(x,y,x1,x2,x3,y1,y2,y3)
    d = ((y2-y3).*(x1-x3) + (x3-x2).*(y1-y3));
    b1 = ((y2-y3).*(x-x3) + (x3-x2).*(y-y3)) ./ d;
    b2 = ((y3-y1).*(x-x3) + (x1-x3).*(y-y3)) ./ d;
    b3 = 1 - (b1+b2);
end
