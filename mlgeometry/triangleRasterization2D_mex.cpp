#include "mex.h"

#include <math.h>
#include <algorithm>

void triangleRasterization2D_mex(const double* p1, const double* p2, const double* p3,
                                 const double* v1, const double* v2, const double* v3, const double* cv, bool singleval,
                                 const mwSize& ntri, const mwSize& w, const mwSize& h, const mwSize& d,
                                 double* I)
{
    
    // p1,p2,p3 should now be in pixel coordinates from (1,1) to (w,h)
    
    mwSize npix = w*h;
    
    const double* p1p = p1;
    const double* p2p = p2;
    const double* p3p = p3;
    const double* v1p = v1;
    const double* v2p = v2;
    const double* v3p = v3;
    const double* cvp = cv;
    for (mwSize i=0; i<ntri; i++) {
        
        // get bounding box of triangle
        mwIndex minx = std::max((mwIndex)1,static_cast<mwIndex>(
                floor(std::min(std::min(p1p[0],p2p[0]),p3p[0]))));
        mwIndex miny = std::max((mwIndex)1,static_cast<mwIndex>(
                floor(std::min(std::min(p1p[1],p2p[1]),p3p[1]))));

        mwIndex maxx = std::min(w,static_cast<mwIndex>(
                ceil(std::max(std::max(p1p[0],p2p[0]),p3p[0]))));
        mwIndex maxy = std::min(h,static_cast<mwIndex>(
                ceil(std::max(std::max(p1p[1],p2p[1]),p3p[1]))));
        
        // over all pixels in the boundingbox
        double sx,sy;
        double b1,b2,b3;
        double x23,x3s,y32,y3s,x31;
        double denom;
        for (mwIndex j=minx; j<=maxx; j++) {
            for (mwIndex k=miny; k<=maxy; k++) {

                // get barycentric coordinates
                sx = static_cast<double>(j);
                sy = static_cast<double>(k);
                
                x23 = p3p[0]-p2p[0];
                x3s = sx - p3p[0];
                y32 = p2p[1]-p3p[1];
                y3s = sy - p3p[1];
                x31 = p1p[0]-p3p[0];
                
                denom = y32*x31 + x23*(p1p[1]-p3p[1]);
                b1 = (y32*x3s + x23*y3s) / denom;
                b2 = ((p3p[1]-p1p[1])*x3s + x31*y3s) / denom;
                b3 = 1-(b1+b2);
                
                // skip pixels outside the triangle
                if (b1 < 0.0 || b1 > 1.0 ||
                    b2 < 0.0 || b2 > 1.0 ||
                    b3 < 0.0 || b3 > 1.0) {
                    continue;
                }
                
                // set pixels in image to interpolated values
                double* Ip = I+((j-1)*h+(k-1));
                if (cv == NULL) {
                    // interpolated values
                    for (mwIndex l=0; l<d; l++) {
                        *Ip = b1*v1p[l] + b2*v2p[l] + b3*v3p[l];
                        Ip += npix;
                    }
                } else {
                    // constant values
                    for (mwIndex l=0; l<d; l++) {
                        *Ip = cvp[l];
                        Ip += npix;
                    }
                }
            }
        }
                
        p1p += 2;
        p2p += 2;
        p3p += 2;
        
        v1p += d;
        v2p += d;
        v3p += d;
        
        if (!singleval) {
            cvp += d;
        }
    }
}

/* the gateway function */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    mwIndex i;
    
    double* p1 = NULL;
    double* p2 = NULL;
    double* p3 = NULL;
    double* v1 = NULL;
    double* v2 = NULL;
    double* v3 = NULL;
    double* cv = NULL;
    mwSize ntri;
    mwSize w;
    mwSize h;
    mwSize d;
    double* I = NULL;
    
    bool singleval = false;
    
    if (nrhs != 5 && nrhs != 7) {
        mexErrMsgTxt("Five or seven inputs required.");
    }
    
    int inputind = 0;
    
    // I
    h = mxGetM(prhs[inputind]);
    w = mxGetN(prhs[inputind]);
    if (mxGetNumberOfDimensions(prhs[inputind]) == 2) {
        d = 1;
    } else if (mxGetNumberOfDimensions(prhs[inputind]) == 3) {
        d = mxGetDimensions(prhs[inputind])[2];
    } else {
        mexErrMsgTxt("Invalid format for image.");
    }
    I = static_cast<double*>(mxGetPr(prhs[inputind]));
    inputind++;

    // p1
    ntri = mxGetN(prhs[inputind]);
    if (mxGetNumberOfDimensions(prhs[inputind]) != 2 || mxGetM(prhs[inputind]) != 2) {
        mexErrMsgTxt("Invalid format for triangle vertices.");
    }
    p1 = static_cast<double*>(mxGetPr(prhs[inputind]));
    inputind++;
    
    // p2
    if (mxGetNumberOfDimensions(prhs[inputind]) != 2 || mxGetM(prhs[inputind]) != 2 || mxGetN(prhs[inputind]) != ntri) {
        mexErrMsgTxt("Invalid format for triangle vertices.");
    }
    p2 = static_cast<double*>(mxGetPr(prhs[inputind]));
    inputind++;
    
    // p3
    if (mxGetNumberOfDimensions(prhs[inputind]) != 2 || mxGetM(prhs[inputind]) != 2 || mxGetN(prhs[inputind]) != ntri) {
        mexErrMsgTxt("Invalid format for triangle vertices.");
    }
    p3 = static_cast<double*>(mxGetPr(prhs[inputind]));
    inputind++;
    
    if (nrhs == 5) {
        
        // cv
        d = mxGetM(prhs[inputind]);
        if (mxGetNumberOfDimensions(prhs[inputind]) != 2) {
            mexErrMsgTxt("Invalid format for triangle vertex values.");
        }
        if (mxGetN(prhs[inputind]) == ntri) {
            singleval = false;
        } else if (mxGetN(prhs[inputind]) ==  1) {
            singleval = true;
        } else {
            mexErrMsgTxt("Invalid format for triangle vertex values.");
        }
        cv = static_cast<double*>(mxGetPr(prhs[inputind]));
        inputind++;
        
    } else if (nrhs == 7) {
        
        // v1
        d = mxGetM(prhs[inputind]);
        if (mxGetNumberOfDimensions(prhs[inputind]) != 2 || mxGetN(prhs[inputind]) != ntri) {
            mexErrMsgTxt("Invalid format for triangle vertex values.");
        }
        v1 = static_cast<double*>(mxGetPr(prhs[inputind]));
        inputind++;

        // v2
        if (mxGetNumberOfDimensions(prhs[inputind]) != 2 || mxGetM(prhs[inputind]) != d || mxGetN(prhs[inputind]) != ntri) {
            mexErrMsgTxt("Invalid format for triangle vertex values.");
        }
        v2 = static_cast<double*>(mxGetPr(prhs[inputind]));
        inputind++;

        // v3
        if (mxGetNumberOfDimensions(prhs[inputind]) != 2 || mxGetM(prhs[inputind]) != d || mxGetN(prhs[inputind]) != ntri) {
            mexErrMsgTxt("Invalid format for triangle vertex values.");
        }
        v3 = static_cast<double*>(mxGetPr(prhs[inputind]));
        inputind++;

    }
    
    //  check for proper number of arguments
    if(nlhs != 0) 
        mexErrMsgTxt("No outputs, the first input variable is overwritten.");
    
    triangleRasterization2D_mex(
            p1,p2,p3,
            v1,v2,v3,cv,singleval,
            ntri,w,h,d,
            I);
}
