% operation: 0: assign to image
% operation: 1: add to image
% operation: 2: subtract from image
% operation: 4: multiply with from image
function I = triangleRasterization(I,x,y,v,op)
    if nargin < 5 || isempty(op)
        op = [];
    end

    [bbmin,bbmax] = pointsetBoundingbox([x;y]);
    
    bbmin = max(1,floor(bbmin));
    bbmax = [min(size(I,2),ceil(bbmax(1)));...
             min(size(I,1),ceil(bbmax(2)))];
         
    [px,py] = meshgrid(bbmin(1):bbmax(1),bbmin(2):bbmax(2));
    px = px(:)';
    py = py(:)';
    bc = [((y(2)-y(3)).*(px-x(3)) + (x(3)-x(2)).*(py-y(3))) ./ ...
          ((y(2)-y(3))*(x(1)-x(3)) + (x(3)-x(2))*(y(1)-y(3))); ...
          ((y(3)-y(1)).*(px-x(3)) + (x(1)-x(3)).*(py-y(3))) ./ ...
          ((y(2)-y(3))*(x(1)-x(3)) + (x(3)-x(2))*(y(1)-y(3))); ...
          ones(1,numel(px))];
	bc(3,:) = 1-(bc(1,:)+bc(2,:));
    
    mask = bc(1,:) >= 0 & bc(1,:) <= 1 & ...
           bc(2,:) >= 0 & bc(2,:) <= 1 & ...
           bc(3,:) >= 0 & bc(3,:) <= 1;
    
       
    px = repmat(px(mask),size(v,1),1);
    py = repmat(py(mask),size(v,1),1);
    pc = repmat((1:size(v,1))',1,size(px,2));
    vals = v(:,1)*bc(1,mask) + ...
           v(:,2)*bc(2,mask) + ...
           v(:,3)*bc(3,mask);

    if isempty(op)
        I(sub2ind(size(I),py(:),px(:),pc(:))) = vals(:);
    else
        I(sub2ind(size(I),py(:),px(:),pc(:))) = op(I(sub2ind(size(I),py(:),px(:),pc(:))),vals(:));
    end
end
