function [x,y] = polylineCirclearc(cx,cy,r,startangle,endangle,in4,mode)
    arcangle = smod(endangle-startangle,-pi,pi);

    if nargin < 6 || isempty(in4)
        nsamples = max(3,round(128*(abs(arcangle)/(2*pi))));
    else
        if nargin < 7 || isempty(mode)
            mode = 'samplecount';
        end
        if strcmp(mode,'samplecount')
            nsamples = in4;
        elseif strcmp(mode,'maxerror')
            maxangle = acos((r-in4)/r)*2;
            nsamples = max(3,ceil(abs(arcangle)/maxangle));
        else
            error('Unkown polygon circle mode.');
        end
    end

    angles = smod(linspace(startangle,startangle+arcangle,nsamples),-pi,pi);
    [x,y] = pol2cart(angles,abs(r));
    x = x + cx;
    y = y + cy;
end
