function [ox,oy] = polylineOffset(x,y,offset,res)
    if numel(x) < 2
        error('path must hav at least 2 vertices');
    end
        
	plinenormals = polylineNormals([x;y],false);

    % construct polygon from polyline by offsetting polyline by a small
    % epsilon in positive and negative normal direction
    if offset < eps('double')*50000
        epsoffset = offset;
    else
        epsoffset = offset * 0.0001;
    end
    plineoffset1 = [x;y] + plinenormals .* (epsoffset);
    plineoffset2 = [x;y] + plinenormals .* -(epsoffset);
    plinepolygon = [plineoffset1,fliplr(plineoffset2)];
    [plinepolygon(1,:),plinepolygon(2,:)] = ...
        poly2cw(plinepolygon(1,:),plinepolygon(2,:));
    
    % offset the polygon
    if offset < eps('double')*50000
        ox = {{plinepolygon(1,:)}};
        oy = {{plinepolygon(2,:)}};
    else
        if nargin >= 4
            [ox,oy] = polygonOffset(plinepolygon(1,:),plinepolygon(2,:),offset,res);
        else
            [ox,oy] = polygonOffset(plinepolygon(1,:),plinepolygon(2,:),offset);
        end
    end
end
