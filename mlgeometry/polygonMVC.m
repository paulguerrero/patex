function mvc = polygonMVC(p,poly)

	if size(poly,2) < 3
        error('Given polygon is degenerate.');
	end
    
    mvc = zeros(size(poly,2),size(p,2));
    
    vecx = bsxfun(@minus,poly(1,:)',p(1,:));
    vecy = bsxfun(@minus,poly(2,:)',p(2,:));
    dists = sqrt(vecx.^2 + vecy.^2);
    areas = -0.5.*(vecx.*vecy([2:end,1],:) - vecx([2:end,1],:).*vecy);
    
    handledmask = false(1,size(p,2));
    
    % handle points on corners (weight = 1 for corner vertex, 0 for all
    % other vertices)
    [mindist,mindistvind] = min(dists,[],1);
    mask = not(handledmask) & mindist == 0;
    mvc(:,mask) = 0;
    mvc(sub2ind(size(mvc),mindistvind(mask),find(mask))) = 1;
    handledmask = handledmask | mask;
    
    % handle points on edges (linear interpolation weights between vertices
    % adjacent to edge)
    [minarea,minareavind] = min(abs(areas),[],1);
    mask = not(handledmask) & minarea == 0;
    mvc(:,mask) = 0;
    inds1 = sub2ind(size(mvc),minareavind(mask),find(mask));
    inds2 = sub2ind(size(mvc),mod(minareavind(mask),size(poly,2))+1,find(mask));
    mvc(inds1) = dists(inds2)./(dists(inds1)+dists(inds2));
    mvc(inds2) = dists(inds1)./(dists(inds1)+dists(inds2));
    handledmask = handledmask | mask;
    
    % handle all remaining points
    mask = not(handledmask);
    tanhalfalpha = ...
        (dists(:,mask).*dists([2:end,1],mask) - ...
         (vecx(:,mask).*vecx([2:end,1],mask) + vecy(:,mask).*vecy([2:end,1],mask))) ...
        ./ (2.*areas(:,mask));
    mvc(:,mask) = ...
        2.* ((tanhalfalpha([end,1:end-1],:) + tanhalfalpha) ./ dists(:,mask));
    mvc(:,mask) = bsxfun(@times,mvc(:,mask),1./sum(mvc(:,mask),1)); % normalize
end
