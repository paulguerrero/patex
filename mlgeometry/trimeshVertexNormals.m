% may also pass precomputed values for face normals, face normals and face areas, or edges in varargin
function vn = trimeshVertexNormals(verts,faces,mode,varargin)
    
    if abs(nargin) < 3 || isempty(mode)
        mode = 'faceavg';
    end

    vn = zeros(3,size(verts,2));
    if strcmp(mode, 'faceavg') || strcmp(mode, 'faceavg_area')
        
        if numel(varargin) < 1
            facenormals = trimeshFaceNormals(verts,faces);
        else
            facenormals = varargin{1};
        end
        
        if strcmp(mode, 'faceavg')
            
            vn(1,:) = accumarray(faces(:),reshape(facenormals(ones(1,3).*1,:),[],1),[size(verts,2),1]);
            vn(2,:) = accumarray(faces(:),reshape(facenormals(ones(1,3).*2,:),[],1),[size(verts,2),1]);
            vn(3,:) = accumarray(faces(:),reshape(facenormals(ones(1,3).*3,:),[],1),[size(verts,2),1]);
            vc = accumarray(faces(:),1,[size(verts,2),1]);
        else
            if numel(varargin) < 2
                faceareas = triangleArea(verts(:,faces(1,:)),verts(:,faces(2,:)),verts(:,faces(3,:)));
            else
                faceareas = varargin{2};
            end
            
            vn(1,:) = accumarray(faces(:),reshape(facenormals(ones(1,3).*1,:),[],1),[size(verts,2),1]);
            vn(2,:) = accumarray(faces(:),reshape(facenormals(ones(1,3).*2,:),[],1),[size(verts,2),1]);
            vn(3,:) = accumarray(faces(:),reshape(facenormals(ones(1,3).*3,:),[],1),[size(verts,2),1]);
            vc = accumarray(faces(:),reshape(faceareas(ones(1,3),:),[],1),[size(verts,2),1]);
        end
        
        mask = vc==0;
%         if any(mask)
%             warning('Isolated vertices (without adjacent faces) found. Using default normal [1;0;0].');
%         end
        vn(1,mask) = 1;
        vn(2,mask) = 0;
        vn(3,mask) = 0;
        
        inds = find(not(mask));
        
        vlen = sqrt(sum(vn.^2,1));
        mask = vlen(inds)==0;
%         if any(mask)
%             warning('Face normals average to zero for some vertices. Using default normal [1;0;0].');
%         end
        
        inds1 = inds(mask);
        vn(1,inds1) = 1;
        vn(2,inds1) = 0;
        vn(3,inds1) = 0;
        
        inds1 = inds(not(mask));
        vn(:,inds1) = bsxfun(@rdivide,vn(:,inds1),vlen(inds1));
    elseif strcmp(mode, 'nbrsvd')
        
        if numel(varargin) < 1
            edges = trimeshEdges(faces,size(verts,2));
        else
            edges = varargin{1};
        end
        
        edges = logical(edges);
        edges = edges | edges';
        
        for i=1:size(verts,2)
            vn = [verts(:,i),verts(:,edges(i,:)>0)];
            [~,~,vn] = svd(bsxfun(@minus,vn,mean(vn,2))');
            vn = vn(3,:)'/norm(vn(3,:));
        end
    else
        error('Unkown vertex normal calculation mode.');
    end

end
