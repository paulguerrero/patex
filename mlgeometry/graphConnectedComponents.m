function [compverts,compedges] = graphConnectedComponents(verts,edges)

    adjvinds = cell(1,size(verts,2));
    adjvertangles = cell(1,size(verts,2));
    for i=1:size(edges,2)
        adjvinds{edges(1,i)}(end+1) = edges(2,i);
        adjvinds{edges(2,i)}(end+1) = edges(1,i);
    end
    for i=1:size(verts,2)
        vecs = bsxfun(@minus,verts(:,adjvinds{i}),verts(:,i));
        [angles,~] = cart2pol(vecs(1,:),vecs(2,:));
        [~,perm] = sort(angles,'ascend');
        adjvinds{i} = adjvinds{i}(perm);
        adjvertangles{i} = angles(perm);
    end    

    adjacencyinds = cell2mat(cellfun(@(x,y) [x;ones(1,numel(x)).*y],...
        adjvinds,num2cell(1:numel(adjvinds)),'UniformOutput',false));
    G = sparse(adjacencyinds(1,:),adjacencyinds(2,:),ones(1,size(adjacencyinds,2)),size(verts,2),size(verts,2),size(adjacencyinds,2));
    
    [ncomps,compinds] = graphconncomp(G);
    
    compverts = cell(1,ncomps);
    vertindmap = nan(1,size(verts,2));
    for i=1:size(verts,2)
        compverts{compinds(i)}(:,end+1) = verts(:,i);
        vertindmap(i) = size(compverts{compinds(i)},2);
    end
    compedges = cell(1,ncomps);
    for i=1:size(edges,2)
        if compinds(edges(1,i)) ~= compinds(edges(2,i))
            error('edge connects disconnected components');
        end
        compedges{compinds(edges(1,i))}(:,end+1) = ...
            [vertindmap(edges(1,i));vertindmap(edges(2,i))];
    end

end
