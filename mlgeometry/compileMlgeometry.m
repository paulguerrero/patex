function compileMlgeometry(options,utilpath)

    % unload all currently loaded mex files
    clear mex;
    
    includeoptions = {['-I',utilpath]};
    
    % get compile options
    if any(strcmp(options,'debug'))
        additionaloptions = {'-g'};
    else
        % currently triTriIntersectionDevillers.cpp takes very long to compile
        % with optimizations (although the code itself is relatively short)
        disp('This may take some time, some compilers seem to need a really long time to compile a file used in the next few functions... (the file itself is rather small, there seems to be no good reason)');
        additionaloptions = {};
    end
    
    % compile files
    try
        mex('-largeArrayDims',includeoptions{:},additionaloptions{:},'pointInPolygon_mex.cpp',[utilpath,'/mexutils.cpp'],'geom.cpp','triTriIntersectionDevillers.cpp');
        mex('-largeArrayDims',includeoptions{:},additionaloptions{:},'pointPolylineDistance_mex.cpp');
        mex('-largeArrayDims',includeoptions{:},additionaloptions{:},'linesegLinesegDistance3D_mex.cpp',[utilpath,'/mexutils.cpp'],'geom.cpp','triTriIntersectionDevillers.cpp');
        mex('-largeArrayDims',includeoptions{:},additionaloptions{:},'lineLineDistance3D_mex.cpp',[utilpath,'/mexutils.cpp'],'geom.cpp','triTriIntersectionDevillers.cpp');
        mex('-largeArrayDims',includeoptions{:},additionaloptions{:},'linesegTriangleDistance3D_mex.cpp',[utilpath,'/mexutils.cpp'],'geom.cpp','triTriIntersectionDevillers.cpp');
        mex('-largeArrayDims',includeoptions{:},additionaloptions{:},'pointTriangleDistance3D_mex.cpp',[utilpath,'/mexutils.cpp'],'geom.cpp','triTriIntersectionDevillers.cpp');
        mex('-largeArrayDims',includeoptions{:},additionaloptions{:},'pointLinesegDistance3D_mex.cpp',[utilpath,'/mexutils.cpp'],'geom.cpp','triTriIntersectionDevillers.cpp');
        mex('-largeArrayDims',includeoptions{:},additionaloptions{:},'trianglesetTrianglesetIntersection3D_mex.cpp',[utilpath,'/mexutils.cpp'],'geom.cpp','triTriIntersectionDevillers.cpp');
        mex('-largeArrayDims',includeoptions{:},additionaloptions{:},'linesegLinesegIntersection2D_mex.cpp',[utilpath,'/mexutils.cpp'],'geom.cpp','triTriIntersectionDevillers.cpp');
        mex('-largeArrayDims',includeoptions{:},additionaloptions{:},'rayTriangleIntersection3D_mex.cpp');
        mex('-largeArrayDims',includeoptions{:},additionaloptions{:},'triangleRasterization2D_mex.cpp');
        mex('-largeArrayDims',includeoptions{:},additionaloptions{:},'polygonPolygonIntersection_mex.cpp',[utilpath,'/mexutils.cpp'],'geom.cpp','triTriIntersectionDevillers.cpp');
        mex('-largeArrayDims',includeoptions{:},additionaloptions{:},'pointLinesegDistance2D_mex.cpp',[utilpath,'/mexutils.cpp'],'geom.cpp','triTriIntersectionDevillers.cpp');
    catch err
        if exist(fullfile(pwd,'precompiled.conf'),'file') == 2
            delete('precompiled.conf')
        end
        rethrow(err);
    end
    
    % update precompiled info
    updatePrecompiledInfo('precompiled.conf',{...
        'pointInPolygon_mex',...
        'pointPolylineDistance_mex',...
        'linesegLinesegDistance3D_mex',...
        'lineLineDistance3D_mex',...
        'linesegTriangleDistance3D_mex',...
        'pointTriangleDistance3D_mex',...
        'pointLinesegDistance3D_mex',...
        'trianglesetTrianglesetIntersection3D_mex',...
        'linesegLinesegIntersection2D_mex',...
        'rayTriangleIntersection3D_mex',...
        'triangleRasterization2D_mex',...
        'polygonPolygonIntersection_mex',...
        'pointLinesegDistance2D_mex'},...
        'C++');

    disp('done');
end
