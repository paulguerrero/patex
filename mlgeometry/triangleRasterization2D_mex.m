% triangleRasterization2D_mex(I,p1,p2,p3,cv)
% triangleRasterization2D_mex(I,p1,p2,p3,v1,v2,v3)
% Rasterize n triangles
% I is a w x h x d image
% p1,p2,p3 (2 x n) are corners of the triangles
% v1,v2,v3 (d x n) are values to be interpolated
% cv (d x n or d x 1) are constant values per triangle or one value for all
% triangles
% WARNING: only use on non-shared variables, the input variable I is
% overwritten (for performance)