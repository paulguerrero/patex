% order: inverse translate, inverse rotate, inverse scale, mirror (= inverse mirror)
% scale: 3 x n or 3 x 1 matrix of 3-dimensional scale vectors
% angle: 4 x n or 4 x 1 matrix of quaternions
% trans: 3 x n or 3 x 1 matrix of 3-dimensional translations
% optional: mirror: 1 x n or 1 x 1 matrix of mirroring flags
% (mirroring is about xz plane: y = -y)
% returns 4 x 4 x n 3D transformation matrices
function m = transformMat3Dinv(scale, angle, trans, mirror)
    
    
    scale = reshape(scale,3,1,[]);
%     angle = reshape(angle,4,1,[]);
    trans = reshape(trans,3,1,[]);
    
    if nargin >= 4
        mirror = reshape(mirror,1,1,[]);
        mir = (logical(mirror).*-2+1);
    else
        mir = ones(1,1,size(scale,3));
    end
    
    transmat = repmat(eye(4,4),1,1,size(scale,3));
    transmat(1,4,:) = -trans(1,1,:);
    transmat(2,4,:) = -trans(2,1,:);
    transmat(3,4,:) = -trans(3,1,:);

    % matrix multiplication written out (so i can multiply n matrices with n vectors)
    invrot = permute(quat2dcm(quatconj(angle')),[2,1,3]);
    invscale = 1./scale;
    m = [[     invscale(1,:,:).*invrot(1,1,:),      invscale(1,:,:).*invrot(1,2,:),      invscale(1,:,:).*invrot(1,3,:); ...
          mir.*invscale(2,:,:).*invrot(2,1,:), mir.*invscale(2,:,:).*invrot(2,2,:), mir.*invscale(2,:,:).*invrot(2,3,:); ...
               invscale(3,:,:).*invrot(3,1,:),      invscale(3,:,:).*invrot(3,2,:),      invscale(3,:,:).*invrot(3,3,:)], zeros(3,1,size(scale,3));...
         zeros(1,3,size(scale,3)), ones(1,1,size(scale,3))];

	for i=1:size(scale,3)
        m(:,:,i) =  m(:,:,i) * transmat(:,:,i);
	end
end
