% ouput is 2x3 or 3x4 with 
% obbox(:,1) the origin
% obbox(:,2) the x-side vector (from origin)
% obbox(:,3) the y-side vector (from origin)
% obbox(:,4) the z-side vector (from origin - only for 3 dimensions)
function obbox = pointsetOrientedBoundingbox(p,orientation)
    
    if size(p,1) == 2
        axes = zeros(2,1);
        [axes(1),axes(2)] = pol2cart(orientation,1);
        axes = [axes,[-axes(2);axes(1)]]; % orthogonal direction is rotated ccw (like cartesian y axis)
    elseif size(p,1) == 3
        axes = quat2dcm(orientation)';
    else
        error('Can only handle 2- or 3-dimensional point sets.');
    end
    
    axescoords = axes'*p;

    minanglecoords = min(axescoords,[],2);
    maxanglecoords = max(axescoords,[],2);

    obbox(:,1) = axes*minanglecoords;
    obbox(:,2) = (maxanglecoords(1)-minanglecoords(1)).*axes(:,1);
    obbox(:,3) = (maxanglecoords(2)-minanglecoords(2)).*axes(:,2);
    if size(maxanglecoords,1) >= 3
        obbox(:,4) = (maxanglecoords(3)-minanglecoords(3)).*axes(:,3);
    end
end
