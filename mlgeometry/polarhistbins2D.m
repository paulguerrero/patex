function bins = polarhistbins2D(angularpos,radialpos,descpos)

    if nargin < 3 || isempty(descpos)
        descpos = [0;0];
    end
    
    % generate list of all 2D cartesian bin postions for a single descriptor at (0,0)
    [binposx,binposy] = ndgrid(angularpos,radialpos);
    binposx = binposx(:)';
    binposy = binposy(:)';
    [binposx,binposy] = pol2cart(binposx,binposy);
    
    % generate list of all 2D cartesian bin postions for a all descriptors at pos
    if size(descpos,2) > 1
        binposx = bsxfun(@plus,binposx',descpos(1,:));
        binposy = bsxfun(@plus,binposy',descpos(2,:));
        bins = [binposx(:)';binposy(:)'];
    else
        bins = [binposx + descpos(1); binposy + descpos(2)];
    end
end
