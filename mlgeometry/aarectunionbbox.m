% axis-aligned bounding boxes of axis aligned rectangle pairs
% [iminx,iminy,imaxx,imaxy] = aarectunionbbox(r1min,r1max,r2min,r2max)
% [iminx,iminy,imaxx,imaxy] = aarectunionbbox(r1min,r1max,r2min,r2max,ind1,ind2)
function [uminx,uminy,umaxx,umaxy] = aarectunionbbox(r1min,r1max,r2min,r2max,ind1,ind2)

    if size(r1min,1) ~= 2 || size(r1max,1) ~= 2 || ...
       size(r2min,1) ~= 2 || size(r2max,1) ~= 2
        error('Input rectangle min/max corners must be given as 2XN arrays');
    end

    if nargin == 5 || ndims(ind1) ~= ndims(ind2) || any(size(ind1) ~= size(ind2))
        error('Given indices must have same shape and size.');
    end
    
    if nargin < 6 || isempty(ind1)
        % all pairwise intersections
        uminx = bsxfun(@min,r1min(1,:)',r2min(1,:));
        uminy = bsxfun(@min,r1min(2,:)',r2min(2,:));
        umaxx = bsxfun(@max,r1max(1,:)',r2max(1,:));
        umaxy = bsxfun(@max,r1max(2,:)',r2max(2,:));
    else
        % intersections the given pairs
        uminx = min(r1min(1,ind1(:)),r2min(1,ind2(:)));
        uminy = min(r1min(2,ind1(:)),r2min(2,ind2(:)));
        umaxx = max(r1max(1,ind1(:)),r2max(1,ind2(:)));
        umaxy = max(r1max(2,ind1(:)),r2max(2,ind2(:)));
        
        uminx = reshape(uminx,size(ind1));
        uminy = reshape(uminy,size(ind1));
        umaxx = reshape(umaxx,size(ind1));
        umaxy = reshape(umaxy,size(ind1));
    end
end
