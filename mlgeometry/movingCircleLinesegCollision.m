% returns collision time, collision point on the line segment and line
% segment index
function [t,cp,sind] = movingCircleLinesegCollision(startpos, vel, radius, p1, p2)
    
    % Same as circle trajectory - line segment capsule intersection test.
    % Closest point on line segment to trajectory-capsule intersection
    % point is the point first touched by the circle.    
    
    if size(startpos,2) ~= 1
        error('only one circle at a time supported');
    end
    
    if isempty(p1)
        t = nan;
        cp = nan(2,1);
        sind = nan;
        return;
    end

    
    traj = [startpos;startpos+vel];
    velmag = sqrt(sum(vel.^2,1));
    
    % test for intersection with capsule caps 
    % ( = circle around line segment endpoints)
    [cap1id,~,cap1it,~,~] = pointPolylineDistance_mex(...
        p1(1,:),p1(2,:),...
        traj(1),traj(2),traj(3),traj(4));
    [cap2id,~,cap2it,~,~] = pointPolylineDistance_mex(...
        p2(1,:),p2(2,:),...
        traj(1),traj(2),traj(3),traj(4));
    cap1imask = cap1id <= radius;
    cap2imask = cap2id <= radius;
    
    % if the trajectory endpoint is inside the closest point add back the distance from the trajectory endpoint to the closest point
    cap1it = cap1it(cap1imask);
    cap1id = cap1id(cap1imask);
    p1temp = p1(:,cap1imask);
    mask = cap1it == 1;
    if any(mask)
        trajnormal = [-vel(2),vel(1)]./velmag;
        endpointvec = [traj(3) - p1temp(1,mask);...
                       traj(4) - p1temp(2,mask)];
        orthdist = abs(endpointvec(1,:).*trajnormal(1) + endpointvec(2,:).*trajnormal(2));
        endpointdist = sqrt(sum(endpointvec.^2,1));
        cap1it(mask) = cap1it(mask) - (sqrt(radius.^2-orthdist.^2) - sqrt(endpointdist.^2-orthdist.^2)) ./ velmag;
    end
    
    mask = not(mask);
    if any(mask)
        cap1it(mask) = cap1it(mask) - sqrt(radius.^2-cap1id(mask).^2) ./ velmag;
    end
    
    % if the trajectory endpoint is inside the closest point add back the distance from the trajectory endpoint to the closest point
    cap2it = cap2it(cap2imask);
    cap2id = cap2id(cap2imask);
    p2temp = p2(:,cap2imask);
    mask = cap2it == 1;
    if any(mask)
        trajnormal = [-vel(2),vel(1)]./velmag;
        endpointvec = [traj(3) - p2temp(1,mask);...
                       traj(4) - p2temp(2,mask)];
        orthdist = abs(endpointvec(1,:).*trajnormal(1) + endpointvec(2,:).*trajnormal(2));
        endpointdist = sqrt(sum(endpointvec.^2,1));
        cap2it(mask) = cap2it(mask) - (sqrt(radius.^2-orthdist.^2) - sqrt(endpointdist.^2-orthdist.^2)) ./ velmag;
    end
    
    mask = not(mask);
    if any(mask)
        cap2it(mask) = cap2it(mask) - sqrt(radius.^2-cap2id(mask).^2) ./ velmag;
    end
    
    
    % todo: cull all irrelevant line segments
    
    % test for intersections with capsule sides
    segvecs = p2-p1;
    seglens = sqrt(sum(segvecs.^2,1));
    segdirs = segvecs ./ seglens([1,1],:);
    offsets = [-segdirs(2,:);segdirs(1,:)].*radius;
    csides1 = [p1+offsets;p2+offsets];
    csides2 = [p1-offsets;p2-offsets];
    [iside1,ip1] = linesegLinesegIntersection2D_mex(traj(1:2,:),traj(3:4,:),csides1(1:2,:),csides1(3:4,:));
    [iside2,ip2] = linesegLinesegIntersection2D_mex(traj(1:2,:),traj(3:4,:),csides2(1:2,:),csides2(3:4,:));
    
    side1imask = iside1>0;
    side2imask = iside2>0;
    
    error('todo: compute side1 and side2 intersection t value using the intersection points ip1 and ip2');
%     side1it = sqrt(sum((ip1-traj(1:2,:)).^2,1)) ./ sqrt(sum((traj(3:4,:)-traj(1:2,:)).^2,1));
%     side2it = sqrt(sum((ip1-traj(1:2,:)).^2,1)) ./ sqrt(sum((traj(3:4,:)-traj(1:2,:)).^2,1));
    side1it = iside1.intNormalizedDistance1To2(side1imask);
    side2it = iside2.intNormalizedDistance1To2(side2imask);
    
    
    % find capsule and part of the capsule that has intersection with minimum time
    t = nan(4,size(p1,2));
    t(1,cap1imask) = cap1it;
    t(2,cap2imask) = cap2it;
    t(3,side1imask) = side1it;
    t(4,side2imask) = side2it;
    
    [t,part] = min(t,[],1);
    [t,sind] = min(t,[],2);
    
    if isnan(t)
        sind = nan;
        cp = nan(2,1);
    else
        part = part(sind);

        % find intersection point on line segment
        if part == 1
            cp = p1(:,sind);
        elseif part == 2
            cp = p2(:,sind);
        elseif part == 3
            f = iside1.intNormalizedDistance2To1(sind);
            cp = p1(:,sind) .* (1-f) + p2(:,sind) .* f;
        elseif part == 4
            f = iside2.intNormalizedDistance2To1(sind);
            cp = p1(:,sind) .* (1-f) + p2(:,sind) .* f;
        else
            assert(true,'this should not be');
        end
    end

end
