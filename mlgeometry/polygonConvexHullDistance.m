function d = polygonConvexHullDistance(x,y,geolib)
    
    if any(isnan(x))
        error('polygons with holes not supported (hole distance is inf)');
    end

    d = zeros(1,numel(x));

    polyextent = sqrt((max(x)-min(x)).^2 + (max(y)-min(y)).^2);
    samepointtolerance = polyextent*0.0000001;
	bridgesampledist = polyextent*0.005;
     
    % compute convex hull
    hullinds = convhull(x,y)';
    if not(isempty(hullinds))
        hullinds(end) = []; % remove endindex (because it is = startindex)
    end
    
    % find pockets
    pocketlen = [diff(hullinds)-1,(hullinds(1)-1)+(numel(x)-hullinds(end))];
    pocketinds = {};
    for i=1:numel(pocketlen)-1
        
        if pocketlen(i) > 0
            pocketinds{end+1} = hullinds(i):hullinds(i+1); %#ok<AGROW>
%             pocketinds{end} = pocketinds{end};
        end
    end
    if pocketlen(end) > 0
        pocketinds{end+1} = [hullinds(end):numel(x),1:hullinds(1)];
%         pocketinds{end} = binds(pocketinds{end});
    end
    
    % compute distance of each pocket point from bridge
    for i=1:numel(pocketinds)
        % refine the bridge edge
        bridgestart = [x(pocketinds{i}(1));y(pocketinds{i}(1))];
        bridgeend = [x(pocketinds{i}(end));y(pocketinds{i}(end))];
        bridgelen = sqrt(sum((bridgeend - bridgestart).^2,1));
        np = max(10,ceil(bridgelen / bridgesampledist));
        f = linspace(0,1,np);
        bridgex = bridgestart(1) + (bridgeend(1)-bridgestart(1)) .* f;
        bridgey = bridgestart(2) + (bridgeend(2)-bridgestart(2)) .* f;
        
        pocketx = [bridgex(1),x(pocketinds{i}(2:end-1)),bridgex(end:-1:2)];
        pockety = [bridgey(1),y(pocketinds{i}(2:end-1)),bridgey(end:-1:2)];
        
%         ptri = PolyTri([pocketx;pockety]',[1:numel(pocketx);2:numel(pocketx),1]');
        ptri = PolyTri([pocketx;pockety]',[],false);
        sourcep = [x(pocketinds{i}(2:end-1));y(pocketinds{i}(2:end-1))];
        goalp = [bridgex;bridgey];
        pid = ptri.meshElementId(sourcep,ones(1,size(sourcep,2)).*3,samepointtolerance);
        gid = ptri.meshElementId(goalp,ones(1,size(goalp,2)).*3,samepointtolerance);
        
        if any(isnan(pid)) || any(isnan(gid))
            error('no mesh element found for a pocket vertex');
        end
        
        distmat = computeTrimeshGeodists(geolib,...
            sourcep,...
            goalp,...
            ptri.f,ptri.v,pid,gid,...
            ones(1,size(sourcep,2)).*3,ones(1,size(goalp,2)).*3);
        d(pocketinds{i}(2:end-1)) = min(distmat,[],2)';
    end
end
