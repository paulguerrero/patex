% order: mirror, scale, rotate, translate
% scale: 2 x n or 2 x 1 matrix of 2-dimensional scale vectors
% angle: 1 x n or 1 x 1 matrix of angles (in radians)
% trans: 2 x n or 2 x 1 matrix of 2-dimensional translations
% optional: mirror: 1 x n or 1 x 1 matrix of mirroring flags
% (mirroring is about x axis: y = -y)
% returns 3 x 3 x n 2D transformation matrices
function m = transformMat2D(scale, angle, trans, mirror)
    
    scale = reshape(scale,2,1,[]);
    angle = reshape(angle,1,1,[]);
    trans = reshape(trans,2,1,[]);
    
    if nargin >= 4
        mirror = reshape(mirror,1,1,[]);
        mir = (logical(mirror).*-2+1);
    else
        mir = ones(1,1,size(scale,3));
    end

    % matrix multiplication written out (so i can multiply n matrices with n vectors)
    cosangle = cos(angle);
    sinangle = sin(angle);
    m = [cosangle.*scale(1,:,:), -sinangle.*scale(2,:,:).*mir, trans(1,:,:); ...
         sinangle.*scale(1,:,:),  cosangle.*scale(2,:,:).*mir, trans(2,:,:); ...
         zeros(1,2,size(scale,3)), ones(1,1,size(scale,3))];
end
