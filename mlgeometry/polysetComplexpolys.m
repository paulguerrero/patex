% get a list of complex polygons in the set of polygon boundaries
function [cpolys,polyinds] = polysetComplexpolys(polys)
    a = polysetHierarchy(polys);
    
    rootpolyinds = find(not(any(a,2)));
    activepolymask = true(1,numel(polys));
    
    cpolys = cell(1,0);
    polyinds = cell(1,0);
    while not(isempty(rootpolyinds))
        for i=1:numel(rootpolyinds)
            childpolyinds = find(a(:,rootpolyinds(i)))';
            polyinds{end+1} = [rootpolyinds(i),childpolyinds]; %#ok<AGROW>
            cpolys{end+1} = polys(polyinds{end}); %#ok<AGROW>
        end
        allpolyinds = unique([polyinds{end-numel(rootpolyinds)+1:end}]);
        
        activepolymask(allpolyinds) = false;
        a(allpolyinds,:) = 0;
        a(:,allpolyinds) = 0;
        
        activepolyinds = find(activepolymask);
        
        rootpolyinds = activepolyinds(not(any(a(activepolymask,activepolymask),2)));
    end
end
