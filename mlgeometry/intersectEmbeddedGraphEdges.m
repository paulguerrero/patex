% merges two graphs embedded in the plane so that the resulting graph has
% at least all vertices and edges of the input graphs and is also embedded
% in the plane
% graphs are assumed to be undirected
function [ov,oe] = intersectEmbeddedGraphEdges(verts,edges,vertexsnapdist,edgesnapdist)

    if nargin < 3
        vertexsnapdist = 0;
    end
    
    if nargin < 4
        edgesnapdist = vertexsnapdist;
    end
    
    if any(isnan(verts))
        error('Some vertices are undefined.');
    end
    
	ov = verts;
    oe = edges;
    
    % delete double edges
    oe = removeDoubleEdges(oe);
    
    % snap vertices to vertices
    d = squareform(pdist(ov'));
    d = d + diag(inf(1,size(ov,2)));
    mergefrom = [];
    mergeto = [];
    while true
        [dtemp,v1ind] = min(d,[],1);
        [mind,v2ind] = min(dtemp);
        v1ind = v1ind(v2ind);
        if mind > vertexsnapdist
            break;
        end
        
        mergefrom(end+1) = max(v1ind,v2ind); %#ok<AGROW>
        mergeto(end+1) = min(v1ind,v2ind); %#ok<AGROW>
        
        % remove the merged vertex from the distance matrix
        d(:,mergefrom(end)) = inf;
        d(mergefrom(end),:) = inf;
    end
    
    clear d;
    
%     [ov,oe] = mergeGraphVertices(v2ind,v1ind,ov,oe);
    [ov,oe] = mergeGraphVertices(mergefrom,mergeto,ov,oe);
    
    % find adjacent edges
    vertadjedges = cell(1,size(ov,2));
    for i=1:size(oe,2)
        vertadjedges{oe(1,i)}(end+1) = i;
        vertadjedges{oe(2,i)}(end+1) = i;
    end
    
    % snap vertices to edges
    [d,f,px,py] = pointLinesegDistance2D(...
        ov(1,:),ov(2,:),...
        ov(1,oe(1,:)),ov(2,oe(1,:)),...
        ov(1,oe(2,:)),ov(2,oe(2,:)));
    
    % remove snapping of a vertex to the adjacent edge
    for i=1:size(oe,2)
        d(oe(1,i),i) = nan;
        d(oe(2,i),i) = nan;
    end
    
    [d,eind] = min(d,[],2);
    vind = find(d <= edgesnapdist);
    eind = eind(vind);
%     d = d(vind);
    ind = sub2ind(size(px),vind,eind);
    f = f(ind);
    px = px(ind)';
    py = py(ind)';
    
    newvind = size(ov,2)+1:size(ov,2)+numel(px);
    ov = [ov,[px;py]];
    
    oe = splitGraphEdges(eind,f,newvind,oe);
    [ov,oe] = mergeGraphVertices(vind,newvind,ov,oe);
    
    % delete double edges
    oe = removeDoubleEdges(oe);

%     % all edges adjacent to v2ind are attached to v1ind instead
%     vindmap = 1:size(ov,2);
%     vindmap(v2ind) = v1ind;
%     oe = vindmap(oe);
%     
%     vindmap = nan(1,size(ov,2));
%     mask = true(1,size(ov,2));
%     mask(v2ind) = false;
%     ov(:,v2ind) = [];
%     vindmap(mask) = 1:size(ov,2);
%     oe = vindmap(oe);


    % find adjacent edges
    vertadjedges = cell(1,size(ov,2));
    for i=1:size(oe,2)
        vertadjedges{oe(1,i)}(end+1) = i;
        vertadjedges{oe(2,i)}(end+1) = i;
    end
    edgeadjedges = cell(1,size(oe,2));
    for i=1:size(oe,2)
        edgeadjedges{i} = [vertadjedges{oe(1,i)},vertadjedges{oe(2,i)}];
        edgeadjedges{i}(edgeadjedges{i} == i) = [];
    end
    
    % intersect edges
    [intersects,ip,~,t1,~] = linesegLinesegIntersection2D_mex(...
        ov(:,oe(1,:)),ov(:,oe(2,:)),...
        ov(:,oe(1,:)),ov(:,oe(2,:)));
    ipx = ip(:,:,1);
    ipy = ip(:,:,2);
    
    % remove edge self-intersections
    intersects(sub2ind(size(intersects),1:size(intersects),1:size(intersects))) = false;
	
    % remove intersections of edges connected by a vertex
    adjedgeinds = cell2mat(cellfun(@(x,y) [x;ones(1,numel(x)).*y],...
        edgeadjedges,num2cell(1:numel(edgeadjedges)),'UniformOutput',false));
    adjedgeinds = sub2ind(size(intersects),adjedgeinds(1,:),adjedgeinds(2,:));
    intersects(adjedgeinds) = 0;
    intersects(adjedgeinds) = 0;
    
    % add intersection vertices
    [e1ind,e2ind] = find(intersects>0);
    mask = e1ind > e2ind;
    e1ind(mask) = [];
    e2ind(mask) = [];
    eind = sub2ind(size(intersects),e1ind,e2ind);
    ivertinds = nan(size(intersects));
    ivertinds(eind) = size(ov,2)+(1:numel(eind));
    % copy upper triangular to lower triangular part
    ivertinds(tril(true(size(ivertinds)),-1)) = 0; 
    ivertinds = ivertinds + ivertinds'; 
    ov = [ov,[ipx(eind)';ipy(eind)']];
    
    % go through all rows (edges of merged graph) and refine edges
    deleteinds = [];
    for j=1:size(intersects,1)
        if any(intersects(j,:))
            inds = find(intersects(j,:));
            [~,perm] = sort(t1(j,inds),'ascend');
            oe = [oe,[oe(1,j),ivertinds(j,inds(perm));...
                      ivertinds(j,inds(perm)),oe(2,j)]]; %#ok<AGROW>
            deleteinds = [deleteinds,j]; %#ok<AGROW>
        end
    end
    oe(:,deleteinds) = [];

    
    % snap vertices to vertices
    [v1ind,v2ind] = find(squareform(pdist(ov)) <= vertexsnapdist);
    mask = v1ind>=v2ind;
    v1ind(mask) = [];
    v2ind(mask) = [];
    
    [ov,oe] = mergeGraphVertices(v2ind,v1ind,ov,oe);
    
    % delete double edges
    oe = removeDoubleEdges(oe);
    
    % just for debug
    if any(any(isnan(oe)))
        error('some edges are nan');
    end
end

function edges = removeDoubleEdges(edges)
    maxvind = max(max(edges));
    adjmat = sparse(edges(1,:),edges(2,:),ones(1,size(edges,2)),maxvind,maxvind);
    adjmat = adjmat>=1 | adjmat'>=1;
    [vind1,vind2] = find(adjmat);
    mask = vind1>=vind2; % also remove edges from and to same vertex
    vind1(mask) = [];
    vind2(mask) = [];
    edges = [vind1';vind2'];
end

