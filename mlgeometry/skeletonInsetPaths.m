function [pathstart,pathend,pathstarttime,pathendtime,origintraj] = ...
    skeletonInsetPaths(sx,sy,st,binds,efrom,eto,angleres,originx,originy)
	
    bx = sx(binds);
    by = sy(binds);
    
    % binds must be in ccw order of boundary vertices
    reversed = false;
    if ispolycw(bx,by)
        binds = binds([1,end:-1:2]);
        bx = sx(binds);
        by = sy(binds);
        reversed = true;
    end
    
    if nargin < 7 || isempty(originx) || isempty(originy)
        originx = bx(1);
        originy = by(1);
    end
    
    % orient all skeleton edges in direction of increasing time
    mask = st(eto) < st(efrom);
    temp = eto(mask);
    eto(mask) = efrom(mask);
    efrom(mask) = temp;
    
    % make connectivity data structures
    vedges = cell(1,numel(sx));
    vedgeleaving = cell(1,numel(sx));
    vneighbours = cell(1,numel(sx));
    for i=1:numel(efrom)
        vedges{efrom(i)}(end+1) = i;
        vedgeleaving{efrom(i)}(end+1) = true;
        vneighbours{efrom(i)}(end+1) = eto(i);
        
        vedges{eto(i)}(end+1) = i;
        vedgeleaving{eto(i)}(end+1) = false;
        vneighbours{eto(i)}(end+1) = efrom(i);
    end
    
%     % compute angles at boundary vertices
%     bangles = polylineAngles([bx;by],true);
    
    % compute normals pointing inwards for each boundary segment
    bnormals = polylineNormals([bx;by],true,false);
%     bnormals = -polylineNormals([bx;by],true,false);
    
    % resample polyline with points that are approx. equally spaced in
    % arclength
    polygonextent = sqrt((max(bx)-min(bx)).^2 + (max(by)-min(by)).^2);
    resampledist = polygonextent*0.02;
    [bsamples,~,origvertinds] = ...
        polylineResample([bx;by],resampledist,'spacing',true,true);
    bsamplesegstart = origvertinds+1;
    bsamplesegend = [origvertinds(2:end)-1,size(bsamples,2)];
    
%     % notch indices (not really needed, just for checking)
%     vnotchmask = bangles < 0;
    
    % build the ordered list of inset paths
    segpathinds = cell(1,numel(bx));
    edgetopathmap = zeros(1,numel(efrom));
    outstandingmask = true(1,numel(efrom));
    pathstart = [];
    pathend = [];
    pathstarttime = [];
    pathendtime = [];
    pathdir = [];
    anglestep = (2*pi)/angleres;
    for i=1:numel(bx)
        einds = vedges{binds(i)};
        if i==1
            lastsegnormal = bnormals(:,end);
        else
            lastsegnormal = bnormals(:,i-1);
        end
        thissegnormal = bnormals(:,i);
        
        if numel(einds) == 1
%             if vnotchmask(i)
%                 error('non-notch with notch angle found');
%             end
            
            segpathinds{i} = size(pathstart,2)+1;

            addInnerSkeletonInsetPaths(einds);
        elseif numel(einds) == 2
%             if not(vnotchmask(i))
%                 error('notch with non-notch angle found');
%             end
            
            % put skeleton edges in correct order
            sdirs = [sx(eto(einds))-sx(efrom(einds));...
                     sy(eto(einds))-sy(efrom(einds))];
            sdirs = sdirs ./ repmat(sqrt(sum(sdirs.^2,1)),2,1);
            [~,firstseg] = max(dot(lastsegnormal(:,[1,1]),sdirs));
            if firstseg ~= 1
                einds = einds([2,1]);
            end
            
            addInnerSkeletonInsetPaths(einds(1));
            
            % add inner paths, in regularly spaced angles between the two skeleton edges
            [angle1,~] = cart2pol(lastsegnormal(1),lastsegnormal(2));
            
            anglediff = acos(min(1,max(-1,dot(lastsegnormal,thissegnormal))));
            
            nsteps = max(2,ceil(abs(anglediff) / anglestep));
            anglesamples = linspace(0,anglediff,nsteps);
            anglesamples = anglesamples(2:end-1);
            if not(isempty(anglesamples))
                anglesamples = angle1-anglesamples; % negative to go clockwise
                mask = anglesamples < - pi;
                anglesamples(mask) = anglesamples(mask) + 2*pi;
                mask = anglesamples > pi;
                anglesamples(mask) = 2*pi - anglesamples(mask);

                [x,y] = pol2cart(anglesamples,ones(1,numel(anglesamples)));
                pathdir(:,end+1:end+numel(anglesamples)) = [x;y];

                pathstart(:,end+1:end+numel(anglesamples)) = ...
                    repmat([bx(i);by(i)],1,numel(anglesamples));
                pathend(:,end+1:end+numel(anglesamples)) = nan(2,numel(anglesamples));

                pathstarttime(end+1:end+numel(anglesamples)) = 0;
                pathendtime(end+1:end+numel(anglesamples)) = nan;
            end
            
            segpathinds{i} = size(pathstart,2)+1;
            
            addInnerSkeletonInsetPaths(einds(2));
        else
            error('border vertex with more than two incident skeleton edges found.')
        end
        
        
        
        % add paths from boundary segment
        segbsamples = bsamples(:,bsamplesegstart(i):bsamplesegend(i));
        if not(isempty(segbsamples))
            segpathinds{i}(end+1:end+size(segbsamples,2)) = ...
                size(pathstart,2)+1:size(pathstart,2)+size(segbsamples,2);
            pathstart(:,end+1:end+size(segbsamples,2)) = segbsamples;
            pathend(:,end+1:end+size(segbsamples,2)) = nan(2,size(segbsamples,2));
            pathdir(:,end+1:end+size(segbsamples,2)) = thissegnormal(:,ones(1,size(segbsamples,2)));
            pathstarttime(end+1:end+size(segbsamples,2)) = 0;
            pathendtime(end+1:end+size(segbsamples,2)) = nan;
        end
        segpathinds{i}(end+1) = size(pathstart,2)+1;
    end
    
    segpathinds{end}(end) = 1;
    
    if any(outstandingmask)
        error('some edges of the skeleton were not reached');
    end
    
    % add path for origin
    originpathind = 0;
    if nargin > 7 && not(isempty(originx)) && not(isempty(originy))
        [~,si,f,startpathx,startpathy] = ...
            pointPolylineDistance_mex(originx,originy,bx([1:end,1]),by([1:end,1]));
        seglen = sqrt((bx(mod(si,numel(bx))+1)-bx(si)).^2 + ...
                      (by(mod(si,numel(by))+1)-by(si)).^2);
        if f < 0.00001
            originpathind = segpathinds{si}(1);
        elseif f > 0.99999
            originpathind = segpathinds{si}(end);
        else
%             segpathendind = segpathinds(2,si);
%             segpathstartind = segpathinds(1,si);
%             if segpathendind < segpathstartind
%                 segpathendind = segpathendind+size(pathstart,2);
%             end
%             pinds = segpathstartind:segpathendind;
%             pinds = mod(pinds-1,size(pathstart,2))+1;
            pinds = segpathinds{si};
            pf = sqrt(sum((pathstart(:,pinds) - repmat([bx(si);by(si)],1,numel(pinds))).^2,1)) ./ seglen;
            startpathpos = interp1(pf,1:numel(pinds),f,'linear','extrap');
            closestpathind = max(1,min(numel(pinds),round(startpathpos)));
            closestpathfdist = abs(f-pf(closestpathind));

            if closestpathfdist*seglen < 0.00001
%                 closestpathind = closestpathind + segpathinds(1,si);
                originpathind = pinds(closestpathind);
            else
                indbefore = min(numel(pinds)-1,max(1,floor(startpathpos)));
                pindbefore = pinds(indbefore);
                pindafter = pinds(indbefore+1);
                if indbefore == 1
                    pindbefore = pindafter-1; 
                else
                    pindafter = pindbefore+1;
                end
                pathstart = [pathstart(:,1:pindbefore),[startpathx;startpathy],pathstart(:,pindafter:end)];
                pathend = [pathend(:,1:pindbefore),nan(2,1),pathend(:,pindafter:end)];
                pathdir = [pathdir(:,1:pindbefore),bnormals(:,si),pathdir(:,pindafter:end)];
                pathstarttime = [pathstarttime(1:pindbefore),0,pathstarttime(pindafter:end)];
                pathendtime = [pathendtime(1:pindbefore),nan,pathendtime(pindafter:end)];
                originpathind = pindbefore+1;
                mask = edgetopathmap > pindbefore;
                edgetopathmap(mask) = edgetopathmap(mask) + 1;
            end
        end
    end
    
    % intersect all paths that start from the boundary with the skeleton edges
    % (for the inner paths end time and end position are already known)
    notchverts = binds(cellfun(@(x) numel(x) == 2,vedges(binds)));
    notchedgemask = false(1,numel(efrom));
    notchedgemask([vedges{notchverts}]) = true;
    notnotchedgeinds = find(not(notchedgemask));
    bpathinds = find(not(isnan(pathdir(1,:))));
    skelbounddiag = sqrt((max(sx)-min(sx)).^2 + ...
                         (max(sy)-min(sy)).^2);
    [intersects,~,~,t1,t2] = linesegLinesegIntersection2D_mex(...
        [sx(efrom(not(notchedgemask)));...
         sy(efrom(not(notchedgemask)))],...
        [sx(eto(not(notchedgemask)));...
         sy(eto(not(notchedgemask)))],...
        pathstart(:,bpathinds),...
        pathstart(:,bpathinds)+pathdir(:,bpathinds).*skelbounddiag*2);
    t2(intersects < 1) = nan;
%     mask = intersects.intNormalizedDistance1To2 > 1 | ...
%            intersects.intNormalizedDistance1To2 < 0;
%     intersects.intNormalizedDistance2To1(mask) = nan;
%     mask = intersects.intNormalizedDistance2To1 < 0;
%     intersects.intNormalizedDistance2To1(mask) = nan;
%     [~,eind] = min(intersects.intNormalizedDistance2To1,[],1);
    [~,eind] = min(t2,[],1);
    if any(isnan(eind))
        error('no intersection for a path with the skeleton');
    end
%     f = intersects.intNormalizedDistance1To2(...
%         sub2ind(size(intersects.intNormalizedDistance1To2),eind,1:numel(eind)));
    f = t1(sub2ind(size(t1),eind,1:numel(eind)));

    eind = notnotchedgeinds(eind);
    
    endposx = sx(efrom(eind)) .* (1-f) + sx(eto(eind)) .* f;
    endposy = sy(efrom(eind)) .* (1-f) + sy(eto(eind)) .* f;
    endtime = st(efrom(eind)) .* (1-f) + st(eto(eind)) .* f;
    
    pathend(:,bpathinds) = [endposx;endposy];
    pathendtime(:,bpathinds) = endtime;
    
    % trajectory of origin over all paths
    if nargout >= 5
        origintraj = [];
        origintraj(:,1) = [pathstarttime(originpathind);pathendtime(originpathind);originpathind];
        
%         % find successor edges of all paths
        pathendeind = zeros(1,size(pathstart,2));
        pathendeind(bpathinds) = eind;
        
        
%         pathtoedgemap(edgetopathmap) = 1:numel(edgetopathmap);
%         bpathmask = false(1,size(pathstarttime,2));
%         bpathmask(bpathinds) = true;
%         edgepathinds = find(not(bpathmask));
%         pathedgeinds = pathtoedgemap(edgepathinds);
%         for k=1:numel(pathedgeinds)
%             edgesucc = vedges{eto(pathedgeinds(k))};
%             edgesucc(not(vedgeleaving{eto(pathedgeinds(k))})) = [];
%             if not(isempty(edgesucc))
%                 pathendeind(edgepathinds(k)) = edgesucc;
%             else
%                 pathendeind(edgepathinds(k)) = nan;
%             end
%         end
        
        edgequeue = pathendeind(originpathind);
        if edgequeue == 0
            pathendeind(edgetopathmap) = 1:numel(edgetopathmap);
            origineind = pathendeind(originpathind);
            % queue edge successors leaving the target vertex
            edgesucc = vedges{eto(origineind)};
            edgesucc(not(vedgeleaving{eto(origineind)})) = [];
            edgequeue = edgesucc;
        end
        while not(isempty(edgequeue))
            qeind = edgequeue(1);
            edgequeue(1) = [];
            pind = edgetopathmap(qeind);
            origintraj(:,end+1) = [origintraj(2,end);pathendtime(pind);pind]; %#ok<AGROW>

            % queue edge successors leaving the target vertex
            edgesucc = vedges{eto(qeind)};
            edgesucc(not(vedgeleaving{eto(qeind)})) = [];
            edgequeue = [edgequeue,edgesucc]; %#ok<AGROW>
        end
    end
    
    % shuffle paths so that the origin path is the first one
    pathstart = pathstart(:,[originpathind:end,1:originpathind-1]);
    pathend = pathend(:,[originpathind:end,1:originpathind-1]);
    pathstarttime = pathstarttime([originpathind:end,1:originpathind-1]);
    pathendtime = pathendtime([originpathind:end,1:originpathind-1]);
    if nargout >= 5
        old2newmap = zeros(1,size(pathstart,2));
        old2newmap([originpathind:end,1:originpathind-1]) = 1:size(pathstart,2);
        origintraj(3,:) = old2newmap(origintraj(3,:));
    end
    
    if reversed
        perm = [1,size(pathstart,2):-1:2];
        old2newmap = nan(1,size(pathstart,2));
        old2newmap(perm) = 1:size(pathstart,2);
        pathstart = pathstart(:,perm);
        pathend = pathend(:,perm);
        pathstarttime = pathstarttime(:,perm);
        pathendtime = pathendtime(:,perm);
        origintraj(3,:) = old2newmap(origintraj(3,:));
    end
    
    % add inner paths starting from einds
    function addInnerSkeletonInsetPaths(einds)
        
        edgequeue = einds;
        while not(isempty(edgequeue))
            % take first edge from the queue
            qeind = edgequeue(1);
            edgequeue(1) = [];
            
            % check if all edges arriving at the source vertex are done
            edgepred = vedges{efrom(qeind)};
            edgepred(vedgeleaving{efrom(qeind)}) = [];
            if not(any(outstandingmask(edgepred)))
                
                % add edge
                pathnum = size(pathstart,2);
                pathstart(:,pathnum+1) = [sx(efrom(qeind));sy(efrom(qeind))];
                pathend(:,pathnum+1) = [sx(eto(qeind));sy(eto(qeind))];
                pathstarttime(:,pathnum+1) = st(efrom(qeind));
                pathendtime(:,pathnum+1) = st(eto(qeind));
                pathdir(:,pathnum+1) = nan(2,1);
                
                outstandingmask(qeind) = false;
                edgetopathmap(qeind) = size(pathstart,2);
                
                % queue edge successors leaving the target vertex
                edgesucc = vedges{eto(qeind)};
                edgesucc(not(vedgeleaving{eto(qeind)})) = [];
                edgequeue = [edgequeue,edgesucc]; %#ok<AGROW>
            end
        end
    end

end
