% minimum and optionally maximum pairwise distances of a set of points to a set of axis aligned boxes
% (points inside boxes have distance 0 to the box)
function [closestdist,farthestdist] = pointAABoxDistance(p,boxmin,boxmax) %,cpx,cpy,fpx,fpy
    
    minvecx = bsxfun(@minus,boxmin(1,:),p(1,:)');
    minvecy = bsxfun(@minus,boxmin(2,:),p(2,:)');
    
    maxvecx = bsxfun(@minus,p(1,:)',boxmax(1,:));
    maxvecy = bsxfun(@minus,p(2,:)',boxmax(2,:));
    
    vecx = max(0,max(minvecx,maxvecx));
    vecy = max(0,max(minvecy,maxvecy));
    
    closestdist = sqrt(vecx.^2+vecy.^2);
    
%     if nargout >= 3
%         % this is only right if the point lies on the outside
%         cpx = bsxfun(@plus,p(1,:),vecx);
%         cpy = bsxfun(@plus,p(2,:),vecy);
%     end
    
    if nargout >= 2
        vecx = max(abs(minvecx),abs(maxvecx));
        vecy = max(abs(minvecy),abs(maxvecy));

        farthestdist = sqrt(vecx.^2+vecy.^2);
        
%         if nargout >= 5
%             fpx = bsxfun(@plus,p(1,:),vecx);
%             fpy = bsxfun(@plus,p(2,:),vecy);
%         end
    end
end
