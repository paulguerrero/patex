function p = posetransform(p,targetpose,currentpose,uniformscale)
    % if no current pose is given, assume it is for 2D:
    %    currentpose = [...
    %        0;0;... % position
    %        0;...   % rotation
    %        1;1;... % scale
    %        0];     % mirrored
    % or for 3D:
    %    currentpose = [...
    %        0;0;0;...   % position
    %        1;0;0;0;... % rotation
    %        1;1;1;...   % scale
    %        0];         % mirrored
    
    if isempty(p)
        return;
    end
    
    if nargin < 4
        uniformscale = false;
    end
    
    % convert to non-uniform scale format
    if uniformscale
        if size(targetpose,1) == 8 || size(targetpose,1) == 9
            targetpose = targetpose([1:7,8,8,8,9:end],:);
        elseif size(targetpose,1) == 4 || size(targetpose,1) == 5
            targetpose = targetpose([1:3,4,4,5:end],:);
        else
            error('Invalid pose format.');
        end
        if nargin >= 3 && not(isempty(currentpose))
            if size(currentpose,1) == 8 || size(currentpose,1) == 9
                currentpose = currentpose([1:7,8,8,8,9:end],:);
            elseif size(currentpose,1) == 4 || size(currentpose,1) == 5
                currentpose = currentpose([1:3,4,4,5:end],:);
            else
                error('Invalid pose format.');
            end
        end
        if size(p,1) == 8 || size(p,1) == 9
            p = p([1:7,8,8,8,9:end],:);
        elseif size(p,1) == 4 || size(p,1) == 5
            p = p([1:3,4,4,5:end],:);
        elseif size(p,1) == 2 || size(p,1) == 3
            % do nothing (points, not poses)
        else
            error('Invalid pose format.');
        end
    end
    
    if nargin >= 3 && not(isempty(currentpose))
        if size(targetpose,1) ~= size(currentpose,1)
            error('Pose format does not match for current and target poses.');
        end
        
        % avoid problems with scaling if both sizes are close to zero
        if size(currentpose,1) == 11 || size(currentpose,1) == 10
            mask = bsxfun(@and,abs(currentpose(8:10,:)) < eps('double')*100,abs(targetpose(8:10,:)) < eps('double')*100);
            if any(mask)
                % need to expand if there are not the same number of both
                % poses
                if size(currentpose,2) < size(targetpose,2)
                    if size(currentpose,2) ~= 1
                        error('Pose counts do not match.');
                    end
                    currentpose = currentpose(:,ones(1,size(targetpose,2)));
                end
                if size(targetpose,2) < size(currentpose,2)
                    if size(targetpose,2) ~= 1
                        error('Pose counts do not match.');
                    end
                    targetpose = targetpose(:,ones(1,size(currentpose,2)));
                end
                
                
                currentpose(8,mask(1,:)) = 1;
                targetpose(8,mask(1,:)) = 1;
                currentpose(9,mask(2,:)) = 1;
                targetpose(9,mask(2,:)) = 1;
                currentpose(10,mask(3,:)) = 1;
                targetpose(10,mask(3,:)) = 1;
            end
        elseif size(currentpose,1) == 6 || size(currentpose,1) == 5
            mask = bsxfun(@and,abs(currentpose(4:5,:)) < eps('double')*100,abs(targetpose(4:5,:)) < eps('double')*100);
            if any(mask)
                % need to expand if there are not the same number of both
                % poses
                if size(currentpose,2) < size(targetpose,2)
                    if size(currentpose,2) ~= 1
                        error('Pose counts do not match.');
                    end
                    currentpose = currentpose(:,ones(1,size(targetpose,2)));
                end
                if size(targetpose,2) < size(currentpose,2)
                    if size(targetpose,2) ~= 1
                        error('Pose counts do not match.');
                    end
                    targetpose = targetpose(:,ones(1,size(currentpose,2)));
                end
                
                currentpose(4,mask(1,:)) = 1;
                targetpose(4,mask(1,:)) = 1;
                currentpose(5,mask(2,:)) = 1;
                targetpose(5,mask(2,:)) = 1;
            end
        else
            error('Invalid pose format.');
        end
        
        if size(currentpose,1) == 11
            p = transform3Dinv(p,currentpose(8:10,:), currentpose(4:7,:), currentpose(1:3,:), currentpose(11,:));
        elseif size(currentpose,1) == 10
            p = transform3Dinv(p,currentpose(8:10,:), currentpose(4:7,:), currentpose(1:3,:));
        elseif size(currentpose,1) == 6
            % pose with mirroring
            p = transform2Dinv(p,currentpose(4:5,:), currentpose(3,:), currentpose(1:2,:), currentpose(6,:));
        elseif size(currentpose,1) == 5
            p = transform2Dinv(p,currentpose(4:5,:), currentpose(3,:), currentpose(1:2,:));
        else
            error('Invalid pose format.');
        end
    end

    if size(targetpose,1) >= 11
        % pose with mirroring
        p = transform3D(p,targetpose(8:10,:), targetpose(4:7,:), targetpose(1:3,:), targetpose(11,:));
    elseif size(targetpose,1) == 10
        p = transform3D(p,targetpose(8:10,:), targetpose(4:7,:), targetpose(1:3,:));
    elseif size(targetpose,1) == 6
        % pose with mirroring
        p = transform2D(p,targetpose(4:5,:), targetpose(3,:), targetpose(1:2,:), targetpose(6,:));
    elseif size(targetpose,1) == 5
        p = transform2D(p,targetpose(4:5,:), targetpose(3,:), targetpose(1:2,:));
    else
        error('Invalid pose format.');
    end
    
    % convert back to uniform scale format
    if uniformscale
        if size(p,1) == 10 || size(p,1) == 11
            p = p([1:8,11:end],:);
        elseif size(p,1) == 5 || size(p,1) == 6
            p = p([1:4,6:end],:);
        elseif size(p,1) == 3 || size(p,1) == 2
            % do nothing (return points, not poses)
        else
            error('Invalid pose format.');
        end
    end
end
