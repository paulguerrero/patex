classdef GuidedPropagationSolver < StandardPatternSolver
    
properties(SetAccess=protected)
    
    solver = StochasticLinearSolver.empty;
    solutionchoiceinds = zeros(1,0);
    startelement = zeros(1,0);

    choicestate = [];
    choicestatevar = [];
    choiceelmposes = cell(1,0);
    choicerelvals = cell(1,0);
    choicenodevisited = false(0,0);
    choicesolinds = cell(1,0);
    choicepqueue = cell(1,0);
    choiceregcost = zeros(1,0);
    choicesynccost = zeros(1,0);
    choiceiter = zeros(1,0);
    choiceclosed = false(1,0);
    choicetree = sparse([]); % rows contain childs
    
    visfunc = [];
end

methods(Static)

function s = defaultSettings
	s = StochasticLinearSolver.defaultSettings;
end
    
end

methods

% obj = GuidedPropagationSolver
% obj = GuidedPropagationSolver(obj2)
% obj = GuidedPropagationSolver(pgraph)
function obj = GuidedPropagationSolver(varargin)
    superarginds = zeros(1,0);
    if numel(varargin) == 1 && isa(varargin{1},'PatternGraph')
        superarginds = 1;
    end
    
    obj@StandardPatternSolver(varargin{superarginds});
    varargin(superarginds) = [];
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'GuidedPropagationSolver')
        obj.copyFrom(varargin{1});
    else
        error('Invalid arguments.');
    end
end

function delete(obj)
    obj.clear;
end

function copyFrom(obj,obj2)
    obj.copyFrom@StandardPatternSolver(obj2);
    
    
    obj.solver = obj2.solver;
    if not(isempty(obj.solver))
        obj.solver = obj2.solver.clone;
    end
    obj.solutionchoiceinds = obj2.solutionchoiceinds;
    
    obj.choicestate = obj2.choicestate;
    obj.choicestatevar = obj2.choicestatevar;
    obj.choiceelmposes = obj2.choiceelmposes;
    obj.choicerelvals = obj2.choicerelvals;
    obj.choicenodevisited = obj2.choicenodevisited;
    obj.choicesolinds = obj2.choicesolinds;
    obj.choicepqueue = obj2.choicepqueue;
    obj.choiceregcost = obj2.choiceregcost;
    obj.choicesynccost = obj2.choicesynccost;
    obj.choiceiter = obj2.choiceiter;
    obj.choiceclosed = obj2.choiceclosed;
    obj.choicetree = obj2.choicetree;
    
    obj.visfunc = obj2.visfunc;
end

function obj2 = clone(obj)
    obj2 = GuidedPropagationSolver(obj);
end

function clear(obj)
    obj.clear@StandardPatternSolver;
    
    delete(obj.solver(isvalid(obj.solver)));
    obj.solver = StochasticLinearSolver.empty;
    
    obj.clearChoices;
end

function clearChoices(obj)
    obj.solutionchoiceinds = zeros(1,0);
    obj.startelement = zeros(1,0);
    
    obj.choicestate = [];
    obj.choicestatevar = [];
    obj.choiceelmposes = cell(1,0);
    obj.choicerelvals = cell(1,0);
    obj.choicenodevisited = false(0,0);
    obj.choicesolinds = cell(1,0);
    obj.choicepqueue = cell(1,0);
    obj.choiceregcost = zeros(1,0);
    obj.choicesynccost = zeros(1,0);
    obj.choiceiter = zeros(1,0);
    obj.choiceclosed = false(1,0);
    obj.choicetree = sparse([]);
end

function clearFunctions(obj)
    % needed to save the solver
    obj.visfunc = [];
    obj.relfs = cell(1,0);
    if not(isempty(obj.solver)) && isvalid(obj.solver)
        obj.solver.clearFunctions;
    end
end

function setVisfunction(obj,func)
    obj.visfunc = func;
end

function prepare(obj,newelmposes,newrelvals)

    obj.prepare@StandardPatternSolver(newelmposes,newrelvals);
    
%     obj.solver = StochasticLinearSolver.empty;
    obj.solutionchoiceinds = zeros(1,0);
    
    obj.prepareChoices;
end

function prepareChoices(obj)
    nodecount = numel(obj.pgraph.nodes);
    statedim = numel(obj.patternstate);
    
    obj.choicestate = zeros(statedim,0);
    obj.choicestatevar = zeros(statedim,0);
    obj.choiceelmposes = cell(1,0);
    obj.choicerelvals = cell(1,0);
    obj.choicenodevisited = false(nodecount,0);
    obj.choicesolinds = cell(1,0);
    obj.choicepqueue = cell(1,0);
    obj.choiceregcost = zeros(1,0);    
    obj.choicesynccost = zeros(1,0);
    obj.choiceiter = zeros(1,0);
    obj.choiceclosed = false(1,0);
    obj.choicetree = sparse([]);
end

function updateChoices(obj,startelm)
    
    disp(['**** Building choice tree for ',num2str(size(obj.solutionstate,2)),' solutions...']);
    
    branchthresh = obj.settings.synctolerance*2;
    
    obj.clearChoices;
    obj.prepareChoices;
    
    obj.startelement = startelm;
    
    if isempty(obj.solutionstate)
        return;
    end
    
    changedelminds = find(any(obj.elmposechanged,1));
    changedelmnodeinds = changedelminds + obj.pgraph.elmoffset;
    changedelmstateinds = cat(1,obj.node2stateinds{changedelmnodeinds});
    
    state = obj.patternstate;
    statevar = obj.patternstatevar;
    
    state(changedelmstateinds) = obj.newelmposes(:,changedelminds);
    % variance for element poses does not change
    
    visitedmask = false(1,numel(obj.pgraph.nodes));
%     visitedmask(changedelmnodeinds) = true;
    
    pqueue = startelm;
    
    obj.choicestate(:,end+1) = state;
    obj.choicestatevar(:,end+1) = statevar;
    obj.choicenodevisited(:,end+1) = visitedmask;
    obj.choicesolinds{:,end+1} = (1:size(obj.solutionstate,2))'; % solutions that are still available in the branch of the choice tree
    obj.choicepqueue{:,end+1} = pqueue;
    obj.choiceclosed(:,end+1) = false;
    obj.choiceregcost(:,end+1) = 0;
    obj.choicesynccost(:,end+1) = 0;
    obj.choiceiter(:,end+1) = 1;
    obj.choicetree(1,1) = 0;

    while not(all(obj.choiceclosed))
        % pick first open choice
        choiceind = find(not(obj.choiceclosed),1,'first');
        
        state = obj.choicestate(:,choiceind);
        statevar = obj.choicestatevar(:,choiceind);
        visitedmask = obj.choicenodevisited(:,choiceind);
        solinds = obj.choicesolinds{choiceind};
        pqueue = obj.choicepqueue{choiceind};
        
        pqueue_elmmask = pqueue-obj.pgraph.elmoffset >= 1 & pqueue-obj.pgraph.elmoffset <= numel(obj.pgraph.elms);
        
        if not(any(pqueue_elmmask)) || numel(solinds) <= 1
            
            if numel(solinds) == 1
                state = obj.solutionstate(:,solinds);
                statevar = obj.solutionstatevar(:,solinds);
                visitedmask(:) = true;
                pqueue = zeros(1,0);
            end
            
            % set all unvisited relationships to the average of the
            % solutions (relationships should really be the same if all
            % elements are the same)
            if not(all(visitedmask))
                warning([...
                    'Some unvisited relationships left after visiting all elements, ',...
                    'setting them to the average of all solutions in the branch.']);
                
                unvisitedrelmask = not(visitedmask(obj.pgraph.rel2nodeinds));
                unvisitedrelnodeinds = obj.pgraph.rel2nodeinds(unvisitedrelmask);
                unvisitedrelstateinds = [...
                    cat(1,obj.node2stateinds{unvisitedrelnodeinds});...
                    obj.nodefixorig2stateinds(unvisitedrelnodeinds)';...
                    obj.nodefixnew2stateinds(unvisitedrelnodeinds)'];
                
                state(unvisitedrelstateinds) = mean(...
                    obj.solutionstate(unvisitedrelstateinds,solinds),2);
                statevar(unvisitedrelstateinds) = mean(...
                    obj.solutionstatevar(unvisitedrelstateinds,solinds),2);
                
                visitedmask(unvisitedrelnodeinds) = true;
                pqueue(obj.choicenodevisited(pqueue)) = [];
            end
            
            obj.choicestate(:,choiceind) = state;
            obj.choicestatevar(:,choiceind) = statevar;
            obj.choicenodevisited(:,choiceind) = visitedmask;
            obj.choicepqueue{choiceind} = pqueue;
            obj.choiceclosed(choiceind) = true;
            
%             disp(['choiceind: ',num2str(choiceind),' finished - solinds: ',num2str(solinds')]);
%             obj.outfun(state,statevar,visitedmask);
            
            continue;
        end
        
        choicesolstates = obj.solutionstate(:,solinds);
        choicesolstatevar = obj.solutionstatevar(:,solinds);
        
        % take the element that most solutions agree on (has the smallest
        % variance across solutions)
        pqueueelmstateinds = cat(1,obj.node2stateinds{pqueue(pqueue_elmmask)});
        
        intersolelmvar = nan(numel(pqueue),1);
        intersolelmvar(pqueue_elmmask,:) = ...
            mean(... % variance of nodefix state
                reshape(sum(reshape(...
                bsxfun(@minus,choicesolstates(pqueueelmstateinds,:), ...
                 mean(choicesolstates(pqueueelmstateinds,:),2)).^2 ...
                 ./ choicesolstatevar(pqueueelmstateinds,:),...
                 obj.settings.elmposedim,[]),1),[],numel(solinds)),...
                 2);
           
        [~,queueind] = min(intersolelmvar); % nans at relationships are ignored
        nodeind = pqueue(queueind);
        pqueue(queueind) = [];
        pqueue_elmmask(queueind) = [];
        
        visitedmask(nodeind) = true;
        
%         disp(['choiceind: ',num2str(choiceind),' - solinds: ',num2str(solinds')]);
%         obj.outfun(state,statevar,visitedmask);
        
        nodestateinds = cat(1,obj.node2stateinds{nodeind});        
        
        % difference between node values for the current solution
        intersoldiff = zeros(numel(solinds),numel(solinds));
        for i=1:numel(nodestateinds)
            intersoldiff = intersoldiff + bsxfun(@minus,...
                    choicesolstates(nodestateinds(i),:)',...
                    choicesolstates(nodestateinds(i),:)).^2 ./ ...
                (bsxfun(@plus,...
                    choicesolstatevar(nodestateinds(i),:)',...
                    choicesolstatevar(nodestateinds(i),:)).*0.5);
        end
        
        solbranchind = cluster(...
            linkage(squareform(intersoldiff,'tovector'),'complete'),...
            'cutoff',branchthresh,'criterion','distance');
        
        branchchoicesolinds = array2cell_mex((1:numel(solinds))',solbranchind);
        
        % for each distinct state, create a new branch in the choice tree
        for i=1:size(branchchoicesolinds,2)
            
            branchnodeinds = nodeind;
            
            % add relationships that are the same in all solutions of the branch
            % compute upper bound for the maximum pairwise distance between
            % a relationship in different solutions of this branch as
            % 2*maximum distance to the average solution value
            pqueuerelstateinds = cat(1,obj.node2stateinds{pqueue(not(pqueue_elmmask))});
            branchsolreldistbound = nan(numel(pqueue),1);
            branchsolreldistbound(not(pqueue_elmmask),:) = ...
                max(... % variance of nodefix state
                    bsxfun(@minus,choicesolstates(pqueuerelstateinds,branchchoicesolinds{i}), ...
                     mean(choicesolstates(pqueuerelstateinds,branchchoicesolinds{i}),2)).^2 ...
                     ./ choicesolstatevar(pqueuerelstateinds,branchchoicesolinds{i}),...
                     [],2) .* 2;
                 
            relnodeinds = pqueue(branchsolreldistbound < obj.settings.synctolerance * 2);
            
            branchnodeinds = unique([branchnodeinds,relnodeinds]);
            
            branchnodestateinds = [...
                cat(1,obj.node2stateinds{branchnodeinds});...
                obj.nodefixorig2stateinds(branchnodeinds)';...
                obj.nodefixnew2stateinds(branchnodeinds)'];
            % todo: grpfix2stateinds are never set in the choice state (but
            % the choice state is not really needed)
            
            branchvisitedmask = visitedmask;
            branchvisitedmask(branchnodeinds) = true;
            
            avgbranchsolstate = mean(choicesolstates(branchnodestateinds,branchchoicesolinds{i}),2);
            avgbranchsolstatevar = mean(choicesolstatevar(branchnodestateinds,branchchoicesolinds{i}),2);
            
            branchstate = state;
            branchstatevar = statevar;
            branchstate(branchnodestateinds,:) = avgbranchsolstate;
            branchstatevar(branchnodestateinds,:) = avgbranchsolstatevar;
            
            % add all unvisited nodes to the queue
            % todo: might also experiment with adding only close nodes
            % (graph distance)
            branchqueued = setdiff(find(not(branchvisitedmask))',pqueue);
            branchpqueue = [pqueue,branchqueued];
            branchpqueue(branchvisitedmask(branchpqueue)) = [];
            
%             if i==1
%                 branchchoiceind = choiceind;
%             else
%                 branchchoiceind = size(obj.choicestate,2)+1;
%             end
            if size(branchchoicesolinds,2) == 1
                % does not branch
                branchchoiceind = choiceind;
            else
                % branches
                branchchoiceind = size(obj.choicestate,2)+1;
                obj.choiceclosed(choiceind) = true;
            end

            obj.choicestate(:,branchchoiceind) = branchstate;
            obj.choicestatevar(:,branchchoiceind) = branchstatevar;
            obj.choicenodevisited(:,branchchoiceind) = branchvisitedmask;
            obj.choicesolinds{:,branchchoiceind} = solinds(branchchoicesolinds{i});
            obj.choicepqueue{:,branchchoiceind} = branchpqueue;
            obj.choiceclosed(:,branchchoiceind) = false;
            obj.choiceregcost(:,branchchoiceind) = 0;
            obj.choicesynccost(:,branchchoiceind) = 0;
            obj.choiceiter(:,branchchoiceind) = obj.choiceiter(choiceind) + 1;
            if size(branchchoicesolinds,2) == 1
                % no branching, choice tree does not change
            else
                obj.choicetree(branchchoiceind,choiceind) = 1;
                obj.choicetree(:,branchchoiceind) = 0;
            end
            
%             disp(['choiceind: ',num2str(choiceind),' - branch: ',num2str(i),' - solinds: ',num2str(solinds(branchchoicesolinds{i})')]);
%             obj.outfun(branchstate,branchstatevar,branchvisitedmask);
        end
    end
    
    % get element poses and relationship values from choice states
    obj.choiceelmposes = cell(1,size(obj.choicestate,2));
    obj.choicerelvals = cell(1,size(obj.choicestate,2));
    for i=1:size(obj.choicestate,2)
        eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        eposes(1:obj.settings.elmposedim,:) = reshape(obj.choicestate(obj.elmposestateinds(:),i),obj.settings.elmposedim,numel(obj.pgraph.elms));
        eposes = PatternSolver.sim2affinepose(eposes);
        
        % update all element instances
        posediff = eposes - [obj.pgraph.elms.pose2D];
        posediff(3:4,:) = smod(posediff(3:4,:),-pi,pi);
        elmchanged = any(bsxfun(@rdivide,...
            posediff.^2,...
            obj.pgraph.elmposevar) > ...
            obj.settings.synctolerance*0.01,1);
        eposes = PatternWidget.updateInstances(eposes,obj.pgraph,1:size(obj.pgraph.instgroups,2),elmchanged);
        
        obj.choiceelmposes{i} = eposes;
        obj.choicerelvals{i} = obj.choicestate(obj.relstateinds,i)'; % todo: relvals need to be recomputed after instancing etc.
    end
    
%     % get element poses and relationship values from solution states
%     obj.solutionelmposes = cell(1,size(obj.solutionstate,2));
%     obj.solutionrelvals = cell(1,size(obj.solutionstate,2));
%     for i=1:size(obj.solutionstate,2)
%         eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
%         eposes(1:obj.settings.elmposedim,:) = reshape(obj.solutionstate(obj.elmposestateinds(:),i),obj.settings.elmposedim,numel(obj.pgraph.elms));
%         eposes = PatternSolver.sim2affinepose(eposes);
%         obj.solutionelmposes{i} = eposes;
%         obj.solutionrelvals{i} = obj.solutionstate(obj.relstateinds,i)';
%     end
    
    % get choice index for each solution
    leafchoiceinds = find(not(any(obj.choicetree,1)));
    obj.solutionchoiceinds = nan(1,size(obj.solutionstate,2));
    solleafchoiceind = cellind_mex(obj.choicesolinds(leafchoiceinds));
    obj.solutionchoiceinds(:,cat(1,obj.choicesolinds{leafchoiceinds})) = leafchoiceinds(cat(1,solleafchoiceind{:}));

    disp('done');
end

% [selmposes,srelvals,scost] = solve(obj,newelmposes,newrelvals,editedelminds,startelm)
% [selmposes,srelvals,scost] = solve(obj,newelmposes,newrelvals,editedelminds,lsolver,startelm)
% [selmposes,srelvals,scost] = solve(obj,lsolver,startelm)
% [selmposes,srelvals,scost] = solve(...,nvpairs)
function [selmposes,srelvals,scost] = solve(obj,varargin)
    
    % options: 
    % - move one element or element group at a time
    % or: move until reaching a relationship that is different in different
    % solutions (propagate to the relationship that are the same in every
    % solution first?)
    % - if the user chooses one of the proposed edits, fix the relations
    % that were visited to get to that edit and remove them from the
    % combination generation? => no, user might not want, that. user sees
    % only element positions, might want different pose for an element that
    % would be fixed by these relationships => only fix relationships that
    % are directly fixed by the elements (add that to the combination
    % validation?)
    % - changes are always propagated to the entire group? (to be
    % consistent with the combination generation)
    % - go as fast as possible to the next ambiguity (maybe shouldn't fix
    % things that are the same everywhere before that, because we can't be
    % sure we haven't missed a solution)
    % output: - tree of states (choice tree), leafs are solutions found by
    % linear solver, cut tree at some height(s) to get solution
    % recommendations
    
    
    % steps:
    % user moves element
    
    % for a given set of fixed elements and a currently selected element
    % (the start of the propagation): generate a set of linear & nonlinear
    % solutions with the changed elements fixed, then start propagating
    % towards these solutions, stop when propagation diverges (or after
    % moving an element/element group?), return the set of partial
    % solutions (which include where the selected element moves to)
    
    
    
    obj.clear;
    
    if numel(varargin) >= 2 && isa(varargin{1},'StandardPatternSolver')
        % use existing solution
        
        obj.solver = varargin{1}.clone;
        varargin(1) = [];
        obj.setPatterngraph(obj.solver.pgraph);
        
        startelm = varargin{2};
        varargin(2) = [];
        
        optionalargs = struct(...
            'fixedelminds',zeros(1,0),...
            'combinations',zeros(0,0));
        [~,additionalargmask] = nvpairs2struct(varargin,optionalargs,false,0);
        varargin(additionalargmask) = [];
        
        obj.settings = nvpairs2struct(varargin,obj.settings);
        
        eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        eposes(1:obj.settings.elmposedim,:) = reshape(obj.solver.newelmposes,obj.settings.elmposedim,numel(obj.pgraph.elms));
        newelmposes = PatternSolver.sim2affinepose(eposes);
        newrelvals = obj.solver.newrelvals;
        
    elseif numel(varargin) >= 3 && isnumeric(varargin{1})
        % solve anew
        
        newelmposes = varargin{1};
        newrelvals = varargin{2};
        editedelminds = varargin{3};
        varargin(1:3) = [];
        
        if isempty(newelmposes) || isempty(newrelvals)
            error('Pattern without elements or without relationships.');
        end
        
        if numel(varargin) >= 1 && isa(varargin{1},'StandardPatternSolver')
            obj.solver = varargin{1}.clone;
            varargin(1) = [];
            obj.setPatterngraph(obj.solver.pgraph);
        else
            obj.solver = StochasticLinearSolver(obj.pgraph);
        end
        
        if numel(varargin) >= 1 && isnumeric(varargin{1})
            startelm = varargin{1};
            varargin(1) = [];
        else
            error('Invalid arguments.');
        end
        
        optionalargs = struct(...
            'fixedelminds',zeros(1,0),...
            'combinations',zeros(0,0));
        [optionalargs,additionalargmask] = nvpairs2struct(varargin,optionalargs,false,0);
        varargin(additionalargmask) = [];
        
        obj.settings = nvpairs2struct(varargin,obj.settings);
        
        solversettings = struct2nvpairs(obj.settings);
        solveradditionalargs = struct2nvpairs(optionalargs);
        
        obj.solver.setVisfunction(obj.visfunc);
        obj.solver.solve(newelmposes,newrelvals,editedelminds,solveradditionalargs{:},solversettings{:});    


%         obj.solutionstate = lsolver.solutionstate;
%         obj.solutionstartstate = lsolver.solutionstartstate;
%         obj.solutionstatevar = lsolver.solutionstatevar;
%         obj.solutioncost = lsolver.solutioncost;
%         obj.solutionsubstateinds = lsolver.solutionsubstateinds;
%         obj.solutionactiveconsrelinds = lsolver.solutionactiveconsrelinds;
%         obj.solutionactivefixnodeinds = lsolver.solutionactivefixnodeinds;
%         obj.solutionactiveregnodeinds = lsolver.solutionactiveregnodeinds;
%         obj.solutioncombs = lsolver.solutioncombs;
    else
        error('Invalid arguments.');
    end
    
    obj.prepare(newelmposes,newrelvals);
    
    obj.solutionstate = obj.solver.solutionstate;
    obj.solutionstatevar = obj.solver.solutionstatevar;
    obj.solutioncost = obj.solver.solutioncost;
    obj.solutionelmposes = obj.solver.solutionelmposes;
    obj.solutionrelvals = obj.solver.solutionrelvals;
    
    % solve non-linear on demand, as user gets close to choosing that path
    
    % output: these states ordered in a tree based on propagation order

    
    % put nodes in queue if they have not been visited (visited nodes are
    % always set to the correct values - the values in the solution) and
    % are siblings, parents, or childs of the current node
    % check if rel. group is not free, if it
    % isn't, set whole rel. group when setting a rel. node (do not check
    % combination to do that, the combination might have set the group to
    % free, but in the final result all rel. got the same value)
    
    obj.updateChoices(startelm);
    
    selmposes = obj.solutionelmposes;
    srelvals = obj.solutionrelvals;
    scost = obj.solutioncost;
end

% function [selmposes,srelvals,scost] = solve2(obj,newelmposes,newrelvals,startelm)
%     
%     % options: 
%     % - move one element or element group at a time
%     % or: move until reaching a relationship that is different in different
%     % solutions (propagate to the relationship that are the same in every
%     % solution first?)
%     % - if the user chooses one of the proposed edits, fix the relations
%     % that were visited to get to that edit and remove them from the
%     % combination generation? => no, user might not want, that. user sees
%     % only element positions, might want different pose for an element that
%     % would be fixed by these relationships => only fix relationships that
%     % are directly fixed by the elements (add that to the combination
%     % validation?)
%     % - changes are always propagated to the entire group? (to be
%     % consistent with the combination generation)
%     % - go as fast as possible to the next ambiguity (maybe shouldn't fix
%     % things that are the same everywhere before that, because we can't be
%     % sure we haven't missed a solution)
%     % output: - tree of states (choice tree), leafs are solutions found by
%     % linear solver, cut tree at some height(s) to get solution
%     % recommendations
%     
%     
%     % steps:
%     % user moves element
%     
%     % for a given set of fixed elements and a currently selected element
%     % (the start of the propagation): generate a set of linear & nonlinear
%     % solutions with the changed elements fixed, then start propagating
%     % towards these solutions, stop when propagation diverges (or after
%     % moving an element/element group?), return the set of partial
%     % solutions (which include where the selected element moves to)
%     
%     branchthresh = obj.settings.synctolerance*2;
%     
%     obj.clear;
% 
%     obj.prepare(newelmposes,newrelvals);
%     
%     lsolver = StochasticLinearSolver(obj.pgraph);
%     lsolver.solve(newelmposes,newrelvals);
%     % solve non-linear on demand, as user gets close to choosing that path
%     
%     % output: these states ordered in a tree based on propagation order
%     obj.solutionstate = lsolver.solutionstate;
%     obj.solutionstatevar = lsolver.solutionstatevar;
%     
%     % put nodes in queue if they have not been visited (visited nodes are
%     % always set to the correct values - the values in the solution) and
%     % are siblings, parents, or childs of the current node
%     % check if rel. group is not free, if it
%     % isn't, set whole rel. group when setting a rel. node (do not check
%     % combination to do that, the combination might have set the group to
%     % free, but in the final result all rel. got the same value)
%     
%     changedelminds = find(any(obj.elmposechanged,1));
%     changedelmnodeinds = changedelminds + obj.pgraph.elmoffset;
%     changedelmstateinds = cat(1,obj.node2stateinds{changedelmnodeinds});
%     
%     state = obj.patternstate;
%     statevar = obj.patternstatevar;
%     
%     state(changedelmstateinds) = newelmposes(:,changedelminds);
%     % variance for element poses does not change
%     
%     visitedmask = false(1,numel(obj.pgraph.nodes));
% %     visitedmask(changedelmnodeinds) = true;
%     
% %     pqueue = unique([obj.parentnodeinds{visitedmask},obj.childnodeinds{visitedmask},obj.siblingnodeinds{visitedmask}]);
% %     pqueue = unique([obj.parentnodeinds{startelm},obj.childnodeinds{startelm},obj.siblingnodeinds{startelm}]);
%     pqueue = startelm;
% %     pqueue(visitedmask(pqueue)) = [];
% %     pqueue(obj.nodefixorig2stateinds(pqueue) > 0.5) = []; % don't queue fixed nodes
%     % todo: remove unchanged nodes
%     
%     obj.choicestate(:,end+1) = state;
%     obj.choicestatevar(:,end+1) = statevar;
%     obj.choicenodevisited(:,end+1) = visitedmask;
%     obj.choicesolinds{:,end+1} = 1:size(obj.solutionstate,2); % solutions that are still available in the branch of the choice tree
%     obj.choicepqueue{:,end+1} = pqueue;
%     obj.choiceclosed(:,end+1) = false;
%     obj.choiceregcost(:,end+1) = 0;
%     obj.choicesynccost(:,end+1) = 0;
%     obj.choiceiter(:,end+1) = 1;
%     obj.choicetree(1,1) = 1;
%     
%     while not(all(obj.choiceclosed))
%         % pick first open choice
%         choiceind = find(not(obj.choiceclosed),1,'first');
%         
%         state = obj.choicestate(:,choiceind);
%         statevar = obj.choicestatevar(:,choiceind);
%         visitedmask = obj.choicenodevisited(:,choiceind);
%         solinds = obj.choicesolinds{choiceind};
%         pqueue = obj.choicepqueue{choiceind};
%         
%         if isempty(pqueue)
%             
%             % todo: if queue is empty, but there are still some unvisited
%             % and changed nodes left, add the closest changed node to the
%             % list (close by graph distance)
%             % or: just always use the closest node by graph distance?
%             % (where siblings have distance 1, too => = constraint
%             % hypergraph distance) - but: would neet to get hypergraph
%             % distance, easier the othe way
%             
%             obj.choiceclosed(choiceind) = true;
%             continue;
%         end
%         
%         % only propagate to nodes that are actually changed in at least one
%         % of the active solutions
%         
%         
%         
%         % take the node most states agree on
%         choicesolstates = obj.solutionstate(:,obj.choicesolinds{choiceind});
%         choicesolstatevar = obj.solutionstatevar(:,obj.choicesolinds{choiceind});
%         
%         pqueue_elmmask = pqueue-obj.pgraph.elmoffset >= 1 & pqueue-obj.pgraph.elmoffset <= numel(obj.pgraph.elms);
%         
%         intersolvar = nan(numel(pqueue),1);
%         
%         pqueuegrpmask = logical(obj.pgraph.groups(pqueue,:));
%         pqueuegrpinds = find(any(pqueuegrpmask,1));
%         
%         % element variance
%         pqueueelmstateinds = cat(1,obj.node2stateinds{pqueue(pqueue_elmmask)});
%         intersolvar(pqueue_elmmask,:) = ...
%             mean(... % variance of nodefix state
%                 reshape(sum(reshape(...
%                 bsxfun(@minus,choicesolstates(pqueueelmstateinds,:), ...
%                  mean(choicesolstates(pqueueelmstateinds,:),2)).^2 ...
%                  ./ choicesolstatevar(pqueueelmstateinds,:),...
%                  obj.settings.elmposedim,[]),1),[],numel(solinds)),...
%                  2);
%         % relationship variance
%         pqueuerelstateinds = cat(1,obj.node2stateinds{pqueue(not(pqueue_elmmask))});
%         intersolvar(not(pqueue_elmmask),:) = ...
%             mean(... % variance of nodefix state
%                 bsxfun(@minus,choicesolstates(pqueuerelstateinds,:), ...
%                  mean(choicesolstates(pqueuerelstateinds,:),2)).^2 ...
%                  ./ choicesolstatevar(pqueuerelstateinds,:),...
%                  2);
%         % add group fixed variance of all groups each node is part of
%         intersolgrpvars = ...
%              mean(... % variance of grpfix state
%                 bsxfun(@minus,choicesolstates(obj.grpfix2stateinds(pqueuegrpinds),:), ...
%                 mean(choicesolstates(obj.grpfix2stateinds(pqueuegrpinds),:),2)).^2, 2);
%         for i=1:numel(pqueue)
%             nodegrpmask = logical(obj.pgraph.groups(pqueue(i),pqueuegrpinds));
%             intersolvar(i) = intersolvar(i) + sum(intersolgrpvars(nodegrpmask));
%         end
%            
%         [~,queueind] = min(intersolvar);
%         nodeind = pqueue(queueind);
%         pqueue(queueind) = [];
%         
%         visitedmask(nodeind) = true;
%         
%         disp(['choiceind: ',num2str(choiceind)]);
%         obj.outfun(state,statevar,visitedmask);
%         
% %         % check how many different states there are in the solutions for
% %         % the current node
% %         nodesolfixstate = [...
% %             round(choicesolstates(obj.nodefix2stateinds(nodeind),:));...
% %             round(choicesolstates(obj.grpfix2stateinds(nodeind),:))];
% %         [nodesolfixstate,~,solbranchind] = unique(nodesolfixstate','rows');
% 
%         nodestateinds = cat(1,obj.node2stateinds{nodeind});        
%         nodegrpinds = find(obj.pgraph.groups(nodeind,:));
%         
%         % difference between node values for the current solution
%         intersoldiff = zeros(numel(solinds),numel(solinds));
%         for i=1:numel(nodestateinds)
%             intersoldiff = intersoldiff + bsxfun(@minus,...
%                     choicesolstates(nodestateinds(i),:)',...
%                     choicesolstates(nodestateinds(i),:)).^2 ./ ...
%                 (bsxfun(@plus,...
%                     choicesolstatevar(nodestateinds(i),:)',...
%                     choicesolstatevar(nodestateinds(i),:)).*0.5);
%         end
%         for i=1:numel(nodegrpinds)
%             intersoldiff = intersoldiff + bsxfun(@minus,...
%                 choicesolstates(obj.grpfix2stateinds(nodegrpinds(i)),:)',...
%                 choicesolstates(obj.grpfix2stateinds(nodegrpinds(i)),:)).^2;            
%         end
%         
%         solbranchind = cluster(...
%             linkage(squareform(intersoldiff,'tovector'),'complete'),...
%             'cutoff',branchthresh,'criterion','distance');
%         
%         branchchoicesolinds = array2cell_mex((1:numel(solbranchind))',solbranchind);
%         
%         % for each distinct state, create a new branch in the choice tree
%         for i=1:size(branchchoicesolinds,2)
%             
%             branchnodeinds = nodeind;
%             % for all fixed groups add their members to the branchnodeinds
%             for j=1:numel(nodegrpinds)
%                 if mean(choicesolstates(obj.grpfix2stateinds(nodegrpinds(j)),branchchoicesolinds{i}),2) > 0.5
%                     grpmembernodeinds = find(obj.pgraph.groups(:,nodegrpinds(j)))';
%                     branchnodeinds = [branchnodeinds,grpmembernodeinds]; %#ok<AGROW>
%                 end
%             end
%             branchnodeinds = unique(branchnodeinds);
%             branchnodestateinds = [...
%                 cat(1,obj.node2stateinds{branchnodeinds});...
%                 obj.nodefixorig2stateinds(branchnodeinds)';...
%                 obj.nodefixnew2stateinds(branchnodeinds)';...
%                 obj.grpfix2stateinds(nodegrpinds)'];
%             
%             branchvisitedmask = visitedmask;
%             branchvisitedmask(branchnodeinds) = true;
%             
%             avgbranchsolstate = mean(choicesolstates(branchnodestateinds,branchchoicesolinds{i}),2);
%             avgbranchsolstatevar = mean(choicesolstatevar(branchnodestateinds,branchchoicesolinds{i}),2);
%             
%             % todo: what if the solutions are very different? => should
%             % only happen if nodes are set to free => also check the actual
%             % value of these nodes in this case (if they are different)
%             
%             branchstate = state;
%             branchstatevar = statevar;
%             branchstate(branchnodestateinds,:) = avgbranchsolstate;
%             branchstatevar(branchnodestateinds,:) = avgbranchsolstatevar;
%             
%             branchqueued = setdiff([obj.parentnodeinds{branchnodeinds},obj.childnodeinds{branchnodeinds},obj.siblingnodeinds{branchnodeinds}],pqueue);
%             branchqueued(branchvisitedmask(branchqueued)) = [];
% %             branchqueued(obj.nodefixorig2stateinds(branchpqueue) > 0.5) = []; % don't queue fixed nodes
%             branchpqueue = [pqueue,branchqueued];
%             
%             if i==1
%                 branchchoiceind = choiceind;
%             else
%                 branchchoiceind = size(obj.choicestate,2)+1;
%             end
%             
% %             unique([pqueue,obj.parentnodeinds{branchnodeinds},obj.childnodeinds{branchnodeinds},obj.siblingnodeinds{branchnodeinds}]);
%             obj.choicestate(:,branchchoiceind) = branchstate;
%             obj.choicestatevar(:,branchchoiceind) = branchstatevar;
%             obj.choicenodevisited(:,branchchoiceind) = branchvisitedmask;
%             obj.choicesolinds{:,branchchoiceind} = solinds(branchchoicesolinds{i});
%             obj.choicepqueue{:,branchchoiceind} = branchpqueue;
%             obj.choiceclosed(:,branchchoiceind) = false;
%             obj.choiceregcost(:,branchchoiceind) = 0;
%             obj.choicesynccost(:,branchchoiceind) = 0;
%             obj.choiceiter(:,branchchoiceind) = obj.choiceiter(choiceind) + 1;
%             obj.choicetree(branchchoiceind,choiceind) = 1;
%             obj.choicetree(:,branchchoiceind) = 0;
%         end
%         
% %         obj.choiceclosed(:,choiceind) = true;
%     end
%     
%     % get element poses and relationship values from states
%     obj.solutionelmposes = cell(1,size(obj.solutionstate,2));
%     obj.solutionrelvals = cell(1,size(obj.solutionstate,2));
%     for i=1:size(obj.solutionstate,2)
%         eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
%         eposes(1:obj.settings.elmposedim,:) = reshape(obj.solutionstate(obj.elmposestateinds(:),i),obj.settings.elmposedim,numel(obj.pgraph.elms));
%         eposes = PatternSolver.sim2affinepose(eposes);
%         obj.solutionelmposes{i} = eposes;
%         obj.solutionrelvals{i} = obj.solutionstate(obj.relstateinds,i)';
%     end
%     
%     % get choice index for each solution
%     obj.solutionchoiceinds([obj.choicesolinds{:}]) = cell2mat(cellind_mex(obj.choicesolinds));
%     
%     selmposes = obj.solutionelmposes;
%     srelvals = obj.solutionrelvals;
%     scost = obj.solutioncost;
% end

function stop = outfun(obj,fullstate,fullstatevar,visitedmask,substate,substateinds,iter,fval) %#ok<INUSL>
%     stop = false;
%     return; % temp
    
    if nargin < 5
        substate = zeros(0,1);
        substateinds = zeros(0,1);
    end

    state = fullstate;
    state(substateinds) = substate;
    
    if nargin >= 8
        disp(['*** iteration ',num2str(iter),':']);
        disp(['    objective function value: ',num2str(fval)]);
    end
    
    if not(isempty(obj.visfunc))
        elmposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        
%         elmoffset = obj.pgraph.elmoffset;
%         elmstateinds = cat(1,obj.node2stateinds{elmoffset+1 : elmoffset+numel(obj.pgraph.elms)});
        
        elmposes(1:obj.settings.elmposedim,:) = reshape(state(obj.elmposestateinds),obj.settings.elmposedim,[]);
        
        nodefixorig = state(obj.nodefixorig2stateinds);
        nodefixnew = state(obj.nodefixnew2stateinds);
        nodevisited = visitedmask;
        
        elmposes = PatternSolver.sim2affinepose(elmposes);
        
        if nargin(obj.visfunc) == 5
            stop = obj.visfunc(...
                obj,...
                elmposes,...
                nodefixorig,...
                nodefixnew,...
                nodevisited);
        elseif nargin(obj.visfunc) == 4
            stop = obj.visfunc(...
                obj,...
                elmposes,...
                nodefixorig,...
                nodefixnew);
        else
            error('Incompatible visualization function.');
        end
        
    else
        stop = false;
    end
end

end

end
