classdef PatternEditorFigure < EditorFigure
    
properties
    % widgets
    navwidget = NavigationWidget.empty;
    pwidget = PatternWidget.empty;
    
    autocompletepatterneditor = AutocompletePatternEditor.empty;
    varexplorepatterneditor = VarexplorePatternEditor.empty;
    
    patterngrapheditor = PatternGraphEditor.empty;
    
    helpMenu_version = gobjects(1,1);
end

properties(Access=protected)
    
end

methods
    
function obj = PatternEditorFigure
    
    obj@EditorFigure;
    
    % UI settings
    
    obj.fig.Position = [20,60,1920,1080];
%     obj.fig.Position = [20,20,1280,720];
    %
    settings = struct(...
        'color_elements',rgbhex2rgbdec('A5A5A5'),... %737373 %4D4D4D
        'color_selectedelements',rgbhex2rgbdec('2b8cbe'),...
        'color_draggedelements',rgbhex2rgbdec('2b8cbe'),...
        'color_changedelements',rgbhex2rgbdec('2b8cbe'),...%1d91c0
        'color_fixedelements',rgbhex2rgbdec('2b8cbe'),... % 54278f
        'color_linkedelements',rgbhex2rgbdec('7fcdbb'),...
        'color_selectedgroups',rgbhex2rgbdec('7bccc4'),...
        'blendoutstrength_hierarchy',0.8,...
        'linewidth_overlay',2,...
        'color_overlay',rgbhex2rgbdec('fd8d3c'),...
        'color_choicecircle',rgbhex2rgbdec('e31a1c'),...
        'color_choicecircle_selected',rgbhex2rgbdec('fd8d3c'),...
        'radius_choicecircle',5,...
        'radius_choicecircle_selected',10,...
        'blendoutstrength_overlay',0.9,...
        'color_sidepanelsolution_background',rgbhex2rgbdec('fcfbfd'),...
        'color_sidepanelsolution_backgroundcolor_selected',rgbhex2rgbdec('edf8e9'),...
        'color_sidepanelbackground',rgbhex2rgbdec('737373'),...
        'color_sidepanelrow_selected',rgbhex2rgbdec('525252'),...
        'color_sidepanelsolution_margin',rgbhex2rgbdec('31a354'),...
        'color_sidepanelmodelements',rgbhex2rgbdec('A05AA5'),... % '54278f'
        'linewidth_selectedelements',2);
    
    obj.helpMenu_version = uimenu(obj.helpMenu,...
        'Label','Version 0.8',...
        'Checked','off',...
        'Enable','off');
    
    % doesn't seem to work - need to do it manually
%     obj.fig.BusyAction = 'cancel';
%     obj.fig.Interruptible = 'off';
    
    
    inputargs = struct2nvpairs(settings);
    
    if obj.toolpanelVisible
        obj.toggleToolpanelVisible;
    end
    if not(obj.navpanelVisible)
        obj.toggleNavpanelVisible;
    end
    
    
    % create menus struct
    menus = struct;
    for i=1:numel(obj.fig.Children)
        if strcmp(obj.fig.Children(i).Type,'uimenu')
            menus.(obj.fig.Children(i).Label) = obj.fig.Children(i);
        end
    end
    
    % create widgets and editors
    obj.navwidget = NavigationWidget(obj.mainaxes,...
        obj.navpanel);
    obj.pwidget = PatternWidget(obj.mainaxes,...
        obj.navwidget,obj,obj.fig,obj.toolpanel,obj.mainpanel,menus,inputargs{:});
%     editorfig
        
    obj.autocompletepatterneditor = AutocompletePatternEditor(obj.mainaxes,...
        obj.pwidget,menus,inputargs{:});
    obj.varexplorepatterneditor = VarexplorePatternEditor(obj.mainaxes,...
        obj.pwidget,menus,inputargs{:});
    
    obj.patterngrapheditor = PatternGraphEditor(obj.mainaxes,...
        obj.pwidget,obj.toolpanel,menus);
    
    obj.addEditor(obj.autocompletepatterneditor);
    obj.addEditor(obj.varexplorepatterneditor);

    obj.addEditor(obj.patterngrapheditor,true);
    
    obj.seleditor = obj.varexplorepatterneditor;
    resetCamera(obj.mainaxes);
end

function delete(obj)
    % delete editors and widgets
    if not(isempty(obj.patterngrapheditor))
        delete(obj.patterngrapheditor(isvalid(obj.patterngrapheditor)));
    end
    
    if not(isempty(obj.varexplorepatterneditor))
        delete(obj.varexplorepatterneditor(isvalid(obj.varexplorepatterneditor)));
    end
    if not(isempty(obj.autocompletepatterneditor))
        delete(obj.autocompletepatterneditor(isvalid(obj.autocompletepatterneditor)));
    end
    
    if not(isempty(obj.pwidget))
        delete(obj.pwidget(isvalid(obj.pwidget)));
    end
    if not(isempty(obj.navwidget))
        delete(obj.navwidget(isvalid(obj.navwidget)));
    end
end

end

end
