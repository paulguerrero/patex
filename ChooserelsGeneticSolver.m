classdef ChooserelsGeneticSolver < StandardPatternSolver
    
properties(SetAccess=protected)
    % last solution
    solutioncost = zeros(1,0);
    solutionstate = [];
    solutionstatevar = [];
    solutionelmposes = cell(1,0);
    solutionrelvals = cell(1,0);
    
%     synctolerance = 0.001;

    visualize = 'all';
%     visualize = 'steps';
%     visualize = 'solutions';
%     visualize = 'none';
end

properties(Access=protected)
    visfunc = [];
end

methods

% obj = ChooserelsGeneticSolver
% obj = ChooserelsGeneticSolver(pgraph)
function obj = ChooserelsGeneticSolver(varargin)
    superarginds = zeros(1,0);
    if numel(varargin) == 1
        superarginds = 1;
    end
    
    obj@StandardPatternSolver(varargin{superarginds});
    varargin(superarginds) = [];
    
    if numel(varargin) == 0
        % do nothing
    else
        error('Invalid arguments.');
    end
end

function clear(obj)
    obj.clear@StandardPatternSolver;
end

function prepare(obj,newelmposes,newrelvals)

    obj.prepare@StandardPatternSolver(newelmposes,newrelvals);
    
    obj.solutioncost = zeros(1,0);
    obj.solutionstate = zeros(numel(obj.patternstate),0);
    obj.solutionstatevar = zeros(numel(obj.patternstate),0);
    obj.solutionelmposes = cell(1,0);
    obj.solutionrelvals = cell(1,0);
    
end

function [selmposes,srelvals,scost] = solve(obj,newelmposes,newrelvals)
    
    obj.clear;

    obj.prepare(newelmposes,newrelvals);
    
    % reg. weights for interpretation 'rotate around central element'
    obj.regw_relfixed = 10;
    obj.regw_relgrpschanged = 10;
    obj.regw_elmgrpschanged = 50;
    obj.regw_grpsbroken = 300;
    
%     % reg. weights for interpretation 'move the whole pattern'
%     obj.regw_relfixed = 100;
%     obj.regw_relgrpschanged = 100;
%     obj.regw_elmgrpschanged = 1;
%     obj.regw_grpsbroken = 300;
    
    state = obj.patternstate;
    statevar = obj.patternstatevar;

    % modify state with the element changed by the user
    elmoffset = obj.pgraph.elmoffset;
    userchangedelmind = find(any(obj.origelmposes ~= obj.newelmposes,1));
    userchangednodeind = userchangedelmind + elmoffset;
    state(obj.elmposestateinds(:,userchangedelmind)) = obj.newelmposes(:,userchangedelmind);
    state(obj.nodefixorig2stateinds(userchangednodeind)) = 0;
    state(obj.nodefixnew2stateinds(userchangednodeind)) = 1;

    obj.fullstate = state;
    obj.fullstatevar = statevar;
    obj.substateinds = (1:numel(state))';
    obj.substateinds(ismember(obj.substateinds,obj.nodefixorig2stateinds(userchangednodeind))) = []; % do not change the fixed status of the element changed by the user
    obj.substateinds(ismember(obj.substateinds,obj.nodefixnew2stateinds(userchangednodeind))) = []; % do not change the fixed status of the element changed by the user

    obj.activeconsrelinds = 1:numel(obj.pgraph.rels);
    obj.activefixnodeinds = 1:numel(obj.pgraph.nodes);
    obj.activeregnodeinds = 1:numel(obj.pgraph.nodes);

    if strcmp(obj.visualize,'all')
        ofun = @(options,state,solverflag) obj.outfun(options,state,solverflag);
    else
        ofun = [];
    end

    lb = -inf(size(state));    
    ub = inf(size(state));
    lb(obj.nodefixorig2stateinds) = 0;
    ub(obj.nodefixorig2stateinds) = 1;
    
    % genetic algorithm
    [bestsubstates,objectivevals,~,~,~,~] = gamultiobj(...
        @(x) obj.evalmultiobjective_nograd(x,1000,100,1),numel(obj.substateinds),...
        [],[],[],[],lb(obj.substateinds),ub(obj.substateinds),[],...
        gaoptimset(...
        'InitialPopulation',state(obj.substateinds)',...
        'Generations',100000,... % matlab default is 200*number of variables
        'TolFun',1e-8,...
        'Display','final',...%'off',... 
        'OutputFcns',ofun));
    
    % store solutions
    multiobjweights = [...
        obj.regw_relfixed;...
        obj.regw_relgrpschanged;...
        obj.regw_elmgrpschanged;...
        obj.regw_grpsbroken];
    
    obj.solutionstate = repmat(state,1,size(bestsubstates,1));
    obj.solutionstatevar = repmat(statevar,1,size(bestsubstates,1)); % for now, need to recompute afterwards
    obj.solutionstate(obj.substateinds,:) = bestsubstates';
    obj.solutioncost = (objectivevals * multiobjweights)';
    
    elmstateinds = cat(1,obj.node2stateinds{elmoffset+1 : elmoffset+numel(obj.pgraph.elms)});
    relstateinds = cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds(1:numel(obj.pgraph.rels))});

    obj.solutionelmposes = cell(1,size(obj.solutionstate,2));
    obj.solutionrelvals = cell(1,size(obj.solutionstate,2));
    for i=1:numel(obj.solutionelmposes)
        obj.solutionelmposes{:,i} = reshape(obj.solutionstate(elmstateinds,i),obj.elmposedim,numel(obj.pgraph.elms));
        obj.solutionrelvals{:,i} = obj.solutionstate(relstateinds,i)';
    end
    
    
    [~,perm] = sort(obj.solutioncost,'ascend');
    obj.solutioncost = obj.solutioncost(perm);
    obj.solutionstate = obj.solutionstate(:,perm);
    obj.solutionstatevar = obj.solutionstatevar(:,perm);
    obj.solutionelmposes = obj.solutionelmposes(perm);
    obj.solutionrelvals = obj.solutionrelvals(perm);
    
    clusterthreshold = 0.4;
    keepinds = StandardPatternSolver.findUniqueStates(...
        obj.solutionstate,obj.solutionstatevar,clusterthreshold);
    
    obj.solutioncost = obj.solutioncost(keepinds);
    obj.solutionstate = obj.solutionstate(:,keepinds);
    obj.solutionstatevar = obj.solutionstatevar(:,keepinds);
    obj.solutionelmposes = obj.solutionelmposes(keepinds);
    obj.solutionrelvals = obj.solutionrelvals(keepinds);
    

    % set outputs
    selmposes = obj.solutionelmposes;
    srelvals = obj.solutionrelvals;
    scost = obj.solutioncost;
end

function val = evalmultiobjective_nograd(obj,substate,consistencyweight,fixedrelsweight,regularizationweight)
   
    activerelinds = zeros(1,0);
    if consistencyweight ~= 0
        activerelinds = [activerelinds,obj.activeconsrelinds];
    end
    if fixedrelsweight ~= 0
        activerelinds = [activerelinds,obj.pgraph.node2relinds(obj.activefixnodeinds)];
    end
    if regularizationweight ~= 0
        activerelinds = [activerelinds,obj.pgraph.node2relinds(obj.activeregnodeinds)];
    end
    activerelinds(isnan(activerelinds)) = [];
    activerelinds = unique(activerelinds);
    [currentstate,currentstatevar,currentrelval,currentrelvar] = obj.updateCurrentstate(substate,obj.fullstate,obj.fullstatevar,activerelinds);

    baseval = 0;
    if consistencyweight ~= 0
        baseval = baseval + obj.consistency_nograd(currentstate,currentrelval,currentrelvar,obj.activeconsrelinds)*consistencyweight;%*1000;
    end
    if fixedrelsweight ~= 0
        baseval = baseval + obj.fixedrels_nograd(currentstate,currentstatevar,obj.activefixnodeinds)*fixedrelsweight;%*100;
    end
    regvals = zeros(1,4);
    if regularizationweight ~= 0
        [~,relfixedval,relgrpschangedval,elmgrpschangedval,grpsbrokenval] = ...
            obj.regularization_nograd(currentstate,currentrelval,obj.activeregnodeinds);
        regvals = [relfixedval,relgrpschangedval,elmgrpschangedval,grpsbrokenval] .* regularizationweight;
    end
    
    val = baseval + regvals;
end

function setVisfunction(obj,func)
    obj.visfunc = func;
end

function showState(obj,substate,substateinds,state,statevar)
    obj.fullstate = state;
    obj.fullstatevar = statevar;
    obj.substateinds = substateinds;
    
    obj.outfun(substate,[],[]); 
end

function [state,options,optchanged] = outfun(obj,options,state,solverflag) %#ok<INUSD>
%     stop = false;
%     return; % temp
    
%     state = obj.fullstate;
%     state(obj.substateinds) = substate;
    
    if nargin >= 3
        disp(['*** generation: ',num2str(state.Generation),' - population size: ',num2str(size(state.Population,1)),' - best scores: ',num2str(max(state.Score,[],1))]);
    end
    
    optchanged = false;

    individual1state = state.Population(1,:)';
    
    if not(isempty(obj.visfunc))
        elmposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
       
        elmoffset = obj.pgraph.elmoffset;
        elmstateinds = cat(1,obj.node2stateinds{elmoffset+1 : elmoffset+numel(obj.pgraph.elms)});
        
        elmposes(1:obj.elmposedim,:) = reshape(individual1state(elmstateinds),obj.elmposedim,[]);
        
        stop = obj.visfunc(obj,elmposes);
        
        if stop
            state.StopFlag = 'stopped by user';
        end
    end
end

end

end
