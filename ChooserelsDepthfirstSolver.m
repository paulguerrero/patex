classdef ChooserelsDepthfirstSolver < StandardPatternSolver
    
properties(SetAccess=protected)
    % last solution
    solutioncost = zeros(1,0);
    solutionstate = [];
    solutionstatevar = [];
    solutionelmposes = cell(1,0);
    solutionrelvals = cell(1,0);
    
%     synctolerance = 0.001;

    visualize = 'all';
%     visualize = 'steps';
%     visualize = 'solutions';
%     visualize = 'none';
end

properties(Access=protected)
    visfunc = [];
end

methods

% obj = ChooserelsDepthfirstSolver
% obj = ChooserelsDepthfirstSolver(pgraph)
function obj = ChooserelsDepthfirstSolver(varargin)
    superarginds = zeros(1,0);
    if numel(varargin) == 1
        superarginds = 1;
    end
    
    obj@StandardPatternSolver(varargin{superarginds});
    varargin(superarginds) = [];
    
    if numel(varargin) == 0
        % do nothing
    else
        error('Invalid arguments.');
    end
end

function clear(obj)
    obj.clear@StandardPatternSolver;
end

function prepare(obj,newelmposes,newrelvals)

    obj.prepare@StandardPatternSolver(newelmposes,newrelvals);
    
    obj.solutioncost = zeros(1,0);
    obj.solutionstate = zeros(numel(obj.patternstate),0);
    obj.solutionstatevar = zeros(numel(obj.patternstate),0);
    obj.solutionelmposes = cell(1,0);
    obj.solutionrelvals = cell(1,0);
    
end

function [selmposes,srelvals,scost] = solve(obj,newelmposes,newrelvals)
    obj.clear;

    obj.prepare(newelmposes,newrelvals);
    
    obj.solve_stochastic;
    
    [~,perm] = sort(obj.solutioncost,'ascend');
    obj.solutioncost = obj.solutioncost(perm);
    obj.solutionstate = obj.solutionstate(:,perm);
    obj.solutionstatevar = obj.solutionstatevar(:,perm);
    obj.solutionelmposes = obj.solutionelmposes(perm);
    obj.solutionrelvals = obj.solutionrelvals(perm);
    
    clusterthreshold = sqrt(obj.synctolerance);
    keepinds = StandardPatternSolver.findUniqueStates(...
        obj.solutionstate,obj.solutionstatevar,clusterthreshold);
    
    obj.solutioncost = obj.solutioncost(keepinds);
    obj.solutionstate = obj.solutionstate(:,keepinds);
    obj.solutionstatevar = obj.solutionstatevar(:,keepinds);
    obj.solutionelmposes = obj.solutionelmposes(keepinds);
    obj.solutionrelvals = obj.solutionrelvals(keepinds);
    

    % set outputs
    selmposes = obj.solutionelmposes;
    srelvals = obj.solutionrelvals;
    scost = obj.solutioncost;
end

% estimate solution based on linear constraints (linear realtionships)
function se = estimate_solution_diffmanip(...
        obj,newstate,origstate,origstatevar,...
        consweight,fixweight,regweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken)
    
    % can be thought of as change in relationships that is estimated not
    % alter the relationship values (estimated at the original state)
    
    % all relatinoships are active
    activerelinds = 1:numel(obj.pgraph.rels);

    % do not include nodefix indicators the substate for estimating the jacobian
%     jacsubstateinds = substateinds;
    substateinds = (1:numel(origstate))';
    substateinds(ismember(substateinds,[obj.nodefixorig2stateinds,obj.nodefixnew2stateinds])) = [];
    jacobian = estimate_jacobian(...
        obj,origstate,origstatevar,origstate(substateinds),substateinds,activerelinds,...
        consweight,fixweight,regweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    
    statechange = newstate-origstate;
    if any(obj.statecircular)
        statechange(obj.statecircular) = smod(statechange(obj.statecircular),-pi,pi);
    end
    
    se = origstate;
    lagrangemult = (jacobian*jacobian') \ (jacobian*statechange(substateinds));
    correctedstatechange = statechange(substateinds) - jacobian' * lagrangemult;
    se(substateinds) = origstate(substateinds) + correctedstatechange;
    if any(obj.statecircular)
        se(obj.statecircular) = smod(se(obj.statecircular),-pi,pi);
    end
    
    error('Need to update this method.');
end

% output: state estimates and objective function value estimates
function [se,ove,se2,ove2] = estimate_solution_origstart(...
        obj,origstate,origstatevar,fixedstate,substateinds,substatefreemask,origval,origjacobian,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        consweight,fixweight,regweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken)
    
    % can be thought of as setting change in free relationships to value
    % that is estimated to cancel the estimated change in relationship
    % values introduced by fixed relationship
    % (estimations at original state)

    % get fixed/free state indices
    substatefixedmask = not(substatefreemask);
    fixedsubstateinds = substateinds(substatefixedmask);
    freesubstateinds = substateinds(substatefreemask);
    
    % Taylor expansion at the original state
    
    % zeroth-order approximation
    if nargin < 7 || isempty(origval)
        origval = obj.evalobjective_vector(...
            origstate,origstatevar,origstate(substateinds),substateinds,...
            activeconsrelinds,activefixnodeinds,activeregnodeinds,...
            consweight,fixweight,regweight,...
            regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    end
    
    % first-order approximation
    % estimate jacobian of the relationships at the original state
    % todo: don't need to esimate anew every time
    if nargin < 8 || isempty(origjacobian)
        origjacobian = obj.estimate_jacobian(...
            origstate,origstatevar,origstate(substateinds),substateinds,...
            activeconsrelinds,activefixnodeinds,activeregnodeinds,...
            consweight,fixweight,regweight,...
            regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    end
    
    % get fixed state change
    fixedstatechange = fixedstate(fixedsubstateinds) - origstate(fixedsubstateinds);
    if any(obj.statecircular(fixedsubstateinds))
        fixedstatechange(obj.statecircular(fixedsubstateinds)) = smod(fixedstatechange(obj.statecircular(fixedsubstateinds)),-pi,pi);
    end
    
%     fixedchangedval = (-origval - origjacobian(:,fixedsubstatemask) * fixedstatechange);
%     if any(obj.statecircular)
%         fixedchangedval(obj.statecircular) = smod(fixedchangedval(obj.statecircular),-pi,pi);
%     end
    
    % find best values for free state change    
    % minimize taylor expansion at original state over the free state
    % subspace, while keeping the fixed state subspace fixed
    freestatechange = pinv(origjacobian(:,substatefreemask)) * ...
        (-origval - origjacobian(:,substatefixedmask) * fixedstatechange);
    if any(obj.statecircular(freesubstateinds))
        freestatechange(obj.statecircular(freesubstateinds)) = smod(freestatechange(obj.statecircular(freesubstateinds)),-pi,pi);
    end
    
    % assemble solution
    se = fixedstate;
    se(fixedsubstateinds) = origstate(fixedsubstateinds) + fixedstatechange;
    se(freesubstateinds) = origstate(freesubstateinds) + freestatechange;
%     se(not(substatemask)) = fixedstate(nodefixstateinds);
    if any(obj.statecircular)
        se(obj.statecircular) = smod(se(obj.statecircular),-pi,pi);
    end
    
    % taylor expansion (with two terms)
    ove = origval + origjacobian(:,substatefixedmask) * fixedstatechange + origjacobian(:,substatefreemask) * freestatechange;
    
%     r = numel(freesubstateinds) - rank(origjacobian(:,substatefreemask));
    
    if nargout >= 3
        freestatechange2 = origjacobian(:,substatefreemask) \ ...
            (-origval - origjacobian(:,substatefixedmask) * fixedstatechange);
        if any(obj.statecircular(freesubstateinds))
            freestatechange2(obj.statecircular(freesubstateinds)) = smod(freestatechange2(obj.statecircular(freesubstateinds)),-pi,pi);
        end

        se2 = fixedstate;
        se2(fixedsubstateinds) = origstate(fixedsubstateinds) + fixedstatechange;
        se2(freesubstateinds) = origstate(freesubstateinds) + freestatechange2;
%         se2(nodefixstateinds) = fixedstate(nodefixstateinds);
        
        if any(obj.statecircular)
            se2(obj.statecircular) = smod(se2(obj.statecircular),-pi,pi);
        end
        
        ove2 = origval + origjacobian(:,substatefixedmask) * fixedstatechange + origjacobian(:,substatefreemask) * freestatechange2;
    end
end

function [se,ove,se2,ove2] = estimate_solution_fixedstart(...
        obj,fixedstate,fixedstatevar,substateinds,substatefreemask,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        consweight,fixweight,regweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken)
    
    % can be thought of as change in free relationships to value that is
    % estimated to bring all relationships to 0 (estimated at original
    % state but with fixed relationships)

    % get fixed/free state indices
    freesubstateinds = substateinds(substatefreemask);
    
    % Taylor expansion at the original state but with the fixed
    % relationship values
    
    % zeroth-order approximation
    fixedval = obj.evalobjective_vector(...
        fixedstate,fixedstatevar,fixedstate(substateinds),substateinds,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        consweight,fixweight,regweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    
    % first-order approximation
    % estimate jacobian of the relationships at the original state
    fixedjacobian = obj.estimate_jacobian(...
        fixedstate,fixedstatevar,fixedstate(substateinds),substateinds,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...    
        consweight,fixweight,regweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    
    % find best values for free state change
    % minimze taylor expansion at the original state modified by the fixed
    % relationship values over the free state subspace 
    freestatechange = pinv(fixedjacobian(:,substatefreemask)) * -fixedval;
    if any(obj.statecircular(freesubstateinds))
        freestatechange(obj.statecircular(freesubstateinds)) = smod(freestatechange(obj.statecircular(freesubstateinds)),-pi,pi);
    end
    
    % assemble solution
    se = fixedstate;
    se(freesubstateinds) = fixedstate(freesubstateinds) + freestatechange;
    if any(obj.statecircular)
        se(obj.statecircular) = smod(se(obj.statecircular),-pi,pi);
    end
    
    % taylor expansion (with two terms)
    ove = fixedval + fixedjacobian(:,substatefreemask) * freestatechange;
    
    if nargout >= 3
        freestatechange2 = fixedjacobian(:,substatefreemask) \ -fixedval;
        if any(obj.statecircular(freesubstateinds))
            freestatechange2(obj.statecircular(freesubstateinds)) = smod(freestatechange2(obj.statecircular(freesubstateinds)),-pi,pi);
        end

        se2 = fixedstate;
        se2(freesubstateinds) = fixedstate(freesubstateinds) + freestatechange2;
        if any(obj.statecircular)
            se2(obj.statecircular) = smod(se2(obj.statecircular),-pi,pi);
        end
        
        ove2 = fixedval + fixedjacobian(:,substatefreemask) * freestatechange2;
    end
end

function [combs,relgrpsubsetinds] = generateCombinations(obj,maxgencombs,maxoutcombs,componentnodeinds)
    
    rngstate = rng(50152527); % temp: seed rng to get repeatable results
    
%     % determine which relationship groups should be part of the combinations:
%     % distance of a group is minimum distance of group member to changed node
%     reldist = min(obj.pgraph.nodedist(userchangednodeind,obj.pgraph.rel2nodeinds),[],1);
%     relgrpdist = nan(1,size(obj.relgrps,2));
%     for i=1:size(obj.relgrps,2)
%         relgrpdist(i) = min(reldist(logical(obj.relgrps(:,i))));
%     end
% %     relgrpsubsetmask = relgrpdist <= 3;
%     relgrpsubsetmask = relgrpdist <= 2;
%     relgrpsubsetinds = find(relgrpsubsetmask);

%     % determine which relationship groups should be part of the combinations:
%     % onnly changed relationships
%     relgrpchanged = false(1,size(obj.relgrps,2));
%     for i=1:size(obj.relgrps,2)
%         relgrpchanged(i) = any(obj.relvalchanged(logical(obj.relgrps(:,i))));
%     end
%     relgrpsubsetmask = relgrpchanged;
%     relgrpsubsetinds = find(relgrpsubsetmask);
    
%     % determine which relationship groups should be part of the combinations:
%     % all relationships
%     relgrpsubsetinds = 1:size(obj.relgrps,2);
    
    % determine which relationship groups should be part of the combinations:
    % only relationship groups that are reachable in the graph from the
    % element(s) modified by the user (i.e. no relationship groups
    % consisting only of disconnected components)
%     elmoffset = obj.pgraph.elmoffset;
%     userchangedelmind = find(any(obj.elmposechanged,1));
%     userchangednodeind = userchangedelmind + elmoffset;
%     reldist = min(obj.pgraph.nodedist(userchangednodeind,obj.pgraph.rel2nodeinds),[],1);
%     relgrpdist = nan(1,size(obj.relgrps,2));
%     for i=1:size(obj.relgrps,2)
%         relgrpdist(i) = min(reldist(logical(obj.relgrps(:,i))));
%     end
%     relgrpsubsetinds = find(not(isinf(relgrpdist)));
    componentrelinds = obj.pgraph.node2relinds(componentnodeinds);
    componentrelinds(isnan(componentrelinds)) = [];
    relgrpsubsetinds = find(any(obj.relgrps(componentrelinds,:),1));
    
    
    grpsubsetnchoices = cellfun(@(x) numel(x),obj.relgrpchangedinds(1,relgrpsubsetinds))+2;
    
    
    % generate combinations of indicator values for the relationship groups
    % (each group can either be set to free, old value, or any of the new
    % values that members of the group have been changed to)
    totalncombs = prod(grpsubsetnchoices);
    combs = randpermvec(zeros(1,numel(relgrpsubsetinds)),grpsubsetnchoices-1,min(totalncombs,maxgencombs));
    
    % pick a random subset of the larger random subset of combinations,
    % with higher chance of picking combinations with many fixed
    % relationships
    ncombs = min(maxoutcombs,size(combs,1));
    combfixedcount = sum(combs~=0,2);
    
    if ncombs < size(combs,1)
        pickedcombmask = false(1,size(combs,1));
        while true
            nremaining = ncombs - sum(pickedcombmask);

            if nremaining <= 0
                break;
            end

            unpickedinds = find(not(pickedcombmask));

            % higher chance of picking combinations with many fixed
            % relationships
            strata = [0,cumsum(combfixedcount(unpickedinds))'];

            r = mindistrand(nremaining,min(diff(strata)),0,strata(end));

            combinds = nan(1,numel(r));
            for i=1:numel(r)
                combinds(i) = unpickedinds(min(numel(unpickedinds),find(r(i) - strata >= 0,1,'last')));
            end

            pickedcombmask(combinds) = true;
        end
    else
        pickedcombmask = true(1,size(combs,1));
    end
    
    combs = combs(pickedcombmask,:);
    
    
    rng(rngstate); % temp: set rng back to original state
end

function [combstate,combstatevar,combsubstatefreemask,combisvalid] = getCombinationState(obj,comb,combrelgrps,metarelsandchilds,substateinds)
    
    elmoffset = obj.pgraph.elmoffset;
    userchangedelmind = find(any(obj.elmposechanged,1));
    userchangednodeind = userchangedelmind + elmoffset;
    unchangedrelnodeinds = obj.pgraph.rel2nodeinds(not(obj.relvalchanged));
    elmnodeinds = (1:numel(obj.pgraph.elms)) + elmoffset;
    
    relfixorigstateinds = obj.nodefixorig2stateinds(obj.pgraph.rel2nodeinds);
    relfixnewstateinds = obj.nodefixnew2stateinds(obj.pgraph.rel2nodeinds);
    
    combstate = obj.patternstate;
    combstate(relfixorigstateinds) = 0;%1;
    combstate(relfixnewstateinds) = 0;
    combstatevar = obj.patternstatevar;

    combstatefreemask = false(size(combstate));
    combstatefreemask(substateinds) = true;
    for j=1:numel(combrelgrps)
        grprelinds = logical(obj.relgrps(:,combrelgrps(j)));

        stateinds = relfixorigstateinds(grprelinds);
        combstate(stateinds) = comb(j) == 1;
        combstatefreemask(stateinds) = false;


        stateinds = relfixnewstateinds(grprelinds);
        if comb(j)>=2
%             combstate(stateinds) = ismember(find(grprelinds),obj.relgrpchangedinds{1,combrelgrps(j)}{comb(j)-1});
            combstate(stateinds) = 1; % here for set all in group to new
        else
            combstate(stateinds) = 0;
        end
        combstatefreemask(stateinds) = false;

        if comb(j) == 1
            % set all group relationships to their original values
            stateinds = obj.relstateinds(grprelinds);
            combstate(stateinds) = obj.origrelvals(grprelinds);
            combstatefreemask(stateinds) = false;
        elseif comb(j) >= 2

            % fix all group relationships to one of the new
            % values (wich one is determined by comb(j))
            stateinds = obj.relstateinds(grprelinds);

            combstate(stateinds) = obj.relgrpchangedinds{2,combrelgrps(j)}(comb(j)-1); % intialize all members to new value

            % fix only the members that share one of the new
            % values to this new value (wich one is determined
            % by comb(j))
%             stateinds = obj.relstateinds(obj.relgrpchangedinds{1,combrelgrps(j)}{comb(j)-1});
            stateinds = obj.relstateinds(grprelinds); % here for set all in group to new

            combstatefreemask(stateinds) = false; % only fix members that actually have the new value
        end
    end

    % modify state with the element changed by the user
    combstate(obj.elmposestateinds(:,userchangedelmind)) = obj.newelmposes(:,userchangedelmind);
    combstate(obj.nodefixorig2stateinds(userchangednodeind)) = 0;
    combstate(obj.nodefixnew2stateinds(userchangednodeind)) = 1;

    combstatefreemask(obj.elmposestateinds(:,userchangedelmind)) = false; % do not change the element modified by the user
    combstatefreemask(obj.nodefixorig2stateinds(userchangednodeind)) = false; % do not change the fixed status of the element changed by the user
    combstatefreemask(obj.nodefixnew2stateinds(userchangednodeind)) = false; % do not change the fixed status of the element changed by the user
    combstatefreemask(obj.nodefixorig2stateinds(elmnodeinds)) = false; % do not change the fixed status of element nodes
    combstatefreemask(obj.nodefixnew2stateinds(elmnodeinds)) = false; % do not change the fixed status of element nodes
    combstatefreemask(obj.nodefixnew2stateinds(unchangedrelnodeinds)) = false; % do not change the fixed to new status of unchanged relationship nodes (should remain at 0)
    combstatefreemask(obj.grpfix2stateinds) = false; % do not change groupfix state
    
    combsubstatefreemask = combstatefreemask(substateinds);

    
    % test combination for validity
    
    combisvalid = true;
    
    % search for combinations where the relationship and both childs are
    % fixed (either to old or new value) and test if there are conflicting
    % fixed relationships in the combination
    relfixedmask = any(obj.relgrps(:,combrelgrps(comb > 0)),2);
    relandchildsfixedinds = metarelsandchilds(1,all(relfixedmask(metarelsandchilds),1));
    if combisvalid && not(isempty(relandchildsfixedinds))
%         [~,~,~,~,wnormresidual] = obj.relresidual(combstate,relandchildsfixedinds);

        wnormresidual = evalobjective_vector(...
            obj,combstate,combstatevar,combstate,(1:numel(combstate))',...
            relandchildsfixedinds,zeros(1,0),zeros(1,0),...
            1,0,0,...
            0,0,0,0);
        wnormresidual = wnormresidual.^2;

%         synctolerance = 0.1;
        conflictrelinds = relandchildsfixedinds(wnormresidual > obj.synctolerance);

        combisvalid = isempty(conflictrelinds);
    end
    
    % todo: check if the state estimate now approx. has zero consistentcy
    % penalty
    
    % check that a circle of reldirdiff nodes adds to approx. 2pi
    
    % check the objectve function values of the linear approximations, only
    % use those with small residuals (otherwise there is inconsistency)
    % => done
    
    % check rank deficiency of the jacobian for severly underconstrained
    % combinations?
    
    % pre-invert jacobian? But: need to invert different subsets of
    % columns. Use Woodbury matrix identity to remove those columns from
    % the full matrix inverse? => Only useful if the columns to be removed
    % is < 0.5*#remainingcolumns, since a 2*#removedcolumns square matrix
    % needs to be inverted (or more complicated: precomputed inverted
    % matrices for a hierarchy of subsets of the matrix columns)
    
    % additional combination option for group: break group and fix
    % all the changed relationships in the group (requires a single
    % additional option)
    % alternative: break the group and fix only one of the changed
    % relationships in the group (requires nchanged additonal options)
    
    % todo: also check for cut-off parts of the graph (where
    % the residual cannot be propagated because the subgraph is
    % cut-off by fixed relationships, e.g. the highest-level relationships in the simple pattern if the direction difference group is fixed)

    % for each closed subset of siblings of fixed parents (all
    % childs of the fixed parents are in the subset):
    % check if there is a solution for the subset of nodes that
    % fulfills all parents (invalid if not)

    % different interpretations: incompatible relationships:
    % having both relationships fixed would lead to
    % inconsistent patterns

    % treat relationships that have fixed children as fixed?

    % modify existing solutions slightly and see if the
    % remainder of the objective function changes (consistency
    % and regularization except fixed/unfixed

    % check if current fixed values are (approximately) already
    % ocurring in one of the (consistent) solutions, do not use
    % combination if they are used already (the result would be
    % the same as int that solution)

    % optimize with all set to free first, then always check if
    % the fixed relationships are fixed to the same value as in
    % the first solution (or any other solution) and only take
    % the combination if there is at least one difference or if
    % relationship that is fixed in the solution is free in the
    % combination (otherwise, we would get the same result as
    % in the first solution (?) )
    
    % additional constraints:
    % - remove invalid configurations (that would result in
    % consistency or fixed being violated). Check fixed nodes
    % locally, if they can be satisfied by their childs nodes, if
    % their parents can be satisfied, etc., re-generate random
    % indicators if config is invalid
end

% set nodefixorig and nodefixnew state of relationships based
% on closeness of the estimate to orig/new relationship values
function state = setNodefixstate(obj,state)
    origreldist = state(obj.relstateinds) - obj.origrelvals';
    state(obj.nodefixorig2stateinds(obj.pgraph.rel2nodeinds)) = ...
        exp(-origreldist.^2 ./ (obj.synctolerance.*2.*obj.origrelvars'));
    newreldist = state(obj.relstateinds) - obj.newrelvals';
    state(obj.nodefixnew2stateinds(obj.pgraph.rel2nodeinds)) = ...
        min(...
            1-state(obj.nodefixorig2stateinds(obj.pgraph.rel2nodeinds)),...
            exp(-newreldist.^2 ./ (2.*obj.origrelvars')));
end

function solve_stochastic(obj)
    
    % set parameters
    nsolutions = 10;
%     nsolutions = 3;
    
    consweight = 1000;
    fixweight = 0;
    regweight = 1;
    
    regw_relfixed = 0;
    regw_elmgrpschanged = 1;% 10% 0.1
    regw_relgrpschanged = 1;
    regw_grpsbroken = 10;%100;%;500;% 30 %3;
    
%     obj.visualize = 'all';
    obj.visualize = 'solutions';
    
    % find the current pattern graph component: all nodes that are
    % connected to any of the elements changed by the user
    elmoffset = obj.pgraph.elmoffset;
    userchangedelmind = find(any(obj.elmposechanged,1));
    userchangednodeind = userchangedelmind + elmoffset;
    nodedist = min(obj.pgraph.nodedist(userchangednodeind,:),[],1);
    grpdist = nan(1,size(obj.pgraph.groups,2));
    for i=1:size(obj.pgraph.groups,2)
        grpdist(i) = min(nodedist(logical(obj.pgraph.groups(:,i))));
    end
    grpsubsetinds = find(not(isinf(grpdist)));
    componentnodeinds = find(any(obj.pgraph.groups(:,grpsubsetinds),2))'; %#ok<FNDSB>
    componentrelinds = obj.pgraph.node2relinds(componentnodeinds);
    componentrelinds(isnan(componentrelinds)) = [];
    componentstateinds = cat(1,obj.node2stateinds{componentnodeinds});
    
    % only optimize pattern graph nodes in the current component
    substatemask = false(size(obj.patternstate));
    substatemask(componentstateinds) = true;
    substatemask(obj.nodefixorig2stateinds) = false;
    substatemask(obj.nodefixnew2stateinds) = false;
    substatemask(obj.grpfix2stateinds) = false;
    substateinds = find(substatemask);
    
    % all relationships in the current component are active
    activeconsrelinds = componentrelinds;
    activefixnodeinds = componentnodeinds;
    activeregnodeinds = componentnodeinds;
    
    % generate combinations
    [combs,combrelgrps] = obj.generateCombinations(10000,1000,componentnodeinds);
    
    % get relationships and their childs (to filter out inconsistent
    % combinations later)
    nodesandchilds = nan(3,numel(obj.pgraph.nodes));
    nodesandchilds(1,:) = 1:numel(obj.pgraph.nodes);
    for i=1:numel(obj.pgraph.nodes)
        nodesandchilds(1+1:1+numel(obj.childnodeinds{i}),i) = obj.childnodeinds{i};
    end
    relsandchilds = obj.pgraph.node2relinds(nodesandchilds(:,obj.pgraph.rel2nodeinds)')';
    metarelsandchilds = relsandchilds(:,not(any(isnan(relsandchilds),1)));
    
    
    % compute jacobian and objective value at starting point (before user
    % change)
    origstate = obj.patternstate;
    origstatevar = obj.patternstatevar;
    
    [~,~,objfun_consinds] = obj.objfunprops(...
            activeconsrelinds,activefixnodeinds,activeregnodeinds,...
            consweight,fixweight,regweight,...
            regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);

    % zeroth-order approximation and first-order approximation
    % (estimate objective function value and jacobian of the relationships
    % at the original state)
    [origval,origjacobian] = obj.evalobjective_vector(...
        origstate,origstatevar,origstate(substateinds),substateinds,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        consweight,fixweight,regweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    
%     % to compare the analytic gradient of the scalar objective function to
%     % the finite difference gradient
%     origjacobian_fd = obj.estimate_jacobian(...
%         origstate,origstatevar,origstate(substateinds),substateinds,...
%         activeconsrelinds,activefixnodeinds,activeregnodeinds,...
%         consweight,fixweight,regweight,...
%         regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    
%     % to compare the analytic gradient of the scalar objective function to
%     % the finite difference gradient
%     [valtest,origgradient] = obj.evalobjective_vecnorm(...
%         origstate,origstatevar,origstate(substateinds),substateinds,...
%         activeconsrelinds,activefixnodeinds,activeregnodeinds,...
%         consweight,fixweight,regweight,...
%         regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
%     origgradient_fd = jacobianest(...
%         @(x) obj.evalobjective_vecnorm(...
%             origstate,origstatevar,x,substateinds,...
%             activeconsrelinds,activefixnodeinds,activeregnodeinds,...
%             consweight,fixweight,regweight,...
%             regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken),...
%         origstate(substateinds));

    warning('off','MATLAB:rankDeficientMatrix');
    
    % compute linear estimates for the solution for each of the combinations
    keepcombs = false(1,size(combs,1));
    nodestateinds = cat(1,obj.node2stateinds{:});
%     elmstateinds = obj.elmposestateinds(:);
    stateestimates = zeros(numel(obj.patternstate),0);
    stateestimatevals = zeros(1,0);
    stateestimatedists = zeros(0,0);
    stateestimatesubstateinds = cell(1,0);
    
    tic;
    for i=1:size(combs,1)
        comb = combs(i,:);
        [combstate,~,combsubstatefreemask,combisvalid] = obj.getCombinationState(comb,combrelgrps,metarelsandchilds,substateinds);
        
        if combisvalid
            
            [stateestimate,stateestimateval] = obj.estimate_solution_origstart(...
                origstate,origstatevar,combstate,substateinds,combsubstatefreemask,origval,origjacobian,...
                activeconsrelinds,activefixnodeinds,activeregnodeinds);
            
            % todo: why do estimates not care about the fixed state of the
            % highest-level relations? (even if dirdist group is free)?
            % => reldist has zero gradient in origstate, would need hessian
            % or change to reldiff
            
            stateestimates(:,end+1) = stateestimate; %#ok<AGROW>
            stateestimatesubstateinds{:,end+1} = substateinds(combsubstatefreemask); %#ok<AGROW>
%             stateestimatevals(:,end+1) = mean(stateestimateval.^2,1); %#ok<AGROW>
            stateestimatevals(:,end+1) = mean(stateestimateval(objfun_consinds).^2,1); %#ok<AGROW>
            
            % todo: set fixed/nonfixed of free relationships in
            % stateestimate based on closeness of the relationship values
            % to the fixed or the original value => should make the
            % non-linear solver converge faster
            
%             stateestimatedists(end+1,:) = ...
%                 sqrt(sum(bsxfun(@rdivide,...
%                     bsxfun(@minus,stateestimates(elmstateinds,end),stateestimates(elmstateinds,1:end-1)),...
%                     sqrt(obj.patternstatevar(elmstateinds,:)))...
%                 .^2,1)); %#ok<AGROW>
%             stateestimatedists(end+1,:) = ...
%                 sqrt(sum(bsxfun(@rdivide,...
%                     bsxfun(@minus,stateestimates(elmstateinds,end),stateestimates(elmstateinds,1:end-1)).^2,...
%                     obj.patternstatevar(elmstateinds,:))...
%                 .^2,1)); %#ok<AGROW>
%             stateestimatedists(end+1,:) = ...
%                 sum(abs(bsxfun(@rdivide,...
%                     bsxfun(@minus,stateestimates(elmstateinds,end),stateestimates(elmstateinds,1:end-1)),...
%                     sqrt(obj.patternstatevar(elmstateinds,:))))...
%                 ,1); %#ok<AGROW>
            stateestimatedists(end+1,:) = ...
                sqrt(sum(bsxfun(@rdivide,...
                    bsxfun(@minus,stateestimates(nodestateinds,end),stateestimates(nodestateinds,1:end-1)),...
                    sqrt(obj.patternstatevar(nodestateinds,:)))...
                .^2,1)); %#ok<AGROW>
            stateestimatedists(:,end+1) = 0; %#ok<AGROW>
            
            keepcombs(i) = true;
        end
    end
    
    toc;
    
    warning('on','MATLAB:rankDeficientMatrix');
    
%     toc;
    
    combs(not(keepcombs),:) = [];    

    mask = stateestimatevals > obj.synctolerance*100;
    combs(mask,:) = [];    
    stateestimates(:,mask) = [];
    stateestimatevals(:,mask) = [];
    stateestimatedists(:,mask) = [];
    stateestimatedists(mask,:) = [];
    stateestimatesubstateinds(mask) = [];
    
    stateestimatedists = stateestimatedists + stateestimatedists';
    
    
    
    % cluster linear state estimates by pair-wise distance
%     maxclusterdist = 50;
%     maxclusterdist = obj.synctolerance*500;
%     maxclusterdist = sqrt(obj.synctolerance)*20;
    l = linkage(squareform(stateestimatedists,'tovector'),'complete');
%     maxclusterdist = l(end,3)*0.3;
%     maxclusterdist = l(end,3)*0.1;
%     combsolutioninds = cluster(l,'cutoff',maxclusterdist,'criterion','distance');
    combsolutioninds = cluster(l,'maxclust',10);
    solutioncombinds = array2cell_mex(1:numel(combsolutioninds),combsolutioninds');
    
    solutionestimate = zeros(size(obj.solutionstate));
    nsolutions = min(nsolutions,numel(solutioncombinds));
    for i=1:nsolutions
            
        solutioncombs = combs(solutioncombinds{i},:);
        fixedcount = sum(solutioncombs~=0,2);
        [~,ind] = max(fixedcount);
        combind = solutioncombinds{i}(ind);

        comb = combs(combind,:);

        [combstate,combstatevar,combsubstatefreemask,combisvalid] = obj.getCombinationState(comb,combrelgrps,metarelsandchilds,substateinds);

        if not(combisvalid)
            error('Combination is not valid, this should not happen')
        end
        
%         fixedrelinds = ...
%             combstate(obj.nodefixorig2stateinds(obj.pgraph.rel2nodeinds)) > 0.5 | ...
%             combstate(obj.nodefixnew2stateinds(obj.pgraph.rel2nodeinds)) > 0.5;
%         fixedrelinds(ismember(obj.relstateinds(fixedrelinds),combsubstateinds)) = [];
%         nonfixedrelmask = true(1,numel(obj.pgraph.rels));
%         nonfixedrelmask(fixedrelinds) = false;
        
        combsubstateinds = substateinds(combsubstatefreemask);
        
        stateestimate = stateestimates(:,combind);
        
        % re-evaluate all free relationships in the state estimate
        % (linear estimation results in some discrepancies in the element
        % positions and relationships values)
        combfreenodeinds = obj.state2nodeinds(combsubstateinds);
        combfreenodeinds(isnan(combfreenodeinds)) = [];
        combfreerelinds = obj.pgraph.node2relinds(combfreenodeinds);
        combfreerelinds(isnan(combfreerelinds)) = [];
        stateestimate = obj.evalrelsFromElements(stateestimate,combstatevar,combfreerelinds);
        
        % set fixed state of nodes in the state estimate
        stateestimate = obj.setNodefixstate(stateestimate);
        
        % define lower and upper bounds for the state
        lb = -inf(size(combstate));
        ub = inf(size(combstate));
        
        % starting close to the lower or upper bound seems to be a bad idea
        % (slow convergence, even if the starting point = solution for this
        % subspace of the domain) and setting the lower/upper bound to 0/1
        % makes matlab think the initial state is out of bounds and set it
        % somewhere betweent the bounds which gives better convergence

%         lb(obj.nodefixorig2stateinds) = 0-1e-12;
%         ub(obj.nodefixorig2stateinds) = 1+1e-12;
%         lb(obj.nodefixnew2stateinds) = 0-1e-12;
%         ub(obj.nodefixnew2stateinds) = 1+1e-12; % apparently matlab seems to think the starting point violates the bounds if i set it to 0 or 1 (even though the doc says lb <= x <= ub)
        lb(obj.nodefixorig2stateinds) = 0;
        ub(obj.nodefixorig2stateinds) = 1;
        lb(obj.nodefixnew2stateinds) = 0;
        ub(obj.nodefixnew2stateinds) = 1; % apparently matlab seems to think the starting point violates the bounds if i set it to 0 or 1 (even though the doc says lb <= x <= ub)


        if strcmp(obj.visualize,'all')
            ofun = @(substate,optimValues,solverflag) obj.outfun(combstate,combstatevar,substate,combsubstateinds,optimValues.iteration,optimValues.fval);
        else
            ofun = [];
        end
        
        obj.outfun(combstate,combstatevar,combstate(combsubstateinds),combsubstateinds);
        
%         startstate = combstate;
        startstate = stateestimate;
        startstatevar = combstatevar;
        
        objfun = @(x) obj.evalobjective_vecnorm(...
            startstate,startstatevar,...
            x,combsubstateinds,...
            activeconsrelinds,activefixnodeinds,activeregnodeinds,...
            consweight,fixweight,regweight,...
            regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
        
%         objfun = @(x) obj.evalobjective_nograd(...
%             startstate,startstatevar,...
%             x,combsubstateinds,...
%             activeconsrelinds,activefixnodeinds,activeregnodeinds,...
%             consweight,fixweight,regweight,...
%             regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
        
        if all(isinf(lb(combsubstateinds))) && all(isinf(ub(combsubstateinds)))
            tic;
            [bestsubstate,objectiveval] = fminunc(...
                objfun,...
                startstate(combsubstateinds),...
                optimoptions(@fminunc,...
                'Algorithm','trust-region',...'quasi-newton',...
                'MaxFunEvals',100000,... % matlab default is 100*number of variables
                'MaxIter',100,... % 100 for trust region, 200 for quasi-newton
                'TolFun',1e-6,... % matlab default is 1e-6
                'TolX',1e-10,... % default for interior point is 1e-10
                'GradObj','on',...'off',...
                'Diagnostics','on',...
                'Display','final',...%'off',... 
                'FunValCheck','off',...
                'OutputFcn',ofun,...
                'DerivativeCheck','off'));
            toc;
        else
            % Matlab's fmincon
            [bestsubstate,objectiveval] = fmincon(...
                objfun,...
                startstate(combsubstateinds),...
                [],[],[],[],lb(combsubstateinds),ub(combsubstateinds),[],...
                optimoptions(@fmincon,...
                'MaxFunEvals',100000,... % matlab default is 100*number of variables
                'MaxIter',200,...
                'TolFun',1e-6,... % matlab default is 1e-6
                'TolX',1e-10,... % default for interior point is 1e-10
                'GradObj','off',...
                'Diagnostics','off',...
                'Display','final',...%'off',... 
                'FunValCheck','on',...
                'OutputFcn',ofun,...
                'DerivativeCheck','off'));
%                 'Algorithm','interior-point',... % is default
        end
        
        % store solution
        obj.solutioncost(:,end+1) = objectiveval;
        obj.solutionstate(:,end+1) = combstate;
        obj.solutionstate(combsubstateinds,end) = bestsubstate;
        obj.solutionstatevar(:,end+1) = combstatevar;
        
        obj.solutionstate(:,end) = obj.setNodefixstate(obj.solutionstate(:,end));
        
        obj.outfun(obj.solutionstate(:,end),combstatevar,obj.solutionstate(combsubstateinds,end),combsubstateinds);
        
        solutionestimate(:,end+1) = stateestimate; %#ok<AGROW>
        
        obj.outfun(stateestimate,combstatevar,stateestimate(combsubstateinds),combsubstateinds);

        eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        eposes(1:obj.elmposedim,:) = reshape(obj.solutionstate(obj.elmposestateinds,end),obj.elmposedim,numel(obj.pgraph.elms));
        eposes = PatternSolver.sim2affinepose(eposes);
        obj.solutionelmposes{:,end+1} = eposes;
        obj.solutionrelvals{:,end+1} = obj.solutionstate(obj.relstateinds,end)';
    end
end

function setVisfunction(obj,func)
    obj.visfunc = func;
end

function showState(obj,substate,substateinds,state,statevar)
    obj.fullstate = state;
    obj.fullstatevar = statevar;
    obj.substateinds = substateinds;
    
    obj.outfun(substate,[],[]); 
end

function stop = outfun(obj,fullstate,fullstatevar,substate,substateinds,iter,fval) %#ok<INUSL>
%     stop = false;
%     return; % temp
    
    state = fullstate;
    state(substateinds) = substate;
    
    if nargin >= 7
        disp(['*** iteration ',num2str(iter),':']);
        disp(['    objective function value: ',num2str(fval)]);
    end
    
    if not(isempty(obj.visfunc))
        elmposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        
%         elmoffset = obj.pgraph.elmoffset;
%         elmstateinds = cat(1,obj.node2stateinds{elmoffset+1 : elmoffset+numel(obj.pgraph.elms)});
        
        elmposes(1:obj.elmposedim,:) = reshape(state(obj.elmposestateinds),obj.elmposedim,[]);
        
        nodefixorig = state(obj.nodefixorig2stateinds);
        nodefixnew = state(obj.nodefixnew2stateinds);
        
        elmposes = PatternSolver.sim2affinepose(elmposes);
        stop = obj.visfunc(...
            obj,...
            elmposes,...
            nodefixorig,...
            nodefixnew);
        
    else
        stop = false;
    end
end

end

end
