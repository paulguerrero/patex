classdef StandardPatternSolver < PatternSolver
    
properties(SetAccess=protected)
    solutioncost = zeros(1,0);
    solutionstate = [];
    solutionstatevar = [];
    solutionelmposes = cell(1,0);
    solutionrelvals = cell(1,0);
end
    
properties(SetAccess=protected)
    
    newelmposes = zeros(2,0);
    newrelvals = zeros(1,0);
    
    origelmposes = zeros(2,0);
    origrelvals = zeros(1,0);
    origrelvars = zeros(1,0);
    
    relweights = zeros(1,0);
    elmweights = zeros(1,0);
    
    elmposechanged = false(1,0);
    relvalchanged = false(1,0);
    
    elmgrps = sparse([]);
    relgrps = sparse([]);
    
    grpsize = zeros(1,0);
    
    elmgrpchanged = false(1,0);
    elmgrpchangedinds = cell(3,0); % first row: clusters of relationships that changed (one cluster for each distinct new value), second row: mean value for cluster, third row: mean variance for cluster
    relgrpchanged = false(1,0);
    relgrpchangedinds = cell(3,0);
end

methods(Static)

function s = defaultSettings
	s = catstruct(...
        PatternSolver.defaultSettings,...
        struct(...
            'consweight',1000,...     
            'fixweight',100,...
            'regweight',1,... % maximum area that is considered to be noise (given as fraction of the total number of image pixels)
            'regw_relfixed',0,...
            'regw_elmgrpschanged',0.1,...
            'regw_relgrpschanged',1,...
            'regw_grpsbroken',10,...
            'visualize','none',...
            'posefixdims',[1,2]));
end
    
end

methods
    
% obj = StandardPatternSolver
% obj = StandardPatternSolver(obj2)
% obj = StandardPatternSolver(pgraph)
function obj = StandardPatternSolver(varargin)
    superarginds = zeros(1,0);
    if numel(varargin) == 1 && isa(varargin{1},'PatternGraph')
        superarginds = 1;
    end
    
    obj@PatternSolver(varargin{superarginds});
    varargin(superarginds) = [];
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'StandardPatternSolver')
        obj.copyFrom(varargin{1});
    else
        error('Invalid arguments.');
    end
end

function copyFrom(obj,obj2)
    obj.copyFrom@PatternSolver(obj2);
    
    obj.solutioncost = obj2.solutioncost;
    obj.solutionstate = obj2.solutionstate;
    obj.solutionstatevar = obj2.solutionstatevar;
    obj.solutionelmposes = obj2.solutionelmposes;
    obj.solutionrelvals = obj2.solutionrelvals;

    obj.newelmposes = obj2.newelmposes;
    obj.newrelvals = obj2.newrelvals;

    obj.origelmposes = obj2.origelmposes;
    obj.origrelvals = obj2.origrelvals;
    obj.origrelvars = obj2.origrelvars;

    obj.relweights = obj2.relweights;
    obj.elmweights = obj2.elmweights;
    
    obj.elmposechanged = obj2.elmposechanged;
    obj.relvalchanged = obj2.relvalchanged;
    
    obj.elmgrps = obj2.elmgrps;
    obj.relgrps = obj2.relgrps;
    
    obj.grpsize = obj2.grpsize;
    
%     obj.elmgrpinds = obj2.elmgrpinds;
%     obj.relgrpinds = obj2.relgrpinds;
    
    obj.elmgrpchanged = obj2.elmgrpchanged;
%     obj.elmgrpchangedinds = obj2.elmgrpchangedinds;
    obj.relgrpchanged = obj2.relgrpchanged;
    obj.relgrpchangedinds = obj2.relgrpchangedinds;
end

function obj2 = clone(obj)
    obj2 = StandardPatternSolver(obj);
end
    
function clear(obj)
    obj.clear@PatternSolver;
    
    obj.solutioncost = zeros(1,0);
    obj.solutionstate = [];
    obj.solutionstatevar = [];
    obj.solutionelmposes = cell(1,0);
    obj.solutionrelvals = cell(1,0);

    obj.newelmposes = zeros(2,0);
    obj.newrelvals = zeros(1,0);

    obj.origelmposes = zeros(2,0);
    obj.origrelvals = zeros(1,0);
    obj.origrelvars = zeros(1,0);

    obj.relweights = zeros(1,0);
    obj.elmweights = zeros(1,0);
    
    obj.elmposechanged = false(1,0);
    obj.relvalchanged = false(1,0);
    
    obj.elmgrps = sparse([]);
    obj.relgrps = sparse([]);
    
    obj.grpsize = zeros(1,0);
    
    obj.elmgrpchanged = false(1,0);
    obj.relgrpchanged = false(1,0);
    obj.relgrpchangedinds = cell(3,0); % first row: which nodes, second row: new values, third row: variances
end

function prepare(obj,newelmposes,newrelvals)

    obj.prepare@PatternSolver;
    
    obj.solutioncost = zeros(1,0);
    obj.solutionstate = zeros(numel(obj.patternstate),0);
    obj.solutionstatevar = zeros(numel(obj.patternstate),0);
    obj.solutionelmposes = cell(1,0);
    obj.solutionrelvals = cell(1,0);
    
    obj.newelmposes = PatternSolver.affine2simpose(newelmposes);
    obj.newelmposes = obj.newelmposes(1:obj.settings.elmposedim,:);
    obj.newrelvals = newrelvals;
    
    obj.origelmposes = [obj.pgraph.elms.pose];
    obj.origelmposes = obj.origelmposes(1:obj.settings.elmposedim,:);
    
    obj.origrelvals = [obj.pgraph.rels.value];
    obj.origrelvars = [obj.pgraph.rels.variance];
    
    obj.relweights = ones(1,numel(obj.pgraph.rels));
    obj.elmweights = ones(1,numel(obj.pgraph.elms));
    

    % evaluate which relationships changed and which relationship groups
    % changed (and how)
    
    posediff = obj.origelmposes - obj.newelmposes;
    posediff(obj.elmposecircular) = smod(posediff(obj.elmposecircular),-pi,pi);
    obj.elmposechanged = bsxfun(@rdivide,posediff.^2,obj.elmposevar) > obj.settings.synctolerance;
    reldiff = obj.origrelvals - obj.newrelvals;
    reldiff(obj.relcircular) = smod(reldiff(obj.relcircular),-pi,pi);
    obj.relvalchanged = bsxfun(@rdivide,reldiff.^2,obj.origrelvars) > obj.settings.synctolerance;
    
    obj.elmgrps = obj.pgraph.groups(obj.pgraph.elmoffset+1:obj.pgraph.elmoffset+numel(obj.pgraph.elms),obj.elmgrpinds);
    
    obj.relgrps = obj.pgraph.groups(obj.pgraph.rel2nodeinds,obj.relgrpinds);

    obj.grpsize = full(sum(obj.pgraph.groups,1));



    % find out which element groups changed and to which distinct new
    % values the group members were changed to
    obj.elmgrpchanged = false(1,size(obj.elmgrps,2));
%     obj.elmgrpchangedinds = cell(3,size(obj.elmgrps,2));
    for i=1:size(obj.elmgrps,2)
        elmchangedinds = find(any(obj.elmposechanged,1) & logical(obj.elmgrps(:,i))'); %#ok<EFIND>
        obj.elmgrpchanged(i) = not(isempty(elmchangedinds));
        
        % doesn't make sense for elements, as element groups are neither
        % expected to have the same poses nor change poses by the same
        % amount
%         
%         if numel(elmchangedinds) >= 2
%             clusterinds = cluster(...
%                     linkage([obj.newelmposes(:,elmchangedinds)',repmat(obj.elmposevar',numel(elmchangedinds),1)],...
%                     'complete',{@(x,Y) bsxfun(@rdivide, bsxfun(@minus,x(1:obj.settings.elmposedim),Y(1:obj.settings.elmposedim,:)).^2, x(obj.settings.elmposedim+1:2*obj.settings.elmposedim))}),...
%                 'cutoff',obj.settings.synctolerance,...
%                 'criterion','distance');
%             obj.elmgrpchangedinds{1,i} = array2cell_mex(elmchangedinds,clusterinds);
%             obj.elmgrpchangedinds{2,i} = cellfun(@(x) mean(obj.newelmposes(:,x),2), obj.elmgrpchangedinds{1,i});
%             obj.elmgrpchangedinds{3,i} = repmat(obj.elmposevar,1,numel(obj.elmgrpchangedinds{1,i}));
%         elseif numel(elmchangedinds) == 1
%             obj.elmgrpchangedinds{1,i} = {elmchangedinds};
%             obj.elmgrpchangedinds{2,i} = obj.newelmposes(:,elmchangedinds);
%             obj.elmgrpchangedinds{3,i} = repmat(obj.elmposevar,1,numel(elmchangedinds));
%         else
%             obj.elmgrpchangedinds{1,i} = cell(1,0);
%             obj.elmgrpchangedinds{2,i} = zeros(1,0);
%             obj.elmgrpchangedinds{3,i} = zeros(1,0);
%         end
    end

    
    % find out which relationship groups changed and to which distinct new
    % values the group members were changed to
    obj.relgrpchanged = false(1,size(obj.relgrps,2));
    obj.relgrpchangedinds = cell(3,size(obj.relgrps,2));
    for i=1:size(obj.relgrps,2)
        relchangedinds = find(obj.relvalchanged & logical(obj.relgrps(:,i))');
        obj.relgrpchanged(i) = not(isempty(relchangedinds));
        
        if strcmp(obj.settings.relgrouptype,'change')
            
            if obj.relgrpcircular(i)
                relvalchange = smod(obj.newrelvals(relchangedinds) - obj.origrelvals(relchangedinds),-pi,pi);
            else
                relvalchange = obj.newrelvals(relchangedinds) - obj.origrelvals(relchangedinds);
            end
            
            if numel(relchangedinds) >= 2
                
                if obj.relgrpcircular(i)
                    distfun = @(x,Y) smod(x(1)-Y(:,1),-pi,pi).^2 ./ x(2);
                else
                    distfun = @(x,Y) (x(1)-Y(:,1)).^2 ./ x(2);
                end
                
                clusterinds = cluster(...
                        linkage([relvalchange',obj.origrelvars(relchangedinds)'],...
                        'complete',{distfun}),...
                    'cutoff',obj.settings.synctolerance,...
                    'criterion','distance');
                obj.relgrpchangedinds{1,i} = array2cell_mex(relchangedinds,clusterinds');
                indclusters = array2cell_mex(1:numel(relchangedinds),clusterinds');
                obj.relgrpchangedinds{2,i} = cellfun(@(x) mean(relvalchange(x)), indclusters);
                obj.relgrpchangedinds{3,i} = cellfun(@(x) mean(obj.origrelvars(x)), obj.relgrpchangedinds{1,i});
            elseif numel(relchangedinds) == 1
                obj.relgrpchangedinds{1,i} = {relchangedinds};
                obj.relgrpchangedinds{2,i} = relvalchange;
                obj.relgrpchangedinds{3,i} = obj.origrelvars(relchangedinds);
            else
                obj.relgrpchangedinds{1,i} = cell(1,0);
                obj.relgrpchangedinds{2,i} = zeros(1,0);
                obj.relgrpchangedinds{3,i} = zeros(1,0);
            end
            
        elseif strcmp(obj.settings.relgrouptype,'value')
            
            if numel(relchangedinds) >= 2
                
                if obj.relgrpcircular(i)
                    distfun = @(x,Y) smod(x(1)-Y(:,1),-pi,pi).^2 ./ x(2);
                else
                    distfun = @(x,Y) (x(1)-Y(:,1)).^2 ./ x(2);
                end
                
                clusterinds = cluster(...
                        linkage([obj.newrelvals(relchangedinds)',obj.origrelvars(relchangedinds)'],...
                        'complete',{distfun}),...
                    'cutoff',obj.settings.synctolerance,...
                    'criterion','distance');
                obj.relgrpchangedinds{1,i} = array2cell_mex(relchangedinds,clusterinds');
                obj.relgrpchangedinds{2,i} = cellfun(@(x) mean(obj.newrelvals(x)), obj.relgrpchangedinds{1,i});
                obj.relgrpchangedinds{3,i} = cellfun(@(x) mean(obj.origrelvars(x)), obj.relgrpchangedinds{1,i});
            elseif numel(relchangedinds) == 1
                obj.relgrpchangedinds{1,i} = {relchangedinds};
                obj.relgrpchangedinds{2,i} = obj.newrelvals(relchangedinds);
                obj.relgrpchangedinds{3,i} = obj.origrelvars(relchangedinds);
            else
                obj.relgrpchangedinds{1,i} = cell(1,0);
                obj.relgrpchangedinds{2,i} = zeros(1,0);
                obj.relgrpchangedinds{3,i} = zeros(1,0);
            end
            
        else
            error('Unknown relationship group type.');
        end
    end
    

end

% properties of the consistency objective functions
function [ofrelinds,ofcircular] = consobjfunprops(obj,activerelinds)
    
    % consistency penalty
    % (one function output per active relationship)
    ofrelinds = activerelinds;
    ofcircular = obj.relcircular(activerelinds);
end

% properties of the fix objective functions
function [ofnodeinds,ofcircular] = fixobjfunprops(obj,activenodeinds)
    
    ofnodeinds = zeros(1,0);
    ofcircular = false(1,0);
    
    % but not the orientation
%     posefixdims = [1,2]; % temporary
    
    activeelminds = activenodeinds-obj.pgraph.elmoffset;
    mask = activeelminds < 1 | activeelminds > numel(obj.pgraph.elms);
    activeelminds(mask) = [];
    
    % penalty for elements that are fixed to the new value
    elmposenodeinds = repmat(activeelminds(:)' + obj.pgraph.elmoffset,numel(obj.settings.posefixdims),1);
    ofnodeinds = [ofnodeinds,elmposenodeinds(:)'];
    ofcircular = [ofcircular,reshape(obj.elmposecircular(obj.settings.posefixdims,activeelminds),1,[])];
end

% properties of the regularization objective functions
function [ofnodeinds,ofcircular] = regobjfunprops(obj,activenodeinds,regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken) %#ok<INUSL>
    
    ofnodeinds = zeros(1,0);
    ofcircular = false(1,0);
    
    activerelinds = obj.pgraph.node2relinds(activenodeinds);
    activerelinds(isnan(activerelinds)) = [];
    
    activeelminds = activenodeinds-obj.pgraph.elmoffset;
    activeelminds(activeelminds < 1 | activeelminds > numel(obj.pgraph.elms)) = [];
        
    activerelmask = false(1,numel(obj.pgraph.rels));
    activerelmask(activerelinds) = true;
    
    if regw_elmgrpschanged ~= 0
        elmposenodeinds = repmat(activeelminds(:)' + obj.pgraph.elmoffset,obj.settings.elmposedim,1);
        ofnodeinds = [ofnodeinds,elmposenodeinds(:)'];
        ofcircular = [ofcircular,reshape(obj.elmposecircular(:,activeelminds),1,[])];
    end
    
    if regw_relgrpschanged ~= 0
        ofnodeinds = [ofnodeinds,obj.pgraph.rel2nodeinds(activerelinds)];
        ofcircular = [ofcircular,obj.relcircular(activerelinds)];
    end
    
    % groups broken penalty
    % (one function output per active relationship that is in a group with
    % at least one other relationship)
    if regw_grpsbroken ~= 0
        grprelinds = cell(1,numel(obj.relgrprelinds));
        for i=1:numel(obj.relgrprelinds)
            grelinds = obj.relgrprelinds{i}(activerelmask(obj.relgrprelinds{i}));
            if numel(grelinds) >= 2
                grprelinds{i} = grelinds;
            end
        end
        activegrprelinds = [grprelinds{:}];

        ofnodeinds = [ofnodeinds,obj.pgraph.rel2nodeinds(activegrprelinds)];
        ofcircular = [ofcircular,obj.relcircular(activegrprelinds)];
    end
end

% properties of the objective functions
function [ofnodeinds,ofcircular,ofconsinds,offixinds,ofreginds] = objfunprops(obj,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        consistencyweight,fixweight,regularizationweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken)
    
    if not(isrow(activeconsrelinds))
        error('activeconsrelinds must be given in row format.');
    end
    if not(isrow(activefixnodeinds))
        error('activefixnodeinds must be given in row format.');
    end
    if not(isrow(activeregnodeinds))
        error('activeregnodeinds must be given in row format.');
    end
    
    ofnodeinds = zeros(1,0);
    ofcircular = false(1,0);
    
    ofconsinds = zeros(1,0);
    offixinds = zeros(1,0);
    ofreginds = zeros(1,0);

    
    if consistencyweight ~= 0
        [consofrelinds,consofcircular] = obj.consobjfunprops(...
            activeconsrelinds);

        ofconsinds = numel(ofnodeinds);
        
        ofnodeinds = [ofnodeinds,obj.pgraph.rel2nodeinds(consofrelinds)];
        ofcircular = [ofcircular,consofcircular];
        
        ofconsinds = ofconsinds+1:numel(ofnodeinds);
    end
    
    if fixweight ~= 0
        [fixofnodeinds,fixofcircular] = obj.fixobjfunprops(...
            activefixnodeinds);

        offixinds = numel(ofnodeinds);
        
        ofnodeinds = [ofnodeinds,fixofnodeinds];
        ofcircular = [ofcircular,fixofcircular];
        
        offixinds = offixinds+1:numel(ofnodeinds);
    end
    
    if regularizationweight ~= 0
        [regofnodeinds,regofcircular] = obj.regobjfunprops(...
            activeregnodeinds,regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);

        ofreginds = numel(ofnodeinds);
        
        ofnodeinds = [ofnodeinds,regofnodeinds];
        ofcircular = [ofcircular,regofcircular];
        
        ofreginds = ofreginds+1:numel(ofnodeinds);
    end
end

% re-compute pattern node state (including relationship state) from element state
function [state,statevar] = evalrelsFromElements(obj,state,statevar,activerelinds)
    if nargin < 4
        activerelinds = 1:numel(obj.pgraph.rels);
    end
    
    if isempty(activerelinds)
        % state and statevar are unchanged
        return;
    end
    
    nnodes = numel(obj.pgraph.nodes);
    
    n2relinds = obj.pgraph.node2relinds;

    % also queue ancestors that have not been computed yet
    descendantinds = obj.pgraph.descendants(obj.pgraph.rel2nodeinds(activerelinds),obj.pgraph.rel2nodeinds(not(obj.pgraph.rels.hasValue)));
    queuedmask = false(1,nnodes);
    queuedmask([obj.pgraph.rel2nodeinds(activerelinds),descendantinds]) = true;
    
    srcnodeind = zeros(1,nnodes);
    tgtnodeind = zeros(1,nnodes);
    srcnodeind(obj.pgraph.rel2nodeinds) = obj.pgraph.relsrcind;
    tgtnodeind(obj.pgraph.rel2nodeinds) = obj.pgraph.reltgtind;

    % recompute all queued relationships in the correct order
    while(any(queuedmask))
        nodemask = srcnodeind > 0 & tgtnodeind > 0; % has source and target (not an element)
        nodemask(nodemask) = ...
            queuedmask(nodemask) & ...
            not(queuedmask(srcnodeind(nodemask))) & ...
            not(queuedmask(tgtnodeind(nodemask)));

        if not(any(nodemask))
            error('The pattern graph may have cycles or disconnected components.');
        end
        
        relinds = n2relinds(nodemask);
        stateinds = obj.relstateinds(relinds);
        [state(stateinds),statevar(stateinds)] = obj.evalrels(state,relinds);
        
        queuedmask(nodemask) = false;
    end
end

% evaluate relationshis in the current state
% relationship gradient is given as #activerel x #nodestate matrix
function [relval,relvar,relgrad] = evalrels(obj,state,activerelinds)
    
    % todo: for relationships that need other properties of the elements
    % other than their pose (e.g. distance to the boundary): need to move
    % elements to the position given by the state and pass them to the
    % relationship function (boundary vertex positions relative to the pose
    % act like constants in the relationship function)
    % - or: pass new pose (given by state) and the feature (or property of
    % the feature) to the compute_o function. Do not move with move()
    % function or something similar, since that would take too long,
    % instead transform desired properties of the elements direclty in the
    % compute_o function (e.g. transform boundary vertices)
    
    relval = nan(numel(activerelinds),1);
    if nargout >= 2
        relvar = nan(numel(activerelinds),1);
        if nargout >= 3
            relgrad = zeros(numel(activerelinds),numel(cat(1,obj.node2stateinds{:})));
        end
    end
    
    all2active = nan(1,numel(obj.pgraph.rels));
    all2active(activerelinds) = 1:numel(activerelinds);
    
    relfout = cell(1,max(1,min(3,nargout)));
    
    for i=1:numel(obj.relfs)
        
        rinds = all2active(obj.relfrelinds{i});
        activemask = not(isnan(rinds));
        
        if any(activemask)
            
            [relfout{:}] = obj.relfs{i}(...
                reshape(state(obj.relfinput1inds{i}(:,activemask)),size(obj.relfinput1inds{i}(:,activemask))),...
                reshape(state(obj.relfinput2inds{i}(:,activemask)),size(obj.relfinput2inds{i}(:,activemask))));
            
            relval(rinds(activemask)) = relfout{1};
            if nargout >= 2
                relvar(rinds(activemask)) = relfout{2};
                if nargout >= 3
                    relfinputstateinds = [obj.relfinput1inds{i}(:,activemask);obj.relfinput2inds{i}(:,activemask)];
                    inds = sub2ind(size(relgrad),repmat(rinds(activemask),size(relfinputstateinds,1),1),relfinputstateinds);
                    relgrad(inds(:)) = reshape([relfout{3}(:,:,1);relfout{3}(:,:,2)],[],1,1);
                end
            end
            
        end
    end
end

% use the vector objective function instead (with weight ~= 0 only for consistency)
% function [relval,relvar,residual,normresidual,wnormresidual] = relresidual(obj,state,activerelinds)

% update full state with given substate and re-compute active relatinoship values
% relationship gradient is given as #rel x #nodestate matrix
function [currentstate,currentstatevar,currentrelval,currentrelvar,currentrelgrad] = updateCurrentstate(...
        obj,fullstate,fullstatevar,substate,substateinds,activerelinds)
    
    % update state with given substate
    currentstate = fullstate;
    currentstate(substateinds) = substate;
    
    % evaluate relationships
    evalrelout = cell(1,max(1,min(3,nargout-2)));
    [evalrelout{:}] = obj.evalrels(currentstate,activerelinds);
    
    if nargout >= 3
        currentrelval = nan(numel(obj.pgraph.rels),1);
        currentrelval(activerelinds,:) = evalrelout{1};
        if nargout >= 4
            currentrelvar = nan(numel(obj.pgraph.rels),1);
            currentrelvar(activerelinds,:) = evalrelout{2};
            if nargout >= 5
                currentrelgrad = nan(numel(obj.pgraph.rels),numel(cat(1,obj.node2stateinds{:})));
                currentrelgrad(activerelinds,:) = evalrelout{3};
            end
        end
    end
    
    % update state variance if new relationship values for the current state have been computed
    currentstatevar = fullstatevar;
    if nargout >= 4
        currentstatevar(obj.relstateinds(activerelinds)) = currentrelvar(activerelinds);
    end
end

% estimate jacobian at given substate
function jac = estimate_jacobian(...
        obj,fullstate,fullstatevar,substate,substateinds,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        consistencyweight,fixweight,regularizationweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken)
    
    circargoutind = 4;
    jac = jacobianest(...
        @(x) obj.evalobjective_vector(...
            fullstate,fullstatevar,x,substateinds,...
            activeconsrelinds,activefixnodeinds,activeregnodeinds,...
            consistencyweight,fixweight,regularizationweight,...
            regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken),...
        substate,circargoutind);
end

% evaluate norm of the vector-valued objective function at given substate
function [val,grad,var] = evalobjective_vecnorm(...
        obj,fullstate,fullstatevar,substate,substateinds,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        consistencyweight,fixweight,regularizationweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken)
    
    objout = cell(1,max(0,min(2,nargout-1)));
    
    [vals,objout{:}] = obj.evalobjective_vector(...
        fullstate,fullstatevar,substate,substateinds,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        consistencyweight,fixweight,regularizationweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
    
    % squared l2 norm
    val = sum(vals.^2);
    if nargout >= 2
        jac = objout{1};

        % grad = (d(sum_i x_i^2) / d(x_1,...,x_n)) * jac (chain rule)
        % = (2*x_1,...,2*x_n) * jac
        grad = (2.*vals)' * jac;
        grad = grad';

        if nargout >= 3
            vars = objout{2};

            var = sum(2.*vars.^2);
        end
    end
end

% evaluate vector-valued objective function at given substate
function [vals,jac,vars,circrange] = evalobjective_vector(...
        obj,fullstate,fullstatevar,substate,substateinds,...
        activeconsrelinds,activefixnodeinds,activeregnodeinds,...
        consistencyweight,fixweight,regularizationweight,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken)
    
    if not(iscolumn(fullstate))
        error('fullstate must be given in column format.');
    end
    if not(iscolumn(fullstatevar))
        error('fullstatevar must be given in column format.');
    end
    
    if nargin < 5
        substate = fullstate;
        substateinds = (1:numel(fullstate))';
    else
        if not(iscolumn(substate))
            error('substate must be given in column format.');
        end
        if not(iscolumn(substateinds))
            error('substateinds must be given in column format.');
        end
    end
    
    if nargin < 8
        nrels = numel(obj.pgraph.rels);
        nnodes = numel(obj.pgraph.nodes);
        activeconsrelinds = 1:nrels;
        activefixnodeinds = 1:nnodes;
        activeregnodeinds = 1:nnodes;
    else
        if not(isrow(activeconsrelinds))
            error('activeconsrelinds must be given in row format.');
        end
        if not(isrow(activefixnodeinds))
            error('activefixnodeinds must be given in row format.');
        end
        if not(isrow(activeregnodeinds))
            error('activeregnodeinds must be given in row format.');
        end
    end
    
    if nargin < 11
        consistencyweight = 1000;
        fixweight = 500;
        regularizationweight = 1;
    end
    
    if nargin < 15
        regw_relfixed = 10;
        regw_elmgrpschanged = 1;
        regw_relgrpschanged = 1;
        regw_grpsbroken = 500;
    end
    
    % find all active relationships
    activerelinds = zeros(1,0);
    if consistencyweight ~= 0
        activerelinds = [activerelinds,activeconsrelinds];
    end
    if fixweight ~= 0
        activerelinds = [activerelinds,obj.pgraph.node2relinds(activefixnodeinds)];
    end
    if regularizationweight ~= 0
        activerelinds = [activerelinds,obj.pgraph.node2relinds(activeregnodeinds)];
    end
    activerelinds(isnan(activerelinds)) = [];
    activerelinds = unique(activerelinds);
    
    % get current state and evaluate active relationships
    currentrelgrad = cell(1,nargout>=2);
    
    [currentstate,currentstatevar,currentrelval,currentrelvar,currentrelgrad{:}] = obj.updateCurrentstate(...
        fullstate,fullstatevar,substate,substateinds,activerelinds);
    
    if nargout >= 2
        currentrelgrad = currentrelgrad{:};
    else
        currentrelgrad = [];
    end
    
    vals = zeros(0,1);
    if nargout >= 2
        jac = zeros(numel(vals),numel(substateinds));
        if nargout >= 3
            vars = zeros(size(vals));
            if nargout >= 4
                circrange = zeros(numel(vals),2);
            end
        end
    end
    
    % evaluate consistency functions
    if consistencyweight ~= 0

        consistencyout = cell(1,max(1,min(4,nargout)));
        
        [consistencyout{:}] = obj.consistency_vector(...
            currentstate,currentstatevar,currentrelval,currentrelvar,currentrelgrad,activeconsrelinds);
        
        vals = [vals;consistencyout{1}.*consistencyweight];
        if nargout >= 2
            jac = [jac;consistencyout{2}(:,substateinds).*consistencyweight];
            if nargout >= 3
                vars = [vars;consistencyout{3}.*consistencyweight^2];
                if nargout >= 4
                    circrange = [circrange;consistencyout{4}.*consistencyweight];
                end
            end
        end
        
    end
    
    % evaluate fixed node functions
    if fixweight ~= 0

        fixout = cell(1,max(1,min(4,nargout)));
        
        [fixout{:}] = obj.fix_vector(...
            currentstate,currentstatevar,activefixnodeinds);
        
        vals = [vals;fixout{1}.*fixweight];
        if nargout >= 2
            jac = [jac;fixout{2}(:,substateinds).*fixweight];
            if nargout >= 3
                vars = [vars;fixout{3}.*fixweight^2];
                if nargout >= 4
                    circrange = [circrange;fixout{4}.*fixweight];
                end
            end
        end
        
    end
    
    % evaluate regularization functions
    if regularizationweight ~= 0
        
        regularizationout = cell(1,max(1,min(4,nargout)));
        
        [regularizationout{:}] = obj.regularization_vector(...
            currentstate,currentstatevar,activeregnodeinds,...
            regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken);
        
        vals = [vals;regularizationout{1}.*regularizationweight];
        if nargout >= 2
            jac = [jac;regularizationout{2}(:,substateinds).*regularizationweight];
            if nargout >= 3
                vars = [vars;regularizationout{3}.*regularizationweight^2];
                if nargout >= 4
                    circrange = [circrange;regularizationout{4}.*regularizationweight];
                end
            end
        end
    end
end

function [vals,jac,vars,circrange] = consistency_vector(obj,...
        state,statevar,relval,relvar,relgrad,activerelinds)
    
    % residuals of angles close to pi or -pi (maximum residual) are a
    % problem, because the gradient there is not defined, but since we only
    % should have small residuals (small steps), we should not get that
    % large residuals
    
    residual = relval(activerelinds) - state(obj.relstateinds(activerelinds));
    circularmask = obj.relcircular(activerelinds);
    if any(circularmask)
        residual(circularmask) = smod(residual(circularmask),-pi,pi);
    end
    fweightnorm = (obj.relweights(activerelinds)' ./ sqrt(relvar(activerelinds)));
    vals = residual .* fweightnorm;

    if nargout >= 2
        % jacobian
        
        jac = zeros(numel(vals),numel(state));
        nodestateinds = cat(1,obj.node2stateinds{:});
        jac(:,nodestateinds) = relgrad(activerelinds,:);
        jac(:,nodestateinds) = bsxfun(@times,jac(:,nodestateinds),fweightnorm);        
        
        % include d(vals_i) / d(state_{rel2state(i)}) (how much the
        % weighted relationship function residual changes by changing the
        % relationship function = -1*weight, since the relationship function value is subtracted to get the residual)
        jacinds = sub2ind(size(jac),(1:numel(activerelinds))',obj.relstateinds(activerelinds));
        jac(jacinds) = jac(jacinds) - fweightnorm;

        if nargout >= 3
            % variance
            
            vars = (relvar(activerelinds)+statevar(obj.relstateinds(activerelinds))) .* fweightnorm.^2;

            if nargout >= 4
                % range of circular values
                
                circrange = nan(numel(vals),2);
                circrange(circularmask,:) = bsxfun(@times,[-pi,pi],fweightnorm(circularmask));
            end
        end
    end
end

function [vals,jac,vars,circrange] = fix_vector(obj,...
        state,statevar,activenodeinds)
    
    vals = zeros(0,1);
    if nargout >= 2
        jac = zeros(0,numel(state));
        if nargout >= 3
            vars = zeros(0,1);
            if nargout >= 4
                circrange = nan(0,2);
            end
        end
    end
    
    nodefixnewweights = state(obj.nodefixnew2stateinds(activenodeinds));

    % need to use all active nodes (also those with weight zero), otherwise
    % the number of rows in he jacobian and the number of values returned
    % by the function may change for different states
    
    activeelminds = activenodeinds-obj.pgraph.elmoffset;
    activeelmfixnewweights = nodefixnewweights;
    mask = activeelminds < 1 | activeelminds > numel(obj.pgraph.elms);
    activeelminds(mask) = [];
    activeelmfixnewweights(mask) = [];
    
    % penalty for elements that are fixed to the new value
    elmfixnewvals = zeros(numel(activeelminds)*numel(obj.settings.posefixdims),1);
    if nargout >= 2
        elmfixnewjac = zeros(numel(elmfixnewvals),numel(state));
        if nargout >= 3
            elmfixnewvars = zeros(numel(elmfixnewvals),1); %#ok<PREALL>
            if nargout >= 4
                elmfixnewcircrange = nan(numel(elmfixnewvals),2);
            end
        end
    end

    elmpose2funinds = nan(numel(obj.settings.posefixdims),numel(obj.pgraph.elms));
    elmpose2funinds(:,activeelminds) = reshape(1 : numel(activeelminds)*numel(obj.settings.posefixdims),numel(obj.settings.posefixdims),[]);

    residual = ...
        reshape(state(obj.elmposestateinds(obj.settings.posefixdims,activeelminds)),...
        size(obj.elmposestateinds(obj.settings.posefixdims,activeelminds))) - ...
        obj.newelmposes(obj.settings.posefixdims,activeelminds); % obj.oldelmposes(:,activeelmmask);
    circularmask = obj.elmposecircular(obj.settings.posefixdims,activeelminds);
    residual(circularmask) = smod(residual(circularmask),-pi,pi);
    residual = residual(:);

    fnorm = bsxfun(@rdivide,activeelmfixnewweights',sqrt(obj.elmposevar(obj.settings.posefixdims)));
    fnorm = fnorm(:);
    elmfixnewvals = residual .* fnorm;

    vals = [vals; elmfixnewvals];
    if nargout >= 2
        % d elmfixnewvals / d residual
        jacinds = sub2ind(size(elmfixnewjac),...
            elmpose2funinds(:,activeelminds),...
            obj.elmposestateinds(obj.settings.posefixdims,activeelminds));
        jacinds = jacinds(:);
        elmfixnewjac(jacinds) = elmfixnewjac(jacinds) + fnorm;

        % d elmfixnewvals / d activeelmfixnewweights
        activeelmfixnewstateinds = obj.nodefixnew2stateinds(activeelminds+obj.pgraph.elmoffset);
        jacinds = sub2ind(size(elmfixnewjac),...
            elmpose2funinds(:,activeelminds),...
            repmat(activeelmfixnewstateinds,numel(obj.settings.posefixdims),1));
        jacinds = jacinds(:);
        elmfixnewjac(jacinds) = elmfixnewjac(jacinds) + residual ./ repmat(sqrt(obj.elmposevar(obj.settings.posefixdims)),numel(activeelminds),1);

        jac = [jac; elmfixnewjac];
        if nargout >= 3
            elmfixnewvars = ...
                repmat(obj.elmposevar(obj.settings.posefixdims),1,numel(activeelminds)) + ...
                statevar(obj.elmposestateinds(obj.settings.posefixdims,activeelminds));
            elmfixnewvars = elmfixnewvars(:) .* fnorm.^2;

            vars = [vars; elmfixnewvars]; 
            if nargout >= 4
                elmfixnewcircrange(circularmask(:),:) = bsxfun(@times,[-pi,pi],fnorm(circularmask(:)));

                circrange = [circrange; elmfixnewcircrange];
            end
        end
    end
    
end

function [vals,jac,vars,circrange] = regularization_vector(obj,...
        state,statevar,activenodeinds,...
        regw_relfixed,regw_elmgrpschanged,regw_relgrpschanged,regw_grpsbroken) %#ok<INUSL>
    
    % todo: add to reg (?): preserve 90 or 180 degree angles, etc.
    
    
    vals = zeros(0,1);
    if nargout >= 2
        jac = zeros(0,numel(state));
        if nargout >= 3
            vars = zeros(0,1);
            if nargout >= 4
                circrange = nan(0,2);
            end
        end
    end
    
    activerelinds = obj.pgraph.node2relinds(activenodeinds);
    activerelinds(isnan(activerelinds)) = [];
    activeelminds = activenodeinds-obj.pgraph.elmoffset;
    activeelminds(activeelminds < 1 | activeelminds > numel(obj.pgraph.elms)) = [];

    activerelmask = false(1,numel(obj.pgraph.rels));
    activerelmask(activerelinds) = true;
    
    % how many element groups changed
    if regw_elmgrpschanged ~= 0
        
        elmgrpchangedvals = zeros(numel(activeelminds)*obj.settings.elmposedim,1);
        if nargout >= 2
            elmgrpchangedjac = zeros(numel(elmgrpchangedvals),numel(state));
            if nargout >= 3
                elmgrpchangedvars = zeros(numel(elmgrpchangedvals),1); %#ok<PREALL>
                if nargout >= 4
                    elmgrpchangedcircrange = nan(numel(elmgrpchangedvals),2);
                end
            end
        end
        
        elmpose2funinds = nan(obj.settings.elmposedim,numel(obj.pgraph.elms));
        elmpose2funinds(:,activeelminds) = reshape(1 : numel(activeelminds)*obj.settings.elmposedim,obj.settings.elmposedim,[]);
        
        elmgrpschangedweight = full(obj.elmgrps(activeelminds,:) * (1./sum(obj.elmgrps(activeelminds,:),1)'));
        
        residual = ...
            reshape(state(obj.elmposestateinds(:,activeelminds)),size(obj.elmposestateinds(:,activeelminds))) - ...
            obj.newelmposes(:,activeelminds); % obj.oldelmposes(:,activeelmmask);
        circularmask = obj.elmposecircular(:,activeelminds);
        residual(circularmask) = smod(residual(circularmask),-pi,pi);
        residual = residual(:);
        
        fnorm = bsxfun(@rdivide,elmgrpschangedweight',sqrt(obj.elmposevar));
        fnorm = fnorm(:);
        elmgrpchangedvals = residual .* fnorm;
        
        vals = [vals; elmgrpchangedvals .* regw_elmgrpschanged];
        if nargout >= 2
            jacinds = sub2ind(size(elmgrpchangedjac),elmpose2funinds(:,activeelminds),obj.elmposestateinds(:,activeelminds));
            jacinds = jacinds(:);
            elmgrpchangedjac(jacinds) = elmgrpchangedjac(jacinds) + fnorm;
            
            jac = [jac; elmgrpchangedjac .* regw_elmgrpschanged];
            if nargout >= 3
                elmgrpchangedvars = ...
                    repmat(obj.elmposevar,1,numel(activeelminds)) + ...
                    statevar(obj.elmposestateinds(:,activeelminds));
                elmgrpchangedvars = elmgrpchangedvars(:) .* fnorm.^2;
                
                vars = [vars; elmgrpchangedvars .* regw_elmgrpschanged^2]; 
                if nargout >= 4
                    elmgrpchangedcircrange(circularmask(:),:) = bsxfun(@times,[-pi,pi],fnorm(circularmask(:)));
                    
                    circrange = [circrange; elmgrpchangedcircrange .* regw_elmgrpschanged];
                end
            end
        end
    end
    
    % how many relationship groups changed
    if regw_relgrpschanged ~= 0
        
        relgrpchangedvals = zeros(numel(activerelinds),1);
        if nargout >= 2
            relgrpchangedjac = zeros(numel(relgrpchangedvals),numel(state));
            if nargout >= 3
                relgrpchangedvars = zeros(numel(relgrpchangedvals),1); %#ok<PREALL>
                if nargout >= 4
                    relgrpchangedcircrange = nan(numel(relgrpchangedvals),2);
                end
            end
        end
        
        rel2funinds = nan(numel(obj.pgraph.rels),1);
        rel2funinds(activerelinds) = 1:numel(activerelinds);
        
        relgrpschangedweight = full(obj.relgrps(activerelinds,:) * (1./sum(obj.relgrps(activerelinds,:),1)'));
        
        % compute changed relationship value penalty
        residual = state(obj.relstateinds(activerelinds)) - obj.origrelvals(activerelinds)';
        circularmask = obj.relcircular(activerelinds);
        if any(circularmask)
            residual(circularmask) = smod(residual(circularmask),-pi,pi);
        end
        fnorm = relgrpschangedweight ./ sqrt(obj.origrelvars(activerelinds)');
        relgrpchangedvals = residual .* fnorm;
        
        vals = [vals; relgrpchangedvals .* regw_relgrpschanged];
        if nargout >= 2
            % d relgrpchangedvals_i / d activerel_i = fnorm;
            jacinds = sub2ind(size(relgrpchangedjac),rel2funinds(activerelinds),obj.relstateinds(activerelinds));
            relgrpchangedjac(jacinds) = relgrpchangedjac(jacinds) + fnorm;
            
            jac = [jac; relgrpchangedjac .* regw_relgrpschanged];
            if nargout >= 3
                relgrpchangedvars = (obj.origrelvars(activerelinds)' + statevar(obj.relstateinds(activerelinds))) .* fnorm.^2;
                
                vars = [vars; relgrpchangedvars .* regw_relgrpschanged^2];
                if nargout >= 4
%                     circularmask = obj.relcircular(activerelinds)';
                    relgrpchangedcircrange(circularmask',:) = bsxfun(@times,[-pi,pi],fnorm(circularmask'));
                    
                    circrange = [circrange; relgrpchangedcircrange .* regw_relgrpschanged];
                end
            end
        end
    end

    % how many groups are broken up
    if regw_grpsbroken ~= 0
        
        % get active groups (with more than two active relationships),
        % their group members and a running index over group members

        nfuns = 0;
        grprelinds = cell(1,numel(obj.relgrprelinds));
        grpfuninds = cell(1,numel(obj.relgrprelinds));        
        activegrpinds = find(sum(obj.relgrps(activerelinds,:),1) >= 2);
        for i=activegrpinds
            grprelinds{i} = obj.relgrprelinds{i}(activerelmask(obj.relgrprelinds{i}));
            grpfuninds{i} = nfuns+1 : nfuns+numel(grprelinds{i});
            nfuns = nfuns + numel(grprelinds{i});
        end
        
        grpbrokenvals = zeros(nfuns,1);
        if nargout >= 2
            grpbrokenjac = zeros(numel(grpbrokenvals),numel(state));
            if nargout >= 3
                grpbrokenvars = zeros(numel(grpbrokenvals),1);    
                if nargout >= 4
                    grpbrokencircrange = nan(numel(grpbrokenvals),2);
                end
            end
        end
        
        if strcmp(obj.settings.relgrouptype,'change')
            
            % compute one group penalty vaue per active group member
            % group penaly is difference to average value CHANGE in the group
            % !also change the assignment of new values to a group in
            % StochasticLinearSolver.getCombinationState!
            for i=activegrpinds
                grpstateinds = obj.relstateinds(grprelinds{i});
                grprelvals = state(grpstateinds);
                grprelchange = grprelvals - obj.origrelvals(grprelinds{i})';
                grprelvar = statevar(grpstateinds);
                if obj.relgrpcircular(i)
                    grpavgchange = circ_mean(grprelchange,[],1);
                else
                    grpavgchange = mean(grprelchange);
                end
                residual = grprelchange - grpavgchange;
                if obj.relgrpcircular(i)
                    residual = smod(residual,-pi,pi);
                end

                % can't use squared because residual would be zero when
                % group is not broken (need signed residual instead)
                fnorm = 1./sqrt(grprelvar);
                grpbrokenvals(grpfuninds{i}) = residual .* fnorm;

                if nargout >= 2

                    % d grpbrokenvals(grpfuninds{i})_j / d grprelvals_j = fnorm_j - fnorm_j/numel(grpstateinds)
                    % d grpbrokenvals(grpfuninds{i})_j / d grprelvals_k~=j = -fnorm_j/numel(grpstateinds)
                    jacinds = sub2ind(size(grpbrokenjac),grpfuninds{i}',grpstateinds);
                    grpbrokenjac(jacinds) = grpbrokenjac(jacinds) + fnorm;
                    grpbrokenjac(grpfuninds{i}',grpstateinds) = grpbrokenjac(grpfuninds{i}',grpstateinds) - repmat(fnorm./numel(grpstateinds),1,numel(grpstateinds));

                    if nargout >= 3

                        % var(sum(x_i)) = sum(var(x_i))
                        % var(a * x_i) = a^2 * var(x_i)
                        % =>
                        % var((x_a - sum(x_i)/n) * fnorm)
                        % = var(x_a - sum(x_i)/n) * fnorm^2
                        % = (var(x_a) + var(sum(x_i)/n)) * fnorm^2
                        % = (var(x_a) + sum(var(x_i)/n^2)) * fnorm^2
                        grpbrokenvars(grpfuninds{i}) = (grprelvar + sum(grprelvar)./numel(grprelvar)^2) .* fnorm.^2;

                        if nargout >= 4
                            circularmask = obj.relcircular(grprelinds{i})';
                            grpbrokencircrange(grpfuninds{i}(circularmask),:) = bsxfun(@times,[-pi,pi],fnorm(circularmask));
                        end
                    end
                end
            end
            
        elseif strcmp(obj.settings.relgrouptype,'value')
           
            % compute one group penalty vaue per active group member
            % group penaly is difference to average value in the group
            % !also change the assignment of new values to a group in
            % StochasticLinearSolver.getCombinationState!
            for i=activegrpinds
                grpstateinds = obj.relstateinds(grprelinds{i});
                grprelvals = state(grpstateinds);
                grprelvar = statevar(grpstateinds);
                if obj.relgrpcircular(i)
                    grpavg = circ_mean(grprelvals,[],1);
                else
                    grpavg = mean(grprelvals);
                end
                residual = grprelvals - grpavg;
                if obj.relgrpcircular(i)
                    residual = smod(residual,-pi,pi);
                end

                % can't use squared because residual would be zero when
                % group is not broken (need signed residual instead)
                fnorm = 1./sqrt(grprelvar);
                grpbrokenvals(grpfuninds{i}) = residual .* fnorm;

                if nargout >= 2

                    % d grpbrokenvals(grpfuninds{i})_j / d grprelvals_j = fnorm_j - fnorm_j/numel(grpstateinds)
                    % d grpbrokenvals(grpfuninds{i})_j / d grprelvals_k~=j = -fnorm_j/numel(grpstateinds)
                    jacinds = sub2ind(size(grpbrokenjac),grpfuninds{i}',grpstateinds);
                    grpbrokenjac(jacinds) = grpbrokenjac(jacinds) + fnorm;
                    grpbrokenjac(grpfuninds{i}',grpstateinds) = grpbrokenjac(grpfuninds{i}',grpstateinds) - repmat(fnorm./numel(grpstateinds),1,numel(grpstateinds));

                    if nargout >= 3

                        % var(sum(x_i)) = sum(var(x_i))
                        % var(a * x_i) = a^2 * var(x_i)
                        % =>
                        % var((x_a - sum(x_i)/n) * fnorm)
                        % = var(x_a - sum(x_i)/n) * fnorm^2
                        % = (var(x_a) + var(sum(x_i)/n)) * fnorm^2
                        % = (var(x_a) + sum(var(x_i)/n^2)) * fnorm^2
                        grpbrokenvars(grpfuninds{i}) = (grprelvar + sum(grprelvar)./numel(grprelvar)^2) .* fnorm.^2;

                        if nargout >= 4
                            circularmask = obj.relcircular(grprelinds{i})';
                            grpbrokencircrange(grpfuninds{i}(circularmask),:) = bsxfun(@times,[-pi,pi],fnorm(circularmask));
                        end
                    end
                end
            end
            
        else
            error('Unknown relationship group type.');
        end
        
        % append to outputs
        vals = [vals; grpbrokenvals .* regw_grpsbroken];
        if nargout >= 2
            jac = [jac; grpbrokenjac .* regw_grpsbroken];
            if nargout >= 3
                vars = [vars; grpbrokenvars .* regw_grpsbroken^2];    
                if nargout >= 4
                    circrange = [circrange; grpbrokencircrange .* regw_grpsbroken];
                end
            end
        end
    
    end
    
end

% set nodefixorig and nodefixnew state of relationships based
% on closeness of the estimate to orig/new relationship values
function state = nodefixFromNodestate(obj,state)
    origreldist = state(obj.relstateinds) - obj.origrelvals';
    if any(obj.relcircular)
        origreldist(obj.relcircular) = smod(origreldist(obj.relcircular),-pi,pi);
    end
    state(obj.nodefixorig2stateinds(obj.pgraph.rel2nodeinds)) = ...
        exp(-origreldist.^2 ./ (obj.settings.synctolerance.*2.*obj.origrelvars'));
    
    newreldist = state(obj.relstateinds) - obj.newrelvals';
    if any(obj.relcircular)
        newreldist(obj.relcircular) = smod(newreldist(obj.relcircular),-pi,pi);
    end
    state(obj.nodefixnew2stateinds(obj.pgraph.rel2nodeinds)) = ...
        min(...
            1-state(obj.nodefixorig2stateinds(obj.pgraph.rel2nodeinds)),...
            exp(-newreldist.^2 ./ (2.*obj.origrelvars')));
end

% set groupfix state of relationships based on closeness of the estimate to
% orig/new relationship values
function state = groupfixFromNodestate(obj,state,statevar)
    
%     rgroupinds = find(obj.rgroupmask);
    for i=1:numel(obj.relgrprelinds)
        grpstateinds = obj.relstateinds(obj.relgrprelinds{i});
        
        grprelvals = state(grpstateinds);
        grprelvar = statevar(grpstateinds);
        if obj.relgrpcircular(i)
            grpavg = circ_mean(grprelvals,[],1);
        else
            grpavg = mean(grprelvals);
        end
        residual = grprelvals - grpavg;
        if obj.relgrpcircular(i)
            residual = smod(residual,-pi,pi);
        end
        
        state(obj.grpfix2stateinds(obj.relgrpinds(i))) = min(exp(-residual.^2 ./ (2.*grprelvar)));
    end
end

function solpgs = createSolutionPgraphs(obj,solinds,recomputerels)
    
    if nargin < 3 || isempty(recomputerels)
        recomputerels = true;
    end
    
    solpgs = PatternGraph.empty(1,0);
    
    for i=1:numel(solinds)
        solpgs(i) = obj.pgraph.clone;
        solelmposes = obj.solutionelmposes{solinds(i)};

        % update all element instances
        posediff = solelmposes - [obj.pgraph.elms.pose2D];
        posediff(3:4,:) = smod(posediff(3:4,:),-pi,pi);
        elmchanged = any(bsxfun(@rdivide,...
            posediff.^2,...
            obj.pgraph.elmposevar) > ...
            obj.settings.synctolerance*0.01,1);
        solelmposes = PatternWidget.updateInstances(solelmposes,obj.pgraph,1:size(solpgs(i).instgroups,2),elmchanged);

        % update all element poses in the new pattern
        % graph and update hierarchy childs from parents
        solpgs(i).transformElements(1:numel(solpgs(i).elms),solelmposes);

        % update all hierarchy elements from childs
        solpgs(i).updateHierarchyelements; 

        if recomputerels
            solpgs(i).recomputeRelationships;
        end
    end
end

end

methods(Static)

function keepinds = findUniqueStates(state,statevar,clusterthreshold)
    dists = zeros(size(state,2));
    for i=1:size(state,2)-1
        dists(i,i+1:end) = sqrt(sum(bsxfun(@rdivide, bsxfun(@minus,state(:,i),state(:,i+1:end)), sqrt(statevar(:,i))).^2,1));
    end
    dists = dists ./ size(state,1);
    
    dists = dists + dists';

    links = linkage(squareform(dists,'tovector'),'complete');

    clusterinds = cluster(links,'cutoff',clusterthreshold,'criterion','distance');
    clusters = array2cell_mex(1:numel(clusterinds),clusterinds');
    keepinds = cellfun(@(x) x(1),clusters);

    keepinds = sort(keepinds,'ascend');
end

end

end

