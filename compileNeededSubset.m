function compileNeededSubset(options)
    
    if nargin < 1
        options = {};
    end
    
    utilpath = 'mlutil';
    
    [hasarch,~,precompiledarch,precompiledrtime,envarch,envrtimes,selrtimeind] = checkRuntime('precompiled.conf');
    if isempty(selrtimeind)
        mex -setup;
        [hasarch,~,precompiledarch,precompiledrtime,envarch,envrtimes,selrtimeind] = checkRuntime('precompiled.conf');
        if isempty(selrtimeind)
            error('No suitable runtime environment found for compilation.');
        end
    end
    
    % unload all currently loaded mex files
    clear mex;
    
    mask = strcmp(precompiledarch,envarch);
    precompiledrtime(mask) = [];
    precompiledarch(mask) = [];
    
    % compile
    currentfolder = pwd;
    try
        disp('compiling utility functions...');
        cd mlutil;
        compileMlutil(options);
        cd(currentfolder);
        disp('compiling geometry functions...');
        cd mlgeometry;
        compileMlgeometry(options,['../',utilpath]);
        cd(currentfolder);
    catch err
        cd(currentfolder);
        if hasarch
            % need to recompile for the current runtime, remove from list
            % of precompiled runtimes
            writePrecompiledRuntimes('precompiled.conf',...
                precompiledarch,precompiledrtime);
        end
        rethrow(err);
    end
    
    precompiledarch{end+1} = envarch;
    precompiledrtime{end+1} = envrtimes{selrtimeind};
    writePrecompiledRuntimes('precompiled.conf',...
        precompiledarch,precompiledrtime);
    
    disp('Everything compiled.');
end
