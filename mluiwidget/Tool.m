classdef Tool < AxesWidget
    
properties
%     name = '';
end

methods
    
function obj = Tool(varargin)
    obj = obj@AxesWidget(varargin{:});
    
    obj.name = obj.name_s;
end
    
end

methods (Abstract,Static)
    n = name_s;
end
    
end
