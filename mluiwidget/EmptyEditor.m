classdef EmptyEditor < AxesWidget
    
properties(SetAccess = protected)
    
    polygons = ScenePolygon.empty;
    
    % ui elements
    navwidget = NavigationWidget.empty;
    
    polygonsVisible = true;
    
    sidepanelVisible = false;
end

properties(SetObservable, AbortSet)
    selpolyinds = zeros(1,0);
    selpanel = zeros(1,0);
    
    tool = '';
end

properties(Access=protected)
    
    fig = gobjects(1,0);
    toolpanel = gobjects(1,0);
    mainpanel = gobjects(1,0);
    sidepanel = gobjects(1,0);
    
    selpolyindsListener = event.listener.empty;
    toolListener = event.listener.empty;
    
    uigroup = UIHandlegroup.empty;
    
    gmenu_polygons = gobjects(1,0); 
    gmenu_polygonselect = gobjects(1,0); 
    
    gtoolbuttons = gobjects(1,0);
    
    gpolygons = gobjects(1,0);
    
    lastpolygonloadname = '';
    lastpolygonsavename = '';
end

methods
   
function obj = EmptyEditor(ax,navwidget,fig,toolpanel,mainpanel,menus)
    obj@AxesWidget(ax,'Empty Editor');

    obj.navwidget = navwidget;
    
    obj.fig = fig;
    obj.toolpanel = toolpanel;
    obj.mainpanel = mainpanel;
    
    obj.uigroup = UIHandlegroup;
    
    obj.sidepanel = uipanel(obj.fig,...
        'Position',[0,0,1,1],... % will be adjusted in layout panels
        'BorderType','line',...
        'BorderWidth',5,...
        'HighlightColor',[0.7,0.7,0.7],...
        'Visible','off');
    obj.uigroup.addChilds(obj.sidepanel);
    
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Load polygons...',...
        'Callback', {@(src,evt) obj.loadPolygons},...
        'Separator','on'));
    obj.uigroup.addChilds(uimenu(menus.File,...
        'Label','Save Polygons...',...
        'Callback', {@(src,evt) obj.savePolygons}));
    
    obj.gmenu_polygons = uimenu(menus.Vis,...
        'Label','Polygons',...
        'Callback', {@(src,evt) obj.togglePolygonsVisible},...
        'Checked',iif(obj.polygonsVisible,'on','off'),...
        'Separator','on');
    obj.uigroup.addChilds(obj.gmenu_polygons);
    
    obj.uigroup.addChilds(uimenu(menus.Actions,...
        'Label','Delete Polygon',...
        'Callback', {@(src,evt) obj.deletePolygonsPressed}));
    
    obj.gmenu_polygonselect = uimenu(menus.Tools,...
        'Label','Select/Move Polygon',...
        'Checked',iif(strcmp(obj.tool,'polygonselect'),'on','off'),...
        'Callback', {@(src,evt) obj.selectPolygonsPressed});
    obj.uigroup.addChilds(obj.gmenu_polygonselect);
    
    obj.gtoolbuttons = uibuttongroup(toolpanel,...
        'Position',[0,0,0.45,1],...
        'SelectedObject',[],...
        'SelectionChangedFcn',{@(src,evt) obj.toolbuttonPressed});
    obj.uigroup.addChilds(obj.gtoolbuttons);
    obj.uigroup.addChilds(uicontrol(obj.gtoolbuttons,...
        'Style','togglebutton',...
        'String','Select Polygon',...
        'Min',0,'Max',1,...
        'Value',iif(strcmp(obj.tool,'polygonselect'),1,0),...
        'Tag','polygonselect',...
        'Units','normalized',...
        'Position',[0,0.5,0.33,0.5]));
    % apparently the first button added to the button group gets selected,
    % regardless of its value
    obj.gtoolbuttons.Children(1).Value = iif(strcmp(obj.tool,'polygonselect'),1,0);
    
    obj.selpolyindsListener = obj.addlistener('selpolyinds','PostSet',@(src,evt) obj.selpolyindsChanged);
    obj.toolListener = obj.addlistener('tool','PostSet',@(src,evt) obj.toolChanged);

    obj.hide;
end

function delete(obj)
    obj.hide();
    
    delete(obj.polygons(isvalid(obj.polygons)));
    
    delete(obj.selpolyindsListener(isvalid(obj.selpolyindsListener)));
    delete(obj.toolListener(isvalid(obj.toolListener)));
    
    delete(obj.uigroup(isvalid(obj.uigroup)));
end

function clear(obj)
    obj.selpolyinds = zeros(1,0);
    obj.tool = '';
end

function loadPolygons(obj,filename)
    
    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastpolygonloadname))
            loadname = obj.lastpolygonloadname;
        else
            loadname = 'temppolygons.svg';
        end

        [fname,pathname,~] = ...
            uigetfile({'*.svg','SVG (*.svg)'},'Load Polygons',loadname);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    [type,polyverts] = loadSvg(filename);
    polyverts = polyverts(strcmp(type,'polygon'));
    
    polys = ScenePolygon.empty(1,0);
    for i=1:numel(polyverts)
        if not(isempty(polyverts{i}))
            polys(end+1) = ScenePolygon(polyverts{i},''); %#ok<AGROW>
        end
    end
    
    obj.addPolygons(polys);
    
    obj.updateMainaxes;
    resetCamera(obj.axes);
    obj.showPolygons;
    
    obj.lastpolygonloadname = filename;
    
    disp('done');
end

function savePolygons(obj,filename)

    if nargin < 2 || isempty(filename)
        if not(isempty(obj.lastpolygonsavename))
            savename = obj.lastpolygonsavename;
        else
            savename = 'temppolygons.svg';
        end
        
        [fname,pathname,~] = ...
            uiputfile({'*.svg','SVG (*.svg)'},'Save Polygons',savename);
        
        if not(isnumeric(fname) && fname == 0)
            filename = [pathname,fname];
        else
            return;
        end
    end
    
    saveSvg(filename,repmat({'polygon'},1,numel(obj.polygons)),{obj.polygons.verts});
    
    obj.lastpolygonsavename = filename;
    
    disp('done');
end

function addPolygons(obj,polygons)
    obj.polygons = [obj.polygons,polygons];
end

function removePolygons(obj,inds)
    if isa(inds,'ScenePolygon')
        inds = find(ismember(obj.polygons,inds));
    end
    
    delete(obj.polygons(inds(isvalid(obj.polygons(inds)))));
    obj.polygons(inds) = [];
end

function unblock(obj)
    obj.blockcallbacks = false;
    obj.navwidget.unblock;
end

function mousePressed(obj,src,evt)

    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;
    
    obj.selpanel = gparent(obj.fig.CurrentObject,'uipanel',true);
    
    obj.mousedown = true;

    obj.mousepos = mouseposOnPlane(obj.axes);
    obj.clickedpos = obj.mousepos;
    
    if obj.selpanel == obj.mainpanel
        if obj.navwidget.zoommode || ...
           obj.navwidget.panmode || ...
           obj.navwidget.orbitmode

            obj.navwidget.mousePressed(src,evt);
        elseif strcmp(obj.tool,'polygonselect')
            % find closest, then smallest annotation
                
            polyind = EmptyEditor.closestPolygon(...
                obj.clickedpos(1:2),{obj.polygons.verts});

            obj.selpolyinds = selectMultiple(obj.selpolyinds,polyind,src.SelectionType);
        elseif strcmp(obj.tool,'')
            % do nothing
        else
            error('Unknown tool.')
        end
    elseif obj.selpanel == obj.sidepanel
        % do nothing
    else
        % do nothing
    end

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt)

    if obj.mousedown

        if obj.blockcallbacks
            return;
        end
        obj.blockcallbacks = true;

        obj.mousepos = mouseposOnPlane(obj.axes);

        if obj.selpanel == obj.mainpanel
            if obj.navwidget.zoommode || ...
               obj.navwidget.panmode || ...
               obj.navwidget.orbitmode
                % camera navigation

                obj.navwidget.mouseMoved(src,evt);
            elseif strcmp(obj.tool,'polygonselect')
                % do nothing
            elseif strcmp(obj.tool,'')
                % do nothing
            else
                error('Unknown tool.')
            end
        elseif obj.selpanel == obj.sidepanel
            % do nothing
        end

        obj.blockcallbacks = false;
    end
end

function mouseReleased(obj,src,evt)

    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;

    if obj.selpanel == obj.mainpanel
        if obj.navwidget.zoommode || ...
           obj.navwidget.panmode || ...
           obj.navwidget.orbitmode

            obj.navwidget.mouseReleased(src,evt);
        elseif strcmp(obj.tool,'polygonselect')
            % do nothing
        elseif strcmp(obj.tool,'')
            % do nothing
        else
            error('Unknown tool.')
        end
    elseif obj.selpanel == obj.sidepanel
        % do nothing
    end

    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt) %#ok<INUSL>
    if strcmp(evt.Key,'1')
        if obj.navwidget.panmode || obj.navwidget.zoommode || obj.navwidget.orbitmode
            obj.navwidget.setPanmode(false);
        else
            obj.navwidget.setPanmode(true);
        end
    elseif strcmp(evt.Key,'c')
        resetCamera(obj.axes);
    elseif obj.selpanel == obj.mainpanel
        if strcmp(obj.tool,'polygonselect')    
            if strcmp(evt.Key,'delete')
                obj.removePolygons(obj.selpolygoninds);
                obj.selpolygoninds = zeros(1,0);
            elseif strcmp(evt.Key,'a')
                obj.selpolygoninds = 1:numel(obj.polygons);
            end
        end
    end
end

function keyReleased(obj,src,evt) %#ok<INUSD>
    % do nothing
end

function selpolygonindsChanged(obj)
    if obj.polygonsVisible
        obj.showPolygons;
    end
end

function toolbuttonPressed(obj)
    if isempty(obj.gtoolbuttons.SelectedObject)
        obj.tool = '';
    else
        obj.tool = obj.gtoolbuttons.SelectedObject.Tag;
    end
end

function toolChanged(obj)
    toolbuttonind = find(strcmp({obj.gtoolbuttons.Children.Tag},'polygonselect'),1,'first');
    if strcmp(obj.tool,'polygonselect')
        obj.gmenu_polygonselect.Checked = 'on';
        obj.gtoolbuttons.Children(toolbuttonind).Value = 1;
        if not(obj.polygonsVisible)
            obj.togglePolygonsVisible;
        end
    else
        obj.gmenu_polygonselect.Checked = 'off';
        obj.gtoolbuttons.Children(toolbuttonind).Value = 0;
    end
end

function selpolyindsChanged(obj)
    if obj.polygonsVisible
        obj.showPolygons;
    end
end

function deletePolygonsPressed(obj)
    obj.removePolygons(obj.selpolygoninds);
    obj.selpolygoninds = zeros(1,0);
end

function selectPolygonsPressed(obj)
    if strcmp(obj.tool,'polygonselect')
        obj.tool = '';
    else
        obj.tool = 'polygonselect';
    end
end

function togglePolygonsVisible(obj)
    obj.polygonsVisible = not(obj.polygonsVisible);
    if obj.polygonsVisible
        set(obj.gmenu_polygons,'Checked','on');
        obj.showPolygons;
    else
        set(obj.gmenu_polygons,'Checked','off');
        obj.showPolygons;
    end    
end

function layoutPanels(obj)
    if isgraphics(obj.toolpanel) && ...
       isgraphics(obj.mainpanel) && ...
       isgraphics(obj.sidepanel)
   
        
        mainpanelpos = obj.mainpanel.Position;
        toolpanelpos = obj.toolpanel.Position;
        sidepanelpos = obj.sidepanel.Position;
        
        if obj.toolpanel.Visible
            sidepanelpos(4) = max(1-toolpanelpos(4),0.0001);
            mainpanelpos(4) = max(1-toolpanelpos(4),0.0001);
        else
            sidepanelpos(4) = 1;
            mainpanelpos(4) = 1;
        end
        
        if obj.sidepanelVisible
            sidepanelpos(1) = 0.8;
            sidepanelpos(3) = 0.2;
            
            mainpanelpos(3) = max(1-sidepanelpos(3),0.0001);
        else
            sidepanelpos(1) = 1;
            sidepanelpos(3) = 0;
            
            mainpanelpos(3) = 1;
        end

        if any(obj.mainpanel.Position ~= mainpanelpos)
            obj.mainpanel.Position = mainpanelpos;
        end
        if any(obj.sidepanel.Position ~= sidepanelpos)
            obj.sidepanel.Position = sidepanelpos;
        end
        if any(obj.toolpanel.Position ~= toolpanelpos)
            obj.toolpanel.Position = toolpanelpos;
        end
    end
end

function showSidepanel(obj)
    if isgraphics(obj.sidepanel)
        if not(strcmp(obj.sidepanel.Visible,'on'))
            obj.sidepanel.Visible = 'on';
            obj.layoutPanels;
        end
    end
end

function hideSidepanel(obj)
    if isgraphics(obj.sidepanel)
        if not(strcmp(obj.sidepanel.Visible,'off'))
            obj.sidepanel.Visible = 'off';
            obj.layoutPanels;
        end
    end
end

function updateMainaxes(obj)
    if not(isempty(obj.axes)) && isgraphics(obj.axes)
    
        if isempty(obj.polygons)
            bbmin = [-1;-1;-1];
            bbmax = [1;1;1];
        else
            fpadding = [0;0;0.05];

            [bbmin,bbmax] = obj.polygons.boundingbox;
            if size(bbmin,1) == 2
                bbmin = [bbmin;zeros(1,size(bbmin,2))];
                bbmax = [bbmax;zeros(1,size(bbmax,2))];
            end
            bbmin = min(bbmin,[],2);
            bbmax = max(bbmax,[],2);
            bbdiag = sqrt(sum((bbmax-bbmin).^2,1));
            bbmax = bbmax + bbdiag .* fpadding;
            bbmin = bbmin - bbdiag .* fpadding;
        end
        
        set(obj.axes,'XLim',[bbmin(1),bbmax(1)]);
        set(obj.axes,'YLim',[bbmin(2),bbmax(2)]);
        set(obj.axes,'ZLim',[bbmin(3),bbmax(3)]);
    end
end

function showAxescontents(obj)
    if obj.polygonsVisible
        obj.showPolygons
    end
end

function hideAxescontents(obj)
    obj.hidePolygons;
end

function showPolygons(obj)
    if not(isempty(obj.polygons))
        % get annotation polygons and convert from [0,1] to [imgmin,imgmax]        
        
        edgecolor = repmat([0;0;1],1,numel(obj.polygons));
        edgecolor(:,obj.selpolyinds) = repmat([1;0;0],1,numel(obj.selpolyinds));
        
        obj.gpolygons = showFaces(...
            {obj.polygons.verts},obj.gpolygons,obj.axes,...
            'facecolor','none',...
            'edgecolor',edgecolor,...
            'edgewidth',1,...
            'stackz',(1:numel(obj.polygons)));
    else
        obj.hidePolygons;
    end
end

function hidePolygons(obj)
    delete(obj.gpolygons(isgraphics(obj.gpolygons)));
    obj.gpolygons = gobjects(1,0);
end

function show(obj)
    obj.show@AxesWidget;

    if isvalid(obj.uigroup)
        obj.uigroup.show;
    end
    
    if not(isempty(obj.navwidget)) &&  isvalid(obj.navwidget)
        obj.navwidget.show;
    end
    
    obj.layoutPanels;
    
    if isvalid(obj.selpolyindsListener)
        obj.selpolyindsListener.Enabled = true;
    end
    if isvalid(obj.toolListener)
        obj.toolListener.Enabled = true;
    end
    
    obj.updateMainaxes;
    obj.showAxescontents;
    if obj.sidepanelVisible
        obj.showSidepanel;
    end
end

function hide(obj)
    obj.hide@AxesWidget;
    
    if isvalid(obj.uigroup)
        obj.uigroup.hide;
    end

    if not(isempty(obj.navwidget)) &&  isvalid(obj.navwidget)
        obj.navwidget.hide;
    end
    
    if isvalid(obj.selpolyindsListener)
        obj.selpolyindsListener.Enabled = false;
    end
    if isvalid(obj.toolListener)
        obj.toolListener.Enabled = false;
    end
    
    obj.hideAxescontents;
    obj.hideSidepanel;
end

end

methods(Static)
    
function [polyind,minedgedist] = closestPolygon(pos,polygons)
    polyind = [];
    minedgedist = inf;
        
    for i=1:numel(polygons)
        [edgedist,~] = pointPolylineDistance_mex(...
            pos(1),pos(2),...
            polygons{i}(1,[1:end,1]),...
            polygons{i}(2,[1:end,1]));
        if edgedist < minedgedist
            minedgedist = edgedist;
            polyind = i;
        end
    end
end

end

end