classdef SceneArrayCopyWidget < SceneCreateRectWidget
    
properties(SetAccess=protected)
    element = ScenePolygon.empty;
    
    spacingx = 0.1;
    spacingy = 0.3;
end
    
properties(Access=protected)
    array = ScenePolygon.empty(1,0);
    
    garray = gobjects(1,0);
end

methods

% obj = SceneArrayCopyWidget()
function obj = SceneArrayCopyWidget(varargin)
    obj = obj@SceneCreateRectWidget(varargin{:});
end

function delete(obj)
    obj.hide;
    
    delete(obj.array(isvalid(obj.array)));
end

function setRect(obj,c,w,h)
    obj.setRect@SceneCreateRectWidget(c,w,h);
    
    obj.updateArray;
end

function setElement(obj,elm)
    if numel(elm) > 1 || not(isa(elm,'SceneElement'))
        error('Invalid type of element for array copy.');
    end
    if numel(elm) ~= numel(obj.element) || any(elm ~= obj.element)
        obj.element = elm;
        delete(obj.array(isvalid(obj.array)));
        obj.array = ScenePolygon.empty(1,0);
        obj.updateArray;
    end
end

function setArray(obj,spacingx,spacingy)
    obj.spacingx = spacingx;
    obj.spacingy = spacingy;
    
    obj.updateArray;
end

function attach(obj,elm,center,width,height,spacingx,spacingy,grouppose,groupgnode)
    obj.attach@SceneCreateRectWidget(center,width,height,grouppose,groupgnode);
    
    obj.setElement(elm);
    obj.setArray(spacingx,spacingy);
end

function detach(obj)
    obj.detach@SceneCreateRectWidget;
    
    obj.setElement(ScenePolygon.empty);
    obj.setArray(0.1,0.3);
end

function updateArray(obj)
    if isempty(obj.element) || isempty(obj.center)
        delete(obj.array(isvalid(obj.array)));
        obj.array = ScenePolygon.empty(1,0);
    else
        [elmbbmin,elmbbmax] = obj.element.boundingbox;
        elmbbsize = elmbbmax - elmbbmin;
        
        [xpos,ypos] = rectangleArray(...
            elmbbsize,...
            obj.center,obj.width,obj.height,...
            'spacing',obj.spacingx,obj.spacingy);
        
        % remove superfluous array elements
        delinds = numel(xpos) + 1 : numel(obj.array);
        delete(obj.array(delinds(isvalid(obj.array(delinds)))));
        obj.array(delinds) = [];
        
        % add missing array elements
        for i=numel(obj.array)+1:numel(xpos)
            obj.array(i) = obj.element.clone;
            obj.array(i).setScenegroup(SceneGroup.empty);
        end
        
        % move elements to correct positions
        for i=1:numel(obj.array)
            obj.array(i).move([xpos(i);ypos(i)]-obj.array(i).position(1:2),'relative');
        end
    end
end

function mouseMoved(obj,src,evt)
    obj.mouseMoved@SceneCreateRectWidget(src,evt);
    
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;
    
    if obj.mousedown && not(isempty(obj.fixedcornerpos))
        obj.showArray;
    end

    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt) %#ok<INUSD>
end

function keyReleased(obj,src,evt) %#ok<INUSD>
end

function showArray(obj)
    if not(isempty(obj.array)) && not(isempty(obj.element)) && not(isempty(obj.axes))
        
        if numel(obj.garray) ~= numel(obj.array) || any(not(isgraphics(obj.garray)))
            obj.garray(not(isgraphics(obj.garray))) = [];
            
            delinds = numel(obj.array) + 1 : numel(obj.garray);
            delete(obj.garray(delinds(isgraphics(obj.garray(delinds)))));
            
            obj.garray(numel(obj.garray) + 1 : numel(obj.array)) = gobjects;
        end
        
        if any(strcmp(obj.element.type,{'mesh','surface'}))
            obj.garray = showSceneFeatures(...
                obj.array,obj.garray,obj.axes,...
                'facecolor',rgbhex2rgbdec('bebada')',...
                'edgecolor','none');
        else
            obj.garray = showSceneFeatures(...
                obj.array,obj.garray,obj.axes,...
                'facecolor','none',...
                'edgecolor',rgbhex2rgbdec('bebada')',...
                'edgewidth',2);
        end
    else
        obj.hideArray;
    end
end

function hideArray(obj)
    delete(obj.garray(isvalid(obj.garray)));
    obj.garray = gobjects(1,0);
end

function show(obj)
    obj.show@SceneCreateRectWidget;
    
    obj.showArray;
end

function hide(obj)
    obj.hide@SceneCreateRectWidget;
    
    obj.hideArray;
end

end

end
