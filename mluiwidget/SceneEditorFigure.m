% editor figure to be used with the mlscene module
classdef SceneEditorFigure < EditorFigure
    
properties
    % widgets
    navwidget = NavigationWidget.empty;
    
    sceneeditor = SceneEditor.empty;
end

methods
    
function obj = SceneEditorFigure
    
    obj@EditorFigure;
    
    menus = figureMenus(obj.fig);
    
    % create widgets and editors
    obj.navwidget = NavigationWidget(obj.mainaxes,...
        obj.navpanel);
        
    obj.sceneeditor = SceneEditor(obj.mainaxes,...
        obj.navwidget,obj.fig,obj.toolpanel,obj.mainpanel,menus);
    
    obj.addEditor(obj.sceneeditor);
    
    obj.seleditor = obj.sceneeditor;
    resetCamera(obj.mainaxes);
end

function delete(obj)
    % delete editors and widgets
    if not(isempty(obj.sceneeditor))
        delete(obj.sceneeditor(isvalid(obj.sceneeditor)));
    end
    if not(isempty(obj.navwidget))
        delete(obj.navwidget(isvalid(obj.navwidget)));
    end
end

end

end
