function [pos,localpos,tl,itype] = rayGroupplaneIntersection(globalgrouppose,rayp1,rayp2)
    ray = [rayp1,rayp1 + (rayp2 - rayp1) .* 100];
    
    planenormal = posetransformNormal([0;0;1],globalgrouppose);
    planeorigin = posetransform([0;0;0],globalgrouppose);
    [pos,tl,itype] = linePlaneIntersection(...
        planenormal,planeorigin,ray(:,1),ray(:,2));
    
    localpos = posetransform(pos,identpose3D,globalgrouppose); 
    localpos = localpos(1:2,:);
end
