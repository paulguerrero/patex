classdef ArrayCopyTool < Tool
    
properties(SetAccess=protected)
    editor = Open3DEditor.empty;
    
    group = SceneGroup.empty
end

properties(Access=protected)
    arraycopywidget = SceneArrayCopyWidget.empty;
end

methods(Static)

function n = name_s
    n = 'ArrayCopy';
end

end

methods
    
% obj = ArrayCopyTool()
% obj = ArrayCopyTool(axes, editor)
function obj = ArrayCopyTool(varargin)
    
    superargs = cell(1,0);
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 2 && isgraphics(varargin{1})
        superargs = varargin(1);
        varargin(1) = [];
    else
        error('Invalid arguments.');
    end
    
    obj = obj@Tool(superargs{:});
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'Open3DEditor')
        obj.editor = varargin{1};
    else
        error('Invalid arguments.');
    end
    
    obj.arraycopywidget = SceneArrayCopyWidget;
end

function delete(obj)
    obj.hide;
    
    delete(obj.arraycopywidget(isvalid(obj.arraycopywidget)));
end

function mousePressed(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        obj.clickedpos = obj.mousepos;
    end
    
    if not(isempty(obj.editor)) && not(isempty(obj.editor.scene)) 

        if strcmp(src.SelectionType,'alt')
            
            obj.editor.selectClosestFeature('element','normal');
        
        elseif strcmp(src.SelectionType,'normal') && not(isempty(obj.editor.selfeatinds))
        
            [obj.group,~,pos] = SceneEditor.closestGroup(...
                obj.editor.scene.groups,obj.mousepos(:,1),obj.mousepos(:,2));

            elm = obj.editor.scene.features(obj.editor.selfeatinds);

            obj.arraycopywidget.attach(...
                elm,...
                pos(1:2),0,0,0.1,0.3,...
                obj.group.globaloriginpose,obj.editor.getGraphics(obj.group));

            obj.arraycopywidget.mousePressed(src,evt);
            obj.arraycopywidget.show;
        end
    end

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
    end
    
    if obj.arraycopywidget.isattached
        obj.arraycopywidget.mouseMoved(src,evt);
    end
    
    obj.blockcallbacks = false;
end

function mouseReleased(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;
    
    if not(isempty(obj.editor)) && not(isempty(obj.editor.scene)) && obj.arraycopywidget.isattached
        verts = [...
            obj.arraycopywidget.center + [-obj.arraycopywidget.width;-obj.arraycopywidget.height],....
            obj.arraycopywidget.center + [-obj.arraycopywidget.width; obj.arraycopywidget.height],....
            obj.arraycopywidget.center + [ obj.arraycopywidget.width; obj.arraycopywidget.height],....
            obj.arraycopywidget.center + [ obj.arraycopywidget.width;-obj.arraycopywidget.height]];
        newrect = ScenePolygon(verts);
        obj.group.addElements(newrect);
        
        elm = obj.editor.scene.features(obj.editor.selfeatinds);
        
        [elmbbmin,elmbbmax] = obj.element.boundingbox;
        elmbbsize = elmbbmax - elmbbmin;
        
        [xpos,ypos] = rectangleArray(...
            elmbbsize,...
            obj.center,obj.width,obj.height,...
            'spacing',obj.spacingx,obj.spacingy);
        
        arrayelms = ScenePolygon.empty(1,0);
        for i=1:numel(xpos)
            arrayelms(i) = elm.clone;
            arrayelms(i).setScenegroup(SceneGroup.empty);
            arrayelms(i).move([xpos(i);ypos(i)]-arrayelms(i).position(1:2),'relative');
        end
        
        obj.group.addElements(arrayelms);
        
        obj.arraycopywidget.hide;
        obj.arraycopywidget.mouseReleased(src,evt);
        obj.arraycopywidget.detach;

        obj.editor.showAllSceneFeatures(newrect);
    end

    obj.blockcallbacks = false;
end


function keyPressed(obj,src,evt) %#ok<INUSL>
end

function keyReleased(obj,src,evt) %#ok<INUSL>
end

function show(obj)
    obj.show@Tool;
    
    if isvalid(obj.arraycopywidget)
        obj.arraycopywidget.show();
    end
end

function hide(obj)
    obj.hide@Tool;
    
    if isvalid(obj.arraycopywidget)
        obj.arraycopywidget.hide();
    end
end
    
end

end
