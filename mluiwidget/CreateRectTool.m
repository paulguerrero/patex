classdef CreateRectTool < Tool
    
properties(SetAccess=protected)
    editor = Open3DEditor.empty;
    
    group = SceneGroup.empty
end

properties(Access=protected)
    createrectwidget = SceneCreateRectWidget.empty;
end

methods(Static)

function n = name_s
    n = 'CreateRect';
end

end

methods
    
% obj = CreateRectTool()
% obj = CreateRectTool(axes, editor)
function obj = CreateRectTool(varargin)
    
    superargs = cell(1,0);
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 2 && isgraphics(varargin{1})
        superargs = varargin(1);
        varargin(1) = [];
    else
        error('Invalid arguments.');
    end
    
    obj = obj@Tool(superargs{:});
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'Open3DEditor')
        obj.editor = varargin{1};
    else
        error('Invalid arguments.');
    end
    
    obj.createrectwidget = SceneCreateRectWidget;
end

function delete(obj)
    obj.hide;
    
    delete(obj.createrectwidget(isvalid(obj.createrectwidget)));
end

function mousePressed(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        obj.clickedpos = obj.mousepos;
    end
    
    if not(isempty(obj.editor)) && not(isempty(obj.editor.scene))

        [obj.group,~,pos] = SceneEditor.closestGroup(...
            obj.editor.scene.groups,obj.mousepos(:,1),obj.mousepos(:,2));
        
        obj.createrectwidget.attach(pos(1:2),0,0,obj.group.globaloriginpose,obj.editor.getGraphics(obj.group));
        obj.createrectwidget.mousePressed(src,evt);
        obj.createrectwidget.show;
    end

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
    end
    
    if obj.createrectwidget.isattached
        obj.createrectwidget.mouseMoved(src,evt);
    end
    
    obj.blockcallbacks = false;
end

function mouseReleased(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;
    
    if not(isempty(obj.editor)) && not(isempty(obj.editor.scene)) && obj.createrectwidget.isattached
        verts = [...
            obj.createrectwidget.center + [-obj.createrectwidget.width;-obj.createrectwidget.height],....
            obj.createrectwidget.center + [-obj.createrectwidget.width; obj.createrectwidget.height],....
            obj.createrectwidget.center + [ obj.createrectwidget.width; obj.createrectwidget.height],....
            obj.createrectwidget.center + [ obj.createrectwidget.width;-obj.createrectwidget.height]];
        newrect = ScenePolygon(verts);
        obj.group.addElements(newrect);
        
        obj.createrectwidget.hide;
        obj.createrectwidget.mouseReleased(src,evt);
        obj.createrectwidget.detach;

        obj.editor.showAllSceneFeatures(newrect);
    end

    obj.blockcallbacks = false;
end


function keyPressed(obj,src,evt) %#ok<INUSL>
end

function keyReleased(obj,src,evt) %#ok<INUSL>
end

function show(obj)
    obj.show@Tool;
    
    if isvalid(obj.createrectwidget)
        obj.createrectwidget.show();
    end
end

function hide(obj)
    obj.hide@Tool;
    
    if isvalid(obj.createrectwidget)
        obj.createrectwidget.hide();
    end
end
    
end

end
