% widget: uses only the minimal information necessary to get a parameter
% (no editor or something similar needed), used by tool
classdef SceneCreateRectWidget < AxesWidget
    
properties(SetAccess=protected)
    center = zeros(2,0);
    width = 0;
    height = 0;
    
    grouppose = identpose3D; % global origin pose of group containing the basepoly (so that basepolys without group can be handled as well)
    
    isattached = false;
end
    
properties(Access=protected)
    fixedcornerpos = [];
    
    rect = ScenePolygon.empty;
    
    grect = gobjects(1,0);
end

methods

% obj = SceneCreateRectWidget()
function obj = SceneCreateRectWidget()
    obj = obj@AxesWidget(gobjects(1,0));
    
    obj.rect = ScenePolygon;
    obj.updateRect;
end

function delete(obj)
    obj.hide;
    
    delete(obj.rect(isvalid(obj.rect)));
end
    
function setRect(obj,c,w,h)
    obj.center = c;
    obj.width = w;
    obj.height = h;
    obj.updateRect;
end

function setGroup(obj,pose,gnode)
    obj.grouppose = pose;
    
    obj.axes = gnode;
end

function attach(obj,center,width,height,grouppose,groupgnode)
    obj.setRect(center,width,height);
    obj.setGroup(grouppose,groupgnode);
    
    obj.isattached = true;
end

function detach(obj)
    obj.setRect(zeros(2,0),0,0);
    obj.setGroup(identpose3D,gobjects(1,0));
    
    obj.isattached = false;
end

function updateRect(obj)
    if isempty(obj.center)
        verts = zeros(2,0);
    else
        verts = [...
            obj.center + [-obj.width;-obj.height].*0.5,....
            obj.center + [-obj.width; obj.height].*0.5,....
            obj.center + [ obj.width; obj.height].*0.5,....
            obj.center + [ obj.width;-obj.height].*0.5];
    end

    obj.rect.setVerts(verts);
end

function mousePressed(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        obj.clickedpos = obj.mousepos;
    end
    
    if obj.isattached
        [~,clickedplanepos,tl,itype] = rayGroupplaneIntersection(identpose3D,obj.clickedpos(:,1),obj.clickedpos(:,2));
        
        if tl > 0 && itype == 1
            if isempty(obj.center)
                obj.fixedcornerpos = clickedplanepos;
                disp('1');
            else
                corners = [...
                    obj.center + [-obj.width;-obj.height].*0.5,....
                    obj.center + [-obj.width; obj.height].*0.5,....
                    obj.center + [ obj.width; obj.height].*0.5,....
                    obj.center + [ obj.width;-obj.height].*0.5];
                [~,cornerind] = max(sum(bsxfun(@minus,clickedplanepos,corners).^2,1));
                obj.fixedcornerpos = corners(:,cornerind);
                disp('2');
            end
        end
        
        disp(['grouppose: ',num2str(obj.grouppose')]);
        disp(['clicked: ',num2str(clickedplanepos')]);
        disp(['fixedcornerpos: ',num2str(obj.fixedcornerpos')]);
    end
    
    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
    end
    
    if obj.mousedown && not(isempty(obj.fixedcornerpos))
        
        [~,mouseplanepos,tl,itype] = rayGroupplaneIntersection(identpose3D,obj.mousepos(:,1),obj.mousepos(:,2));
        
        disp(['moved: ',num2str(mouseplanepos')]);
        disp(['fixedcornerpos: ',num2str(obj.fixedcornerpos')]);
        
        if tl > 0 && itype == 1
            % standard intersection
            obj.setRect(...
                (mouseplanepos + obj.fixedcornerpos) .* 0.5,...
                max(0.0001,abs(mouseplanepos(1)-obj.fixedcornerpos(1))),...
                max(0.0001,abs(mouseplanepos(2)-obj.fixedcornerpos(2))));
        end

        obj.showRect;
    end

    obj.blockcallbacks = false;
end

function mouseReleased(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;

    obj.fixedcornerpos = [];

    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt) %#ok<INUSD>
end

function keyReleased(obj,src,evt) %#ok<INUSD>
end

function showRect(obj)
    if not(isempty(obj.rect)) && not(isempty(obj.axes))
        if numel(obj.grect) ~= numel(obj.rect) || any(not(isgraphics(obj.grect)))
            delete(obj.grect(isgraphics(obj.grect)));
            obj.grect = gobjects(1,0);
        end

        obj.grect = showScenePolygons(...
            obj.rect,obj.grect,obj.axes,...
            'facecolor','none',...
            'edgecolor',rgbhex2rgbdec('bebada')');
    else
        obj.hideRect;
    end
end

function hideRect(obj)
    delete(obj.grect(isgraphics(obj.grect)));
    obj.grect = gobjects(1,0);
end

function show(obj)
    obj.show@AxesWidget;
    
    obj.showRect;
end

function hide(obj)
    obj.hide@AxesWidget;
    
    obj.hideRect;
end

end

end
