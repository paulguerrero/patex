classdef SceneCreatePolygonWidget < AxesWidget
    
properties(SetAccess=protected)
    verts = zeros(2,0);
    
    grouppose = identpose3D;
    
    snapping = false;
    
    isattached = false;
end

properties(Access=protected)
    polygroup = SceneGroup.empty
    poly = ScenePolygon.empty;
    
%     clickedgrouppos = zeros(2,0);
    
    gpoly = gobjects(1,0);
    
    selvertind = zeros(1,0);
end

methods
    
% obj = SceneCreatePolygonWidget()
function obj = SceneCreatePolygonWidget()
    obj = obj@AxesWidget(gobjects(1,0));
    
    obj.poly = ScenePolygon;
end

function delete(obj)
    obj.hide;
    
    delete(obj.poly(isvalid(obj.poly)));
end

function attach(obj,v,grouppose,gnode)
    obj.setVerts(v);
    obj.setGroup(grouppose,gnode);
    
    obj.isattached = true;
end

function detach(obj)
    obj.setVerts(zeros(2,0));
    obj.setGroup(identpose3D,gobjects(1,0));
    
    obj.isattached = false;
end

function setGroup(obj,pose,gnode)
    obj.grouppose = pose;
    
    obj.axes = gnode;
end

function setVerts(obj,v)
    obj.verts = v;
    obj.updatePoly;
end

function updatePoly(obj)
    obj.poly.setVerts(obj.verts);
end

function mousePressed(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        obj.clickedpos = obj.mousepos;
    end
    
    if obj.isattached
        if strcmp(src.SelectionType,'normal')
            % add new point

            [~,pos,tl,itype] = rayGroupplaneIntersection(identpose3D,obj.clickedpos(:,1),obj.clickedpos(:,2));
            
            if not(isempty(obj.verts)) && sqrt(sum((pos-obj.verts(:,end)).^2,1)) > eps('double')*1000
                if tl > 0 && itype == 1
%                     obj.clickedgrouppos = pos;
                    
                    if size(obj.verts,2) >= 3
                        dists = pointLinesegDistance2D_mex(pos,obj.verts,obj.verts(:,[2:end,1]));
                        [~,ind] = min(dists);
                        v = obj.verts;
                        v = [v(:,1:ind),pos,v(:,ind+1:end)];
                        obj.setVerts(v);
                        obj.selvertind = ind+1;
                    else
                        obj.setVerts([obj.verts,pos(1:2)]);
                        obj.selvertind = size(obj.verts,2);
                    end
                end
            end

            obj.showPoly;

        elseif strcmp(src.SelectionType,'alt')
            % remove last point

            [~,pos,tl,itype] = rayGroupplaneIntersection(identpose3D,obj.clickedpos(:,1),obj.clickedpos(:,2));
            if tl > 0 && itype == 1
%                 obj.clickedgrouppos = pos;
                
                if not(isempty(obj.verts))
                    obj.selvertind = zeros(1,0);
                    
                    sqdists = sum(bsxfun(@minus,pos,obj.verts).^2,1);
                    [~,ind] = min(sqdists);
                    v = obj.verts;
                    v(:,ind) = [];
                    obj.setVerts(v);
                end
            end

            obj.showPoly;
        end
    end

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
    end
    
    if obj.mousedown && obj.isattached && strcmp(src.SelectionType,'normal') && not(isempty(obj.selvertind))
        [~,pos,tl,itype] = rayGroupplaneIntersection(identpose3D,obj.mousepos(:,1),obj.mousepos(:,2));
        if tl > 0 && itype == 1
        
            if obj.snapping && obj.selvertind >= 2
                v = obj.verts;
                
                snapdist = pi/4;
                dragvec = pos - v(:,obj.selvertind-1);
                [dragdir,draglen] = cart2pol(dragvec(1),dragvec(2));
                dragdir = smod(round(dragdir/snapdist)*snapdist,-pi,pi);
                [dragvec(1),dragvec(2)] = pol2cart(dragdir,draglen);
                
                v(:,obj.selvertind) = v(:,obj.selvertind-1) + dragvec;
                obj.setVerts(v);
            else
                v = obj.verts;
                v(:,obj.selvertind) = pos;
                obj.setVerts(v);
            end
        end
        
        obj.showPoly;
    end

    obj.blockcallbacks = false;
end

function mouseReleased(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;
    
    obj.selvertind = zeros(1,0);

    % do nothing

    obj.blockcallbacks = false;
end


function keyPressed(obj,src,evt) %#ok<INUSL>
    if strcmp(evt.Key,'x')
        obj.snapping = true;
    end
end

function keyReleased(obj,src,evt) %#ok<INUSL>
    if strcmp(evt.Key,'x')
        obj.snapping = false;
    end
end

function showPoly(obj)
    if not(isempty(obj.poly)) && not(isempty(obj.axes))
        if numel(obj.gpoly) ~= numel(obj.poly) || any(not(isgraphics(obj.gpoly)))
            delete(obj.gpoly(isgraphics(obj.gpoly)));
            obj.gpoly = gobjects(1,0);
        end
        
        obj.gpoly = showScenePolygons(...
            obj.poly,obj.gpoly,obj.axes,...
            'edgecolor',rgbhex2rgbdec('bebada')');
    else
        obj.hidePoly;
    end
end

function hidePoly(obj)
    delete(obj.gpoly(isgraphics(obj.gpoly)));
    obj.gpoly = gobjects(1,0);
end

function show(obj)
    obj.show@AxesWidget;
    
    obj.showPoly;
end

function hide(obj)
    obj.hide@AxesWidget;
    
    obj.hidePoly;
end
    
end

end
