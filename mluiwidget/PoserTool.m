% tool: can be called just with axes and editor, mousePressed should then
% work as expected
classdef PoserTool < Tool
    
properties(SetAccess=protected)
    editor = SceneEditor.empty;
    
    copymode = 'move'; % 'move', 'copy'
end

properties(Access=protected)
    selfeatindsListener = event.listener.empty;

    poserwidget = ScenePoserWidget.empty;
    
    mousepos_screen = [];
    clickedpos_screen = [];
    
    intentionalchange = false;
end

methods(Static)

function n = name_s
    n = 'Poser';
end

end

methods
    
% obj = PoserTool()
% obj = PoserTool(axes, editor)
function obj = PoserTool(varargin)
    
    superargs = cell(1,0);
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 2 && isgraphics(varargin{1})
        superargs = varargin(1);
        varargin(1) = [];
    else
        error('Invalid arguments.');
    end
    
    obj = obj@Tool(superargs{:});
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneEditor')
        obj.editor = varargin{1};
    else
        error('Invalid arguments.');
    end
    
    obj.poserwidget = ScenePoserWidget;
    
    obj.selfeatindsListener = obj.editor.addlistener('selfeatinds','PostSet',@(src,evt) obj.selfeatindsChanged);
end

function delete(obj)
    obj.hide;

    delete(obj.selfeatindsListener(isvalid(obj.selfeatindsListener)));
end

function selfeatindsChanged(obj)
    obj.showPosechangewidget;
end

function showPosechangewidget(obj)
    if not(isempty(obj.editor)) && not(isempty(obj.editor.scene)) && numel(obj.editor.selfeatinds) ==  1 && obj.editor.scene.features(obj.editor.selfeatinds).isElement
        selfeat = obj.editor.scene.features(obj.editor.selfeatinds);
        obj.poserwidget.attach(selfeat,identpose3D,selfeat.scenegroup.globaloriginpose,obj.editor.getGraphics(selfeat.scenegroup))
        obj.poserwidget.show;
    else    
        obj.hidePosechangewidget;
    end
end

function hidePosechangewidget(obj)
    obj.poserwidget.detach;
    obj.poserwidget.hide;
end

function setCopymode(obj,m)
    obj.copymode = m;
end

function mousePressed(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    obj.mousepos_screen = mouseScreenpos;
    obj.clickedpos_screen = obj.mousepos_screen;
    
    if not(isempty(obj.editor)) && not(isempty(obj.editor.scene))
        obj.editor.selectClosestFeature('element','normal');
    end

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
    end
    
    obj.mousepos_screen = mouseScreenpos;
    
    if not(isempty(obj.editor)) && not(isempty(obj.editor.scene)) && obj.mousedown % && norm(obj.mousepos_screen - obj.clickedpos_screen) > 5
    
        if obj.intentionalchange
            obj.poserwidget.mouseMoved(src,evt);
        else
            if norm(obj.mousepos_screen - obj.clickedpos_screen) > 5 && ...
               numel(obj.editor.selfeatinds) ==  1 && obj.editor.scene.features(obj.editor.selfeatinds).isElement
       
                obj.poserwidget.mousePressed(src,evt);
                obj.intentionalchange = true;
            end
        end
    end

    obj.blockcallbacks = false;
end

function mouseReleased(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;

    if not(isempty(obj.editor)) && not(isempty(obj.editor.scene))
        
        obj.poserwidget.mouseReleased(src,evt);
        
        elm = obj.editor.scene.features(obj.editor.selfeatinds);
        
        if strcmp(obj.copymode,'move') || strcmp(obj.copymode,'copy')
            if strcmp(obj.copymode,'copy')
                origelm = elm;
                elm = origelm.clone;
                elm.setScenegroup(SceneGroup.empty());
                elm.setScenegroup(origelm.scenegroup);
            end
            elm.transform(obj.poserwidget.posechange,'relative');
        else
            error('Unrecognized change pose mode.');
        end
        
        obj.editor.selfeatinds = find(elm == obj.editor.scene.features,1,'first');
        
        if numel(elm) ~= numel(obj.editor.selfeatinds) || elm ~= obj.editor.scene.features(obj.editor.selfeatinds)
            obj.editor.selfeatinds = find(elm == obj.editor.scene.features,1,'first');
        else
            % reset posechange to zero (since element moved to changed pose)
            obj.poserwidget.setPosechange(identpose3D);
        end
        
        obj.intentionalchange = false;
        obj.editor.showAllSceneFeatures([elm,elm.subfeatures{:}]);
    end

    obj.blockcallbacks = false;
end


function keyPressed(obj,src,evt)
    if strcmp(evt.Key,'escape')
        obj.poserwidget.mouseReleased(src,evt);
        
        obj.mousedown = false;
    elseif strcmp(evt.Key,'delete')
        if not(isempty(obj.editor.selfeatinds))
            selms = obj.editor.scene.features(obj.editor.selfeatinds);
            obj.editor.selfeatinds = zeros(1,0);
            allfeatures = [selms,selms.subfeatures{:}];
            obj.editor.hideAllSceneFeatures(allfeatures);

            Scene.removeElements_s(selms);
        end
    end
end

function keyReleased(obj,src,evt) %#ok<INUSL>
end

function show(obj)
    obj.show@Tool;
    
    obj.showPosechangewidget;
    
    if isvalid(obj.selfeatindsListener)
        obj.selfeatindsListener.Enabled = true;
    end
end

function hide(obj)
    obj.hide@Tool;
    
    if isvalid(obj.selfeatindsListener)
        obj.selfeatindsListener.Enabled = false;
    end
    
    obj.hidePosechangewidget;
end
    
end

end
