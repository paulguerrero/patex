% widget: uses only the minimal information necessary to get a parameter
% (no editor or something similar needed), used by tool
classdef SceneExtrudeWidget < AxesWidget
    
properties(SetAccess=protected)
    extrudevec = [0;0;0.0001];
    
    grouppose = identpose3D; % global origin pose of group containing the basepoly (so that basepolys without group can be handled as well)
    basepoly = ScenePolygon.empty;
end
    
properties(Access=protected)
    translatewidget = TranslateWidget.empty;
    
    clickedextrudevec = zeros(3,0);
    
    extrudemesh = SceneMesh.empty;
    
    gextrudemesh = gobjects(1,0);
end

methods

% obj = SceneExtrudeWidget()
function obj = SceneExtrudeWidget()
    obj = obj@AxesWidget(gobjects(1,0));
    
    obj.translatewidget = TranslateWidget;
    obj.extrudemesh = SceneMesh;
end

function delete(obj)
    obj.hide;
    
    delete(obj.translatewidget(isvalid(obj.translatewidget)));
    delete(obj.extrudemesh(isvalid(obj.extrudemesh)));
end
    
function setBasepoly(obj,poly)
    if not(isempty(poly)) && not(any(strcmp(poly.type,{'polygon','complexpolygon'})))
        error('Invalid type of element for extrusion.');
    end
    obj.basepoly = poly;
    obj.updateExtrudemesh;
end

function setExtrudevec(obj,vec)
    obj.extrudevec = vec;
    obj.updateExtrudemesh;
end

function setGroup(obj,pose,gnode)
    obj.grouppose = pose;
    
    obj.axes = gnode;
end

function attach(obj,basepoly,extrudevec,grouppose,groupgnode)
    obj.setBasepoly(basepoly);
    obj.setExtrudevec(extrudevec);
    obj.setGroup(grouppose,groupgnode);
end

function detach(obj)
    obj.setBasepoly(ScenePolygon.empty());
    obj.setExtrudevec([0;0;0.0001]);
    obj.setGroup(identpose3D,gobjects(1,0));
end

function updateExtrudemesh(obj)
    if isempty(obj.basepoly)
        verts = zeros(3,0);
        faces = zeros(3,0);
    else
        [verts,faces] = polylineExtrude(...
            obj.basepoly.verts,obj.extrudevec);
    end
    obj.extrudemesh.setVertsAndFaces(verts,faces);
end

function mousePressed(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        obj.clickedpos = obj.mousepos;
    end
    
    
    obj.clickedextrudevec = obj.extrudevec;
    
    if not(isempty(obj.basepoly))
        % instead of lifting the base poly by the extrude amount, lower
        % the ray by the extrude amount
        lsegstart = obj.mousepos(:,1) - obj.clickedextrudevec;
        lsegend = obj.mousepos(:,2)+(obj.mousepos(:,2)-obj.mousepos(:,1)).*1000 - obj.clickedextrudevec;
        [~,~,featclosestpoint] = obj.basepoly.linesegDistance(...
            lsegstart,lsegend);
        featclosestpoint = [...
            featclosestpoint(:,:,1)';...
            featclosestpoint(:,:,2)';...
            featclosestpoint(:,:,3)'];

        widgetpose = identpose3D;
%         widgetpose(1:3) = posetransform(featclosestpoint,identpose3D,obj.grouppose);
        widgetpose(1:3) = featclosestpoint;
        widgetpose(8:10) = max(obj.basepoly.pose3D(8:10));
        widgetpose(11) = obj.basepoly.pose3D(11);
        obj.translatewidget.attach(widgetpose,obj.grouppose,obj.axes,'z');
        obj.translatewidget.mousePressed(src,evt);
        obj.translatewidget.show();
    end

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
    end
    
    if obj.mousedown && not(isempty(obj.basepoly))
        obj.translatewidget.mouseMoved(src,evt);
        obj.setExtrudevec(obj.translatewidget.posedelta(1:3)+obj.clickedextrudevec);
        obj.showExtrudemesh;
    end

    obj.blockcallbacks = false;
end

function mouseReleased(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;

    if not(isempty(obj.basepoly))
        obj.translatewidget.mouseReleased(src,evt);
        obj.translatewidget.hide();
        obj.translatewidget.detach();
    end

    obj.blockcallbacks = false;
end


function keyPressed(obj,src,evt) %#ok<INUSD>
end

function keyReleased(obj,src,evt) %#ok<INUSD>
end

function showExtrudemesh(obj)
    if not(isempty(obj.extrudemesh))
        if numel(obj.gextrudemesh) ~= numel(obj.extrudemesh) || any(not(isgraphics(obj.gextrudemesh)))
            delete(obj.gextrudemesh(isgraphics(obj.gextrudemesh)));
            obj.gextrudemesh = gobjects(1,0);
        end
        
        obj.gextrudemesh = showSceneMeshes(...
            obj.extrudemesh,obj.gextrudemesh,obj.axes,...
            'facecolor',rgbhex2rgbdec('bebada')',...
            'edgecolor','none');
    else
        obj.hideExtrudemesh;
    end
end

function hideExtrudemesh(obj)
    delete(obj.gextrudemesh(isgraphics(obj.gextrudemesh)));
    obj.gextrudemesh = gobjects(1,0);
end

function show(obj)
    obj.show@AxesWidget;
    
    obj.showExtrudemesh;
end

function hide(obj)
    obj.hide@AxesWidget;
    
    if isvalid(obj.translatewidget)
        obj.translatewidget.hide;
    end
    
    obj.hideExtrudemesh;
end

end

end
