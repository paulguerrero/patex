classdef ScenePoserWidget < AxesWidget
    
properties(SetAccess=protected)
   element = ScenePolygon.empty;
   
   posechange = identpose3D;
end

properties(Access=protected)
    translatewidget = TranslateWidget.empty;
    rotatewidget = RotateWidget.empty;
    
    changedelm = ScenePolygon.empty; % translated element
    
    grouppose = [];
    
    gchangedelm = gobjects(1,0);
    
    clickedposechange = identpose3D;
end

methods
    
% obj = ScenePoserWidget()
function obj = ScenePoserWidget()
    obj = obj@AxesWidget(gobjects(1,0));
    
    obj.translatewidget = TranslateWidget;
    obj.rotatewidget = RotateWidget;
end

function delete(obj)
    obj.hide;
    
    delete(obj.translatewidget(isvalid(obj.translatewidget)));
    delete(obj.rotatewidget(isvalid(obj.rotatewidget)));
    delete(obj.changedelm(isvalid(obj.changedelm)));
end

function setElement(obj,elm)
    if not(isa(elm,'SceneElement'))
        error('Invalid type of element for pose change.');
    end
    if numel(elm) ~= numel(obj.element) || any(elm ~= obj.element)
        obj.element = elm;
        delete(obj.changedelm(isvalid(obj.changedelm)));
        obj.changedelm = obj.element;
        if not(isempty(obj.element))
            obj.changedelm = obj.element.clone;
        end
        obj.updateChangedelm;
    end
end

function setPosechange(obj,pchange)
    obj.posechange = pchange;
    obj.updateChangedelm;
end

function setGroup(obj,pose,gnode)
    obj.grouppose = pose;
    
    obj.axes = gnode;
end

function updateChangedelm(obj)
    if not(isempty(obj.changedelm))
        newpose = posecombine(obj.element.pose3D,obj.posechange);
        obj.changedelm.transform(newpose,'absolute');
    end
end

function attach(obj,elm,pchange,grouppose,groupgnode)
    obj.setElement(elm);
    obj.setPosechange(pchange);
    obj.setGroup(grouppose,groupgnode);
    
    obj.showTranslatewidget;
    obj.hideRotatewidget;
end

function showTranslatewidget(obj)
    if not(isempty(obj.axes)) && not(isempty(obj.element)) && not(isempty(obj.grouppose))
        startpose = posecombine(obj.element.pose3D,obj.posechange);        
        widgetpose = identpose3D;
        widgetpose(1:3) = startpose(1:3);
        widgetpose(8:10) = max(startpose(8:10));
        widgetpose(11) = 0;
        if isa(obj.element,'SceneElement2D')
            obj.translatewidget.attach(widgetpose,obj.grouppose,obj.axes,'xy');
        else
            obj.translatewidget.attach(widgetpose,obj.grouppose,obj.axes,'xy_z');
        end
        obj.translatewidget.show();
    else
        obj.hideTranslatewidget;
    end
end

function hideTranslatewidget(obj)
    obj.translatewidget.detach;
    obj.translatewidget.hide;
end

function showRotatewidget(obj)
    if not(isempty(obj.axes)) && not(isempty(obj.element)) && not(isempty(obj.grouppose))
        startpose = posecombine(obj.element.pose3D,obj.posechange);
        widgetpose = identpose3D;
        widgetpose(1:3) = startpose(1:3);
        widgetpose(4:7) = startpose(4:7);
        widgetpose(8:10) = max(startpose(8:10));
        widgetpose(11) = 0;
        obj.rotatewidget.attach(widgetpose,obj.grouppose,obj.axes,'z');
        obj.rotatewidget.show();
    else
        obj.hideRotatewidget;
    end
end

function hideRotatewidget(obj)
    obj.rotatewidget.detach;
    obj.rotatewidget.hide;
end

function detach(obj)
    obj.hideTranslatewidget;
    obj.hideRotatewidget;
    
    obj.setElement(ScenePolygon.empty());
    obj.setPosechange(identpose3D);
    obj.setGroup(identpose3D,gobjects(1,0));
end

function mousePressed(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    if not(isempty(obj.element))
        
        obj.clickedposechange = obj.posechange;
        
        if strcmp(src.SelectionType,'normal')

            obj.showTranslatewidget;
            obj.hideRotatewidget;            
            obj.translatewidget.mousePressed(src,evt);
            
        elseif strcmp(src.SelectionType,'extend')
            
            obj.hideTranslatewidget;
            obj.showRotatewidget;
            obj.rotatewidget.mousePressed(src,evt);

        elseif strcmp(src.SelectionType,'alt')
            
            warning('Scaling not yet implemented!');
            
        end
        
    end

    obj.blockcallbacks = false;
end


function mouseMoved(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;
    
    if obj.mousedown && not(isempty(obj.element))
        if obj.translatewidget.isattached
            obj.translatewidget.mouseMoved(src,evt);
        end
        if obj.rotatewidget.isattached
            obj.rotatewidget.mouseMoved(src,evt);
        end
        
        pchange = identpose3D;
        if strcmp(src.SelectionType,'normal')
            pchange(1:3) = obj.translatewidget.posedelta(1:3);
        elseif strcmp(src.SelectionType,'extend')
            pchange(4:7) = obj.rotatewidget.posedelta(4:7);
        else
            warning('Scaling not yet implemented!');
        end
        
        obj.setPosechange(posecombine(obj.clickedposechange,pchange));
        
        obj.showChangedelm;
    end

    obj.blockcallbacks = false;
end

function mouseReleased(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;

    if not(isempty(obj.element))
        if strcmp(src.SelectionType,'normal')
            obj.translatewidget.mouseReleased(src,evt);
        elseif strcmp(src.SelectionType,'extend')
            obj.rotatewidget.mouseReleased(src,evt);
        elseif strcmp(src.SelectionType,'alt')
            warning('Scaling not yet implemented!');
        end
        
        obj.hideRotatewidget;
        obj.showTranslatewidget;
        
%         obj.showRotatewidget;
        
%         obj.translatewidget.hide();
%         obj.rotatewidget.hide();
        
%         obj.translatewidget.detach();
%         obj.rotatewidget.detach();
    end
    
%     obj.hideExtrudemesh;

    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt) %#ok<INUSD>
%     if strcmp(evt.Key,'escape')
%         obj.hideExtrudemesh;
%         delete(obj.extrudemesh(isvalid(obj.extrudemesh)));
%         obj.extrudemesh = SceneMesh.empty;
%         
%         obj.translatewidget.hide();
%         
%         obj.mousedown = false;
%     end
end

function keyReleased(obj,src,evt) %#ok<INUSD>
end

function showChangedelm(obj)
    if not(isempty(obj.changedelm))
        
        if numel(obj.gchangedelm) ~= numel(obj.changedelm)
            delete(obj.gchangedelm(isgraphics(obj.gchangedelm)));
            obj.gchangedelm = gobjects(1,numel(obj.changedelm));
        end
        
        if any(strcmp(obj.changedelm.type,{'mesh','surface'}))
            obj.gchangedelm = showSceneFeatures(...
                obj.changedelm,obj.gchangedelm,obj.axes,...
                'facecolor',rgbhex2rgbdec('bebada')',...
                'edgecolor','none');
        else
            obj.gchangedelm = showSceneFeatures(...
                obj.changedelm,obj.gchangedelm,obj.axes,...
                'facecolor','none',...
                'edgecolor',rgbhex2rgbdec('bebada')',...
                'edgewidth',2);
        end
    else
        obj.hideChangedelm;
    end
end

function hideChangedelm(obj)
    delete(obj.gchangedelm(isgraphics(obj.gchangedelm)));
    obj.gchangedelm = gobjects(1,0);
end

function show(obj)
    obj.show@AxesWidget;
    
    obj.showChangedelm;
    
    obj.showTranslatewidget;
    obj.hideRotatewidget;
end

function hide(obj)
    obj.hide@AxesWidget;

    if isvalid(obj.translatewidget)
        obj.hideTranslatewidget;
    end
    if isvalid(obj.rotatewidget)
        obj.hideRotatewidget;
    end
    
    obj.hideChangedelm;
end
        
end

end
