classdef CreatePolygonTool < Tool
    
properties(SetAccess=protected)
    editor = Open3DEditor.empty;
    
    group = SceneGroup.empty
    
    snapping = false;
end

properties(Access=protected)
    createpolygonwidget = SceneCreatePolygonWidget.empty;
end

methods(Static)

function n = name_s
    n = 'CreatePolygon';
end

end

methods
    
% obj = CreatePolygonTool()
% obj = CreatePolygonTool(axes, editor)
function obj = CreatePolygonTool(varargin)
    
    superargs = cell(1,0);
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 2 && isgraphics(varargin{1})
        superargs = varargin(1);
        varargin(1) = [];
    else
        error('Invalid arguments.');
    end
    
    obj = obj@Tool(superargs{:});
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'Open3DEditor')
        obj.editor = varargin{1};
    else
        error('Invalid arguments.');
    end
    
    obj.createpolygonwidget = SceneCreatePolygonWidget;
end

function delete(obj)
    obj.hide;
    
    delete(obj.createpolygonwidget(isvalid(obj.createpolygonwidget)));
end

function mousePressed(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        obj.clickedpos = obj.mousepos;
    end
    
    if not(isempty(obj.editor)) && not(isempty(obj.editor.scene))

        if not(obj.createpolygonwidget.isattached) && strcmp(src.SelectionType,'normal')
            
            [obj.group,~,pos] = SceneEditor.closestGroup(...
                obj.editor.scene.groups,obj.mousepos(:,1),obj.mousepos(:,2));
            
            obj.createpolygonwidget.attach(pos(1:2),obj.group.globaloriginpose,obj.editor.getGraphics(obj.group));
            obj.createpolygonwidget.mousePressed(src,evt);
            obj.createpolygonwidget.show;

        elseif obj.createpolygonwidget.isattached && strcmp(src.SelectionType,'extend')
            % finish polygon
            
            newpoly = ScenePolygon(obj.createpolygonwidget.verts);
            obj.group.addElements(newpoly);
            
            obj.createpolygonwidget.hide;
            obj.createpolygonwidget.mousePressed(src,evt);
            obj.createpolygonwidget.detach;
            
            obj.editor.showAllSceneFeatures(newpoly);
        else
            obj.createpolygonwidget.mousePressed(src,evt);
        end
    end

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
    end
    
    obj.createpolygonwidget.mouseMoved(src,evt);
    
    obj.blockcallbacks = false;
end

function mouseReleased(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;
    
    obj.createpolygonwidget.mouseReleased(src,evt);

    obj.blockcallbacks = false;
end


function keyPressed(obj,src,evt) %#ok<INUSL>
    if strcmp(evt.Key,'x')
        obj.snapping = true;
    end
end

function keyReleased(obj,src,evt) %#ok<INUSL>
    if strcmp(evt.Key,'x')
        obj.snapping = false;
    end
end

function show(obj)
    obj.show@Tool;
    
    if isvalid(obj.createpolygonwidget)
        obj.createpolygonwidget.show();
    end
end

function hide(obj)
    obj.hide@Tool;
    
    if isvalid(obj.createpolygonwidget)
        obj.createpolygonwidget.hide();
    end
end
    
end

end
