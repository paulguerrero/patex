classdef CreateCircleTool < Tool
    
properties(SetAccess=protected)
    editor = Open3DEditor.empty;
    
    group = SceneGroup.empty
end

properties(Access=protected)
    createcirclewidget = SceneCreateCircleWidget.empty;
end

methods(Static)

function n = name_s
    n = 'CreateCircle';
end

end

methods
    
% obj = CreateCircleTool()
% obj = CreateCircleTool(axes, editor)
function obj = CreateCircleTool(varargin)
    
    superargs = cell(1,0);
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 2 && isgraphics(varargin{1})
        superargs = varargin(1);
        varargin(1) = [];
    else
        error('Invalid arguments.');
    end
    
    obj = obj@Tool(superargs{:});
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'Open3DEditor')
        obj.editor = varargin{1};
    else
        error('Invalid arguments.');
    end
    
    obj.createcirclewidget = SceneCreateCircleWidget;
end

function delete(obj)
    obj.hide;
    
    delete(obj.createcirclewidget(isvalid(obj.createcirclewidget)));
end

function mousePressed(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        obj.clickedpos = obj.mousepos;
    end
    
    if not(isempty(obj.editor)) && not(isempty(obj.editor.scene))

        [obj.group,~,pos] = SceneEditor.closestGroup(...
            obj.editor.scene.groups,obj.mousepos(:,1),obj.mousepos(:,2));
        
        obj.createcirclewidget.attach(pos(1:2),0,obj.group.globaloriginpose,obj.editor.getGraphics(obj.group));
        obj.createcirclewidget.mousePressed(src,evt);
        obj.createcirclewidget.show;
    end

    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
    end
    
    if obj.createcirclewidget.isattached
        obj.createcirclewidget.mouseMoved(src,evt);
    end
    
    obj.blockcallbacks = false;
end

function mouseReleased(obj,src,evt)
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;
    
    if obj.createcirclewidget.isattached
        verts = [];
        [verts(1,:),verts(2,:)] = polygonCircle(...
            obj.createcirclewidget.center(1),...
            obj.createcirclewidget.center(2),...
            obj.createcirclewidget.radius);
        newcircle = ScenePolygon(verts);
        obj.group.addElements(newcircle);
        
        obj.createcirclewidget.hide;
        obj.createcirclewidget.mouseReleased(src,evt);
        obj.createcirclewidget.detach;

        obj.editor.showAllSceneFeatures(newcircle);
    end

    obj.blockcallbacks = false;
end


function keyPressed(obj,src,evt) %#ok<INUSD>
end

function keyReleased(obj,src,evt) %#ok<INUSD>
end

function show(obj)
    obj.show@Tool;
    
    if isvalid(obj.createcirclewidget)
        obj.createcirclewidget.show();
    end
end

function hide(obj)
    obj.hide@Tool;
    
    if isvalid(obj.createcirclewidget)
        obj.createcirclewidget.hide();
    end
end
    
end

end
