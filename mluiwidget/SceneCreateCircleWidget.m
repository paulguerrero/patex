% widget: uses only the minimal information necessary to get a parameter
% (no editor or something similar needed), used by tool
classdef SceneCreateCircleWidget < AxesWidget
    
properties(SetAccess=protected)
    center = zeros(2,0);
    radius = 0;
    
    grouppose = identpose3D; % global origin pose of group containing the basepoly (so that basepolys without group can be handled as well)
    
    isattached = false;
end
    
properties(Access=protected)
    circle = ScenePolygon.empty;
    draggingcenter = false;
    draggingradius = false;
    
    gcircle = gobjects(1,0);
end

methods

% obj = SceneCreateCircleWidget()
function obj = SceneCreateCircleWidget()
    obj = obj@AxesWidget(gobjects(1,0));
    
    obj.circle = ScenePolygon;
    obj.updateCircle;
end

function delete(obj)
    obj.hide;
    
    delete(obj.circle(isvalid(obj.circle)));
end
    
function setCircle(obj,c,r)
    obj.center = c;
    obj.radius = r;
    obj.updateCircle;
end

function setGroup(obj,pose,gnode)
    obj.grouppose = pose;
    
    obj.axes = gnode;
end

function attach(obj,center,radius,grouppose,groupgnode)
    obj.setCircle(center,radius);
    obj.setGroup(grouppose,groupgnode);
    
    obj.isattached = true;
end

function detach(obj)
    obj.setCircle(zeros(2,0),0);
    obj.setGroup(identpose3D,gobjects(1,0));
    
    obj.isattached = false;
end

function updateCircle(obj)
    if isempty(obj.center)
        verts = zeros(2,0);
    else
        verts = [];
        [verts(1,:),verts(2,:)] = polygonCircle(obj.center(1),obj.center(2),obj.radius);
    end

    obj.circle.setVerts(verts);
end

function mousePressed(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
        obj.clickedpos = obj.mousepos;
    end
    
    if obj.isattached
        [~,clickedplanepos,tl,itype] = rayGroupplaneIntersection(identpose3D,obj.clickedpos(:,1),obj.clickedpos(:,2));

        if tl > 0 && itype == 1
            if isempty(obj.center)
                obj.center = clickedplanepos;
                obj.radius = 0;
            else
                centerdist = sqrt(sum((obj.center - clickedplanepos).^2,1));
                if centerdist >= obj.radius * 0.5
                    obj.setCircle(obj.center,centerdist);
                    obj.draggingradius = true;
                else
                    obj.setCircle(clickedplanepos,obj.radius);
                    obj.draggingcenter = true;
                end
            end
            
            obj.showCircle;
        end
    end
    
    obj.blockcallbacks = false;
end

function mouseMoved(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    if not(isempty(obj.axes))
        obj.mousepos = mouseray(obj.axes);
        obj.mousepos(:,2) = obj.mousepos(:,1) + (obj.mousepos(:,2)-obj.mousepos(:,1)).*2; % extend mouse ray to go beyond the camera target
    end
    
    if obj.mousedown && obj.isattached
        
        [~,mouseplanepos,tl,itype] = rayGroupplaneIntersection(identpose3D,obj.mousepos(:,1),obj.mousepos(:,2));
        
        if tl > 0 && itype == 1
            if obj.draggingradius
                centerdist = sqrt(sum((obj.center - mouseplanepos).^2,1));
                obj.setCircle(obj.center,centerdist);
            elseif obj.draggingcenter
                obj.setCircle(mouseplanepos,obj.radius);
            end
        end

        obj.showCircle;
    end

    obj.blockcallbacks = false;
end

function mouseReleased(obj,src,evt) %#ok<INUSD>
    if obj.blockcallbacks
        return;
    end
    obj.blockcallbacks = true;

    obj.mousedown = false;

    obj.draggingcenter = false;
    obj.draggingradius = false;

    obj.blockcallbacks = false;
end

function keyPressed(obj,src,evt) %#ok<INUSD>
end

function keyReleased(obj,src,evt) %#ok<INUSD>
end

function showCircle(obj)
    if not(isempty(obj.circle)) && not(isempty(obj.axes))
        if numel(obj.gcircle) ~= numel(obj.circle) || any(not(isgraphics(obj.gcircle)))
            delete(obj.gcircle(isgraphics(obj.gcircle)));
            obj.gcircle = gobjects(1,0);
        end

        obj.gcircle = showScenePolygons(...
            obj.circle,obj.gcircle,obj.axes,...
            'facecolor','none',...
            'edgecolor',rgbhex2rgbdec('bebada')');
    else
        obj.hideCircle;
    end
end

function hideCircle(obj)
    delete(obj.gcircle(isgraphics(obj.gcircle)));
    obj.gcircle = gobjects(1,0);
end

function show(obj)
    obj.show@AxesWidget;
    
    obj.showCircle;
end

function hide(obj)
    obj.hide@AxesWidget;
    
    obj.hideCircle;
end

end

end
