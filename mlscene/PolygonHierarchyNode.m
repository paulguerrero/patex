classdef PolygonHierarchyNode < handle %& matlab.mixin.Heterogeneous

properties(Dependent,SetAccess=protected)
    parentpolygons;
    childfeatures;
    directparentpolygons;
    directsiblingpolygons; % share one or more direct parents
    directchildfeatures;
end

properties(Dependent,SetAccess=private)
    scenegroup;
end

properties(Access={?PolygonHierarchyNode,?SceneImporter,?SceneExporter})
    p_parentpolygons = [];
    p_childfeatures = [];
    p_directparentpolygons = [];
    p_directsiblingpolygons = [];
    p_directchildfeatures = [];
end

methods

function obj = PolygonHierarchyNode
    
	if nargin == 0
        % do nothing
    else
        error('Invalid number of arguments.');
	end
end

function copyFrom(obj,obj2)
    obj.p_parentpolygons = obj2.p_parentpolygons;
    obj.p_childfeatures = obj2.p_childfeatures;
    obj.p_directparentpolygons = obj2.p_directparentpolygons;
    obj.p_directsiblingpolygons = obj2.p_directsiblingpolygons;
    obj.p_directchildfeatures = obj2.p_directchildfeatures;
end

function obj2 = clone(obj)
    obj2 = PolygonHierarchyNode(obj);
end

function val = get.scenegroup(obj)
    val = obj.getScenegroup;
    val = val{:};
end

% function set.scenegroup(obj,val)
%     obj.setScenegroup(val);
% end
    
function val = get.parentpolygons(obj)
    if not(isa(obj.p_parentpolygons,'ScenePolygon'));
        obj.updateParentpolygons;
    end
    
    val = obj.p_parentpolygons;
end

function val = get.childfeatures(obj)
    if not(isa(obj.p_childfeatures,'SceneFeature'));
        obj.updateChildfeatures;
    end
    
    val = obj.p_childfeatures;
end

function val = get.directparentpolygons(obj)
    if not(isa(obj.p_directparentpolygons,'ScenePolygon'));
        obj.updateDirectparentpolygons;
    end
    
    val = obj.p_directparentpolygons;
end

function val = get.directsiblingpolygons(obj)
    if not(isa(obj.p_directsiblingpolygons,'ScenePolygon'));
        obj.updateDirectsiblingpolygons;
    end
    
    val = obj.p_directsiblingpolygons;
end

function val = get.directchildfeatures(obj)
    if not(isa(obj.p_directchildfeatures,'SceneFeature'));
        obj.updateDirectchildfeatures;
    end
    
    val = obj.p_directchildfeatures;
end

end

methods(Sealed)
    
function clearHierarchy(obj)
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_parentpolygons = [];
        o.p_childfeatures = [];
        o.p_directparentpolygons = [];
        o.p_directsiblingpolygons = [];
        o.p_directchildfeatures = [];
    end
end
    
function updateHierarchy(obj)
    mask = not(obj.hasParentpolygons);
    if any(mask)
        obj(mask).updateParentpolygons;
    end
    mask = not(obj.hasChildfeatures);
    if any(mask)
        obj(mask).updateChildfeatures;
    end
    mask = not(obj.hasDirectparentpolygons);
    if any(mask)
        obj(mask).updateDirectparentpolygons;
    end
    mask = not(obj.hasDirectsiblingpolygons);
    if any(mask)
        obj(mask).updateDirectsiblingpolygons;
    end
    mask = not(obj.hasDirectchildfeatures);
    if any(mask)
        obj(mask).updateDirectchildfeatures;
    end
end
    
function updateParentpolygons(obj)
    nodegroups = obj.getScenegroup;
    nodegroups = [nodegroups{:}];
    
    if isempty(nodegroups)
        for i=1:numel(obj)
            o = obj(i);
            o.p_parentpolygons = [];
        end
        return;
    elseif not(all(nodegroups == nodegroups(1)))
        error('Only hierarchy nodes from the same group are allowed.');
    end
    
    % todo: with multiple objects: look for objects with same scene group
    
    allpolys = nodegroups(1).polygons;
    
    elmfeatmask = arrayfun(@(x) isa(x,'SceneElementFeature'),obj);
    polymask = false(size(elmfeatmask));
    polymask(not(elmfeatmask)) = arrayfun(@(x) isa(x,'ScenePolygon'),obj(not(elmfeatmask)));
    othermask = not(elmfeatmask | polymask);
    
    % temporary: meshes and surfaces are not in the hierarchy
    meshmask = false(size(elmfeatmask));
    meshmask(othermask) = arrayfun(@(x) isa(x,'SceneMesh'),obj(othermask));
    othermask = othermask & not(meshmask);
    surfmask = false(size(elmfeatmask));
    surfmask(elmfeatmask) = arrayfun(@(x) isa(x,'SceneSurface'),obj(elmfeatmask));
    elmfeatmask = elmfeatmask & not(surfmask);
    
%     parentmask = false(numel(obj),numel(allpolys));
    parentinds = cell(numel(obj),1);
    if any(polymask)
        polyinds = find(polymask);
        objpolys = obj(polyinds);
        
        % only polygons with areas larger than (1+eps)*myarea are
        % parents (avoid loops)
        polyareas = [allpolys.area];
        objpolyareas = [objpolys.area]';
        candidateinds = cell(numel(objpolys),1);
        for i=1:numel(objpolys)
            candidateinds{i} = find(allpolys ~= objpolys(i) & polyareas > objpolyareas(i)*1.25);
        end
        
%         % find sets of features of same class and call function once for each set
%         [~,~,id] = unique([objpolys.type]);
%         typegroups = array2cell_mex(1:numel(id),id');
%         for i=1:numel(typegroups)
%             parentinds(polyinds(typegroups{i})) = objpolys(typegroups{i}).inPolygon(allpolys,candidateinds(typegroups{i}));
%         end
        parentinds(polyinds) = objpolys.inPolygon(allpolys,candidateinds);
        
%         parentinds(polyinds) = objpolys.inPolygon(allpolys,candidateinds);
    end
    if any(elmfeatmask)
        elmfeatinds = find(elmfeatmask);
        
%         % find sets of features of same class and call function once for each set
%         [~,~,id] = unique([obj(elmfeatinds).type]);
%         typegroups = array2cell_mex(1:numel(id),id');
%         for i=1:numel(typegroups)
%             parentinds(elmfeatinds(typegroups{i})) = obj(elmfeatinds(typegroups{i})).inPolygon(allpolys);
%         end
        parentinds(elmfeatinds) = obj(elmfeatinds).inPolygon(allpolys);
        
        % the parent element is always a parent polygon (if it is a
        % polygon), add parent element to the parentinds
        mask = false(1,numel(allpolys));
        for i=1:numel(elmfeatinds)
            mask(:) = false;
            mask(parentinds{elmfeatinds(i)}) = true;
            parentinds{elmfeatinds(i)} = find(mask | allpolys == obj(elmfeatinds(i)).parentelement);
        end
    end
    if any(othermask)
        otherinds = find(othermask);
        
%         % find sets of features of same class and call function once for each set
%         [~,~,id] = unique([obj(otherinds).type]);
%         typegroups = array2cell_mex(1:numel(id),id');
%         for i=1:numel(typegroups)
%             parentinds(otherinds(typegroups{i})) = obj(otherinds(typegroups{i})).inPolygon(allpolys);
%         end
        parentinds(otherinds) = obj(otherinds).inPolygon(allpolys);
    end
    
    for i=1:numel(obj)
        o = obj(i); % this is necessary for performance for some reason (100x faster)
        o.p_parentpolygons = allpolys(parentinds{i});
    end
    
%     allpolys = obj.scenegroup.polygons;
%     polyareas = [];
%     
%     for i=1:numel(obj)
%     
%         if isa(obj(i),'ScenePolygon')
% 
%             % only polygons with areas larger than (1+eps)*myarea are
%             % parents (avoid loops)
%             if isempty(polyareas)
%                 polyareas = [allpolys.area];
%             end
%             myarea = obj(i).area;
%             mask = allpolys ~= obj(i) & polyareas > myarea*1.25;
% 
%             parentmask = false(1,numel(allpolys));
%             parentmask(mask) = logical(obj(i).inPolygon(allpolys(mask)));
%         elseif isa(obj(i),'SceneElementFeature')
%             parentmask = logical(obj(i).inPolygon(allpolys));
%             
%             % the parent element is always a parent polygon (if it is a polygon)
%             parentmask = parentmask | allpolys == obj.parentelement;
%         else
%             parentmask = logical(obj(i).inPolygon(allpolys));
%         end
%         
%         obj(i).p_parentpolygons = allpolys(parentmask);
%     end
end

function updateChildfeatures(obj)

%     if isempty(obj.scenegroup)
%         obj.p_childfeatures = [];
%         return;
%     end
    
%     nodegroups = [obj.scenegroup];
    nodegroups = obj.getScenegroup;
    nodegroups = [nodegroups{:}];
    
    if isempty(nodegroups)
        for i=1:numel(obj)
            o = obj(i);
            o.p_childfeatures = [];
        end
        return;
    elseif not(all(nodegroups == nodegroups(1)))
        error('Only hierarchy nodes from the same group are allowed.');
    end
    
    polymask = arrayfun(@(x) isa(x,'ScenePolygon'),obj);
    othermask = not(polymask);
    
    if any(polymask)
        allfeatures = nodegroups(1).features;
        
        % update all feature parent polygons at the same time
        mask = not(allfeatures.hasParentpolygons); 
        allfeatures(mask).updateParentpolygons;
        
        allparents = {allfeatures.parentpolygons};
        featinds = cellind_mex(allparents);
        featinds = [featinds{:}];
        allparents = [allparents{:}];
        
        polyinds = find(polymask);
        for i=1:numel(polyinds)
            o = obj(polyinds(i));
            childinds = unique(featinds(allparents == o));
            o.p_childfeatures = allfeatures(childinds);
        end
    end
    if any(othermask)
        otherinds = find(othermask);
        for i=1:numel(otherinds)
            o = obj(otherinds(i)); % this is necessary for performance for some reason (100x faster)
            o.p_childfeatures = ScenePolygon.empty;
        end
    end
    
    
%     if obj.type ~= SceneFeature.POLYGON
%         obj.p_childfeatures = ScenePolygon.empty;
%         return;
%     end
%     
%     allfeatures = obj.scenegroup.features;
%     mask = not(allfeatures.hasParentpolygons); 
%     allfeatures(mask).updateParentpolygons; % needed so updateParentpolygon is only called once with all features as argument
%     allparents = {allfeatures.parentpolygons};
%     
%     featinds = cellfun(@(x,y) ones(1,numel(x)).*y,allparents,num2cell(1:numel(allparents)),'UniformOutput',false);
%     featinds = [featinds{:}];
%     allparents = [allparents{:}];
%     
%     childmask = ismember(allparents,obj);
%     childinds = unique(featinds(childmask));
%     obj.p_childfeatures = allfeatures(childinds);
end

function updateDirectparentpolygons(obj)
    for i=1:numel(obj)
        o = obj(i); % this is necessary for performance for some reason (100x faster)
        if isempty(o.parentpolygons)
            o.p_directparentpolygons = ScenePolygon.empty;
        else
            if isa(o,'SceneElementFeature')
                o.p_directparentpolygons = o.parentelement;
            else
                parents = o.parentpolygons;
                mask = ismember(parents,[parents.parentpolygons]);
                o.p_directparentpolygons = parents(not(mask));
            end
        end
    end
end

function updateDirectsiblingpolygons(obj)
%     if isempty(obj.scenegroup)
%         obj.p_directsiblingpolygons = [];
%         return;
%     end
    
%     nodegroups = [obj.scenegroup];
    nodegroups = obj.getScenegroup;
    nodegroups = [nodegroups{:}];
    
    if isempty(nodegroups)
        for i=1:numel(obj)
            o = obj(i); % this is necessary for performance for some reason (100x faster)
            o.p_directsiblingpolygons = [];
        end
        return;
    elseif not(all(nodegroups == nodegroups(1)))
        error('Only hierarchy nodes from the same group are allowed.');
    end
    
    % get all objects except the this object
    allpolys = nodegroups(1).polygons;
%     obj.scenegroup.polygons(not(obj.scenegroup.polygons == obj));

    if isempty(allpolys)
        for i=1:numel(obj)
            o = obj(i); % this is necessary for performance for some reason (100x faster)
            o.p_directsiblingpolygons = ScenePolygon.empty;
        end
        return;
    end

    % get child polygon indices (into the list of all polygons) for all polygons
    noparentpolys = false(1,numel(allpolys));
    parentpolychildpolyinds = cell(1,numel(allpolys));
    for i=1:numel(allpolys)
        noparentpolys(i) = isempty(allpolys(i).directparentpolygons);
        for j=1:numel(allpolys(i).directparentpolygons)
            ind = find(allpolys == allpolys(i).directparentpolygons(j),1,'first');
            parentpolychildpolyinds{ind}(end+1) = i;
        end
    end
    
    % get direct parent indices (into the list of all polygons) for all given features
    featdirectparents = {obj.directparentpolygons};
    featdirectparentfeatinds = cellind_mex(featdirectparents);
    featdirectparents = [featdirectparents{:}];
    featdirectparentfeatinds = [featdirectparentfeatinds{:}];
    [~,featdirectparentinds] = ismember(featdirectparents,allpolys);
    featdirectparentinds = array2cell_mex(featdirectparentinds,featdirectparentfeatinds,numel(obj),2);
    
    clear featdirectparents featdirectparentfeatinds;
    
%     objcells = num2cell(obj);
    for i=1:numel(obj)
        o = obj(i); % this is necessary for performance for some reason (100x faster)
        if isempty(o.directparentpolygons)
            % find all objects that have empty direct parent objects as well
%             o.p_directsiblingpolygons = allpolys(...
%                 arrayfun(@(x) isempty(x.directparentpolygons), allpolys) & ...
%                 allpolys ~= o);
            o.p_directsiblingpolygons = allpolys(noparentpolys & allpolys ~= o);
        else
            % find all objects that share one or more direct parents
%             directparents = o.directparentpolygons;
%             sharedparentpolys = false(1,numel(allpolys));
%             for j=1:numel(allpolys)
%                 sharedparentpolys(j) = any(ismember(directparents,allpolys(j).directparentpolygons));
%             end
%             o.p_directsiblingpolygons = allpolys(sharedparentpolys & allpolys ~= o);
            
%             o.p_directsiblingpolygons = allpolys(...
%                 arrayfun(@(x) any(ismember(directparents,x.directparentpolygons)),allpolys) & ...
%                 allpolys ~= o);

%             [~,inds] = ismember(o.directparentpolygons,allpolys); % get parent polygon indices of the feature
%             polyinds = unique([parentpolychildpolyinds{inds}]);

            polyinds = unique([parentpolychildpolyinds{featdirectparentinds{i}}]);
            o.p_directsiblingpolygons = allpolys(polyinds);
        end
    end
end

function updateDirectchildfeatures(obj)
%     if isempty(obj.scenegroup)
%         obj.p_directchildfeatures = [];
%         return;
%     end
    
%     nodegroups = [obj.scenegroup];
    nodegroups = obj.getScenegroup;
    nodegroups = [nodegroups{:}];
    
    if isempty(nodegroups)
        for i=1:numel(obj)
            o = obj(i); % this is necessary for performance for some reason (100x faster)
            o.p_directchildfeatures = [];
        end
        return;
    elseif not(all(nodegroups == nodegroups(1)))
        error('Only hierarchy nodes from the same group are allowed.');
    end
    
%     if obj.type ~= SceneFeature.POLYGON
%         obj.p_directchildfeatures = ScenePolygon.empty;
%         return;
%     end
    
    polymask = arrayfun(@(x) isa(x,'ScenePolygon'),obj);
    othermask = not(polymask);
    if any(polymask)
        polyinds = find(polymask);

        for i=1:numel(polyinds)
            o = obj(polyinds(i)); % this is necessary for performance for some reason (100x faster)
            childs = o.childfeatures;
            if not(isempty(childs))
                alldirectparents = {childs.directparentpolygons};

                childinds = cellfun(@(x,y) ones(1,numel(x)).*y,...
                    alldirectparents,...
                    num2cell(1:numel(alldirectparents)),...
                    'UniformOutput',false);
                childinds = [childinds{:}];
                alldirectparents = [alldirectparents{:}];

%                 directchildmask = ismember(alldirectparents,obj(polyinds(i)));
                directchildmask = alldirectparents == o;
                directchildinds = unique(childinds(directchildmask));
            else
                directchildinds = ones(1,0);
            end
            o.p_directchildfeatures = childs(directchildinds);
        end
    end
    if any(othermask)
        otherinds = find(othermask);
        for i=1:numel(otherinds)
            o = obj(otherinds(i)); % this is necessary for performance for some reason (100x faster)
            o.p_directchildfeatures = ScenePolygon.empty;
        end
    end
    
%     childs = obj.childfeatures;
%     if not(isempty(childs))
%         alldirectparents = {childs.directparentpolygons};
% 
%         childinds = cellfun(@(x,y) ones(1,numel(x)).*y,...
%             alldirectparents,...
%             num2cell(1:numel(alldirectparents)),...
%             'UniformOutput',false);
%         childinds = [childinds{:}];
%         alldirectparents = [alldirectparents{:}];
% 
%         directchildmask = ismember(alldirectparents,obj);
%         directchildinds = unique(childinds(directchildmask));
%     else
%         directchildinds = ones(1,0);
%     end
%     obj.p_directchildfeatures = childs(directchildinds);
end

function b = hasParentpolygons(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = isa(obj(i).p_parentpolygons,'ScenePolygon');
    end
%     b = arrayfun(@(x) isa(x.p_parentpolygons,'ScenePolygon'),obj);
end
function b = hasChildfeatures(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = isa(obj(i).p_childfeatures,'SceneFeature');
    end
%     b = arrayfun(@(x) isa(x.p_childfeatures,'SceneFeature'),obj);
end
function b = hasDirectparentpolygons(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = isa(obj(i).p_directparentpolygons,'ScenePolygon');
    end
%     b = arrayfun(@(x) isa(x.p_directparentpolygons,'ScenePolygon'),obj);
end
function b = hasDirectsiblingpolygons(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = isa(obj(i).p_directsiblingpolygons,'ScenePolygon');
    end
%     b = arrayfun(@(x) isa(x.p_directsiblingpolygons,'ScenePolygon'),obj);
end
function b = hasDirectchildfeatures(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = isa(obj(i).p_directchildfeatures,'SceneFeature');
    end
%     b = arrayfun(@(x) isa(x.p_directchildfeatures,'SceneFeature'),obj);
end

end

methods(Static)

function updateAllDirectchildfeatures(scenegroup)
    allfeatures = scenegroup.features;
    for i=1:numel(allfeatures)
        f = allfeatures(i); % this is necessary for performance for some reason (100x faster)
        f.p_directchildfeatures = ScenePolygon.empty;
    end
    
    for i=1:numel(allfeatures)
        pobjs = allfeatures(i).directparentpolygons;
        for j=1:numel(pobjs)
            o = pobjs(j); % this is necessary for performance for some reason (100x faster)
            o.p_directchildfeatures(end+1) = allfeatures(i);
        end
    end
end

function updateAllChildfeatures(scenegroup)
    allfeatures = scenegroup.features;
    for i=1:numel(allfeatures)
        f = allfeatures(i); % this is necessary for performance for some reason (100x faster)
        f.p_childfeatures = ScenePolygon.empty;
    end
    
    for i=1:numel(allfeatures)
        pobjs = allfeatures(i).parentpolygons;
        for j=1:numel(pobjs)
            o = pobjs(j); % this is necessary for performance for some reason (100x faster)
            o.p_childfeatures(end+1) = allfeatures(i);
        end
    end
end

% find the smallest polygon containing the point
function parentind = findPointParentpolygon(px,py,polys,allowedlabels)
    if nargin<4
        allowedlabels = [];
    end
    
    if not(isempty(allowedlabels))
        % parent of the points needs to have an allowed label
        allowed = false(1,numel(polys));
        for i=1:numel(polys)
            allowed(i) = any(ismember(polys(i).getLabels,allowedlabels));
        end
        pinds = find(allowed);
    else
        % parent of the points can be any object
        pinds = 1:numel(polys);
    end
    
    % find the smallest polygon containing the point
    parentind = zeros(size(px));
    parentarea = inf(size(px));
    for j=pinds
        inpolymask = pointInPolygon_mex(... % (this already checks if point is in poly bbox)
            px(:)',py(:)',...
            polys(j).verts(1,:),...
            polys(j).verts(2,:));
        inpolymask = reshape(inpolymask,size(px));
        
%         psize = polys(j).size;
        parea = polys(j).area;
        % smaller area also means that it cannot contain a polygon with
        % larger area (as opposed to size if the centroid is placed far
        % from the center)
        
        mask = parea < parentarea & inpolymask;
        parentind(mask) = j;
        parentarea(mask) = parea;
    end
end

% find the smallest polygon at least partially containing the feature
% (Note that this is different from the parentpolygons property in that
% only the given polygons are considered and only the polygon with smallest
% area is taken.)
function parentind = findFeatureParentpolygon(feats,polys,allowedlabels)
    if nargin<4
        allowedlabels = [];
    end
    
    if not(isempty(allowedlabels))
        % parent of the points needs to have an allowed label
        allowed = false(1,numel(polys));
        for i=1:numel(polys)
            allowed(i) = any(ismember(polys(i).getLabels,allowedlabels));
        end
        pinds = find(allowed);
    else
        % parent of the points can be any object
        pinds = 1:numel(polys);
    end
    
    % find the smallest polygon containing or intersecting the feature
    polyareas = [polys(pinds).area];
    parentind = zeros(1,numel(feats));
    containingpolyinds = feats.inPolygon(polys(pinds));
    for j=1:numel(feats)
%         containingpolyinds{j} = feats(j).inPolygon(polys(pinds));
%         containingpolyinds = [containingpolyinds{:}];
        
        if isempty(containingpolyinds{j})
            parentind(j) = 0;
        else
            [~,minareaind] = min(polyareas(containingpolyinds{j}));
            parentind(j) = pinds(containingpolyinds{j}(minareaind));
        end
    end 
end

% find the smallest polygon at least partially containing the polyline
function parentind = findPolylineParentpolygon(polylines,closed,polys,allowedlabels)
    if nargin<4
        allowedlabels = [];
    end
    
    if not(iscell(polylines))
        if isempty(polylines)
            polylines = {};
        else
            polylines = {polylines};
        end
    end
    
    if not(isempty(allowedlabels))
        % parent of the points needs to have an allowed label
        allowed = false(1,numel(polys));
        for i=1:numel(polys)
            allowed(i) = any(ismember(polys(i).getLabels,allowedlabels));
        end
        pinds = find(allowed);
    else
        % parent of the points can be any object
        pinds = 1:numel(polys);
    end
    
    % find the smallest polygon containing or intersecting the feature
    polyareas = zeros(1,numel(polys));
    polyareas(pinds) = [polys(pinds).area];
    parentind = zeros(1,numel(polylines));
    [polybbmin,polybbmax] = polys(pinds).boundingbox;
    for j=1:numel(polylines)
        [mybbmin,mybbmax] = pointsetBoundingbox(polylines{j});
        pindcandidates = pinds(...
            polybbmin(1,:) < mybbmax(1) & polybbmin(2,:) < mybbmax(2) & ...
            polybbmax(1,:) > mybbmin(1) & polybbmax(2,:) > mybbmin(2));
        
        p = polylines{j}(:,1);
        if closed(j)
            pts = polylines{j}(:,[1:end,1]);
        else
            pts = polylines{j};
        end
        containingcandidatemask = false(1,numel(pindcandidates));
        for i=1:numel(pindcandidates)
            intersects = linesegLinesegIntersection2D_mex(...
                pts(:,1:end-1),pts(:,2:end),...
                polys(pindcandidates(i)).verts,...
                polys(pindcandidates(i)).verts(:,[2:end,1]));
            intersecting = any(intersects(:) > 0);

            pointinpoly = pointInPolygon_mex(...
                p(1),p(2),...
                polys(pindcandidates(i)).verts(1,:),polys(pindcandidates(i)).verts(2,:));

            containingcandidatemask(i) = intersecting || pointinpoly;
        end
        
        containingpolyinds = pindcandidates(containingcandidatemask);
        if isempty(containingpolyinds)
            parentind(j) = 0;
        else
            [~,minareaind] = min(polyareas(containingpolyinds));
            parentind(j) = containingpolyinds(minareaind);
        end
    end 
end

end

methods(Abstract)
    hasScenegroup(obj);
    fp = getScenegroup(obj);
    
%     updateParentpolygons(obj);
end

methods(Abstract)
    % relations 0 and 2 are never returned, because only relevant poly indices
    % (i.e. feature is inside poly or intersecting) are returned
    % relation: 0: feature and poly are separate
    % relation: 1: feature completely inside poly
    % relation: 2: poly completely inside feature
    % relation: 3: feature and poly are intersecting
    [polyinds,relation] = inPolygon(obj,poly,objcandidateinds);
end

end

