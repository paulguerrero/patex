classdef SceneComplexPolygonset < ScenePointset2D
    
properties(SetAccess=protected)
    polygons = SceneComplexPolygon.empty(1,0);
end
    
properties(Dependent,SetAccess=protected)
    area;
    perimeter;
end

properties(Access={?ScenePolyline,?SceneImporter,?SceneExporter})
    p_area = [];
    p_perimeter = [];
end

methods(Static)

function n = type_s
    n = 'complexpolygonset';
end

function d = dimension_s
    d = 1;
end

end

methods

% obj = SceneComplexPolygonset()
% obj = SceneComplexPolygonset(copyobj)
% obj = SceneComplexPolygonset(polygons)
% obj = SceneComplexPolygonset(polygons, labels)
function obj = SceneComplexPolygonset(varargin)

    obj@ScenePointset2D;
    
    if numel(varargin) == 0
        % do nothing (default values)
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneComplexPolygonset')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneComplexPolygon')
        obj.setPolygons(varargin{1});
        
        obj.definePosition(obj.center);
        obj.defineOrientation(obj.pcorientation);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
        
    elseif numel(varargin) == 2 && isa(varargin{1},'SceneComplexPolygon')
        obj.setPolygons(varargin{1});
        obj.setLabels(varargin{2});
        
        obj.definePosition(obj.center);
        obj.defineOrientation(obj.pcorientation);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
    else
        error('Cannot construct SceneComplexPolygonset with these parameters.')
    end
end

function copyFrom(obj,obj2)

    copyFrom@ScenePointset2D(obj,obj2);
    
    obj.polygons = SceneFeature.copyFeatsarrayFrom(obj.polygons,obj2.polygons);

    obj.p_area = obj2.p_area;
    obj.p_perimeter = obj2.p_perimeter;
end

function obj2 = clone(obj)
    obj2 = SceneComplexPolygonset(obj);
end

function delete(obj)
    delete(obj.polygons(isvalid(obj.polygons)));
end

function setPolygons(obj,polygons)
    obj.polygons = obj.copyFeatsarrayFrom(obj.polygons,polygons);
    
    obj.polygons.setScenegroup(obj.scenegroup);
    obj.polygons.setLabels(obj.labels);
    
    obj.dirty;
end

% function setVerts(obj,verts) %#ok<INUSD>
%     error('Directly setting the vertices is not allowed in a polygon set, set the vertices of the individual polygons instead.');
% end

function val = get.area(obj)
    if isempty(obj.p_area)
        obj.updateArea;
    end
    val = obj.p_area;
end
function val = getArea(obj)
    mask = not(obj.hasArea);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updateArea;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updateArea;
                end
            end
        end
    end
    val = [obj.p_area];
end
function updateArea(obj)
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_area = sum([obj.polygons.area]); % do not consider overlapping polygon areas for now
    end
end
function b = hasArea(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_area));
    end
end

function val = get.perimeter(obj)
    if isempty(obj.p_perimeter)
        obj.updatePerimeter;
    end
    val = obj.p_perimeter;
end
function val = getPerimeter(obj)
    mask = not(obj.hasPerimeter);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updatePerimeter;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updatePerimeter;
                end
            end
        end
    end
    val = [obj.p_perimeter];
end
function updatePerimeter(obj)
    
    for k=1:numel(obj)
        o = obj(k);

        o.p_perimeter = sum([obj.polygons.perimeter]);
    end
end
function b = hasPerimeter(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_perimeter));
    end
end
    
function updateCenter(obj)
    for k=1:numel(obj)
        o = obj(k);
        
        polycenters = [obj.polygons.center];
        polyarea = [obj.polygons.area];
        
        o.p_center = sum(bsxfun(@times,polycenters,polyarea),2)./sum(polyarea);
    end
end

end

methods(Access=protected)

function feats = subfeaturesImpl(obj)
    mask = not(obj.hasSubfeatures);
    if any(mask)
        obj(mask).updateSubfeatures;
    end
    
    feats = cell(1,numel(obj));
    for k=1:numel(obj)
        feats{k} = [obj(k).polygons.corners,obj(k).polygons.edges,obj(k).polygons.segments];
    end
end
function b = hasSubfeaturesImpl(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        o = obj(i); 
        b(i) = not(any(o.polygons.cornersDirty) | any(o.polygons.edgesDirty));
    end
end
function updateSubfeaturesImpl(obj)
    allpolys = [obj.polys];
    allpolys.updateCorners;
    allpolys.updateEdges;
end

function dirtyImpl(obj)
    obj.dirtyImpl@ScenePointset2D;
    
    for k=1:numel(obj)
        o = obj(k);

        o.p_area = [];
        o.p_perimeter = [];
    end
end

function updateLocalImpl(obj)

    obj.updateLocalImpl@ScenePointset2D;
    
    mask = not(obj.hasArea);
    if any(mask)
        obj(mask).updateArea;
    end
    mask = not(obj.hasPerimeter);
    if any(mask)
        obj(mask).updatePerimeter;
    end
    mask = not(obj.hasRoundness);
    if any(mask)
        obj(mask).updateRoundness;
    end
end

% relations 0 and 2 are never returned, because only relevant poly indices
% (i.e. feature is inside poly or intersecting) are returned
% relation: 0: feature and poly are separate
% relation: 1: feature completely inside poly
% relation: 2: poly completely inside feature
% relation: 3: feature and poly are intersecting
function [polyinds,relation] = inPolygonImpl(obj,poly,polycandidates)

    error('Not implemented yet.');
    
%     polyinds = cell(numel(obj),1);
%     if nargout >= 2
%         relation = cell(numel(obj),1);
%     end
%     if not(isempty(poly)) && not(isempty(obj))
%         [polybbmin,polybbmax] = poly.boundingbox;
%         [mybbmin,mybbmax] = obj.boundingbox;
% 
%         for j=1:numel(obj)
%             
%             % find polygons with bounding boxes containing the polygon set
%             % bounding box
%             if nargin < 3 || isempty(polycandidates)
%                 polyinds{j} = find(...
%                     polybbmin(1,:) < mybbmax(1,j) & polybbmin(2,:) < mybbmax(2,j) & ...
%                     polybbmax(1,:) > mybbmin(1,j) & polybbmax(2,:) > mybbmin(2,j));
%             else
%                 polyinds{j} = polycandidates{j}(...
%                     polybbmin(1,polycandidates{j}) < mybbmax(1,j) & polybbmin(2,polycandidates{j}) < mybbmax(2,j) & ...
%                     polybbmax(1,polycandidates{j}) > mybbmin(1,j) & polybbmax(2,polycandidates{j}) > mybbmin(2,j));
%             end
%             
%             % find all polygons containing at least part of the point set
%             [polyinds] = obj.polygons.inPolygonImpl
%             p = obj(j).verts(:,1);
%             keepmask = false(1,numel(polyinds{j}));
%             if nargout >= 2
%                 relation{j} = zeros(1,numel(polyinds{j}));
%             end
%             for i=1:numel(polyinds{j})
%                 
%                 pointinpoly = pointInPolygon_mex(p(1),p(2),poly(polyinds{j}(i)).verts(1,:),poly(polyinds{j}(i)).verts(2,:));
% 
%                 intersecting = any(pointinpoly);
%                 contained = all(pointinpoly);
%                 
%                 keepmask(i) = intersecting | contained;
%                 
%                 if nargout >= 2
%                     if contained
%                         relation{j}(i) = 1;
%                     elseif intersecting
%                         relation{j}(i) = 3;
%                     end
%                 end
%             end
%             polyinds{j} = polyinds{j}(keepmask);
%             if nargout >= 2
%                 relation{j} = relation{j}(keepmask);
%             end
%         end
%     end
end
    
function [d,cp] = pointDistanceImpl(obj,p,pose)
    
    d = inf(numel(obj),size(p,2));
    cp = zeros(numel(obj),size(p,2),size(p,1));
    for i=1:numel(obj)
        o = obj(i);
        
        if size(p,1) == 2
            if nargin >= 3 && not(isempty(pose)) && not(all(pose{i}==obj(i).pose2D))
                opose = repmat(o.pose2D,1,size(pose,2));
                opolyposes = [o.polygons.pose2D];
            else
                opose = [];
            end
        elseif size(p,1) == 3
            if nargin >= 3 && not(isempty(pose)) && not(all(pose{i}==obj(i).pose3D))
                opose = repmat(o.pose3D,1,size(pose,2));
                opolyposes = [o.polygons.pose3D];
            else
                opose = [];
            end
        else
            error('Only implemented for two or three dimensions.');
        end
        
        od = zeros(numel(o.polygons),size(p,2));
        ocp = zeros(numel(o.polygons),size(p,2),size(p,1));
        for j=1:numel(o.polygons)
            if not(isempty(opose))
                polyposearg = {posetransform(opolyposes(:,j),pose,opose)};
            else
                polyposearg = cell(1,0);
            end

            [od(j,:),ocp(j,:,:)] = o.polygons(j).pointDistanceImpl(p,polyposearg{:});
        end
        
        [d(i,:),mininds] = min(od,[],1);
        linind = sub2ind(size(ocp),mininds,1:numel(mininds),1);
        cp(i,:,1) = ocp(linind);
        linind = sub2ind(size(ocp),mininds,1:numel(mininds),2);
        cp(i,:,2) = ocp(linind);
        if size(p,1) == 3
            linind = sub2ind(size(ocp),mininds,1:numel(mininds),3);
            cp(i,:,3) = ocp(linind);
        end
    end
end

function [d,t,cp] = linesegDistanceImpl(obj,p1,p2,pose) % distance to a 3D line segment

    d = inf(numel(obj),size(p1,2));
    t = nan(numel(obj),size(p1,2));
    cp = zeros(numel(obj),size(p1,2),size(p1,1));
    for i=1:numel(obj)
        o = obj(i);
        
        if size(p,1) == 2
            if nargin >= 3 && not(isempty(pose)) && not(all(pose{i}==obj(i).pose2D))
                opose = repmat(o.pose2D,1,size(pose,2));
                opolyposes = [o.polygons.pose2D];
            else
                opose = [];
            end
        elseif size(p,1) == 3
            if nargin >= 3 && not(isempty(pose)) && not(all(pose{i}==obj(i).pose3D))
                opose = repmat(o.pose3D,1,size(pose,2));
                opolyposes = [o.polygons.pose3D];
            else
                opose = [];
            end
        else
            error('Only implemented for two or three dimensions.');
        end
        
        od = zeros(numel(o.polygons),size(p1,2));
        ot = nan(numel(o.polygons),size(p1,2));
        ocp = zeros(numel(o.polygons),size(p1,2),size(p1,1));
        for j=1:numel(o.polygons)
            if not(isempty(opose))
                polyposearg = {posetransform(opolyposes(:,j),pose,opose)};
            else
                polyposearg = cell(1,0);
            end

            [od(j,:),ot(j,:),ocp(j,:,:)] = o.polygons(j).linesegDistanceImpl(p1,p2,polyposearg{:});
        end
        
        [d(i,:),mininds] = min(od,[],1);
        linind = sub2ind(size(ot),mininds,1:numel(mininds));
        t(i,:) = ot(linind);
        linind = sub2ind(size(ocp),mininds,1:numel(mininds),1);
        cp(i,:,1) = ocp(linind);
        linind = sub2ind(size(ocp),mininds,1:numel(mininds),2);
        cp(i,:,2) = ocp(linind);
        if size(p,1) == 3
            linind = sub2ind(size(ocp),mininds,1:numel(mininds),3);
            cp(i,:,3) = ocp(linind);
        end
    end
end
    
% function updateContextImpl(obj)
%     obj.updateContextImpl@ScenePolyline;
% end
% 
% function clearContextImpl(obj)
%     obj.clearContextImpl@ScenePolyline;
% end
    
function moveAboutOriginImpl(obj,movevec)
    obj.moveAboutOriginImpl@ScenePointset2D(movevec);
    
%     for i=1:numel(obj.polygons)
%         obj.polygons(i).move(movevec,'relative');
%     end
end

function rotateAboutOriginImpl(obj,rotangle)
    obj.rotateAboutOriginImpl@ScenePointset2D(rotangle);
    
%     for i=1:numel(obj.polygons)
%         obj.polygons(i).rotate(rotangle,'relative',[0;0]);
%     end
end

function scaleAboutOriginImpl(obj,relscale)
    
    obj.scaleAboutOriginImpl@ScenePointset2D(relscale);
    
%     for i=1:numel(obj.polygons)
%         obj.polygons(i).scale(relscale,'relative',[0;0],0);
%     end
    
    if not(isempty(obj.p_area))
        obj.p_area = obj.p_area * prod(relscale);
    end
    
    obj.p_perimeter = [];
end

% when mirroring a polygon, the winding order needs to be mantained and as
% a result the ordering of the vertices is reversed
function mirrorAboutOriginImpl(obj,mirchange)
    
    if not(mirchange)
        return;
    end
    
%     obj.mirrorAboutOriginImpl@ScenePointset2D(mirchange);
    
    for i=1:numel(obj.polygons)
        obj.polygons(i).mirror(mirchange,'relative',[0;0],0);
    end
        
    obj.p_orientedbbox = []; 
    if obj.hasConvexhull
        obj.p_convexhull = [obj.p_convexhull(1,:);-obj.p_convexhull(2,:)]; 
        obj.p_convexhull = obj.p_convexhull(:,[1,end:-1:2]);
    end
    obj.p_principalcomps = [];
    obj.p_principalbbox = [];
end

function s = similarityImpl(obj,obj2)
    s = ones(numel(obj),numel(obj2)); % all polygon sets have similarity 1 to each other for now
end

function v = getVertsImpl(obj)
    v = [obj.polygons.verts];
end

function setVertsImpl(obj,v)
    voffset = 0;
    for i=1:numel(obj.polygons)
        if voffset >= size(v,2)
            delinds = i:numel(obj.polygons);
            delete(obj.polygons(delinds(isvalid(obj.polygons(delinds)))));
            obj.polygons(delinds) = [];
            break;
        end
        
        npolyverts = size(obj.polygons(i).verts,2);
        obj.polygons(i).verts = v(:,...
            voffset+1 : ...
            min(end,voffset + npolyverts));
        
        voffset = voffset + npolyverts;
    end
end

end

end
