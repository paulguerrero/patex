% a parametrized surface
classdef SceneSurface < SceneElementFeature

properties(SetAccess=protected)
    faceindices = [];
    vparams = [];

    boundarypoly = ScenePolygon.empty;
end

properties(Dependent, SetAccess=protected)
    vertexindices;

    verts;
    faces;
    
    edges;
    edgefaces;
    
    fnormals;
    ftangents;
    fbitangents;
    fareas;
    vnormals;
    vtangents;
    vbitangents;
    boundary;
    
    avgtangent;
    avgbitangent;
    avgnormal;
    
    orientedbboxpose;
end

properties(Access={?SceneSurface,?SceneImporter,?SceneExporter})
    p_vertexindices = [];
    
    p_verts = [];
    p_faces = [];
    
    p_edges = [];
    p_edgefaces = [];
    
    p_fnormals = [];
    p_ftangents = [];
    p_fbitangents = [];
    p_fareas = [];
    p_vnormals = [];
    p_vtangents = [];
    p_vbitangents = [];
    p_boundary = [];
    
    p_avgtangent = [];
    p_avgbitangent = [];
    p_avgnormal = [];
    
    p_orientedbboxpose = [];
end

methods(Static)

function n = type_s
    n = 'surface';
end

function d = dimension_s
    d = 2;
end

end

methods
    
% SceneSurface - empty constructor
% SceneSurface(surface2) - copy constructor
% SceneSurface(parentelement,faceindices) - standard constructor
% SceneSurface(parentelement,faceindices,vparams) - standard constructor
% SceneSurface(...,labels) - standard constructor
% vparams must be given in ascending order of vertex indices
function obj = SceneSurface(varargin)
    
    if numel(varargin) == 0
        superargs = {};
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneSurface')
        superargs = {};
    elseif numel(varargin) >= 2 && numel(varargin) <= 4
        superargs = varargin(1);
        varargin(1) = [];
        
        if numel(varargin) >= 2 && ischar(varargin{2})
            superargs(end+1) = varargin(2);
            varargin(2) = [];        
        elseif numel(varargin) >= 3 && ischar(varargin{3})
            superargs(end+1) = varargin(3);
            varargin(3) = [];
        end
    else
        error('Invalid arguments.');
    end
    
    obj@SceneElementFeature(superargs{:});
%     obj.type = SceneFeature.SURFACE;
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneSurface')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) >= 1 && numel(varargin) <= 2
        if numel(varargin) >= 2
            obj.setFaceindicesAndVparams(varargin{1},varargin{2});
        else
            obj.setFaceindicesAndVparams(varargin{1});
        end
    else
        error('Invalid number of arguments.');
    end
end

function delete(obj)
    if not(isempty(obj.boundarypoly))
        delete(obj.boundarypoly(isvalid(obj.boundarypoly)));
    end
end

function copyFrom(obj,obj2)
    obj.copyFrom@SceneElementFeature(obj2);
    
    obj.faceindices = obj2.faceindices;
    obj.vparams = obj2.vparams;
    
    obj.p_vertexindices = obj2.p_vertexindices;
    obj.p_verts = obj2.p_verts;
    obj.p_faces = obj2.p_faces;
    obj.p_fnormals = obj2.p_fnormals;
    obj.p_ftangents = obj2.p_ftangents;
    obj.p_fareas = obj2.p_fareas;
    obj.p_fbitangents = obj2.p_fbitangents;
    obj.p_vnormals = obj2.p_vnormals;
    obj.p_vtangents = obj2.p_vtangents;
    obj.p_vbitangents = obj2.p_vbitangents;
    obj.p_boundary = obj2.p_boundary;
    obj.p_avgtangent = obj2.p_avgtangent;
    obj.p_avgbitangent = obj2.p_avgbitangent;
    obj.p_avgnormal = obj2.p_avgnormal;
    obj.p_orientedbboxpose = obj2.p_orientedbboxpose;
    
    % boundary polygons (copy conservatively)
    obj.boundarypoly = SceneFeature.copyFeatsarrayFrom(obj.boundarypoly,obj2.boundarypoly);
end

function obj2 = clone(obj)
    obj2 = SceneSurface(obj);
end
    
function setFaceindicesAndVparams(obj,faceindices,vparams)
    
    obj.dirty;
    
    obj.faceindices = faceindices;
    if nargin >= 3 && not(isempty(vparams))
        obj.vparams = vparams;
    else
        obj.vparams = zeros(0,numel(obj.vertexindices));
    end
    
%         if size(obj.vparams,2) ~= numel(obj.vertexindices)
%             error('Invalid number of vertex params.');
%         end

    if not(isempty(obj.boundarypoly))
        delete(obj.boundarypoly(isvalid(obj.boundarypoly)));
    end
    obj.boundarypoly = ScenePolygon.empty;
end

function bpolys = removeBoundarypoly(obj,bpolyinds)
    if nargin < 2
        bpolyinds = 1:numel(obj.boundarypoly);
    end
    
    if not(isnumeric(bpolyinds))
        bpolyinds = find(ismember(obj.boundarypoly,bpolyinds));
    end
    
    bpolys = obj.boundarypoly(bpolyinds);
    obj.boundarypoly(bpolyinds) = [];
    
    if nargout < 1
        delete(bpolys(isvalid(bpolys)));
    end
end

function addBoundarypoly(obj,val)
    if not(isa(val,'ScenePolygon'))
        error('Boundary polygon must be a polygon.');
    end
    
    % remove polys that are already boundarypolys
    if not(isempty(obj.boundarypoly))
        val(ismember(val,obj.boundarypoly)) = [];
    end
    
    obj.boundarypoly = [obj.boundarypoly,val];
end

function setBoundarypoly(obj,val)
    if not(isa(val,'ScenePolygon'))
        error('Boundary polygon must be a polygon.');
    end
    
    if not(isempty(obj.boundarypoly)) && (isempty(val) || ...
       numel(obj.boundarypoly) ~= numel(val) || ...
       any(obj.boundarypoly ~= val))
        
        delete(obj.boundarypoly(isvalid(obj.boundarypoly)));
    end
    
    obj.boundarypoly = val;
end

function poly = createBoundarypoly(obj)
    boundaries = obj.boundary;
    
    % to surface parameter space
    poly = ScenePolygon.empty;
    for j=1:numel(boundaries)
        bpverts = obj.toOrientedbboxParamspace(boundaries{j});
        bpverts = bpverts(1:2,:);
%         bpolys{end+1} = obj.toOrientedbboxParamspace(boundaries{j}); %#ok<AGROW>
%         bpolys{end} = bpolys{end}(1:2,:);
        
        poly(end+1) = ScenePolygon(bpverts,obj.labelStrs); %#ok<AGROW>
        [additionalpolys,alldegenerate] = poly(end).removeDegeneracies(true);
        if alldegenerate
            delete(poly(end));
            poly(end) = [];
        elseif not(isempty(additionalpolys))
            poly(end+1:end+numel(additionalpolys)) = additionalpolys;
        end
        
        
        
%         % convert boundaries to simple polygons (the projection of the
%         % boundary to 2D is not necessarily simple)
% %         is = linesegLinesegIntersect(...
% %             [bpolys{end};bpolys{end}(:,[2:end,1])]',...
% %             [bpolys{end};bpolys{end}(:,[2:end,1])]');
%         is = linesegLinesegIntersection2D_mex(...
%             bpolys{end},bpolys{end}(:,[2:end,1]),...
%             bpolys{end},bpolys{end}(:,[2:end,1]));
%         % remove intersections of segment with itself
%         is(sub2ind(size(is),1:size(is),1:size(is))) = 0;
%         % remove intersections of segment with direct neighbors
%         is(sub2ind(size(is),1:size(is),[2:size(is),1])) = 0;
%         is(sub2ind(size(is),[2:size(is),1],1:size(is))) = 0;
%         if any(is(:) > 0)
%             % boundary has self-intersections
%             [~,~,bpolybbdiag] = pointsetBoundingbox(bpolys{j});
%             newboundaries = polylineSetOutline(bpolys(j),true,false,false,bpolybbdiag*0.0001);
%             if numel(newboundaries) == 0
%                 bpolys(end) = [];
%                 continue;
%             elseif numel(newboundaries) > 1
%                 pareas = cellfun(@(x) polyarea(x(1,:),x(2,:)),newboundaries);
%                 [~,maxareaind] = max(pareas);
%                 bpolys(end) = newboundaries(maxareaind);
%             else
%                 bpolys(end) = newboundaries(1);
%             end
%         end
    end
    
%     if isempty(bpolys)
%         % create very small triangle in case the surface boundary polygon
%         % is completely degenerate (better than nothing)
%         [~,~,bpolybbdiag] = pointsetBoundingbox(obj.verts);
%         bpolys = {[...
%             obj.verts(:,1),...
%             obj.verts(:,1)+[0;bpolybbdiag*0.0001],...
%             obj.verts(:,1)+[bpolybbdiag*0.0001;0]]};
%     end

    % ignore boundaries that are holes
    ishole = false(1,numel(poly));
    for j=1:numel(poly)
        for k=j+1:numel(poly)
            pcr = polygonContainmentRelation(...
                poly(j).verts(1,:),poly(j).verts(2,:),...
                poly(k).verts(1,:),poly(k).verts(2,:));
            if pcr == 1
                ishole(j) = true;
            elseif pcr == 2
                ishole(k) = true;
            end
        end
    end
    delete(poly(ishole));
    poly(ishole) = [];
    
%     poly = ScenePolygon.empty;
%     for i=1:numel(bpolys)
    for i=1:numel(poly)
%         poly(end+1) = ScenePolygon(bpolys{i},obj.labelStrs); %#ok<AGROW>
        poly(i).defineOrientation(0); % so it points in the x direction of the parameter space
    end
end

% function set.vparams(obj,val)
%     obj.setVparams(val);
% end

function [bbmin,bbmax,bbdiag] = paramboundingbox(obj) % bounding box in parameter space (origin remains the same as in normal space)
    bbmin = zeros(3,numel(obj));
    bbmax = zeros(3,numel(obj));
    for i=1:numel(obj)
        pverts = [obj(i).avgtangent';obj(i).avgbitangent';obj(i).avgnormal']*obj(i).verts;
        bbmin(:,i) = min(pverts,[],2);
        bbmax(:,i) = max(pverts,[],2);
    end
    if nargout >= 3
        bbdiag = sqrt(sum((bbmax-bbmin).^2,1));
    end    
end

function [bbmin,bbmax,bbdiag] = orientedboundingbox(obj) % paramboundingbox in normal space coordinates
    [bbmin,bbmax] = obj.paramboundingbox;
    for i=1:numel(obj)
        basischange = [obj(i).avgtangent,obj(i).avgbitangent,obj(i).avgnormal];
        bbmin(:,i) = basischange * bbmin(:,i);
        bbmax(:,i) = basischange * bbmax(:,i);
    end
    if nargout >= 3
        bbdiag = sqrt(sum((bbmax-bbmin).^2,1));
    end    
end

% project a pose to planar parameter space with the origin in the lower left corner of the oriented bounding box
function params = toOrientedbboxParamspace(obj,pose)
    params = posetransform(pose,identpose3D,obj.orientedbboxpose);
end

% get pose from parameters of planar parameter space with the origin in the lower left corner of the oriented bounding box
function pose = fromOrientedbboxParamspace(obj,params)
    pose = posetransform(params,obj.orientedbboxpose,identpose3D);
end

function setVparams(obj,val)
    if size(val,2) ~= numel(obj.vertexindices)
        error('Invalid number of vertex parameters.');
    end
    obj.vparams = val;
    obj.dirty;
end

% projection to plane of average normal
function vparams = generatePlanarParametrization(obj)
    v = obj.verts;
    n = obj.avgnormal;
    if abs(dot(n,[0;0;1])) < abs(dot(n,[0;1;0]))
        xaxis = cross([0;0;1],n);
    else
        xaxis = cross([0;1;0],n);
    end
    yaxis = cross(n,xaxis);

    % re-normalize
    xaxis = xaxis ./ sqrt(sum(xaxis.^2,1));
    yaxis = yaxis ./ sqrt(sum(yaxis.^2,1));

    surfmean = mean(v,2);
    v = bsxfun(@minus,v,surfmean);
    v = v - bsxfun(@times,n,n' * v);
    vparams = [xaxis,yaxis]' * v;
end

function val = get.vertexindices(obj)
	if isempty(obj.p_vertexindices)
        obj.updateVertexIndices;
	end
    val = obj.p_vertexindices;
end
function updateVertexIndices(obj)

    for k=1:numel(obj)
        o = obj(k);

        if not(isempty(o.parentelement))
            o.p_vertexindices = unique(o.parentelement.faces(:,o.faceindices))';
        end
    end
end
function b = hasVertexIndices(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_vertexindices));
    end
end

function val = get.verts(obj)
	if isempty(obj.p_verts)
        obj.updateVertices;
	end
    val = obj.p_verts;
end
function updateVertices(obj)
    mask = not(obj.hasVertexIndices);
    if any(mask)
        obj(mask).updateVertexIndices;
    end

    for k=1:numel(obj)
        o = obj(k);

        if not(isempty(o.parentelement))
            o.p_verts = o.parentelement.verts(:,o.vertexindices);
        end
    end
end
function b = hasVertices(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_verts));
    end
end

function val = get.faces(obj)
	if isempty(obj.p_faces)
        obj.updateFaces;
	end
    val = obj.p_faces;
end
function updateFaces(obj)
    mask = not(obj.hasVertexIndices);
    if any(mask)
        obj(mask).updateVertexIndices;
    end
    
    for k=1:numel(obj)
        o = obj(k);

        if not(isempty(o.parentelement))
            meshvind2surfacevind = zeros(max(o.vertexindices),1);
            meshvind2surfacevind(o.vertexindices) = 1:numel(o.vertexindices);

            o.p_faces = meshvind2surfacevind(o.parentelement.faces(:,o.faceindices));
        end
    end
end
function b = hasFaces(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_faces));
    end
end

function val = get.edges(obj)
	if isempty(obj.p_edges)
        obj.updateEdges;
	end
    val = obj.p_edges;
end
function updateEdges(obj)

    mask = not(obj.hasVertices);
    if any(mask)
        obj(mask).updateVertices;
    end
    mask = not(obj.hasFaces);
    if any(mask)
        obj(mask).updateFaces;
    end
    
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_edges = trimeshEdges(o.faces,size(o.verts,2));
    end
end
function b = hasEdges(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_edges));
    end
end

function finds = connectedComponents(obj)
    
    mask = not(obj.hasEdges);
    if any(mask)
        obj(mask).updateEdges;
    end
    mask = not(obj.hasEdgefaces);
    if any(mask)
        obj(mask).updateEdgefaces;
    end
    
    efaces = obj.edgefaces;
    einds = find(logical(efaces) & logical(efaces'));
    f1 = efaces(einds);
    efaces = efaces';
    f2 = efaces(einds);
    
    A = sparse(f1,f2,ones(size(f1)),size(obj.faces,2),size(obj.faces,2));
    g = graph(A);
    finds = g.conncomp;
end

function val = get.edgefaces(obj)
	if isempty(obj.p_edgefaces)
        obj.updateEdgesfaces;
	end
    val = obj.p_edgefaces;
end
function updateEdgefaces(obj)
    
    mask = not(obj.hasVertices);
    if any(mask)
        obj(mask).updateVertices;
    end
    mask = not(obj.hasFaces);
    if any(mask)
        obj(mask).updateFaces;
    end
    mask = not(obj.hasEdges);
    if any(mask)
        obj(mask).updateEdges;
    end

    for k=1:numel(obj)
        o = obj(k);
    
        faceedges = [o.faces([1,2],:),o.faces([2,3],:),o.faces([3,1],:)];
        edgefaceinds = repmat(1:size(o.faces,2),1,3);
        
        % set duplicate edgefaceinds (multiple faces adjacent to same
        % half-edge) to 0, so sparse() does not add the face indices
        halfedgeinds = 1:size(faceedges,2);
        [faceedges_sorted,perm] = sortrows([faceedges(1,:)',faceedges(2,:)']);
        halfedgeinds = halfedgeinds(perm);
        duplicateinds = find(all(diff(faceedges_sorted,1,1)==0,2))+1;
        if not(isempty(duplicateinds))
            warning('Non-manifold faces or edges found while computing edge faces.');
            edgefaceinds(halfedgeinds(duplicateinds)) = 0;
        end
        
        o.p_edgefaces = sparse(faceedges(1,:)',faceedges(2,:)',edgefaceinds',size(o.verts,2),size(o.verts,2));
    end
end
function b = hasEdgefaces(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_edgefaces));
    end
end

function val = get.fnormals(obj)
	if isempty(obj.p_fnormals)
        obj.updateFnormals;
	end
    val = obj.p_fnormals;
end
function updateFnormals(obj)
    
    for k=1:numel(obj)
        o = obj(k);

        if not(isempty(o.parentelement))
            o.p_fnormals = o.parentelement.fnormals(:,o.faceindices);
        end
    end
end
function b = hasFnormals(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_fnormals));
    end
end

function val = get.ftangents(obj)
	if isempty(obj.p_ftangents)
        obj.updateFtangents;
	end
    val = obj.p_ftangents;
end
function updateFtangents(obj)
    
    mask = not(obj.hasFnormals);
    if any(mask)
        obj(mask).updateFnormals;
    end
    
    for k=1:numel(obj)
        o = obj(k);

        if not(isempty(o.parentelement))
    
            if isempty(o.vparams)
                f = o.faces;
                
                warning('No parameterization given, using standard (probably suboptimal) parameterization to compute tangents.');
                
                dirs = quat2dcm(o.parentelement.orientation');
                maindirs = dirs * o.fnormals;
                
                [~,maindirind] = max(abs(maindirs),[],1);
                
                o.p_ftangents = zeros(3,size(f,2));
                o.p_fbitangents = zeros(3,size(f,2));
                
                % faces with normals that point mainly in the local x
                % direction of the mesh
                mask = maindirind == 1 & maindirs(1,:) >= 0;
                o.p_ftangents(:,mask) = repmat([0;1;0],1,sum(mask));
                mask = maindirind == 1 & maindirs(1,:) < 0;
                o.p_ftangents(:,mask) = repmat([0;-1;0],1,sum(mask));
                
                % faces with normals that point mainly in the local y
                % direction of the mesh
                mask = maindirind == 2 & maindirs(2,:) >= 0;
                o.p_ftangents(:,mask) = repmat([-1;0;0],1,sum(mask));
                mask = maindirind == 2 & maindirs(2,:) < 0;
                o.p_ftangents(:,mask) = repmat([1;0;0],1,sum(mask));
                
                % faces with normals that point mainly in the local z
                % direction of the mesh
                mask = maindirind == 3 & maindirs(3,:) >= 0;
                o.p_ftangents(:,mask) = repmat([1;0;0],1,sum(mask));
                mask = maindirind == 3 & maindirs(3,:) < 0;
                o.p_ftangents(:,mask) = repmat([-1;0;0],1,sum(mask));
                
            else
                % compute face tangents
                v = o.verts;
                vp = o.vparams;
                f = o.faces;

                e1 = v(:,f(2,:))-v(:,f(1,:));
                e2 = v(:,f(3,:))-v(:,f(1,:));

                s1 = vp(:,f(2,:))-vp(:,f(1,:));
                s2 = vp(:,f(3,:))-vp(:,f(1,:));

                r = 1./(s1(1,:).*s2(2,:) - s2(1,:).*s1(2,:));

                o.p_ftangents = bsxfun(@times,r,bsxfun(@times,s2(2,:),e1) + bsxfun(@times,-s1(2,:),e2));
    %             o.p_fbitangents = bsxfun(@times,r,bsxfun(@times,-s2(1,:),e1) + bsxfun(@times,s1(1,:),e2));
            end

            % Gram-Schmidt orthogonalize tangents (orthogonal to normals)
            o.p_ftangents = o.p_ftangents - bsxfun(@times,o.fnormals,dot(o.p_ftangents,o.fnormals));

            % compute bitangents
            o.p_fbitangents = cross(o.fnormals,o.p_ftangents);

            % get tangent/bitangent length and normalize
            ftangentlen = sqrt(sum(o.p_ftangents.^2,1));
            fbitangentlen = sqrt(sum(o.p_fbitangents.^2,1));
            o.p_ftangents = bsxfun(@times,o.p_ftangents,1./ftangentlen);
            o.p_fbitangents = bsxfun(@times,o.p_fbitangents,1./fbitangentlen);

            if any(any(isnan(o.p_ftangents) | isinf(o.p_ftangents))) || ...
               any(any(isnan(o.p_fbitangents) | isinf(o.p_fbitangents)))
                warning( ['Undefined tangents or bitangents, '...
                          'the parameterization is probably not well defined.']);
            end
        end
    end
end
function b = hasFtangents(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_ftangents));
    end
end

function val = get.fbitangents(obj)
	if isempty(obj.p_fbitangents)
        obj.updateFtangents;
	end
    val = obj.p_fbitangents;
end
function b = hasFbitangents(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_fbitangents));
    end
end

function val = get.vnormals(obj)
	if isempty(obj.p_vnormals)
%         obj.updateVnormals;
        obj.updateVtangents; % tangents are also computed by averaging face tangents, to be consistent, normals should be computed by averaging face normals
	end
    val = obj.p_vnormals;
end
% function updateVnormals(obj)
%     mask = not(obj.hasVertexIndices);
%     if any(mask)
%         obj(mask).updateVertexIndices;
%     end
%     
%     for k=1:numel(obj)
%         o = obj(k);
% 
%         if not(isempty(o.parentelement))
%             o.p_vnormals = o.parentelement.vnormals(:,o.vertexindices);
%         end
%     end
% end
function b = hasVnormals(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_vnormals));
    end
end

function val = get.vtangents(obj)
	if isempty(obj.p_vtangents)
        obj.updateVtangents;
	end
    val = obj.p_vtangents;
end
function updateVtangents(obj)
    
    mask = not(obj.hasFnormals) | not(obj.hasFtangents);
    if any(mask)
        obj(mask).updateFtangents;
    end
    
    for k=1:numel(obj)
        o = obj(k);

        if not(isempty(o.parentelement))
    
            % compute face tangents
            v = o.verts;
            f = o.faces;
            fn = o.fnormals;
            ft = o.ftangents;

            % accumulate to get vertex normals
            o.p_vnormals = [];
            o.p_vnormals(1,:) = accumarray(f(:),reshape(fn([1,1,1],:),[],1),[size(v,2),1]);
            o.p_vnormals(2,:) = accumarray(f(:),reshape(fn([2,2,2],:),[],1),[size(v,2),1]);
            o.p_vnormals(3,:) = accumarray(f(:),reshape(fn([3,3,3],:),[],1),[size(v,2),1]);
            vnormallen = sqrt(sum(o.p_vnormals.^2,1));
            o.p_vnormals = bsxfun(@times,o.p_vnormals,1./vnormallen);
            
            % accumulate to get vertex tangents
            o.p_vtangents = [];
            o.p_vtangents(1,:) = accumarray(f(:),reshape(ft([1,1,1],:),[],1),[size(v,2),1]);
            o.p_vtangents(2,:) = accumarray(f(:),reshape(ft([2,2,2],:),[],1),[size(v,2),1]);
            o.p_vtangents(3,:) = accumarray(f(:),reshape(ft([3,3,3],:),[],1),[size(v,2),1]);
            vtangentlen = sqrt(sum(o.p_vtangents.^2,1));
            o.p_vtangents = bsxfun(@times,o.p_vtangents,1./vtangentlen);

            % Gram-Schmidt orthogonalize vertex tangents (orthogonal to normal)
            o.p_vtangents = o.p_vtangents - bsxfun(@times,o.vnormals,dot(o.p_vtangents,o.vnormals));

            % compute bitangents
%             o.p_vbitangents = [];
%             o.p_vbitangents(1,:) = accumarray(f(:),reshape(fb([1,1,1],:),[],1),[size(v,2),1]);
%             o.p_vbitangents(2,:) = accumarray(f(:),reshape(fb([2,2,2],:),[],1),[size(v,2),1]);
%             o.p_vbitangents(3,:) = accumarray(f(:),reshape(fb([3,3,3],:),[],1),[size(v,2),1]);
            o.p_vbitangents = cross(o.vnormals,o.p_vtangents);
            vbitangentlen = sqrt(sum(o.p_vbitangents.^2,1));
            o.p_vbitangents = bsxfun(@times,o.p_vbitangents,1./vbitangentlen);

            if any(any(isnan(o.p_vnormals) | isinf(o.p_vnormals))) || ...
               any(any(isnan(o.p_vtangents) | isinf(o.p_vtangents))) || ...
               any(any(isnan(o.p_vbitangents) | isinf(o.p_vbitangents)))
                warning( ['Undefined vertex normals, tangents or bitangents, '...
                          'the parameterization is probably not well defined.']);
            end

%             vavgscale = accumarray(f(:),reshape(favgscale([1,1,1],:),[],1),[size(v,2),1]);
%             n = accumarray(f(:),1,[size(v,2),1]);
%             vavgscale = vavgscale ./ n;
        end
    end
end
function b = hasVtangents(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_vtangents));
    end
end

function val = get.vbitangents(obj)
	if isempty(obj.p_vbitangents)
        obj.updateVtangents;
	end
    val = obj.p_vbitangents;
end
function b = hasVbitangents(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_vbitangents));
    end
end

function val = get.fareas(obj)
	if isempty(obj.p_fareas)
        obj.updateFareas;
	end
    val = obj.p_fareas;
end
function updateFareas(obj)
    
    for k=1:numel(obj)
        o = obj(k);

        if not(isempty(o.parentelement))
            o.p_fareas = o.parentelement.fareas(:,o.faceindices);
        end
    end
end
function b = hasFareas(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_fareas));
    end
end

function val = get.boundary(obj)
	if isempty(obj.p_boundary)
        obj.updateBoundary;
	end
    val = obj.p_boundary;
end
function updateBoundary(obj)
    mask = not(obj.hasVertices);
    if any(mask)
        obj(mask).updateVertices;
    end
    mask = not(obj.hasFaces);
    if any(mask)
        obj(mask).updateFaces;
    end
    
    for k=1:numel(obj)
        o = obj(k);

        if not(isempty(o.parentelement))
            binds = trimeshBoundary(o.faces);
            o.p_boundary = cellfun(@(x) o.verts(:,x),binds,'UniformOutput',false);
        end
    end
end
function b = hasBoundary(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(iscell(obj(k).p_boundary));
    end
end

function val = get.avgtangent(obj)
	if isempty(obj.p_avgtangent)
        obj.updateAvgtangent;
	end
    val = obj.p_avgtangent;
end
function updateAvgtangent(obj)
    mask = not(obj.hasFareas);
    if any(mask)
        obj(mask).updateFareas;
    end
    mask = not(obj.hasAvgnormal);
    if any(mask)
        obj(mask).updateAvgnormal;
    end
    mask = not(obj.hasFtangents);
    if any(mask)
        obj(mask).updateFtangents;
    end
    
    for k=1:numel(obj)
        o = obj(k);

        if not(isempty(o.parentelement))
            fa = o.fareas;
            fasum = sum(fa);
%             favgscale = sum(sqrt(ftangentlen.*fbitangentlen).*fa) ./ fasum; % area preserving average
% 
            o.p_avgtangent = sum(bsxfun(@times,o.ftangents,fa),2) ./ fasum;
            len = sqrt(sum(o.p_avgtangent.^2,1));
            if len < 0.000001
                warning('Average tangent close to zero, using default average tangent [1;0;0].');
                o.p_avgtangent = [1;0;0];
            else
                o.p_avgtangent = o.p_avgtangent ./ len;
            end

            % Gram-Schmidt orthogonalize average tangent (orthogonal to normal)
            o.p_avgtangent = o.p_avgtangent - bsxfun(@times,o.avgnormal,dot(o.p_avgtangent,o.avgnormal));

            % compute bitangent
            o.p_avgbitangent = cross(o.avgnormal,o.p_avgtangent);
        end
    end
end
function b = hasAvgtangent(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_avgtangent));
    end
end

function val = get.avgbitangent(obj)
	if isempty(obj.p_avgbitangent)
        obj.updateAvgtangent;
	end
    val = obj.p_avgbitangent;
end
function b = hasAvgbitangent(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_avgbitangent));
    end
end

function val = get.avgnormal(obj)
	if isempty(obj.p_avgnormal)
        obj.updateAvgnormal;
	end
    val = obj.p_avgnormal;
end
function updateAvgnormal(obj)
    mask = not(obj.hasFareas);
    if any(mask)
        obj(mask).updateFareas;
    end
    mask = not(obj.hasFnormals);
    if any(mask)
        obj(mask).updateFnormals;
    end
    
    for k=1:numel(obj)
        o = obj(k);
       
        if not(isempty(o.parentelement))
            fa = o.fareas;
            fasum = sum(fa);
            
            o.p_avgnormal = sum(bsxfun(@times,o.fnormals,fa),2) ./ fasum;    
            len = sqrt(sum(o.p_avgnormal.^2,1));
            if len < 0.000001
                warning('Average normal close to zero, using default average normal [0;0;1].');
                o.p_avgnormal = [0;0;1];
            else
                o.p_avgnormal = o.p_avgnormal ./ len;
            end
        end
    end
end
function b = hasAvgnormal(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_avgnormal));
    end
end

function val = get.orientedbboxpose(obj)
	if isempty(obj.p_orientedbboxpose)
        obj.updateOrientedbboxpose;
	end
    val = obj.p_orientedbboxpose;
end
function updateOrientedbboxpose(obj)
    mask = not(obj.hasAvgtangent) | not(obj.hasAvgbitangent) | not(obj.hasAvgnormal);
    if any(mask)
        obj(mask).updateAvgtangent;
    end

    for k=1:numel(obj)
        o = obj(k);

        if not(isempty(o.parentelement))
            [pbbmin,~] = o.orientedboundingbox;
            o.p_orientedbboxpose = [...
                pbbmin;...
                dcm2quat([o.avgtangent,o.avgbitangent,o.avgnormal]')';...
                1;1;1;... % no scaling (would need average tangent magnitude)
                0];       % no mirroring for now
        end
    end
end
function b = hasOrientedbboxpose(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_orientedbboxpose));
    end
end

end

methods(Access=protected)
    
function dirtyImpl(obj)
    obj.dirtyImpl@SceneElementFeature;
    
    for k=1:numel(obj)
        o = obj(k);

        o.p_vertexindices = [];
        o.p_verts = [];
        o.p_faces = [];
        o.p_fnormals = [];
        o.p_ftangents = [];
        o.p_fbitangents = [];
        o.p_fareas = [];
        o.p_vnormals = [];
        o.p_vtangents = [];
        o.p_vbitangents = [];
        o.p_boundary = [];
        o.p_avgtangent = [];
        o.p_avgbitangent = [];
        o.p_avgnormal = [];
        o.p_orientedbboxpose = [];
    end
end

function updateLocalImpl(obj)
    obj.updateLocalImpl@SceneElementFeature;
    
    mask = not(obj.hasVertexIndices);
    if any(mask)
        obj(mask).updateVertexIndices;
    end
    mask = not(obj.hasVertices);
    if any(mask)
        obj(mask).updateVertices;
    end
    mask = not(obj.hasFaces);
    if any(mask)
        obj(mask).updateFaces;
    end
    mask = not(obj.hasFnormals);
    if any(mask)
        obj(mask).updateFnormals;
    end
    mask = not(obj.hasFtangents);
    if any(mask)
        obj(mask).updateFtangents;
    end
    mask = not(obj.hasFareas);
    if any(mask)
        obj(mask).updateFareas;
    end
    mask = not(obj.hasFbitangents);
    if any(mask)
        obj(mask).updateFtangents;
    end
    mask = not(obj.hasVnormals);
    if any(mask)
        obj(mask).updateVtangents;
    end
    mask = not(obj.hasVtangents);
    if any(mask)
        obj(mask).updateVtangents;
    end
    mask = not(obj.hasVbitangents);
    if any(mask)
        obj(mask).updateVtangents;
    end
    mask = not(obj.hasBoundary);
    if any(mask)
        obj(mask).updateBoundary;
    end
    mask = not(obj.hasAvgtangent);
    if any(mask)
        obj(mask).updateAvgtangent;
    end
    mask = not(obj.hasAvgbitangent);
    if any(mask)
        obj(mask).updateAvgtangent;
    end
    mask = not(obj.hasAvgnormal);
    if any(mask)
        obj(mask).updateAvgtangent;
    end
    mask = not(obj.hasOrientedbboxpose);
    if any(mask)
        obj(mask).updateOrientedbboxpose;
    end
end
    
function updateBoundingboxImpl(obj)
    for i=1:numel(obj)
        o = obj(i); % for performance
        v = o.verts;
        o.p_boundingboxmin = min(v,[],2);
        o.p_boundingboxmax = max(v,[],2);
    end
end

% % pose must be 2D
% function [center,width,height,orientation] = posedBoundingbox2DImpl(obj,framepose,featpose)
%     
%     center = zeros(2,numel(obj));
%     width = zeros(1,numel(obj));
%     height = zeros(1,numel(obj));
%     orientation = zeros(1,numel(obj));
%     for i=1:numel(obj)
%         v = posetransform(obj(i).verts,featpose(:,i),obj(i).pose3D);
%         v = posetransform(v(1:2,:),identpose2D,framepose(:,i));
% 
%         bbmin = min(v,[],2);
%         bbmax = max(v,[],2);
%         center(:,i) = (bbmin + bbmax) .* 0.5;
%         width(i) = bbmax(1) - bbmin(1);
%         height(i) = bbmax(2) - bbmin(2);
%         center(:,i) = posetransform(center(:,i),framepose(:,i),identpose2D);
%         
%         orientation(i) = framepose(3,i);
%     end
% end
    
function polyinds = inPolygonImpl(obj,poly,objcandidates)
    polyinds = cell(numel(obj),1); % not in hierarchy for now

    % todo: use projection of mesh to xy plane
end

function [d,cp] = pointDistanceImpl(obj,p,pose)
%     % approximate with distance to closest vertex for now
%     if size(p,1) < 3
%         warning('Scene:mesh','Given point is 2D, setting z to 0.');
%         p(3,:) = 0;
%     end

    if size(p,1) == 2

        d = inf(numel(obj),size(p,2));
        cp = zeros(numel(obj),size(p,2),2);
        fi = zeros(numel(obj),size(p,2));
        fbary = zeros(numel(obj),size(p,2),3);
        for i=1:numel(obj)
            % distance to projection of 3D mesh to xy-plane (= distance
            % of 3D mesh to line orthogonal to xy-plane through the
            % given point)

            maxz = max(obj.verts(3,obj(i).faces(:)));
            minz = min(obj.verts(3,obj(i).faces(:)));

            p1 = [p;minz(ones(1,size(p,2)))];
            p2 = [p;maxz(ones(1,size(p,2)))];

            if nargin >= 3 && not(isempty(pose)) && not(all(pose{i} == obj(i).parentelement.pose))
                v = posetransform(obj(i).verts,pose{i},obj(i).parentelement.pose);
            else
                v = obj(i).verts;
            end

            [dists,~,b1,b2,b3] = linesegTriangleDistance3D_mex(p1,p2,...
                v(:,obj(i).faces(1,:)),...
                v(:,obj(i).faces(2,:)),...
                v(:,obj(i).faces(3,:)));

            [d(i,:),fi(i,:)] = min(dists,[],2);
            

            % get closest points
            mininds_lin = sub2ind(size(b1),1:size(b1,1),fi(i,:));
            fbary(i,:,1) = b1(mininds_lin);
            fbary(i,:,2) = b2(mininds_lin);
            fbary(i,:,3) = b3(mininds_lin);
            cp(i,:,:) = permute(...
                bsxfun(@times,v(1:2,obj(i).faces(1,fi(i,:))),fbary(i,:,1)) + ...
                bsxfun(@times,v(1:2,obj(i).faces(2,fi(i,:))),fbary(i,:,2)) + ...
                bsxfun(@times,v(1:2,obj(i).faces(3,fi(i,:))),fbary(i,:,3)),[3,2,1]);
            % (ignore z value of closest point)
        end
%         if nargout >= 3
%             addinfo = struct(...
%                 'fi',num2cell(fi),...
%                 'fbary',mat2cell(fbary,ones(1,size(fbary,1)),ones(1,size(fbary,2)),3));
%         end

    elseif size(p,1) == 3

        d = inf(numel(obj),size(p,2));
        cp = zeros(numel(obj),size(p,2),3);
        fi = zeros(numel(obj),size(p,2));
        fbary = zeros(numel(obj),size(p,2),3);
        for i=1:numel(obj)

            if nargin >= 3 && not(isempty(pose)) && not(all(pose{i} == obj(i).parentelement.pose))
                v = posetransform(obj(i).verts,pose{i},obj(i).parentelement.pose);
            else
                if not(isempty(obj(i).scenegroup))
                    grouppose = obj(i).scenegroup.globaloriginpose;
                    v = transform3D(obj(i).verts,grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
                end
            end

            [dists,b1,b2,b3] = pointTriangleDistance3D_mex(p,...
                v(:,obj(i).faces(1,:)),...
                v(:,obj(i).faces(2,:)),...
                v(:,obj(i).faces(3,:)));

            [d(i,:),fi(i,:)] = min(dists,[],2);

            % get closest points
            mininds_lin = sub2ind(size(b1),1:size(b1,1),fi(i,:));
            fbary(i,:,1) = b1(mininds_lin);
            fbary(i,:,2) = b2(mininds_lin);
            fbary(i,:,3) = b3(mininds_lin);
            cp(i,:,:) = permute(...
                bsxfun(@times,v(:,obj(i).faces(1,fi(i,:))),fbary(i,:,1)) + ...
                bsxfun(@times,v(:,obj(i).faces(2,fi(i,:))),fbary(i,:,2)) + ...
                bsxfun(@times,v(:,obj(i).faces(3,fi(i,:))),fbary(i,:,3)),[3,2,1]);
        end
%         if nargout >= 3
%             addinfo = struct(...
%                 'fi',num2cell(fi),...
%                 'fbary',mat2cell(fbary,ones(1,size(fbary,1)),ones(1,size(fbary,2)),3));
%         end
    else
        error('Only implemented for two or three dimensions.');
    end
end

% 3D line segment
function [d,t,cp] = linesegDistanceImpl(obj,p1,p2,pose)
    if size(p1,1) == 2
        error('Not yet implemented.');
        % todo: implement a function like linesegTriangleDistance_mex (2D)
    elseif size(p1,1) == 3
        d = inf(numel(obj),size(p1,2));
        t = nan(numel(obj),size(p1,2));
        cp = zeros(numel(obj),size(p1,2),3);
        fi = zeros(numel(obj),size(p1,2));
        fbary = zeros(numel(obj),size(p1,2),3);
        
        [pelms,~,objpelmind] = unique([obj.parentelement]);
        pelmpose = pelms.getPose3D;
%         objpose = pelmpose(:,objpelmind);
        for i=1:numel(obj)

%             objpose = obj(i).parentelement.pose;
            objfaces = obj(i).faces;
            
            if nargin >= 4 && not(isempty(pose)) && not(all(pose{i} == pelmpose(:,objpelmind(i))))
                v = posetransform(obj(i).verts,pose{i},pelmpose(:,objpelmind(i)));
            else
                if not(isempty(obj(i).scenegroup))
                    grouppose = obj(i).scenegroup.globaloriginpose;
                    v = transform3D(obj(i).verts,grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
                end
            end

            [dists,t1,b1,b2,b3] = linesegTriangleDistance3D_mex(p1,p2,...
                v(:,objfaces(1,:)),...
                v(:,objfaces(2,:)),...
                v(:,objfaces(3,:)));

%             [d(i,:),mininds] = min(dists,[],2);
%             lininds = sub2ind(size(t1),1:size(t1,1),mininds');
%             t(i,:) = t1(lininds);
            
            % get element with minimum distance (and minimum t if there
            % is more than one element with the same minimum distance)
            mindist = min(dists,[],2);
            t1candidates = t1;
            t1candidates(not(bsxfun(@eq,dists,mindist))) = nan;
            [t(i,:),fi(i,:)] = min(t1candidates,[],2);
            mininds_lin = sub2ind(size(t1),1:size(t1,1),fi(i,:));

            d(i,:) = dists(mininds_lin);

            % get closest points
            fbary(i,:,1) = b1(mininds_lin);
            fbary(i,:,2) = b2(mininds_lin);
            fbary(i,:,3) = b3(mininds_lin);
            cp(i,:,:) = permute(...
                bsxfun(@times,v(:,objfaces(1,fi(i,:))),fbary(i,:,1)) + ...
                bsxfun(@times,v(:,objfaces(2,fi(i,:))),fbary(i,:,2)) + ...
                bsxfun(@times,v(:,objfaces(3,fi(i,:))),fbary(i,:,3)),[3,2,1]);
        end
%         if nargout >= 4
%             addinfo = struct(...
%                 'fi',num2cell(fi),...
%                 'fbary',mat2cell(fbary,ones(1,size(fbary,1)),ones(1,size(fbary,2)),3));
%         end
    else
        error('Only implemented for two or three dimensions.');
    end
end

function s = similarityImpl(obj,obj2)
    % just use similarity = 1 for now
    s = ones(numel(obj),numel(obj2));
end

end % methods

end % classdef
