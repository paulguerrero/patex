function gobjs = showSceneMeshes(meshes,gobjs,parent,varargin)
    
%     if not(iscell(meshes))
%         meshes = num2cell(meshes);
%     end
    
    % create patch vertices, faces, etc.
    if iscell(meshes)
        verts = cell(1,numel(meshes));
        faces = cell(1,numel(meshes));
        vnormals = cell(1,numel(meshes));
        for j=1:numel(meshes)
            verts{j} = cell(1,numel(meshes{j}));
            faces{j} = cell(1,numel(meshes{j}));
            vnormals{j} = cell(1,numel(meshes{j}));
            for k=1:numel(meshes{j})
    %         verts{j} = {meshes{j}.verts};
    %         faces{j} = {meshes{j}.faces};

                verts{j}{k} = meshes{j}(k).verts(:,meshes{j}(k).faces(:));
    %             vnormals{j}{k} = meshes{j}(k).vnormals(:,meshes{j}(k).faces(:));
                vnormals{j}{k} = reshape(cat(1,meshes{j}(k).fnormals,meshes{j}(k).fnormals,meshes{j}(k).fnormals),3,[]);
                faces{j}{k} = reshape(1:numel(meshes{j}(k).faces),3,[]);
            end
        end
    else
        verts = cell(1,numel(meshes));
        faces = cell(1,numel(meshes));
        vnormals = cell(1,numel(meshes));
        for k=1:numel(meshes)
%         verts{j} = {meshes{j}.verts};
%         faces{j} = {meshes{j}.faces};

            verts{k} = meshes(k).verts(:,meshes(k).faces(:));
%             vnormals{j}{k} = meshes{j}(k).vnormals(:,meshes{j}(k).faces(:));
            vnormals{k} = reshape(cat(1,meshes(k).fnormals,meshes(k).fnormals,meshes(k).fnormals),3,[]);
            faces{k} = reshape(1:numel(meshes(k).faces),3,[]);
        end
    end
    
    gobjs = showPatches(verts,faces,gobjs,parent,...
        'vnormals',vnormals,...
        varargin{:});
end
