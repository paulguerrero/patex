classdef SceneSurfaceGroup < SceneGroup
    
properties(SetAccess=protected)
    surface = SceneSurface.empty(1,0);
end

properties(Access=protected)
    p_surfparentpose = identpose3D;
    
    surfpositionListener = event.listener.empty;
    surforientationListener = event.listener.empty;
    surfsizeListener = event.listener.empty;
    surfmirroredListener = event.listener.empty;
    surfdeleteListener = event.listener.empty;
    surfparentListener = event.listener.empty;
    surfparentgroupListener = event.listener.empty;
end

methods(Static)
    
function n = type_s
    n = 'surfacegroup';
end

end

methods
    
% obj = SceneSurfaceGroup()
% obj = SceneSurfaceGroup(surface)
function obj = SceneSurfaceGroup(varargin)

    obj = obj@SceneGroup;
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneSurface')    
        obj.setSurface(varargin{1});
    else
        error('Invalid arguments.');
    end
end

function delete(obj)
    delete(obj.surfpositionListener(isvalid(obj.surfpositionListener)));
    delete(obj.surforientationListener(isvalid(obj.surforientationListener)));
    delete(obj.surfsizeListener(isvalid(obj.surfsizeListener)));
    delete(obj.surfmirroredListener(isvalid(obj.surfmirroredListener)));
    delete(obj.surfdeleteListener(isvalid(obj.surfdeleteListener)));
    delete(obj.surfparentListener(isvalid(obj.surfparentListener)));
    delete(obj.surfparentgroupListener(isvalid(obj.surfparentgroupListener)));
end

function setSurface(obj,surf)
    obj.surface = surf;
    obj.updateParentgroup;
    obj.updateOriginpose;
    
    delete(obj.surfpositionListener(isvalid(obj.surfpositionListener)));
    delete(obj.surforientationListener(isvalid(obj.surforientationListener)));
    delete(obj.surfsizeListener(isvalid(obj.surfsizeListener)));
    delete(obj.surfmirroredListener(isvalid(obj.surfmirroredListener)));
    delete(obj.surfdeleteListener(isvalid(obj.surfdeleteListener)));
    delete(obj.surfparentListener(isvalid(obj.surfparentListener)));
    delete(obj.surfparentgroupListener(isvalid(obj.surfparentgroupListener)));
    
    if not(isempty(obj.surface))
        obj.surfdeleteListener = obj.surface.addlistener('ObjectBeingDestroyed',@(src,evt) obj.surfaceDeleted);
        obj.surfdeleteListener.Enabled = false;
        obj.surfparentListener = obj.surface.addlistener('parentelement','PostSet',@(src,evt) obj.surfaceparentChanged);
        if not(isempty(obj.surface.parentelement))
            obj.surfpositionListener = obj.surface.parentelement.addlistener('position','PostSet',@(src,evt) obj.surfaceposeChanged);
            obj.surforientationListener = obj.surface.parentelement.addlistener('orientation','PostSet',@(src,evt) obj.surfaceposeChanged);
            obj.surfsizeListener = obj.surface.parentelement.addlistener('size','PostSet',@(src,evt) obj.surfaceposeChanged);
            obj.surfmirroredListener = obj.surface.parentelement.addlistener('mirrored','PostSet',@(src,evt) obj.surfaceposeChanged);
            obj.surfparentgroupListener = obj.surface.parentelement.addlistener('ScenegroupChanged',@(src,evt) obj.surfaceparentgroupChanged);
        end
    end
end

function b = hasSurface(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        o = obj(i);
        b(i) = not(isempty(o.surface));
    end
end

function surfaceparentChanged(obj)
    obj.updateParentgroup;
    
    delete(obj.surfpositionListener(isvalid(obj.surfpositionListener)));
    delete(obj.surforientationListener(isvalid(obj.surforientationListener)));
    delete(obj.surfsizeListener(isvalid(obj.surfsizeListener)));
    delete(obj.surfmirroredListener(isvalid(obj.surfmirroredListener)));
    delete(obj.surfparentgroupListener(isvalid(obj.surfparentgroupListener)));
    if not(isempty(obj.surface.parentelement))
        obj.surfpositionListener = obj.surface.parentelement.addlistener('position','PostSet',@(src,evt) obj.surfaceposeChanged);
        obj.surforientationListener = obj.surface.parentelement.addlistener('orientation','PostSet',@(src,evt) obj.surfaceposeChanged);
        obj.surfsizeListener = obj.surface.parentelement.addlistener('size','PostSet',@(src,evt) obj.surfaceposeChanged);
        obj.surfmirroredListener = obj.surface.parentelement.addlistener('mirrored','PostSet',@(src,evt) obj.surfaceposeChanged);
        obj.surfparentgroupListener = obj.surface.parentelement.addlistener('ScenegroupChanged',@(src,evt) obj.surfaceparentgroupChanged);
    end
end

function surfaceparentgroupChanged(obj)
    obj.updateParentgroup;
end

function surfaceposeChanged(obj)
    newsurfparentpose = obj.surface.parentelement.pose3D;
    
    obj.setOriginpose(posetransform(obj.originpose,newsurfparentpose,obj.p_surfparentpose));
    
    obj.p_surfparentpose = newsurfparentpose;
end

function surfaceDeleted(obj)
    if not(isempty(obj.parentgroup))
        if isvalid(obj.parentgroup)
            obj.parentgroup.removeChildgroups(obj);
        end
    elseif not(isempty(obj.scene)) && isvalid(obj.scene)
        obj.scene.removeRootgroups(obj);
        obj.delete;
    end
end

function updateParentgroup(obj)
    if isempty(obj.surface)
        newparent = SceneGroup.empty;
    else
        newparent = obj.surface.scenegroup;
    end
    
    obj.setParentgroup(newparent);
end

function updateOriginpose(obj)
    surfaces = [obj.surface];
    obj.setOriginpose([surfaces.orientedbboxpose]);
    
    for i=1:numel(obj)
        o = obj(i);
        o.p_surfparentpose = o.surface.parentelement.pose3D;
    end
end

end % methods

methods(Static)
    
function surfgroups = meshSurfaceGroups(mesh)
    
    surfgroups = cell(1,numel(mesh.surfaces));
    
    % wirhout a scenegroup, it might have surface groups, but these are
    % impossible to get (only connected through event listeners)
    if isempty(mesh.surfaces) || isempty(mesh.scenegroup)
        return;
    end
    
    parentgrp = mesh.scenegroup;
    
    surfgroupmask = false(1,numel(parentgrp.childgroups));
    for i=1:numel(parentgrp.childgroups)
        surfgroupmask(i) = isa(parentgrp.childgroups(i),'SceneSurfaceGroup');
    end
    siblingsurfgrps = parentgrp.childgroups(surfgroupmask);
    
    % only keep surface groups that are attached to a surface
    siblingsurfgrps(not(siblingsurfgrps.hasSurface)) = [];
    
    % only keep surface groups that are attached to one of the mesh surfaces
    [hasgroup,ind] = ismember(mesh.surfaces,[siblingsurfgrps.surface]);
    
    surfgroups(hasgroup) = num2cell(siblingsurfgrps(ind(hasgroup)));
end
    
end

end
