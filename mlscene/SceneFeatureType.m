classdef SceneFeatureType
    
properties(SetAccess=protected)
    classname = '';
end

properties(Dependent,SetAccess=private)
    name;
end

properties(Access=protected)
    % to speed up access (so I don't have to call str2func with the classname every time)
    constructorhandle = function_handle.empty;
    namehandle = function_handle.empty;
end

methods

function obj = SceneFeatureType(classname)

    if nargin == 0
        % do noting (for empty list)
    elseif nargin == 1

        obj.classname = classname;

        % check if the given classname is a class derived from
        % SceneFeature and has all necessary static functions
        % (the latter is guaranteed when being derived from
        % SceneFeature, but it is only checked when the
        % class is actually instantiated or when the static functions
        % are called, so better to check right away)

        if not(exist(obj.classname,'class'))
            error(['Cannot find the class ''',obj.classname,'''.']);
        end
        if not(any(strcmp(...
                'SceneFeature',...
                superclasses(obj.classname))))
            error('Class is not a SceneFeature.');
        end

        classmethods = methods(obj.classname);

        if not(all(ismember({...
                'type_s',...
                },classmethods)));
            error('Class does not have all necessary static functions.');
        end

        obj.constructorhandle = str2func([obj.classname]);
        obj.namehandle = str2func([obj.classname,'.type_s']);
    else
        error('Invalid arguments.');
    end
end

function val = get.name(obj)
    val = obj.namehandle();
end

function instance = createInstance(obj,varargin)
    instance = obj.constructorhandle(varargin{:});
end

end

methods(Static)

% factory methods (create relation element of correct class given a relation name)
function [name2type,classname2type] = library
    persistent name2type_singleton;
    persistent classname2type_singleton;

    if not(isa(name2type_singleton,'containers.Map'))
        name2type_singleton = containers.Map('KeyType','char','ValueType','any');
        classname2type_singleton = containers.Map('KeyType','char','ValueType','any');

        % add all known relation types
        SceneFeatureType.addFeattype('ScenePoint');
        SceneFeatureType.addFeattype('ScenePolyline');
        SceneFeatureType.addFeattype('ScenePolygon');
        SceneFeatureType.addFeattype('ScenePolygonset');
        SceneFeatureType.addFeattype('SceneComplexPolygon');
        SceneFeatureType.addFeattype('SceneMesh');
        SceneFeatureType.addFeattype('SceneCorner');
        SceneFeatureType.addFeattype('SceneBoundarySegment');
        SceneFeatureType.addFeattype('SceneEdge');
        SceneFeatureType.addFeattype('SceneSurface');
    end
    
    name2type = name2type_singleton;
    classname2type = classname2type_singleton;
end

function addFeattype(classname)
    if isempty(classname)
        error('Empty class name.');
    end
    
    [name2type,classname2type] = SceneFeatureType.library;
    
    if any(strcmp(classname,classname2type.keys))
        error(['The feature ',classname,' is already in the library.']);
    end
    
%     existingids = id2type.keys;
%     existingids = [existingids{:}];
%     newid = missingint(existingids);
    feattype = SceneFeatureType(classname);
    typename = feattype.name;
    if isempty(typename)
        delete(feattype(isvalid(feattype)));
        error('Feature type name must not be empty.');
    end
    if any(strcmp(typename,name2type.keys))
        delete(feattype(isvalid(feattype)));
        error(['The feature ',typename,' is already in the library.']);
    end
    
    name2type(feattype.name) = feattype; %#ok<NASGU>
    classname2type(feattype.classname) = feattype; %#ok<NASGU>
end

function removeFeattype(classname)
    if isempty(classname)
        error('Empty class name.');
    end
    
    [name2type,classname2type] = SceneFeatureType.library;
    feattype = classname2type(classname);
    name2type.remove(feattype.name);
    classname2type.remove(feattype.classname);
end

function feattype = fromClassname(classname)
    if isempty(classname)
        feattype = SceneFeatureType.empty;
        return;
    end
    
    [~,classname2type] = SceneFeatureType.library;

    if not(all(classname2type.isKey(classname)))
        error('Feature type not found.');
    end
    if iscell(classname)
        feattype = classname2type.values(classname);
        feattype = [feattype{:}];
    else
        feattype = classname2type(classname);
    end
end

function feattype = fromName(name)
    if isempty(name)
        feattype = SceneFeatureType.empty;
        return;
    end
    
    [name2type,~] = SceneFeatureType.library;

    if not(all(name2type.isKey(name)))
        error('Feature type not found.');
    end
    if iscell(name)
        feattype = name2type.values(name);
        feattype = [feattype{:}];
    else
        feattype = name2type(name);
    end
end

% function feattype = fromID(id)
%     if isempty(id)
%         feattype = SceneFeatureType.empty;
%         return;
%     elseif numel(id) > 1
%         id = num2cell(id);
%     end
%     
%     [id2type,~,~] = SceneFeatureType.library;
% 
%     if not(all(id2type.isKey(id)))
%         error('Feature type not found.');
%     end
%     if iscell(id)
%         feattype = id2type.values(id);
%         feattype = [feattype{:}];
%     else
%         feattype = id2type(id);
%     end
% end

% elm = create(featname,varargin)
function elm = create(featname,varargin)
    reltype = SceneFeatureType.fromName(featname);
    elm = reltype.createInstance(varargin{:});
end

end

end

