function handle = showScenePolygonDetails(polys,handle,parent,stackz)

    if nargin < 4 || isempty(stackz)
        stackz = 0;
    end
    
    if not(iscell(polys))
        polys = num2cell(polys);
    end

    if size(handle,1) ~= 6 || size(handle,2) ~= numel(polys) || not(all(isgraphics(handle(:))))
        delete(handle(isgraphics(handle)));
        handle = gobjects(6,numel(polys));
        
        for i=1:numel(polys)
            handle(1,i) = patch([0,0],[0,0],0,...
                'Parent',parent(i),...        
                'FaceColor','none',...
                'LineStyle','none',...
                'Marker','o',...
                'MarkerFaceColor','blue',...
                'Visible','off');
            handle(1,i).addprop('Stackz').SetMethod = @changeStackz;
            handle(1,i).Stackz = stackz;
            handle(2,i) = patch([0,0],[0,0],0,...
                'Parent',parent(i),...        
                'FaceColor','none',...
                'EdgeColor','blue',...
                'LineWidth',1,...
                'Visible','off');
            handle(2,i).addprop('Stackz').SetMethod = @changeStackz;
            handle(2,i).Stackz = stackz;
            handle(3,i) = patch([0,0],[0,0],0,...
                'Parent',parent(i),...        
                'FaceColor','none',...
                'EdgeColor','blue',...
                'LineWidth',2,...
                'Visible','off');
            handle(3,i).addprop('Stackz').SetMethod = @changeStackz;
            handle(3,i).Stackz = stackz;
            handle(4,i) = patch([0,0],[0,0],0,...
                'Parent',parent(i),...        
                'FaceColor','none',...
                'EdgeColor','blue',...
                'LineWidth',1,...
                'Visible','off');
            handle(4,i).addprop('Stackz').SetMethod = @changeStackz;
            handle(4,i).Stackz = stackz;
            handle(5,i) = patch([0,0],[0,0],0,...
                'Parent',parent(i),...        
                'FaceColor','none',...
                'LineStyle','none',...
                'Marker','o',...
                'MarkerFaceColor','blue',...
                'Visible','off');
            handle(5,i).addprop('Stackz').SetMethod = @changeStackz;
            handle(5,i).Stackz = stackz;
            handle(6,i) = patch([0,0],[0,0],0,...
                'Parent',parent(i),...        
                'FaceColor','none',...
                'EdgeColor','green',...
                'LineWidth',1,...
                'Visible','off');
            handle(6,i).addprop('Stackz').SetMethod = @changeStackz;
            handle(6,i).Stackz = stackz;
        end
    end
    
    for j=1:numel(polys)
        % show centroids
        if get(handle(1,j),'Parent') ~= parent(j)
            set(handle(1,j),'Parent',parent(j));
        end
        centers = [polys{j}.center];
        set(handle(1,j),'XData',centers(1,:),'YData',centers(2,:),'Visible','on');

        % show angles
        if get(handle(2,j),'Parent') ~= parent(j)
            set(handle(2,j),'Parent',parent(j));
        end
        veclen = [polys{j}.radius] .* 0.8;
        angles = polys{j}.getOrientation;
        positions = polys{j}.getPosition;
        [xdata,ydata] = pol2cart(angles,ones(1,numel(angles)).*veclen);
        xdata = xdata + positions(1,:);
        ydata = ydata + positions(2,:);
        xdata = [positions(1,:);xdata;nan(1,numel(xdata))]; %#ok<AGROW>
        ydata = [positions(2,:);ydata;nan(1,numel(ydata))]; %#ok<AGROW>
        xdata = xdata(:)';
        ydata = ydata(:)';
        set(handle(2,j),'XData',xdata','YData',ydata,'Visible','on');

        % show convex hulls
        if get(handle(3,j),'Parent') ~= parent(j)
            set(handle(3,j),'Parent',parent(j));
        end
        xdata = [];
        ydata = [];
        for i=1:numel(polys{j})
            if not(isempty(polys{j}(i).convexhull))
                [x,y] = closePolygons(polys{j}(i).convexhull(1,:),...
                                      polys{j}(i).convexhull(2,:));

                xdata = [xdata,x,nan]; %#ok<AGROW>
                ydata = [ydata,y,nan]; %#ok<AGROW>
            end
        end
        set(handle(3,j),'XData',xdata','YData',ydata,'Visible','on');

        % show scales
        if get(handle(4,j),'Parent') ~= parent(j)
            set(handle(4,j),'Parent',parent(j));
        end
        polyradii = [polys{j}.radius];
        xdata = [];
        ydata = [];
        for i=1:numel(polys{j})
            [cx,cy] = polygonCircle(positions(1,i),positions(2,i),polyradii(i),64);
            xdata = [xdata,cx([1:end,1]),nan]; %#ok<AGROW>
            ydata = [ydata,cy([1:end,1]),nan]; %#ok<AGROW>
        end    
        set(handle(4,j),'XData',xdata,'YData',ydata,'Visible','on');

        % show corners
        if get(handle(5,j),'Parent') ~= parent(j)
            set(handle(5,j),'Parent',parent(j));
        end
        xdata = [];
        ydata = [];
        for i=1:numel(polys{j})
            xdata = [xdata,polys{j}(i).verts(1,[polys{j}(i).corners.cornervertex])]; %#ok<AGROW>
            ydata = [ydata,polys{j}(i).verts(2,[polys{j}(i).corners.cornervertex])]; %#ok<AGROW>
        end
        set(handle(5,j),'XData',xdata,'YData',ydata,'Visible','on');
        
        % show oriented boundingboxes
        if get(handle(6,j),'Parent') ~= parent(j)
            set(handle(6,j),'Parent',parent(j));
        end
        xdata = [];
        ydata = [];
        for i=1:numel(polys{j})
            obbox = polys{j}(i).orientedbbox;
            verts = [obbox(:,1),obbox(:,1)+obbox(:,3),obbox(:,1)+obbox(:,3)+obbox(:,2),obbox(:,1)+obbox(:,2)];
            xdata = [xdata,verts(1,[1:end,1]),nan]; %#ok<AGROW>
            ydata = [ydata,verts(2,[1:end,1]),nan]; %#ok<AGROW>
        end
        set(handle(6,j),'XData',xdata,'YData',ydata,'Visible','on');
    end
end
