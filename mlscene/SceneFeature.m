% A general feature of the scene
classdef (Abstract) SceneFeature < handle & matlab.mixin.Heterogeneous

properties(SetAccess=protected)
    % these two cannot be static, as static properties can not be queried
    % efficiently on heterogeneous arrays (would only get the static
    % property of the common base class)
    type = [];
    dimension = []; % manifold dimension
    name = '';
end

properties(Dependent,SetAccess=private)
    labels;
    scenegroup;
    boundingboxmin;
    boundingboxmax;
    labelStrs;
end

properties(Access={?SceneFeature,?SceneImporter,?SceneExporter})
    p_boundingboxmin = [];
    p_boundingboxmax = [];
end

methods
    
function obj = SceneFeature
    obj.type = obj.type_s;
    obj.dimension = obj.dimension_s;
end

function setName(obj,name)
    obj.name = name;
end

function copyFrom(obj,obj2)
   
    obj.name = obj2.name;
    
    obj.p_boundingboxmin = obj2.p_boundingboxmin;
    obj.p_boundingboxmax = obj2.p_boundingboxmax;
end
    
function val = get.labels(obj)
    val = obj.getLabels;
    val = val{:};
end

function val = get.labelStrs(obj)
    val = obj.getLabelStrs;
    val = val{:};
end

function val = get.scenegroup(obj)
    val = obj.getScenegroup;
    val = val{:};
end

function val = get.boundingboxmin(obj)
    if isempty(obj.p_boundingboxmin)
        obj.updateBoundingbox;
    end
    
    val = obj.p_boundingboxmin;
end
function val = get.boundingboxmax(obj)
    if isempty(obj.p_boundingboxmax)
        obj.updateBoundingbox
    end
    
    val = obj.p_boundingboxmax;
end

end

methods(Sealed)
% need sealing so i can call these methods on heterogenous arrays of
% Scene features
function b = eq(obj,obj2)
    b = obj.eq@handle(obj2);
end

function b = ne(obj,obj2)
    b = obj.ne@handle(obj2);
end

function b = hasScenegroup(obj)
    if numel(obj) == 1
        b = obj.hasScenegroupImpl;
    else
        b = false(1,numel(obj));
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                b(objinds{i}) = obj(objinds{i}).hasScenegroupImpl;
            end
        end
    end
end

function setScenegroup(obj,sg)
    if numel(obj) == 1
        obj.setScenegroupImpl(sg);
    else
        if not(iscell(sg))
            sg = {sg};
        end
        [types,objinds] = SceneFeature.groupByType(obj);
        if numel(sg) == 1
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    obj(objinds{i}).setScenegroupImpl(sg);
                end
            end
        else
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    obj(objinds{i}).setScenegroupImpl(sg(objinds{i}));
                end
            end
        end
    end
end

function g = getScenegroup(obj)
    if numel(obj) == 1
        g = obj.getScenegroupImpl;
    else
        g = cell(1,numel(obj));
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                g(objinds{i}) = obj(objinds{i}).getScenegroupImpl;
            end
        end
    end
end

function b = hasLabels(obj)
    if numel(obj) == 1
        b = obj.hasLabelsImpl;
    else
        b = false(1,numel(obj));
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                b(objinds{i}) = obj(objinds{i}).hasLabelsImpl;
            end
        end
    end
end

function setLabels(obj,l)
%     if ischar(l)
%         l = {l};
%     end
    
    if iscellstr(l) || isreal(l)
        l = repmat({l},1,numel(obj));
    end
    
    if numel(obj) == 1
        obj.setLabelsImpl(l);
    else
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                obj(objinds{i}).setLabelsImpl(l(objinds{i}));
            end
        end
    end
end

function l = getLabels(obj)
    if numel(obj) == 1
        l = obj.getLabelsImpl;
    else
        l = cell(1,numel(obj));
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                l(objinds{i}) = obj(objinds{i}).getLabelsImpl;
            end
        end
    end
end

function l = getLabelStrs(obj)
    % get current labels
    lbls = obj.getLabels;
    
    % convert to string
    if numel(obj) == 1
        if not(isempty(obj.scenegroup)) && not(isempty(obj.scenegroup.scene)) 
            l = SceneFeature.labelsAsStr(lbls,obj.scenegroup.scene.categories);
        else
            l = SceneFeature.labelsAsStr(lbls);
        end
    else
        l = cell(1,numel(obj));
        
        if not(isempty(obj))

            % make sure that all features are in a Scene
            groups = obj.getScenegroup;
            hasscenemask = not(cellisempty_mex(groups,true));
            hassceneinds = find(hasscenemask);
            groups = [groups{hasscenemask}];
            for k=1:numel(groups)
                hasscenemask(hassceneinds(k)) = not(isempty(groups(k).scene));
            end
            
            % sort by scene
            if any(hasscenemask)
                scenes = [groups(hasscenemask(hassceneinds)).scene];
                [scenes,~,objsceneind(hasscenemask)] = unique(scenes);
            else
                scenes = [];
            end
            clear groups hassceneinds;
            
%             hasscenemask = false(1,numel(obj));
%             for k=1:numel(obj)
%                 hasscenemask(k) = (not(isempty(obj(k).scenegroup)) && not(isempty(obj(k).scenegroup.scene)));
%             end
% 
%             if any(hasscenemask)
%                 groups = obj(hasscenemask).getScenegroup;
%                 groups = [groups{:}];
%                 scenes = [groups.scene];
%                 [scenes,~,objsceneind(hasscenemask)] = unique(scenes);
%             else
%                 scenes = [];
%             end
            
            objsceneind(not(hasscenemask)) = numel(scenes)+1;
            sceneobjinds = array2cell_mex(1:numel(obj),objsceneind,numel(scenes)+1,2);

            % convert for each scene separately (scenes have different categories)
            for i=1:numel(scenes)
                l(sceneobjinds{i}) = SceneFeature.labelsAsStr(lbls(sceneobjinds{i}),scenes(i).categories);
            end
            % and convert labels for the features that are not in a Scene
            l(sceneobjinds{end}) = SceneFeature.labelsAsStr(lbls(sceneobjinds{end}));
        end
    end
end

function l = getLabelInds(obj)
    % get current labels
    lbls = obj.getLabels;
    
    % convert to string
    if numel(obj) == 1
        if not(isempty(obj.scenegroup)) && not(isempty(obj.scenegroup.scene)) 
            l = SceneFeature.labelsAsInd(lbls,obj.scenegroup.scene.categories);
        else
            l = SceneFeature.labelsAsInd(lbls);
        end
    else
        l = cell(1,numel(obj));
        
        if not(isempty(obj))
        
            % make sure that all features are in a Scene
            hasscenemask = false(1,numel(obj));
            for k=1:numel(obj)
                hasscenemask(k) = (not(isempty(obj(k).scenegroup)) && not(isempty(obj(k).scenegroup.scene)));
            end

            % sort by scene
            if any(hasscenemask)
                groups = obj(hasscenemask).getScenegroup;
                groups = [groups{:}];
                scenes = [groups.scene];
                [scenes,~,objsceneind(hasscenemask)] = unique(scenes);
            else
                scenes = [];
            end
            objsceneind(not(hasscenemask)) = numel(scenes)+1;
            sceneobjinds = array2cell_mex(1:numel(obj),objsceneind,numel(scenes)+1,2);

            % convert for each scene separately (scenes have different categories)
            for i=1:numel(scenes)
                l(sceneobjinds{i}) = SceneFeature.labelsAsInd(lbls(sceneobjinds{i}),scenes(i).categories);
            end
            % and convert labels for the features that are not in a Scene
            l(sceneobjinds{end}) = SceneFeature.labelsAsInd(lbls(sceneobjinds{end}));
        end
    end
end

function b = labelsAreStr(obj)
    lbls = obj.getLabels;
    
    b = false(1,numel(lbls));
    for i=1:numel(lbls)
        b(i) = iscellstr(lbls{i});
    end
end

function b = labelsAreInd(obj)
    lbls = obj.getLabels;
    
    b = false(1,numel(lbls));
    for i=1:numel(lbls)
        b(i) = all(arrayfun(@isreal,lbls{i}));
    end
end

function labelsToInd(obj)
    obj.setLabels(obj.getLabelInds);
end

function labelsToStr(obj)
    obj.setLabels(obj.getLabelStrs);
end

function dirty(obj)
    if numel(obj) == 1
        obj.dirtyImpl;
    else
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                obj(objinds{i}).dirtyImpl;
            end
        end
    end
    
    for k=1:numel(obj)
        o = obj(k);
        o.p_boundingboxmin = [];
        o.p_boundingboxmax = [];
    end

    obj.clearContext;
end

function update(obj)
    obj.updateLocal;
    
    obj.updateContext;
end

% just local properties (not the context)
function updateLocal(obj)
    if numel(obj) == 1
        obj.updateLocalImpl;
    else
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                obj(objinds{i}).updateLocalImpl;
            end
        end
    end
    
    mask = not(obj.hasBoundingboxmin) | not(obj.hasBoundingboxmax);
    if any(mask)
        obj(mask).updateBoundingbox;
    end
end

function [bbmin,bbmax,bbdiag] = boundingbox(obj)
    
    bbmin = obj.getBoundingboxmin;
    bbmax = obj.getBoundingboxmax;
    
    if not(isempty(obj))
        % pad 2D bounding boxes with zeros if there are 3D bounding boxes present
        dim = zeros(1,numel(bbmin));
        for i=1:numel(bbmin)
            dim(i) = size(bbmin{i},1);
        end
        if not(all(dim == dim(1)))
            maxdim = max(dim);
            inds = find(dim < maxdim);
            for i=inds
                bbmin{i}(end+1:maxdim,:) = 0;
                bbmax{i}(end+1:maxdim,:) = 0;
            end
        end
    end
    
    bbmin = [bbmin{:}];
    bbmax = [bbmax{:}];
    
    if nargout >=3
        bbdiag = sqrt(sum((bbmax-bbmin).^2,1));
    end
end
function val = getBoundingboxmin(obj)
    mask = not(obj.hasBoundingboxmin);
    if any(mask)
        obj(mask).updateBoundingbox;
    end
    val = {obj.p_boundingboxmin};
end
function val = getBoundingboxmax(obj)
    mask = not(obj.hasBoundingboxmax);
    if any(mask)
        obj(mask).updateBoundingbox;
    end
    val = {obj.p_boundingboxmax};
end
function b = hasBoundingboxmin(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_boundingboxmin));
    end
end
function b = hasBoundingboxmax(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_boundingboxmax));
    end
end
function b = hasBoundingbox(obj)
    b = obj.hasBoundingboxmin & obj.hasBoundingboxmax;
end
function updateBoundingbox(obj)
    if numel(obj) == 1
        obj.updateBoundingboxImpl;
    else
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                obj(objinds{i}).updateBoundingboxImpl;
            end
        end
    end
end

% function b = inContext(obj,feats)
%     if isempty(obj) || isempty(feats)
%         b = false(numel(obj),numel(feats));
%     else
%         % find elements with bounding boxes that overlap this bounding box
%         [featbbmin,featbbmax] = feats.boundingbox;
%         [mybbmin,mybbmax] = obj.boundingbox;
%         
%         % only use projection to xy plane (hierarchy is 2D)
%         featbbmin = featbbmin(1:2,:);
%         featbbmax = featbbmax(1:2,:);
%         mybbmin = mybbmin(1:2,:);
%         mybbmax = mybbmax(1:2,:);
% 
% %         bbdim = max(size(mybbmin,1),size(featbbmin,1));
% %         
% %         % pad z coordinate with zeros if necessary
% %         if size(featbbmin,1) < bbdim
% %             featbbmin(end:bbdim,:) = 0;
% %             featbbmax(end:bbdim,:) = 0;
% %         end
% %         if size(mybbmin,1) < bbdim
% %             mybbmin(end:bbdim,:) = 0;
% %             mybbmax(end:bbdim,:) = 0;
% %         end
% % 
% %         directparents = {obj.directparentpolygons};
% %         directparentbbmin = cellfun(@(x) [x.boundingboxmin], directparents,'UniformOutput',false);
% %         directparentbbmax = cellfun(@(x) [x.boundingboxmax], directparents,'UniformOutput',false);
% %         directparentbbmin = cellfun(@(x) ... % pad z-coord with zeros
% %             [x;zeros(bbdim-size(x,1),size(x,2))], directparentbbmin,'UniformOutput',false);
% %         directparentbbmax = cellfun(@(x) ... % pad z-coord with zeros
% %             [x;zeros(bbdim-size(x,1),size(x,2))], directparentbbmax,'UniformOutput',false);
%         
%         % also add the direct parent bounding box to get the direct siblings
%         mask = not(arrayfun(@(x) isa(x.p_parentpolygons,'ScenePolygon'),obj));
%         obj(mask).updateParentpolygons;
%         directparents = {obj.directparentpolygons};
%         mybbmin = cell2mat(cellfun(@(x,y) ...
%             {min([mybbmin(:,y),x.boundingboxmin],[],2)}, ...
%             directparents, num2cell(1:numel(directparents))));
%         mybbmax = cell2mat(cellfun(@(x,y) ...
%             {max([mybbmax(:,y),x.boundingboxmax],[],2)}, ...
%             directparents,num2cell(1:numel(directparents))));
% 
%         b = bsxfun(@lt,featbbmin(1,:),mybbmax(1,:)') & bsxfun(@lt,featbbmin(2,:),mybbmax(2,:)') & ...
%             bsxfun(@gt,featbbmax(1,:),mybbmin(1,:)') & bsxfun(@gt,featbbmax(2,:),mybbmin(2,:)');
%     end
% end

function updateContext(obj)
    
    if numel(obj) == 1
        obj.updateContextImpl;
    else
        % sort features by type
%         objtypes = [obj.type];
%         [types,~,objtypes] = unique(objtypes);
%         objinds = array2cell_mex(1:numel(obj),objtypes,numel(typeids),2);
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                obj(objinds{i}).updateContextImpl;
            end
        end
    end
    
%     obj.updateHierarchy;
end

function clearContext(obj)
    if numel(obj) == 1
        obj.updateContextImpl;
    else
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                obj(objinds{i}).clearContextImpl;
            end
        end
    end
    
%     obj.clearHierarchy;
end

% relations 0 and 2 are never returned, because only relevant poly indices
% (i.e. feature is inside poly or intersecting) are returned
% relation: 0: feature and poly are separate
% relation: 1: feature completely inside poly
% relation: 2: poly completely inside feature
% relation: 3: feature and poly are intersecting
function [polyinds,relation] = inPolygon(obj,poly,objcandidateinds)
    if numel(obj) == 1
        if nargin < 3 || isempty(objcandidateinds)
            [polyinds,relation] = obj.inPolygonImpl(poly);
        else
            [polyinds,relation] = obj.inPolygonImpl(poly,objcandidateinds);
        end
    else
        polyinds = cell(numel(obj),1);
        if nargout >= 2
            relation = cell(numel(obj),1);
        end
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                if nargin < 3 || isempty(objcandidateinds)
                    [polyinds(objinds{i}),relation(objinds{i})] = ...
                        obj(objinds{i}).inPolygonImpl(poly);
                else
                    [polyinds(objinds{i}),relation(objinds{i})] = ...
                        obj(objinds{i}).inPolygonImpl(poly,objcandidateinds(objinds{i}));
                end
            end
        end
    end
end

function [d,cp] = pointDistance(obj,p,pose)
    if numel(obj) == 1
        if nargin < 3 || isempty(pose)
            [d,cp] = obj.pointDistanceImpl(p);
        else
            [d,cp] = obj.pointDistanceImpl(p,pose);
        end
    else
        d = inf(numel(obj),size(p,2));
        cp = zeros(numel(obj),size(p,2),size(p,1));
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                if nargin < 3 || isempty(pose)
                    [d(objinds{i},:),cp(objinds{i},:,:)] = ...
                        obj(objinds{i}).pointDistanceImpl(p);
                else
                    [d(objinds{i},:),cp(objinds{i},:,:)] = ...
                        obj(objinds{i}).pointDistanceImpl(p,pose(objinds{i}));
                end
            end
        end
    end
end
function [d,t,cp] = linesegDistance(obj,p1,p2,pose)
    if numel(obj) == 1
        if nargin < 4 || isempty(pose)
            [d,t,cp] = obj.linesegDistanceImpl(p1,p2);
        else
            [d,t,cp] = obj.linesegDistanceImpl(p1,p2,pose);
        end
    else
        d = inf(numel(obj),size(p1,2));
        t = nan(numel(obj),size(p1,2));
        cp = zeros(numel(obj),size(p1,2),size(p1,1));
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                if nargin < 4 || isempty(pose)
                    [d(objinds{i},:),t(objinds{i},:),cp(objinds{i},:,:)] = ...
                        obj(objinds{i}).linesegDistanceImpl(p1,p2);
                else
                    [d(objinds{i},:),t(objinds{i},:),cp(objinds{i},:,:)] = ...
                        obj(objinds{i}).linesegDistanceImpl(p1,p2,pose(objinds{i}));
                end
            end
        end
    end
end

function s = similarity(obj,obj2)
    if numel(obj) == 1
        s = obj.similarityImpl(obj2);
    else
        s = zeros(numel(obj),numel(obj2));
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                s(objinds{i},:) = obj(objinds{i}).similarityImpl(obj2);
            end
        end
    end
end

function b = isElement(obj)
    if numel(obj) == 1
        b = obj.isElementImpl;
    else
        b = false(1,numel(obj));
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                b(:,objinds{i}) = obj(objinds{i}).isElementImpl;
            end
        end
    end
end

function b = isElementFeature(obj)
    if numel(obj) == 1
        b = obj.isElementFeatureImpl;
    else
        b = false(1,numel(obj));
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                b(:,objinds{i}) = obj(objinds{i}).isElementFeatureImpl;
            end
        end
    end
end

end

methods(Static)

% labels can be an array of labels (numeric array of array of strings), or
% a cell array thereof
% output has the same format but with strings as labels
function lstr = labelsAsStr(labels,categories)
    if nargin < 2
        categories = cell(1,0);
    end
    
    if isreal(labels)
        if any(labels > numel(categories) | labels < 1)
            error('Invalid label index for the given categories.');
        end
        lstr = categories(labels);
    elseif iscellstr(labels)
        lstr = labels;
    elseif iscell(labels)
        lstr = cell(1,numel(labels));
        for i=1:numel(labels)
            if isreal(labels{i})
                if any(labels{i} > numel(categories) | labels{i} < 1)
                    error('Invalid label index for the given categories.');
                end
                lstr{i} = categories(labels{i});
            elseif iscellstr(labels{i})
                lstr{i} = labels{i};
            else
                error('Invalid label format.');
            end
        end
    else
        error('Invalid label format.');
    end
end

% labels can be an array of labels (numeric array of array of strings), or
% a cell array thereof
% output has the same format but with indices into the scene categories as
% labels
function lind = labelsAsInd(labels,categories)
    if nargin < 2
        categories = cell(1,0);
    end
    
    if isreal(labels)
        lind = labels;
    elseif iscellstr(labels)
%         if nargin < 2
%             error('No categories available, cannot convert labels to index.');
%         end
        [hascat,ind] = ismember(labels,categories);
        if not(all(hascat))
            error('Labels not found in the categories.')
        end
        lind = ind;
    elseif iscell(labels)
        lind = cell(1,numel(labels));
        for i=1:numel(labels)
            if isreal(labels{i})
                lind{i} = labels{i};
            elseif iscellstr(labels{i})
%                 if nargin < 2
%                     error('No categories available, cannot convert labels to index.');
%                 end
                [hascat,ind] = ismember(labels{i},categories);
                if not(all(hascat))
                    error('Labels not found in the categories.')
                end
                lind{i} = ind;
            else
                error('Invalid label format.');
            end
        end
    else
        error('Invalid label format.');
    end
end

% copy feature array conservatively (i.e. keeping as many features from the
% original array as possible and modifying them instead of replacing them)
function feats = copyFeatsarrayFrom(feats,templatefeats)
    if not(isempty(feats))
        delfeats = feats(numel(templatefeats)+1:end);
        delete(delfeats(isvalid(delfeats)));
        feats(numel(templatefeats)+1:end) = [];
    end
    if isempty(templatefeats)
        feats = templatefeats; % to copy [] value
    else
        if isempty(feats)
            feats = templatefeats.empty;
        else
            for i=1:numel(feats)
                if strcmp(class(feats(i)),class(templatefeats(i)))
                    feats(i).copyFrom(templatefeats(i));
                else
                    delete(feats(i));
                    feats(i) = templatefeats(i).clone;
                end
            end
        end
        for i=numel(feats)+1:numel(templatefeats)
            feats(i) = templatefeats(i).clone;
        end
    end
end

function [types,objinds] = groupByType(feats)
    if isempty(feats)
        types = cell(1,0);
        objinds = cell(1,0);
    else
        types = {feats.type};
        if any(not(strcmp(types,types{1})))
            [types,~,objtypeinds] = unique(types);
            objinds = array2cell_mex(1:numel(feats),objtypeinds',numel(types),2);
        else
            types = types(1);
            objinds = {1:numel(feats)};
        end
    end
end

end

methods(Access=protected)

function dirtyImpl(obj) %#ok<MANU>
    % do nothing
end

function updateLocalImpl(obj) %#ok<MANU>
    % do nothing
end
    
function updateContextImpl(obj) %#ok<MANU>
    % do nothing
end

function clearContextImpl(obj) %#ok<MANU>
    % do nothing
end

end

methods(Abstract)
    obj2 = clone(obj);
end

methods(Abstract,Access=protected)
    
    isElementImpl(obj);
    isElementFeatureImpl(obj);
    
    getScenegroupImpl(obj);
    setScenegroupImpl(obj);
    hasScenegroupImpl(obj);

    l = getLabelsImpl(obj);
    setLabelsImpl(obj,l);
    hasLabelsImpl(obj); % if the feature has its own labels (= not derived from parent)
    
    updateBoundingboxImpl(obj);
    
    [d,cp] = pointDistanceImpl(obj,p,pose); % distance to a 2D or 3D point
    [d,t,cp] = linesegDistanceImpl(obj,p1,p2,pose); % distance to a 2D or 3D line segment
    
    [polyinds,relation] = inPolygonImpl(obj,poly,polycandidates);
    
    s = similarityImpl(obj,obj2);
end

methods(Abstract,Static)
    n = type_s;
    d = dimension_s;
end

end
