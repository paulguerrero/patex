function gobjs = showSceneCorners(corners,gobjs,parent,varargin)

%     if not(iscell(corners))
%         corners = num2cell(corners);
%     end
    
    if iscell(corners)
        pts = cell(1,numel(corners));
        for j=1:numel(corners)
            pts{j} = [corners{j}.pos];
        end
    else
        pts = [corners.pos];
    end
    
    gobjs = showPoints(pts,gobjs,parent,varargin{:});
end
