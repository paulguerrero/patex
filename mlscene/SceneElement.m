% A basic independent element of the scene
classdef (Abstract) SceneElement < SceneFeature

% Each element has a position ,orientation, size (e.g. extent) and mirrored
% property. When the element is transformed (moved, scaled, rotated, etc.),
% all its properties are invalidated, since they are stored in global
% coordinates for efficiency (e.g. area of an object when scaled, skeleton
% of an object when moved, etc.). Overwrite the move,rotate,scale and
% mirror methods in the subclasses to update the properties. The position,
% orientation, etc., as one of the element properties, are transformed as
% well. To change the local coordinate frame without altering the vertices,
% use definePosition, defineOrientation, defineSize, defineMirrored, etc.

properties(Dependent)
    pose;
    pose2D;
    pose3D;
end

properties(Access={?SceneElement,?SceneImporter,?SceneExporter})
    p_scenegroup = SceneGroup.empty;
    p_labels = cell(1,0);
end

events
    ScenegroupChanged
end

methods
    
function obj = SceneElement(labels)
    obj = obj@SceneFeature;
    
    if nargin >= 1
        obj.p_labels = labels;
    end
end

function copyFrom(obj,obj2)
    obj.copyFrom@SceneFeature(obj2);
    
    obj.p_scenegroup = obj2.p_scenegroup;
    obj.p_labels = obj2.p_labels;
end
    
function val = get.pose(obj)
    val = obj.getPoseImpl;
end

function set.pose(obj,val)
    obj.setPoseImpl(val);
end

function val = get.pose2D(obj)
    val = obj.getPose2DImpl;
end

function set.pose2D(obj,val)
    obj.setPose2DImpl(val);
end

function val = get.pose3D(obj)
    val = obj.getPose3DImpl;
end

function set.pose3D(obj,val)
    obj.setPose3DImpl(val);
end

function setPose(obj,val)
    obj.setPoseImpl(val);
end
function setPose2D(obj,val)
    obj.setPose2DImpl(val);
end
function setPose3D(obj,val)
    obj.setPose3DImpl(val);
end

% define the pose without moving the element (i.e. define local coordinate system)
function definePose(obj,val)
    obj.definePoseImpl(val);
end
function val = defaultPose(obj)
    val = obj.defaultPoseImpl;
end
function val = defaultPose2D(obj)
    val = obj.defaultPose;
    if size(val,1) == 11
        val = SceneElement.pose3Dto2D(val);
    elseif size(val,1) ~= 6
        error('Invalid pose, must have 11 components for 3D pose or 6 components for 2D pose.');
    end
end
function val = defaultPose3D(obj)
    val = obj.defaultPose;
    if size(val,1) == 6
        val = SceneElement.pose2Dto3D(val);
    elseif size(val,1) ~= 11
        error('Invalid pose, must have 11 components for 3D pose or 6 components for 2D pose.');
    end
end
    
% function b = hasSubfeatures(obj)
%     b = arrayfun(@(x) isa(x.p_subfeatures,'SceneFeature'),obj);
% end

% function updateSubfeatures(obj)
%     obj.p_subfeatures = SceneCorner.empty;
% end

end

methods(Sealed)
    
function p = getPose3D(obj)
    if numel(obj) == 1
        p = obj.getPose3DImpl;
    else
        p = zeros(11,numel(obj));
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                p(:,objinds{i}) = obj(objinds{i}).getPose3DImpl;
            end
        end
    end
end

function p = getPose2D(obj)
    if numel(obj) == 1
        p = obj.getPose2DImpl;
    else
        p = zeros(6,numel(obj));
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                p(:,objinds{i}) = obj(objinds{i}).getPose2DImpl;
            end
        end
    end
end

% get the 2d bounding box with orientation and scale given by the framepose
% of the feature with pose featpose
function [center,width,height,orientation] = posedBoundingbox2D(obj,framepose,featpose)
    if numel(obj) == 1
        [center,width,height,orientation] = obj.posedBoundingbox2DImpl(framepose,featpose);
    else
        [types,objinds] = SceneFeature.groupByType(obj);
        width = zeros(1,numel(obj));
        height = zeros(1,numel(obj));
        orientation = zeros(1,numel(obj));
        center = zeros(2,numel(obj));
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                [center(:,objinds{i}),width(objinds{i}),height(objinds{i}),orientation(objinds{i})] = ...
                    obj(objinds{i}).posedBoundingbox2DImpl(framepose(:,objinds{i}),featpose(:,objinds{i}));
            end
        end
    end
end

function b = hasSubfeatures(obj)
    if numel(obj) == 1
        b = obj.hasSubfeaturesImpl;
    else
        b = false(1,numel(obj));
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                b(objinds{i}) = obj(objinds{i}).hasSubfeaturesImpl;
            end
        end
    end
end

function updateSubfeatures(obj)
    if numel(obj) == 1
        obj.updateSubfeaturesImpl;
    else
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                obj(objinds{i}).updateSubfeaturesImpl;
            end
        end
    end
end

function feats = subfeatures(obj)
    if numel(obj) == 1
        feats = obj.subfeaturesImpl;
    else
        feats = cell(1,numel(obj));
        [types,objinds] = SceneFeature.groupByType(obj);
        for i=1:numel(types)
            if not(isempty(objinds{i}))
                feats(objinds{i}) = obj(objinds{i}).subfeaturesImpl;
            end
        end
        
        mask = not(obj.hasSubfeatures);
        if any(mask)
            obj(mask).subfeaturesImpl;
        end
%     if not(obj.hasSubfeatures)
%         obj.updateSubfeatures;
%     end
    end
end

   
end

methods(Access = protected)
    
function b = isElementImpl(obj)
    b = true(1,numel(obj));
end

function b = isElementFeatureImpl(obj)
    b = false(1,numel(obj));
end
    
% fp can either be a single group, or a cell array of groups the same size as obj
function setScenegroupImpl(obj,sg)
    
    if not(iscell(sg))
        sg = {sg};
    end
    
    % filter out objects that already have the given scenegroup
    mask = false(1,numel(obj));
    if numel(sg) == 1
        for k=1:numel(obj)
            mask(k) = numel(obj(k).p_scenegroup) ~= numel(sg{1}) || any(obj(k).p_scenegroup ~= sg{1});
        end
    else
        for k=1:numel(obj)
            mask(k) = numel(obj(k).p_scenegroup) ~= numel(sg{k}) || any(obj(k).p_scenegroup ~= sg{k});
        end
    end
    obj = obj(mask);
    if numel(sg) > 1
        sg = sg(mask);
    end
    
    if not(isempty(obj))
        % convert labels to strings
        subfeats = obj.subfeatures;
        allsubfeats = [subfeats{:}];
        allsubfeats.labelsToStr;        
        obj.labelsToStr;
        
        % clear context
        allsubfeats.clearContext;
        obj.clearContext;
        
%         emptygrp = SceneGroup.empty;
%         for k=1:numel(obj)
%             o = obj(k);
%             oldgroup = o.p_scenegroup;
%             o.p_scenegroup = emptygrp;
%             if not(isempty(oldgroup)) && any(oldgroup.elements == o)
%                 oldgroup.removeElements(o);
%             end
%         end
        
        % change Scenes
        if numel(sg) == 1
            for k=1:numel(obj)
                o = obj(k);
                oldgroup = o.p_scenegroup;
                o.p_scenegroup = SceneGroup.empty;
                if not(isempty(oldgroup)) && any(oldgroup.elements == o)
                    [~] = oldgroup.removeElements(o);
                end
                o.p_scenegroup = sg{1};
                o.notify('ScenegroupChanged');
                if not(isempty(sg{1})) && not(any(sg{1}.elements == o))
                    sg{1}.addElements(o);
                end
            end
        else
            for k=1:numel(obj)
                o = obj(k);
                oldgroup = o.p_scenegroup;
                o.p_scenegroup = SceneGroup.empty;
                if not(isempty(oldgroup)) && any(oldgroup.elements == o)
                    [~] = oldgroup.removeElements(o);
                end   
                o.p_scenegroup = sg{k};
                o.notify('ScenegroupChanged');
                if not(isempty(sg{k})) && not(any(sg{k}.elements == o))
                    sg{k}.addElements(o);
                end
            end
        end
        
        % convert labels of features that have a Scene back to indices
        mask = false(1,numel(obj));
        for k=1:numel(obj)
            mask(k) = not(isempty(obj(k).p_scenegroup));
        end
        if any(mask)
            allsubfeats = [subfeats{mask}];
            allsubfeats.labelsToInd;
            obj.labelsToInd;
        end
    end
end
function fp = getScenegroupImpl(obj)
    fp = {obj.p_scenegroup};
end
function b = hasScenegroupImpl(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_scenegroup));
    end
end

function l = getLabelsImpl(obj)
    l = {obj.p_labels};
end
function setLabelsImpl(obj,l)
    if iscellstr(l) || isreal(l)
        l = repmat({l},1,numel(obj));
    end

    for k=1:numel(obj)
        o = obj(k);
        o.p_labels = l{k};
    end
end
function b = hasLabelsImpl(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        o = obj(k);
        b(k) = not(isempty(o.p_labels));
    end
end

function dirtyImpl(obj)
    obj.dirtyImpl@SceneFeature;
end

function updateLocalImpl(obj)
    obj.updateLocalImpl@SceneFeature;
    
    mask = not(obj.hasSubfeatures);
    if any(mask)
        obj(mask).updateSubfeatures;
    end
end

end

methods(Static)
    function pose = pose2Dto3D(pose)
        orient = angle2quat(pose(3,:)',0,0)';
        pose = [...
            pose(1:2,:);...
            zeros(1,size(pose,2));...
            orient;...
            pose(4:5,:);...
            ones(1,size(pose,2));...
            pose(6,:)];
    end
    
    function pose = pose3Dto2D(pose)
        yaw = quat2angle(pose(4:7,:)')';
        pose = [...
            pose(1:2,:);...
            yaw;...
            pose(8:9,:);... % size
            pose(11,:)];   % mirrored
    end
end

% methods(Access=protected)
% 
% function updateContextImpl(obj)
%     obj.updateContextImpl@SceneFeature;
% end
% 
% function clearContextImpl(obj)
%     obj.clearContextImpl@SceneFeature;
% end
% 
% end

methods(Abstract, Access=protected)
    subfeaturesImpl(obj);
    updateSubfeaturesImpl(obj);
    hasSubfeaturesImpl(obj);
    getPoseImpl(obj);
    setPoseImpl(obj);
    getPose2DImpl(obj);
    setPose2DImpl(obj);
    getPose3DImpl(obj);
    setPose3DImpl(obj);
    definePoseImpl(obj);
    defaultPoseImpl(obj);
    
    [center,width,height,orientation] = posedBoundingbox2DImpl(obj,framepose,featpose);
end

end
