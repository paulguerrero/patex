classdef ScenePointset2D < SceneElement2D

% properties(SetAccess={?ScenePointset2D,?SceneImporter,?SceneExporter})
%     
%     
%     verts = zeros(2,0);
% end

properties(Dependent,SetAccess=protected)
    verts;
    
    center;
    pcorientation; % orientation based on principal components (largest is x, second largest y)
    extent;
    radius;
    orientedbbox;
    convexhull;
    principalcomps;
    principalbbox; % [min. princomp. direction corner, side1 (princomp1 dir),side2 (princomp2 dir)]
end

properties(SetObservable,AbortSet,Access={?ScenePointset2D,?SceneImporter,?SceneExporter})
    p_verts = [];
end

properties(Access={?ScenePointset2D,?SceneImporter,?SceneExporter})
    p_center = [];
    p_pcorientation = [];
    p_extent = [];
    p_radius = [];
    p_orientedbbox = [];
    p_convexhull = [];
    p_principalcomps = [];
    p_principalbbox = [];
end

methods(Static)

function n = type_s
    n = 'pointset2D';
end

function d = dimension_s
    d = 0;
end

end

methods
    
function obj = ScenePointset2D(verts,labels)
    obj@SceneElement2D;
    
    if nargin == 0
        % do nothing (default values)
    elseif nargin == 1 && isa(verts,'ScenePolyline')
        obj2 = verts;
        
        obj.copyFrom(obj2);
    elseif nargin >= 2
    
        obj.setLabels(labels);
        
%         if nargin < 3
%             [verts,closed] = polylineClose(verts);
%         end
        
        obj.setVerts(verts);
        
        obj.definePosition(obj.center);
        obj.defineOrientation(0);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
    end
end

function obj2 = clone(obj)
    obj2 = ScenePointset2D(obj);
end

function delete(obj) %#ok<INUSD>
	% do nothing
end

function copyFrom(obj,obj2)
    obj.copyFrom@SceneElement2D(obj2);
    
    obj.p_verts = obj2.p_verts;
    
    obj.p_center = obj2.p_center;
    obj.p_pcorientation = obj2.p_pcorientation;
    obj.p_extent = obj2.p_extent;
    obj.p_radius = obj2.p_radius;
    obj.p_orientedbbox = obj2.p_orientedbbox;
    
    obj.p_convexhull = obj2.p_convexhull; % convex hulls of the polygons
    
    obj.p_principalcomps = obj2.p_principalcomps;
    obj.p_principalbbox = obj2.p_principalbbox;
end

function setVerts(obj,verts)
    if size(verts,1) ~= 2
        error('Vertices must be two-dimensional.');
    end
    
%     obj.p_verts = verts;
    obj.setVertsImpl(verts);
    
    obj.dirty;
end

function v = get.verts(obj)
    v = obj.getVertsImpl;
end

function set.verts(obj,v)
    obj.setVertsImpl(v);
end

function b = hasCenter(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_center));
    end
end
function val = get.center(obj)
    if isempty(obj.p_center)
        obj.updateCenter;
    end
    
    val = obj.p_center;
end
function val = getCenter(obj)
    mask = not(obj.hasCenter);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updateCenter;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updateCenter;
                end
            end
        end
    end
    val = [obj.p_center];
end
function b = hasPcorientation(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_pcorientation));
    end
end
function val = get.pcorientation(obj)
    if isempty(obj.p_pcorientation)
        obj.updatePcorientation;
    end
    
    val = obj.p_pcorientation;
end
function val = getPcorientation(obj)
    mask = not(obj.hasPcorientation);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updatePcorientation;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updatePcorientation;
                end
            end
        end
    end
    val = [obj.p_pcorientation];
end
function b = hasExtent(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_extent));
    end
end
function val = get.extent(obj)
    if isempty(obj.p_extent)
        obj.updateExtent;
    end
    
    val = obj.p_extent;
end
function val = getExtent(obj)
    mask = not(obj.hasExtent);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updateExtent;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updateExtent;
                end
            end
        end
    end
    val = {obj.p_extent};
end
function b = hasRadius(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_radius));
    end
end
function val = getRadius(obj)
    mask = not(obj.hasRadius);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updateRadius;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updateRadius;
                end
            end
        end
    end
    val = [obj.p_radius];
end
function val = get.radius(obj)
    if isempty(obj.p_radius)
        obj.updateRadius;
    end
    
    val = obj.p_radius;
end
function val = get.orientedbbox(obj)
    if isempty(obj.p_orientedbbox)
       obj.updateOrientedbbox;
    end
    
    val = obj.p_orientedbbox;
end
function val = getOrientedbbox(obj)
    mask = not(obj.hasOrientedbbox);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updateOrientedbbox;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updateOrientedbbox;
                end
            end
        end
    end
    val = [obj.p_orientedbbox];
end
function b = hasOrientedbbox(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_orientedbbox));
    end
end
function val = get.convexhull(obj)
    if isempty(obj.p_convexhull)
        obj.updateConvexhull;
    end
    
    val = obj.p_convexhull;
end
function val = getConvexhull(obj)
    mask = not(obj.hasConvexhull);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updateConvexhull;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updateConvexhull;
                end
            end
        end
    end
    val = {obj.p_convexhull};
end
function b = hasConvexhull(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_convexhull));
    end
end

function val = get.principalcomps(obj)
    if isempty(obj.p_principalcomps)
       obj.updatePrincipalcomps;
    end
    
    val = obj.p_principalcomps;
end
function val = getPrincipalcomps(obj)
    mask = not(obj.hasPrincipalcomps);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updatePrincipalcomps;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updatePrincipalcomps;
                end
            end
        end
    end
    val = {obj.p_principalcomps};
end
function b = hasPrincipalcomps(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_principalcomps));
    end
end
function val = get.principalbbox(obj)
    if isempty(obj.p_principalbbox)
       obj.updatePrincipalbbox;
    end
    
    val = obj.p_principalbbox;
end
function val = getPrincipalbbox(obj)
    mask = not(obj.hasPrincipalbbox);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updatePrincipalbbox;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updatePrincipalbbox;
                end
            end
        end
    end
    val = {obj.p_principalbbox};
end
function b = hasPrincipalbbox(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_principalbbox));
    end
end

function updateOrientedbbox(obj)
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_orientedbbox = zeros(2,3);

        if not(isempty(o.verts))
            angleaxes = zeros(2,1);
            [angleaxes(1),angleaxes(2)] = pol2cart(o.orientation,1);
            angleaxes = [angleaxes,[-angleaxes(2);angleaxes(1)]]; %#ok<AGROW> % orthogonal direction is rotated ccw (like cartesian y axis)
            anglecoords = angleaxes'*o.verts;

            minanglecoords = min(anglecoords,[],2);
            maxanglecoords = max(anglecoords,[],2);

            o.p_orientedbbox(:,1) = angleaxes*minanglecoords;
            o.p_orientedbbox(:,2) = (maxanglecoords(1)-minanglecoords(1)).*angleaxes(:,1);
            o.p_orientedbbox(:,3) = (maxanglecoords(2)-minanglecoords(2)).*angleaxes(:,2);
        end
    end
end
function updateConvexhull(obj)
    for k=1:numel(obj)
        o = obj(k);

        pts = o.verts;
        
        if not(isempty(pts))
            hinds = convhull(pts(1,:),pts(2,:));
            if not(isempty(hinds))
                hinds(end) = []; % remove endindex (because it is = startindex)
            end
            o.p_convexhull = pts(:,hinds);
        else
            o.p_convexhull = zeros(2,0);
        end
    end
end
function updatePrincipalcomps(obj)
    for k=1:numel(obj)
        o = obj(k);
    
        if isempty(o.verts)
            o.p_principalcomps = zeros(2,0);
        else
            o.p_principalcomps = pca(o.verts');
        end
    end
end
function updatePrincipalbbox(obj)
    
    mask = not(obj.hasPrincipalcomps);
    if any(mask)
        obj(mask).updatePrincipalcomps;
    end
    
    for k=1:numel(obj)
        o = obj(k);
        o.p_principalbbox = zeros(2,3);

        pcacoords = o.principalcomps'*o.verts;

        minpcacoords = min(pcacoords,[],2);
        maxpcacoords = max(pcacoords,[],2);

        o.p_principalbbox(:,1) = o.principalcomps*minpcacoords;
        o.p_principalbbox(:,2) = (maxpcacoords(1)-minpcacoords(1)).*o.principalcomps(:,1);
        o.p_principalbbox(:,3) = (maxpcacoords(2)-minpcacoords(2)).*o.principalcomps(:,2);
    end
end

function updateCenter(obj)
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_center = mean(o.verts,2);
    end
end
function updatePcorientation(obj)
    for k=1:numel(obj)
        o = obj(k);
        
%         [coeff,~] = pca(o.verts');
%         [o.p_pcorientation,~] = cart2pol(coeff(1,1),coeff(2,1));
        
        pcomps = obj.principalcomps;
        o.p_pcorientation = cart2pol(pcomps(1,1),pcomps(2,1));
    end
end
function updateExtent(obj)
    extents = computeExtent(obj,obj.getOrientation);
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_extent = extents(:,k);
        
%         % compute maximum extent of polyline in local x and y direction
%         [xaxis(1,1),xaxis(2,1)] = pol2cart(o.orientation,1);
%         yaxis = [-xaxis(2);xaxis(1)];
% 
%         verts_local = [xaxis';yaxis'] * o.verts;
%         o.p_extent = max(verts_local,2) - min(verts_local,2);
    end
end
function val = computeExtent(obj,orient)
    val = zeros(2,numel(obj));
    for k=1:numel(obj)
        o = obj(k);
        
        if isempty(o.verts)
            val(:,k) = 0;
        else
            % compute maximum extent of mesh in local x, y and z direction
            [xaxis(1,1),xaxis(2,1)] = pol2cart(orient(:,k),1);
            yaxis = [-xaxis(2);xaxis(1)];

            verts_local = [xaxis';yaxis'] * o.verts;
            val(:,k) = max(verts_local,[],2) - min(verts_local,[],2);
        end
    end
end
function updateRadius(obj)
    for k=1:numel(obj)
        o = obj(k);

        o.p_radius = max(sqrt(sum(bsxfun(@minus,o.verts,o.position).^2,1)));
    end
end

function updateSymmetries(obj)
    for k=1:numel(obj)
        o = obj(k);

        o.p_symmetries = pointsetSymmetries(...
            o.verts,...
            o.position,...
            o.orientation);
    end
end

end

methods(Access=protected)
    
function val = defaultPoseImpl(obj)
    orient = [obj.pcorientation];
    val = [...
        [obj.center];...
        orient;...
        obj.computeExtent(orient);...
        false(1,numel(obj))];
end
    
function dirtyImpl(obj)
    obj.dirtyImpl@SceneElement2D;
    
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_center = [];
        o.p_pcorientation = [];
        o.p_extent = [];
        o.p_radius = [];
        o.p_orientedbbox = [];
        o.p_convexhull = [];
        o.p_principalcomps = [];
        o.p_principalbbox = [];    
    end
end
    
function updateLocalImpl(obj)
    obj.updateLocalImpl@SceneElement2D;
    
    mask = not(obj.hasCenter);
    if any(mask)
        obj(mask).updateCenter;
    end
    mask = not(obj.hasPcorientation);
    if any(mask)
        obj(mask).updatePcorientation;
    end
    mask = not(obj.hasExtent);
    if any(mask)
        obj(mask).updateExtent;
    end
    mask = not(obj.hasRadius);
    if any(mask)
        obj(mask).updateRadius;
    end
    mask = not(obj.hasOrientedbbox);
    if any(mask)
        obj(mask).updateOrientedbbox;
    end
    mask = not(obj.hasConvexhull);
    if any(mask)
        obj(mask).updateConvexhull;
    end
    mask = not(obj.hasPrincipalcomps);
    if any(mask)
        obj(mask).updatePrincipalcomps;
    end
    mask = not(obj.hasPrincipalbbox);
    if any(mask)
        obj(mask).updatePrincipalbbox;
    end
end
    
function updateBoundingboxImpl(obj)
    for i=1:numel(obj)
        o = obj(i); % for performance
        v = o.verts;
        o.p_boundingboxmin = min(v,[],2);
        o.p_boundingboxmax = max(v,[],2);
    end
end

% pose must be 2D
function [center,width,height,orientation] = posedBoundingbox2DImpl(obj,framepose,featpose)
    
    if nargin < 2 || isempty(framepose)
        framepose = [...
            zeros(numel(obj));...
            zeros(numel(obj));...
            zeros(numel(obj));...
            ones(numel(obj));...
            zeros(numel(obj))];
    end
    if nargin < 2 || isempty(featpose)
        featpose = obj.getPose2D;
    end
    
    center = zeros(2,numel(obj));
    width = zeros(1,numel(obj));
    height = zeros(1,numel(obj));
    orientation = zeros(1,numel(obj));
    for i=1:numel(obj)
        v = posetransform(obj(i).verts,SceneElement.pose3Dto2D(featpose(:,i)),obj(i).pose2D);
        v = posetransform(v,identpose2D,framepose(:,i));

        bbmin = min(v,[],2);
        bbmax = max(v,[],2);
        center(:,i) = (bbmin + bbmax) .* 0.5;
        width(i) = bbmax(1) - bbmin(1);
        height(i) = bbmax(2) - bbmin(2);
        
        center(:,i) = posetransform(center(:,i),framepose(:,i),identpose2D);

        width(i) = width(i) * framepose(4,i);
        height(i) = height(i) * framepose(5,i);
        orientation(i) = framepose(3,i);
    end
end
    
% relations 0 and 2 are never returned, because only relevant poly indices
% (i.e. feature is inside poly or intersecting) are returned
% relation: 0: feature and poly are separate
% relation: 1: feature completely inside poly
% relation: 2: poly completely inside feature
% relation: 3: feature and poly are intersecting
function [polyinds,relation] = inPolygonImpl(obj,poly,polycandidates)

    polyinds = cell(numel(obj),1);
    if nargout >= 2
        relation = cell(numel(obj),1);
    end
    if not(isempty(poly)) && not(isempty(obj))
        [polybbmin,polybbmax] = poly.boundingbox;
        [mybbmin,mybbmax] = obj.boundingbox;

        for j=1:numel(obj)
            
            % find polygons with bounding boxes containing the point set
            % bounding box
            if nargin < 3 || isempty(polycandidates)
                polyinds{j} = find(...
                    polybbmin(1,:) < mybbmax(1,j) & polybbmin(2,:) < mybbmax(2,j) & ...
                    polybbmax(1,:) > mybbmin(1,j) & polybbmax(2,:) > mybbmin(2,j));
            else
                polyinds{j} = polycandidates{j}(...
                    polybbmin(1,polycandidates{j}) < mybbmax(1,j) & polybbmin(2,polycandidates{j}) < mybbmax(2,j) & ...
                    polybbmax(1,polycandidates{j}) > mybbmin(1,j) & polybbmax(2,polycandidates{j}) > mybbmin(2,j));
            end
            
            % find all polygons containing at least part of the point set
            p = obj(j).verts(:,1);
            keepmask = false(1,numel(polyinds{j}));
            if nargout >= 2
                relation{j} = zeros(1,numel(polyinds{j}));
            end
            for i=1:numel(polyinds{j})
                
                pointinpoly = pointInPolygon_mex(p(1),p(2),poly(polyinds{j}(i)).verts(1,:),poly(polyinds{j}(i)).verts(2,:));

                intersecting = any(pointinpoly);
                contained = all(pointinpoly);
                
                keepmask(i) = intersecting | contained;
                
                if nargout >= 2
                    if contained
                        relation{j}(i) = 1;
                    elseif intersecting
                        relation{j}(i) = 3;
                    end
                end
            end
            polyinds{j} = polyinds{j}(keepmask);
            if nargout >= 2
                relation{j} = relation{j}(keepmask);
            end
        end
    end
end
    
function [d,cp] = pointDistanceImpl(obj,p,pose)
    
    if size(p,1) == 2
        d = inf(numel(obj),size(p,2));
        cp = zeros(numel(obj),size(p,2),2);
        for i=1:numel(obj)
            v = obj(i).verts;
            if nargin >= 3 && not(isempty(pose)) && not(all(pose{i}==obj(i).pose))
                v = posetransform(v,pose{i},obj(i).pose);
            end
            
            for j=1:size(p,2)
                [d(i,j),vind] = min((p(1,j) - v(1,:)).^2 + (p(2,j) - v(2,:)).^2);
                d(i,j) = sqrt(d(i,j));
                cp(i,j,1) = v(1,vind);
                cp(i,j,2) = v(2,vind);
            end
        end
    elseif size(p,1) == 3
        % assume z-coordinates of polyline are zero
        d = inf(numel(obj),size(p,2));
        cp = zeros(numel(obj),size(p,2),3);
        for i=1:numel(obj)
            v = [obj(i).verts;zeros(1,size(obj(i).verts,2))];
            if nargin >= 3 && not(isempty(pose))
                if not(all(pose{i}==obj(i).pose3D))
                    v = posetransform(v,pose{i},obj(i).pose3D);
                end
            else
                if not(isempty(obj.scenegroup))
                    grouppose = obj.scenegroup.globaloriginpose;
                    v = transform3D(v,grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
                end
            end
            
            for j=1:size(p,2)
                [d(i,j),vind] = min((p(1,j) - v(1,:)).^2 + (p(2,j) - v(2,:)).^2);
                d(i,j) = sqrt(d(i,j) + p(3,j)^2);
                cp(i,j,1) = v(1,vind);
                cp(i,j,2) = v(2,vind);
                cp(i,j,3) = 0;
            end
        end
    else
        error('Only implemented for two or three dimensions.');
    end
end

function [d,t,cp] = linesegDistanceImpl(obj,p1,p2,pose) % distance to a 3D line segment

    error('Not yet implemented.');
    
%     if size(p1,1) == 2
%         error('Not yet implemented.');
%         % todo: implement a function like linesegLinesegDistance_mex (2D)
%     elseif  size(p1,1) == 3
%         d = inf(numel(obj),size(p1,2));
%         t = nan(numel(obj),size(p1,2));
%         cp = zeros(numel(obj),size(p1,2),3);
%         for i=1:numel(obj)
%             v = [obj(i).verts;zeros(1,size(obj(i).verts,2))];
%             if nargin >= 4 && not(isempty(pose))
%                 if not(all(pose{i}==obj(i).pose3D))
%                     v = posetransform(v,pose{i},obj(i).pose3D);
%                 end
%             else
%                 if not(isempty(obj(i).scenegroup))
%                     grouppose = obj(i).scenegroup.globaloriginpose;
%                     v = transform3D(v,grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
%                 end
%             end
% 
%             if obj(i).closed
%                 lsegstart = v;
%                 lsegend = v(:,[2:end,1]);
%             else
%                 lsegstart = v(:,1:end-1);
%                 lsegend = v(:,2:end);
%             end
%             [dists,t1,t2] = linesegLinesegDistance3D_mex(p1,p2,...
%                 lsegstart,lsegend);
%             
% %             [d(i,:),mininds] = min(dists,[],2);
% %             t(i,:) = t1(sub2ind(size(t1),1:size(t1,1),mininds));
%             
%             % get element with minimum distance (and minimum t if there
%             % is more than one element with the same minimum distance)
%             mindist = min(dists,[],2);
%             t1candidates = t1;
%             t1candidates(not(bsxfun(@eq,dists,mindist))) = nan;
%             [t(i,:),mininds] = min(t1candidates,[],2);
%             mininds = mininds';
%             mininds_lin = sub2ind(size(t1),1:size(t1,1),mininds);
% 
%             d(i,:) = dists(mininds_lin);
%             
%             % get closest points
% %             t2 = t2(sub2ind(size(t2),1:size(t2,1),mininds));
%             t2 = t2(mininds_lin);
%             cp(i,:,:) = permute(lsegstart(:,mininds) + ...
%                 bsxfun(@times,lsegend(:,mininds)-lsegstart(:,mininds),t2),[3,2,1]);
%         end
%     else
%         error('Only implemented for two or three dimensions.');
%     end
end

% function updateSubfeaturesImpl(obj)
%     obj.p_subfeatures = [obj.corners,obj.edges,obj.segments];
% end
function feats = subfeaturesImpl(obj) 
    feats = repmat({SceneCorner.empty},1,numel(obj));
end
function b = hasSubfeaturesImpl(obj) %#ok<MANU>
    b = true; % means subfeatures are up-to-date
end
function updateSubfeaturesImpl(obj)
    % do nothing
end

function moveAboutOriginImpl(obj,movevec)
    
    obj.verts = bsxfun(@plus,obj.verts,movevec);
    
    if obj.hasCenter
        obj.p_center = obj.p_center + movevec;
    end
    if obj.hasOrientedbbox
        obj.p_orientedbbox(:,1) = obj.p_orientedbbox(:,1) + movevec;
    end
    if obj.hasConvexhull
        obj.p_convexhull = [obj.p_convexhull(1,:)+movevec(1);obj.p_convexhull(2,:)+movevec(2)];
    end
    if obj.hasPrincipalbbox
        obj.p_principalbbox(:,1) = obj.p_principalbbox(:,1) + movevec;
    end
end

function rotateAboutOriginImpl(obj,rotangle)
    rotmat = [cos(rotangle),-sin(rotangle);...
              sin(rotangle),cos(rotangle)];
	
    obj.verts = rotmat*obj.verts; 

    if obj.hasCenter
        obj.p_center = rotmat * obj.p_center;
    end
    if obj.hasPcorientation
        obj.p_pcorientation = obj.p_pcorientation + rotangle;
    end
    obj.p_orientedbbox = [];
    if obj.hasConvexhull
        obj.p_convexhull = rotmat*obj.p_convexhull;
    end
    obj.p_principalcomps = [];
    obj.p_principalbbox = [];
end

function scaleAboutOriginImpl(obj,relscale)
    
    obj.verts = bsxfun(@times,obj.verts,relscale); 
    
    if obj.hasCenter
        obj.p_center = obj.p_center .* relscale;
    end
    if obj.hasExtent
        if any(relscale ~= relscale(1))
            % non-uniform scaling, need to recompute extent
            obj.p_extent = [];
        else
            obj.p_extent = obj.p_extent .* relscale(1);
        end
    end
    if obj.hasRadius
        if any(relscale ~= relscale(1))
            % non-uniform scaling, need to recompute radius
            obj.p_radius = [];
        else
            obj.p_radius = obj.p_radius .* relscale(1);
        end
    end
    obj.p_orientedbbox = [];
    if obj.hasConvexhull
        obj.p_convexhull = bsxfun(@times,obj.p_convexhull,relscale);
    end
    if any(relscale ~= relscale(1))
        obj.p_principalcomps = [];
    end
    obj.p_principalbbox = [];
end

function mirrorAboutOriginImpl(obj,mirchange)
    
    if not(mirchange)
        return;
    end
    
    obj.verts = bsxfun(@times,obj.verts,[1;-1]);
    
    if obj.hasCenter
        obj.p_center = [obj.p_center(1);-obj.p_center(2)];
    end
    
    obj.p_orientedbbox = []; 
    if obj.hasConvexhull
        obj.p_convexhull = [obj.p_convexhull(1,:);-obj.p_convexhull(2,:)]; 
        obj.p_convexhull = obj.p_convexhull(:,[1,end:-1:2]);
    end
    obj.p_principalcomps = [];
    obj.p_principalbbox = [];
end

function s = similarityImpl(obj,obj2)
    s = ones(numel(obj),numel(obj2)); % all point sets have similarity 1 to each other for now
end

function setVertsImpl(obj,verts)
    obj.p_verts = verts;
end

function verts = getVertsImpl(obj)
    verts = obj.p_verts;
end

end

end % classdef
