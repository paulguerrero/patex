classdef SceneMaterial < handle

properties
    name = '';
    
    ambient = [];
    diffuse = [];
    specular = [];
    transmittance = [];
    emission = [];
    shininess = [];
    ior = [];
    
    ambient_texname = '';
    diffuse_texname = '';
    specular_texname = '';
    normal_texname = '';
    
    attribs = containers.Map('KeyType','char','ValueType','char');
end

methods

% SceneMaterial()
% SceneMaterial(obj2)
% SceneMaterial(name)
function obj = SceneMaterial(varargin)
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && ischar(varargin{1})
        obj.name = varargin{1};
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneMaterial')
        obj.copyFrom(varargin{1})
    else
        error('Invalid arguments.');
    end
end

function delete(obj)
    if not(isempty(obj.attribs))
        delete(obj.attribs(isvalid(obj.attribs)));
    end
end

function copyFrom(obj,obj2)
    obj.name = obj2.name;
    
    obj.ambient = obj2.ambient;
    obj.diffuse = obj2.diffuse;
    obj.specular = obj2.specular;
    obj.transmittance = obj2.transmittance;
    obj.emission = obj2.emission;
    obj.shininess = obj2.shininess;
    obj.ior = obj2.ior;
    
    obj.ambient_texname = obj2.ambient_texname;
    obj.diffuse_texname = obj2.diffuse_texname;
    obj.specular_texname = obj2.specular_texname;
    obj.normal_texname = obj2.normal_texname;
    
    if not(isempty(obj.attribs))
        delete(obj.attribs(isvalid(obj.attribs)));
    end
    if isempty(obj2.attribs)
        obj.attribs = containers.Map('KeyType','char','ValueType','char');
    else
        obj.attribs = containers.Map(obj2.attribs.keys,obj2.attribs.values);
    end
end

function obj2 = clone(obj)
    obj2 = SceneMaterial(obj);
end

end

end
