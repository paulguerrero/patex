classdef SceneGraphics < AxesWidget
    
properties(SetAccess = protected)
    axes = gobjects(1,0);
    
    polygonsVisible = true;
    pointsVisible = true;
    polylinesVisible = true;
    meshesVisible = true;
    
    cornersVisible = false;
    edgesVisible = false;
    segmentsVisible = false;
    surfacesVisible = false;
    
    normalsVisible = false;
    curvatureVisible = false;
    verticesVisible = false;
    
    polygondetailsVisible = false;
%     polygonskeletonVisible = false;
    
    lightsVisible = true;
    
    scenecolors = struct(...
        'mesh',rgbhex2rgbdec('ffffb3')',...
        'selmesh',rgbhex2rgbdec('9dde91')');
    scenealphas = struct(...
        'mesh',1,...
        'selmesh',1);
    
%     gprops = containers.Map('KeyType','char','ValueType','any');
%     gpropcustom = 
end

properties(Access=protected)
    % axes content (each: {scene part,graphics,gprop id})
    ggroups = {SceneGroup.empty(1,0);gobjects(1,0);cell(1,0)};
    
    gpoints = {ScenePoint.empty(1,0);gobjects(1,0);cell(1,0)};
    gpolylines = {ScenePolyline.empty(1,0);gobjects(1,0);cell(1,0)};
    gpolygons = {ScenePolygon.empty(1,0);gobjects(1,0);cell(1,0)};
    gcomplexpolygons = {SceneComplexPolygon.empty(1,0);gobjects(1,0);cell(1,0)};
    gmeshes = {SceneMesh.empty(1,0);gobjects(1,0);cell(1,0)};
    
    gcorners = {SceneCorner.empty(1,0);gobjects(1,0);cell(1,0)};
    gedges = {SceneEdge.empty(1,0);gobjects(2,0);cell(1,0)};
    gsegments = {SceneBoundarySegment.empty(1,0);gobjects(2,0);cell(1,0)};
    gsurfaces = {SceneSurface.empty(1,0);gobjects(1,0);cell(1,0)};
    
    gverts = gobjects(1,1);
    gnormals = gobjects(1,1);
    
    gpolygondetails = {ScenePolygon.empty(1,0);gobjects(6,0),cell(1,0)};
    
    glights = gobjects(1,0);
end

methods
    
function obj = SceneGraphics()
    % add default graphics properties for empty id
    obj.gprops('') = gproperties;
end

function delete(obj)
    obj.hide;
end

function ind = addGraphicsProperties(obj,id,varargin)
    if strcmp(id,'')
        error('Must pass a non-empty id.');
    end
    obj.gprops(id) = varargin;
    ind = numel(obj.gprops);
end

function removeGraphicsProperties(obj,ids)
    if any(strcmp(ids,''))
        error('Must pass non-empty ids.');
    end
    
    obj.gprops.remove(ids)
    
    mask = ismember(obj.gpoints{3},ids);
    obj.gpoints{3}(mask) = {''};
    mask = ismember(obj.gpolylines{3},ids);
    obj.gpolylines{3}(mask) = {''};
    mask = ismember(obj.gpolygons{3},ids);
    obj.gpolygons{3}(mask) = {''};
    mask = ismember(obj.gcomplexpolygons{3},ids);
    obj.gcomplexpolygons{3}(mask) = {''};
    mask = ismember(obj.gmeshes{3},ids);
    obj.gmeshes{3}(mask) = {''};
    mask = ismember(obj.gcorners{3},ids);
    obj.gcorners{3}(mask) = {''};
    mask = ismember(obj.gedges{3},ids);
    obj.gedges{3}(mask) = {''};
    mask = ismember(obj.gsegments{3},ids);
    obj.gsegments{3}(mask) = {''};
    mask = ismember(obj.gsurfaces{3},ids);
    obj.gsurfaces{3}(mask) = {''};
end

function g = getGraphics(obj,scenepart)
    g = gobjects(1,numel(scenepart));
    if isa(scenepart,'SceneGroup')
        [~,ind] = ismember(scenepart,obj.ggroups{1});
        g(ind > 0) = obj.ggroups{2}(ind);
    elseif isa(scenepart,'SceneFeature')
        ftypes = {scenepart.type};
        
        [ftypes,~,feattypeind] = unique(ftypes);
        typefeatinds = array2cell_mex(1:numel(ftypes),feattypeind,numel(ftypes));
        
        for i=1:numel(ftypes)
            if strcmp(ftypes{i},'point')
                glist = obj.gpoints;
            elseif strcmp(ftypes{i},'polyline')
                glist = obj.gpolylines;
            elseif strcmp(ftypes{i},'polygon')
                glist = obj.gpolygons;
            elseif strcmp(ftypes{i},'complexpolygon')
                glist = obj.gcomplexpolygons;
            elseif strcmp(ftypes{i},'mesh')
                glist = obj.gmeshes;
            elseif strcmp(ftypes{i},'corner')
                glist = obj.gcorners;
            elseif strcmp(ftypes{i},'edge')
                glist = obj.gedges;
            elseif strcmp(ftypes{i},'segment')
                glist = obj.gsegments;
            elseif strcmp(ftypes{i},'surface')
                glist = obj.gsurfaces;
            else
                error(['Unrecognized scene feature type: ',ftypes{i}]);
            end
            
            [~,ind] = ismember(scenepart(typefeatinds{i}),glist{1});
            g(typefeatinds{i}(ind > 0)) = glist{2}(ind);
        end
    else
        error(['Unrecognized scene part: ',class(scenepart)]);
    end
end

function f = getAllFeatures(obj)
    f = unique([...
        obj.gpoints{1},...
        obj.gpolylines{1},...
        obj.gpolygons{1},...
        obj.gcomplexpolygons{1},...
        obj.gmeshes{1},...
        obj.gcorners{1},...
        obj.gedges{1},...
        obj.gsegments{1},...
        obj.gsurfaces{1},...
        obj.gpolygondetails{1},...
        ]);
end

function f = getAllGraphicsProperties(obj)
    f = unique([...
        obj.gpoints{3},...
        obj.gpolylines{3},...
        obj.gpolygons{3},...
        obj.gcomplexpolygons{3},...
        obj.gmeshes{3},...
        obj.gcorners{3},...
        obj.gedges{3},...
        obj.gsegments{3},...
        obj.gsurfaces{3},...
        obj.gpolygondetails{3},...
        ]);
end

function togglePointsVisible(obj)
    obj.pointsVisible = not(obj.pointsVisible);
    if obj.pointsVisible
        set(obj.gmenu_points,'Checked','on');
        obj.showAllScenePoints;
    else
        set(obj.gmenu_points,'Checked','off');
        obj.hideAllScenePoints;
    end    
end

function togglePolylinesVisible(obj)
    obj.polylinesVisible = not(obj.polylinesVisible);
    if obj.polylinesVisible
        set(obj.gmenu_polylines,'Checked','on');
        obj.showAllScenePolylines;
    else
        set(obj.gmenu_polylines,'Checked','off');
        obj.hideAllScenePolylines;
    end    
end

function togglePolygonsVisible(obj)
    obj.polygonsVisible = not(obj.polygonsVisible);
    if obj.polygonsVisible
        set(obj.gmenu_polygons,'Checked','on');
        obj.showAllScenePolygons;
        obj.showAllSceneComplexPolygons;
    else
        set(obj.gmenu_polygons,'Checked','off');
        obj.hideAllScenePolygons;
        obj.hideAllSceneComplexPolygons;
    end    
end

function toggleMeshesVisible(obj)
    obj.meshesVisible = not(obj.meshesVisible);
    if obj.meshesVisible
        set(obj.gmenu_meshes,'Checked','on');
        obj.showAllSceneMeshes;
    else
        set(obj.gmenu_meshes,'Checked','off');
        obj.hideAllSceneMeshes;
    end    
end

function toggleCornersVisible(obj)
    obj.cornersVisible = not(obj.cornersVisible);
    if obj.cornersVisible
        set(obj.gmenu_corners,'Checked','on');
        obj.showAllSceneCorners;
    else
        set(obj.gmenu_corners,'Checked','off');
        obj.hideAllSceneCorners;
    end  
end

function toggleEdgesVisible(obj)
    obj.edgesVisible = not(obj.edgesVisible);
    if obj.edgesVisible
        set(obj.gmenu_edges,'Checked','on');
        obj.showAllSceneCorners;
        obj.showAllSceneEdges;
    else
        set(obj.gmenu_edges,'Checked','off');
        obj.hideAllSceneCorners;
        obj.hideAllSceneEdges;
    end  
end

function toggleSegmentsVisible(obj)
    obj.segmentsVisible = not(obj.segmentsVisible);
    if obj.segmentsVisible
        set(obj.gmenu_segments,'Checked','on');
        obj.showAllSceneSegments;
    else
        set(obj.gmenu_segments,'Checked','off');
        obj.hideAllSceneSegments;
    end  
end

function toggleSurfacesVisible(obj)
    obj.surfacesVisible = not(obj.surfacesVisible);
    if obj.surfacesVisible
        set(obj.gmenu_surfaces,'Checked','on');
        obj.showAllSceneSurfaces;
    else
        set(obj.gmenu_surfaces,'Checked','off');
        obj.hideAllSceneSurfaces;
    end  
end

function toggleNormalsVisible(obj)
    obj.normalsVisible = not(obj.normalsVisible);
    if obj.normalsVisible
        if obj.curvatureVisible
            obj.toggleCurvatureVisible;
        end
        if not(obj.verticesVisible)
            obj.toggleVerticesVisible;
        end
        set(obj.gmenu_normals,'Checked','on');
        obj.showAllSceneNormals;
    else
        set(obj.gmenu_normals,'Checked','off');
        obj.hideAllSceneNormals;
    end
end

function toggleCurvatureVisible(obj)
    obj.curvatureVisible = not(obj.curvatureVisible);
    if obj.curvatureVisible
        if obj.normalsVisible
            obj.toggleNormalsVisible
        end
        if not(obj.verticesVisible)
            obj.toggleVerticesVisible;
        end
        set(obj.gmenu_curv,'Checked','on');
        obj.showAllSceneCurvature;
    else
        set(obj.gmenu_curv,'Checked','off');
        obj.hideAllSceneCurvature;
    end
end

function toggleVerticesVisible(obj)
    obj.verticesVisible = not(obj.verticesVisible);
    if obj.verticesVisible
        set(obj.gmenu_verts,'Checked','on');
        obj.showAllSceneVerts;
    else
        set(obj.gmenu_verts,'Checked','off');
        obj.hideAllSceneVerts;
    end
end

function togglePolygondetailsVisible(obj)
    obj.polygondetailsVisible = not(obj.polygondetailsVisible);
    if obj.polygondetailsVisible
        set(obj.gmenu_polygondetails,'Checked','on');
        obj.showAllScenePolygonDetails;
    else
        set(obj.gmenu_polygondetails,'Checked','off');
        obj.hideAllScenePolygonDetails;
    end
end

% function togglePolygonskeletonVisible(obj)
%     obj.polygonskeletonVisible = not(obj.polygonskeletonVisible);
%     if obj.polygonskeletonVisible
%         set(obj.gmenu_polygonskeleton,'Checked','on');
%         obj.showAllScenePolygonSkeletons;
%     else
%         set(obj.gmenu_polygonskeleton,'Checked','off');
%         obj.hideAllScenePolygonSkeletons;
%     end
% end

function toggleLightsVisible(obj)
    obj.lightsVisible = not(obj.lightsVisible);
    if obj.lightsVisible
        set(obj.gmenu_lights,'Checked','on');
        obj.showAllSceneLights;
    else
        set(obj.gmenu_lights,'Checked','off');
        obj.hideAllSceneLights;
    end
end

function showAllSceneFeatures(obj,features,propids)
    
    if not(isempty(obj.scene))
        if nargin<2 || isempty(features)
            features = obj.getAllFeatures;
        end
        
        groups = features.getScenegroup;
        groups = unique([groups{:}]);
        
        missingmask = not(ismember(groups,obj.ggroups{1}));
        if any(missingmask)
            obj.showAllSceneGroups(groups(missingmask));
        end
        
        feattypes = {features.type};
        
%         [types,~,feattypeind] = unique(feattypes);
%         typefeatinds = array2cell_mex(1:numel(features),feattypeind,numel(types));
%         
%         for i=1:numel(types)
%             if strcmp(types{i},'point')
%                 if obj.pointsVisible
%                     obj.showAllScenePoints(features(typefeatinds{i}));
%                 end
%             elseif strcmp(types{i},'polyline')
%                 if obj.polylinesVisible
%                     obj.showAllScenePolylines(features(typefeatinds{i}));
%                 end
%             elseif strcmp(types{i},'polygon')
%                 if obj.polygonsVisible
%                     obj.showAllScenePolygons(features(typefeatinds{i}));
%                 end
%             elseif strcmp(types{i},'complexpolygon')
%                 if obj.polygonsVisible
%                     obj.showAllSceneComplexPolygons(features(typefeatinds{i}));
%                 end
%             elseif strcmp(types{i},'mesh')
%                 if obj.meshesVisible
%                     meshes = features(typefeatinds{i});
%                     
%                     obj.showAllSceneMeshes(meshes);
%                     
%                     meshgroups = meshes.getScenegroup;
%                     meshgroups = unique([meshgroups{:}]);
%                     if not(isempty(meshgroups))
%                         meshchildgroups = [meshgroups.childgroups];
%                         meshchildgroups = meshchildgroups(strcmp({meshchildgroups.type},'surfacegroup'));
%                         if not(isempty(meshchildgroups))
%                             surfacegroups = meshchildgroups(ismember([meshchildgroups.surface],[meshes.surfaces]));
%                             if not(isempty(surfacegroups))
%                                 obj.showAllSceneGroups(surfacegroups);
%                             end
%                         end
%                     end
%                 end
%             elseif strcmp(types{i},'corner')
%                 
%             else
%                 warning('No show method defined for some of the given features.');
%             end
%         end
        
        if obj.pointsVisible
            mask = strcmp(feattypes,'point');
            if any(mask)
                obj.showAllScenePoints(features(mask),propids(mask));
            end
        end   
        if obj.polylinesVisible
            mask = strcmp(feattypes,'polyline');
            if any(mask)
                obj.showAllScenePolylines(features(mask),propids(mask));
            end
        end
        if obj.polygonsVisible
            mask = strcmp(feattypes,'polyline');
            if any(mask)
                obj.showAllScenePolygons(features(mask),propids(mask));
            end
            mask = strcmp(feattypes,'complexpolygon');
            if any(mask)
                obj.showAllSceneComplexPolygons(features(mask),propids(mask));
            end
        end
        if obj.meshesVisible
            mask = strcmp(feattypes,'mesh');
            meshes = features(mask);
            if not(isempty(meshes))
                obj.showAllSceneMeshes(meshes,propids(mask));
                
                meshgroups = meshes.getScenegroup;
                meshgroups = unique([meshgroups{:}]);
                if not(isempty(meshgroups))
                    meshchildgroups = [meshgroups.childgroups];
                    meshchildgroups = meshchildgroups(strcmp({meshchildgroups.type},'surfacegroup'));
                    if not(isempty(meshchildgroups))
                        surfacegroups = meshchildgroups(ismember([meshchildgroups.surface],[meshes.surfaces]));
                        if not(isempty(surfacegroups))
                            obj.showAllSceneGroups(surfacegroups);
                        end
                    end
                end
            end
        end
        if obj.cornersVisible
            mask = strcmp(feattypes,'corner');
            if any(mask)
                obj.showAllSceneCorners(features(mask),propids(mask));
            end
        end  
        if obj.edgesVisible
            mask = strcmp(feattypes,'edge');
            if any(mask)
                obj.showAllSceneEdges(features(mask),propids(mask));
            end
        end  
        if obj.segmentsVisible
            mask = strcmp(feattypes,'segment');
            if any(mask)
                obj.showAllSceneSegments(features(mask),propids(mask));
            end
        end
        if obj.surfacesVisible
            mask = strcmp(feattypes,'surface');
            if any(mask)
                obj.showAllSceneSurfaces(features(mask),propids(mask));
            end
        end
        if obj.polygondetailsVisible
            mask = strcmp(feattypes,'polygon');
            if any(mask)
                obj.showAllScenePolygonDetails(features(mask),propids(mask));
            end
        end
    end
end

function hideAllSceneFeatures(obj,features)
    
    if nargin >= 2
        if isempty(features)
            return;
        end
        
        validmask = isvalid(features);
        invalidfeatures = features(not(validmask));

        obj.hideAllScenePoints(invalidfeatures);
        obj.hideAllScenePolylines(invalidfeatures);
        obj.hideAllScenePolygons(invalidfeatures);
        obj.hideAllSceneComplexPolygons(invalidfeatures);
        obj.hideAllSceneMeshes(invalidfeatures);
        obj.hideAllSceneCorners(invalidfeatures);
        obj.hideAllSceneEdges(invalidfeatures);
        obj.hideAllSceneSegments(invalidfeatures);
        obj.hideAllSceneSurfaces(invalidfeatures);

        obj.hideAllScenePolygonDetails(invalidfeatures);        
        
        
        features = features(validmask);
        feattypes = {features.type};
    
%         groups = features.getScenegroup;
%         groups = [groups{:}];
        
        obj.hideAllScenePoints(features(strcmp(feattypes,'point')));
        obj.hideAllScenePolylines(features(strcmp(feattypes,'polyline')));
        obj.hideAllScenePolygons(features(strcmp(feattypes,'polygon')));
        obj.hideAllSceneComplexPolygons(features(strcmp(feattypes,'complexpolygon')));
        obj.hideAllSceneMeshes(features(strcmp(feattypes,'mesh')));
        obj.hideAllSceneCorners(features(strcmp(feattypes,'corner')));
        obj.hideAllSceneEdges(features(strcmp(feattypes,'edge')));
        obj.hideAllSceneSegments(features(strcmp(feattypes,'segment')));
        obj.hideAllSceneSurfaces(features(strcmp(feattypes,'surface')));

        obj.hideAllScenePolygonDetails(features(strcmp(feattypes,'polygon')));
        
%         obj.hideAllSceneGroups(groups);
    else
        obj.hideAllScenePoints;
        obj.hideAllScenePolylines;
        obj.hideAllScenePolygons;
        obj.hideAllSceneComplexPolygons;
        obj.hideAllSceneMeshes;
        obj.hideAllSceneCorners;
        obj.hideAllSceneEdges;
        obj.hideAllSceneSegments;
        obj.hideAllSceneSurfaces;

        obj.hideAllScenePolygonDetails;
        
%         obj.hideAllSceneGroups;
    end
end

function showAllSceneGroups(obj,groups)
    if nargin < 2
        groups = obj.ggroups{1};
    end

    if isempty(groups)
        return;
    end

    parentgroups = unique([groups.parentgroup]);
    missingparentmask = not(ismember(parentgroups,obj.ggroups{1}));
    if any(missingparentmask)
        missingparentgroups = parentgroups(missingparentmask);
        groups(ismember(groups,missingparentgroups)) = []; % remove missing parents from set of groups, because they will already be shown by the recursion
        obj.showAllSceneGroups(missingparentgroups); % recurse to show missing parent groups
    end


    [~,ginds] = ismember(groups,obj.ggroups{1});
    newinds = find(ginds==0);
    if not(isempty(newinds))
        newginds = numel(obj.ggroups{1})+1 : numel(obj.ggroups{1})+numel(newinds);
        ginds(newinds) = newginds;
        obj.ggroups{1}(newginds) = groups(newinds);
        obj.ggroups{2}(newginds) = gobjects(1,numel(newinds));
    end

    % todo: do it hierarchically, parent groups first if they do not exist, then child
    % groups

%     groups.hierarchydepth = 

    gparents = gobjects(1,numel(groups));

    parentgroups = {groups.parentgroup};
    emptyparentmask = cellfun(@(x) isempty(x), parentgroups);

    if not(all(emptyparentmask))
        nonaxesgparents = [parentgroups{:}];

        [~,nonaxesgparentind] = ismember(nonaxesgparents,obj.ggroups{1});
        if any(isnan(nonaxesgparentind))
            error('Some groups do not have a graphics parent.');
        end

        gparents = gobjects(1,numel(groups));
        gparents(not(emptyparentmask)) = obj.ggroups{2}(nonaxesgparentind);
    end
    if any(emptyparentmask)
        gparents(emptyparentmask) = obj.axes;
    end

    obj.ggroups{2}(ginds) = showSceneGroups(...
        groups,obj.ggroups{2}(ginds),gparents);
end

function hideAllSceneGroups(obj,groups)
    if nargin >= 2
        [~,inds] = ismember(groups,obj.ggroups{1});
        inds(inds==0) = [];
        delete(obj.ggroups{2}(inds(isgraphics(obj.ggroups{2}(inds)))));
        obj.ggroups{1}(inds) = [];
        obj.ggroups{2}(inds) = [];
    else
        delete(obj.ggroups{2}(isgraphics(obj.ggroups{2})));
        obj.ggroups{1} = SceneGroup.empty(1,0);
        obj.ggroups{2} = gobjects(1,0);
    end
end

function showAllScenePoints(obj,points,propids)
    if nargin < 2
        points = obj.gpoints{1};
    end

    if isempty(points)
        return;
    end

    [~,ginds] = ismember(points,obj.gpoints{1});
    newinds = find(ginds==0);
    if not(isempty(newinds))
        newginds = numel(obj.gpoints{1})+1 : numel(obj.gpoints{1})+numel(newinds);
        ginds(newinds) = newginds;
        obj.gpoints{1}(newginds) = points(newinds);
        obj.gpoints{2}(newginds) = gobjects(1,numel(newinds));
    end

    selinds = find(ismember(points,obj.scene.features(obj.selpointfeatinds)));

    % make selected point red
    vals = obj.gprops.values(propids);
    
    colors = repmat(rgbhex2rgbdec('4D4D4D')',1,numel(points));

    colors(:,selinds) = repmat(rgbhex2rgbdec('fb8072')',1,numel(selinds));

    obj.gpoints{2}(ginds) = showScenePoints(...
        points,obj.gpoints{2}(ginds),obj.featureGParents(obj.gpoints{1}(ginds)),...
        'markerfacecolor',colors);
end

function hideAllScenePoints(obj,points)
    if nargin >= 2
        [~,inds] = ismember(points,obj.gpoints{1});
        inds(inds==0) = [];
        delete(obj.gpoints{2}(inds(isgraphics(obj.gpoints{2}(inds)))));
        obj.gpoints{1}(inds) = [];
        obj.gpoints{2}(inds) = [];
    else
        delete(obj.gpoints{2}(isgraphics(obj.gpoints{2})));
        obj.gpoints{1} = ScenePoint.empty(1,0);
        obj.gpoints{2} = gobjects(1,0);
    end
end

function showAllScenePolylines(obj,polylines)
    if nargin < 2
        polylines = obj.gpolylines{1};
    end

    if isempty(polylines)
        return;
    end

    [~,ginds] = ismember(polylines,obj.gpolylines{1});
    newinds = find(ginds==0);
    if not(isempty(newinds))
        newginds = numel(obj.gpolylines{1})+1 : numel(obj.gpolylines{1})+numel(newinds);
        ginds(newinds) = newginds;
        obj.gpolylines{1}(newginds) = polylines(newinds);
        obj.gpolylines{2}(newginds) = gobjects(1,numel(newinds));
    end

    selinds = find(ismember(polylines,obj.scene.features(obj.selpolylinefeatinds)));

    % make selected polyline red
    colors = repmat([0;0;0],1,numel(polylines));
    colors(:,selinds) = repmat(rgbhex2rgbdec('4D4D4D')',1,numel(selinds));

    obj.gpolylines{2}(ginds) = showScenePolylines(...
        polylines,obj.gpolylines{2}(ginds),obj.featureGParents(obj.gpolylines{1}(ginds)),...
        'edgecolor',colors);
end

function hideAllScenePolylines(obj,polylines)
    if nargin >= 2
        [~,inds] = ismember(polylines,obj.gpolylines{1});
        inds(inds==0) = [];
        delete(obj.gpolylines{2}(inds(isgraphics(obj.gpolylines{2}(inds)))));
        obj.gpolylines{1}(inds) = [];
        obj.gpolylines{2}(inds) = [];
    else
        delete(obj.gpolylines{2}(isgraphics(obj.gpolylines{2})));
        obj.gpolylines{1} = ScenePolyline.empty(1,0);
        obj.gpolylines{2} = gobjects(1,0);
    end
end

function showAllScenePolygons(obj,polygons)
    if nargin < 2
        polygons = obj.gpolygons{1};
    end

    if isempty(polygons)
        return;
    end

    [~,ginds] = ismember(polygons,obj.gpolygons{1});
    newinds = find(ginds==0);
    if not(isempty(newinds))
        newginds = numel(obj.gpolygons{1})+1 : numel(obj.gpolygons{1})+numel(newinds);
        ginds(newinds) = newginds;
        obj.gpolygons{1}(newginds) = polygons(newinds);
        obj.gpolygons{2}(newginds) = gobjects(1,numel(newinds));
    end

    selinds = find(ismember(polygons,obj.scene.features(obj.selpolygonfeatinds)));

    % make selected polygon red
    colors = repmat(rgbhex2rgbdec('4D4D4D')',1,numel(polygons));
    colors(:,selinds) = repmat(rgbhex2rgbdec('fb8072')',1,numel(selinds));

    % thick selected lines (but slower)
    linewidths = ones(1,numel(polygons));
    linewidths(selinds) = 2;        

    obj.gpolygons{2}(ginds) = showScenePolygons(...
        polygons,obj.gpolygons{2}(ginds),obj.featureGParents(obj.gpolygons{1}(ginds)),...
        'edgecolor',mat2cell(colors,3,ones(1,size(colors,2))),...
        'edgewidth',linewidths);
end

function gparents = featureGParents(obj,features)
    parents = features.getScenegroup;
    parents = [parents{:}];
    [~,gparentind] = ismember(parents,obj.ggroups{1});
    if any(isnan(gparentind))
        error('Some features do not have a graphics parent.');
    end
    gparents = obj.ggroups{2}(gparentind);
end

function hideAllScenePolygons(obj,polygons)
    if nargin >= 2
        [~,inds] = ismember(polygons,obj.gpolygons{1});
        inds(inds==0) = [];
        delete(obj.gpolygons{2}(inds(isgraphics(obj.gpolygons{2}(inds)))));
        obj.gpolygons{1}(inds) = [];
        obj.gpolygons{2}(inds) = [];
    else
        delete(obj.gpolygons{2}(isgraphics(obj.gpolygons{2})));
        obj.gpolygons{1} = ScenePolygon.empty(1,0);
        obj.gpolygons{2} = gobjects(1,0);
    end
end

function showAllSceneComplexPolygons(obj,complexpolygons)
    if nargin < 2
        complexpolygons = obj.gcomplexpolygons{1};
    end

    if isempty(complexpolygons)
        return;
    end

    [~,ginds] = ismember(complexpolygons,obj.gcomplexpolygons{1});
    newinds = find(ginds==0);
    if not(isempty(newinds))
        newginds = numel(obj.gcomplexpolygons{1})+1 : numel(obj.gcomplexpolygons{1})+numel(newinds);
        ginds(newinds) = newginds;
        obj.gcomplexpolygons{1}(newginds) = complexpolygons(newinds);
        obj.gcomplexpolygons{2}(newginds) = gobjects(1,numel(newinds));
    end

    selinds = find(ismember(complexpolygons,obj.scene.features(obj.selcomplexpolygonfeatinds)));

    % make selected polygon red
    colors = repmat(rgbhex2rgbdec('4D4D4D')',1,numel(complexpolygons));
    colors(:,selinds) = repmat(rgbhex2rgbdec('fb8072')',1,numel(selinds));

    % thick selected lines (but slower)
    linewidths = ones(1,numel(complexpolygons));
    linewidths(selinds) = 2;        

    obj.gcomplexpolygons{2}(ginds) = showSceneComplexPolygons(...
        complexpolygons,obj.gcomplexpolygons{2}(ginds),obj.featureGParents(obj.gcomplexpolygons{1}(ginds)),...
        'edgecolor',mat2cell(colors,3,ones(1,size(colors,2))),...
        'edgewidth',linewidths);
end

function hideAllSceneComplexPolygons(obj,complexpolygons)
    if nargin >= 2
        [~,inds] = ismember(complexpolygons,obj.gcomplexpolygons{1});
        inds(inds==0) = [];
        delete(obj.gcomplexpolygons{2}(inds(isgraphics(obj.gcomplexpolygons{2}(inds)))));
        obj.gcomplexpolygons{1}(inds) = [];
        obj.gcomplexpolygons{2}(inds) = [];
    else
        delete(obj.gcomplexpolygons{2}(isgraphics(obj.gcomplexpolygons{2})));
        obj.gcomplexpolygons{1} = SceneComplexPolygon.empty(1,0);
        obj.gcomplexpolygons{2} = gobjects(1,0);
    end
end

function showAllSceneMeshes(obj,meshes)
    if nargin < 2
        meshes = obj.gmeshes{1};
    end

    if isempty(meshes)
        return;
    end

    [~,ginds] = ismember(meshes,obj.gmeshes{1});
    newinds = find(ginds==0);
    if not(isempty(newinds))
        newginds = numel(obj.gmeshes{1})+1 : numel(obj.gmeshes{1})+numel(newinds);
        ginds(newinds) = newginds;
        obj.gmeshes{1}(newginds) = meshes(newinds);
        obj.gmeshes{2}(newginds) = gobjects(1,numel(newinds));
    end

    selinds = find(ismember(meshes,obj.scene.features(obj.selmeshfeatinds)));

    % make selected corner red
    meshcolors = repmat(obj.scenecolors.mesh,1,numel(meshes));
    meshcolors(:,selinds) = repmat(obj.scenecolors.selmesh,1,numel(selinds));
    meshalphas = ones(1,numel(meshes)).*obj.scenealphas.mesh;
    meshalphas(:,selinds) = ones(1,numel(meshes)).*obj.scenealphas.selmesh;
        
    obj.gmeshes{2}(ginds) = showSceneMeshes(...
        meshes,obj.gmeshes{2}(ginds),obj.featureGParents(obj.gmeshes{1}(ginds)),...
        'facecolor',meshcolors,...
        'facealpha',meshalphas,...
        'edgecolor','none');%,... % [0.5;0.5;0.5]
%         'lightingbugWorkaround','on');
end

function hideAllSceneMeshes(obj,meshes)
    if nargin >= 2
        [~,inds] = ismember(meshes,obj.gmeshes{1});
        inds(inds==0) = [];
        delete(obj.gmeshes{2}(inds(isgraphics(obj.gmeshes{2}(inds)))));
        obj.gmeshes{1}(inds) = [];
        obj.gmeshes{2}(inds) = [];
    else
        delete(obj.gmeshes{2}(isgraphics(obj.gmeshes{2})));
        obj.gmeshes{1} = SceneMesh.empty(1,0);
        obj.gmeshes{2} = gobjects(1,0);
    end
end

function showAllSceneCorners(obj,corners)
    if nargin < 2
        corners = obj.gcorners{1};
    end

    if isempty(corners)
        return;
    end

    [~,ginds] = ismember(corners,obj.gcorners{1});
    newinds = find(ginds==0);
    if not(isempty(newinds))
        newginds = numel(obj.gcorners{1})+1 : numel(obj.gcorners{1})+numel(newinds);
        ginds(newinds) = newginds;
        obj.gcorners{1}(newginds) = corners(newinds);
        obj.gcorners{2}(newginds) = gobjects(1,numel(newinds));
    end

    selinds = find(ismember(corners,obj.scene.features(obj.selcornerfeatinds)));

    % make selected corners red
    colors = repmat([0;0;1],1,numel(corners));
    colors(:,selinds) = repmat([1;0;0],1,numel(selinds));

    obj.gcorners{2}(ginds) = showSceneCorners(...
        corners,obj.gcorners{2}(ginds),obj.featureGParents(obj.gcorners{1}(ginds)),...
        'markerfacecolor',colors);
end

function hideAllSceneCorners(obj,corners)
    if nargin >= 2
        [~,inds] = ismember(corners,obj.gcorners{1});
        inds(inds==0) = [];
        delete(obj.gcorners{2}(inds(isgraphics(obj.gcorners{2}(inds)))));
        obj.gcorners{1}(inds) = [];
        obj.gcorners{2}(inds) = [];
    else
        delete(obj.gcorners{2}(isgraphics(obj.gcorners{2})));
        obj.gcorners{1} = SceneCorner.empty(1,0);
        obj.gcorners{2} = gobjects(1,0);
    end
end

function showAllSceneEdges(obj,edges)
    if nargin < 2
        edges = obj.gedges{1};
    end

    if isempty(edges)
        return;
    end

    [~,ginds] = ismember(edges,obj.gedges{1});
    newinds = find(ginds==0);
    if not(isempty(newinds))
        newginds = numel(obj.gedges{1})+1 : numel(obj.gedges{1})+numel(newinds);
        ginds(newinds) = newginds;
        obj.gedges{1}(newginds) = edges(newinds);
        obj.gedges{2}(newginds) = gobjects(1,numel(newinds));
    end

    colors = repmat([0;0;1],1,numel(edges));
    colors(:,selinds) = repmat([1;0;0],1,numel(selinds));

    obj.gedges{2}(ginds) = showSceneSegments(...
        edges,obj.gedges{2}(ginds),obj.featureGParents(obj.gedges{1}(ginds)),...
        'edgecolor',colors,...
        'edgewidth',3);
end
function hideAllSceneEdges(obj,edges)
    if nargin >= 2
        [~,inds] = ismember(edges,obj.gedges{1});
        inds(inds==0) = [];
        delete(obj.gedges{2}(inds(isgraphics(obj.gedges{2}(inds)))));
        obj.gedges{1}(inds) = [];
        obj.gedges{2}(inds) = [];
    else
        delete(obj.gedges{2}(isgraphics(obj.gedges{2})));
        obj.gedges{1} = SceneEdge.empty(1,0);
        obj.gedges{2} = gobjects(1,0);
    end
end

function showAllSceneSegments(obj,segments)
    if nargin < 2
        segments = obj.gsegments{1};
    end

    if isempty(segments)
        return;
    end

    [~,ginds] = ismember(segments,obj.gsegments{1});
    newinds = find(ginds==0);
    if not(isempty(newinds))
        newginds = numel(obj.gsegments{1})+1 : numel(obj.gsegments{1})+numel(newinds);
        ginds(newinds) = newginds;
        obj.gsegments{1}(newginds) = segments(newinds);
        obj.gsegments{2}(newginds) = gobjects(1,numel(newinds));
    end

    selinds = find(ismember(segments,obj.scene.features(obj.selsegmentfeatinds)));

    % make selected segments red
    colors = repmat([0;0;1],1,numel(segments));
    colors(:,selinds) = repmat([1;0;0],1,numel(selinds));

    obj.gsegments{2}(ginds) = showSceneSegments(...
        segments,obj.gsegments{2}(ginds),obj.featureGParents(obj.gedges{1}(ginds)),...
        'edgecolor',colors,...
        'edgewidth',3);
end
function hideAllSceneSegments(obj,segments)
    if nargin >= 2
        [~,inds] = ismember(segments,obj.gsegments{1});
        inds(inds==0) = [];
        delete(obj.gsegments{2}(inds(isgraphics(obj.gsegments{2}(inds)))));
        obj.gsegments{1}(inds) = [];
        obj.gsegments{2}(inds) = [];
    else
        delete(obj.gsegments{2}(isgraphics(obj.gsegments{2})));
        obj.gsegments{1} = SceneBoundarySegment.empty(1,0);
        obj.gsegments{2} = gobjects(1,0);
    end
end

function showAllSceneSurfaces(obj,surfaces)
    if nargin < 2
        surfaces = obj.gsurfaces{1};
    end

    if isempty(surfaces)
        return;
    end

    [~,ginds] = ismember(surfaces,obj.gsurfaces{1});
    newinds = find(ginds==0);
    if not(isempty(newinds))
        newginds = numel(obj.gsurfaces{1})+1 : numel(obj.gsurfaces{1})+numel(newinds);
        ginds(newinds) = newginds;
        obj.gsurfaces{1}(newginds) = surfaces(newinds);
        obj.gsurfaces{2}(newginds) = gobjects(1,numel(newinds));
    end

    selinds = find(ismember(surfaces,obj.scene.features(obj.selsurfacefeatinds)));

    % make selected corner red
    surfacecolors = repmat(rgbhex2rgbdec('bebada')',1,numel(surfaces));
    surfacecolors(:,selinds) = repmat(rgbhex2rgbdec('6f5aff')',1,numel(selinds)); 

    edgecolors = repmat({'none'},1,numel(surfaces));
    edgecolors(selinds) = mat2cell(surfacecolors(:,selinds),3,numel(selinds));

    obj.gsurfaces{2}(ginds) = showSceneSurfaces(...
        surfaces,obj.gsurfaces{2}(ginds),obj.featureGParents(obj.gsurfaces{1}(ginds)),...,...
        'facecolor',surfacecolors,...
        'edgecolor',edgecolors);
end

function hideAllSceneSurfaces(obj,surfaces)
    if nargin >= 2
        [~,inds] = ismember(surfaces,obj.gsurfaces{1});
        inds(inds==0) = [];
        delete(obj.gsurfaces{2}(inds(isgraphics(obj.gsurfaces{2}(inds)))));
        obj.gsurfaces{1}(inds) = [];
        obj.gsurfaces{2}(inds) = [];
    else
        delete(obj.gsurfaces{2}(isgraphics(obj.gsurfaces{2})));
        obj.gsurfaces{1} = SceneSurface.empty(1,0);
        obj.gsurfaces{2} = gobjects(1,0);
    end
end

function showAllSceneVerts(obj)
    if not(isempty(obj.gpolygons{1}))
        if isempty(obj.gverts) || not(isgraphics(obj.gverts))
            obj.gverts = patch([0,0],[0,0],0,...
                'Parent',obj.axes,...
                'FaceColor','none',...
                'LineStyle','none',...
                'Marker','o',...
                'MarkerFaceColor','blue',...
                'Visible','off');
            obj.gverts.addprop('Stackz').SetMethod = @changeStackz;
            obj.gverts.Stackz = -0.1; % above lines does not work with line smoothing
        end

%         allpoints = cell2mat([obj.scene.polygons.visrep]);
        allverts = cell2mat(obj.gpolygons{1}.verts);
        set(obj.gverts,...
            'XData',allverts(1,:),...
            'YData',allverts(2,:),...
            'ZData',zeros(1,size(allverts,2)),...    
            'Visible','on');
    else
        obj.hideAllSceneVerts;
    end
end

function hideAllSceneVerts(obj)
    if isgraphics(obj.gverts)
        delete(obj.gverts)
    end
end

function showAllSceneNormals(obj)
    if not(isempty(obj.gpolygons{1})) || not(isempty(obj.gmeshes{1}))
        if isempty(obj.gnormals) ||  not(isgraphics(obj.gnormals))
            obj.gnormals = line([0,0],[0,0],...
                'Parent',obj.axes,...
                'Color','blue',...
                'Visible','off');
            
            obj.gnormals.addprop('Stackz').SetMethod = @changeStackz;
            obj.gnormals.Stackz = 0.35;
        end
        
        allverts = cell(1,0);
        allnormals = cell(1,0);
        
        if not(isempty(obj.gpolygons{1}))
            allverts{end+1} = {obj.gpolygons{1}.verts};
            allnormals{end+1} = cell(1,numel(allverts));

            polylinesclosed = [obj.gpolygons{1}.closed];
            for i=1:numel(allverts{end})
                allnormals{end}{i} = polylineNormals(...
                    allverts{end}{i},polylinesclosed(i));
            end
            allnormals{end} = [allnormals{end}{:}];
            allverts{end} = [allverts{end}{:}];
            allnormals{end}(3,:) = 0;
            allverts{end}(3,:) = 0;
        end
        
        if not(isempty(obj.gmeshes{1}))
            allverts{end+1} = [allverts,{obj.gmeshes{1}.verts}];
            allnormals{end+1} = [allnormals,{obj.gmeshes{1}.vnormals}];
            
            for i=1:numel(allverts{end})
                if isempty(allnormals{end}{i})
                    allnormals{end}{i} = trimeshVertexNormals(obj.gmeshes{1}(i).verts,obj.gmeshes{1}(i).faces,'faceavg_area');
                end
            end
            
            allverts{end} = [allverts{end}{:}];
            allnormals{end} = [allnormals{end}{:}];
        end
        
        allverts = [allverts{:}];
        allnormals = [allnormals{:}];
        
        [~,~,sceneextent] = pointsetBoundingbox(allverts);
        displaysize = sceneextent*0.01;
                     
        xdata = [allverts(1,:);allverts(1,:)+allnormals(1,:).*displaysize;nan(1,size(allverts,2))];
        xdata = xdata(:)';
        ydata = [allverts(2,:);allverts(2,:)+allnormals(2,:).*displaysize;nan(1,size(allverts,2))];
        ydata = ydata(:)';
        zdata = [allverts(3,:);allverts(3,:)+allnormals(3,:).*displaysize;nan(1,size(allverts,2))];
        zdata = zdata(:)';
        set(obj.gnormals,'XData',xdata,'YData',ydata,'ZData',zdata,'Visible','on');
    else
        obj.hideAllSceneNormals;
    end
end

function hideAllSceneNormals(obj)
    delete(obj.gnormals(isgraphics(obj.gnormals)));
end

function showAllSceneCurvature(obj)
    
    if not(isempty(obj.gpolygons{1}))
        if isempty(obj.gnormals) ||  not(isgraphics(obj.gnormals))
            obj.gnormals = line([0,0],[0,0],...
                'Parent',obj.axes,...
                'Color','blue',...
                'Visible','off');
            obj.gnormals.addprop('Stackz').SetMethod = @changeStackz;
            obj.gnormals.Stackz = 0.35;
        end
        
        polylines = {obj.gpolygons{1}.verts};
        polylinesclosed = [obj.gpolygons{1}.closed];
        
        allverts = cell2mat(polylines);
        allnormals = [];
        allcurv = [];
        for i=1:numel(polylines)
            [polylinecurv,~,polylinenormals,~] = polylineDiffProperties(...
                polylines{i},polylinesclosed(i));
            allnormals = [allnormals,polylinenormals]; %#ok<AGROW>
            allcurv = [allcurv,polylinecurv]; %#ok<AGROW>
        end
        
        length = sign(allcurv).*log(abs(allcurv).*100+1);
        
        xdata = [allverts(1,:);allverts(1,:)+allnormals(1,:).*length.*10;nan(1,size(allverts,2))];
        xdata = xdata(:)';
        ydata = [allverts(2,:);allverts(2,:)+allnormals(2,:).*length.*10;nan(1,size(allverts,2))];
        ydata = ydata(:)';
        set(obj.gnormals,'XData',xdata,'YData',ydata,'Visible','on');
    else
        obj.hideAllSceneCurvature;
    end
end

function hideAllSceneCurvature(obj)
    if isgraphics(obj.gnormals)
        delete(obj.gnormals);
    end
end

function showAllScenePolygonDetails(obj,polygons)
       
    if nargin < 2
        polygons = obj.gpolygondetails{1};
    end

    if isempty(polygons)
        return;
    end

    [~,ginds] = ismember(polygons,obj.gpolygondetails{1});
    newinds = find(ginds==0);
    if not(isempty(newinds))
        newginds = numel(obj.gpolygondetails{1})+1 : numel(obj.gpolygondetails{1})+numel(newinds);
        ginds(newinds) = newginds;
        obj.gpolygondetails{1}(newginds) = polygons(newinds);
        obj.gpolygondetails{2}(newginds) = gobjects(1,numel(newinds));
    end
        
    obj.gpolygondetails{2}(ginds) = showScenePolygonDetails(...
        polygons,obj.gpolygondetails{2}(ginds),obj.featureGParents(obj.gpolygondetails{1}(ginds)));
end

function hideAllScenePolygonDetails(obj,polygons)
    if nargin >= 2
        [~,inds] = ismember(polygons,obj.gpolygondetails{1});
        inds(inds==0) = [];
        delete(obj.gpolygondetails{2}(inds(isgraphics(obj.gpolygondetails{2}(inds)))));
        obj.gpolygondetails{1}(inds) = [];
        obj.gpolygondetails{2}(inds) = [];
    else
        delete(obj.gpolygondetails{2}(isgraphics(obj.gpolygondetails{2})));
        obj.gpolygondetails{1} = ScenePolygon.empty(1,0);
        obj.gpolygondetails{2} = gobjects(6,0);
    end
end

% function showAllScenePolygonSkeletons(obj,polygons)
%     if not(isempty(obj.scene))
% 
%         polygons = obj.scene.features(obj.selpolygonfeatinds);
%         polygons(not(polygons.hasSkeleton)) = [];
%         skels = [polygons.skeleton];
%         
%         if isempty(skels)
%             obj.hideAllScenePolygonSkeletons;
%             return;
%         end
%         
%         obj.gpolygonskeletons = showScenePolygonSkeletons(...
%             {skels},obj.gpolygonskeletons,obj.axes);
%     end
% end
% 
% function hideAllScenePolygonSkeletons(obj)
%     delete(obj.gpolygonskeletons(isgraphics(obj.gpolygonskeletons)));
%     obj.gpolygonskeletons = gobjects(1,0);
% end

function showAllSceneLights(obj)
    
    if not(isempty(obj.scene))
    
        if numel(obj.glights) ~= 3 || any(not(isgraphics(obj.glights)))
            delete(obj.glights(isgraphics(obj.glights)));
            obj.glights = gobjects(1,0);
            obj.glights(1) = light('Parent',obj.axes,'Style','infinite');
            obj.glights(2) = light('Parent',obj.axes,'Style','infinite');
%             obj.glights(3) = light('Parent',obj.axes,'Style','infinite','Color',[0.5,0.5,0.5]); % temp
        end

        % for skyscrapers
        [~,~,bbdiag] = obj.scene.boundingbox;
        
        bbdiag = max(bbdiag,10);

%         light1pos = [75.3255;143.8211;110];
%         light2pos = [-75.3255;-143.8211;110];

        light1pos = [bbdiag,bbdiag*2,-bbdiag*1.5]; % negative z because of lighting bug
        light2pos = [-bbdiag,-bbdiag*2,-bbdiag*1.5]; % negative z because of lighting bug
%         light3pos = [0,bbdiag*2,-bbdiag*1.5]; % negative z because of lighting bug

        obj.glights(1).Position = light1pos';
        obj.glights(2).Position = light2pos';
%         obj.glights(3).Position = light3pos';
    
%         show dummy points to see where the lights are
%         delete(obj.hlightdummytemp(isvalid(obj.hlightdummytemp)));
%         obj.hlightdummytemp = gobjects(1,0);
%         obj.hlightdummytemp(1) = patch('XData',light1pos(1),'YData',light1pos(2),'ZData',light1pos(3),'Parent',obj.axes,'LineStyle','none','Marker','o','MarkerFaceColor','red');
%         obj.hlightdummytemp(2) = patch('XData',light2pos(1),'YData',light2pos(2),'ZData',light2pos(3),'Parent',obj.axes,'LineStyle','none','Marker','o','MarkerFaceColor','blue');
    else
        obj.hideAllSceneLights;
    end
end

function hideAllSceneLights(obj)
    delete(obj.glights(isgraphics(obj.glights)));
    obj.glights = gobjects(1,0);
end

function show(obj)
    obj.showAllSceneGroups;
    
    obj.showAllSceneFeatures;
    
    if obj.normalsVisible
        obj.showAllSceneNormals;
    end
    if obj.curvatureVisible
        obj.showAllSceneCurvature;
    end
    if obj.verticesVisible
        obj.showAllSceneVerts;
    end
    if obj.lightsVisible
        obj.showAllSceneLights;
    end
end

function hide(obj)
    obj.hideAllSceneNormals;
    obj.hideAllSceneCurvature;
    obj.hideAllSceneVerts;
    obj.hideAllSceneLights;
    
    obj.hideAllSceneFeatures;
    
    obj.hideAllSceneGroups;
end

end

end
