% show polygons
% Polygons may be grouped together in cells, in this case all polygons in a
% cell are created as a single patch object to improve performance.
% Note that for filled polygons, this requires creating a n x d_max matrix,
% where n is the number of polygons in the cell and d_max the maximum
% number of vertices of any polygon in the cell. If polygons are displayed
% without a fill, there is no overhead and it should always be faster. Note
% that some properties (such as the line width) can only be specified per
% patch.
function gobjs = showScenePolygons(polys,gobjs,parent,varargin)
    
%     if not(iscell(polys))
%         polys = num2cell(polys);
%     end

    if iscell(polys)
        verts = cell(1,numel(polys));
        for i=1:numel(polys)
            verts{i} = {polys{i}.verts};
        end
    else
        verts = {polys.verts};
    end
    
    options = nvpairs2struct(varargin);
    if isfield(options,'facecolor') && not(strcmp(options.facecolor,'none'))

        gobjs = showFaces(verts,gobjs,parent,...
            varargin{:});
        
        % todo: if multiple components are present, triangulating is
        % probably better to save space
    else
        
%         % close polygon boundary (last edge) when showing only edges
%         for i=1:numel(verts)
%             if iscell(verts{i})
%                 for j=1:numel(verts{i})
%                     verts{i}{j} = verts{i}{j}(:,[1:end,1]);
%                 end
%             else
%                 verts{i} = verts{i}(:,[1:end,1]);
%             end
%         end
        
        [verts,varargin] = closeGraphicsPolylines(verts,varargin);

        
        gobjs = showPolygons(verts,gobjs,parent,...
            'facecolor','none',... % default for non-filled polygons
            'edgecolor',[0;0;0],... % default for non-filled polygons
            varargin{:});
    end
end
