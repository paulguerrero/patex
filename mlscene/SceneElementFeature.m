classdef (Abstract) SceneElementFeature < SceneFeature

properties(SetAccess={?SceneElementFeature,?SceneImporter,?SceneExporter}, SetObservable, AbortSet)
	parentelement = [];
end

properties(Access={?SceneElementFeature,?SceneImporter,?SceneExporter})
    p_labels = [];
end

methods
    
% SceneElementFeature()
% SceneElementFeature(obj2)
% SceneElementFeature(parentelement)
% SceneElementFeature(parentelement,labels)
function obj = SceneElementFeature(varargin)
    obj@SceneFeature;
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneElementFeature')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 1
%         obj.parentelement = varargin{1};
        obj.setParentelement(varargin{1});
    elseif numel(varargin) == 2
%         obj.parentelement = varargin{1};
        obj.setLabels(varargin{2});
        obj.setParentelement(varargin{1});
    else
        error('Invalid arguments.');
    end
end
    
function copyFrom(obj,obj2)
	obj.copyFrom@SceneFeature(obj2);
    
    obj.parentelement = obj2.parentelement;
    obj.p_labels = obj2.p_labels;
end

end

methods(Sealed)

function setParentelement(obj,val)
    
    if not(iscell(val))
        val = {val};
    end
    
    mask = false(1,numel(obj));
    if numel(val) == 1
        for k=1:numel(obj)
            mask(k) = numel(obj(k).parentelement) ~= numel(val{1}) || any(obj(k).parentelement ~= val{1});
        end
    else
        for k=1:numel(obj)
            mask(k) = numel(obj(k).parentelement) ~= numel(val{k}) || any(obj(k).parentelement ~= val{k});
        end
    end
    obj = obj(mask);
    
    if not(isempty(obj))
        
        % scenegroup might change, convert labels to group-independent string
        mask = obj.hasScenegroup;
        if any(mask)
            oldgroups = obj(mask).getScenegroup;
            obj(mask).labelsToStr;
        else
            oldgroups = cell(1,numel(obj));
        end
        
        if numel(val) == 1
            for k=1:numel(obj)
                o = obj(k);
                o.parentelement = val{1};
            end
        else
            for k=1:numel(obj)
                o = obj(k);
                o.parentelement = val{k};
            end
        end
        
        % convert label back
        mask = obj.hasScenegroup;
        if any(mask)
            newgroups = obj(mask).getScenegroup;
            obj(mask).labelsToInd;
        else
            newgroups = cell(1,numel(obj));
        end
        
        % update groups that changed (where a feature was transfered from
        % one group to the other)
        changedmask = false(1,numel(oldgroups));
        for i=1:numel(oldgroups)
            changedmask(i) = numel(oldgroups{i}) ~= numel(newgroups{i}) || any(oldgroups{i} ~= newgroups{i});
        end
        grps = unique([oldgroups{changedmask},newgroups{changedmask}]);
        for i=1:numel(grps)
            grps(i).updateElements;
        end
            
        obj.dirty;
    end
end

function b = hasParentelement(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).parentelement));
    end
end

end

methods(Access=protected)
    
function b = isElementImpl(obj)
    b = false(1,numel(obj));
end

function b = isElementFeatureImpl(obj)
    b = true(1,numel(obj));
end
    
function dirtyImpl(obj)
    obj.dirtyImpl@SceneFeature;
end

function updateLocalImpl(obj)
    obj.updateLocalImpl@SceneFeature;
end

function fp = getScenegroupImpl(obj)
    fp = cell(1,numel(obj));
    hasparentmask = obj.hasParentelement;
    if any(hasparentmask)
        parentelms = [obj(hasparentmask).parentelement];
        fp(hasparentmask) = parentelms.getScenegroup;
%         fp(hasparentmask) = {parentelms.scenegroup};
    end
    if not(all(hasparentmask))
        fp(not(hasparentmask)) = repmat({SceneGroup.empty},1,sum(not(hasparentmask)));
    end
end
function setScenegroupImpl(obj,fp) %#ok<INUSD>
    error('Cannot set scene group of element feature, need to set scene group of parent element instead.');
end
function b = hasScenegroupImpl(obj)
    b = false(1,numel(obj));
%     hasparentmask = obj.hasParentelement;
%     if any(hasparentmask)
%         parentelms = [obj(hasparentmask).parentelement];
%         b(hasparentmask) = parentelms.hasScenegroup;
%     end
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).parentelement)) && not(isempty(obj(k).parentelement.scenegroup));
    end
end

function l = getLabelsImpl(obj)
    l = cell(1,numel(obj));
    
    % has own labels => return own labels
    haslblmask = obj.hasLabels;
    if any(haslblmask)
        l(haslblmask) = {obj(haslblmask).p_labels};
    end
    
    % no labels => return parentelement labels
    hasparentmask = obj.hasParentelement;
    mask = not(haslblmask) & hasparentmask;
    if any(mask)
        parentelms = [obj(mask).parentelement];
        l(mask) = parentelms.getLabels;
    end
    
    % no labels and no parent element => return empty (cell(1,0)) labels
    mask = not(haslblmask) & not(hasparentmask);
    if any(mask)
        l(mask) = {cell(1,0)};
    end
end
function setLabelsImpl(obj,l)
    if iscellstr(l) || isreal(l)
        l = repmat({l},1,numel(obj));
    end
    
    for k=1:numel(obj)
        o = obj(k);
        o.p_labels = l{k};
    end
end
function b = hasLabelsImpl(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        o = obj(k);
        b(k) = not(isempty(o.p_labels));
    end
end
    
end

% methods(Abstract)
%     dirty(obj);
% end

end % classdef
