classdef ScenePolyline < ScenePointset2D

properties(SetAccess={?ScenePolyline,?SceneImporter,?SceneExporter})
    segments = SceneBoundarySegment.empty(0,0); % custom segments
end

properties(Dependent,SetAccess=protected)
    closed;
    cornerthresh;
    
    corners;
    edges;
end

properties(SetObservable,AbortSet,Access={?ScenePointset2D,?SceneImporter,?SceneExporter})
    p_closed = [];
    p_cornerthresh = pi/8;
end

properties(Access={?ScenePolyline,?SceneImporter,?SceneExporter})
    % SceneFeature.empty(0,0) => not requested (and not computed)
    % SceneFeature.empty(1,0) => computed, but has zero corners, etc.
    % SceneFeature.empty(0,1) => dirty
    p_corners = SceneCorner.empty(0,0);
    p_edges = SceneEdge.empty(0,0);
end

methods(Static)

function n = type_s
    n = 'polyline';
end

function d = dimension_s
    d = 1;
end

end

methods
    
function obj = ScenePolyline(verts,labels,closed)
    obj@ScenePointset2D;
%     obj.type = SceneFeature.POLYLINE;
    
    if nargin == 0
        % do nothing (default values)
    elseif nargin == 1 && isa(verts,'ScenePolyline')
        obj2 = verts;
        
        obj.copyFrom(obj2);
    elseif nargin >= 2
    
        obj.setLabels(labels);
        
        if nargin < 3
            [verts,closed] = polylineClose(verts);
        end
        
        obj.setVerts(verts,closed);
        
        obj.definePosition(obj.center);
        obj.defineOrientation(0);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
    end
end

function obj2 = clone(obj)
    obj2 = ScenePolyline(obj);
end

function delete(obj)
    if not(isempty(obj.p_corners))
        delete(obj.p_corners(isvalid(obj.p_corners)));
    end
    if not(isempty(obj.p_edges))
        delete(obj.p_edges(isvalid(obj.p_edges)));
    end
    if not(isempty(obj.segments))
        delete(obj.segments(isvalid(obj.segments)));
    end
end

function copyFrom(obj,obj2)
    obj.copyFrom@ScenePointset2D(obj2);

    % corners (copy conservatively)
    obj.p_corners = SceneFeature.copyFeatsarrayFrom(obj.p_corners,obj2.p_corners);
    if not(isempty(obj.p_corners))
        obj.p_corners.setParentelement(obj);
    end
    
    % edges
    obj.p_edges = SceneFeature.copyFeatsarrayFrom(obj.p_edges,obj2.p_edges);
    if not(isempty(obj.p_edges))
        obj.p_edges.setParentelement(obj);
    end
    
    % segments
    obj.segments = SceneFeature.copyFeatsarrayFrom(obj.segments,obj2.segments);
    if not(isempty(obj.segments))
        obj.segments.setParentelement(obj);
    end
end

function setVerts(obj,verts,closed)
    
    obj.setVerts@ScenePointset2D(verts);
    
    obj.closed = closed;
    
    % can keep segments (they just rely on normalized start/end arclen)
    if not(isempty(obj.segments))
        obj.segments.dirty;
    end

    obj.dirty;
end

function c = get.closed(obj)
    c = obj.getClosedImpl;
end

function set.closed(obj,c)
    obj.setClosedImpl(c);
end

function c = get.cornerthresh(obj)
    c = obj.getCornerthreshImpl;
end

function set.cornerthresh(obj,c)
    obj.setCornerthreshImpl(c);
end

function val = get.corners(obj)
    if size(obj.p_corners,1) == 0 && size(obj.p_corners,2) == 1 
        obj.updateCorners;
    end
    
    val = obj.p_corners;
end
function val = getCorners(obj)
    mask = obj.cornersDirty;
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updateCorners;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updateCorners;
                end
            end
        end
    end
    val = {obj.p_corners};
end
function b = cornersComputed(obj)
%     b = not(isempty(obj.p_corners));
%     b = arrayfun(@(x) isa(x.p_corners,'SceneCorner'),obj);
    b = false(1,numel(obj));
    for i=1:numel(obj)
%         b(i) = isa(obj(i).p_corners,'SceneCorner');
        b(i) = size(obj(i).p_corners,1) == 1;
    end
end
function b = cornersDirty(obj)
%     b = not(isempty(obj.p_corners));
%     b = arrayfun(@(x) isa(x.p_corners,'SceneCorner'),obj);
    b = false(1,numel(obj));
    for i=1:numel(obj)
%         b(i) = isa(obj(i).p_corners,'SceneCorner');
        b(i) = size(obj(i).p_corners,1) == 0 && size(obj(i).p_corners,2) == 1 ;
    end
end

function val = get.edges(obj)
    if size(obj.p_edges,1) == 0 && size(obj.p_edges,2) == 1 
        obj.updateEdges;
    end
    
    val = obj.p_edges;
end
function val = getEdges(obj)
    mask = obj.edgesDirty;
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updateEdges;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updateEdges;
                end
            end
        end
    end
    val = {obj.p_edges};
end
function b = edgesComputed(obj)
%     b = not(isempty(obj.p_edges));
%     b = arrayfun(@(x) isa(x.p_edges,'SceneEdge'),obj);
    b = false(1,numel(obj));
    for i=1:numel(obj)
%         b(i) = isa(obj(i).p_edges,'SceneEdge');
        b(i) = size(obj(i).p_edges,1) == 1;
    end
end
function b = edgesDirty(obj)
%     b = not(isempty(obj.p_edges));
%     b = arrayfun(@(x) isa(x.p_edges,'SceneEdge'),obj);
    b = false(1,numel(obj));
    for i=1:numel(obj)
%         b(i) = isa(obj(i).p_edges,'SceneEdge');
        b(i) = size(obj(i).p_edges,1) == 0 && size(obj(i).p_edges,2) == 1;
    end
end

% function b = segmentsDirty(obj)
%     b = false(size(obj)); % always false, because segments are never dirty (cannot be updated)
% end


function segs = addSegments(obj,startarclen,endarclen,labels)
    if nargin >= 4
        obj.segments(end+1) = SceneBoundarySegment(obj,startarclen,endarclen,labels);
        segs = obj.segments(end);
    elseif nargin >= 3
        obj.segments(end+1) = SceneBoundarySegment(obj,startarclen,endarclen);
        segs = obj.segments(end);
    elseif nargin >=2 && isa(startarclen,'SceneBoundarySegment')
        segs = startarclen;
        if not(isempty(segs))
            segs.setParentelement(obj);
            obj.segments(end+1:end+numel(segs)) = segs;
        end
    else
        error('Invalid arguments.');
    end
end

function segs = removeSegments(obj,segmentinds)
    if nargin < 2
        segmentinds = 1:numel(obj.segments);
    end
    
    if not(isnumeric(segmentinds))
        segmentinds = find(ismember(obj.segments,segmentinds));
    end
    
    segs = obj.segments(segmentinds);
    obj.segments(segmentinds) = [];
%     obj.p_subfeatures = [];
    
    if nargout < 1
        delete(segs(isvalid(segs)));
    else
        if not(isempty(segs))
            segs.setParentelement(SceneMesh.empty);
        end
    end
end

% function updateOrientedbbox(obj)
%     for k=1:numel(obj)
%         o = obj(k);
%         
%         o.p_orientedbbox = zeros(2,3);
% 
%         if not(isempty(o.verts))
%             angleaxes = zeros(2,1);
%             [angleaxes(1),angleaxes(2)] = pol2cart(o.orientation,1);
%             angleaxes = [angleaxes,[-angleaxes(2);angleaxes(1)]]; %#ok<AGROW> % orthogonal direction is rotated ccw (like cartesian y axis)
%             anglecoords = angleaxes'*o.verts;
% 
%             minanglecoords = min(anglecoords,[],2);
%             maxanglecoords = max(anglecoords,[],2);
% 
%             o.p_orientedbbox(:,1) = angleaxes*minanglecoords;
%             o.p_orientedbbox(:,2) = (maxanglecoords(1)-minanglecoords(1)).*angleaxes(:,1);
%             o.p_orientedbbox(:,3) = (maxanglecoords(2)-minanglecoords(2)).*angleaxes(:,2);
%         end
%     end
% end
% function updateConvexhull(obj)
%     for k=1:numel(obj)
%         o = obj(k);
% 
%         pts = o.verts;
%         
%         if not(isempty(pts))
%             hinds = convhull(pts(1,:),pts(2,:));
%             if not(isempty(hinds))
%                 hinds(end) = []; % remove endindex (because it is = startindex)
%             end
%             o.p_convexhull = pts(:,hinds);
%         else
%             o.p_convexhull = zeros(2,0);
%         end
%     end
% end
function updatePrincipalcomps(obj)
    for k=1:numel(obj)
        o = obj(k);
    
        if isempty(o.verts)
            o.p_principalcomps = zeros(2,0);
        else
            vertweight = polylineArclen(o.verts,o.closed);
            vertweight = diff(vertweight);
            vertweight = (vertweight([end,1:end-1]) + vertweight)./2;

            o.p_principalcomps = pca(o.verts','Weights',vertweight);
        end
    end
end
% function updatePrincipalbbox(obj)
%     
%     mask = not(obj.hasPrincipalcomps);
%     if any(mask)
%         obj(mask).updatePrincipalcomps;
%     end
%     
%     for k=1:numel(obj)
%         o = obj(k);
%         o.p_principalbbox = zeros(2,3);
% 
%         pcacoords = o.principalcomps'*o.verts;
% 
%         minpcacoords = min(pcacoords,[],2);
%         maxpcacoords = max(pcacoords,[],2);
% 
%         o.p_principalbbox(:,1) = o.principalcomps*minpcacoords;
%         o.p_principalbbox(:,2) = (maxpcacoords(1)-minpcacoords(1)).*o.principalcomps(:,1);
%         o.p_principalbbox(:,3) = (maxpcacoords(2)-minpcacoords(2)).*o.principalcomps(:,2);
%     end
% end

function updateCorners(obj)
    
    for k=1:numel(obj)
        o = obj(k);
        
        delete(o.p_corners(isvalid(o.p_corners)));
        o.p_corners = SceneCorner.empty(1,0);

        angles = polylineAngles(o.verts,o.closed);
        arclens = polylineArclen(o.verts,o.closed);
        if o.closed
            cornerinds = find(abs(angles) >= o.cornerthresh);
        else
            % if not closed, first and last vertex are always a corner
            cornerinds = [1,find(abs(angles) >= o.cornerthresh)+1,size(o.verts,2)];
        end
        [curv,~,~,~] = polylineDiffProperties(o.verts,o.closed);

    %     seganglethresh = pi/16;
        seganglethresh = pi/32;
        seglenthresh = 1/4;

        % analyze segments to find (approx.) straight lines and split segments into
        % straight and rounded parts
        if o.closed
            lastcornerind = numel(cornerinds);
        else
            lastcornerind = numel(cornerinds)-1;
        end
        newcornerinds = [];
        for i=1:lastcornerind
            startvert = cornerinds(i);
            endvert = cornerinds(mod(i,numel(cornerinds))+1);
            if endvert > startvert
                segverts = startvert:endvert;
                segalen = arclens(endvert) - arclens(startvert);
            else
                segverts = [startvert:size(o.verts,2),1:endvert];
                segalen = arclens(endvert) + arclens(end) - arclens(startvert);
            end

            cumangle = 0;
            alen = 0;

            for j=1:numel(segverts)-2
                alen = alen + arclens(segverts(j)+1)-arclens(segverts(j));
                cumangle = cumangle + angles(segverts(j+1));
                curvratio = max(curv(segverts(j))/curv(segverts(j+1)),...
                                curv(segverts(j+1))/curv(segverts(j)));
                if abs(cumangle) > seganglethresh && curvratio > 5
                    if alen > segalen*seglenthresh
                        newcornerinds(end+1) = segverts(j+1); %#ok<AGROW>
                    end
                    cumangle = 0;
                    alen = 0;
                end
            end
        end

        cornerinds = [cornerinds,newcornerinds]; %#ok<AGROW>
        cornerinds = sort(cornerinds,'ascend');

        % merge very short segments to the adjacent segment to which it has the
        % smallest angle

        for i=1:numel(cornerinds)
            o.p_corners(i) = SceneCorner(o,cornerinds(i));
        end
    
    end
end
function updateEdges(obj)
    
%     mask = not(obj.hasCorners);
%     if any(mask)
%         obj(mask).updateCorners;
%     end
    
    mask = obj.cornersDirty |  not(obj.cornersComputed);
    if any(mask)
        obj(mask).updateCorners;
    end
    
    for k=1:numel(obj)
        o = obj(k);
        
        delete(o.p_edges(isvalid(o.p_edges)));
        o.p_edges = SceneEdge.empty(1,0);

        startcornerinds = [o.corners.cornervertex];
        if not(isempty(startcornerinds))
            if not(o.closed)
                startcornerinds(end) = [];
            end
            for i=1:numel(startcornerinds)
                o.p_edges(i) = SceneEdge(o,startcornerinds(i));
            end
        end
    end
end
% 
% function updateCenter(obj)
%     for k=1:numel(obj)
%         o = obj(k);
%         
%         o.p_center = mean(o.verts,2);
%     end
% end
% function updatePcorientation(obj)
%     for k=1:numel(obj)
%         o = obj(k);
%         
% %         [coeff,~] = pca(o.verts');
% %         [o.p_pcorientation,~] = cart2pol(coeff(1,1),coeff(2,1));
%         
%         pcomps = obj.principalcomps;
%         o.p_pcorientation = cart2pol(pcomps(1,1),pcomps(2,1));
%     end
% end
% function updateExtent(obj)
%     extents = computeExtent(obj,obj.getOrientation);
%     for k=1:numel(obj)
%         o = obj(k);
%         
%         o.p_extent = extents(:,k);
%         
% %         % compute maximum extent of polyline in local x and y direction
% %         [xaxis(1,1),xaxis(2,1)] = pol2cart(o.orientation,1);
% %         yaxis = [-xaxis(2);xaxis(1)];
% % 
% %         verts_local = [xaxis';yaxis'] * o.verts;
% %         o.p_extent = max(verts_local,2) - min(verts_local,2);
%     end
% end
% function val = computeExtent(obj,orient)
%     val = zeros(2,numel(obj));
%     for k=1:numel(obj)
%         o = obj(k);
%         
%         if isempty(o.verts)
%             val(:,k) = 0;
%         else
%             % compute maximum extent of mesh in local x, y and z direction
%             [xaxis(1,1),xaxis(2,1)] = pol2cart(orient(:,k),1);
%             yaxis = [-xaxis(2);xaxis(1)];
% 
%             verts_local = [xaxis';yaxis'] * o.verts;
%             val(:,k) = max(verts_local,[],2) - min(verts_local,[],2);
%         end
%     end
% end
% function updateRadius(obj)
%     for k=1:numel(obj)
%         o = obj(k);
% 
%         o.p_radius = max(sqrt(sum(bsxfun(@minus,o.verts,o.position).^2,1)));
%     end
% end
function updateSymmetries(obj)
%     mask = not(obj.hasPcorientation);
%     if any(mask)
%         obj(mask).updatePcorientation;
%     end
%     mask = not(obj.hasCentroid);
%     if any(mask)
%         obj(mask).updateCentroid;
%     end
    
    for k=1:numel(obj)
        o = obj(k);

        arclen = polylineArclen(o.verts,o.closed);
        darclen = differential1(arclen,o.closed);
        o.p_symmetries = pointsetSymmetries(...
            o.verts,...
            o.position,...
            o.orientation,...
            darclen);
    end
end

end

methods(Access=protected)
    
% function val = defaultPoseImpl(obj)
%     orient = [obj.pcorientation];
%     val = [...
%         [obj.center];...
%         orient;...
%         obj.computeExtent(orient);...
%         false(1,numel(obj))];
% end
    
function dirtyImpl(obj)
    obj.dirtyImpl@ScenePointset2D;
    
    for k=1:numel(obj)
        o = obj(k);
        
%         o.p_center = [];
%         o.p_pcorientation = [];
%         o.p_extent = [];
%         o.p_radius = [];
%         o.p_orientedbbox = [];
%         o.p_convexhull = [];
%         o.p_principalcomps = [];
%         o.p_principalbbox = [];    
        
        
        if obj.cornersComputed
            delete(o.p_corners(isvalid(o.p_corners)));
            o.p_corners = SceneCorner.empty(0,1);
        end
        if obj.edgesComputed
            delete(o.p_edges(isvalid(o.p_edges)));
            o.p_edges = SceneEdge.empty(0,1);
        end
    end
end
    
function updateLocalImpl(obj)
    obj.updateLocalImpl@ScenePointset2D;
    
    mask = obj.cornersDirty;
    if any(mask)
        obj(mask).updateCorners;
    end
    mask = obj.edgesDirty;
    if any(mask)
        obj(mask).updateEdges;
    end
end
    
% function updateBoundingboxImpl(obj)
%     for i=1:numel(obj)
%         o = obj(i); % for performance
%         v = o.verts;
%         o.p_boundingboxmin = min(v,[],2);
%         o.p_boundingboxmax = max(v,[],2);
%     end
% end

% % pose must be 2D
% function [center,width,height,orientation] = posedBoundingbox2DImpl(obj,framepose,featpose)
%     
%     if nargin < 2 || isempty(framepose)
%         framepose = [...
%             zeros(numel(obj));...
%             zeros(numel(obj));...
%             zeros(numel(obj));...
%             ones(numel(obj));...
%             zeros(numel(obj))];
%     end
%     if nargin < 2 || isempty(featpose)
%         featpose = obj.getPose2D;
%     end
%     
%     center = zeros(2,numel(obj));
%     width = zeros(1,numel(obj));
%     height = zeros(1,numel(obj));
%     orientation = zeros(1,numel(obj));
%     for i=1:numel(obj)
%         v = posetransform(obj(i).verts,SceneElement.pose3Dto2D(featpose(:,i)),obj(i).pose2D);
%         v = posetransform(v,identpose2D,framepose(:,i));
% 
%         bbmin = min(v,[],2);
%         bbmax = max(v,[],2);
%         center(:,i) = (bbmin + bbmax) .* 0.5;
%         width(i) = bbmax(1) - bbmin(1);
%         height(i) = bbmax(2) - bbmin(2);
%         
%         center(:,i) = posetransform(center(:,i),framepose(:,i),identpose2D);
% 
%         width(i) = width(i) * framepose(4,i);
%         height(i) = height(i) * framepose(5,i);
%         orientation(i) = framepose(3,i);
%     end
% end
    
% relations 0 and 2 are never returned, because only relevant poly indices
% (i.e. feature is inside poly or intersecting) are returned
% relation: 0: feature and poly are separate
% relation: 1: feature completely inside poly
% relation: 2: poly completely inside feature
% relation: 3: feature and poly are intersecting
function [polyinds,relation] = inPolygonImpl(obj,poly,polycandidates)

    polyinds = cell(numel(obj),1);
    if nargout >= 2
        relation = cell(numel(obj),1);
    end
    if not(isempty(poly)) && not(isempty(obj))
        [polybbmin,polybbmax] = poly.boundingbox;
        [mybbmin,mybbmax] = obj.boundingbox;

        for j=1:numel(obj)
            
            % find polygons with bounding boxes containing the polyline
            if nargin < 3 || isempty(polycandidates)
                polyinds{j} = find(...
                    polybbmin(1,:) < mybbmax(1,j) & polybbmin(2,:) < mybbmax(2,j) & ...
                    polybbmax(1,:) > mybbmin(1,j) & polybbmax(2,:) > mybbmin(2,j));
            else
                polyinds{j} = polycandidates{j}(...
                    polybbmin(1,polycandidates{j}) < mybbmax(1,j) & polybbmin(2,polycandidates{j}) < mybbmax(2,j) & ...
                    polybbmax(1,polycandidates{j}) > mybbmin(1,j) & polybbmax(2,polycandidates{j}) > mybbmin(2,j));
            end
            
            % find all polygons containing at least part of the polyline
            p = obj(j).verts(:,1);
            if obj(j).closed
                pts = obj(j).verts(:,[1:end,1]);
            else
                pts = obj(j).verts;
            end
            keepmask = false(1,numel(polyinds{j}));
            if nargout >= 2
                relation{j} = zeros(1,numel(polyinds{j}));
            end
            for i=1:numel(polyinds{j})
                % test if part of the polyline is inside the polygon
%                 intersects = linesegLinesegIntersect(...
%                     [pts(:,1:end-1);pts(:,2:end)]',...
%                     [poly(polyinds{j}(i)).verts;...
%                      poly(polyinds{j}(i)).verts(:,[2:end,1])]');
%                 intersecting = any(intersects.intAdjacencyMatrix(:) == 1);
                intersects = linesegLinesegIntersection2D_mex(...
                    pts(:,1:end-1),pts(:,2:end),...
                    poly(polyinds{j}(i)).verts,...
                    poly(polyinds{j}(i)).verts(:,[2:end,1]));
                intersecting = any(intersects(:) > 0);
                
                pointinpoly = pointInPolygon_mex(p(1),p(2),poly(polyinds{j}(i)).verts(1,:),poly(polyinds{j}(i)).verts(2,:));

                keepmask(i) = intersecting | pointinpoly;
                
                if nargout >= 2
                    if intersecting
                        relation{j}(i) = 3;
                    elseif pointinpoly
                        relation{j}(i) = 1;
                    end
                end
            end
            polyinds{j} = polyinds{j}(keepmask);
            if nargout >= 2
                relation{j} = relation{j}(keepmask);
            end
        end
    end
end
    
function [d,cp] = pointDistanceImpl(obj,p,pose)
    
    if size(p,1) == 2
        d = inf(numel(obj),size(p,2));
        cp = zeros(numel(obj),size(p,2),2);
        for i=1:numel(obj)
            v = obj(i).verts;
            if nargin >= 3 && not(isempty(pose)) && not(all(pose{i}==obj(i).pose))
                v = posetransform(v,pose{i},obj(i).pose);
            end
            
            if obj(i).closed
                [d(i,:),~,~,cp(i,:,1),cp(i,:,2)] = pointPolylineDistance_mex(p(1,:),p(2,:),...
                    v(1,[1:end,1]),v(2,[1:end,1]));
            else
                [d(i,:),~,~,cp(i,:,1),cp(i,:,2)] = pointPolylineDistance_mex(p(1,:),p(2,:),...
                    v(1,:),v(2,:));
            end
        end
    elseif size(p,1) == 3
        % assume z-coordinates of polyline are zero
        d = inf(numel(obj),size(p,2));
        cp = zeros(numel(obj),size(p,2),3);
        for i=1:numel(obj)
            v = [obj(i).verts;zeros(1,size(obj(i).verts,2))];
            if nargin >= 3 && not(isempty(pose))
                if not(all(pose{i}==obj(i).pose3D))
                    v = posetransform(v,pose{i},obj(i).pose3D);
                end
            else
                if not(isempty(obj.scenegroup))
                    grouppose = obj.scenegroup.globaloriginpose;
                    v = transform3D(v,grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
                end
            end
            
            if obj(i).closed
                lsegstart = v;
                lsegend = v(:,[2:end,1]);
            else
                lsegstart = v(:,1:end-1);
                lsegend = v(:,2:end);
            end
            [dists,t] = pointLinesegDistance3D_mex(p,...
                lsegstart,lsegend);
            [d(i,:),mininds] = min(dists,[],2);
            mininds = mininds';
            mininds_lin = sub2ind(size(t),1:size(t,1),mininds);
            
            % get closest points
            t = t(mininds_lin);
            cp(i,:,:) = permute(lsegstart(:,mininds) + ...
                bsxfun(@times,lsegend(:,mininds)-lsegstart(:,mininds),t),[3,2,1]);
        end
    else
        error('Only implemented for two or three dimensions.');
    end
end

function [d,t,cp] = linesegDistanceImpl(obj,p1,p2,pose) % distance to a 3D line segment

    if size(p1,1) == 2
        error('Not yet implemented.');
        % todo: implement a function like linesegLinesegDistance_mex (2D)
    elseif  size(p1,1) == 3
        d = inf(numel(obj),size(p1,2));
        t = nan(numel(obj),size(p1,2));
        cp = zeros(numel(obj),size(p1,2),3);
        for i=1:numel(obj)
            v = [obj(i).verts;zeros(1,size(obj(i).verts,2))];
            if nargin >= 4 && not(isempty(pose))
                if not(all(pose{i}==obj(i).pose3D))
                    v = posetransform(v,pose{i},obj(i).pose3D);
                end
            else
                if not(isempty(obj(i).scenegroup))
                    grouppose = obj(i).scenegroup.globaloriginpose;
                    v = transform3D(v,grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
                end
            end

            if obj(i).closed
                lsegstart = v;
                lsegend = v(:,[2:end,1]);
            else
                lsegstart = v(:,1:end-1);
                lsegend = v(:,2:end);
            end
            [dists,t1,t2] = linesegLinesegDistance3D_mex(p1,p2,...
                lsegstart,lsegend);
            
%             [d(i,:),mininds] = min(dists,[],2);
%             t(i,:) = t1(sub2ind(size(t1),1:size(t1,1),mininds));
            
            % get element with minimum distance (and minimum t if there
            % is more than one element with the same minimum distance)
            mindist = min(dists,[],2);
            t1candidates = t1;
            t1candidates(not(bsxfun(@eq,dists,mindist))) = nan;
            [t(i,:),mininds] = min(t1candidates,[],2);
            mininds = mininds';
            mininds_lin = sub2ind(size(t1),1:size(t1,1),mininds);

            d(i,:) = dists(mininds_lin);
            
            % get closest points
%             t2 = t2(sub2ind(size(t2),1:size(t2,1),mininds));
            t2 = t2(mininds_lin);
            cp(i,:,:) = permute(lsegstart(:,mininds) + ...
                bsxfun(@times,lsegend(:,mininds)-lsegstart(:,mininds),t2),[3,2,1]);
        end
    else
        error('Only implemented for two or three dimensions.');
    end
end

function feats = subfeaturesImpl(obj)
    mask = not(obj.hasSubfeatures);
    if any(mask)
        obj(mask).updateSubfeatures;
    end
    
    feats = cell(1,numel(obj));
    for k=1:numel(obj)
        feats{k} = [obj(k).p_corners,obj(k).p_edges,obj(k).segments];
    end
end
function b = hasSubfeaturesImpl(obj)
    b = not(obj.cornersDirty | obj.edgesDirty);
end
function updateSubfeaturesImpl(obj)
    obj.updateCorners;
    obj.updateEdges;
end

function moveAboutOriginImpl(obj,movevec)
    
    obj.moveAboutOriginImpl@ScenePointset2D(movevec);
    
    % corners, edges and segments may store intermediate results that need
    % to be invalidated
    if not(isempty(obj.p_corners))
        obj.p_corners.dirty;
    end
    if not(isempty(obj.p_edges))
        obj.p_edges.dirty;
    end
    if not(isempty(obj.segments))
        obj.segments.dirty;
    end
end

function rotateAboutOriginImpl(obj,rotangle)
    
    obj.rotateAboutOriginImpl@ScenePointset2D(rotangle);

    obj.p_principalbbox = [];
    
    % corners, edges and segments may store intermediate results that need
    % to be invalidated
    if not(isempty(obj.p_corners))
        obj.p_corners.dirty;
    end
    if not(isempty(obj.p_edges))
        obj.p_edges.dirty;
    end
    if not(isempty(obj.segments))
        obj.segments.dirty;
    end
    

end

function scaleAboutOriginImpl(obj,relscale)
    
    obj.scaleAboutOriginImpl@ScenePointset2D(relscale);
    
    if not(isempty(obj.p_corners))
        obj.p_corners.dirty;
    end
    if not(isempty(obj.p_edges))
        obj.p_edges.dirty;
    end
    if not(isempty(obj.segments))
        obj.segments.dirty;
    end
end

function mirrorAboutOriginImpl(obj,mirchange)
    
    if not(mirchange)
        return;
    end
    
    obj.mirrorAboutOriginImpl@ScenePointset2D(mirchange);
    
    % corners, edges and segments may store intermediate results that need
    % to be invalidated
    if not(isempty(obj.p_corners))
        obj.p_corners.dirty;
    end
    if not(isempty(obj.p_edges))
        obj.p_edges.dirty;
    end
    if not(isempty(obj.segments))
        obj.segments.dirty;
    end
end

function s = similarityImpl(obj,obj2)
    s = ones(numel(obj),numel(obj2)); % all polylines have similarity 1 to each other for now
end

function c = getClosedImpl(obj)
    c = obj.p_closed;
end

function setClosedImpl(obj,c)
    obj.p_closed = c;
end

function c = getCornerthreshImpl(obj)
    c = obj.p_cornerthresh;
end

function setCornerthreshImpl(obj,c)
    obj.p_cornerthresh = c;
end

end

end % classdef
