classdef SceneExporter < handle
    
properties(SetAccess=protected)
    scene = Scene.empty;
    
    svgnode = [];
    canvasbbox = [];
    
    % scene elements, features and groups
    surfaces = {};
    groups = {}; % may have more or less groups than surfaces (not all groups must be onn surfaces and not all surfaces must have groups)
    elements = {};
    features = {};
    
%     surfaceids
    settings = struct;
    
    offmeshfilenames = cell(1,0);
end

methods(Static)

function s = defaultSettings
    s = struct;
    
    s.flipobjy = false;
    s.flipobjxy = false;
    s.flipobjyz = false;
    s.flipyz = false;
    s.rotztoy = false;
    s.rotytoz = false;
    s.format = '';
    s.localcoords = false; % save each off meshes in its local coordinate frame
end

end

methods
    
% obj = SceneExporter()
% obj = SceneExporter(scene)
% obj = SceneExporter(scene,settings)
% obj = SceneExporter(scene,nvpairs)
function obj = SceneExporter(varargin)
    if numel(varargin) == 0
        obj.settings = SceneExporter.defaultSettings;
    elseif numel(varargin) == 1
        obj.scene = varargin{1};
        obj.settings = SceneExporter.defaultSettings;
    elseif numel(varargin) == 2 && isstruct(varargin{2})
        obj.scene = varargin{1};
        obj.settings = varargin{2};
    elseif ischar(varargin{2})
        obj.scene = varargin{1};
        obj.settings = SceneExporter.defaultSettings;
        obj.settings = nvpairs2struct(varargin(2:end),obj.settings);
    else
        error('Invalid arguments.');
    end
end

function clear(obj)
    obj.svgnode = [];
    obj.canvasbbox = [];
    obj.surfaces = {};
    obj.groups = {};
    obj.elements = {};
    obj.features = {};
end

function export(obj,filename,varargin)
    
    if not(isempty(varargin)) && isstruct(varargin{1})
        obj.settings = varargin{1};
    elseif not(isempty(varargin))
        obj.settings = nvpairs2struct(varargin,obj.settings);
    else
        % do nothing
    end
    
    if isempty(obj.settings.format)
        
        if not(ischar(filename))
            error('Cannot determine format from file extension, must specify format.');
        end
        
        % get format from file extension
        [~,~,ext] = fileparts(filename);
        if strcmp(ext,'.svg')
            obj.settings.format = 'svg';
        elseif strcmp(ext,'.obj')
            obj.settings.format = 'obj';
        elseif strcmp(ext,'.off')
            obj.settings.format = 'off';
        elseif strcmp(ext,'.dae')
            obj.settings.format = 'collada';
        else
            error('Unknown file extension for exporting scene.');
        end
    end
    
    if strcmp(obj.settings.format,'svg')
        obj.writesvgscene(filename);
    elseif strcmp(obj.settings.format,'obj')
        obj.writeobjscene(filename);
    elseif strcmp(obj.settings.format,'off')
        obj.writeoffscene(filename);
    elseif strcmp(obj.settings.format,'collada')
        obj.writecolladascene(filename);
    else
        error('Unknown scene export format.');
    end
end

function writecolladascene(obj,filename)
    [meshes,materials,nodes] = obj.scene2colladastructs(obj.scene);
    
    % should be renamed to writeMlsceneCollada (is not Open3D specific) and
    % o3dcollada should be mlscenecollada and should be a separate module
    writeO3dCollada_mex(filename,meshes,materials,nodes);
end

function [meshes,materials,nodes] = scene2colladastructs(obj,scene) %#ok<INUSL>
    % currently only meshes and surfaces are exported
    elms = scene.meshes;
    
%     % get surfaces that are attached to the elms (and the elms they are
%     % attached to)
%     surfs = {elms.surfaces};
%     surfmeshinds = cellind_mex(surfs);
    
    % meshes
    surfs = cell(1,numel(elms));
    surfmeshinds = cell(1,numel(elms));
    surfsurfinds = cell(1,numel(elms));
    meshes = cell(1,numel(elms));
    for i=1:numel(elms)
        meshes{i} = struct;
        meshes{i}.name = elms(i).name;
        meshes{i}.verts = elms(i).verts;
        meshes{i}.faces = elms(i).faces;
        if not(isempty(elms(i).facevnormalinds))
            meshes{i}.vnormals = elms(i).vnormals;
            meshes{i}.facevnormalinds = elms(i).facevnormalinds;
        end
        if not(isempty(elms(i).facetexcoordinds))
            meshes{i}.texcoords = elms(i).texcoords;
            meshes{i}.facetexcoordinds = elms(i).facetexcoordinds;
        end
        
        surfs{i} = elms(i).surfaces;
        surfmeshinds{i} = ones(1,numel(surfs{i})).*i;
        surfsurfinds{i} = 1:numel(surfs{i});
        
        meshes{i}.surffaceinds = {surfs{i}.faceindices};
        meshes{i}.surfvparams = {surfs{i}.vparams};
        meshes{i}.surfnames = {surfs{i}.name};
    end
    
    surfs = [surfs{:}];
    surfmeshinds = [surfmeshinds{:}];
    surfsurfinds = [surfsurfinds{:}];

    % materials
    mats = SceneMaterial.empty(1,0);
    for i=1:numel(elms)
        mats = [mats,elms(i).materials]; %#ok<AGROW>
    end
    mats = unique(mats);

    materials = cell(1,numel(mats));
    for i=1:numel(mats)
        materials{i}.name = mats(i).name;
        materials{i}.ambient = mats(i).ambient;
        materials{i}.diffuse = mats(i).diffuse;

        if not(isempty(mats(i).specular))
            materials{i}.specular = mats.specular;
        end
        if not(isempty(mats(i).transmittance))
            materials{i}.transmittance = mats.transmittance;
        end
        if not(isempty(mats(i).emission))
            materials{i}.emission = mats.emission;
        end
        if not(isempty(mats(i).shininess))
            materials{i}.shininess = mats.shininess;
        end
        if not(isempty(mats(i).ior))
            materials{i}.ior = mats.ior;
        end
        if not(isempty(mats(i).ambient_texname))
            materials{i}.ambient_texname = mats.ambient_texname;
        end
        if not(isempty(mats(i).diffuse_texname))
            materials{i}.diffuse_texname = mats.diffuse_texname;
        end
        if not(isempty(mats(i).specular_texname))
            materials{i}.specular_texname = mats.specular_texname;
        end
        if not(isempty(mats(i).normal_texname))
            materials{i}.normal_texname = mats.normal_texname;
        end
    end

    % groups
    grps = scene.groups;

    % sort by hierarchy depth
    hdepths = [grps.hierarchydepth];
    [~,perm] = sort(hdepths,'ascend');
    grps = grps(perm);
    hdepths = hdepths(perm); %#ok<NASGU>

    nodes = cell(1,numel(grps));
    for i=1:numel(grps)
        nodes{i}.name = grps(i).name;
        nodes{i}.transform = pose2mat(grps(i).originpose);
        if isempty(grps(i).parentgroup)
            nodes{i}.parentind = 0;
        else
            pgroupind = find(grps == grps(i).parentgroup);
            if numel(pgroupind) ~= 1
                error('Must also include all ancestor groups when writing a group.');
            end
            if pgroupind >= i
                error('Groups are not ordered correctly, parent group is after child group.');
            end

            nodes{i}.parentind = pgroupind;
        end
        if isempty(grps(i).meshes)
            nodes{i}.meshinds = zeros(1,0);
        else
            [~,nodes{i}.meshinds] = ismember(grps(i).meshes,elms);
        end

        nodes{i}.materialinds = zeros(1,numel(nodes{i}.meshinds));
        for j=1:numel(nodes{i}.meshinds)
            mesh = elms(nodes{i}.meshinds(j));
            if isempty(mesh.materials)
                nodes{i}.materialinds(j) = 0;
            else
                if numel(mesh.materials) > 1
                    warning('Mesh has more than one material, currently only one material per mesh is supported, skipping remaining materials.');
                end
                material = mesh.materials(1);

                matind = find(mats == material);
                if numel(matind) ~= 1
                    error('Material of mesh not found (or duplicates detected).');
                end

                nodes{i}.materialinds(j) = matind;
            end
        end
        
        if isa(grps(i),'SceneSurfaceGroup') && not(isempty(grps(i).surface))
            sind = find(surfs==grps(i).surface,1,'first');
            nodes{i}.surfind = [surfmeshinds(sind),surfsurfinds(sind)];
        else
            nodes{i}.surfind = [];
        end
    end
end

function writeoffscene(obj,filename)
    
    meshes = obj.scene.meshes;
    
    if iscell(filename)
        % filename for each mesh given
        filenames = filename;
%         perm = 1:numel(filenames);
    elseif exist(filename,'dir') == 7
        % directory given to store all mesh files
        filedir = filename;
        filenames = SceneExporter.generateoffmeshfilenames(meshes,filedir);
    else
        % single filename given (for a single mesh)
        filenames = {filename};
%         perm = 1:numel(filenames);
    end
    
    if numel(filenames) ~= numel(meshes)
        error('Number of file names does not match number of meshes.');
    end
    
    % generate comments that store the mesh surfaces
    comments = cell(1,numel(meshes));
    for i=1:numel(meshes)
        comments{i} = cell(1,numel(meshes(i).surfaces)+1);
        meshlabels = meshes(i).labelStrs;
        if cellfun(@(x) any(x == ' '),meshlabels)
            error('Cannot store labels with spaces in the OFF file format.');
        end
        comments{i}{1} = [' _label_ ',sprintf('%s ',meshlabels{:})];
        for j=1:numel(meshes(i).surfaces)
            surf = meshes(i).surfaces(j);
            comments{i}{j+1} = [' _surface_ ',sprintf('%lu ',surf.faceindices)];
            if not(isempty(surf.vparams))
                comments{i}{j+1} = [comments{i}{j+1},'_vparams_ ',sprintf('%lu ',surf.vparams)];
            end
        end
    end
    
    for i=1:numel(meshes)
        group = meshes(i).scenegroup;
        meshpose = meshes(i).pose3D;
        meshglobalpose = group.globalpose(meshpose);
        
        if obj.settings.localcoords
            % transform to local mesh coordinates
            verts = posetransform(meshes(i).verts,identpose3D,meshpose);
        else
            % transform to global coordinates
            verts = posetransform(meshes(i).verts,meshglobalpose,meshpose);
        end
        faces = meshes(i).faces;
        
        if obj.settings.rotztoy
            % rotate z to y
            verts = [1,0,0; 0,0,1; 0,-1,0] * verts;
        end
        
        writeoffmesh(filenames{i},verts,faces,comments{i});
    end
    
%     % undo the permutation
%     filenames(perm) = filenames;
    
    obj.offmeshfilenames = cell(1,numel(filenames));
    for i=1:numel(filenames)
        [~,fname,fext] = fileparts(filenames{i});
        obj.offmeshfilenames{i} = [fname,fext];
    end
end

function writeobjscene(obj,filename)
    scenegroups = obj.scene.groups;
    verts = cell(1,0);
    vnormals = cell(1,0);
    vtexcoords = cell(1,0);
    faces = cell(1,0);
    facevnormalinds = cell(1,0);
    facetexcoordinds = cell(1,0);
    meshnames = cell(1,0);
    smoothgroups = cell(1,0);
    matgroups = cell(1,0);
    matnames = cell(1,0);
    
    maxbatchverts = 1000000;
    
    for i=1:numel(scenegroups)
        group = scenegroups(i);
        groupmeshes = group.meshes;
        meshposes = groupmeshes.getPose3D;
        meshglobalposes = group.globalpose(meshposes);
        
        for j=1:numel(groupmeshes)
            if isempty(groupmeshes(j).name)
                error('Mesh without name found.');
            else
                meshnames{:,end+1} = groupmeshes(j).name; %#ok<AGROW>
            end
            verts{:,end+1} = posetransform(groupmeshes(j).verts,meshglobalposes(:,j),meshposes(:,j)); %#ok<AGROW>
            vnormals{:,end+1} = posetransformNormal(groupmeshes(j).vnormals,meshglobalposes(:,j),meshposes(:,j)); %#ok<AGROW>
            vtexcoords{:,end+1} = groupmeshes(j).texcoords; %#ok<AGROW>
            faces{:,end+1} = groupmeshes(j).faces; %#ok<AGROW>
            facevnormalinds{:,end+1} = groupmeshes(j).facevnormalinds; %#ok<AGROW>
            facetexcoordinds{:,end+1} = groupmeshes(j).facetexcoordinds; %#ok<AGROW>
            if not(isempty(groupmeshes(j).facesmoothgroupind))
                faceinds = 1:numel(groupmeshes(j).facesmoothgroupind);
                mask = groupmeshes(j).facesmoothgroupind >= 1;
                smoothgroups{:,end+1} = array2cell_mex(...
                    faceinds(mask),groupmeshes(j).facesmoothgroupind(mask),[],2); %#ok<AGROW>
            else
                smoothgroups{:,end+1} = {}; %#ok<AGROW>
            end
            if not(isempty(groupmeshes(j).facematerialind))
                faceinds = 1:numel(groupmeshes(j).facematerialind);
                mask = groupmeshes(j).facematerialind >= 1;
                matgroups{:,end+1} = array2cell_mex(...
                    faceinds(mask),groupmeshes(j).facematerialind(mask),[],2); %#ok<AGROW>
            else
                matgroups{:,end+1} = {}; %#ok<AGROW>
            end
            
            matnames{:,end+1} = {groupmeshes(j).materials.name}; %#ok<AGROW>
        
            if obj.settings.flipobjy
                verts{:,end}(2,:) = -verts{:,end}(2,:);
                vnormals{:,end}(2,:) = -vnormals{:,end}(2,:);
            end
            if obj.settings.flipobjxy
                verts{:,end} = verts{:,end}([2,1,3],:);
                vnormals{:,end} = vnormals{:,end}([2,1,3],:);
            end
            if obj.settings.flipobjyz
                verts{:,end} = verts{:,end}([1,3,2],:);
                vnormals{:,end} = vnormals{:,end}([1,3,2],:);
            end
            if obj.settings.rotytoz
                verts{:,end} = [1,0,0; 0,0,-1; 0,1,0] * verts{:,end};
                vnormals{:,end} = [1,0,0; 0,0,-1; 0,1,0] * vnormals{:,end};
            end
            if obj.settings.rotztoy
                verts{:,end} = [1,0,0; 0,0,-1; 0,1,0]' * verts{:,end};
                vnormals{:,end} = [1,0,0; 0,0,-1; 0,1,0]' * vnormals{:,end};
            end
        end
    end
    
    writeobjmesh_mex(...
        filename,verts,vnormals,vtexcoords,...
        faces,facevnormalinds,facetexcoordinds,...
        meshnames,smoothgroups,matgroups,matnames,maxbatchverts);
end

function writesvgscene(obj,filename)
    obj.clear;

    doc = com.mathworks.xml.XMLUtils.createDocument('svg');
    doc.setDocumentURI(['file:/',filename]);
    
    obj.writeSceneSvgNode(doc.getDocumentElement,[]);
    
    xmlwrite(filename,doc);
end

function node = writeSceneSvgNode(obj,node,parentnode)
    obj.writeSvgNode(node,parentnode);
    
    % write scene nodes and svg geometry
    obj.writeSceneNode([],node);
end

function node = writeSvgNode(obj,node,parentnode)
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('svg');
        parentnode.appendChild(node);
    end
    
    [canvasmin,canvasmax] = obj.scene.boundingbox;
    if isempty(canvasmin)
        % empty scene
        canvasmin = [-1;-1];
        canvasmax = [1;1];
    end
    canvasmin = canvasmin(1:2);
    canvasmax = canvasmax(1:2);
    obj.canvasbbox = [canvasmin,canvasmax];
    
    % write svg header
    node.setAttribute('version','1.1');
    node.setAttribute('xmlns','http://www.w3.org/2000/svg');
    node.setAttribute('xmlns:xlink','http://www.w3.org/1999/xlink');
    node.setAttribute('x',[num2str(canvasmin(1),10),'px']);
    node.setAttribute('y',[num2str(canvasmin(2),10),'px']);
    node.setAttribute('width',[num2str(canvasmax(1) - canvasmin(1),10),'px']);
    node.setAttribute('height',[num2str(canvasmax(2) - canvasmin(2),10),'px']);
    
    obj.svgnode = node;
end

function node = writeSceneNode(obj,node,parentnode)
    
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('scene');
        parentnode.appendChild(node);
    end
    
    node.setAttribute('name',obj.scene.name);
    
    % write categories
    categoriesNode = node.getOwnerDocument.createElement('categories');
    for i=1:numel(obj.scene.categories)
        catNode = node.getOwnerDocument.createElement('category');
        catNode.setAttribute('name',obj.scene.categories{i});
%         if isfield(obj.scene.catattributes,'walkable')
%             catNode.setAttribute('walkable',num2str(obj.scene.catattributes(i).walkable));
%         end
%         if isfield(obj.scene.catattributes,'isroom')
%             catNode.setAttribute('isroom',num2str(obj.scene.catattributes(i).isroom));
%         end
        categoriesNode.appendChild(catNode);
    end
    node.appendChild(categoriesNode);

    % write groups
    groupsNode = node.getOwnerDocument.createElement('groups');
    node.appendChild(groupsNode);
    for i=1:numel(obj.scene.rootgroups)
        obj.writeSceneGroup(...
            obj.scene.rootgroups(i),[],groupsNode);
    end
end

function node = writeSceneGroup(obj,group,node,parentnode)
    
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('group');
        parentnode.appendChild(node);
        node.setAttribute('grpid',sprintf('%.30g',numel(obj.groups)+1));
        obj.groups{end+1} = group;
    end
    
    % write group attributes
    node.setAttribute('name',group.name);
    node.setAttribute('pose',sprintf('%.30g,',group.originpose));
    
    % write elements
%     grpsurfindoffset = numel(obj.surfaces);
    elementsNode = node.getOwnerDocument.createElement('elements');
    node.appendChild(elementsNode);
    grpelms = group.elements;
%     % remove planar proxies
%     if not(isempty(group.meshes))
%         grpelms(ismember(grpelms,[group.meshes.planarproxy])) = [];
%     end
    for i=1:numel(grpelms)
        obj.writeSceneFeature(grpelms(i),[],elementsNode);
    end
%     grpsurfinds = grpsurfindoffset+1 : numel(obj.surfaces);
    
%     % write texture layers
%     texlayersNode = node.getOwnerDocument.createElement('texturelayers');
%     node.appendChild(texlayersNode);
%     for i=1:size(group.texlayers,4)
% 
%         id = obj.svgnode.getChildNodes().getLength()+1;
%         
%         coords = SceneExporter.swapy([...
%             group.texmin(:,i),...
%             [group.texmax(1,i);group.texmin(2,i)],...
%             [group.texmin(1,i);group.texmax(2,i)]],...
%             obj.canvasbbox(:,1),obj.canvasbbox(:,2));
%         rgba = group.texlayers(:,:,:,i);
%         attr = containers.Map('KeyType','char','ValueType','char');
%         attr('id') = num2str(id);
%         writesvgimages({coords},{rgba},{attr},obj.svgnode);
%         delete(attr);
%         
%         tlayNode = node.getOwnerDocument.createElement('layer');
%         tlayNode.setAttribute('id',num2str(id));
%         texlayersNode.appendChild(tlayNode);
%     end
    
    % write child groups
    groupsNode = node.getOwnerDocument.createElement('groups');
    node.appendChild(groupsNode);
    for i=1:numel(group.childgroups)
        obj.writeSceneGroup(group.childgroups(i),[],groupsNode);
    end
end

function node = writeSceneFeature(obj,feat,node,parentnode)
%     feattype = SceneFeatureType.fromID(feat.type);
    
    switch feat.type_s
        case 'corner'
            node = obj.writeCorner(elm,node,parentnode);
        case 'segment'
            node = obj.writeSegment(elm,node,parentnode);
        case 'edge'
            node = obj.writeEdge(elm,node,parentnode);
        case 'surface'
            node = obj.writeSurface(elm,node,parentnode);
        case 'polygon'
            node = obj.writeScenePolygon(feat,node,parentnode);
        case 'polyline'
            node = obj.writeScenePolyline(feat,node,parentnode);
        case 'point'
            node = obj.writeScenePoint(feat,node,parentnode);
        case 'mesh'
            node = obj.writeSceneMesh(feat,node,parentnode);
        otherwise
            warning('Unknown scene feature type, skipping.');
    end
end

function node = writeSceneFeatureBase(obj,feat,node) %#ok<INUSL>
    node.setAttribute('name',feat.name);
    
    if feat.hasLabels
        featlabels = feat.labelStrs;
        if numel(featlabels) > 1
            error('Multiple labels currently not supported by svg scene files.')
        end
        node.setAttribute('label',featlabels{1});
    end
end

function node = writeSceneElementBase(obj,elm,node)
    
    obj.writeSceneFeatureBase(elm,node);
    
    if isa(elm,'SceneElement3D')
        node.setAttribute('position',[sprintf('%.30g',elm.position(1)),',',sprintf('%.30g',elm.position(2)),',',sprintf('%.30g',elm.position(3))]);
        node.setAttribute('orientation',[sprintf('%.30g',elm.orientation(1)),',',sprintf('%.30g',elm.orientation(2)),',',sprintf('%.30g',elm.orientation(3)),',',sprintf('%.30g',elm.orientation(4))]);
        node.setAttribute('size',[sprintf('%.30g',elm.size(1)),',',sprintf('%.30g',elm.size(2)),',',sprintf('%.30g',elm.size(3))]);
        node.setAttribute('mirrored',sprintf('%.30g',elm.mirrored));
    elseif isa(elm,'SceneElement2D')
        node.setAttribute('position',[sprintf('%.30g',elm.position(1)),',',sprintf('%.30g',elm.position(2))]);
        node.setAttribute('orientation',sprintf('%.30g',elm.orientation));
        node.setAttribute('size',[sprintf('%.30g',elm.size(1)),',',sprintf('%.30g',elm.size(2))]);
        node.setAttribute('mirrored',sprintf('%.30g',elm.mirrored));
    else
        error('Unkown element type.');
    end
    
end

function node = writeScenePolygon(obj,poly,node,parentnode)
    
    if isempty(node) == isempty(parentnode)
        error('must pass either node or parentnode.');
    end

    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('poly');
        parentnode.appendChild(node);
        node.setAttribute('featid',sprintf('%.30g',numel(obj.features)+1));
        obj.features{end+1} = poly;
        node.setAttribute('elmid',sprintf('%.30g',numel(obj.elements)+1));
        obj.elements{end+1} = poly;
    end
    
    obj.writeScenePolyline(poly,node,[]);
    
    % write svg geometry for visrep polylines
%     idoffset = obj.svgnode.getChildNodes().getLength();
%     ids = idoffset+1:idoffset+numel(poly.visrep);
%     canvasdiag = sqrt(sum((obj.canvasbbox(:,2)-obj.canvasbbox(:,1)).^2,1));
%     shapetype = {};
%     coords = {};
%     attr = {};
%     for j=1:numel(poly.visrep)
%         if poly.visrepclosed(j)
%             shapetype{end+1} = 'polygon'; %#ok<AGROW>
% %             coords{end+1} = SceneExporter.swapy(...
% %                 poly.visrep{j}(:,[1:end,1]),obj.canvasbbox(:,1),obj.canvasbbox(:,2)); %#ok<AGROW>
%         else
%             shapetype{end+1} = 'polyline'; %#ok<AGROW>
% %             coords{end+1} = SceneExporter.swapy(...
% %                 poly.visrep{j},obj.canvasbbox(:,1),obj.canvasbbox(:,2)); %#ok<AGROW>
%         end
%         coords{end+1} = SceneExporter.swapy(...
%             poly.visrep{j},obj.canvasbbox(:,1),obj.canvasbbox(:,2)); %#ok<AGROW>
%         attr{end+1} = containers.Map('KeyType','char','ValueType','char'); %#ok<AGROW>
%         attr{end}('fill') = 'none';
%         attr{end}('stroke') = '#000000';
%         attr{end}('stroke-width') = num2str(canvasdiag.*0.001);
%         attr{end}('id') = num2str(ids(j));
%     end
%     writesvggeometry(shapetype,coords,attr,obj.svgnode);
%     for i=1:numel(attr)
%         delete(attr{i});
%     end
%     
%     % polyline ids
%     pidstr = sprintf('%.30g,',ids);
%     pidstr(end) = [];
%     node.setAttribute('visrepids',pidstr);
    

%     if poly.hasSkeleton
%         obj.writePolygonSkeleton(poly.skeleton,[],node);
%     end
%     if poly.hasFreeareabounds
%         fastr = sprintf('%.30g,',poly.freeareabounds(:)');
%         fastr(end) = [];
%         node.setAttribute('freeareabound',fastr);
%     end
%     if poly.hasInsetpaths && poly.hasInsetpathorigin
%         opstr = sprintf('%.30g,',poly.insetpaths(:)');
%         opstr(end) = [];
%         node.setAttribute('offsetpaths',opstr);
%         opostr = sprintf('%.30g,',poly.insetpathorigin(:)');
%         opostr(end) = [];
%         node.setAttribute('offsetpathorigin',opostr);
%     end
%     if poly.hasCellgraph
%         for i=1:numel(poly.cellgraph.cells)
%             cellNode = node.getOwnerDocument.createElement('cell');
%             clstr = sprintf('%.30g,',poly.cellgraph.cells{i}(:)');
%             clstr(end) = [];
%             cellNode.setAttribute('inds',clstr);
%             node.appendChild(cellNode);
%         end
%         clvstr = sprintf('%.30g,',poly.cellgraph.cellverts(:)');
%         clvstr(end) = [];
%         node.setAttribute('cellverts',clvstr);
%     end
end

function node = writePolygonSkeleton(obj,skel,node,parentnode) %#ok<INUSL>
    
    if isempty(node) == isempty(parentnode)
        error('must pass either node or parentnode.');
    end
    
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('skeleton');
        parentnode.appendChild(node);
    end
    
    node.setAttribute('verts',sprintf('%.30g,',skel.verts));
    node.setAttribute('edges',sprintf('%.30g,',skel.edges));
    node.setAttribute('verttime',sprintf('%.30g,',skel.verttime));
    
    node.setAttribute('boundaryvertinds',sprintf('%.30g,',skel.boundaryvertinds));
    node.setAttribute('prunededgemask',sprintf('%.30g,',double(skel.prunededgemask)));
end

function node = writeScenePolyline(obj,pline,node,parentnode)
   
    if isempty(node) == isempty(parentnode)
        error('must pass either node or parentnode.');
    end
    
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('pline');
        parentnode.appendChild(node);
        node.setAttribute('featid',sprintf('%.30g',numel(obj.features)+1));
        obj.features{end+1} = pline;
        node.setAttribute('elmid',sprintf('%.30g',numel(obj.elements)+1));
        obj.elements{end+1} = pline;
    end
    
    obj.writeSceneElementBase(pline,node);
    
    % write svg geometry for verts
    id = obj.svgnode.getChildNodes().getLength()+1;
    canvasdiag = sqrt(sum((obj.canvasbbox(:,2)-obj.canvasbbox(:,1)).^2,1));
    if pline.closed
        shapetype = 'polygon';
%         coords = SceneExporter.swapy(...
%             pline.verts(:,[1:end,1]),obj.canvasbbox(:,1),obj.canvasbbox(:,2));
    else
        shapetype = 'polyline';
%         coords = SceneExporter.swapy(...
%             pline.verts,obj.canvasbbox(:,1),obj.canvasbbox(:,2));
    end
    coords = SceneExporter.swapy(...
        pline.verts,obj.canvasbbox(:,1),obj.canvasbbox(:,2));
    attr = containers.Map('KeyType','char','ValueType','char');
    attr('fill') = 'none';
    attr('stroke') = '#000000';
    attr('stroke-width') = num2str(canvasdiag.*0.001);
    attr('id') = num2str(id);
    writesvggeometry({shapetype},{coords},{attr},obj.svgnode);
    delete(attr);

    % polyline ids
    pidstr = sprintf('%.30g',id);
    node.setAttribute('vertids',pidstr);

    for i=1:numel(pline.corners)
        obj.writeCorner(pline.corners(i),[],node);
    end
    for i=1:numel(pline.edges)
        obj.writeEdge(pline.edges(i),[],node);
    end
    for i=1:numel(pline.segments)
        obj.writeSegment(pline.segments(i),[],node);
    end
end

function node = writeScenePoint(obj,point,node,parentnode)
    
    if isempty(node) == isempty(parentnode)
        error('must pass either node or parentnode.');
    end
    
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('point');
        parentnode.appendChild(node);
        node.setAttribute('featid',sprintf('%.30g',numel(obj.features)+1));
        obj.features{end+1} = point;
        node.setAttribute('elmid',sprintf('%.30g',numel(obj.elements)+1));
        obj.elements{end+1} = point;
    end
    
    obj.writeSceneElementBase(point,node);
    
    % write svg geometry
    id = obj.svgnode.getChildNodes().getLength()+1;
    canvasdiag = sqrt(sum((obj.canvasbbox(:,2)-obj.canvasbbox(:,1)).^2,1));
    shapetype = 'polyline';
    coords = SceneExporter.swapy(...
        point.pos,obj.canvasbbox(:,1),obj.canvasbbox(:,2));
    attr = containers.Map('KeyType','char','ValueType','char');
    attr('fill') = 'none';
    attr('stroke') = '#000000';
    attr('stroke-width') = num2str(canvasdiag.*0.001);
    attr('id') = num2str(id);
    writesvggeometry({shapetype},{coords},{attr},obj.svgnode);
    delete(attr);
    
    % polyline ids
    pidstr = sprintf('%.30g',id);
    node.setAttribute('p',pidstr);
end

function node = writeSceneMesh(obj,mesh,node,parentnode)
    
    if isempty(node) == isempty(parentnode)
        error('must pass either node or parentnode.');
    end
    
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('mesh');
        parentnode.appendChild(node);
        node.setAttribute('featid',sprintf('%.30g',numel(obj.features)+1));
        obj.features{end+1} = mesh;
        node.setAttribute('elmid',sprintf('%.30g',numel(obj.elements)+1));
        obj.elements{end+1} = mesh;
    end
    
    obj.writeSceneElementBase(mesh,node);
    
    % write svg geometry
    id = obj.svgnode.getChildNodes().getLength()+1;
    meshcoords = [];
    meshstruct = struct;
    meshstruct.name = mesh.name;
    meshstruct.v = mesh.verts';
    meshstruct.n = mesh.vnormals';
    meshstruct.u = mesh.texcoords';
    meshstruct.f = mesh.faces';
    meshstruct.fvn = mesh.facevnormalinds;
    meshstruct.ftc = mesh.facetexcoordinds;
    meshstruct.fmg = mesh.facematerialind;
    meshstruct.fsg = mesh.facesmoothgroupind;
    meshstruct.matnames = {mesh.materials.name};
    meshstruct.fa = mesh.fareas;
    meshstruct.fn = mesh.fnormals';
    meshstruct.e = mesh.edges;
    meshstruct.te = mesh.edgefaces;
    meshstruct.valence = mesh.valence;
    meshstruct.isboundarye = mesh.isboundarye;
    meshstruct.isboundaryv = mesh.isboundaryv;
    meshstruct.isboundaryf = mesh.isboundaryf;
    attr = containers.Map('KeyType','char','ValueType','char');
    attr('id') = num2str(id);
    writesvgmesh({meshcoords},{meshstruct},{attr},obj.svgnode);
    delete(attr);

    % polyline ids
    pidstr = sprintf('%.30g',id);
    node.setAttribute('p',pidstr);
    
    for i=1:numel(mesh.surfaces)    
        obj.writeSurface(mesh.surfaces(i),[],node);
    end
    
    % write planar proxies
    boutlinelistnode = node.getOwnerDocument.createElement('planarproxys');
    node.appendChild(boutlinelistnode);
    for i=1:numel(mesh.planarproxy)
        if isa(mesh.planarproxy(i),'ScenePolygon')
            boutlinenode = obj.writeScenePolygon(mesh.planarproxy(i),[],boutlinelistnode);
            if ismember(mesh.planarproxy(i),[mesh.scenegroup.elements])
                boutlinenode.setAttribute('ingroup','1');
            end
        elseif isa(mesh.planarproxy(i),'ScenePolyline')
            boutlinenode = obj.writeScenePolyline(mesh.planarproxy(i),[],boutlinelistnode);
            if ismember(mesh.planarproxy(i),[mesh.scenegroup.elements])
                boutlinenode.setAttribute('ingroup','1');
            end
        else
            warning('Skipping corrupt planarproxy, must be either a polygon or a polyline.');
        end
    end
%     if not(isempty(mesh.planarproxy))
%         
%         if not(isa(mesh.planarproxy,'ScenePolygon') || isa(mesh.planarproxy,'ScenePolyline'))
%             warning('Skipping corrupt planarproxy, must be either a polygon or a polyline.');
%         else
%             if isa(mesh.planarproxy,'ScenePolygon')
%                 planarproxynode = node.getOwnerDocument.createElement('basepoly');
%                 node.appendChild(planarproxynode);
%                 node.setAttribute('featid',sprintf('%.30g',numel(obj.features)+1));
%                 obj.features(end+1) = mesh.planarproxy;
%                 node.setAttribute('elmid',sprintf('%.30g',numel(obj.elements)+1));
%                 obj.elements(end+1) = mesh.planarproxy;
%                 obj.writeScenePolygon(mesh.planarproxy,planarproxynode,[]);
%             elseif isa(mesh.planarproxy,'ScenePolyline')
%                 planarproxynode = node.getOwnerDocument.createElement('planarproxy');
%                 node.appendChild(planarproxynode);
%                 node.setAttribute('featid',sprintf('%.30g',numel(obj.features)+1));
%                 obj.features(end+1) = mesh.planarproxy;
%                 node.setAttribute('elmid',sprintf('%.30g',numel(obj.elements)+1));
%                 obj.elements(end+1) = mesh.planarproxy;
%                 obj.writeScenePolyline(mesh.planarproxy,planarproxynode,[]);
%             end
%             
%             if ismember(mesh.planarproxy,[mesh.scenegroup.elements])
%                 planarproxynode.setAttribute('ingroup','1');
%             end
%         end
%         
%     end
end

function writeCorner(obj,corner,node,parentnode)
    if isempty(node) == isempty(parentnode)
        error('must pass either node or parentnode.');
    end
    
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('corner');
        parentnode.appendChild(node);
        node.setAttribute('featid',sprintf('%.30g',numel(obj.features)+1));
        obj.features{end+1} = corner;
    end

    obj.writeSceneFeatureBase(corner,node);

    node.setAttribute('vidx',sprintf('%.30g',corner.cornervertex));
end

function writeEdge(obj,edge,node,parentnode)
    if isempty(node) == isempty(parentnode)
        error('must pass either node or parentnode.');
    end
    
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('edge');
        parentnode.appendChild(node);
        node.setAttribute('featid',sprintf('%.30g',numel(obj.features)+1));
        obj.features{end+1} = edge;
    end
    
    obj.writeSegment(edge,node,[]);
end

function writeSegment(obj,segment,node,parentnode)
    if isempty(node) == isempty(parentnode)
        error('must pass either node or parentnode.');
    end
    
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('segment');
        parentnode.appendChild(node);
        node.setAttribute('featid',sprintf('%.30g',numel(obj.features)+1));
        obj.features{end+1} = segment;
    end
            
    obj.writeSceneFeatureBase(segment,node);

    node.setAttribute('startarclen',sprintf('%.30g',segment.startarclen));
    node.setAttribute('endarclen',sprintf('%.30g',segment.endarclen));
end

function writeSurface(obj,surface,node,parentnode)
    
    if isempty(node) == isempty(parentnode)
        error('must pass either node or parentnode.');
    end
    
    if isempty(node) && not(isempty(parentnode))
        node = parentnode.getOwnerDocument.createElement('surface');
        parentnode.appendChild(node);
        node.setAttribute('surfid',sprintf('%.30g',numel(obj.surfaces)+1));
        obj.surfaces{end+1} = surface;
        node.setAttribute('featid',sprintf('%.30g',numel(obj.features)+1));
        obj.features{end+1} = surface;
    end
    
    node.setAttribute('faceindices',sprintf('%.30g,',surface.faceindices));
    for j=1:size(surface.vparams,1)
        vparamnode = node.getOwnerDocument.createElement('vertexparams');
        vparamnode.setAttribute('p',sprintf('%.30g,',surface.vparams(j,:)));
        node.appendChild(vparamnode);
    end
    
    obj.writeSceneFeatureBase(surface,node);
    
    % write boundary polygon(s)
    bpolylistnode = node.getOwnerDocument.createElement('boundarypolys');
    node.appendChild(bpolylistnode);
    for i=1:numel(surface.boundarypoly)
%         boundarypolynode = node.getOwnerDocument.createElement('boundarypolys');
%         node.appendChild(boundarypolynode);
%         node.setAttribute('featid',sprintf('%.30g',numel(obj.features)+1));
%         obj.features(end+1) = surface.boundarypoly(i);
%         node.setAttribute('elmid',sprintf('%.30g',numel(obj.elements)+1));
%         obj.elements(end+1) = surface.boundarypoly(i);
        bpolynode = obj.writeScenePolygon(surface.boundarypoly(i),[],bpolylistnode);
        
        % check if the boundary polygon is in one of the child groups
        childgroups = surface.scenegroup.childgroups;
        for j=1:numel(childgroups)
            if not(isempty(childgroups(j).surface)) && childgroups(j).surface == surface
                if ismember(surface.boundarypoly(i),childgroups(j).elements)
                    bpolynode.setAttribute('ingroup','1');
                    break;
                end
            end
        end
    end
end
    
end

methods(Static)

function exportScene(filename,scene,varargin)
    exporter = SceneExporter(scene);
    exporter.export(filename,varargin{:});
    delete(exporter(isvalid(exporter)));
end
    
function points = swapy(points,canvasmin,canvasmax)
    % mirror points in y-direction (in svg, the coordinate origin is in the
    % upper left)
    points(2,:) = canvasmax(2) - (points(2,:)-canvasmin(2));
end

function filenames = generateoffmeshfilenames(meshes,filedir)
    meshnames = {meshes.name};

%     [meshnames,perm] = sort(meshnames);
%     meshes = meshes(perm);

    filenames = cell(1,numel(meshnames));
    filenames{1} = fullfile(filedir,[meshnames{1},'.off']);

    suffixnum = uint64(2);
    for i=2:numel(meshnames)
        if strcmp(meshnames{i},meshnames{i-1})
            filenames{i} = fullfile(filedir,[meshnames{i},'_',sprintf('%u',suffixnum),'.off']);
            if suffixnum >= intmax('uint64')
                error('Too many meshes!');
            end
            suffixnum = suffixnum + 1;
        else
            filenames{i} = fullfile(filedir,[meshnames{i},'.off']);
            suffixnum = uint64(2);
        end
    end
end

end

end
