classdef SceneMesh < SceneElement3D
    
% properties(SetAccess={?SceneMesh,?SceneImporter,?SceneExporter})
% end

% faces should be stored counter-clockwise (when viewed from front)
    
properties(Constant)
    MESHEPS = 0.00001; % in multiples of the bounding box diagonal
end

properties(SetAccess={?SceneMesh,?SceneImporter,?SceneExporter})
    
    % face info
    faces = []; % just triangles for now
    facevnormalinds = [];
    facetexcoordinds = [];
    facematerialind = [];
    facesmoothgroupind = [];
    
    % vertex info (arrays do not need to have same size)
    verts = [];
    vnormals = [];
    texcoords = [];
    
    materials = SceneMaterial.empty;
    
    planarproxy = ScenePolyline.empty; % 2d proxy of the mesh
    surfaces = SceneSurface.empty(0,0); % custom surfaces with paramterizations
end

properties(Dependent)
    % for fast access (can be computed from other data)
    valence;
    fnormals;
    fareas;
    
    edges;         % halfedges (v1,v2 stored in sparse matrix)
    edgefaces;
%     faceedges;
    
    isboundarye;
    isboundaryv;
    isboundaryf;
    
    volume;
    center;
    extent;
    radius;

    projection; % projection along the z direction onto the xy plane
end

properties(Access={?SceneMesh,?SceneImporter,?SceneExporter})
    p_valence = [];
    p_fnormals = [];
    p_fareas = [];
    
    p_edges = [];
    p_edgefaces = [];
%     p_faceedges = [];
    
    p_isboundarye = [];    
    p_isboundaryv = [];
    p_isboundaryf = [];
    
    p_volume = [];
    p_center = [];
    p_extent = [];
    p_radius = [];
    
    p_projection = [];
end

methods(Static)

function n = type_s
    n = 'mesh';
end

function d = dimension_s
    d = 2;
end

end

methods

% obj = SceneMesh()
% obj = SceneMesh(mesh)
% obj = SceneMesh(labels)
% obj = SceneMesh(verts,faces[,labels])
% obj = SceneMesh(verts,faces,vnormals,facevnormalinds[,labels])
% obj = SceneMesh(verts,faces,vnormals,facevnormalinds,texcoords,facetexcoordinds[,labels])
function obj = SceneMesh(varargin)
    obj@SceneElement3D;

    if numel(varargin) == 0
        % do nothing (default values)
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneMesh')
        % copy constructor
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 1 && ischar(varargin{1})
        % just the labels
        obj.setLabels(varargin{1});
        [obj.vnormals,obj.facevormalinds] = obj.generateDefaultVnormals;
        
        obj.definePosition(obj.center);
        obj.defineOrientation([1;0;0;0]);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
    elseif numel(varargin) >= 2 && isnumeric(varargin{1})
        % vertices, faces and optionally vnormals, texcoords and labels
        obj.setVertsAndFaces(varargin{1},varargin{2});
        
        nextargin = 3;
        if numel(varargin) >= nextargin+1 && isnumeric(varargin{nextargin+1})
            obj.setVnormals(varargin{nextargin},varargin{nextargin+1});
            nextargin = nextargin+2;
        else
            [obj.vnormals,obj.facevnormalinds] = obj.generateDefaultVnormals;
        end
        
        if numel(varargin) >= nextargin+1 && isnumeric(varargin{nextargin+1})
            obj.setTexcoords(varargin{nextargin},varargin{nextargin+1});
            nextargin = nextargin+2;
        end
        
        if numel(varargin) >= nextargin && ischar(varargin{nextargin})
            obj.setLabels(varargin{nextargin});
        end
        
        obj.definePosition(obj.center);
        obj.defineOrientation([1;0;0;0]);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
    else
        error('Invalid arguments for constructor.');
    end
end

function delete(obj)
    if not(isempty(obj.surfaces))
        delete(obj.surfaces(isvalid(obj.surfaces)));
    end
    if not(isempty(obj.planarproxy))
        delete(obj.planarproxy(isvalid(obj.planarproxy)));
    end
    if not(isempty(obj.materials))
        delete(obj.materials(isvalid(obj.materials)));
    end
end

function copyFrom(obj,obj2)
    obj.copyFrom@SceneElement3D(obj2);

%     if not(isempty(obj.surfaces))
%         delete(obj.surfaces(isvalid(obj.surfaces)));
%     end
%     if not(isempty(obj.planarproxy))
%         delete(obj.planarproxy(isvalid(obj.planarproxy)));
%     end

%     obj.name = obj2.name;
    
    obj.faces = obj2.faces;
    obj.facematerialind = obj2.facematerialind;
    obj.facesmoothgroupind = obj2.facesmoothgroupind;
    obj.facevnormalinds = obj2.facevnormalinds;
    obj.facetexcoordinds = obj2.facetexcoordinds;
    
    obj.verts = obj2.verts;
    obj.vnormals = obj2.vnormals;
    obj.texcoords = obj2.texcoords;
    
    obj.p_valence = obj2.p_valence;
    
    obj.p_fnormals = obj2.p_fnormals;
    obj.p_fareas = obj2.p_fareas;
    
    obj.p_edges = obj2.p_edges;
    obj.p_edgefaces = obj2.p_edgefaces;
    
    obj.p_isboundarye = obj2.p_isboundarye;
    obj.p_isboundaryv = obj2.p_isboundaryv;
    obj.p_isboundaryf = obj2.p_isboundaryf;
    
    obj.p_volume = obj2.p_volume;
    obj.p_center = obj2.p_center;
    obj.p_extent = obj2.p_extent;
    obj.p_radius = obj2.p_radius;
    
    obj.p_projection = obj2.p_projection;
    
    % surfaces (copy conservatively)
    obj.surfaces = SceneFeature.copyFeatsarrayFrom(obj.surfaces,obj2.surfaces);
    if not(isempty(obj.surfaces))
        obj.surfaces.setParentelement(obj);
    end
    
    % planarproxys (copy conservatively)
    obj.planarproxy = SceneFeature.copyFeatsarrayFrom(obj.planarproxy,obj2.planarproxy);
    
    % materials and smoothgroups
    if not(isempty(obj.materials))
        delete(obj.materials(isvalid(obj.materials)));
    end
    obj.materials = obj2.materials;
    for i=1:numel(obj.materials)
        obj.materials(i) = obj.materials(i).clone;
    end
end

function obj2 = clone(obj)
    obj2 = SceneMesh(obj);
end

function [vn,facevnind] = generateDefaultVnormals(obj,faceinds)

    if nargin < 2
        f = obj.faces;
        v = obj.verts;
        fn = obj.fnormals;
        fa = obj.fareas;
    else
        f = obj.faces(:,faceinds);
        vinds = unique(f(:));
        newvinds(vinds) = (1:numel(vinds))';
        f = newvinds(f);
        v = obj.verts(:,vinds);
        fn = obj.fnormals(:,faceinds);
        fa = obj.fareas(faceinds);
    end
    
    vn = trimeshVertexNormals(v,f,'faceavg_area',fn,fa);
    
%     % accumulate face normals to get vertex normals
%     vn = [];
%     vn(1,:) = accumarray(f(:),reshape(fn([1,1,1],:),[],1),[size(v,2),1]);
%     vn(2,:) = accumarray(f(:),reshape(fn([2,2,2],:),[],1),[size(v,2),1]);
%     vn(3,:) = accumarray(f(:),reshape(fn([3,3,3],:),[],1),[size(v,2),1]);
%     vnormallen = sqrt(sum(vn.^2,1));
%     bsxfun(@times,vn,1./vnormallen);
%     vn(:,vnormallen<0.00000001) = [1;0;0];
    
    facevnind = f;
end

function removeFaces(obj,inds)
    obj.faces(:,inds) = [];
    if not(isempty(obj.facevnormalinds))
        obj.facevnormalinds(:,inds) = [];
    end
    if not(isempty(obj.facetexcoordinds))
        obj.facetexcoordinds(:,inds) = [];
    end
    if not(isempty(obj.facematerialind))
        obj.facematerialind(:,inds) = [];
    end
    if not(isempty(obj.facesmoothgroupind))
        obj.facesmoothgroupind(:,inds) = [];
    end
    
    obj.dirty;
end

function disconnectNonmanifoldFaces(obj,duplicatehalfedges)
    v1v2faces = containers.Map('KeyType','double','ValueType','any');
    
    faceedges = [obj.faces([1,2],:),obj.faces([2,3],:),obj.faces([3,1],:)];
    edgefaceinds = repmat(1:size(obj.faces,2),1,3);
    
    for i=1:size(faceedges,2)
        if not(v1v2faces.isKey(faceedges(1,i)))
            v1v2faces(faceedges(1,i)) = containers.Map('KeyType','double','ValueType','any');
        end
        
        v2faces = v1v2faces(faceedges(1,i));
        if v2faces.isKey(faceedges(2,i))
            v2faces(faceedges(2,i)) = [v2faces(faceedges(2,i)),edgefaceinds(i)]; %#ok<NASGU>
        else
            v2faces(faceedges(2,i)) = edgefaceinds(i); %#ok<NASGU>
        end
    end
    
    [duplicatehalfedges(1,:),duplicatehalfedges(2,:)] = find(obj.edges>1);
    
    disconnectfaceinds = zeros(1,0);
    for i=1:size(duplicatehalfedges,2)
        v2faces = v1v2faces(duplicatehalfedges(1,i));
        faceinds = v2faces(duplicatehalfedges(2,i));
        disconnectfaceinds = [disconnectfaceinds,faceinds(2:end)]; %#ok<AGROW>
    end
    disconnectfaceinds = unique(disconnectfaceinds);
        
    for i=1:numel(disconnectfaceinds)
        face = obj.faces(:,disconnectfaceinds(i));

        edgeduplicatemask = [...
            obj.edges(face(1),face(2)) > 1;...
            obj.edges(face(2),face(3)) > 1;...
            obj.edges(face(3),face(1)) > 1];

        vertclonemask = [...
            any(edgeduplicatemask([1,3]));...
            any(edgeduplicatemask([1,2]));...
            any(edgeduplicatemask([2,3]))];

        oldsize = size(obj.verts,2);
        obj.verts = [obj.verts,obj.verts(:,face(vertclonemask))];

        obj.faces(vertclonemask,disconnectfaceinds(i)) = oldsize+1 : oldsize+sum(vertclonemask);
    end
end

function removeDegeneracies(obj)
    
    [~,~,bbdiag] = pointsetBoundingbox(obj.verts);
    epsilon = bbdiag * SceneMesh.MESHEPS;
    
    [~,loopedges,duplicatehalfedges,~] = obj.checkConsistency;
    if not(isempty(loopedges)) || not(isempty(duplicatehalfedges)) % mesh may have border edges
        
        % remove faces adjacent to loop edges
        loopfaceinds = find(any(obj.faces([2,3,1],:) - obj.faces == 0,1));
        if not(isempty(loopfaceinds))
            warning('Removing faces adjacent to loop edges.');
            obj.removeFaces(loopfaceinds);
        end
        
        [~,~,duplicatehalfedges,~] = obj.checkConsistency;
        if not(isempty(duplicatehalfedges)) % mesh may have border edges
%             error('Mesh connectivity is inconsistent for a simple 2D surface in 3D space.');
            
            % disconnect additional faces connected to a half-edge (for
            % now, do not make fancy check to keep faces with most similar
            % normals connected)
            warning('Disconnecting additional faces connected to non-manifold edges.');
            
            obj.disconnectNonmanifoldFaces(duplicatehalfedges);
        end
        
        obj.dirty;
    end
    
    % remove edges with length below eps (edge collapse)
    [vfrom,vto] = find(obj.edges);
    edgelen = sqrt(sum((obj.verts(:,vto) - obj.verts(:,vfrom)).^2,1));
    degedgemask = edgelen <= epsilon*2; % *2 so that triangle height when flipping an edge (deg. faces, later) cannot be below epsilon
    protectededges = logical(sparse(size(obj.verts,2),size(obj.verts,2)));
    if any(degedgemask)
        warning('Removing edges with vanishing length.');
    end
    while any(degedgemask)
        protectededges = obj.collapseEdges(vfrom(degedgemask),vto(degedgemask),protectededges);
        [vfrom,vto] = find(obj.edges);
        protectedmask = protectededges(sub2ind(size(obj.edges),vfrom,vto)');
        edgelen = sqrt(sum((obj.verts(:,vto) - obj.verts(:,vfrom)).^2,1));
        degedgemask = edgelen <= epsilon*2 & not(protectedmask);
    end
    
    % repeatedly swap the longest edge of all faces with area below eps
    % (this assumes there are no edges with zero length, otherwise there is
    % the possiblity of infinite edge swapping e.g. if two zero-area triangles
    % are adjacent and they form a rectangle with zero height: the diagonal
    % could always be swapped)
    if not(obj.hasEdges)
        obj.updateEdges;
    end
    if not(obj.hasEdgefaces)
        obj.updateEdgefaces;
    end
    if not(obj.hasFareas)
        obj.updateFareas;
    end
    % find index of longest edge for each face
    % do not flip protected edges, if a protected edge is the longest edge
    % in a face, all edges in the face have length below eps (the protected
    % edge has length below eps), so flipping would be of no use anyway
    edgelen = sparse(vfrom,vto,edgelen,size(obj.edges,1),size(obj.edges,2));
    edgelen(protectededges) = -1;
    fedgelen = [full(edgelen(sub2ind(size(edgelen),obj.faces(1,:),obj.faces(2,:))));...
                full(edgelen(sub2ind(size(edgelen),obj.faces(2,:),obj.faces(3,:))));...
                full(edgelen(sub2ind(size(edgelen),obj.faces(3,:),obj.faces(1,:))))];
    [fmaxedgelen,fmaxedgeind] = max(fedgelen,[],1);
    
    faltitudes = (2.*obj.p_fareas)./fmaxedgelen;
    faltitudes(fmaxedgelen < 0) = -1; % set protected edges to negative value, even if the area is zero
    degfaceinds = find(faltitudes >= 0 & faltitudes <= epsilon);
    
    if not(isempty(degfaceinds))
        warning('Fixing faces with vanishing area.');
        
        if not(isempty(obj.surfaces))
            error(['Cannot remove face area degeneracies for meshes with surfaces, ',...
                   'mesh connectivity may change.']);
        end

        obj.p_valence = [];
        obj.p_fnormals = [];

        obj.p_isboundarye = [];
        obj.p_isboundaryf = [];
        obj.p_isboundaryv = [];

        obj.p_volume = [];
        obj.p_center = [];
        obj.p_extent = [];
        obj.p_radius = [];
        
        obj.p_projection = [];

        c=0;
        while not(isempty(degfaceinds))
            
            c = c + 1;
            if c > size(obj.faces,2)*3*10
                disp('Apparently in infinite loop.');
%                 error('Apparently in infinite loop.');
            end

            [~,ind] = max(fmaxedgelen(degfaceinds));
            faceind = degfaceinds(ind);
            edgeind = fmaxedgeind(faceind);
            if edgeind == 1
                vfrom = obj.faces(1,faceind);
                vto = obj.faces(2,faceind);
            elseif edgeind == 2
                vfrom = obj.faces(2,faceind);
                vto = obj.faces(3,faceind);
            elseif edgeind == 3
                vfrom = obj.faces(3,faceind);
                vto = obj.faces(1,faceind);
            else
                error('This should not happen, egdeind not in [1,3]');
            end

            % if face of longest edge is border face, just remove it
            if obj.p_edgefaces(vto,vfrom) == 0
                face = obj.faces(:,faceind);
                einds = sub2ind(size(obj.p_edges),face,face([2:end,1]));
                obj.p_edges(einds) = 0;
                obj.p_edgefaces(einds) = 0;
                mask = obj.p_edgefaces>faceind;
                obj.p_edgefaces(mask) = obj.p_edgefaces(mask) - 1;
                obj.faces(:,faceind) = [];
                obj.p_fareas(faceind) = [];

                faltitudes(faceind) = [];
                fedgelen(:,faceind) = [];
                degfaceinds(degfaceinds==faceind) = [];
                
                continue;
            end

            % find the indices of the opposing vertices of the two faces (those
            % that are not adjacent to the shared edge)
            face1ind = faceind;
            face2ind = obj.p_edgefaces(vto,vfrom);
            face1voppositeind = find(obj.faces(:,face1ind) ~= vto & obj.faces(:,face1ind) ~= vfrom);
            face2voppositeind = find(obj.faces(:,face2ind) ~= vto & obj.faces(:,face2ind) ~= vfrom);
            vopposite1 = obj.faces(face1voppositeind,face1ind);
            vopposite2 = obj.faces(face2voppositeind,face2ind);

            if obj.p_edges(vopposite1,vopposite2) || obj.p_edges(vopposite2,vopposite1)
                % Rare case: there is already an edge connecting the
                % opposite vertices of the two triangles, just protect edge
                % for now
                protectededges(vfrom,vto) = true;
                protectededges(vto,vfrom) = true;
                
                % recompute edge lengths and max. face edge lengths of all edges and faces
                [allvfrom,allvto] = find(obj.edges);
                edgelen = sqrt(sum((obj.verts(:,allvto) - obj.verts(:,allvfrom)).^2,1));
                edgelen = sparse(allvfrom,allvto,edgelen,size(obj.edges,1),size(obj.edges,2));
                % do not flip protected edes
                edgelen(protectededges) = -1; %#ok<SPRIX>
                fedgelen = [full(edgelen(sub2ind(size(edgelen),obj.faces(1,:),obj.faces(2,:))));...
                            full(edgelen(sub2ind(size(edgelen),obj.faces(2,:),obj.faces(3,:))));...
                            full(edgelen(sub2ind(size(edgelen),obj.faces(3,:),obj.faces(1,:))))];
                [fmaxedgelen,fmaxedgeind] = max(fedgelen,[],1);
                
                % recompute face altitudes above the max. length edge of all faces
                faltitudes = (2.*obj.p_fareas)./fmaxedgelen;
                faltitudes(fmaxedgelen < 0) = -1; % set protected edges to negative value, even if the area is zero
                degfaceinds = find(faltitudes >= 0 & faltitudes <= epsilon);

                continue;
            end
            
            % find vertex indices of new faces
            newface1 = [obj.faces(smod(face1voppositeind-1,1,4),face1ind);...
                        obj.faces(face1voppositeind,face1ind);...
                        obj.faces(face2voppositeind,face2ind)];
            newface2 = [obj.faces(smod(face2voppositeind-1,1,4),face2ind);...
                        obj.faces(face2voppositeind,face2ind);...
                        obj.faces(face1voppositeind,face1ind)];

            % remove edges of old faces
            einds = sub2ind(size(obj.p_edges),obj.faces(:,face1ind),obj.faces([2:end,1],face1ind));
            obj.p_edges(einds) = 0;
            obj.p_edgefaces(einds) = 0;
            einds = sub2ind(size(obj.p_edges),obj.faces(:,face2ind),obj.faces([2:end,1],face2ind));
            obj.p_edges(einds) = 0;
            obj.p_edgefaces(einds) = 0;

            % add edges of new faces
            neweinds1 = sub2ind(size(obj.p_edges),newface1,newface1([2:end,1]));
            obj.p_edges(neweinds1) = 1;
            obj.p_edgefaces(neweinds1) = face1ind;
            neweinds2 = sub2ind(size(obj.p_edges),newface2,newface2([2:end,1]));
            obj.p_edges(neweinds2) = 1;
            obj.p_edgefaces(neweinds2) = face2ind;

            % modify faces
            obj.faces(:,face1ind) = newface1;
            obj.faces(:,face2ind) = newface2;
            
            % both faces are added to the smoothing and material group of
            % the face with the largest altitude
            if not(isempty(obj.facematerialind)) || not(isempty(obj.facesmoothgroupind)) || ...
               not(isempty(obj.facevnormalinds)) || not(isempty(obj.facetexcoordinds))
                
                [~,ind] = max(faltitudes([face1ind,face2ind]));
                if ind == 1
                    templatefaceind = face1ind;
                    removefaceind = face2ind;
                    templateedge = [vfrom;vto];
                    templatefacevoppositeind = face1voppositeind;
                    removedvertind = vopposite2;
%                     removefacevoppositeind = face2voppositeind;
                else
                    templatefaceind = face2ind;
                    removefaceind = face1ind;
                    templateedge = [vto;vfrom];
                    templatefacevoppositeind = face2voppositeind;
                    removedvertind = vopposite1;
%                     removefacevoppositeind = face1voppositeind;
                end
                if not(isempty(obj.facematerialind))
                    matgroupind = obj.facematerialind(templatefaceind);
                    obj.facematerialind([face1ind,face2ind]) = matgroupind;
                end
                if not(isempty(obj.facesmoothgroupind))
                    smoothgroupind = obj.facesmoothgroupind(templatefaceind);
                    obj.facesmoothgroupind([face1ind,face2ind]) = smoothgroupind;
                end
                
                if not(isempty(obj.facevnormalinds)) || not(isempty(obj.facetexcoordinds))
                    [~,t] = pointLinesegDistance3D_mex(obj.verts(:,removedvertind),...
                        obj.verts(:,templateedge(1)),obj.verts(:,templateedge(2)));
                end
                if not(isempty(obj.facevnormalinds))
                    templatevnedge = obj.facevnormalinds(...
                        [smod(templatefacevoppositeind+1,1,4);smod(templatefacevoppositeind+2,1,4)],...
                        templatefaceind);
                    newvnormal = ...
                        obj.vnormals(:,templatevnedge(1)) .* (1-t) + ...
                        obj.vnormals(:,templatevnedge(2)) .* t;
                    len = sqrt(sum(newvnormal.^2,1)); % re-normalize
                    if len > 0.00000001;
                        newvnormal = newvnormal ./ len;
                    else
                        newvnormal = [1;0;0];
                    end
                    obj.vnormals(:,end+1) = newvnormal;
                    obj.facevnormalinds(:,templatefaceind) = [...
                        obj.facevnormalinds(smod(templatefacevoppositeind-1,1,4),templatefaceind);...
                        obj.facevnormalinds(templatefacevoppositeind,templatefaceind);...
                        size(obj.vnormals,2)];
                    obj.facevnormalinds(:,removefaceind) = [...
                        obj.facevnormalinds(smod(templatefacevoppositeind+1,1,4),templatefaceind);...
                        size(obj.vnormals,2);...
                        obj.facevnormalinds(templatefacevoppositeind,templatefaceind)];
                end
                if not(isempty(obj.facetexcoordinds))
                    templatetexedge = obj.facetexcoordinds(...
                        [smod(templatefacevoppositeind+1,1,4);smod(templatefacevoppositeind+2,1,4)],...
                        templatefaceind);
                    newtexcoord = ...
                        obj.texcoords(:,templatetexedge(1)) .* (1-t) + ...
                        obj.texcoords(:,templatetexedge(2)) .* t;
                    obj.texcoords(:,end+1) = newtexcoord;
                    obj.facetexcoordinds(:,templatefaceind) = [...
                        obj.facetexcoordinds(smod(templatefacevoppositeind-1,1,4),templatefaceind);...
                        obj.facetexcoordinds(templatefacevoppositeind,templatefaceind);...
                        size(obj.texcoords,2)];
                    obj.facetexcoordinds(:,removefaceind) = [...
                        obj.facetexcoordinds(smod(templatefacevoppositeind+1,1,4),templatefaceind);...
                        size(obj.texcoords,2);...
                        obj.facetexcoordinds(templatefacevoppositeind,templatefaceind)];
                end
            end
            
            % if new edge has length 0, collapse it
            if sqrt(sum((obj.verts(:,vopposite1) - obj.verts(:,vopposite2)).^2,1)) < epsilon
                % should be a rare case, so cost of recomputing all areas etc.
                % should not matter so much
                protectededges = obj.collapseEdges(vopposite1,vopposite2,protectededges);

                % recompute face areas of all faces
                obj.updateFareas;
                
                % recompute edge lengths and max. face edge lengths of all edges and faces
                [allvfrom,allvto] = find(obj.edges);
                edgelen = sqrt(sum((obj.verts(:,allvto) - obj.verts(:,allvfrom)).^2,1));
                edgelen = sparse(allvfrom,allvto,edgelen,size(obj.edges,1),size(obj.edges,2));
                % do not flip protected edes
                edgelen(protectededges) = -1; %#ok<SPRIX>
                fedgelen = [full(edgelen(sub2ind(size(edgelen),obj.faces(1,:),obj.faces(2,:))));...
                            full(edgelen(sub2ind(size(edgelen),obj.faces(2,:),obj.faces(3,:))));...
                            full(edgelen(sub2ind(size(edgelen),obj.faces(3,:),obj.faces(1,:))))];
                [fmaxedgelen,fmaxedgeind] = max(fedgelen,[],1);
                
                % recompute face altitudes above the max. length edge of all faces
                faltitudes = (2.*obj.p_fareas)./fmaxedgelen;
                faltitudes(fmaxedgelen < 0) = -1; % set protected edges to negative value, even if the area is zero
            else
                % do not flip the same edge again to avoid loops
                protectededges(vfrom,vto) = true;
                protectededges(vto,vfrom) = true;
                
                % recompute face areas of the two modified faces
                obj.p_fareas([face1ind,face2ind]) = triangleArea(...
                    obj.verts(:,obj.faces(1,[face1ind,face2ind])),...
                    obj.verts(:,obj.faces(2,[face1ind,face2ind])),...
                    obj.verts(:,obj.faces(3,[face1ind,face2ind])));
                
                % recompute edge lengths and max. face edge lengths of modified edges and faces
                newelen1 = sqrt(sum((obj.verts(:,newface1([2:end,1]))-obj.verts(:,newface1)).^2,1));
                newelen2 = sqrt(sum((obj.verts(:,newface2([2:end,1]))-obj.verts(:,newface2)).^2,1));
                % do not flip protected edes
                newelen1(protectededges(sub2ind(size(obj.edges),newface1,newface1([2:end,1])))) = -1;
                newelen2(protectededges(sub2ind(size(obj.edges),newface2,newface2([2:end,1])))) = -1;
                [fmaxedgelen(face1ind),fmaxedgeind(face1ind)] = max(newelen1);
                [fmaxedgelen(face2ind),fmaxedgeind(face2ind)] = max(newelen2);

                % recompute face altitudes above the max. length edge of modified faces
                falt = (2.*obj.p_fareas([face1ind,face2ind]))./fmaxedgelen([face1ind,face2ind]);
                falt(fmaxedgelen([face1ind,face2ind]) < 0) = -1; % set protected edges to negative value, even if the area is zero
                faltitudes([face1ind,face2ind]) = falt;
                
%                 % recompute face altitudes above the max. length edge of modified faces
%                 faltitudes([face1ind,face2ind]) = ...
%                     (2.*obj.p_fareas([face1ind,face2ind]))./fmaxedgelen([face1ind,face2ind]);
            end

            % find remaining degenerate faces
            degfaceinds = find(faltitudes >= 0 & faltitudes <= epsilon);
            

        end
    end
    
    % remove unused vnormals and texcoords
    obj.cleanupVnormals;
    obj.cleanupTexcoords;
    obj.cleanupMaterials;
end

function cleanupVnormals(obj)
    if not(isempty(obj.vnormals))
        mask = false(1,size(obj.vnormals,2));
        mask(obj.facevnormalinds(:)) = true;
        obj.vnormals = obj.vnormals(:,mask);
        old2newind(mask) = 1:size(obj.vnormals,2);
        obj.facevnormalinds = old2newind(obj.facevnormalinds);
        if any(obj.facevnormalinds <= 0)
            error(['Invalid face vertex normal indices after removing unused ',...
                   'vertex normals, this should not happen.']);
        end
    end
end

function cleanupTexcoords(obj)
    if not(isempty(obj.texcoords))
        mask = false(1,size(obj.texcoords,2));
        mask(obj.facetexcoordinds(:)) = true;
        obj.texcoords = obj.texcoords(:,mask);
        old2newind(mask) = 1:size(obj.texcoords,2);
        obj.facetexcoordinds = old2newind(obj.facetexcoordinds);
        if any(obj.facetexcoordinds <= 0)
            error(['Invalid face texture coordinate indices after removing unused ',...
                   'texture coordinates, this should not happen.']);
        end
    end
end

function cleanupMaterials(obj)
    if not(isempty(obj.materials))
        mask = false(1,numel(obj.materials));
        mask(obj.facematerialind) = true;
        if any(not(mask))
            removemats = obj.materials(not(mask));
            delete(removemats(isvalid(removemats)));
        end
        obj.materials = obj.materials(:,mask);
        old2newind(mask) = 1:numel(obj.materials);
        obj.facematerialind = old2newind(obj.facematerialind);
        if any(obj.facematerialind <= 0)
            error(['Invalid face material indices after removing unused ',...
                   'materials, this should not happen.']);
        end
    end
end

% this might leave some texture coordinates and vertex normals unused if
% faces get deleted, might want to clean up afterwards
function protectededges = collapseEdges(obj,vfrom,vto,protectededges)
    
    if isempty(vfrom)
        return;
    end
    
    if not(isempty(obj.surfaces))
        error(['Cannot collapse edges for meshes with surfaces, ',...
               'vparams may not be available for the collapsed vertices.']);
    end
    
    if not(obj.hasEdges)
        obj.updateEdges;
    end
    if not(obj.hasEdgefaces)
        obj.updateEdgefaces;
    end
    
    if nargin < 4 || isempty(protectededges)
        protectededges = logical(sparse(size(obj.edges)));
    end
    
    einds = sub2ind(size(obj.edges),vfrom,vto);
    if any(obj.edges(einds) < 1)
        error('A given edge does not exist.');
    end
    if any(protectededges(einds))
        warning('Protected edges given for collapse, ignoring them.');
        mask = protectededges(einds); 
        einds(mask) = [];
        vfrom(mask) = [];
        vto(mask) = [];
    end
    
    obj.p_valence = [];
    obj.p_fareas = [];
    obj.p_fnormals = [];
    
    obj.p_isboundarye = [];
    obj.p_isboundaryf = [];
    obj.p_isboundaryv = [];
    
    obj.p_volume = [];
    obj.p_center = [];
    obj.p_extent = [];
    obj.p_radius = [];
    obj.p_projection = [];
    
    removedvertmask = false(1,size(obj.verts,2));
    removedfacemask = false(1,size(obj.faces,2));
    for i=1:numel(vfrom)
        
        if obj.p_edges(vfrom(i),vto(i)) < 1 || protectededges(vfrom(i),vto(i))
            % can happen because edges are modified in each iteration
            continue;
        end
        
        vfromneighbormask = obj.p_edges(:,vfrom(i))' | obj.p_edges(vfrom(i),:);
        vtoneighbormask = obj.p_edges(:,vto(i))' | obj.p_edges(vto(i),:);
        vsharedneighbormask = vfromneighbormask & vtoneighbormask;
        faceinds = full([obj.p_edgefaces(vfrom(i),vto(i)),obj.p_edgefaces(vto(i),vfrom(i))]);
        faceinds(faceinds<=0) = []; % can happen if the edge is a border edge
        facevertmask = false(1,size(obj.verts,2));
        facevertmask(obj.faces(:,faceinds)) = true;
%         facevertmask([vfrom(i),vto(i)]) = false;
        if any(vsharedneighbormask & not(facevertmask))
            % there are one or more shared neighbor vertices that are not
            % in any of the two adjacent faces => each of the neighbor
            % vertices forms a three-edged mesh bottleneck with the edge,
            % collapsing the edge would collapse the bottleneck and change
            % the mesh topology (by either closing a hole or splitting the
            % mesh into two or more parts)
            
            protectededges(vfrom(i),vto(i)) = true;
            protectededges(vto(i),vfrom(i)) = true;
            continue;
        end
        
%         obj.p_edges(:,vfrom(i))
        
        % remove edges of face1 and mark face1 for removal
        face1ind = obj.p_edgefaces(vfrom(i),vto(i));
        if obj.p_edgefaces(vfrom(i),vto(i)) > 0
            removedfacemask(face1ind) = true;
            face = obj.faces(:,face1ind);
            einds = sub2ind(size(obj.p_edges),face,face([2:end,1]));
            obj.p_edges(einds) = 0;
            obj.p_edgefaces(einds) = 0;
        end
        
        % remove edges of face2 and mark face2 for removal
        face2ind = obj.p_edgefaces(vto(i),vfrom(i));
        if obj.p_edgefaces(vto(i),vfrom(i)) > 0
            removedfacemask(face2ind) = true;
            face = obj.faces(:,face2ind);
            einds = sub2ind(size(obj.p_edges),face,face([2:end,1]));
            obj.p_edges(einds) = 0;
            obj.p_edgefaces(einds) = 0;
        end
        
        % add edges adjacent to vto to vfrom
        mask1 = obj.p_edges(:,vto(i)) > 0;
        mask2 = obj.p_edges(vto(i),:) > 0;
        if any(mask1 & obj.p_edges(:,vfrom(i)) > 0) || ...
           any(mask2 & obj.p_edges(vfrom(i),:) > 0)
            % this should not happen because we already checked for any
            % shared neighbor vertices that are not on the faces earlier
            error('Additional shared neighbor vertices detected.');
        end
        obj.p_edges(mask1,vfrom(i)) = obj.p_edges(mask1,vto(i));
        obj.p_edges(vfrom(i),mask2) = obj.p_edges(vto(i),mask2);
        obj.p_edgefaces(mask1,vfrom(i)) = obj.p_edgefaces(mask1,vto(i));
        obj.p_edgefaces(vfrom(i),mask2) = obj.p_edgefaces(vto(i),mask2);
        % if one half edge is protected, the other one is as well
        protectededges(vfrom(i),:) = protectededges(vfrom(i),:) |  protectededges(:,vfrom(i))';
        protectededges(:,vfrom(i)) = protectededges(vfrom(i),:)' |  protectededges(:,vfrom(i));
        
        % remove edges adjacent to vto
        obj.p_edges(vto(i),:) = 0;
        obj.p_edges(:,vto(i)) = 0;
        obj.p_edgefaces(vto(i),:) = 0;
        obj.p_edgefaces(:,vto(i)) = 0;
        
        % leave vfrom at its current position and leave texture coordinates
        % and vertex normals as is
        
%         % update vfrom
%         obj.verts(:,vfrom(i)) = (obj.verts(:,vfrom(i)) + obj.verts(:,vto(i))) .* 0.5;
%         if not(isempty(obj.texcoords))
%             obj.texcoords(:,vfrom(i)) = (obj.texcoords(:,vfrom(i)) + obj.texcoords(:,vto(i))) .* 0.5;
%         end
%         if not(isempty(obj.vnormals))
%             obj.vnormals(:,vfrom(i)) = (obj.vnormals(:,vfrom(i)) + obj.vnormals(:,vto(i))) .* 0.5;
%             % re-normalize vertex normal
%             vnormallen = sqrt(sum(obj.vnormals(:,vfrom(i)).^2,1));
%             if vnormallen <= 0
%                 obj.vnormals(:,vfrom(i)) = [1;0;0];
%             else
%                 obj.vnormals(:,vfrom(i)) = obj.vnormals(:,vfrom(i)) ./ vnormallen;
%             end
%         end
        
        % update faces
        obj.faces(obj.faces == vto(i)) = vfrom(i);
        
        % mark vto for removal
        removedvertmask(vto(i)) = true;
    end
    
    keepvertinds = find(not(removedvertmask));
    old2newvertinds = zeros(1,size(obj.verts,2));
    old2newvertinds(keepvertinds) = 1:numel(keepvertinds);
    
    keepfaceinds = find(not(removedfacemask));
    old2newfaceinds = zeros(1,size(obj.faces,2));
    old2newfaceinds(keepfaceinds) = 1:numel(keepfaceinds);
    
    obj.verts = obj.verts(:,keepvertinds);
%     if not(isempty(obj.texcoords))
%         obj.texcoords = obj.texcoords(:,keepvertinds);
%     end
%     if not(isempty(obj.vnormals))
%         obj.vnormals = obj.vnormals(:,keepvertinds);
%     end
    
    obj.p_edges = obj.p_edges(keepvertinds,keepvertinds);
    protectededges = protectededges(keepvertinds,keepvertinds);
    obj.p_edgefaces = obj.p_edgefaces(keepvertinds,keepvertinds);
    mask = obj.p_edgefaces > 0;
    obj.p_edgefaces(mask) = old2newfaceinds(obj.p_edgefaces(mask));
    
    obj.faces = obj.faces(:,keepfaceinds);
    obj.faces = old2newvertinds(obj.faces);
    if not(isempty(obj.facematerialind))
        obj.facematerialind = obj.facematerialind(:,keepfaceinds);
    end
    if not(isempty(obj.facesmoothgroupind))
        obj.facesmoothgroupind = obj.facesmoothgroupind(:,keepfaceinds);
    end
    if not(isempty(obj.facevnormalinds))
        obj.facevnormalinds = obj.facevnormalinds(:,keepfaceinds);
    end
    if not(isempty(obj.facetexcoordinds))
        obj.facetexcoordinds = obj.facetexcoordinds(:,keepfaceinds);
    end
end

function val = get.valence(obj)
    if not(obj.hasValence)
        obj.updateValence;
    end
    val = obj.p_valence;
end
function b = hasValence(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = isempty(obj(k).verts) || not(isempty(obj(k).p_valence));
    end
end

% function val = get.vnormals(obj)
%     if not(obj.hasVnormals)
%         obj.updateVnormals;
%     end
%     val = obj.vnormals;
% end
% function val = get.facevnormalinds(obj)
%     if not(obj.hasFacevnormalinds)
%         obj.updateVnormals;
%     end
%     val = obj.vnormals;
% end
% function b = hasVnormals(obj)
%     b = false(1,numel(obj));
%     for k=1:numel(obj)
%         b(k) = isempty(obj(k).verts) || not(isempty(obj(k).vnormals));
%     end
% end

function val = get.fnormals(obj)
    if not(obj.hasFnormals)
        obj.updateFnormals;
    end
    val = obj.p_fnormals;
end
function b = hasFnormals(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = isempty(obj(k).faces) || not(isempty(obj(k).p_fnormals));
    end
end

function val = get.fareas(obj)
    if not(obj.hasFareas)
        obj.updateFareas;
    end
    val = obj.p_fareas;
end
function b = hasFareas(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = isempty(obj(k).faces) || not(isempty(obj(k).p_fareas));
    end
end

function val = get.edges(obj)
    if not(obj.hasEdges)
        obj.updateEdges;
    end
    val = obj.p_edges;
end
function b = hasEdges(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = isempty(obj(k).faces) || not(isempty(obj(k).p_edges));
    end
end

function val = get.edgefaces(obj)
    if not(obj.hasEdgefaces)
        obj.updateEdgefaces;
    end
    val = obj.p_edgefaces;
end
function b = hasEdgefaces(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = isempty(obj(k).faces) || not(isempty(obj(k).p_edgefaces));
    end
end

% function val = get.faceedges(obj)
%     if not(obj.hasFaceedges)
%         obj.updateFaceedges;
%     end
%     val = obj.p_faceedges;
% end
% function b = hasFaceedges(obj)
%     b = false(1,numel(obj));
%     for k=1:numel(obj)
%         b(k) = isempty(obj(k).faces) || not(isempty(obj(k).p_faceedges));
%     end
% end

function val = get.isboundarye(obj)
    if not(obj.hasIsboundarye)
        obj.updateIsboundarye;
    end
    val = obj.p_isboundarye;
end
function b = hasIsboundarye(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = isempty(obj(k).faces) || not(isempty(obj(k).p_isboundarye));
    end
end

function val = get.isboundaryv(obj)
    if not(obj.hasIsboundaryv)
        obj.updateIsboundaryv;
    end
    val = obj.p_isboundaryv;
end
function b = hasIsboundaryv(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = isempty(obj(k).verts) || not(isempty(obj(k).p_isboundaryv));
    end
end

function val = get.isboundaryf(obj)
    if not(obj.hasIsboundaryf)
        obj.updateIsboundaryf;
    end
    val = obj.p_isboundaryf;
end
function b = hasIsboundaryf(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = isempty(obj(k).faces) || not(isempty(obj(k).p_isboundaryf));
    end
end

function val = get.volume(obj)
    if not(obj.hasVolume)
        obj.updateVolume;
    end
    val = obj.p_volume;
end
function b = hasVolume(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_volume));
    end
end

function val = get.center(obj)
    if not(obj.hasCenter)
        obj.updateCenter;
    end
    val = obj.p_center;
end
function b = hasCenter(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_center));
    end
end

function val = get.extent(obj)
    if not(obj.hasExtent)
        obj.updateExtent;
    end
    val = obj.p_extent;
end
function b = hasExtent(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_extent));
    end
end

function val = get.radius(obj)
    if not(obj.hasRadius)
        obj.updateRadius;
    end
    val = obj.p_radius;
end
function b = hasRadius(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_radius));
    end
end

function val = get.projection(obj)
    if not(obj.hasProjection)
        obj.updateProjection;
    end
    val = obj.p_projection;
end
function b = hasProjection(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = size(obj(k).p_projection,1) > 0;
    end
end

function setVertsAndFaces(obj,verts,faces)
    if size(obj.faces,2) ~= size(faces,2)
        facecountchanged = true;
    else
        facecountchanged = false;
    end
    if size(obj.verts,2) ~= size(verts,2)
        vertcountchanged = true;
    else
        vertcountchanged = false;
    end
    
    obj.verts = verts;
    obj.faces = faces;
    
%     [isconsistent,loopedges,duplicatehalfedges,borderedges] = obj.checkConsistency;
%     if not(isconsistent)
%         if size(loopedges,2) > 0
%             warning(['Mesh ',obj.labelStr,' has ',num2str(size(loopedges,2)),' ',...
%                      'edges that start and end on the same vertex.']);
%         end
%         if size(duplicatehalfedges,2) > 0
%             warning(['Mesh ',obj.labelStr,' has ',num2str(size(duplicatehalfedges,2)),' ',...
%                      'duplicate half-edges, ',...
%                      'this means the mesh may have > 2 faces adjacent to an edge '...
%                      'or inconsistent face winding order.']);
%         end
%         if size(borderedges,2) > 0
%             warning(['Mesh ',obj.labelStr,' has ',num2str(size(borderedges,2)),' border edges.']);
%         end
%         
% %         % ensure that all faces have consistent winding (too slow - need c++ implementation)
% %         obj.faces = trimeshUnifyNormals(obj.faces);
%     end

%     if not(isempty(obj.surfaces))
%         delete(obj.surfaces(isvalid(obj.surfaces)));
%     end
%     obj.surfaces = SceneSurface.empty;
    obj.removeSurfaces;
    
    if not(isempty(obj.planarproxy))
        delete(obj.planarproxy(isvalid(obj.planarproxy)));
    end
    obj.planarproxy = ScenePolyline.empty;
    
    if facecountchanged || vertcountchanged
        if not(isempty(obj.materials))
            delete(obj.materials(isvalid(obj.materials)));
        end
        obj.materials = SceneMaterial.empty;
        
        obj.facevnormalinds = [];
        obj.facetexcoordinds = [];

        obj.vnormals = [];
        obj.texcoords = [];
        
        obj.facematerialind = [];
        obj.facesmoothgroupind = [];
    end
    
    obj.dirty;
%     obj.p_mirrored = false; % set to no mirroring (mirroring has to be given)
end

% function flipFaces(obj,faceinds)
%     obj.faces(:,faceinds) = obj.faces([1,3,2],faceinds);
%     obj.dirty;
%     
%     for i=1:numel(obj.surfaces)
%         obj.surfaces(i).dirty;
%     end
% end

% function setName(obj,name)
%     obj.name = name;
% end

function setVnormals(obj,vnormals,facevnormalinds)
    obj.vnormals = [];
    obj.facevnormalinds = [];
    
    if nargin < 3
        if not(isempty(vnormals))
            if size(vnormals,2) ~= size(obj.verts,2)
                error('Size of vertex normals must match size of vertices.');
            end
            obj.vnormals = vnormals;
            obj.facevnormalinds = obj.faces;
        end
    else
        if any(any(facevnormalinds > size(vnormals,2) | facevnormalinds < 1))
            error('Invalid face vertex normal indices.');
        end
        obj.vnormals = vnormals;
        obj.facevnormalinds = facevnormalinds;
    end
end

function setTexcoords(obj,texcoords,facetexcoordinds)
    obj.texcoords = [];
    obj.facetexcoordinds = [];
    if nargin < 3
        if not(isempty(texcoords))
            if size(texcoords,2) ~= size(obj.verts,2)
                error('Size of texture coordinates must match size of vertices.');
            end
            obj.texcoords = texcoords;
            obj.facetexcoordinds = obj.faces;
        end
    else
        if any(any(facetexcoordinds > size(texcoords,2) | facetexcoordinds < 1))
            error('Invalid face texture coordinate indices.');
        end
        obj.texcoords = texcoords;
        obj.facetexcoordinds = facetexcoordinds;
    end
end

function setSmoothgroups(obj,smoothgroups)
    obj.facesmoothgroupind = [];
    
    if iscell(smoothgroups)
        if not(isempty(smoothgroups))
            obj.facesmoothgroupind = ones(1,numel(smoothgroups)).*-1;
            for i=1:numel(smoothgroups)
                if any(smoothgroups{i} > size(obj.faces,2))
                    error('Smooth group face index exceeds face count.');
                end
                obj.facesmoothgroupind(smoothgroups{i}) = i;
            end
        end
    else
        if not(isempty(smoothgroups)) && any(size(smoothgroups,2) ~= size(obj.faces,2))
            error('Invalid face smoothing group indices.');
        end
        obj.facesmoothgroupind = smoothgroups;
    end
end

function setMaterials(obj,materials,matgroups)
    if not(isempty(obj.materials))
        delete(obj.materials(isvalid(obj.materials)));
    end
    obj.materials = SceneMaterial.empty;
    obj.facematerialind = [];
    
    if iscell(matgroups)
        obj.facematerialind = ones(1,size(obj.faces,2)).*-1;
        
        if numel(materials) ~= numel(matgroups)
            error('Number of materials must match number of material groups.');
        end
        for i=1:numel(matgroups)
            if any(matgroups{i} > size(obj.faces,2))
                error('Material group face index exceeds face count.');
            end
            obj.facematerialind(matgroups{i}) = i;
        end
        if not(isa(materials,'SceneMaterial'))
            error('Materials must be of class ''SceneMaterial''.');
        end
%         if any(obj.facematerialind < 1)
%             % some faces do not have a material, create dummy material
%             warning('Some mesh faces do not have a material assigned.');
%             dummymat = SceneMaterial('none');
%             materials = [materials,dummymat];
%             obj.facematerialind(obj.facematerialind < 1) = numel(materials);
%         end
    else
        if not(isempty(matgroups))
            if any(size(matgroups,2) ~= size(obj.faces,2))
                error('Invalid face material group indices.');
            end
            if max(matgroups) > numel(materials) || any(matgroups < 1)
                error('Invalid face material group indices.');
            end
        end
        obj.facematerialind = matgroups;
    end
    
    obj.materials = materials;
end

function [isconsistent,loopedges,duplicatehalfedges,borderedges] = checkConsistency(obj)
    [isconsistent,loopedges,duplicatehalfedges,borderedges] = trimeshHalfedgeConsistency(obj.edges);
end

function boutlines = removePlanarproxy(obj,boutlineinds)
    if nargin < 2
        boutlineinds = 1:numel(obj.planarproxy);
    end
    
    if not(isnumeric(boutlineinds))
        boutlineinds = find(ismember(obj.planarproxy,boutlineinds));
    end
    
    boutlines = obj.planarproxy(boutlineinds);
    obj.planarproxy(boutlineinds) = [];
    
    if nargout < 1
        delete(boutlines(isvalid(boutlines)));
    end
end

function setPlanarproxy(obj,val)
    if not(isa(val,'ScenePolyline')) && not(isa(val,'ScenePolygon'))
        error('Planar proxy must be a polyline or a polygon.');
    end

    if not(isempty(obj.planarproxy)) && (isempty(val) || obj.planarproxy ~= val)
        delete(obj.planarproxy(isvalid(obj.planarproxy)));
    end
    obj.planarproxy = val;
end

function proxy = createPlanarproxy(obj)
    proxy = ScenePolygon(obj.projection,strcat(obj.labelStrs,'_base'));
end

function b = hasPlanarproxy(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        o = obj(k);
        b(k) = not(isempty(o.planarproxy));
    end
end

function b = hasSurfaces(obj)
%     b = arrayfun(@(x) isa(x.surfaces,'SceneSurface'),obj);
    b = true(size(obj)); % always true, because surfaces are never dirty (cannot be updated)
end

function surfs = addSurfaces(obj,faceindices,vertexparams,labels)
    if nargin >= 4
%         obj.surfaces(end+1) = SceneSurface(obj,faceindices,vertexparams,labels);
%         surfs = obj.surfaces(end);
        surfs = obj.addSurfaces(SceneSurface(SceneMesh.empty,faceindices,vertexparams,labels));
    elseif nargin >= 3
%         obj.surfaces(end+1) = SceneSurface(obj,faceindices,vertexparams);
%         surfs = obj.surfaces(end);
        surfs = obj.addSurfaces(SceneSurface(SceneMesh.empty,faceindices,vertexparams));
    elseif nargin >= 2 && isnumeric(faceindices)
        surfs = obj.addSurfaces(SceneSurface(SceneMesh.empty,faceindices));
    elseif nargin >= 2 && isa(faceindices,'SceneSurface')
        % the mesh takes ownership!
        surfs = faceindices;
        if not(isempty(surfs))
            obj.surfaces(end+1:end+numel(surfs)) = surfs;
            surfs.setParentelement(obj);
        end
    else
        error('Invalid arguments.');
    end
end

% surface = removeSurface(...) % returns the surface
% removeSurface(...) % deletes the surface
% ... removeSurface(obj,surface)
% ... removeSurface(obj,surfaceind)
function surfs = removeSurfaces(obj,surfaceinds)
    if nargin < 2
        surfaceinds = 1:numel(obj.surfaces);
    end
    
    if not(isnumeric(surfaceinds))
        surfaceinds = find(ismember(obj.surfaces,surfaceinds));
    end
    
    surfs = obj.surfaces(surfaceinds);
    obj.surfaces(surfaceinds) = [];
%     obj.p_subfeatures = [];
    
    if not(isempty(surfs))
        surfs.setParentelement(SceneMesh.empty);
    end

    if nargout < 1
        delete(surfs(isvalid(surfs)));
    end
    
%     if isempty(ind)
%         error('Could not find given surface.');
%     end
% 
%     if isvalid(obj.surfaces(ind))
%         delete(obj.surfaces(ind));
%     end
%     obj.surfaces(ind) = [];
%     obj.p_subfeatures = [];
%      if not(isempty(obj.scenegroup))
%         obj.scenegroup.updateElements;
%     end
end

function [v1,v2,v3] = faceverts(obj,inds)
    if nargin < 2
        inds = 1:size(obj.faces,2);
    end

    v1 = obj.verts(:,obj.faces(1,inds));
    v2 = obj.verts(:,obj.faces(2,inds));
    v3 = obj.verts(:,obj.faces(3,inds));
end

function updateEdges(obj)

    for k=1:numel(obj)
        o = obj(k);
        
        o.p_edges = trimeshEdges(o.faces,size(o.verts,2));
    end
end

function updateEdgefaces(obj)

    for k=1:numel(obj)
        o = obj(k);
    
        faceedges = [o.faces([1,2],:),o.faces([2,3],:),o.faces([3,1],:)];
        edgefaceinds = repmat(1:size(o.faces,2),1,3);
        
        % set duplicate edgefaceinds (multiple faces adjacent to same
        % half-edge) to 0, so sparse() does not add the face indices
        halfedgeinds = 1:size(faceedges,2);
        [faceedges_sorted,perm] = sortrows([faceedges(1,:)',faceedges(2,:)']);
        halfedgeinds = halfedgeinds(perm);
        duplicateinds = find(all(diff(faceedges_sorted,1,1)==0,2))+1;
        if not(isempty(duplicateinds))
            warning('Non-manifold faces or edges found while computing edge faces.');
            edgefaceinds(halfedgeinds(duplicateinds)) = 0;
        end
        
        o.p_edgefaces = sparse(faceedges(1,:)',faceedges(2,:)',edgefaceinds',size(o.verts,2),size(o.verts,2));
        
    end
end

function updateFnormals(obj)
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_fnormals = trimeshFaceNormals(o.verts,o.faces);
        if any(any(isnan(o.p_fnormals),1)) && false
            warning('Degenerate faces in the mesh, trying to interpolate face normals from adjacent faces.');
%             degfaceinds = find(any(isnan(o.p_fnormals),1));
        end
    end
end

function updateFareas(obj)
    
    for k=1:numel(obj)
        o = obj(k);

        o.p_fareas = triangleArea(...
            o.verts(:,o.faces(1,:)),...
            o.verts(:,o.faces(2,:)),...
            o.verts(:,o.faces(3,:)));
    end
end

function updateValence(obj)
    mask = not(obj.hasEdges);
    if any(mask)
        obj(mask).updateEdges;
    end
        
    for k=1:numel(obj)
        o = obj(k);

        % indegree + outdegree
        o.p_valence = full(sum(o.edges~=0, 1)) + full(sum(o.edges~=0, 2))';
    end
end
% function updateVnormals(obj)
%     mask = not(obj.hasFnormals);
%     if any(mask)
%         obj(mask).updateFnormals;
%     end
%     mask = not(obj.hasFareas);
%     if any(mask)
%         obj(mask).updateFareas;
%     end
% 
%     for k=1:numel(obj)
%         o = obj(k);
%         
%         o.vnormals = trimeshVertexNormals(o.verts,o.faces,'faceavg_area',o.fnormals,o.fareas);
%         o.facevnormalinds = obj.faces;
%     end
% end

function updateIsboundarye(obj)
    mask = not(obj.hasEdges);
    if any(mask)
        obj(mask).updateEdges;
    end
    
    for k=1:numel(obj)
        o = obj(k);
    
        % edges that do not have a reverse edge
        o.p_isboundarye = o.edges+o.edges' == 1;
    end
end
function updateIsboundaryv(obj)
    mask = not(obj.hasIsboundarye);
    if any(mask)
        obj(mask).updateIsboundarye;
    end
    
    for k=1:numel(obj)
        o = obj(k);
    
        % boundary vertex if any incoming or outgoing edge is a boundary edge
        o.p_isboundaryv = any(o.isboundarye,1) | any(o.isboundarye,2)';
    end
end
function updateIsboundaryf(obj)
    mask = not(obj.hasIsboundaryv);
    if any(mask)
        obj(mask).updateIsboundaryv;
    end
    
    for k=1:numel(obj)
        o = obj(k);
        
        % boundary face if any of the vertices are on the boundary (not any of
        % the edges)
        o.p_isboundaryf = any(o.isboundaryv(o.faces),1);
    end
end
function updateVolume(obj)
    
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_volume = 1; % just use one for now
    end
end
function updateProjection(obj)
    
    for k=1:numel(obj)
        o = obj(k);

        convhullinds = convhull(o.verts(1,:),o.verts(2,:),'simplify',true);
        convhullverts = zeros(2,numel(convhullinds));
        [convhullverts(1,:),convhullverts(2,:)] = poly2cw(o.verts(1,convhullinds),o.verts(2,convhullinds));
        [~,~,convhullbbdiag] = pointsetBoundingbox(convhullverts);
        convhullverts = polylineSimplify(convhullverts',convhullbbdiag*0.001)';
        o.p_projection = convhullverts(:,1:end-1); % last = first vertex from convhull
    end
end

% 0 for edges without neighbor
function [nfinds,nfinds1,nfinds2,nfinds3] = getFaceNeighbors(obj,finds)
    mask = not(obj.hasEdgefaces);
    if any(mask)
        obj(mask).updateEdgefaces;
    end
    
    for k=1:numel(obj)
        o = obj(k);
    
        fedges_reverse = [o.faces([2,1],finds);o.faces([3,2],finds);o.faces([1,3],finds)];

        nfinds1 = full(o.edgefaces(sub2ind(size(o.edgefaces),fedges_reverse(1,:),fedges_reverse(2,:))));
        nfinds2 = full(o.edgefaces(sub2ind(size(o.edgefaces),fedges_reverse(3,:),fedges_reverse(4,:))));
        nfinds3 = full(o.edgefaces(sub2ind(size(o.edgefaces),fedges_reverse(5,:),fedges_reverse(6,:))));
        nfinds = [nfinds1,nfinds2,nfinds3];
    end
end

function updateCenter(obj)
    mask = not(obj.hasBoundingbox);
    if any(mask)
        obj(mask).updateBoundingbox;
    end
    
    for k=1:numel(obj)
        o = obj(k);
        [bbmin,bbmax] = o.boundingbox;
        o.p_center = (bbmin+bbmax)./2;
    end
end
function updateExtent(obj)
    extents = computeExtent(obj,obj.getOrientation);
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_extent = extents(:,k);

%         % compute maximum extent of mesh in local x, y and z direction
%         verts_local = quat2dcm(o.orientation') * o.verts;
%         o.p_extent = max(verts_local,2) - min(verts_local,2);
    end
end
function val = computeExtent(obj,orient)
    val = zeros(3,numel(obj));
    for k=1:numel(obj)
        o = obj(k);
        
        % compute maximum extent of mesh in local x, y and z direction
        verts_local = quat2dcm(orient(:,k)') * o.verts;
        val(:,k) = max(verts_local,[],2) - min(verts_local,[],2);
    end
end
function updateRadius(obj)
    for k=1:numel(obj)
        o = obj(k);

        o.p_radius = max(sqrt(sum(bsxfun(@minus,o.verts,o.position).^2,1)));
    end
end

function updateSymmetries(obj)

    for k=1:numel(obj)
        o = obj(k);

        % no symmetries for now
        o.p_symmetries = false(5,1);
    end
end

% function [fi,d] = closestFace(obj,p)
%     d = pointTriangleDistance3D_mex(p,...
%         obj.verts(:,obj.faces(1,:)),...
%         obj.verts(:,obj.faces(2,:)),...
%         obj.verts(:,obj.faces(3,:)));
%     
%     [d,fi] = mind(d);
% end

end

methods(Access=protected)
    
function val = defaultPoseImpl(obj)
    orient = repmat([1;0;0;0],1,numel(obj));
    val = [...
        [obj.center];...
        orient;...
        obj.computeExtent(orient);...
        false(1,numel(obj))];
end
    
function dirtyImpl(obj)
    obj.dirtyImpl@SceneElement3D;
    
    for k=1:numel(obj)
        o = obj(k);
        o.p_valence = [];
        o.p_fnormals = [];
        o.p_fareas = [];

        o.p_edges = [];
        o.p_edgefaces = [];
%         o.p_faceedges = [];

        o.p_isboundarye = [];    
        o.p_isboundaryv = [];
        o.p_isboundaryf = [];

        o.p_volume = [];
        o.p_center = [];
        o.p_extent = [];
        o.p_radius = [];
        o.p_projection = [];
    end
end

function updateLocalImpl(obj)
    obj.updateLocalImpl@SceneElement3D;
    
    mask = not(obj.hasEdges);
    if any(mask)
        obj(mask).updateEdges;
    end
    mask = not(obj.hasEdgefaces);
    if any(mask)
        obj(mask).updateEdgefaces;
    end
%     mask = obj.hasFaceedges;
%     if any(mask)
%         obj(mask).updateFaceedges;
%     end

    mask = not(obj.hasFnormals);
    if any(mask)
        obj(mask).updateFnormals;
    end
    mask = not(obj.hasFareas);
    if any(mask)
        obj(mask).updateFareas;
    end
    
    mask = not(obj.hasValence);
    if any(mask)
        obj(mask).updateValence;
    end
%     mask = not(obj.hasVnormals);
%     if any(mask)
%         obj(mask).updateVnormals;
%     end
    
    mask = not(obj.hasIsboundarye);
    if any(mask)
        obj(mask).updateIsboundarye;
    end
    mask = not(obj.hasIsboundaryv);
    if any(mask)
        obj(mask).updateIsboundaryv;
    end
    mask = not(obj.hasIsboundaryf);
    if any(mask)
        obj(mask).updateIsboundaryf;
    end
    
    mask = not(obj.hasVolume);
    if any(mask)
        obj(mask).updateVolume;
    end
    mask = not(obj.hasCenter);
    if any(mask)
        obj(mask).updateCenter;
    end
    mask = not(obj.hasExtent);
    if any(mask)
        obj(mask).updateExtent;
    end
    mask = not(obj.hasRadius);
    if any(mask)
        obj(mask).updateRadius;
    end
%     mask = not(obj.hasOrigin);
%     if any(mask)
%         obj(mask).updateOrigin;
%     end
    mask = not(obj.hasPlanarproxy);
    if any(mask)
        obj(mask).updatePlanarproxy;
    end
end
    
function updateBoundingboxImpl(obj)
    for i=1:numel(obj)
        o = obj(i); % for performance
        v = o.verts;
        o.p_boundingboxmin = min(v,[],2);
        o.p_boundingboxmax = max(v,[],2);
    end
end

% pose must be 2D
function [center,width,height,orientation] = posedBoundingbox2DImpl(obj,framepose,featpose)
    
    if nargin < 2 || isempty(framepose)
        framepose = [...
            zeros(numel(obj));...
            zeros(numel(obj));...
            zeros(numel(obj));...
            ones(numel(obj));...
            zeros(numel(obj))];
    end
    if nargin < 2 || isempty(featpose)
        featpose = obj.getPose2D;
    end
    
    center = zeros(2,numel(obj));
    width = zeros(1,numel(obj));
    height = zeros(1,numel(obj));
    orientation = zeros(1,numel(obj));
    for i=1:numel(obj)
        v = posetransform(obj(i).verts,featpose(:,i),obj(i).pose3D);
        v = posetransform(v(1:2,:),identpose2D,framepose(:,i));

        bbmin = min(v,[],2);
        bbmax = max(v,[],2);
        center(:,i) = (bbmin + bbmax) .* 0.5;
        width(i) = bbmax(1) - bbmin(1);
        height(i) = bbmax(2) - bbmin(2);
        
        center(:,i) = posetransform(center(:,i),framepose(:,i),identpose2D);
        
        width(i) = width(i) * framepose(4,i);
        height(i) = height(i) * framepose(5,i);
        orientation(i) = framepose(3,i);
    end
end
    
% relations 0 and 2 are never returned, because only relevant poly indices
% (i.e. feature is inside poly or intersecting) are returned
% relation: 0: feature and poly are separate
% relation: 1: feature completely inside poly
% relation: 2: poly completely inside feature
% relation: 3: feature and poly are intersecting
function [polyinds,relation] = inPolygonImpl(obj,poly,polycandidates)
    % todo: planar proxy too inexact if it is convex hull, e.g. convex hull
    % planar proxy of a house with concave corners might be in polygons the
    % house is not really in
    % => can only use for finding polygon candidates right now, not for
    % hierarchy
    % => need something like outline that is a cut through the mesh with
    % plane z = 0, but two problems:
    % 1) what if the mesh is entirely above or below z = 0? Or is multipart
    % and has no geometry at z = 0 (only above and below?)
    % 2) how to connect planar proxy of multipart mesh, e.g. two components
    % separated in xy plane, when not using convex hull?
    % => need robust outline estimate that takes the geometry in epsilon
    % around z = 0 and projects it to z = 0 and maybe disallow meshes that
    % have multipart projections? (how to check?)
    
    polyinds = cell(numel(obj),1);
    if nargout >= 2
        relation = cell(numel(obj),1);
    end
    if not(isempty(poly)) && not(isempty(obj))
        [polybbmin,polybbmax] = poly.boundingbox;
        [mybbmin,mybbmax] = obj.boundingbox;

        for j=1:numel(obj)
            
            % find polygons with bounding boxes containing the planar proxy
            if nargin < 3 || isempty(polycandidates)
                polyinds{j} = find(...
                    polybbmin(1,:) < mybbmax(1,j) & polybbmin(2,:) < mybbmax(2,j) & ...
                    polybbmax(1,:) > mybbmin(1,j) & polybbmax(2,:) > mybbmin(2,j));
            else
                polyinds{j} = polycandidates{j}(...
                    polybbmin(1,polycandidates{j}) < mybbmax(1,j) & polybbmin(2,polycandidates{j}) < mybbmax(2,j) & ...
                    polybbmax(1,polycandidates{j}) > mybbmin(1,j) & polybbmax(2,polycandidates{j}) > mybbmin(2,j));
            end
            
            % find all polygons containing at least part of the planar proxy
            p = obj(j).projection(:,1);
%             if obj(j).closed
%                 pts = obj(j).boutline(:,[1:end,1]);
%             else
                pts = obj(j).projection; % currently always closed
%             end
            keepmask = false(1,numel(polyinds{j}));
            if nargout >= 2
                relation{j} = zeros(1,numel(polyinds{j}));
            end
            for i=1:numel(polyinds{j})
                % test if part of the planar proxy is inside the polygon
%                 intersects = linesegLinesegIntersect(...
%                     [pts(:,1:end-1);pts(:,2:end)]',...
%                     [poly(polyinds{j}(i)).verts;...
%                      poly(polyinds{j}(i)).verts(:,[2:end,1])]');
%                 intersecting = any(intersects.intAdjacencyMatrix(:) == 1);
                intersects = linesegLinesegIntersection2D_mex(...
                    pts(:,1:end-1),pts(:,2:end),...
                    poly(polyinds{j}(i)).verts,...
                    poly(polyinds{j}(i)).verts(:,[2:end,1]));
                intersecting = any(intersects(:) > 0);
                
                pointinpoly = pointInPolygon_mex(p(1),p(2),poly(polyinds{j}(i)).verts(1,:),poly(polyinds{j}(i)).verts(2,:));

                keepmask(i) = intersecting | pointinpoly;
                
                if nargout >= 2
                    if intersecting
                        relation{j}(i) = 3;
                    elseif pointinpoly
                        relation{j}(i) = 1;
                    end
                end
            end
            polyinds{j} = polyinds{j}(keepmask);
            if nargout >= 2
                relation{j} = relation{j}(keepmask);
            end
        end
    end
    
end
    
function [d,cp] = pointDistanceImpl(obj,p,pose)
    % approximate with distance to closest vertex for now
    if size(p,1) < 3
        warning('Scene:mesh','Given point is 2D, setting z to 0.');
        p(3,:) = 0;
    end

    if size(p,1) == 2

        d = inf(numel(obj),size(p,2));
        cp = zeros(numel(obj),size(p,2),2);
        for i=1:numel(obj)
            % distance to projection of 3D mesh to xy-plane (= distance
            % of 3D mesh to line orthogonal to xy-plane through the
            % given point)

            maxz = max(obj.verts(3,obj(i).faces(:)));
            minz = min(obj.verts(3,obj(i).faces(:)));

            p1 = [p;minz(ones(1,size(p,2)))];
            p2 = [p;maxz(ones(1,size(p,2)))];

            if nargin >= 3 && not(isempty(pose)) && not(all(pose{i}==obj(i).pose))
                v = posetransform(obj(i).verts,pose{i},obj(i).pose);
            else
                v = obj(i).verts;
            end

            [dists,~,b1,b2,b3] = linesegTriangleDistance3D_mex(p1,p2,...
                v(:,obj(i).faces(1,:)),...
                v(:,obj(i).faces(2,:)),...
                v(:,obj(i).faces(3,:)));

            [d(i,:),mininds] = min(dists,[],2);
            mininds = mininds';
            mininds_lin = sub2ind(size(b1),1:size(b1,1),mininds);

            % get closest points
            b1 = b1(mininds_lin);
            b2 = b2(mininds_lin);
            b3 = b3(mininds_lin);
            cp(i,:,:) = permute(...
                bsxfun(@times,v(1:2,obj(i).faces(1,mininds)),b1) + ...
                bsxfun(@times,v(1:2,obj(i).faces(2,mininds)),b2) + ...
                bsxfun(@times,v(1:2,obj(i).faces(3,mininds)),b3),[3,2,1]);
            % (ignore z value of closest point)
        end

    elseif size(p,1) == 3

        d = inf(numel(obj),size(p,2));
        cp = zeros(numel(obj),size(p,2),3);
        for i=1:numel(obj)

            if nargin >= 3 && not(isempty(pose))
                if not(all(pose{i}==obj(i).pose))
                    v = posetransform(obj(i).verts,pose{i},obj(i).pose);
                end
            else
                if not(isempty(obj(i).scenegroup))
                    grouppose = obj(i).scenegroup.globaloriginpose;
                    v = transform3D(obj(i).verts,grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
                end
            end

            [dists,b1,b2,b3] = pointTriangleDistance3D_mex(p,...
                v(:,obj(i).faces(1,:)),...
                v(:,obj(i).faces(2,:)),...
                v(:,obj(i).faces(3,:)));

            [d(i,:),mininds] = min(dists,[],2);
            mininds = mininds';
            mininds_lin = sub2ind(size(b1),1:size(b1,1),mininds);

            % get closest points
            b1 = b1(mininds_lin);
            b2 = b2(mininds_lin);
            b3 = b3(mininds_lin);
            cp(i,:,:) = permute(...
                bsxfun(@times,v(:,obj(i).faces(1,mininds)),b1) + ...
                bsxfun(@times,v(:,obj(i).faces(2,mininds)),b2) + ...
                bsxfun(@times,v(:,obj(i).faces(3,mininds)),b3),[3,2,1]);
        end
    else
        error('Only implemented for two or three dimensions.');
    end
end

% 3D line segment
function [d,t,cp] = linesegDistanceImpl(obj,p1,p2,pose)
    if size(p1,1) == 2
        error('Not yet implemented.');
        % todo: implement a function like linesegTriangleDistance_mex (2D)
    elseif size(p1,1) == 3
        d = inf(numel(obj),size(p1,2));
        t = nan(numel(obj),size(p1,2));
        cp = zeros(numel(obj),size(p1,2),3);
        for i=1:numel(obj)

            if nargin >= 4 && not(isempty(pose))
                if not(all(pose{i}==obj(i).pose))
                    v = posetransform(obj(i).verts,pose{i},obj(i).pose);
                end
            else
                if not(isempty(obj(i).scenegroup))
                    grouppose = obj(i).scenegroup.globaloriginpose;
                    v = transform3D(obj(i).verts,grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
                end
            end

            [dists,t1,b1,b2,b3] = linesegTriangleDistance3D_mex(p1,p2,...
                v(:,obj(i).faces(1,:)),...
                v(:,obj(i).faces(2,:)),...
                v(:,obj(i).faces(3,:)));

            % get element with minimum distance (and minimum t if there
            % is more than one element with the same minimum distance)
            mindist = min(dists,[],2);
            t1candidates = t1;
            t1candidates(not(bsxfun(@eq,dists,mindist))) = nan;
            [t(i,:),mininds] = min(t1candidates,[],2);
            mininds = mininds';
            mininds_lin = sub2ind(size(t1),1:size(t1,1),mininds);

            d(i,:) = dists(mininds_lin);

            % get closest points
            b1 = b1(mininds_lin);
            b2 = b2(mininds_lin);
            b3 = b3(mininds_lin);
            cp(i,:,:) = permute(...
                bsxfun(@times,v(:,obj(i).faces(1,mininds)),b1) + ...
                bsxfun(@times,v(:,obj(i).faces(2,mininds)),b2) + ...
                bsxfun(@times,v(:,obj(i).faces(3,mininds)),b3),[3,2,1]);
        end
    else
        error('Only implemented for two or three dimensions.');
    end
end

function feats = subfeaturesImpl(obj)
    mask = obj.hasSubfeatures;
    if any(mask)
        obj(mask).updateSubfeatures;
    end
    
%     feats = cell(1,numel(obj));
%     for k=1:numel(obj)
%         feats{k} = obj(k).surfaces;
%     end
    feats = {obj.surfaces};
    
%     feats = obj.surfaces;
end
function b = hasSubfeaturesImpl(obj)
    b = obj.hasSurfaces;
end
function updateSubfeaturesImpl(obj) %#ok<MANU>
    % do nothing (surfaces cannot be updated)
%     obj.updateSurfaces;
end

function moveAboutOriginImpl(obj,movevec)
    
    if not(isempty(obj.verts))
        obj.verts = bsxfun(@plus,obj.verts,movevec);
%         obj.verts = [...
%             obj.verts(1,:)+movevec(1);...
%             obj.verts(2,:)+movevec(2);...
%             obj.verts(3,:)+movevec(3)]; 
    end
    
    % face and vertex normals remain the same
    % face areas remain the same
    % volume remains the same
    
    if not(isempty(obj.p_center))
        obj.p_center = obj.p_center + movevec;
    end
    
    if not(isempty(obj.p_projection))
        obj.p_projection = bsxfun(@plus,obj.p_projection,movevec(1:2));
    end

    for i=1:numel(obj.surfaces)
        obj.surfaces(i).dirty;
    end

    if not(isempty(obj.planarproxy))
        obj.planarproxy.move(movevec(1:2),'relative');
    end
end

function rotateAboutOriginImpl(obj,rotangle)
    rotmat = quat2dcm(rotangle')';

    if not(isempty(obj.verts))
        obj.verts = rotmat*obj.verts; 
    end
    
    if not(isempty(obj.vnormals))
        obj.vnormals = rotmat*obj.vnormals;
    end
    if not(isempty(obj.p_fnormals))
        obj.p_fnormals = rotmat*obj.p_fnormals;
    end

    % face areas remain the same
    % volume remains the same
    
    if not(isempty(obj.p_center))
        obj.p_center = rotmat * obj.p_center;
    end
    
    if not(isempty(obj.p_projection))
        % needs to be recomputed
        obj.p_projection = [];
    end

    for i=1:numel(obj.surfaces)
        obj.surfaces(i).dirty;
        
%         newbpolys = obj.surfaces(i).createBoundarypoly;
%         obj.surfaces(i).boundarypoly = SceneFeature.copyFeatsarrayFrom(obj.surfaces(i).boundarypoly,newbpolys);
%         delete(newbpolys(isvalid(newbpolys)));
    end
    
    if not(isempty(obj.planarproxy))
        [yaw,pitch,roll] = quat2angle(rotangle');
        if pitch > 0.0001 || roll > 0.0001
            error('Rotation is not about z-axis, would need to recompute planar proxy.');
        else
%             rotmat = transformMat2D([1;1],yaw,[0;0],false);
%             obj.planarproxy.move(rotmat(1:2,1:2)*(obj.planarproxy.centroid-obj.centroid(1:2)) + obj.centroid(1:2),'absolute');
            obj.planarproxy.rotate(yaw,'relative',[0;0]); % rotate about origin
        end
    end
end

function scaleAboutOriginImpl(obj,relscale)
%     disp(relscale');
    if not(isempty(obj.verts))
        obj.verts = bsxfun(@times,obj.verts,relscale);
    end
    if not(isempty(obj.vnormals))
        if any(relscale ~= relscale(1))
            
            if any(relscale==0)
                % mesh is squashed, normals can only point in squashed
                % dimensions
                obj.p_fnormals(relscale~=0,:) = 0;
            else
                % scale with inverse relscale
                obj.vnormals = bsxfun(@rdivide,obj.vnormals,relscale);
            end
            
            % re-normalize
            nlen = sqrt(sum(obj.vnormals.^2,1));
            mask = nlen == 0;
            obj.vnormals(1,mask) = 1;
            obj.vnormals(2,mask) = 0;
            obj.vnormals(3,mask) = 0;
            obj.vnormals(:,not(mask)) = bsxfun(@rdivide,obj.vnormals(:,not(mask)),nlen(not(mask)));
        end
    end
    if not(isempty(obj.p_fnormals))
        if any(relscale ~= relscale(1))
            if any(relscale==0)
                % mesh is squashed, normals can only point in squashed
                % dimensions
                obj.p_fnormals(relscale~=0,:) = 0;
            else
                % scale with inverse relscale
                obj.p_fnormals = bsxfun(@rdivide,obj.p_fnormals,relscale);
            end
            
            % re-normalize
            nlen = sqrt(sum(obj.p_fnormals.^2,1));
            mask = nlen == 0;
            obj.p_fnormals(1,mask) = 1;
            obj.p_fnormals(2,mask) = 0;
            obj.p_fnormals(3,mask) = 0;
            obj.p_fnormals(:,not(mask)) = bsxfun(@rdivide,obj.p_fnormals(:,not(mask)),nlen(not(mask)));
        end
    end
    if not(isempty(obj.p_fareas))
        if any(relscale ~= relscale(1))
            % need to re-compute
            obj.p_fareas = [];
        end
    end
    if not(isempty(obj.p_volume))
        obj.p_volume = obj.p_volume * prod(relscale);
    end
    if not(isempty(obj.p_center))
        obj.p_center = obj.p_center .* relscale;
    end
    if not(isempty(obj.p_extent))
        if any(relscale ~= relscale(1))
            % non-uniform scaling, need to recompute extent
            obj.p_extent = [];
        else
            obj.p_extent = obj.p_extent .* relscale(1);
        end
    end
    if not(isempty(obj.p_radius))
        if any(relscale ~= relscale(1))
            % non-uniform scaling, need to recompute radius
            obj.p_radius = [];
        else
            obj.p_radius = obj.p_radius .* relscale(1);
        end
    end
    if not(isempty(obj.p_projection))
        obj.p_projection = bsxfun(@times,obj.p_projection,relscale(1:2));
    end

    for i=1:numel(obj.surfaces)
        % todo: would need to re-parametrize for non-uniform scaling?
        % but no need since the CartesianCoordinates do not use the
        % scale information anyways (just orientation and position)
        obj.surfaces(i).dirty;
        
        % would also need to rotate, scale, etc. (or update) group origins
        % => let operations handle this
%         newbpolys = obj.surfaces(i).createBoundarypoly;
%         obj.surfaces(i).boundarypoly = SceneFeature.copyFeatsarrayFrom(obj.surfaces(i).boundarypoly,newbpolys);
%         delete(newbpolys(isvalid(newbpolys)));
    end

    if not(isempty(obj.planarproxy))
%         obj.planarproxy.move(relscale.*(obj.planarproxy.position-obj.position(1:2)) + obj.position(1:2),'absolute');
        obj.planarproxy.scale(relscale(1:2),'relative',[0;0],0); % scale about origin
    end
end

function mirrorAboutOriginImpl(obj,mirchange)
    
    if not(mirchange)
        return;
    end

    if not(isempty(obj.verts))
        obj.verts= [obj.verts(1,:);-obj.verts(2,:);obj.verts(3,:)]; 
    end
    if not(isempty(obj.vnormals))
        obj.vnormals(2,:) = -obj.vnormals(2,:);
    end
    if not(isempty(obj.p_fnormals))
        obj.p_fnormals(2,:) = -obj.p_fnormals(2,:);
    end
    
    % face areas remain the same
    % volume remains the same

    % to keep winding order of faces the same
    if not(isempty(obj.faces))
        obj.faces = obj.faces([1,3,2],:);
    end

    if not(isempty(obj.triedges))
        obj.triedges = [];
    end
    
    if not(isempty(obj.p_edges))
        % need to recompute
        obj.p_edges = [];
    end
    if not(isempty(obj.p_edgefaces))
        % need to recompute
        obj.p_edgefaces = [];
    end
    if not(isempty(obj.p_isboundarye))
        % need to recompute
        obj.p_isboundarye = []; 
    end

    % volume remains the same
    
    if not(isempty(obj.p_center))
        obj.p_center = [obj.p_center(1);-obj.p_center(2);obj.p_center(3)];
    end
    
    if not(isempty(obj.p_projection))
%         obj.p_projection = [];
        % reverse outline to keep winding order the same
        obj.p_projection(2,:) = -obj.p_projection(2,:);
        obj.p_projection = obj.p_projection(:,[1,end:-1:2]);
    end

    for i=1:numel(obj.surfaces)
        error('Surfaces update not yet implemented for mesh mirroring.');
        % todo: order of the vertices of the surface change bacause
        % face vertex indices change (to keep face winding order the same)
        % => need to re-order vertex params and dirty
        obj.surfaces(i).dirty;
    end

    if not(isempty(obj.planarproxy))
        obj.planarproxy.mirror(mirchange,'relative',[0;0],0); % mirror about origin
    end
end

function s = similarityImpl(obj,obj2)
%     v1 = obj.volume;
%     for i=1:numel(obj2)
%         v2 = obj2(i).volume;
%         s(i) = 1/log((max(v1,v2)/min(v1,v2))*exp(1));
%     end
    s = ones(numel(obj),numel(obj2)); % constant for now
%     error('Not yet implemented.');
end

end

% methods(Static)
% 
% function obj = loadobj(s)
%     disp(s);
% end
% 
% end

end
