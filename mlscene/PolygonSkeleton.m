classdef PolygonSkeleton < handle

properties
    verts = zeros(2,0);
    edges = zeros(2,0);
    verttime = []; % roughly the distance from the boundary, depending on the given parameter

    boundaryvertinds = [];
    prunededgemask = [];
end
    
properties(Dependent, SetAccess=private)
    prunedvertmask;
    
    polygon;
    polygoncorners;
end

methods
    
    function val = get.prunedvertmask(obj)
        val = true(1,size(obj.verts,2));
        val(obj.edges(1,not(obj.prunededgemask))) = false;
        val(obj.edges(2,not(obj.prunededgemask))) = false;        
    end
    
    function val = get.polygon(obj)
        val = obj.verts(:,obj.boundaryvertinds);
    end
    
    function val = get.polygoncorners(obj)
        prunedboundaryvertmask = obj.prunedvertmask(obj.boundaryvertinds);
        val = obj.verts(:,obj.boundaryvertinds(prunedboundaryvertmask));
    end
    
    % no cornerverts => skeleton = medial axis
    function obj = PolygonSkeleton(polygon,cornervertinds,timetype)
        if nargin == 0
            % do nothing
        elseif nargin == 1 && isa(polygon,'PolygonSkeleton')
            obj.copyFrom(polygon);
        else
            if nargin < 3 || isempty(timetype)
                timetype = 'distance';
            end

            [sx,sy,st,binds,efrom,eto] = ...
                polygonInnerVoronoiDiagram(polygon(1,:),polygon(2,:),timetype);

            if not(all(binds == 1:numel(binds)))
                error('Boundary vertices not sorted correctly.');
                % todo: sort vertices and update edges if not sorted correctly
            end

            obj.verts = [sx;sy];
            obj.edges = [efrom;eto];
            obj.verttime = st;

            obj.boundaryvertinds = binds;

            if nargin >= 2 %&& not(isempty(cornervertinds))
                prunedvmask = false(1,size(obj.verts,2));
                prunedvmask(obj.boundaryvertinds) = true;
                prunedvmask(cornervertinds) = false;
                obj.prunededgemask = obj.prune(prunedvmask);
            else
                obj.prunededgemask = false(1,size(obj.edges,2));
            end
        end
    end
    
    function copyFrom(obj,obj2)
        obj.verts = obj2.verts;
        obj.edges = obj2.edges;
        obj.verttime = obj2.verttime;

        obj.boundaryvertinds = obj2.boundaryvertinds;
        obj.prunededgemask = obj2.prunededgemask;
    end
    
    function obj2 = clone(obj)
        obj2 = PolygonSkeleton(obj);
    end
    
    function prunededgemask = prune(obj,prunedvmask)
        % just prune the edges directly adjacent to the given vertices
        
        prunededgemask = prunedvmask(obj.edges(1,:)) | prunedvmask(obj.edges(2,:));
    end
    
end

end
