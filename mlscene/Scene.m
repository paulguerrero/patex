classdef Scene < SceneElementSet

properties(SetAccess={?Scene,?SceneImporter,?SceneExporter})
    name = '';
    
    rootgroups = SceneGroup.empty;

    % default categories
    categories = {};
    catattributes = struct.empty(1,0);
    
%     elements = ScenePolygon.empty; % all elements of the scene group
    groups = SceneGroup.empty;
end

methods
    
% Scene(cat,catattr)
function obj = Scene(varargin)
    
    obj = obj@SceneElementSet;
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 2
        cat = varargin{1};
        catattr = varargin{2};
        
        % validate categories
        if numel(catattr) ~= numel(cat)
            error('Invalid size for the category attributes.');
        end

        ncat = numel(cat);

        [cat,ind,~] = unique(cat);
        catattr = catattr(ind);

        if numel(cat) ~= ncat
            warning('Scene:duplicatecats','Duplicate categories detected, ignoring duplicates.');
        end

        mask = strcmp(cat,'');
        cat(mask) = [];
        catattr(mask) = [];

        obj.categories = cat;
        obj.catattributes = catattr;
    else
        error('Invalid arguments.');
    end
end

function delete(obj)
    delete(obj.rootgroups(isvalid(obj.rootgroups)));
end

function copyFrom(obj,obj2) %#ok<INUSD>
    error('Copying a scene is not supported.');
end

function obj2 = clone(obj) %#ok<STOUT,MANU>
    error('Cloning a scene is not supported.');
end

function setName(obj,name)
    obj.name = name;
end

function updateElements(obj,updategroups)
    if nargin < 2 || isempty(updategroups)
        updategroups = true;
    end
    
    obj.updateElements@SceneElementSet;
    
    if updategroups
        for i=1:numel(obj.groups)
            obj.groups(i).updateElements(false);
        end
    end
end

function addRootgroups(obj,groups)
    groups(ismember(groups,obj.rootgroups)) = [];
    
    if isempty(groups)
        return;
    end
    if numel(groups) ~= numel(unique(groups))
        error('Some groups are given multiple times.');
    end
    
    obj.rootgroups = [obj.rootgroups,groups];
    for i=1:numel(groups)
        groups(i).makeRootgroup(obj);
    end
end

function rgroups = removeRootgroups(obj,groups)
    if nargin < 2
        groups = obj.rootgroups;
    end
    
    [~,grpinds] = ismember(obj.rootgroups,groups);
    if any(grpinds<1)
        error('Some given groups are not root groups of the scene.');
    end
    
    if isempty(grpinds)
        return;
    end
    if numel(grpinds) ~= numel(unique(grpinds))
        error('Some groups are given multiple times.');
    end
    
    rgroups = obj.rootgroups(grpinds);
%     obj.rootgroups(grpinds) = [];
    for i=1:numel(groups)
        if not(isempty(groups(i).scene)) && groups(i).scene == obj
            groups(i).makeRootgroup(Scene.empty); % this also removes the groups from the root groups
        end
    end
    
    if nargout < 1
        if not(isempty(rgroups))
            delete(rgroups([rgroups.isroot]));
        end
    end
end

function [newelmfeatinds,newsubfeatinds] = addElements(obj,elms,groups)
    
    newelmfeatinds = zeros(1,0);
    newsubfeatinds = zeros(1,0);
    
    if isempty(elms)
        return;
    end

    if numel(groups) == 1
        grps = groups;
    else
        [grps,~,elmgrpinds] = unique(groups);
    end
    
    if any([grps.scene]~=obj)
        error('Not all given groups are part of the scene.');
    end
    
    oldelmcount = numel(obj.elements);
    oldfeatcount = numel(obj.features);
    
    if numel(grps) == 1
        grps.addElements(elms);
    else
        % arrange according to groups
        grpelms = array2cell(elms,elmgrpinds,numel(grps),2);
        
        for i=1:numel(grps)
            grps(i).addElements(grpelms{i});
        end
    end
    
    newelmfeatinds = oldelmcount+1:numel(obj.elements);
    newsubfeatinds = (oldfeatcount-oldelmcount)+1:(numel(obj.features)-numel(obj.elements));
    newsubfeatinds = newsubfeatinds + numel(obj.elements);
end

function [relms,relmgroups] = removeElements(obj,elms)
    if nargin <= 1
        elms = obj.elements;
    end
    
    if isnumeric(elms)
        % element indices given
        elms = obj.elements(elms);
    end
    
    if nargout == 0
        Scene.removeElements_s(elms,obj);
    elseif nargout == 1
        [relms] = Scene.removeElements_s(elms,obj);
    else
        [relms,relmgroups] = Scene.removeElements_s(elms,obj);
    end

end

function [bbmin,bbmax,bbdiag] = boundingbox(obj)
    if isempty(obj.groups)
        bbmin = [0;0;0];
        bbmax = [0;0;0];
        bbdiag = 0;
        return;
    end
    
    scenegroups = obj.groups;
    scenegroupposes = [scenegroups.globaloriginpose];
    
    [bbmin,bbmax] = scenegroups.boundingbox;
    bbverts = trimeshAACuboid(bbmin,bbmax);

    scenegroupposes = repmat(scenegroupposes,8,1);
    scenegroupposes = reshape(scenegroupposes,size(identpose3D,1),[]);
    
    bbverts = posetransform(bbverts,scenegroupposes,repmat(identpose3D,1,size(bbverts,2)));
    
    bbmin = min(bbverts,[],2);
    bbmax = max(bbverts,[],2);
    
    if nargout >= 3
        bbdiag = sqrt(sum((bbmax-bbmin).^2,1));
    end
end

% function [bbmin,bbmax,bbdiag] = boundingbox2D(obj)
%     error('Not yet implemented');
%     % todo: transform bounding boxes of all groups to the coordinates of
%     % this Scene and then take min and max of them
% end

function rescale(obj,scale)
    for i=1:numel(obj.rootgroups)
        obj.rootgroups(i).rescale(scale);
    end
end

function addCategory(obj,name,attr)
    if isempty(name)
        warning('Scene:emptyCategory','Category name empty, cannot add.');
        return;
    end
    
    if ismember(name,obj.categories)
        warning('Scene:duplicateCategory','Category already exists, cannot add.');
        return;
    end
    
    if nargin < 3
        attr = struct;
    end
    
    obj.categories{end+1} = name;
    if isempty(obj.catattributes) % first entry sets attributes
        obj.catattributes = attr;
    else
        obj.catattributes(end+1) = attr;
    end
end

function removeCategory(obj,ind)
    if ischar(ind)
        ind = find(strcmp(ind,obj.categories),1,'first');
    end
    if isempty(ind)
        warning('Scene:nonexistentCategory','Category not found, cannot remove.');
        return;
    end
    
    feats = obj.allfeatures;
    
    indlabelinds = find(feats.labelsAreInd);
    featindlabels = feats(indlabelinds).getLabels;

    for i=1:numel(indlabelinds)
        featindlabels{i}(featindlabels{i} == ind) = [];
        mask = featindlabels{i} > ind;
        featindlabels{i}(mask) = featindlabels{i}(mask) - 1;
    end
    
    feats(indlabelinds).setLabels(featindlabels);
    
    obj.categories(ind) = [];
    obj.catattributes(ind) = [];
end

end % methods

methods(Access={?Scene,?SceneGroup})
    
% SceneGroup updates Scene with these methods if something is changed in on
% of the SceneGroups

function elementsAdded(obj,elms)
    if not(isempty(elms))
        obj.elements = [obj.elements,elms];
        obj.updateElements(false);
    end
end

function elementsRemoved(obj,elms)
    if not(isempty(elms))
        obj.elements(ismember(obj.elements,elms)) = [];
        obj.updateElements(false);
    end
end
    
function groupsAdded(obj,groups)
    obj.groups = [obj.groups,groups];
    if not(all([groups.scene] == obj))
        error('Some groups are not member of this scene.');
    end
    obj.elementsAdded([groups.elements]);
end

function groupsRemoved(obj,groups)
    obj.groups(ismember(obj.groups,groups)) = [];
    obj.rootgroups(ismember(obj.rootgroups,groups)) = [];
    if any([groups.scene] == obj)
        error('Some groups are still member of this scene.');
    end
    obj.elementsRemoved([groups.elements]);
end

end

methods(Static)

function addLabelsToParent(feats,featscenes)
    if isempty(feats)
        return;
    end
    
    if nargin < 2 || isempty(featscenes)
        feats(arrayfun(@(x) isempty(x.scenegroup),feats)) = [];
        feats(arrayfun(@(x) isempty(x.scenegroup.scene),feats)) = [];
        featscenes = [feats.scenegroup.scene];
    end
    
    if numel(feats) ~= numel(featscenes)
        error('Must provide as many Scenes as features.');
    end
    
    [scenes,~,featsceneinds] = unique(featscenes);
    scenefeats = array2cell(feats,featsceneinds,numel(scenes),2);
    for i=1:numel(scenes)
        
        % add mesh planarproxys and boundary polygons (must be in same Scene)
        scenemeshes = scenefeats{i}(strcmp({scenefeats{i}.type},'mesh'));
        if not(isempty(scenemeshes))
            scenemeshsurfaces = [scenemeshes.surfaces];
            scenefeats{i} = [scenefeats{i},[scenemeshes.planarproxy],[scenemeshsurfaces.boundarypoly]];
        end
        
        % add subfeatures of Scene elements
        elmmask = false(1,numel(scenefeats{i}));
        for j=1:numel(scenefeats{i})
            elmmask(j) = isa(scenefeats{i}(j),'SceneElement');
        end
        if any(elmmask)
            subfeats = scenefeats{i}(elmmask).subfeatures;
            scenefeats{i} = [scenefeats{i},[subfeats{:}]];
        end

        % get unique set of missing labels
        if not(isempty(scenefeats{i}))
            labels = scenefeats{i}.getLabelStr;
            labels = unique([labels{:}]);
%             labels(strcmp(labels,'')) = []; % remove empty label
            labels = labels(not(ismember(labels,scenes(i).categories)));

            % add missing labels to the Scene
            for j=1:numel(labels)
                scenes(i).addCategory(labels{j});
            end
        end
    end
end

function [relms,relmgroups] = removeElements_s(elms,scene)

    relms = ScenePolygon.empty;
    relmgroups = SceneGroup.empty;

%     % remove elements without groups
%     elms(arrayfun(@(x) isempty(x.scenegroup),elms)) = [];

    if nargin < 2
        scene = Scene.empty(1,0);
    end

    if isempty(elms)
        return;
    end
    
    grps = elms.getScenegroup;
    hasgrp = not(cellisempty_mex(grps));
    grps = [grps{:}];
    
    elms(not(elms.hasScenegroup)) = [];
    
    % arrange according to groups
    [grps,~,elmgrpinds] = unique(grps);
    
    if not(isempty(scene)) && (any([grps.scene]~=scene) || any(not(hasgrp)))
        error('Not all elements are part of the scene.');
    end
    
    if numel(grps) == 1
        relms = grps.removeElements(elms);
        relmgroups = repmat(grps,1,numel(relms));
    else
        
        grpelms = array2cell(elms,elmgrpinds,numel(grps),2);

        % sort by depth so elements of child groups are always removed
        % before their parent groups (the child groups may get removed
        % themselves later)
        [~,perm] = sort([grps.hierarchydepth],'descend');
        grps = grps(perm);
        grpelms = grpelms(perm);

        % remove elements for each group
        for i=1:numel(grps)
            re = grps(i).removeElements(grpelms{i});

            relms = [relms,re]; %#ok<AGROW>
            relmgroups = [relmgroups,repmat(grps(i),1,numel(re))]; %#ok<AGROW>
        end
    end
    
    if nargout < 1
        for i=1:numel(relms) % one after the other elements may delete other elements (e.g. mesh its planarproxy)
            if isvalid(relms(i))
                delete(relms(i));
            end
        end
    end
end

end

end % classdef
