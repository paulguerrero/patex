classdef SceneFields < handle

properties(SetAccess={?SceneFields,?SceneImporter,?SceneExporter})

    scenegroup = SceneGroup.empty;
    
    walkablePoly = {};
    tri = PolyTri.empty;
    grid = PolyGrid.empty;
    
    gridField = GridVectorfield2D.empty;    
    triField = TriVectorfield2D.empty;
    
    % global constraint fields
%     goals = {}; % goal positions
%     goals = {[280,-280;...
%              40,-40]};
    goals = {[0;
             -21]};
    distancefields = {}; % length of shortest geodesic path to closest goal
    velocityfields = {};
    obstaclefield = {}; % pointing away from obstacles in close proximity
    
    constrainttypenames = {'point','curve'};
    constraints = {};
    constrainttypes = {};
    constraintweights = [];
    
end

properties(Dependent)
    scene;
end

methods
    
% SceneFields(scenegroup)
function obj = SceneFields(varargin)
    if isempty(varargin)
        % do nothing
    elseif numel(varargin) == 1
        obj.scenegroup = varargin{1};
        
        if not(isempty(obj.walkablePoly))
            obj.createTriangulation;
            obj.createGrid;
        end
        
%     obj.createTriVectorfield;
% %     obj.createGridVectorfield;
%         
%     obj.computeDistancefields;
%     obj.computeVelocityfields;
%     obj.computeObstacleField;
%     obj.computeVectorfield;
    else
        error('Invalid arguments.');
    end
end

% destructor
function delete(obj)
    mask = isvalid(obj.tri);
    delete(obj.tri(mask));
    
    mask = isvalid(obj.grid);
    delete(obj.grid(mask));
    
    mask = isvalid(obj.triField);
    delete(obj.triField(mask));
    
    mask = isvalid(obj.gridField);
    delete(obj.gridField(mask));
end

function val = get.scene(obj)
    val = obj.scenegroup.scene;
end

function rescale(obj,scale)
    mask = isvalid(obj.triField);
    delete(obj.triField(mask));
    mask = isvalid(obj.gridField);
    delete(obj.gridField(mask));
    
    mask = isvalid(obj.tri);
    delete(obj.tri(mask));
    mask = isvalid(obj.grid);
    delete(obj.grid(mask));
    
    if not(isempty(obj.walkablePoly))
        obj.walkablePoly = cellfun(@(x) cellfun(@(y)...
            y.*scale, x,'UniformOutput',false),obj.walkablePoly,'UniformOutput',false);
    end
    obj.texmin = obj.texmin .* scale;
    obj.texmax = obj.texmax .* scale;

    obj.tri = PolyTri.empty;
    obj.grid = PolyGrid.empty;
    
    obj.gridField = GridVectorfield2D.empty;    
    obj.triField = TriVectorfield2D.empty;
end

function computeWalkablePolygon(obj)
    % find concave hull of all closed polylines
    obj.walkablePoly = [];
    
%     polys = {};
%     areas = [];
%     wlk = [];
    
    polys = {obj.scenegroup.polygons.verts};
    polyiswalkable = false(1,numel(obj.scenegroup.polygons));
    for i=1:numel(obj.scenegroup.polygons)
        polyiswalkable(i) = any(obj.scene.walkable(obj.scenegroup.polygons.labels));
    end
    areas = zeros(1,numel(polys));
    for i=1:numel(polys)
        areas(i) = polyarea(polys{i}(1,:),polys{i}(2,:));
    end
    
%     for i=1:numel(obj.scenegroup.polygons)
%         % separate the disconnected parts of the polygon (delimited by
%         % nans)
%         naninds = find(isnan(obj.scenegroup.polygons(i).verts(1,:)));
%         startinds = [1,naninds+1];
%         endinds = [naninds-1,size(obj.scenegroup.polygons(i).verts,2)];
%         for j=1:numel(naninds)+1
%             polys{end+1} = obj.scenegroup.polygons(i).verts(:,startinds(j):endinds(j)); %#ok<AGROW>
%             areas(end+1) = polyarea(polys{end}(1,:),polys{end}(2,:)); %#ok<AGROW>
%             wlk(end+1) = obj.scenegroup.polygons(i).label > 0 && obj.scene.walkable(obj.scenegroup.polygons(i).label); %#ok<AGROW>
%         end
%     end
    
    % sort by area
    [~,perm] = sort(areas,'descend');
    polys = polys(perm);
    polyiswalkable = polyiswalkable(perm);
%     areas = areas(perm);
    
    startind = find(polyiswalkable,1,'first');
    if isempty(startind)
        wpoly_x = [];
        wpoly_y = [];
    else
        wpoly_x = polys{startind}(1,:);
        wpoly_y = polys{startind}(2,:);
        for i=startind+1:numel(polys)
            if polyiswalkable(i)
                [wpoly_x,wpoly_y] = polybool('union',...
                    wpoly_x,wpoly_y,...
                    polys{i}(1,:),polys{i}(2,:));
            else
                [wpoly_x,wpoly_y] = polybool('subtraction',...
                    wpoly_x,wpoly_y,...
                    polys{i}(1,:),polys{i}(2,:));
            end
        end
    end
    
    obj.walkablePoly = [wpoly_x;wpoly_y];
    
    if isempty(obj.walkablePoly)
        warning('Scene:emptyDomain','empty walkable polygon');
    end
    
	% remove duplicate vertex at end of each contour component
    if not(isempty(obj.walkablePoly))
        naninds = find(isnan(obj.walkablePoly(1,:)));
        startinds = [1,naninds+1];
        endinds = [naninds-1,size(obj.walkablePoly,2)];
        mask = all(obj.walkablePoly(:,startinds) == obj.walkablePoly(:,endinds),1);
        obj.walkablePoly(:,endinds(mask)) = [];
    end
    
    % find connected components
    if not(isempty(obj.walkablePoly))
        wpoly = obj.walkablePoly;
        
        [contcompsx,contcompsy] = polysplit(wpoly(1,:),wpoly(2,:));
        contourcomps = cellfun(@(x,y) [x;y], contcompsx,contcompsy,'UniformOutput',false);

        compareas = cellfun(@(x,y) polyarea(x,y),contcompsx,contcompsy);
        exteriormask = ispolycw(contcompsx,contcompsy);
        exteriorinds = find(exteriormask);
        interiorinds = find(not(exteriormask));

        polyinpoly = sparse(numel(interiorinds),numel(exteriorinds));
        for i=1:numel(exteriorinds)
            for j=1:numel(interiorinds)
                polyinpoly(j,i) = all(pointInPolygon_mex(...
                    contourcomps{interiorinds(j)}(1,:),...
                    contourcomps{interiorinds(j)}(2,:),...
                    contourcomps{exteriorinds(i)}(1,:),...
                    contourcomps{exteriorinds(i)}(2,:))); 
            end
        end

        % sort exterior contour polygons by area (smallest first)
        [~,perm] = sort(compareas(exteriorinds),'ascend');
        polyinpoly = polyinpoly(:,perm);

        obj.walkablePoly = cell(1,numel(exteriorinds));
        for i=1:numel(exteriorinds)
            obj.walkablePoly{i} = contourcomps(exteriorinds(i));
        end
        
        for i=1:numel(interiorinds)
            enclosingexteriorind = perm(find(polyinpoly(i,:) > 0,1,'first'));
            obj.walkablePoly{enclosingexteriorind} = [...
                obj.walkablePoly{enclosingexteriorind},...
                contourcomps(interiorinds(i))];
        end
    end
end

function updateWalkablePolygon(obj,polyind)
%     if numel(oind) ~= 1
%         error('just one polygon at a time supported');
%     end

    polys = obj.scenegroup.polygons;
    polyiswalkable = false(1,numel(polys));
    for i=1:numel(polys)
        polyiswalkable(i) = any(obj.scene.walkable(polys.labels));
    end
    
    % find polygons with intersecting boundingboxes
    for pi=polyind
        [mybbmin,mybbmax] = polys(pi).boundingbox;
        [otherbbmin,otherbbmax] = polys.boundingbox;
        inds = find(otherbbmin(1,:) < mybbmax(1) & otherbbmin(2,:) < mybbmax(2) & ...
                    otherbbmax(1,:) > mybbmin(1) & otherbbmax(2,:) > mybbmin(2));
        inds(inds == pi) = [];

        % compute polygon areas
        myarea = polyarea(polys(pi).verts(1,:),polys(pi).verts(2,:));
        otherareas = zeros(1,numel(inds));
        for i=1:numel(inds)
            otherareas(i) = polyarea(polys(inds(i)).verts(1,:),polys(inds(i)).verts(2,:));
        end

        % sort by area and remove polygons with larger area than the one being
        % added
        mask = otherareas < myarea;
        inds = inds(mask);
        otherareas = otherareas(mask);
        [~,perm] = sort(otherareas,'descend');
        inds = inds(perm);
    %     otherareas = otherareas(perm);

        clear otherareas;

        % subtract & add all smaller intersecting polygons
        x = polys(pi).verts(1,:);
        y = polys(pi).verts(2,:);
        for i=inds
            if polyiswalkable(i) == polyiswalkable(pi)
                [x,y] = polybool('union',x,y,...
                    polys(i).verts(1,:),...
                    polys(i).verts(2,:));
            else
                [x,y] = polybool('subtraction',x,y,...
                    polys(i).verts(1,:),...
                    polys(i).verts(2,:));
            end
        end
        
        % cut off everything outside the area of the polygon being added
        [x,y] = polybool('intersection',x,y,...
            polys(pi).verts(1,:),...
            polys(pi).verts(2,:));

        % subtract/add to the walkable polygon
        wpolyx = cell(1,numel(obj.walkablePoly));
        wpolyy = cell(1,numel(obj.walkablePoly));
        for i=1:numel(obj.walkablePoly)
            [wpolyx{i},wpolyy{i}] = ...
                polyjoin(cellfun(@(x) x(1,:),obj.walkablePoly{i},'UniformOutput',false),...
                         cellfun(@(x) x(2,:),obj.walkablePoly{i},'UniformOutput',false));
        end
        [wpolyx,wpolyy] = polyjoin(wpolyx,wpolyy);

        if polyiswalkable(pi)
            [wpolyx,wpolyy] = polybool('union',wpolyx,wpolyy,x,y);
        else
            [wpolyx,wpolyy] = polybool('subtraction',wpolyx,wpolyy,x,y);
        end

        clear x y;
        
        if iscolumn(wpolyx)
            wpolyx = wpolyx';
        end
        if iscolumn(wpolyy)
            wpolyy = wpolyy';
        end

        obj.walkablePoly = [wpolyx;wpolyy];

%         if isempty(obj.walkablePoly)
%             warning('Scene:emptyDomain','empty walkable polygon');
%         end

        % remove duplicate vertex at end of each contour component
        if not(isempty(obj.walkablePoly))
            naninds = find(isnan(obj.walkablePoly(1,:)));
            startinds = [1,naninds+1];
            endinds = [naninds-1,size(obj.walkablePoly,2)];
            mask = all(obj.walkablePoly(:,startinds) == obj.walkablePoly(:,endinds),1);
            obj.walkablePoly(:,endinds(mask)) = [];
        end

        % find connected components
        if not(isempty(obj.walkablePoly))
            wpoly = obj.walkablePoly;

            [contcompsx,contcompsy] = polysplit(wpoly(1,:),wpoly(2,:));
            contourcomps = cellfun(@(x,y) [x;y], contcompsx,contcompsy,'UniformOutput',false);

            compareas = cellfun(@(x,y) polyarea(x,y),contcompsx,contcompsy);
            exteriormask = ispolycw(contcompsx,contcompsy);
            exteriorinds = find(exteriormask);
            interiorinds = find(not(exteriormask));

            polyinpoly = sparse(numel(interiorinds),numel(exteriorinds));
            for i=1:numel(exteriorinds)
                for j=1:numel(interiorinds)
                    polyinpoly(j,i) = all(pointInPolygon_mex(...
                        contourcomps{interiorinds(j)}(1,:),...
                        contourcomps{interiorinds(j)}(2,:),...
                        contourcomps{exteriorinds(i)}(1,:),...
                        contourcomps{exteriorinds(i)}(2,:)));%,edges');%,TOL)  
                end
            end

            % sort exterior contour polygons by area (smallest first)
            [~,perm] = sort(compareas(exteriorinds),'ascend');
            polyinpoly = polyinpoly(:,perm);

            obj.walkablePoly = cell(1,numel(exteriorinds));
            for i=1:numel(exteriorinds)
                obj.walkablePoly{i} = contourcomps(exteriorinds(i));
            end

            for i=1:numel(interiorinds)
                enclosingexteriorind = perm(find(polyinpoly(i,:) > 0,1,'first'));
                obj.walkablePoly{enclosingexteriorind} = [...
                    obj.walkablePoly{enclosingexteriorind},...
                    contourcomps(interiorinds(i))];
            end
        end
    end
end

function createTriangulation(obj)
    mask = isvalid(obj.tri);
	delete(obj.tri(mask));
    obj.tri = PolyTri.empty;
   
    if isempty(obj.walkablePoly)
        error('compute the walkable polygon first');
    end
    
    minangle = 30.0; % in degrees
    
    maxarea = 0.2*0.2; % assuming the scale is 1 unit = 1 meter
    
    % to avoid excessive triangulation if the scale is not set correctly
    [bbmin,bbmax] = pointsetBoundingbox([obj.walkablePoly{:}]);
    maxarea = max(maxarea,(bbmax(1)-bbmin(1)) * (bbmax(2)-bbmin(2)) * 0.00005);
    
%     maxarea = 0.5*0.5; % assuming the scale is 1 unit = 1 meter
    
    for c=1:numel(obj.walkablePoly)
        boundary = obj.walkablePoly{c}{1};
        holes = {};
        for i=2:numel(obj.walkablePoly{c})
            holes = [holes,obj.walkablePoly{c}{i}]; %#ok<AGROW>
        end
        obj.tri(c) = PolyTri(boundary,holes,true,minangle,maxarea);
    end
end

function createGrid(obj)
    mask = isvalid(obj.grid);
    delete(obj.grid(mask));
    obj.grid = PolyGrid.empty;
    
    if isempty(obj.walkablePoly)
        error('compute a walkable polygon first');
    end
    
    boundaries = cell(1,numel(obj.walkablePoly));
    holes = cell(1,numel(obj.walkablePoly));
    for c=1:numel(obj.walkablePoly)
        boundaries{c} = obj.walkablePoly{c}{1};
        holes{c} = {};
        for i=2:numel(obj.walkablePoly{c})
            holes{c} = [holes{c},obj.walkablePoly{c}{i}];
        end
    end
    
	reshint = 10000; % hint for the total number of grid nodes
    tic;
    obj.grid = PolyGrid(boundaries,holes,reshint);
    toc;
end

% function createHoughgrid(obj)
%     reshint = 100000; % hint for the total number of grid nodes    
%     
%     [bbmin,bbmax] = pointsetBoundingbox([obj.walkablePoly{:}]);
%     bbsize = bbmax-bbmin;
%     
%     aspectratio = bbsize(1) / bbsize(2);
%     resy = ceil(sqrt(reshint/aspectratio));
%     resx = ceil(reshint/resy);
%     obj.houghgrid = zeros(resy,resx);
%     obj.houghgridmin = bbmin+(bbsize./[resx;resy]).*0.5;
%     obj.houghgridmax = bbmax-(bbsize./[resx;resy]).*0.5;
% end

function clearVectorfield(obj)
    
    mask = isvalid(obj.tri);
    delete(obj.tri(mask));
    
    mask = isvalid(obj.grid);
    delete(obj.grid(mask));
    
    mask = isvalid(obj.triField);
    delete(obj.triField(mask));
    
    mask = isvalid(obj.gridField);
    delete(obj.gridField(mask));
    
    obj.tri = PolyTri.empty;
    obj.grid = PolyGrid.empty;
    
    obj.gridField = GridVectorfield2D.empty;    
    obj.triField = TriVectorfield2D.empty;

    obj.distancefields = {}; % length of shortest geodesic path to closest goal
    obj.velocityfields = {};
    obj.obstaclefield = {}; % pointing away from obstacles in close proximity
end

function computeVectorfield(obj)
    if isempty(obj.triField)
        obj.createTriVectorfield;
    end
        
    obj.computeDistancefields;
    obj.computeVelocityfields;
    obj.computeObstacleField;
    obj.computeConstrainedVectorfield;
end

function createTriVectorfield(obj)
    mask = isvalid(obj.triField);
	delete(obj.triField(mask));
    obj.triField = TriVectorfield2D.empty;
   
    if isempty(obj.tri)
        obj.createTriangulation;
%         error('compute the triangulation first');
    end
    
    for c=1:numel(obj.tri)
        obj.triField(c) = TriVectorfield2D(obj.tri(c));
        obj.triField(c).precompute;
    end
end

function createGridVectorfield(obj)
    mask = isvalid(obj.gridField);
    delete(obj.gridField(mask));
    obj.gridField = GridVectorfield2D.empty;
    
    if isempty(obj.grid)
        obj.createGrid;
%         error('compute the grid first');
    end
    
    obj.gridField = GridVectorfield2D(obj.grid);
end

function computeConstrainedVectorfield(obj)
	if isempty(obj.velocityfields)
        error('compute velocity field first');
	end
	if isempty(obj.obstaclefield)
        error('compute velocity field first');
	end
    
    if not(isempty(obj.triField))
        % add all constraints to the triangle vectorfield
        pointmask = strcmp(obj.constrainttypes,'point');
        curvemask = strcmp(obj.constrainttypes,'curve');
        pconstraints = [obj.constraints{pointmask}];
        cconstraints = obj.constraints(curvemask);
        for c=1:numel(obj.triField)
            obj.triField(c).removeConstraints;

            if not(isempty(pconstraints))
                obj.triField(c).addVectorConstraint(...
                    pconstraints(1,:),...
                    pconstraints(2,:),...
                    pconstraints(3,:),...
                    pconstraints(4,:),...
                    ones(1,size(pconstraints,2)).*10);
            end

            for i=1:numel(cconstraints)
                obj.triField(c).addCurveConstraint(...
                    cconstraints{i}(1,:),...
                    cconstraints{i}(2,:),...
                    ones(1,size(cconstraints{i},2)).*5);
            end

            mergedfield = obj.velocityfields{1}{c}.*0.7+obj.obstaclefield{c}.*0.3;
            obj.triField(c).addFieldConstraint(...
                mergedfield(1,:),mergedfield(2,:),0.1);

            obj.triField(c).computeEdgeCoeffs;
            obj.triField(c).reconstructVectorfield;
            
%             obj.triField(c).vectors = mergedfield;
        end
    elseif not(isempty(obj.gridField))
        error('computing the complete vectorfield for grids is not supported yet');
    else
        error('create vectorfield first');
    end
end

function computeDistancefields(obj)
    obj.distancefields = {};
    
    if not(isempty(obj.triField))
        for c=1:numel(obj.triField)
            D = obj.computeWalkablePolyGeodists(...
                obj.triField(c).tri.v',...
                obj.goals,...
                ones(1,size(obj.triField(c).tri.v,1)).*3);        

            for i=1:numel(obj.goals)
                obj.distancefields{i}{c} = D(:,i);
            end
        end
    elseif not(isempty(obj.gridField))
        D = obj.computeWalkablePolyGeodists(obj.gridField.gridnodes,obj.goals);

        for i=1:numel(obj.goals)
            obj.distancefields{i} = D(:,i);
        end
    else
        error('create the vector field first');    
    end
end

function computeVelocityfields(obj)
    obj.velocityfields = {};
    
    if isempty(obj.distancefields)
        error('compute distance fields first');
    end
    
    if not(isempty(obj.triField))
        for i=1:numel(obj.distancefields)
            for c=1:numel(obj.triField)
                [dfx,dfy] = trigradient(...
                    obj.tri(c).f,...
                    obj.tri(c).v(:,1),...
                    obj.tri(c).v(:,2),...
                    obj.distancefields{i}{c});
                obj.velocityfields{i}{c} = [-dfx';-dfy'];
            end
        end
    elseif not(isempty(obj.gridField))
        for i=1:numel(obj.distancefields)
            obj.velocityfields{i} = -partialGridGradient(...
                obj.distancefields{i}, ...
                obj.gridField.grid.nodes, ...
                obj.gridField.grid.nedges);
        end
    else
        error('create the vector field first');
    end
end

function computeObstacleField(obj)
    obj.obstaclefield = {};
    
	if not(isempty(obj.triField))
        triFieldsExtent = sqrt(sum((max(cell2mat(cellfun(@(x) max(x.tri.v,[],1)',...
                                                 num2cell(obj.triField),'UniformOutput',false)),[],2) - ...
                                    min(cell2mat(cellfun(@(x) min(x.tri.v,[],1)',...
                                                 num2cell(obj.triField),'UniformOutput',false)),[],2)).^2,1));
        usenormalthresh = 0.000001 * triFieldsExtent;
        repeldist = 0.5;
        maxrepelstrength = 0.001;%40;

        % distance of all vertices to the nearest boundary
        obj.obstaclefield = cell(1,numel(obj.triField));
        for c=1:numel(obj.triField)
            obj.obstaclefield{c} = zeros(2,size(obj.triField(c).tri.v,2));

            evinds = obj.triField(c).tri.edges';
            bevinds = evinds(:,obj.triField(c).tri.boundaryedgeinds);
            bfinds = cell2mat(obj.triField(c).tri.edgeAttachments(bevinds'));
            bfvinds = obj.triField(c).tri.f(bfinds,:);
            eccw = false(1,size(bfvinds,1));
            for i=1:numel(bfinds)
                bfemask = bevinds(1,i) ~= bfvinds(i,:) & bevinds(2,i) ~= bfvinds(i,:);
                eccw(i) = obj.triField(c).isEdgeccw(bfemask,bfinds(i));
            end

            [d,ei,f,px,py] = pointPolylineDistance_mex(...
                obj.triField(c).tri.v(:,1)',...
                obj.triField(c).tri.v(:,2)',...
                obj.triField(c).tri.v(bevinds(1,:),1)',...
                obj.triField(c).tri.v(bevinds(1,:),2)',...
                obj.triField(c).tri.v(bevinds(2,:),1)',...
                obj.triField(c).tri.v(bevinds(2,:),2)');

%             repelstrength = min(maxrepelstrength,repeldist./d - 1);
            repelstrength = min(maxrepelstrength,max(0,min(1,1-d/repeldist)).*maxrepelstrength); % linear falloff

            mask = d < usenormalthresh;
            edgenormals = [-(obj.triField(c).tri.v(bevinds(2,:),2)' - obj.triField(c).tri.v(bevinds(1,:),2)');...
                             obj.triField(c).tri.v(bevinds(2,:),1)' - obj.triField(c).tri.v(bevinds(1,:),1)'];
            edgenormals(:,not(eccw)) = edgenormals(:,not(eccw)) .* -1;
            edgenormallen = sqrt(edgenormals(1,:).^2+edgenormals(2,:).^2);
            edgenormals = edgenormals ./ edgenormallen([1,1],:);
            vertnormals = zeros(2,size(obj.triField(c).tri.v,1));
            vertnormals = vertnormals + ...
                [accumarray(bevinds(1,:)',edgenormals(1,:)',[size(vertnormals,2),1]),...
                 accumarray(bevinds(1,:)',edgenormals(2,:)',[size(vertnormals,2),1])]';
            vertnormals = vertnormals + ...
                [accumarray(bevinds(2,:)',edgenormals(1,:)',[size(vertnormals,2),1]),...
                 accumarray(bevinds(2,:)',edgenormals(2,:)',[size(vertnormals,2),1])]';
            vertnormallen = sqrt(vertnormals(1,:).^2+vertnormals(2,:).^2);
            vertnormals = vertnormals ./ vertnormallen([1,1],:); % re-normalize
            v1inds = bevinds(1,ei);
            v2inds = bevinds(2,ei);
            obj.obstaclefield{c}(:,mask) = ...
                (vertnormals(:,v1inds(mask)) .* (1-f([1,1],mask)) + ...
                vertnormals(:,v2inds(mask)) .* f([1,1],mask)) ...
                .* repelstrength([1,1],mask);
            mask = not(mask);
            fromclosestp = (obj.triField(c).tri.v'-[px;py]) ./ d([1,1],:);
            obj.obstaclefield{c}(:,mask) = fromclosestp(:,mask) .*  repelstrength([1,1],mask);
        end
    elseif not(isempty(obj.gridField))
        error('obstacle field for grid not supported yet')
    else
    	error('compute vectorfield first');
	end
end

% g is either a 2xn list of points or a 1xn cell array
% function D = computeWalkablePolyGeodists(obj,p,g,pf,pc,gf,gc)
% point types: 1 = 'face' 2 = 'edge' 3 = 'vertex'
function [D,P] = computeWalkablePolyGeodists(obj,p,g,pt,gt)

    if isempty(obj.triField)
        error('compute a triangulation of the walkable polygon first');
    end
    
    if nargin < 3 && nargin > 5
        error('must provide 2 - 4 arguments (in addition to the class object argument)');
    end
    
    if nargin < 5
        if iscell(g)
            gt = cell(size(g));
            for i=1:numel(g)
                gt{i} = ones(1,size(g{i},2));
            end
        else
            gt = ones(1,size(g,2));
        end
    end
    
    if nargin < 4
        pt = ones(1,size(p,2));
    end
    
    mask = pt ~= 1 & pt ~= 2 & pt ~= 3;
    if any(mask)
        error('invalid point type given');
    end
    if iscell(gt)
        mask = cellfun(@(x) any(x ~= 1 & x ~= 2 & x ~= 3),gt);
    else
        mask = gt ~= 1 & gt ~= 2 & gt ~= 3;
    end
    if any(mask)
        error('invalid goal type given');
    end
    
    hasedgepoints = false;
    hasedgepoints = hasedgepoints | any(pt == 2);
    if iscell(gt)
        hasedgepoints = hasedgepoints | any(cellfun(@(x) any(x==2),gt));
    else
        hasedgepoints = hasedgepoints | any(gt == 2);
    end
    if hasedgepoints
        error('edge points not supported yet');
        % problem: the indices of these edge will probably be different than the indices of the edges in the geodesic mesh
    end
    
    [~,~,wpolyextent] = pointsetBoundingbox([obj.walkablePoly{:}]);
    tolerance = wpolyextent*0.000001;
    
%     D = ones(size(p,2),size(g,2)).*(realmax('double')*0.001);
    D = inf(size(p,2),size(g,2));
    if nargout > 1
        P = cell(size(p,2),size(g,2));
    end
    for c=1:numel(obj.tri)
        
        cpf = obj.tri(c).meshElementId(p,pt,tolerance);
        cpmask = not(isnan(cpf));
        
        cp = p(:,cpmask);
        cpt = pt(cpmask);
        cpf = cpf(cpmask);
        
        if iscell(g)
            cg = cell(size(g));
            cgf = cell(size(g));
            cgt = cell(size(g));
            for i=1:numel(g)
                cgf{i} = obj.tri(c).meshElementId(g{i},gt{i},tolerance);                
                cgmask = not(isnan(cgf{i}));
                
                cg{i} = g{i}(:,cgmask);
                cgt{i} = gt{i}(:,cgmask);
                cgf{i} = cgf{i}(cgmask);
            end
            cgmask = true(1,numel(cg));
        else
            cgf = obj.tri(c).meshElementId(g,gt,tolerance);
            cgmask = not(isnan(cgf));
            
            cg = g(:,cgmask);
            cgt = gt(:,cgmask);
            cgf = cgf(cgmask);
        end
        
        if not(isempty(cp)) && not(isempty(cg))
            if nargout > 1
                [CD,CP] = computeTrimeshGeodists(obj.geodesiclib,...
                    cp,cg,obj.tri(c).f,obj.tri(c).v,...
                    cpf,cgf,cpt,cgt);
            else
                CD = computeTrimeshGeodists(obj.geodesiclib,...
                    cp,cg,obj.tri(c).f,obj.tri(c).v,...
                    cpf,cgf,cpt,cgt);
            end

            D(cpmask,cgmask) = CD;
            
            if nargout > 1
                P(cpmask,cgmask) = CP;
            end
        end
    end
end

function wpj = joinWalkablePoly(obj,c)
    if nargin == 2
        wpj = cell2mat(cellfun(@(y) [y,nan(2,1)],obj.walkablePoly{c},'UniformOutput',false));
    else
        wpj = cell2mat(cellfun(@(x) ...
            cell2mat(cellfun(@(y) [y,nan(2,1)],x,'UniformOutput',false)), ...
            obj.walkablePoly, 'UniformOutput',false));
        wpj(:,end) = [];
    end
end

function segs = walkablePolyLinesegs(obj,c)
    if nargin == 2
        wpoly = [obj.walkablePoly{c}];
    else
        wpoly = [obj.walkablePoly{:}];
    end
    segs = [];
    for i=1:numel(wpoly)
        segs = [segs,[wpoly{i};wpoly{i}(:,[2:end,1])]]; %#ok<AGROW>
    end
end

function addConstraints(obj,constraints,constrainttypes,constraintweights)
    obj.constraints = [obj.constraints,constraints];
    obj.constrainttypes = [obj.constrainttypes,constrainttypes];
    obj.constraintweights = [obj.constraintweights,constraintweights];
end

function removeConstraints(obj,cind)
    if max(cind) > numel(obj.constraints) || min(cind) < 1
        error('invalid constraint index');
    end
    
    obj.constraints(cind) = [];
    obj.constrainttypes(cind) = [];
    obj.constraintweights(cind) = [];
end

function [path,d] = computeShortestPath(obj,o1,o2)
    
    % compute starting point of path (outside last intersection point of
    % convex hull with ray from centroid in polygon angle direction)
    [~,~,convexhullextent] = pointsetBoundingbox(o1.convexhull);
    [x,y] = pol2cart(o1.angle,convexhullextent*2); 
    linestart = o1.center - [x;y];
    lineend = linestart + 2*[x;y]; % line in angle direction passing through the origin (start & end well outside the bounding box)
    [ix,iy,~,~] = polylinePolylineIntersection(...
        o1.convexhull(1,[1:end,1]),...
        o1.convexhull(2,[1:end,1]),...
        [linestart(1),lineend(1)],...
        [linestart(2),lineend(2)]);
    if isempty(ix)
        error('no intersection with convex hull of polygon, centroid must be outside the convex hull');
    end
%     isegind = floor(isegind);
    idists = sqrt((ix-linestart(1)).^2 + (iy-linestart(2)).^2);
    [~,maxind] = max(idists);
    startpoint = [ix(maxind);iy(maxind)] + ...
                 [x*0.000001;y*0.000001];
% 	starttan = -[x;y] ./ convexhullextent*2;
                          
    % compute ending point of path (outside last intersection point of
    % convex hull with ray from centroid in polygon angle direction)
    [~,~,convexhullextent] = pointsetBoundingbox(o2.convexhull);
    [x,y] = pol2cart(o2.angle,convexhullextent*2); 
    linestart = o2.center - [x;y];
    lineend = linestart + 2.*[x;y]; % line in angle direction passing through the origin (start & end well outside the bounding box)
    [ix,iy,~,~] = polylinePolylineIntersection(...
        o2.convexhull(1,[1:end,1]),...
        o2.convexhull(2,[1:end,1]),...
        [linestart(1),lineend(1)],...
        [linestart(2),lineend(2)]);
    if isempty(ix)
        error('no intersection with convex hull of polygon, centroid must be outside the convex hull');
    end
%     isegind = floor(isegind);
    idists = sqrt((ix-linestart(1)).^2 + (iy-linestart(2)).^2);
    [~,maxind] = max(idists);
    endpoint = [ix(maxind);iy(maxind)] + ...
               [x*0.000001;y*0.000001];
%     endtan = -[x;y] ./ convexhullextent*2;
    
    [d,path] = obj.computeWalkablePolyGeodists(startpoint,endpoint);
    path = path{1};
    if isempty(path)
        error('apparently no path was found between the two selected polygons.');
    end
end

% problem with concave: centroid needs to be inside polygon (line needs to
% hit something)
function path = computeSmoothPath(obj,o1,o2)
    
    [path,~] = obj.computeShortestPath(o1,o2);
    
    % offset path by a small amount so it does not touch the boundary
    wpolysegs = obj.walkablePolyLinesegs;
%     wpolyextent = sqrt(sum((max([obj.walkablePoly{:}],[],2) - ...
%                             min([obj.walkablePoly{:}],[],2)).^2,1));
    [~,~,wpolyextent] = pointsetBoundingbox([obj.walkablePoly{:}]);
    pathnormals = polylineNormals(path,false);
    [d,~] = pointPolylineDistance_mex(...
        path(1,:),path(2,:),...
        wpolysegs(1,:),wpolysegs(2,:),wpolysegs(3,:),wpolysegs(4,:));
    tooclosemask = d < wpolyextent * 0.000001;
    
    offset = pathnormals(:,tooclosemask) .* ...
        max(d([1,1],tooclosemask),wpolyextent * 0.000001) .* 2;
    
    % test if offset vectors point towards the boundary or away from it
    % and flip vectors that point towards it
    insidemask = false(1,size(offset,2));
    for i=1:numel(obj.tri)
        insidemask = insidemask | not(isnan(obj.tri(i).pointLocation((path(:,tooclosemask) + offset)')))';
    end
    offset(:,not(insidemask)) = -offset(:,not(insidemask));
    
    path(:,tooclosemask) = path(:,tooclosemask) + offset;
    
    % create a snake with the path
	membraneEnergy = 0;
    thinplateEnergy = 100; % make proportional to arclength
%     thinplateEnergy = 100000; % make proportional to arclength
    timestep = 1;
    balloonEnergy = 0;
    snakeres = wpolyextent*0.003;
    maxits = 400;
        
    [x,y] = pol2cart(o1.orientation,1);
    starttan = -[x;y];
    [x,y] = pol2cart(o2.orientation,1);
    endtan = -[x;y];

    path = Snake(...
        path, wpolysegs, ...
        path(:,[1,end]),[starttan,endtan],...
        snakeres, maxits,...
        membraneEnergy, thinplateEnergy, timestep, balloonEnergy,true);
end

end % methods

end % classdef
