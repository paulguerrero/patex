function gobjs = showSceneSegments(segments,gobjs,parent,varargin)
    
    if iscell(segments)
        verts = cell(1,numel(segments));
        endpoints = cell(1,numel(segments));    
        for i=1:numel(segments)
            verts{i} = {segments{i}.vertices};
            endpoints{i} = cell(1,numel(verts{i}));
            for j=1:numel(verts{i})
                if not(isempty(verts{i}{j}))
                    endpoints{i}{j} = verts{i}{j}(:,[1,end]);
                else
                    endpoints{i}{j} = [];
                end
            end
        end
    else
        verts = {segments.vertices};
        endpoints = cell(1,numel(verts));    
        for j=1:numel(verts)
            if not(isempty(verts{j}))
                endpoints{j} = verts(:,[1,end]);
            else
                endpoints{j} = [];
            end
        end
    end
    
    if size(gobjs,1) ~= 2
        delete(gobjs(isgraphics(gobjs)));
        gobjs = gobjects(2,numel(segments));
    end
    
    gobjs_polylines = showPolylines(verts,gobjs(1,:),parent,...
        varargin{:},...
        'markersymbol','none');
    
    
    % todo: endpoints (also need to copy parameters that are given per component or per vertex)
    % vfcolor and everything else not needed, only the markeredgecolor and
    % markerfacecolor
    
%     gobjs_endpoints = showPoints(endpoints,gobjs(2,:),parent,...
%         varargin{:},...
%         'facecolor','none',...
%         'edgecolor','none');

    gobjs_endpoints = gobjects(1,numel(gobjs_polylines));

    gobjs = [gobjs_polylines;gobjs_endpoints];
end
