classdef SceneComplexPolygon < ScenePointset2D
    
properties
    boundaries = ScenePolygon.empty(1,0); % first one is the outer boundary
end

properties(Dependent,SetAccess=protected)
    area;
    perimeter;
    
    corners;
    edges;
end

properties(Access={?ScenePolyline,?SceneImporter,?SceneExporter})
    p_area = [];
    p_perimeter = [];
    
    boundarylisteners = event.listener.empty(1,0);
end

methods(Static)

function n = type_s
    n = 'complexpolygon';
end

function d = dimension_s
    d = 1;
end

end

methods
% obj = SceneComplexPolygon()
% obj = SceneComplexPolygon(copyobj)
% obj = SceneComplexPolygon(boundarypolys)
% obj = SceneComplexPolygon(boundaryverts)
% obj = SceneComplexPolygon(boundarypolys, labels)
% obj = SceneComplexPolygon(boundaryverts, labels)
function obj = SceneComplexPolygon(varargin)
    obj@ScenePointset2D;
    
    if numel(varargin) == 0
        % do nothing (default values)
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneComplexPolygon')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 1 && isa(varargin{1},'ScenePolygon')
        obj.setBoundaries(varargin{1});
        
        obj.definePosition(obj.center);
        obj.defineOrientation(obj.pcorientation);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
    
    elseif numel(varargin) == 1 && iscell(varargin{1})
        boundaries = ScenePolygon.empty(1,0);
        for i=1:numel(varargin{1})
            boundaries(i) = ScenePolygon(varargin{1}{i},{});
        end
        
        obj.setBoundaries(boundaries);
        delete(boundaries(isvalid(boundaries)));
        
        obj.definePosition(obj.center);
        obj.defineOrientation(obj.pcorientation);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
        
    elseif numel(varargin) == 2 && isa(varargin{1},'ScenePolygon')
        obj.setBoundaries(varargin{1});
        obj.setLabels(varargin{2});
        
        obj.definePosition(obj.center);
        obj.defineOrientation(obj.pcorientation);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
    elseif numel(varargin) == 2 && iscell(varargin{1})
        boundaries = ScenePolygon.empty(1,0);
        for i=1:numel(varargin{1})
            boundaries(i) = ScenePolygon(varargin{1}{i},varargin{2});
        end
        
        obj.setBoundaries(boundaries);
        delete(boundaries(isvalid(boundaries)));
        obj.setLabels(varargin{2});
        
        obj.definePosition(obj.center);
        obj.defineOrientation(obj.pcorientation);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
    else
        error('cannot construct SceneComplexPolygon with these parameters')
    end
end

function copyFrom(obj,obj2)
    obj.setBoundaries(obj2.boundaries);
    
    obj.p_area = obj2.p_area;
    obj.p_perimeter = obj.p_perimeter;
    
    % boundary listeners are updated when setting the boundaries
end

function obj2 = clone(obj)
    obj2 = SceneComplexPolygon(obj);
end

function setVerts(obj,verts)
    if iscell(verts)
        
        for i=1:min(numel(verts),numel(obj.boundaries))
            obj.boundaries(i).setVerts(verts{i});
        end
        
        if numel(verts) < numel(obj.boundaries)
            % remove boundaries if necessary
            
            delinds = numel(verts)+1:numel(obj.boundaries);
            delete(obj.boundaries(delinds(isvalid(obj.boundaries(delinds)))));
            obj.boundaries(delinds) = [];
        elseif numel(verts) > numel(obj.boundaries)
            % add boundaries if necessary
            
            newinds = numel(obj.boundaries)+1:numel(verts);
            
            for i=newinds
                obj.boundaries(end+1) = ScenePolygon(verts{i});
            end
            
            obj.boundaries(newinds).setScenegroup(obj.scenegroup);
            obj.boundaries(newinds).setLabels(obj.labels);
        end
        
        obj.dirty;
    else
        obj.setVerts@ScenePointset2D;
    end
end

function setBoundaries(obj,boundaries)
%     if not(iscell(boundaries))
%         boundaries = {boundaries};
%     end
    
    if numel(boundaries) < 1
        error('Must pass at least one boundary.');
    end
    
    if numel(boundaries) > 1
        a = polysetHierarchy({boundaries.verts});

        if not(all(a(2:end,1))) || any(any(a(:,2:end))) || a(1,1)
            error('Not all inner boundaries are directly inside the outer boundary.');
        end
    end
    
    delete(obj.boundaries(isvalid(obj.boundaries)));
    delete(obj.boundarylisteners(isvalid(obj.boundarylisteners)));
    
%     % check that all inner boundaries are completely inside the outer
%     % boundary
%     outerv = boundaries(1).verts;
%     for i=2:numel(boundaries)
%         innerv = boundaries(i).verts;
%         crel = polygonContainmentRelation(...
%             outerv(1,:),outerv(2,:),...
%             innerv(1,:),innerv(2,:));
%         if crel ~= 2
%             error('An inner boundary is not completely inside the outer boundary.');
%         end
%     end
    
    obj.boundaries = obj.copyFeatsarrayFrom(obj.boundaries,boundaries);
    
    for i=1:numel(boundaries)
        obj.boundarylisteners(end+1) = obj.boundaries(i).addlistener('p_verts','PostSet',@(src,evt) obj.boundariesChanged);
    end
    
    obj.boundaries.setScenegroup(obj.scenegroup);
    obj.boundaries.setLabels(obj.labels);
    
    obj.dirty;
end

function boundariesChanged(obj,src,evt) %#ok<INUSD>
    obj.dirty;
    obj.p_verts = [];
end

function val = get.corners(obj)
    val = {obj.boundaries.corners};
end
function val = getCorners(obj)
    allboundaries = [obj.boundaries];
    val = allboundaries.getCorners;
end
function b = cornersComputed(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = all(obj(i).boundaries.cornersComputed);
    end
end
function b = cornersDirty(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = any(obj(i).boundaries.cornersDirty);
    end
end
function updateCorners(obj)
    for i=1:numel(obj)
        obj.boundaries.updateCorners;
    end
end

function val = get.edges(obj)
    val = {obj.boundaries.edges};
end
function val = getEdges(obj)
    allboundaries = [obj.boundaries];
    val = allboundaries.getEdges;
end
function b = edgesComputed(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = all(obj(i).boundaries.edgesComputed);
    end
end
function b = edgesDirty(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = any(obj(i).boundaries.edgesDirty);
    end
end
function updateEdges(obj)
    for i=1:numel(obj)
        obj.boundaries.updateEdges;
    end
end

function val = get.area(obj)
    if isempty(obj.p_area)
        obj.updateArea;
    end
    val = obj.p_area;
end
function val = getArea(obj)
    mask = not(obj.hasArea);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updateArea;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updateArea;
                end
            end
        end
    end
    val = [obj.p_area];
end
function updateArea(obj)
    for k=1:numel(obj)
        o = obj(k);
        
        areas = [obj.boundaries.area];
        o.p_area = areas(1) - sum(areas(2:end));
    end
end
function b = hasArea(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_area));
    end
end

function val = get.perimeter(obj)
    if isempty(obj.p_perimeter)
        obj.updatePerimeter;
    end
    val = obj.p_perimeter;
end
function val = getPerimeter(obj)
    mask = not(obj.hasPerimeter);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updatePerimeter;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updatePerimeter;
                end
            end
        end
    end
    val = [obj.p_perimeter];
end
function updatePerimeter(obj)
    for k=1:numel(obj)
        o = obj(k);

        o.p_perimeter = sum([obj.boundaries.perimeter]);
    end
end
function b = hasPerimeter(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_perimeter));
    end
end
    
function updateCenter(obj)
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_center = obj.boundaries(1).center; % use center of outer boundary
    end
end

end

methods(Access=protected)
    
function dirtyImpl(obj)
    obj.dirtyImpl@ScenePointset2D;
    
    for k=1:numel(obj)
        o = obj(k);

        o.p_area = [];
        o.p_perimeter = [];
    end
end

function updateLocalImpl(obj)

    obj.updateLocalImpl@ScenePointset2D;
    
    mask = not(obj.hasArea);
    if any(mask)
        obj(mask).updateArea;
    end
    mask = not(obj.hasPerimeter);
    if any(mask)
        obj(mask).updatePerimeter;
    end
    mask = not(obj.hasRoundness);
    if any(mask)
        obj(mask).updateRoundness;
    end
end

function feats = subfeaturesImpl(obj)
    mask = not(obj.hasSubfeatures);
    if any(mask)
        obj(mask).updateSubfeatures;
    end
    
    feats = cell(1,numel(obj));
    for k=1:numel(obj)
        feats{k} = [obj(k).boundaries.corners,obj(k).boundaries.edges,obj(k).boundaries.segments];
    end
end
function b = hasSubfeaturesImpl(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        o = obj(i); 
        b(i) = not(any(o.boundaries.cornersDirty) | any(o.boundaries.edgesDirty));
    end
end
function updateSubfeaturesImpl(obj)
    allpolys = [obj.polys];
    allpolys.updateCorners;
    allpolys.updateEdges;
end

% function setScenegroupImpl(obj,sg)
%     obj.setScenegroupImpl@ScenePointset2D(sg);
%     
%     obj.boundaries.setScenegroup(sg);
% end

% relations 0 and 2 are never returned, because only relevant poly indices
% (i.e. feature is inside poly or intersecting) are returned
% relation: 0: feature and poly are separate
% relation: 1: feature completely inside poly
% relation: 2: poly completely inside feature
% relation: 3: feature and poly are intersecting
function [polyinds,relation] = inPolygonImpl(obj,poly,polycandidates)

    error('Not implemented yet.');
    
%     polyinds = cell(numel(obj),1);
%     if nargout >= 2
%         relation = cell(numel(obj),1);
%     end
%     if not(isempty(poly)) && not(isempty(obj))
%         [polybbmin,polybbmax] = poly.boundingbox;
%         [mybbmin,mybbmax] = obj.boundingbox;
% 
%         for j=1:numel(obj)
%             
%             % find polygons with bounding boxes containing the polygon set
%             % bounding box
%             if nargin < 3 || isempty(polycandidates)
%                 polyinds{j} = find(...
%                     polybbmin(1,:) < mybbmax(1,j) & polybbmin(2,:) < mybbmax(2,j) & ...
%                     polybbmax(1,:) > mybbmin(1,j) & polybbmax(2,:) > mybbmin(2,j));
%             else
%                 polyinds{j} = polycandidates{j}(...
%                     polybbmin(1,polycandidates{j}) < mybbmax(1,j) & polybbmin(2,polycandidates{j}) < mybbmax(2,j) & ...
%                     polybbmax(1,polycandidates{j}) > mybbmin(1,j) & polybbmax(2,polycandidates{j}) > mybbmin(2,j));
%             end
%             
%             % find all polygons containing at least part of the point set
%             [polyinds] = obj.boundaries.inPolygonImpl
%             p = obj(j).verts(:,1);
%             keepmask = false(1,numel(polyinds{j}));
%             if nargout >= 2
%                 relation{j} = zeros(1,numel(polyinds{j}));
%             end
%             for i=1:numel(polyinds{j})
%                 
%                 pointinpoly = pointInPolygon_mex(p(1),p(2),poly(polyinds{j}(i)).verts(1,:),poly(polyinds{j}(i)).verts(2,:));
% 
%                 intersecting = any(pointinpoly);
%                 contained = all(pointinpoly);
%                 
%                 keepmask(i) = intersecting | contained;
%                 
%                 if nargout >= 2
%                     if contained
%                         relation{j}(i) = 1;
%                     elseif intersecting
%                         relation{j}(i) = 3;
%                     end
%                 end
%             end
%             polyinds{j} = polyinds{j}(keepmask);
%             if nargout >= 2
%                 relation{j} = relation{j}(keepmask);
%             end
%         end
%     end
end
    
function [d,cp] = pointDistanceImpl(obj,p,pose)
    
    d = inf(numel(obj),size(p,2));
    cp = zeros(numel(obj),size(p,2),size(p,1));
    for i=1:numel(obj)
        o = obj(i);
        
        if size(p,1) == 2
            if nargin >= 3 && not(isempty(pose)) && not(all(pose{i}==obj(i).pose2D))
                opose = repmat(o.pose2D,1,size(pose,2));
                opolyposes = [o.boundaries.pose2D];
            else
                opose = [];
            end
        elseif size(p,1) == 3
            if nargin >= 3 && not(isempty(pose)) && not(all(pose{i}==obj(i).pose3D))
                opose = repmat(o.pose3D,1,size(pose,2));
                opolyposes = [o.boundaries.pose3D];
            else
                opose = [];
            end
        else
            error('Only implemented for two or three dimensions.');
        end
        
        od = zeros(numel(o.boundaries),size(p,2));
        ocp = zeros(numel(o.boundaries),size(p,2),size(p,1));
        for j=1:numel(o.boundaries)
            if not(isempty(opose))
                polyposearg = {posetransform(opolyposes(:,j),pose,opose)};
            else
                polyposearg = cell(1,0);
            end

            [od(j,:),ocp(j,:,:)] = o.boundaries(j).pointDistanceImpl(p,polyposearg{:});
        end
        
        [d(i,:),mininds] = min(od,[],1);
        linind = sub2ind(size(ocp),mininds,1:numel(mininds),1);
        cp(i,:,1) = ocp(linind);
        linind = sub2ind(size(ocp),mininds,1:numel(mininds),2);
        cp(i,:,2) = ocp(linind);
        if size(p,1) == 3
            linind = sub2ind(size(ocp),mininds,1:numel(mininds),3);
            cp(i,:,3) = ocp(linind);
        end
    end
end

function [d,t,cp] = linesegDistanceImpl(obj,p1,p2,pose) % distance to a 3D line segment

    d = inf(numel(obj),size(p1,2));
    t = nan(numel(obj),size(p1,2));
    cp = zeros(numel(obj),size(p1,2),size(p1,1));
    for i=1:numel(obj)
        o = obj(i);
        
        od = zeros(numel(o.boundaries),size(p1,2));
        ot = nan(numel(o.boundaries),size(p1,2));
        ocp = zeros(numel(o.boundaries),size(p1,2),size(p1,1));
        oboundaryposes = [o.boundaries.pose3D];
        for j=1:numel(o.boundaries)
            
            if nargin >= 4 && not(isempty(pose)) && not(all(pose{i}==obj(i).pose3D))
                boundarypose = posetransform(oboundaryposes(:,j),pose{i},o.pose3D);
            else
                boundarypose = posetransform(oboundaryposes(:,j),o.scenegroup.globalpose(o.pose3D),o.pose3D);
            end

            [od(j,:),ot(j,:),ocp(j,:,:)] = o.boundaries(j).linesegDistanceImpl(p1,p2,{boundarypose});
        end
        
        [d(i,:),mininds] = min(od,[],1);
        linind = sub2ind(size(ot),mininds,1:numel(mininds));
        t(i,:) = ot(linind);
        linind = sub2ind(size(ocp),mininds,1:numel(mininds),1);
        cp(i,:,1) = ocp(linind);
        linind = sub2ind(size(ocp),mininds,1:numel(mininds),2);
        cp(i,:,2) = ocp(linind);
        if size(p1,1) == 3
            linind = sub2ind(size(ocp),mininds,1:numel(mininds),3);
            cp(i,:,3) = ocp(linind);
        end
    end
end
    
function moveAboutOriginImpl(obj,movevec)
    % do not just call method in ScenePointset2D (which would also work)
    % for performance reasons (re-setting verts in each boundary is more
    % expensive than just moving them)
    
    for i=1:numel(obj.boundaries)
        obj.boundaries(i).move(movevec,'relative');
    end
    
    if obj.hasCenter
        obj.p_center = obj.p_center + movevec;
    end
    if obj.hasOrientedbbox
        obj.p_orientedbbox(:,1) = obj.p_orientedbbox(:,1) + movevec;
    end
    if obj.hasConvexhull
        obj.p_convexhull = [obj.p_convexhull(1,:)+movevec(1);obj.p_convexhull(2,:)+movevec(2)];
    end
    if obj.hasPrincipalbbox
        obj.p_principalbbox(:,1) = obj.p_principalbbox(:,1) + movevec;
    end
end

function rotateAboutOriginImpl(obj,rotangle)
    for i=1:numel(obj.boundaries)
        obj.boundaries(i).rotate(rotangle,'relative',[0;0]);
    end
    
    rotmat = [cos(rotangle),-sin(rotangle);...
              sin(rotangle),cos(rotangle)];

    if obj.hasCenter
        obj.p_center = rotmat * obj.p_center;
    end
    if obj.hasPcorientation
        obj.p_pcorientation = obj.p_pcorientation + rotangle;
    end
    obj.p_orientedbbox = [];
    if obj.hasConvexhull
        obj.p_convexhull = rotmat*obj.p_convexhull;
    end
    obj.p_principalcomps = [];
    obj.p_principalbbox = [];
end

function scaleAboutOriginImpl(obj,relscale)
    
    for i=1:numel(obj.boundaries)
        obj.boundaries(i).scale(relscale,'relative',[0;0],0);
    end
    
    if obj.hasCenter
        obj.p_center = obj.p_center .* relscale;
    end
    if obj.hasExtent
        if any(relscale ~= relscale(1))
            % non-uniform scaling, need to recompute extent
            obj.p_extent = [];
        else
            obj.p_extent = obj.p_extent .* relscale(1);
        end
    end
    if obj.hasRadius
        if any(relscale ~= relscale(1))
            % non-uniform scaling, need to recompute radius
            obj.p_radius = [];
        else
            obj.p_radius = obj.p_radius .* relscale(1);
        end
    end
    obj.p_orientedbbox = [];
    if obj.hasConvexhull
        obj.p_convexhull = bsxfun(@times,obj.p_convexhull,relscale);
    end
    if any(relscale ~= relscale(1))
        obj.p_principalcomps = [];
    end
    obj.p_principalbbox = [];
    
    
    if not(isempty(obj.p_area))
        obj.p_area = obj.p_area * prod(relscale);
    end
    
    obj.p_perimeter = [];
end

% when mirroring a polygon, the winding order needs to be mantained and as
% a result the ordering of the vertices is reversed
function mirrorAboutOriginImpl(obj,mirchange)
    
    if not(mirchange)
        return;
    end
    
    for i=1:numel(obj.boundaries)
        obj.boundaries(i).mirror(mirchange,'relative',[0;0],0);
    end
        
    obj.p_orientedbbox = []; 
    if obj.hasConvexhull
        obj.p_convexhull = [obj.p_convexhull(1,:);-obj.p_convexhull(2,:)]; 
        obj.p_convexhull = obj.p_convexhull(:,[1,end:-1:2]);
    end
    obj.p_principalcomps = [];
    obj.p_principalbbox = [];
end

function s = similarityImpl(obj,obj2)
    s = ones(numel(obj),numel(obj2)); % all polygon sets have similarity 1 to each other for now
end

function v = getVertsImpl(obj)
    v = [obj.boundaries.verts];
end

function setVertsImpl(obj,v)
    voffset = 0;
    for i=1:numel(obj.boundaries)
        if voffset >= size(v,2)
            delinds = i:numel(obj.boundaries);
            delete(obj.boundaries(delinds(isvalid(obj.boundaries(delinds)))));
            obj.boundaries(delinds) = [];
            break;
        end
        
        npolyverts = size(obj.boundaries(i).verts,2);
        obj.boundaries(i).verts = v(:,...
            voffset+1 : ...
            min(end,voffset + npolyverts));
        
        voffset = voffset + npolyverts;
    end
    
    if voffset < size(v,2)
        obj.boundaries(end+1) = ScenePolygon(v(:,voffset+1:size(v,2)),obj.labels);
    end
end

end

end
