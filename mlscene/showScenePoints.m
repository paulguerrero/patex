function gobjs = showScenePoints(pts,gobjs,parent,varargin)
%     if not(iscell(pts))
%         pts = num2cell(pts);
%     end
    
    if iscell(pts)
        for j=1:numel(pts)
            pts{j} = pts{j}.getPosition;
        end
    else
        pts = pts.getPosition;
    end
    
    gobjs = showPoints(pts,gobjs,parent,varargin{:});
end
