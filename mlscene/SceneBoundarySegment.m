classdef SceneBoundarySegment < SceneElementFeature

properties(SetAccess=protected)
    startarclen = 0;
    endarclen = 1;
end

properties(Dependent, SetAccess=protected)
    vertices;
    vertexIndices;
    arclen;
    anglesum;
    curvature;
    startpos;
    endpos;
end

properties(Access={?SceneBoundarySegment,?SceneImporter,?SceneExporter})
    
    p_vertices = [];
    p_vertexIndices = [];
    p_arclen = [];
    p_anglesum = [];
    p_curvature = [];
    p_startpos = [];
    p_endpos = [];
end

methods(Static)

function n = type_s
    n = 'segment';
end

function d = dimension_s
    d = 1;
end

function val = snapdist
    val = 0.005;
end

end

methods
    
% SceneBoundarySegment - empty constructor
% SceneBoundarySegment(segment2) - copy constructor
% SceneBoundarySegment(parentelement,startarclen,endarclen,labels) - standard constructor
function obj = SceneBoundarySegment(varargin)

    if numel(varargin) == 0
        superargs = {};
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneBoundarySegment')
        superargs = {};
    elseif numel(varargin) == 3 || numel(varargin) == 4
        superargs = varargin(1);
        varargin(1) = [];
        
        if numel(varargin) >= 3
            superargs(end+1) = varargin(3);
            varargin(3) = [];
        end
    else
        error('Invalid arguments.');
    end
    
    obj@SceneElementFeature(superargs{:});
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneBoundarySegment')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 2
        obj.setStartendarclen(varargin{1},varargin{2});
    else
        error('Invalid number of arguments.');
    end
end

function copyFrom(obj,obj2)
    obj.copyFrom@SceneElementFeature(obj2);
    
%     obj.parentelement = obj2.parentelement;
    obj.startarclen = obj2.startarclen;
    obj.endarclen = obj2.endarclen;
    
    obj.p_vertices = obj2.p_vertices;
    obj.p_vertexIndices = obj2.p_vertexIndices;
    obj.p_arclen = obj2.p_arclen;
    obj.p_anglesum = obj2.p_anglesum;
    obj.p_curvature = obj2.p_curvature;
    obj.p_startpos = obj2.p_startpos;
    obj.p_endpos = obj2.p_endpos;
end

function obj2 = clone(obj)
    obj2 = SceneBoundarySegment(obj);
end
    
function setStartendarclen(obj,startalen,endalen)
    if any([startalen,endalen] < 0) || ...
       any([startalen,endalen] > 1) 
        error('Segment start- or end-arclength not in [0,1].');
    elseif not(isempty(obj.parentelement)) && endalen < startalen && not(obj.parentelement.closed)
        error('Segment passes the end of an open polyline.')
    end
    
    obj.startarclen = startalen;
    obj.endarclen = endalen;
    
    obj.dirty;
end

function val = get.vertices(obj)
	if isempty(obj.p_vertices)
        obj.updateVertices;
	end
    val = obj.p_vertices;
end
function updateVertices(obj)
    mask = not(obj.hasStartendpos);
    if any(mask)
        obj(mask).updateStartendpos;
    end
    mask = not(obj.hasVertexIndices);
    if any(mask)
        obj(mask).updateVertexIndices;
    end
    
    for k=1:numel(obj)
        o = obj(k);

        if not(isempty(o.parentelement))
            alen = polylineArclen(o.parentelement.verts,o.parentelement.closed);
            alen = alen ./ alen(end);

            startvert = [];
            if not(any(abs(alen - o.startarclen) < o.snapdist))
                startvert = o.startpos;
            end
            endvert = [];
            if not(any(abs(alen - o.endarclen) < o.snapdist))
                endvert = o.endpos;
            end
            
            o.p_vertices = [startvert,o.parentelement.verts(:,o.vertexIndices),endvert];
        end
    end
end
function b = hasVertices(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_vertices));
    end
end

% function b = startsAtVertex(obj)
%     arclen = polylineArclen(obj.parentelement.verts,obj.parentelement.closed);
%     arclen = arclen ./ arclen(end);
%     
%     if obj.parentelement.closed
%         dist = mod(arclen - obj.startarclen,1);
%         mask = dist > 0.5;
%         dist(mask) = 1-dist(mask);
%         b = any(dist < obj.snapdist);
%     else
%         b = any(abs(arclen - obj.startarclen) < obj.snapdist);
%     end
% end
% 
% function b = endsAtVertex(obj)
%     arclen = polylineArclen(obj.parentelement.verts,obj.parentelement.closed);
%     arclen = arclen ./ arclen(end);
%     
%     if obj.parentelement.closed
%         dist = mod(arclen - obj.endarclen,1);
%         mask = dist > 0.5;
%         dist(mask) = 1-dist(mask);
%         b = any(dist < obj.snapdist);
%     else
%         b = any(abs(arclen - obj.endarclen) < obj.snapdist);
%     end
% end

function val = get.startpos(obj)
    if isempty(obj.p_startpos)
        obj.updateStartendpos;
    end
    val = obj.p_startpos;
end
function val = get.endpos(obj)
    if isempty(obj.p_endpos)
        obj.updateStartendpos;
    end
    val = obj.p_endpos;
end
function updateStartendpos(obj)
    
    for k=1:numel(obj)
        o = obj(k);

        if not(isempty(o.parentelement))
            alen = polylineArclen(o.parentelement.verts,o.parentelement.closed);
            alen = alen ./ alen(end);

            if o.parentelement.closed
                o.p_startpos = interp1(alen',o.parentelement.verts(:,[1:end,1])',o.startarclen)';
                o.p_endpos = interp1(alen',o.parentelement.verts(:,[1:end,1])',o.endarclen)';
            else
                o.p_startpos = interp1(alen',o.parentelement.verts',o.startarclen)';
                o.p_endpos = interp1(alen',o.parentelement.verts',o.endarclen)';
            end
        end
    end
end
function b = hasStartendpos(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_startpos)) && not(isempty(obj(k).p_endpos));
    end
end

function val = get.vertexIndices(obj)
    if isempty(obj.p_vertexIndices)
        obj.updateVertexIndices;
    end
    
    val = obj.p_vertexIndices;
end
function updateVertexIndices(obj)
    
    for k=1:numel(obj)
        o = obj(k);

        if not(isempty(o.parentelement))
    
            if not(o.parentelement.closed) && o.startarclen >= o.endarclen
                error('Invalid arc lengths for closed polyline.');
            end
            
            alen = polylineArclen(o.parentelement.verts,o.parentelement.closed);
            alen = alen ./ alen(end);
            if o.parentelement.closed
                alen = alen(1:end-1);
            end

            if o.parentelement.closed
                % add padding equal to the snap distance to start and end, but only
                % if there is enought space between end --> start, otherwise add
                % just enought padding to close the gap between end and start
                uncovered = mod(o.startarclen-o.endarclen,1);
                if uncovered/2 <= o.snapdist
                    sarclen = mod(o.endarclen+uncovered/2,1);
                    relendarclen = 1;
                else
                    sarclen = mod(o.startarclen-o.snapdist,1);
                    relendarclen = mod(mod(o.endarclen+o.snapdist,1)-sarclen,1);
                end
                relarclen = mod(alen-sarclen,1);
                [~,o.p_vertexIndices] = sort(relarclen,'ascend');
                o.p_vertexIndices(relarclen(o.p_vertexIndices) > relendarclen) = [];
            else
                o.p_vertexIndices = find(alen >= o.startarclen - o.snapdist & ...
                                           alen <= o.endarclen + o.snapdist);
            end
    
%             if o.endarclen > o.startarclen
%                 val = find(arclen >= o.startarclen - o.snapdist & ...
%                            arclen <= o.endarclen + o.snapdist);
%             else
%                 val = find(arclen >= o.startarclen - o.snapdist | ...
%                            arclen <= o.endarclen + o.snapdist);    
%             end
% 
%             if o.endvert > o.startvert
%                 val = o.startvert:o.endvert;
%             else
%                 val = [o.startvert:size(o.parentelement.verts,2),1:o.endvert];
%             end
        end
    end
end
function b = hasVertexIndices(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_vertexIndices));
    end
end
    
function val = get.arclen(obj)
    if isempty(obj.p_arclen)
        obj.updateArclen;
    end
    
    val = obj.p_arclen;
end
function updateArclen(obj)
    mask = not(obj.hasVertices);
    if any(mask)
        obj(mask).updateVertices;
    end

    for k=1:numel(obj)
        o = obj(k);
        
        o.p_arclen = polylineArclen(o.vertices,false);
    end
end
function b = hasArclen(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_arclen));
    end
end

function val = get.anglesum(obj)
    if isempty(obj.p_anglesum)
        obj.updateAnglesum;
    end
    
    val = obj.p_anglesum;
end
function updateAnglesum(obj)
    mask = not(obj.hasVertices);
    if any(mask)
        obj(mask).updateVertices;
    end
    
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_anglesum = sum(abs(polylineAngles(o.vertices,false)));
    end
end
function b = hasAnglesum(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_anglesum));
    end
end

function val = get.curvature(obj)
    if isempty(obj.p_curvature)
        obj.updateCurvature;
    end
    
    val = obj.p_curvature;
end
function updateCurvature(obj)
    mask = not(obj.hasVertices);
    if any(mask)
        obj(mask).updateVertices;
    end
    
    for k=1:numel(obj)
        o = obj(k);
    
        [o.p_curvature,~,~,~] = polylineDiffProperties(o.vertices,false);
    end
end
function b = hasCurvature(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_curvature));
    end
end

end

methods(Access=protected)
    
function dirtyImpl(obj)
    obj.dirtyImpl@SceneElementFeature;
    
    for k=1:numel(obj)
        o = obj(k);

        o.p_vertices = [];
        o.p_vertexIndices = [];
        o.p_arclen = [];
        o.p_anglesum = [];
        o.p_curvature = [];
        o.p_startpos = [];
        o.p_endpos = [];
    end
end

function updateLocalImpl(obj)
    obj.updateLocalImpl@SceneElementFeature;
    
    mask = not(obj.hasVertices);
    if any(mask)
        obj(mask).updateVertices;
    end
    mask = not(obj.hasVertexIndices);
    if any(mask)
        obj(mask).updateVertexIndices;
    end
    mask = not(obj.hasArclen);
    if any(mask)
        obj(mask).updateArclen;
    end
    mask = not(obj.hasAnglesum);
    if any(mask)
        obj(mask).updateAnglesum;
    end
    mask = not(obj.hasCurvature);
    if any(mask)
        obj(mask).updateCurvature;
    end
    mask = not(obj.hasStartendpos);
    if any(mask)
        obj(mask).updateStartendpos;
    end
end
    
function updateBoundingboxImpl(obj)
    for i=1:numel(obj)
        o = obj(i); % for performance
        v = o.vertices;
        o.p_boundingboxmin = min(v,[],2);
        o.p_boundingboxmax = max(v,[],2);
    end
end

% % pose must be 2D
% function [center,width,height,orientation] = posedBoundingbox2DImpl(obj,framepose,featpose)
%     
%     center = zeros(2,numel(obj));
%     width = zeros(1,numel(obj));
%     height = zeros(1,numel(obj));
%     orientation = zeros(1,numel(obj));
%     for i=1:numel(obj)
%         v = posetransform(obj(i).vertices,SceneElement.pose3Dto2D(featpose(:,i)),obj(i).pose2D);
%         v = posetransform(v,identpose2D,framepose(:,i));
% 
%         bbmin = min(v,[],2);
%         bbmax = max(v,[],2);
%         center(:,i) = (bbmin + bbmax) .* 0.5;
%         width(i) = bbmax(1) - bbmin(1);
%         height(i) = bbmax(2) - bbmin(2);
%         
%         center(:,i) = posetransform(center(:,i),framepose(:,i),identpose2D);
%         
%         orientation(i) = framepose(3,i);
%     end
% end

function [d,cp] = pointDistanceImpl(obj,p,pose)
    d = inf(numel(obj),size(p,2));
    
    if size(p,1) == 2
        cp = zeros(numel(obj),size(p,2),2);
        for i=1:numel(obj)
            verts = obj(i).vertices;
            if nargin >= 3 && not(isempty(pose)) && not(all(pose{i} == obj(i).parentelement.pose))
                verts = posetransform(verts,pose{i},obj(i).parentelement.pose);
            end
            
            [d(i,:),~,~,cp(i,:,1),cp(i,:,2)] = pointPolylineDistance_mex(p(1,:),p(2,:),...
                verts(1,:),verts(2,:));
        end
    elseif size(p,1) == 3
        cp = zeros(numel(obj),size(p,2),3);
        for i=1:numel(obj)
            % assume z-coordinates of segment are zero
            verts = [obj(i).vertices;zeros(1,size(obj(i).vertices,2))];
            if nargin >= 3 && not(isempty(pose))
                if not(all(pose{i} == obj(i).parentelement.pose3D))
                    verts = posetransform(verts,pose{i},obj(i).parentelement.pose3D);
                end
            else
                if not(isempty(obj.scenegroup))
                    grouppose = obj.scenegroup.globaloriginpose;
                    verts = transform3D(verts,grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
                end
            end
            
            lsegstart = verts(:,1:end-1);
            lsegend = verts(:,2:end);
            [dists,t] = pointLinesegDistance3D_mex(p,...
                lsegstart,lsegend);
            
            [d(i,:),mininds] = min(dists,[],2);
            mininds = mininds';
            
            % get closest points
%             t = t(:,sub2ind(size(t),1:size(t,1),mininds));
            t = t(sub2ind(size(t),1:size(t,1),mininds));
            cp(i,:,:) = permute(lsegstart(:,mininds) + ...
                bsxfun(@times,lsegend(:,mininds)-lsegstart(:,mininds),t),[3,2,1]);
        end
    else
        error('Only implemented for two or three dimensions.');
    end
end

function [d,t,cp] = linesegDistanceImpl(obj,p1,p2,pose) % distance to a 3D line segment
    if size(p1,1) == 2
        error('Not yet implemented.');
        % todo: implement a function like linesegLinesegDistance_mex (2D)
    elseif  size(p1,1) == 3
        d = inf(numel(obj),size(p1,2));
        t = nan(numel(obj),size(p1,2));
        cp = zeros(numel(obj),size(p1,2),3);
        for i=1:numel(obj)
            verts = [obj(i).vertices;zeros(1,size(obj(i).vertices,2))];
            if nargin >= 4 && not(isempty(pose))
                if not(all(pose{i} == obj(i).parentelement.pose3D))
                    verts = posetransform(verts,pose{i},obj(i).parentelement.pose3D);
                end
            else
                if not(isempty(obj.scenegroup))
                    grouppose = obj.scenegroup.globaloriginpose;
                    verts = transform3D(verts,grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
                end
            end
            
            lsegstart = verts(:,1:end-1);
            lsegend = verts(:,2:end);
            [dists,t1,t2] = linesegLinesegDistance3D_mex(p1,p2,...
                lsegstart,lsegend);
            
%             [d(i,:),mininds] = min(dists,[],2);
%             t(i,:) = t1(sub2ind(size(t1),1:size(t1,1),mininds));
            
            mindist = min(dists,[],2);
            t1candidates = t1;
            t1candidates(not(bsxfun(@eq,dists,mindist))) = nan;
            [t(i,:),mininds] = min(t1candidates,[],2);
            mininds = mininds';

            mininds_lin = sub2ind(size(t1),1:size(t1,1),mininds);

            d(i,:) = dists(mininds_lin);
            
            % get closest points
%             t2 = t2(sub2ind(size(t2),1:size(t2,1),mininds));
            t2 = t2(mininds_lin);
            if not(isrow(t2))
                t2 = t2';
            end
            cp(i,:,:) = permute(lsegstart(:,mininds) + ...
                bsxfun(@times,lsegend(:,mininds)-lsegstart(:,mininds),t2),[3,2,1]);
        end
    else
        error('Only implemented for two or three dimensions.');
    end
end

% relations 0 and 2 are never returned, because only relevant poly indices
% (i.e. feature is inside poly or intersecting) are returned
% relation: 0: feature and poly are separate
% relation: 1: feature completely inside poly
% relation: 2: poly completely inside feature
% relation: 3: feature and poly are intersecting
function [polyinds,relation] = inPolygonImpl(obj,poly,polycandidates)

    %     b = zeros(numel(obj),numel(poly));
    polyinds = cell(numel(obj),1);
    if nargout >= 2
        relation = cell(numel(obj),1);
    end
    if not(isempty(poly)) && not(isempty(obj))
        
        [polybbmin,polybbmax] = poly.boundingbox;
        [mybbmin,mybbmax] = obj.boundingbox;

%         b = double(bsxfun(@lt,polybbmin(1,:),mybbmax(1,:)') & bsxfun(@lt,polybbmin(2,:),mybbmax(2,:)') & ...
%                    bsxfun(@gt,polybbmax(1,:),mybbmin(1,:)') & bsxfun(@gt,polybbmax(2,:),mybbmin(2,:)'));

        for j=1:numel(obj)
            % find polygons with bounding boxes containing the segment
            if nargin < 3 || isempty(polycandidates)
                polyinds{j} = find(...
                    polybbmin(1,:) < mybbmax(1,j) & polybbmin(2,:) < mybbmax(2,j) & ...
                    polybbmax(1,:) > mybbmin(1,j) & polybbmax(2,:) > mybbmin(2,j));
            else
                polyinds{j} = polycandidates{j}(...
                    polybbmin(1,polycandidates{j}) < mybbmax(1,j) & polybbmin(2,polycandidates{j}) < mybbmax(2,j) & ...
                    polybbmax(1,polycandidates{j}) > mybbmin(1,j) & polybbmax(2,polycandidates{j}) > mybbmin(2,j));
            end
            
            % find all polygons containing at least part of the segment
            p = obj(j).vertices(:,1);
            pts = obj(j).vertices;
            keepmask = false(1,numel(polyinds{j}));
            if nargout >= 2
                relation{j} = zeros(1,numel(polyinds{j}));
            end
            for i=1:numel(polyinds{j})
                % test if part of the segment is inside the polygon
%                 intersects = linesegLinesegIntersect(...
%                     [pts(:,1:end-1);pts(:,2:end)]',...
%                     [poly(polyinds{j}(i)).verts;...
%                      poly(polyinds{j}(i)).verts(:,[2:end,1])]');
%                 intersecting = any(intersects.intAdjacencyMatrix(:) == 1);
                
                intersects = linesegLinesegIntersection2D_mex(...
                    pts(:,1:end-1),pts(:,2:end),...
                    poly(polyinds{j}(i)).verts,...
                    poly(polyinds{j}(i)).verts(:,[2:end,1]));
                intersecting = any(intersects(:) > 0);
                
                pointinpoly = pointInPolygon_mex(p(1),p(2),poly(polyinds{j}(i)).verts(1,:),poly(polyinds{j}(i)).verts(2,:));

                keepmask(i) = intersecting | pointinpoly;
                
                if nargout >= 2
                    if intersecting
                        relation{j}(i) = 3;
                    elseif pointinpoly
                        relation{j}(i) = 1;
                    end
                end
            end
            polyinds{j} = polyinds{j}(keepmask);
            if nargout >= 2
                relation{j} = relation{j}(keepmask);
            end
        end
    end
end

function s = similarityImpl(obj,obj2)
    s = zeros(numel(obj),numel(obj2));
    for k=1:numel(obj)
        o = obj(k);
        for i=1:numel(obj2)
            o2 = obj2(i);

            al1 = o.arclen(end);
            al2 = o2(i).arclen(end);

%             as1 = o.anglesum;
%             as2 = o2(i).anglesum;

            c1 = abs(o.curvature);
            c2 = abs(o2(i).curvature);

            if numel(c1) <= 2
                c1 = 0;
            else
                c1 = mean(c1(2:end-1));
            end

            if numel(c2) <= 2
                c2 = 0;
            else
                c2 = mean(c2(2:end-1));
            end
%             c1 = mean(o.curvature(2:end-1));
%             c2 = mean(o2(i).curvature(2:end-1));
            curverrortol = 0.3;
            maxc = max(curverrortol,max(c1,c2));

            s(i) = (1 - abs(al1-al2)/max(al1,al2)) * 0.3 + ...
                max(0,min(1, (maxc-abs(c1-c2))/maxc )) * 0.7;

            s(i) = s(i) * 0.7 + o.parentelement.similarity(o2(i).parentelement)*0.3;
%             (1/(abs(c1-c2)+1)) * 0.7;
%             (1 - abs(c1-c2)/max(curverrortol,max(c1,c2))) * 0.7;        
%                     (1 - abs(as1-as2)/max(as1,as2)) * 0.7;
%                 (4/(as1/as2+as2/as1)^2) * 0.7;
%             (1/(abs(as1-as2)+1)) * 0.7;
        end
    end
end

end % methods

end % classdef
