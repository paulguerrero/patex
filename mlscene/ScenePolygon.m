classdef ScenePolygon < ScenePolyline

properties (Dependent,SetAccess=protected)
    area;
    perimeter;
    roundness;
end
properties(Access={?ScenePolygon,?SceneImporter,?SceneExporter})
    p_area = [];
    p_perimeter = [];
    p_roundness = [];
end

methods(Static)

function n = type_s
    n = 'polygon';
end

function d = dimension_s
    d = 1;
end

end

% for corner cells:
% - would need trimmed skeleton with edges only to the corner vertices
% (not the actual hull vertices)
% - also remove dangling edges and vertices (vertices that are not on the
% boundary and attached to only one edge and corresponding edges)

methods

% ScenePolygon(copyobj)
% ScenePolygon(verts, labels)
% ScenePolygon(polylines, labels)
% ScenePolygon(polylines, labels, polylinesclosed)
function obj = ScenePolygon(in1,in2,in3)

    obj@ScenePolyline;
%     obj.type = SceneFeature.POLYGON;
    obj.closed = true;
    
    if nargin == 0
        % do nothing (default values)
    elseif nargin == 1 && isa(in1,'ScenePolygon')
        obj2 = in1;

        obj.copyFrom(obj2);
    elseif nargin == 1
        [verts(1,:),verts(2,:)] = poly2cw(in1(1,:),in1(2,:));
        obj.setVerts(verts);

        obj.definePosition(obj.center);
        obj.defineOrientation(0);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
    elseif nargin == 2 && isnumeric(in1)
        [verts(1,:),verts(2,:)] = poly2cw(in1(1,:),in1(2,:));
        obj.setLabels(in2);
        obj.setVerts(verts);
        
        obj.definePosition(obj.center);
        obj.defineOrientation(0);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
    elseif nargin == 2 && iscell(in1)
        if iscell(in1)
            vrep = in1;
        else
            vrep = {in1};
        end
        [vrep,vrepclosed] = ScenePolygon.preparePolylines(vrep);
        if numel(in1) > 1
            verts = ScenePolygon.computeVertsFromPolylines(vrep,vrepclosed);
        else
            verts = vrep{1};
        end
        
        obj.setLabels(in2);
        obj.setVerts(verts);
        
        obj.definePosition(obj.center);
        obj.defineOrientation(0);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
    elseif nargin == 3 && iscell(in1)
        if iscell(in1)
            vrep = in1;
        else
            vrep = {in1};
        end
        vrepclosed = in3;
        [vrep,vrepclosed] = ScenePolygon.preparePolylines(vrep,vrepclosed);
        if numel(in1) > 1
            verts = ScenePolygon.computeVertsFromPolylines(vrep,vrepclosed);
        else
            verts = vrep{1};
        end
        
        obj.setLabels(in2);
        obj.setVerts(verts);
        
        obj.definePosition(obj.center);
        obj.defineOrientation(0);
        obj.defineSize(obj.extent);
        obj.defineMirrored(false);
    else
        error('cannot construct ScenePolygon with these parameters')
    end
end

function [additionalpolys,alldegenerate] = removeDegeneracies(obj,splitonedgetouch)
    additionalpolys = ScenePolygon.empty(1,0);
    alldegenerate = false;
    
    if nargin < 2 || isempty(splitonedgetouch)
        splitonedgetouch = false;
    end
    
    obj.p_corners = [];
    obj.p_edges = [];
    
    obj.p_center = [];
    obj.p_pcorientation = [];
    obj.p_extent = [];
    obj.p_radius = [];
    obj.p_orientedbbox = [];
    obj.p_convexhull = [];
    obj.p_principalcomps = [];
    obj.p_principalbbox = [];
    
    obj.p_area = [];
    obj.p_perimeter = [];
    obj.p_roundness = [];
    
    [~,~,bbdiag] = obj.boundingbox;
%     poseps = 0.001; % needs to be this high, otherwise the polygon skeleton has problems
    poseps = bbdiag .*0.0001;
    
    if isempty(obj.verts)
        alldegenerate = true;
        return;
    end
    
    polys = {obj.verts};
    
    % contract edges that are too short
    polys = ScenePolygon.removeDegenerateEdges(polys,poseps);
    polys = polys{1};
    if isempty(polys)
        alldegenerate = true;
        return;
    end
    
    % split polygon at vertices that (nearly) touch edges
    if splitonedgetouch
%         obj.p_center = [];
%         obj.p_radius = [];
%         obj.p_extent = [];
%         obj.p_visrep = {};
%         obj.p_visrepclosed = [];
        if not(isempty(obj.segments))
            error('Please remove segments first, segments would get lost.');
        end        
        
        polys = ScenePolygon.removeTouchingVerts(polys,poseps);
        polys = polys{1};
        if isempty(polys)
            alldegenerate = true;
            return;
        end
    end
    
    polys = ScenePolygon.removeIntersectingEdges(polys,poseps);
    polys = polys{1};
    if isempty(polys)
        alldegenerate = true;
        return;
    end
    
    % run deg. edge removal and touching vert removal again, because edges
    % may have changed
    polys = ScenePolygon.removeDegenerateEdges(polys,poseps);
    polys = polys{1};
    if isempty(polys)
        alldegenerate = true;
        return;
    end
    if splitonedgetouch
        polys = ScenePolygon.removeTouchingVerts(polys,poseps);
        polys = polys{1};
        if isempty(polys)
            alldegenerate = true;
            return;
        end
    end
    
    if numel(polys) == 1
        obj.setVerts(polys{1});
        obj.definePose(obj.defaultPose);
%         obj.definePosition(obj.center);
%         obj.defineOrientation(obj.pcorientation);
%         obj.defineSize(obj.extent);
%         obj.defineMirrored(false);
    else
        % pick polygon with largest area
        pareas = cellfun(@(x) polyarea(x(1,:),x(2,:)),polys);
        [~,ind] = max(abs(pareas));
        obj.setVerts(polys{ind});
        obj.definePose(obj.defaultPose);
%         obj.definePosition(obj.center);
%         obj.defineOrientation(obj.pcorientation);
%         obj.defineSize(obj.extent);
%         obj.defineMirrored(false);
        polys(ind) = [];

        for i=1:numel(polys)
            additionalpolys(end+1) = ScenePolygon(polys{i},obj.labels); %#ok<AGROW>
        end
    end
end

end

methods(Static)

function opolys = removeDegenerateEdges(polys,poseps)
%     alldegenerate = false(1,numel(polys));
    opolys = cell(1,numel(polys));
    for k=1:numel(polys)
%         opolys{k} = ScenePolygon.empty(1,0);

        pverts = polys{k};
        
        edgevecs = pverts(:,[2:end,1]) - pverts;
        edgelen = sqrt(sum(edgevecs.^2,1));
        vertangles = polylineAngles(pverts,true);

        degedges = find(edgelen < poseps);
        while(not(isempty(degedges)))
            if size(pverts,2) <= 3
%                 alldegenerate(k) = true;
                break;
            end

            [~,ind] = min(edgelen(degedges));
            edgeind = degedges(ind);

            vind1 = edgeind;
            vind2 = smod(edgeind+1,1,size(pverts,2)+1);

            % delete the one with the smaller angle
            angle1 = abs(vertangles(vind1));
            angle2 = abs(vertangles(vind2));
            if angle1>angle2
                removeind = vind1;
            else
                removeind = vind2;
            end
            pverts(:,removeind) = [];

            % takes longer to recompute, but should be ok
            edgevecs = pverts(:,[2:end,1]) - pverts;
            edgelen = sqrt(sum(edgevecs.^2,1));
            vertangles = polylineAngles(pverts,true);
            degedges = find(edgelen <poseps);
        end
        
        opolys{k} = {pverts};
    end
end

function opolys = removeTouchingVerts(polys,poseps)
%     alldegenerate = false(1,numel(polys));
%     additionalpolys = cell(1,numel(polys));
    opolys = cell(1,numel(polys));
    for k=1:numel(polys)
%         opolys{k} = ScenePolygon.empty(1,0);
        
        newpolys = polys(k);
        finishedpolys = {};

        c=0;
        while not(isempty(newpolys))

            v = newpolys{end};
            newpolys(end) = [];

            segs = [v;v(:,[2:end,1])];
            vedgedists = inf(1,size(v,2));
            vseginds = zeros(1,size(v,2));
            vcp = zeros(2,size(v,2));
            for i=1:size(v,2)
                nonadjacentsegmask = true(1,size(segs,2));
                nonadjacentsegmask(smod([i-1,i],1,size(segs,2)+1)) = false;
                nonadjacentseginds = find(nonadjacentsegmask);
                [vedgedists(i),vseginds(i),~,vcp(1,i),vcp(2,i)] = pointPolylineDistance_mex(...
                    v(1,i),v(2,i),...
                    segs(1,nonadjacentseginds),segs(2,nonadjacentseginds),...
                    segs(3,nonadjacentseginds),segs(4,nonadjacentseginds));

                vseginds(i) = nonadjacentseginds(vseginds(i));
            end

            [mindist,minvind] = min(vedgedists);
            if mindist < poseps
                % split at the vertex with minimum distance to edge
                vind = minvind;
                segind = vseginds(vind);
                cp = vcp(:,vind);

                if segind<vind
                    poly1verts = v(:,[vind:size(v,2),1:segind]);
                else
                    poly1verts = v(:,vind:segind);
                end
                poly1verts(:,1) = cp;

                poly2start = smod(segind+1,1,size(v,2)+1);
                if poly2start<vind
                    poly2verts = v(:,poly2start:vind);
                else
                    poly2verts = v(:,[poly2start:size(v,2),1:vind]);
                end
                poly2verts(:,end) = cp;

                if size(poly1verts,2) >= 3
                    newpolys{end+1} = poly1verts; %#ok<AGROW>
                end
                if size(poly2verts,2) >= 3
                    newpolys{end+1} = poly2verts; %#ok<AGROW>
                end
            else
                finishedpolys{end+1} = v; %#ok<AGROW>
            end

            % safety
            if c > size(polys{k},2)
                error('Apparently stuck in an endless loop.');
            end
            c = c + 1;
        end
        
        opolys{k} = finishedpolys;
        
%         if isempty(finishedpolys)
%             alldegenerate(k) = true;
%         elseif numel(finishedpolys) == 1
%             polys(k).verts = finishedpolys{1};
%         else
%             % pick polygon with largest area
%             pareas = cellfun(@(x) polyarea(x(1,:),x(2,:)),finishedpolys);
%             [~,ind] = max(abs(pareas));
%             polys(k).verts = finishedpolys{ind};
%             finishedpolys(ind) = []; %#ok<AGROW>
% 
%             for i=1:numel(finishedpolys)
%                 additionalpolys{k}(end+1) = ScenePolygon(finishedpolys{i},polys(k).label);
%             end
%         end
    end
end

function opolys = removeIntersectingEdges(polys,poseps)
    opolys = cell(1,numel(polys));
    for k=1:numel(polys)
        pverts = polys{k};
        
        % split at intersecting edges
        is = linesegLinesegIntersection2D_mex(...
            pverts,pverts(:,[2:end,1]),...
            pverts,pverts(:,[2:end,1]));
        % remove intersections of segment with itself
        is(sub2ind(size(is),1:size(is),1:size(is))) = 0;
        % remove intersections of segment with direct neighbors
        is(sub2ind(size(is),1:size(is),[2:size(is),1])) = 0;
        is(sub2ind(size(is),[2:size(is),1],1:size(is))) = 0;
        if any(is(:) > 0)
            % boundary has self-intersections
%             [~,~,bpolybbdiag] = pointsetBoundingbox(v);
            newboundaries = polylineSetOutline(pverts,true,false,false,poseps);
            if numel(newboundaries) == 0
                opolys{k} = {zeros(2,0)};
                continue;
            elseif numel(newboundaries) > 1
                pareas = cellfun(@(x) polyarea(x(1,:),x(2,:)),newboundaries);
                [~,maxareaind] = max(pareas);
                opolys{k} = newboundaries(maxareaind);
            else
                opolys{k} = newboundaries(1);
            end
        else
            opolys{k} = {pverts};
        end
    end
end

end

methods

function copyFrom(obj,obj2)

    copyFrom@ScenePolyline(obj,obj2);
    
    obj.p_area = obj2.p_area;
    obj.p_perimeter = obj2.p_perimeter;
    obj.p_roundness = obj2.p_roundness;
end

function obj2 = clone(obj)
    obj2 = ScenePolygon(obj);
end

% function delete(obj)
% end

function setVerts(obj,verts)
    obj.setVerts@ScenePolyline(verts,true)
end
    
function val = get.area(obj)
    if isempty(obj.p_area)
        obj.updateArea;
    end
    val = obj.p_area;
end
function val = getArea(obj)
    mask = not(obj.hasArea);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updateArea;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updateArea;
                end
            end
        end
    end
    val = [obj.p_area];
end
function updateArea(obj)
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_area = polyarea(o.verts(1,:),o.verts(2,:));
    end
end
function b = hasArea(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_area));
    end
end

function val = get.perimeter(obj)
    if isempty(obj.p_perimeter)
        obj.updatePerimeter;
    end
    val = obj.p_perimeter;
end
function val = getPerimeter(obj)
    mask = not(obj.hasPerimeter);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updatePerimeter;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updatePerimeter;
                end
            end
        end
    end
    val = [obj.p_perimeter];
end
function updatePerimeter(obj)
    
    for k=1:numel(obj)
        o = obj(k);

        perim = polylineArclen(o.verts,true);
        o.p_perimeter = perim(end);
    end
end
function b = hasPerimeter(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_perimeter));
    end
end
    
function val = get.roundness(obj)
    if isempty(obj.p_roundness)
        obj.updateRoundness;
    end
    val = obj.p_roundness;
end
function val = getRoundness(obj)
    mask = not(obj.hasRoundness);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updateRoundness;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updateRoundness;
                end
            end
        end
    end
    val = [obj.p_roundness];
end
function b = hasRoundness(obj)
    b = false(1,numel(obj));
    for i=1:numel(obj)
        b(i) = not(isempty(obj(i).p_roundness));
    end
end
function updateRoundness(obj)
    mask = not(obj.hasArea);
    if any(mask)
        obj(mask).updateArea;
    end
    mask = not(obj.hasPerimeter);
    if any(mask)
        obj(mask).updatePerimeter;
    end
    
    for k=1:numel(obj)
        o = obj(k);

        o.p_roundness = (4*pi*o.area)/(o.perimeter^2);
    end
end

function updateCenter(obj)
    for k=1:numel(obj)
        o = obj(k);
        
        % center is polygon centroid
        o.p_center = zeros(2,1);
        [o.p_center(1),o.p_center(2)] = polygonCentroid(o.verts(1,:),o.verts(2,:));
    end
end

% function feats = getSubfeatures(obj)
%     feats = obj.getSubfeatures@ScenePolyline;
%     
%     % no other subfeatures
% end
% function b = hasSubfeatures(obj)
%     b = obj.hasSubfeatures@ScenePolyline;
%     
%     % no other subfeatures
% end
% function updateSubfeatures(obj)
%     obj.updateSubfeatures@ScenePolyline;
%     
%     % no other subfeatures
% end

end

methods(Access=protected)
    
function dirtyImpl(obj)
    obj.dirtyImpl@ScenePolyline;
    
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_area = [];
        o.p_perimeter = [];
        o.p_roundness = [];
    end
end

function updateLocalImpl(obj)

    obj.updateLocalImpl@ScenePolyline;
    
    mask = not(obj.hasArea);
    if any(mask)
        obj(mask).updateArea;
    end
    mask = not(obj.hasPerimeter);
    if any(mask)
        obj(mask).updatePerimeter;
    end
    mask = not(obj.hasRoundness);
    if any(mask)
        obj(mask).updateRoundness;
    end
end
    
% function updateContextImpl(obj)
%     obj.updateContextImpl@ScenePolyline;
% end
% 
% function clearContextImpl(obj)
%     obj.clearContextImpl@ScenePolyline;
% end
    
% function moveAboutOriginImpl(obj,movevec)
%     obj.moveAboutOriginImpl@ScenePolyline(movevec);
% end
% 
% function rotateAboutOriginImpl(obj,rotangle)
% 
%     obj.rotateAboutOriginImpl@ScenePolyline(rotangle);
% end

function scaleAboutOriginImpl(obj,relscale)
    
    obj.scaleAboutOriginImpl@ScenePolyline(relscale);
    
    if not(isempty(obj.p_area))
        obj.p_area = obj.p_area * prod(relscale);
    end
    
    obj.p_perimeter = [];
    obj.p_roundness = [];
end

% when mirroring a polygon, the winding order needs to be mantained and as
% a result the ordering of the vertices is reversed
function mirrorAboutOriginImpl(obj,mirchange)
    
    if not(mirchange)
        return;
    end
    
    v = bsxfun(@times,obj.verts,[1;-1]);
    obj.verts = v(:,[1,end:-1:2]);
        
    obj.p_orientedbbox = []; 
    if obj.hasConvexhull
        obj.p_convexhull = [obj.p_convexhull(1,:);-obj.p_convexhull(2,:)]; 
        obj.p_convexhull = obj.p_convexhull(:,[1,end:-1:2]);
    end
    obj.p_principalcomps = [];
    obj.p_principalbbox = [];
    
    % corners, edges and segments may store intermediate results that need
    % to be invalidated
    for i=1:numel(obj.p_corners)
        obj.p_corners(i).dirty;
        obj.p_corners(i).cornervertex = numel(obj.p_corners)+1 - obj.p_corners(i).cornervertex;
    end
    for i=1:numel(obj.p_edges)
        obj.p_edges(i).dirty;
        oldstartarclen = obj.p_edges(i).startarclen;
        oldendarclen = obj.p_edges(i).endarclen;
        obj.p_edges(i).startarclen = 1-oldendarclen;
        obj.p_edges(i).endarclen = 1-oldstartarclen;
    end
    for i=1:numel(obj.segments)
        obj.segments(i).dirty;
        oldstartarclen = obj.segments(i).startarclen;
        oldendarclen = obj.segments(i).endarclen;
        obj.segments(i).startarclen = 1-oldendarclen;
        obj.segments(i).endarclen = 1-oldstartarclen;
    end

end

function s = similarityImpl(obj,obj2)
%     s = 0.75;
%     [~,~,a1] = polygonCentroid(obj.verts(1,:),obj.verts(2,:));
%     a1 = abs(a1);
%     [~,~,a2] = polygonCentroid(obj2.verts(1,:),obj2.verts(2,:));
%     a2 = abs(a2);
    s = zeros(numel(obj),numel(obj2));
    for k=1:numel(obj)
        o = obj(k);
        for i=1:numel(obj2)
            o2 = obj2(i);
            if o == o2(i)
                s(i) = 1;
                continue;
            end

            a1 = o.area;
            a2 = o2(i).area;

            r1 = o.roundness;
            r2 = o2(i).roundness;

            [~,~,ca1] = polygonCentroid(o.convexhull(1,:),o.convexhull(2,:));
            ca1 = abs(ca1);
            [~,~,ca2] = polygonCentroid(o2(i).convexhull(1,:),o2(i).convexhull(2,:));
            ca2 = abs(ca2);

            cv1 = 1-max(0,min(1,(ca1-a1)/ca1));
            cv2 = 1-max(0,min(1,(ca2-a2)/ca2));

            s(i) = 1/log((max(a1,a2)/min(a1,a2))*exp(1));
            s(i) = s(i) * (min(r1,r2)/max(r1,r2));
            s(i) = s(i) * (1-abs(cv1-cv2));
            s(i) = sqrt(s(i));
        end
    end
%     s = (min(a1,a2)/max(a1,a2)) * ;

%     s = 1; %htemp
end

end

methods(Static)

function verts = computeVertsFromPolylines(polylines,polylinesclosed)
    
    [bbmax,bbmin,polylineextent] = pointsetBoundingbox(polylines);
    
    verts = polylineSetOutline(polylines,polylinesclosed,false,false,polylineextent*0.0001);
    
    if numel(verts) == 1
        % one component (standard case)
        verts = verts{1};
        [verts(1,:),verts(2,:)] = poly2cw(verts(1,:),verts(2,:));
    elseif isempty(verts)
        % no components => if polylines do not enclose an area, merge small
        % offset hulls around all polylines (offset with rectangle, vertices
        % of sphere may be too close together because sphere is very small)
        x = [];
        y = [];
        for i=1:numel(polylines)
            [ox,oy] = polylineOffset(...
                polylines{i}(1,:),...
                polylines{i}(2,:),...
                polylineextent*0.01,4);
            if isempty(ox) || isempty(ox{1})
                error('Error while offseting polygon: no output.');
            end
            
            [ox,oy] = poly2cw(ox{1}{1},oy{1}{1});
            [x,y] = polybool('union',...
                x,y,...
                ox,oy);
                
        end
        verts = [x;y];
        
        if isempty(verts)
            error('Polygon seems to have an empty area, make sure the polygon has at least some area.');
        end
        
        % remove duplicate vertex at end of each hull component
        naninds = find(isnan(verts(1,:)));
        startinds = [1,naninds+1];
        endinds = [naninds-1,size(verts,2)];
        mask = all(verts(:,startinds) == verts(:,endinds),1);
        verts(:,endinds(mask)) = [];
        
        if isShapeMultipart(verts(1,:),verts(2,:))
            error('Polygons with multiple components not supported.');
        end
    else
        warning('Polygons with multiple components detected, ignoring smaller components.');        
        pareas = cellfun(@(x) polyarea(x(1,:),x(2,:)),verts);
        [~,maxind] = max(pareas);
%         [~,maxind] = max(cellfun(@(x) size(x,2),verts));
        verts = verts{maxind};
        [verts(1,:),verts(2,:)] = poly2cw(verts(1,:),verts(2,:));
%         % multiple components
%         error('Polygons with multiple components not supported.');
    end
    
    % merge hull vertices that are too close together to avoid numeric
    % issues (mainly with skeleton)
    maxcoord = max(max(abs([bbmax,bbmin])));
%     snapdist = 1e-6 * polylineextent;
    snapdist = 1e-6 * maxcoord;
    
    [mindiff,mindiffind] = min(sqrt(sum(diff(...
        [verts(1,[1:end,1]);verts(2,[1:end,1])],1,2).^2,1)));

    while mindiff < snapdist && size(verts,2) > 3
        mergeind1 = mindiffind;
        mergeind2 = mod(mindiffind,size(verts,2))+1;
        verts(:,mergeind1) = (verts(:,mergeind1) + verts(:,mergeind2))./2;
        verts(:,mergeind2) = [];
%         vertspx(mergeind1) = (px(mergeind1) + px(mergeind2))/2;

        [mindiff,mindiffind] = min(sqrt(sum(diff(...
            [verts(1,[1:end,1]);verts(2,[1:end,1])],1,2).^2,1)));
    end
    
    if mindiff < snapdist
        error('Numeric issues with this shape, vertices are too close together.');
    end
end

function [pl,plclosed] = preparePolylines(pl,plclosed)
    plextent = [max(cellfun(@(x) max(x(1,:)),pl)) - min(cellfun(@(x) min(x(1,:)),pl)),...
                max(cellfun(@(x) max(x(2,:)),pl)) - min(cellfun(@(x) min(x(2,:)),pl))];
    minseglen = norm(plextent) * 0.000001;

    if nargin < 2
        [pl,plclosed] = polylineClose(pl);
    end
    
    [pl,ind] = polylineClean(pl,plclosed,minseglen);
    plclosed = plclosed(ind);
    
    if not(plclosed)
        [pl,plclosed] = polylineClose(pl);
    end
    
    pl = polylineSetWinding(pl, plclosed, 'cw');
end

end


end
