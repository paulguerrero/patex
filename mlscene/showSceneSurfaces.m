function gobjs = showSceneSurfaces(surfaces,gobjs,parent,varargin)

    surfaceoffset = 0.0001;
    
%     if not(iscell(surfaces))
%         surfaces = num2cell(surfaces);
%     end
    
    % create patch vertices, faces, etc.
    if iscell(surfaces)
        verts = cell(1,numel(surfaces));
        faces = cell(1,numel(surfaces));
        vnormals = cell(1,numel(surfaces));
        for j=1:numel(surfaces)
            [~,~,bbdiags] = surfaces{j}.boundingbox;
            offsets = bbdiags .* surfaceoffset;

            verts{j} = {surfaces{j}.verts};
            faces{j} = {surfaces{j}.faces};
            vnormals{j} = {surfaces{j}.vnormals};

            % add offsets
            verts{j} = cellfun(@(v,vn,o) v+vn.*o, verts{j},vnormals{j},num2cell(offsets),'UniformOutput',false);
        end
    else
        [~,~,bbdiags] = surfaces.boundingbox;
        offsets = bbdiags .* surfaceoffset;

        verts = {surfaces.verts};
        faces = {surfaces.faces};
        vnormals = {surfaces.vnormals};

        % add offsets
        verts = cellfun(@(v,vn,o) v+vn.*o, verts,vnormals,num2cell(offsets),'UniformOutput',false);
    end
    
    gobjs = showPatches(verts,faces,gobjs,parent,...
        'vnormals',vnormals,...
        varargin{:});
end
