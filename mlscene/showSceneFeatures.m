function gobjs = showSceneFeatures(features,gobjs,parents,varargin)

    if isempty(features)
        gobjs = gobjects(1,0);
    end

    feattypes = {features.type};
    if not(all(strcmp(feattypes,feattypes{1})))
        error('Currently only homogeneous feature arrays are supported.');
    end
    
    feattype = feattypes{1};
    if strcmp(feattype,'point')
        gobjs = showScenePoints(features,gobjs,parents,varargin{:});
    elseif strcmp(feattype,'polyline')
        gobjs = showScenePolylines(features,gobjs,parents,varargin{:});
    elseif strcmp(feattype,'polygon')
        gobjs = showScenePolygons(features,gobjs,parents,varargin{:});
    elseif strcmp(feattype,'complexpolygon')
        gobjs = showSceneComplexPolygons(features,gobjs,parents,varargin{:});
    elseif strcmp(feattype,'mesh')
        gobjs = showSceneMeshes(features,gobjs,parents,varargin{:});
    elseif strcmp(feattype,'corner')
        gobjs = showSceneCorners(features,gobjs,parents,varargin{:});
    elseif strcmp(feattype,'edge')
        gobjs = showSceneEdges(features,gobjs,parents,varargin{:});
    elseif strcmp(feattype,'segment')
        gobjs = showSceneSegments(features,gobjs,parents,varargin{:});
    elseif strcmp(feattype,'surfaces')
        gobjs = showSceneSurfaces(features,gobjs,parents,varargin{:});
    end
end
