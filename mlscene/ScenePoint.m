classdef ScenePoint < SceneElement2D

methods(Static)

function n = type_s
    n = 'point';
end

function d = dimension_s
    d = 0;
end

end

methods

% obj = ScenePoint()
% obj = ScenePoint(obj2)
% obj = ScenePoint(position,orientation,labels)
function obj = ScenePoint(varargin)
    obj@SceneElement2D;
    
    if numel(varargin) == 0
        % empty point
    elseif numel(varargin) == 1 && isa(varargin{1},'ScenePoint')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 3
        obj.setLabels(varargin{3});
        
        obj.definePosition(varargin{1});
        obj.defineOrientation(varargin{2});
        obj.defineSize([0;0]);
        obj.defineMirrored(false);
    else
        error('Invalid number of arguments.');
    end
end

function copyFrom(obj,obj2)
    obj.copyFrom@SceneElement2D(obj2);
end

function obj2 = clone(obj)
    obj2 = ScenePoint(obj);
end

function updateSymmetries(obj)

    for k=1:numel(obj)
        o = obj(k);

        o.p_symmetries = false(5,1);
    end
end

function definePosition(obj,val) %#ok<INUSD>
    error('Cannot redefine position of a point.');
end
function defineSize(obj,val) %#ok<INUSD>
    error('Cannot redefine size of a point.');
end

end

methods(Access=protected)
    
function val = defaultPoseImpl(obj)
    val = [...
        obj.position;...
        0;...
        zeros(3,1);...
        0];
end
    
function updateBoundingboxImpl(obj)
    for i=1:numel(obj)
        o = obj(i); % for performance
        p = o.position;
        o.p_boundingboxmin = p;
        o.p_boundingboxmax = p;
    end
end

function [center,width,height,orientation] = posedBoundingbox2DImpl(obj,framepose,featpose) %#ok<INUSD>
    
    if nargin < 2 || isempty(framepose)
        framepose = [...
            zeros(numel(obj));...
            zeros(numel(obj));...
            zeros(numel(obj));...
            ones(numel(obj));...
            zeros(numel(obj))];
    end
    
    center = obj.getPosition;
    width = zeros(1,numel(obj));
    height = zeros(1,numel(obj));
    orientation = framepose(3,:);
end
    
% relations 0 and 2 are never returned, because only relevant poly indices
% (i.e. feature is inside poly or intersecting) are returned
% relation 3 is never return for a point, since a point is always
% considered to be either inside or outside a poly (never on boundary due
% to numeric precision)
% relation: 0: feature and poly are separate
% relation: 1: feature completely inside poly
% relation: 2: poly completely inside feature
% relation: 3: feature and poly are intersecting
function [polyinds,relation] = inPolygonImpl(obj,poly,polycandidates)
%     b = zeros(1,numel(poly));
    polyinds = cell(numel(obj),1);
    if nargout >= 2
        relation = cell(numel(obj),1);
    end
    if not(isempty(poly)) && not(isempty(obj))
        
        [polybbmin,polybbmax] = poly.boundingbox;
        positions = obj.getPosition;
        
        for j=1:numel(obj)
    
            % find polygons with bounding boxes containing the point
            if nargin < 3 || isempty(polycandidates)
                polyinds{j} = find(...
                    positions(1,j) > polybbmin(1,:) & positions(2,j) > polybbmin(2,:) & ...
                    positions(1,j) < polybbmax(1,:) & positions(2,j) < polybbmax(2,:));
            else
                polyinds{j} = polycandidates{j}(...
                    positions(1,j) > polybbmin(1,polycandidates{j}) & positions(2,j) > polybbmin(2,polycandidates{j}) & ...
                    positions(1,j) < polybbmax(1,polycandidates{j}) & positions(2,j) < polybbmax(2,polycandidates{j}));
            end

            % find all polygons containing the point
            p = positions(:,j);
            keepmask = false(1,numel(polyinds{j}));
            for i=1:numel(polyinds{j})
                % test if point is inside the polygon
                keepmask(i) = pointInPolygon_mex(p(1),p(2),poly(polyinds{j}(i)).verts(1,:),poly(polyinds{j}(i)).verts(2,:));
            end
            polyinds{j} = polyinds{j}(keepmask);
            if nargout >= 2
                relation{j} = ones(1,numel(polyinds{j}));
            end
        end
    end
end
    
function [d,cp] = pointDistanceImpl(obj,p,pose)
    if size(p,1) == 2
        d = inf(numel(obj),size(p,2));
        cp = zeros(numel(obj),size(p,2),2);
        for i=1:numel(obj)
            pt = obj(i).position;
            if nargin >= 3
                pt = posetransform(pt,pose{i},obj(i).pose);
            end
            d(i,:) = sqrt(sum(bsxfun(@minus,pt,p).^2,1));
            cp(i,:,:) = permute(repmat(pt,1,size(p,2)),[3,2,1]);
        end
    elseif size(p,1) == 3
        % assume z-coordinate of point is zero
        pts = obj.getPosition;
        pts = [pts;zeros(1,size(pts,2))];
        if nargin >= 3 && not(isempty(pose))
            pts = posetransform(pts,[pose{:}],obj.getPose3D);
        else
            mask = obj.hasScenegroup;

            sgroups = obj(mask).getScenegroup;
            sgroups = [sgroups{:}];
            grouppose = [sgroups.globaloriginpose];
            pts(:,mask) = transform3D(pts(:,mask),...
                grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
        end
        
        d = pdist2(pts,p);
        cp = permute(repmat(pts,1,1,size(p,2)),[2,3,1]);
    else
        error('Only implemented for two or three dimensions.');
    end
end

function [d,t,cp] = linesegDistanceImpl(obj,p1,p2,pose) % distance to a 3D line segment
    if size(p1,1) == 2
        d = inf(numel(obj),size(p1,2));
        t = nan(numel(obj),size(p1,2));
        cp = zeros(numel(obj),size(p1,2),2);
        pts = obj.getPosition;
        if nargin >= 4 && not(isempty(pose))
            pts = posetransform(pts,[pose{:}],obj.getPose);
        end
        for i=1:size(p1,2)
            [d(:,i),~,t(:,i)] = pointPolylineDistance_mex(pts(1,:), pts(2,:),...
                p1(1,i),p1(2,i),p2(1,i),p2(2,i));
            cp(:,i,:) = permute(pts,[2,3,1]);
        end
    elseif size(p1,1) == 3
        % assume z-coordinate of point is zero
        pts = obj.getPosition;
        pts = [pts;zeros(1,size(pts,2))];
        if nargin >= 4 && not(isempty(pose))
            pts = posetransform(pts,[pose{:}],obj.getPose3D);
        else
            mask = obj.hasScenegroup;

            sgroups = obj(mask).getScenegroup;
            sgroups = [sgroups{:}];
            grouppose = [sgroups.globaloriginpose];
            pts(:,mask) = transform3D(pts(:,mask),...
                grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
        end
        [d,t] = pointLinesegDistance3D_mex(pts,p1,p2);
        cp = permute(repmat(pts,1,1,size(p1,2)),[2,3,1]);
    else
        error('Only implemented for two or three dimensions.');
    end
end

% function updateSubfeaturesImpl(obj)
%     obj.p_subfeatures = SceneCorner.empty;
% end
function feats = subfeaturesImpl(obj)
    feats = repmat({SceneCorner.empty},1,numel(obj));
end
function b = hasSubfeaturesImpl(obj)
    b = true(1,numel(obj));
end
function updateSubfeaturesImpl(obj) %#ok<MANU>
    % do nothing
end

function moveAboutOriginImpl(obj,movevec) %#ok<INUSD>
    % do nothing
end
function rotateAboutOriginImpl(obj,rotangle) %#ok<INUSD>
    % do nothing
end
function scaleAboutOriginImpl(obj,relscale) %#ok<INUSD>
    % do nothing
end
function mirrorAboutOriginImpl(obj,mirchange) %#ok<INUSD>
    % do nothing
end

function s = similarityImpl(obj,obj2)
    s = ones(numel(obj),numel(obj2)); % all points have similarity 1 to each other
end

end

end % classdef
