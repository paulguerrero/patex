% A basic independent element of the scene
classdef (Abstract) SceneElement3D < SceneElement

% Each element has a position ,orientation, size (e.g. extent) and mirrored
% property. When the element is transformed (moved, scaled, rotated, etc.),
% all its properties are invalidated, since they are stored in global
% coordinates for efficiency (e.g. area of an object when scaled, skeleton
% of an object when moved, etc.). Overwrite the move,rotate,scale and
% mirror methods in the subclasses to update the properties. The position,
% orientation, etc., as one of the element properties, are transformed as
% well. To change the local coordinate frame without altering the vertices,
% use definePosition, defineOrientation, defineSize, defineMirrored, etc.

% 3D objects have quaternion for orientation (identity is [1;0;0;0], where
% front is in x direction and up in z direction by convention)

%#ok<*CPROP>
% to suppress warnings that the function size() could be confused with the
% class property obj.size

properties(SetAccess={?SceneElement2D,?SceneImporter,?SceneExporter}, SetObservable, AbortSet)
    position = zeros(3,1);
    orientation = [1;0;0;0];
    size = ones(3,1);
    mirrored = false;
end

properties(Dependent, SetAccess=protected)
    yawPitchRoll; % counter-clockwise about z,y,x axes (when looking at origin)
    symmetries; % the 3 symmetries are: mirror y,x,z
end

properties(Access={?SceneElement3D,?SceneImporter,?SceneExporter})
    p_symmetries = []; % the 5 symmetries are: mirror y,x and rot 90,180,270
end

methods
    
function obj = SceneElement3D(labels,pos,orientation,size,mirrored)
    if nargin >= 1
        superargin = {labels};
    else
        superargin = {};
    end
    
    obj@SceneElement(superargin{:});
    
    if nargin >= 2 && not(isempty(pos))
        obj.position = pos;
    end
    if nargin >= 3 && not(isempty(orientation))
        obj.orientation = orientation;
    end
    if nargin >= 4 && not(isempty(size))
        obj.size = size;
    end
    if nargin >= 5 && not(isempty(mirrored))
        obj.mirrored = mirrored;
    else
        obj.mirrored = false;
    end
end

function copyFrom(obj,obj2)
    obj.copyFrom@SceneElement(obj2);
    
    obj.position = obj2.position;
    obj.orientation = obj2.orientation;
    obj.size = obj2.size;
    obj.mirrored = obj2.mirrored;
   
    obj.p_symmetries = obj2.p_symmetries;
end

function val = get.yawPitchRoll(obj)
    val = zeros(3,1);
    [val(1),val(2),val(3)] = quat2angle(obj.orientation');
end

function val = get.symmetries(obj)
    if isempty(obj.p_symmetries)
        obj.updateSymmetries;
    end
    
    val = obj.p_symmetries;
end

function [type,params] = computeSymmetryparams(obj)
    
%     if nargin < 2
%         onlymirror = false;
%     end
    
    type = {};
    params = zeros(3,0);
    
    % plane orthogonal to front/back direction
    if obj.symmetries(1)
        type{end+1} = 'mirror';
        params(:,end+1) = [obj.position;obj.orientation];
    end
    
    % plane orthogonal to left/right direction
    if obj.symmetries(2)
        type{end+1} = 'mirror';
        params(:,end+1) = [obj.position;smod(obj.orientation+[pi/2;0;0],-pi,pi)];
    end
    
    % plane orthogonal to up/down direction
    if obj.symmetries(3)
        type{end+1} = 'mirror';
        params(:,end+1) = [obj.position;smod(obj.orientation+[0;pi/2;0],-pi,pi)];
    end
    
%     if not(onlymirror) && obj.symmetries(3)
%         type{end+1} = 'rotational';
%         params(:,end+1) = [obj.position;pi*0.5];
%     end
%     
%     if not(onlymirror) && obj.symmetries(4)
%         type{end+1} = 'rotational';
%         params(:,end+1) = [obj.position;pi];
%     end
%     
%     if not(onlymirror) && obj.symmetries(5)
%         type{end+1} = 'rotational';
%         params(:,end+1) = [obj.position;pi*1.5];
%     end
end

function movevec = move(obj,destpos,mode)
    if size(destpos,1) == 2 %#ok<CPROPLC>
        destpos = [destpos;0];
    elseif size(destpos,1) ~= 3 %#ok<CPROPLC>
        error('Invalid format for destination, must be given as 3D point.');
    end
    
    if strcmp(mode,'absolute')
        movevec = destpos-obj.position;
    elseif strcmp(mode,'relative')
        movevec = destpos;
    else
        error('unkown move mode');
    end
    
    if all(movevec == 0)
        % no movement
        return;
    end
    
    obj.moveAboutOriginImpl(movevec);
    
    if strcmp(mode,'absolute')
        obj.position = destpos; % to make sure it gets exactly the given position (no numeric errors)
    else
        obj.position = obj.position+movevec;
    end
    
    if not(isempty(obj.p_boundingboxmin))
        obj.p_boundingboxmin = obj.p_boundingboxmin+movevec;
    end
    if not(isempty(obj.p_boundingboxmax))
        obj.p_boundingboxmax = obj.p_boundingboxmax+movevec;
    end
    obj.clearContext;
end

function rotangle = rotate(obj,destorient,mode,pivotpos)
    
    % identity quaternion is an orientation pointing in x direction and up
    % in z direction
    
    if size(destorient,1) == 1 %#ok<CPROPLC>
        % take as orientation in xy-plane (angle around z axis)
        destorient = angle2quat(destorient,0,0)';
    elseif size(destorient,1) ~= 4 %#ok<CPROPLC>
        error('Invalid format for orientation, must be given as quaternion.');
    end
    if nargin >= 4 && not(isempty(pivotpos))
        if size(pivotpos,1) == 2 %#ok<CPROPLC>
            pivotpos = [pivotpos;0];
        elseif size(pivotpos,1) ~= 3 %#ok<CPROPLC>
            error('Invalid format for center, must be given as 3D point.');
        end
    end
    
    if strcmp(mode,'absolute')
        rotangle = quatmultiply(destorient',quatconj(obj.orientation'))';
    elseif strcmp(mode,'relative')
        rotangle = destorient;
    else
        error('unkown rotate mode');
    end
    
    if all(rotangle == [1;0;0;0])
        % no rotation
        return;
    end
    
    if nargin < 4 || isempty(pivotpos)
        pivotpos = obj.position;
    end
    
    obj.move(-pivotpos,'relative');
    
    obj.rotateAboutOriginImpl(rotangle);
    
    rotmat = quat2dcm(rotangle')';
    
    obj.position = rotmat*obj.position;
    if strcmp(mode,'absolute')
        obj.orientation = destorient; % to make sure it gets exactly the given orientation (no numeric errors)
    else
        obj.orientation = quatmultiply(rotangle',obj.orientation')';
    end
    
    obj.p_boundingboxmin = [];
    obj.p_boundingboxmax = [];
    obj.clearContext;
    
    obj.move(pivotpos,'relative');
end

function relscale = scale(obj,destsize,mode,pivotpos,pivotorient)
    
    if size(destsize,1) == 2 %#ok<CPROPLC>
        % take as scaling in xy-plane
        destsize = [destsize;1]; % convert from 2D to 3D
    elseif size(destsize,1) == 1 %#ok<CPROPLC>
        destsize = destsize([1,1,1],:); % uniform scaling
    elseif size(destsize,1) ~= 3 %#ok<CPROPLC>
        error('Invalid format for scaling, must be given as 1D or 3D vector for uniform and non-uniform scaling, respectively.');
    end
    if nargin >= 4 && not(isempty(pivotpos))
        if size(pivotpos,1) == 2 %#ok<CPROPLC>
            pivotpos = [pivotpos;0];
        elseif size(pivotpos,1) ~= 3 %#ok<CPROPLC>
            error('Invalid format for center, must be given as 3D point.');
        end
    end
    if nargin >= 5 && not(isempty(pivotorient))
        if size(pivotorient,1) == 1 %#ok<CPROPLC>
            % take as orientation in xy-plane (angle around z-axis)
            pivotorient = angle2quat(pivotorient,0,0)';
        elseif size(pivotorient,1) ~= 4 %#ok<CPROPLC>
            error('Invalid format for orientation, must be given as quaternion.');
        end
    end
    
    if any(destsize < 0)
        error(['Negative scaling not supported, use a combination of ',...
               'mirroring, rotations and positive scaling to get the same effect.']);
        % todo: automatically find and apply this combination of mirroring,
        % rotations and positive scaling here
    end
    
    if strcmp(mode,'absolute')
        if nargin >= 5 && not(isempty(pivotorient))
            error('Setting the absolute scale in a custom orientation is not implemented.');
            % would need to get current scale of object in the custom
            % orientation => may be too expensive, also current scaling in
            % other orientation is not known, since current scale might
            % also have been set manually
        end
        
        relscale = destsize./obj.size;
        
        relscale(obj.size == 0 & destsize == 0) = 1;
        
        if any(obj.size == 0 & destsize ~= 0)
            error('Element has zero size, cannot adjust it to any other size.');
        end
    elseif strcmp(mode,'relative')
        relscale = destsize;
    else
        error('unkown scale mode');
    end
    
    if all(relscale == 1)
        % no scaling
        return;
    end
    
    if nargin < 4 || isempty(pivotpos)
        pivotpos = obj.position;
    end
    if nargin < 5 || isempty(pivotorient)
        pivotorient = obj.orientation;
    end
    
    obj.move(-pivotpos,'relative');
    if any(relscale ~= relscale(1))
        obj.rotate(quatconj(pivotorient')','relative',[0;0;0]);
    end
    
    obj.scaleAboutOriginImpl(relscale);
    
    obj.position = obj.position.*relscale;
    if any(relscale ~= relscale(1))
        % scaling may have been about any axis
        a = quatmultiply(obj.orientation',quatconj(pivotorient'))';
        % no transpose for quat2dcm because quat2dcm produces the
        % rotated coordinate frame (i.e. basis of the rotated
        % coordinate frame, not the rotated vector)
%         obj.size = obj.size .* abs(quat2dcm(a') * diag(relscale));
        obj.size = obj.size .* sqrt(sum((quat2dcm(a') * diag(relscale)).^2,2));
    else
        obj.size = obj.size.*relscale(1);
    end
    
    obj.p_boundingboxmin = [];
    obj.p_boundingboxmax = [];
    if any(relscale ~= relscale(1))
        % rotational symmetries may be changed by non-uniform scaling
        obj.p_symmetries = [];
    end
    obj.clearContext;
    
    if any(relscale ~= relscale(1))
        obj.rotate(pivotorient,'relative',[0;0;0]);
    end
    obj.move(pivotpos,'relative');
end

function mirchange = mirror(obj,destmir,mode,pivotpos,pivotorient)
    if nargin < 2
        destmir = true;
    end
    if nargin < 3
        mode = relative;
    end
    
    if nargin >= 4 && not(isempty(pivotpos))
        if size(pivotpos,1) == 2 %#ok<CPROPLC>
            pivotpos = [pivotpos;0];
        elseif size(pivotpos,1) ~= 3 %#ok<CPROPLC>
            error('Invalid format for center, must be given as 3D point.');
        end
    end
    if nargin >= 5 && not(isempty(pivotorient))
        if size(pivotorient,1) == 1 %#ok<CPROPLC>
            % take as orientation in xy-plane (angle around z-axis)
            pivotorient = angle2quat(pivotorient,0,0)';
        elseif size(pivotorient,1) ~= 4 %#ok<CPROPLC>
            error('Invalid format for orientation, must be given as quaternion.');
        end
    end
    
    if strcmp(mode,'absolute')
        mirchange = destmir ~= obj.mirrored;
    elseif strcmp(mode,'relative')
        mirchange = destmir;
    else
        error('unkown mirror mode');
    end
    
    if not(mirchange)
        % no change in mirroring
        return;
    end
    
    if nargin < 4 || isempty(pivotpos)
        pivotpos = obj.position;
    end
    if nargin < 5 || isempty(pivotorient)
        pivotorient = obj.orientation;
    end
    
    obj.move(-pivotpos,'relative');
    obj.rotate(quatconj(pivotorient')','relative',[0;0;0]);
    
    obj.mirrorAboutOriginImpl(mirchange);
    
    obj.position(2) = -obj.position(2);
    % rotate 180 degrees about x-axis
    obj.orientation = quatmultiply(angleaxis2quat(pi,[1;0;0])',obj.orientation')';        
    if mirchange
        error('mirroring not yet implemented');
        % todo: 180 deg. rot. about x is probably not correct, e.g. when
        % orientation direction is almost in xz plane (the mirroring plane)
    end
    if strcmp(mode,'absolute')
        obj.mirrored = destmir;  % to make sure it gets exactly the given mirroring (no numeric errors)
    else
        obj.mirrored = xor(obj.mirrored,mirchange);
    end
        
    obj.p_boundingboxmin = [];
    obj.p_boundingboxmax = [];
    obj.clearContext;
    
    obj.rotate(pivotorient,'relative',[0;0;0]);
    obj.move(pivotpos,'relative');
end

function transform(obj,destpose,mode)
    if numel(obj) ~= 1 && size(destpose,2) == 1 %#ok<CPROPLC>
        destpose = repmat(destpose,1,numel(obj));
    end
    
    if size(destpose,1) ~= 11 %#ok<CPROPLC>
        destpose = obj.pose2Dto3D(destpose);
    end
    
    if strcmp(mode,'absolute')
        for i=1:numel(obj)
            o = obj(i);
            % order does not matter, the individual operations are correctly
            % done in the local coordinate systems
            if not(all(destpose(:,i) == o.pose))
                o.mirror(destpose(11,i),'absolute');
                o.scale(destpose(8:10,i),'absolute');
                o.rotate(destpose(4:7,i),'absolute');
                o.move(destpose(1:3,i),'absolute');
            end
        end
    elseif strcmp(mode,'relative')
        for i=1:numel(obj)
            o = obj(i);
            if not(all(destpose(:,i) == identpose3D))
                o.mirror(destpose(11,i),'relative');
                o.scale(destpose(8:10,i),'relative');
                o.rotate(destpose(4:7,i),'relative');
                o.move(destpose(1:3,i),'relative');
            end
        end
    else
        error('unkown transform mode');
    end
end

% define the pose without moving the element (i.e. define local coordinate system)
function definePosition(obj,val)
    if size(val,1) == 2 %#ok<CPROPLC>
        val = [val;zeros(1,size(val,2))]; %#ok<CPROPLC>
    elseif size(val,1) ~= 3 %#ok<CPROPLC>
        error('Invalid format for position, must be given as 3D point.');
    end
    
    [obj.position] = deal(val);
    
    obj.dirty;
end
function defineOrientation(obj,val)
    if size(val,1) == 1 %#ok<CPROPLC>
        % take as orientation in xy-plane (angle around z-axis)
        val = angle2quat(val,zeros(size(val)),zeros(size(val)))'; %#ok<CPROPLC>
    elseif size(val,1) ~= 4 %#ok<CPROPLC>
        error('Invalid format for orientation, must be given as quaternion.');
    end
    
    [obj.orientation] = deal(val);
    
    obj.dirty;
end
function defineSize(obj,val)
    if size(val,1) == 2 %#ok<CPROPLC>
        val = [val;ones(1,size(val,2))]; %#ok<CPROPLC>
    elseif size(val,1) ~= 3 %#ok<CPROPLC>
        error('Invalid format for size, must be given as 3D vector.');
    end

    [obj.size] = deal(val);
    
    obj.dirty;
end
function defineMirrored(obj,val)
    [obj.mirrored] = deal(val);
    
    obj.dirty;
end

end

methods(Sealed)

function val = getPosition(obj)
    val = [obj.position];
end
function val = getOrientation(obj)
    val = [obj.orientation];
end
function val = getSize(obj)
    val = [obj.size];
end
function val = getMirrored(obj)
    val = [obj.mirrored];
end
function val = getSymmetries(obj)
    mask = not(obj.hasSymmetries);
    if any(mask)
        o = obj(mask);
        if numel(o) == 1
            o.updateSymmetries;
        else
            [types,objinds] = SceneFeature.groupByType(o);
            for i=1:numel(types)
                if not(isempty(objinds{i}))
                    o(objinds{i}).updateSymmetries;
                end
            end
        end
    end
    
    val = [obj.symmetries];
end
function b = hasSymmetries(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_symmetries));
    end
end

end

methods(Access=protected, Sealed)

function val = getPoseImpl(obj)
    val = [obj.getPosition;obj.getOrientation;obj.getSize;obj.getMirrored];
end

function val = getPose2DImpl(obj)
    c = obj.getPosition;
    val = [c(1:2,:);quat2angle(obj.getOrientation')';obj.getSize;obj.getMirrored];
end

function val = getPose3DImpl(obj)
    val = [obj.getPosition;obj.getOrientation;obj.getSize;obj.getMirrored];
end
    
function setPoseImpl(obj,val)
    obj.transform(val,'absolute');
end

function setPose2DImpl(obj,val)
    orient = angle2quat(val(3,:)',0,0)';
    pose3D = [...
        val(1:2,:);...
        zeros(1,size(val,2));...
        orient;...
        val(4:5,:);... % size
        ones(1,size(val,2));...
        val(6,:)]; %#ok<CPROPLC>  % mirrored 
        
    obj.transform(pose3D,'absolute');
end

function setPose3DImpl(obj,val)
    obj.transform(val,'absolute');
end

end

methods(Access=protected)
    
% define the pose without moving the element (i.e. define local coordinate system)
function definePoseImpl(obj,val)
    if size(val,1) == 6 %#ok<CPROPLC>
        % 2d pose
        val = SceneElement.pose2Dto3D(val);
    elseif size(val,1) ~= 11 %#ok<CPROPLC>
        error('Invalid pose, must have 11 components for 3D pose or 6 components for 2D pose.');
    end
    
    obj.position = val(1:3,:);
    obj.orientation = val(4:7,:);
    obj.size = val(8:10,:);
    obj.mirrored = val(11,:);
end
    
function dirtyImpl(obj)
    obj.dirtyImpl@SceneElement;

    for k=1:numel(obj)
        o = obj(k);

        o.p_symmetries = [];
    end
end

function updateLocalImpl(obj)
    obj.updateLocalImpl@SceneElement;

    mask = not(obj.hasSymmetries);
    if any(mask)
        obj(mask).updateSymmetries;
    end
end

end

% methods(Static)
% 
% function obj = loadobj(s) % legacy
%     obj = 
%     disp(s);
% end
% 
% end

methods(Abstract)
    updateSymmetries(obj);
end

methods(Abstract,Access=protected)
    moveAboutOriginImpl(obj,movevec);
    rotateAboutOriginImpl(obj,rotangle);
    scaleAboutOriginImpl(obj,relscale);
    mirrorAboutOriginImpl(obj,mirchange);
end

end
