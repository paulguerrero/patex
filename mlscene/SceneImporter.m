classdef SceneImporter < handle

properties(SetAccess=protected)
%     filename = '';
    
    canvasbbox = [];
    translation = []; % translation from original coordinates to centered coordinates
    scale = []; % scaling from centered coordinates to scaled and centered coordinates
    
    % raw svg geometry
    polylines = [];
    polylinesclosed = [];
    polylineids = [];
    polylineangles = []; % if given by angle markers (circles with the special fill color #FC531F)
    polylinelabels = []; % if given by polyline fill colors
    polylinegroups = sparse([]);
    
    svgmeshstructs = [];
    svgmeshstructids = [];
    
    images = [];
    imageids = [];
    imagemin = [];
    imagemax = [];
    
    % scene elements, features and groups with their ids from the svg format
%     surfaces = SceneSurface.empty;
    surfaces = {};
    surfaceids = [];
    surfacebpolyingroup = {};
    
%     meshes = SceneMesh.empty;
%     meshplanarproxyingroup = [];
    
%     groups = SceneGroup.empty;
    groups = {};
    groupids = [];
    
%     elements = ScenePolygon.empty;
    elements = {};
    elementids = [];
    
%     feats = SceneCorner.empty;
    feats = {};
    featids = [];
    
    elmcomments = cell(1,0); % one set of comments for each loaded element
    
    settings = struct;
end

methods(Static)

function s = defaultSettings
    s = struct;
    
    s.meshidentpose = false; % give meshes an identity pose (instead of a pose defined by their center, etc.)
    s.maxerror = 0.0001;   % true to generate debug info (might use a lot of memory)
    s.flipy = true;
    s.scaleandcenter = true; % scale and center the scene
    s.createpolygonsets = false;
    s.flipobjy = false;
    s.flipobjxy = false;
    s.flipobjyz = false;
    s.flipyz = false;
    s.rotytoz = false;
    s.rotztoy = false;
    s.format = '';
end

end

methods
    
% obj = SceneImporter()
% obj = SceneImporter(settings)
% obj = SceneImporter(nvpairs)
function obj = SceneImporter(varargin)
    if numel(varargin) == 0 || isempty(varargin{1})
        obj.settings = SceneImporter.defaultSettings;
    elseif numel(varargin) == 1 && isstruct(varargin{1})
        obj.settings = varargin{1};
    elseif ischar(varargin{1})
        obj.settings = SceneImporter.defaultSettings;
        obj.settings = nvpairs2struct(varargin,obj.settings);
    else
        error('Invalid arguments.');
    end
end

function clear(obj)
    obj.canvasbbox = [];
    
    obj.polylines = [];
    obj.polylinesclosed = [];
    obj.polylineids = [];
    obj.polylineangles = [];
    obj.polylinelabels = [];
    
    obj.svgmeshstructs = [];
    obj.svgmeshstructids = [];
    
    obj.images = [];
    obj.imageids = [];
    obj.imagemin = [];
    obj.imagemax = [];
    
    obj.surfaces = {};
    obj.surfaceids = [];
    obj.surfacebpolyingroup = {};
    
    obj.groups = {};
    obj.groupids = [];
    
    obj.elements = {};
    obj.elementids = [];
    
    obj.feats = {};
    obj.featids = [];
    
    obj.elmcomments = cell(1,0);
end

function scene = import(obj,filename,varargin)
    
    if not(isempty(varargin)) && isstruct(varargin{1})
        obj.settings = varargin{1};
    elseif not(isempty(varargin))
        obj.settings = nvpairs2struct(varargin,obj.settings);
    else
        % do nothing
    end

    if isempty(obj.settings.format)
        if not(ischar(filename))
            error('Cannot determine format from file extension, must specify format.');
        end
        
        [~,~,ext] = fileparts(filename);
        
        if strcmp(ext,'.svg')
            obj.settings.format = 'svg';
        elseif strcmp(ext,'.obj')
            obj.settings.format = 'obj';
        elseif strcmp(ext,'.off')
            obj.settings.format = 'off';
        elseif strcmp(ext,'.dae')
            obj.settings.format = 'collada';
        else
            error('Unkown file extension for importing scene.');
        end
    end

    if strcmp(obj.settings.format,'svg')
        scene = obj.readsvgscene(filename);
    elseif strcmp(obj.settings.format,'obj')
        scene = obj.readobjscene(filename);
    elseif strcmp(obj.settings.format,'off')
        scene = obj.readoffscene(filename);
    elseif strcmp(obj.settings.format,'collada')
        scene = obj.readcolladascene(filename);
    else
        error('Unrecognized format for importing scene.');
    end

end

function scene = readcolladascene(obj,filename)
    
    if not(exist(filename,'file') == 2)
        error(['file ''',filename,''' not found']);
    end
    
    obj.clear;
    
    % should be renamed to readMlsceneCollada (is not Open3D specific) and
    % o3dcollada should be mlscenecollada and should be a separate module
    [meshstructs,materialstructs,nodestructs] = readO3dCollada_mex(filename);
    
    scene = obj.colladastructs2scene(meshstructs,materialstructs,nodestructs);
end

function scene = colladastructs2scene(obj,meshstructs,materialstructs,nodestructs) %#ok<INUSL>
    
    scene = Scene;
    
    % find childnodes of each node
    childnodeinds = cell(1,numel(nodestructs));
    rootnodeinds = zeros(1,0);
    for i=1:numel(nodestructs)
        if not(isempty(nodestructs{i}.parentind))
            childnodeinds{nodestructs{i}.parentind}(end+1) = i;
        else
            rootnodeinds(end+1) = i; %#ok<AGROW>
        end
    end
    
    nodesgroupinds = zeros(1,numel(nodestructs));
    sgroups = SceneGroup.empty(1,0);
    
    nodequeue = rootnodeinds;
    
    allmeshes = cell(1,numel(meshstructs));
    
    while not(isempty(nodequeue))
        nodeind = nodequeue(1);
        nodequeue(1) = [];
        nodestruct = nodestructs{nodeind};
        
        % create group
        if not(isempty(nodestruct.surfind)) && all(nodestruct.surfind > 0)
            % create surface group
            if size(nodestruct.surfind) ~= 2
                error('Invalid node parent surface indices');
            end
            if numel(allmeshes{nodestruct.surfind(1)}) ~= 1
                error('Invalid node parent surface reference, seems to point to a surface that is not in one of the nodes'' ancestors.');
            end
            mesh = allmeshes{nodestruct.surfind(1)};
            surface = mesh.surfaces(nodestruct.surfind(2));
            
            sgroup = SceneSurfaceGroup(surface);
        elseif isempty(nodestruct.parentind)
            % create root group
            sgroup = SceneGroup(scene);
        else
            % create non-surface child group
            parentsgroupind = nodesgroupinds(nodestruct.parentind);
            if parentsgroupind == 0
                error('Node parent has not been created yet.');
            end
            sgroup = SceneGroup(sgroups(parentsgroupind));
            [originpose,err] = mat2pose(nodestruct.transform);
            if err > 0.00001
                warning('Some node transformations contain shearing, cannot represent shearing with poses, using the similar pose without shearing.');
            end
            sgroup.setOriginpose(originpose);
        end
        sgroup.setName(nodestruct.name);
        sgroups(end+1) = sgroup; %#ok<AGROW>
        nodesgroupinds(nodeind) = numel(sgroups);
        
        meshes = SceneMesh.empty(1,0);
        for j=1:numel(nodestruct.meshinds)
            meshstruct = meshstructs{nodestruct.meshinds(j)};
            
            % can only have either all face vnormals or none
            if any(meshstruct.facevnormalinds == 0)
                meshstruct.facevnormalinds = [];
                meshstruct.vnormals = [];
            end
            % can only have either all face texcoords or none
            if any(meshstruct.facetexcoordinds == 0)
                meshstruct.facetexcoordinds = [];
                meshstruct.texcoords = [];
            end
            
            mesh = SceneMesh(...
                meshstruct.verts,meshstruct.faces,...
                meshstruct.vnormals,meshstruct.facevnormalinds,...
                meshstruct.texcoords,meshstruct.facetexcoordinds);
            if obj.settings.meshidentpose
                mesh.definePosition([0;0;0]);
                mesh.defineOrientation([1;0;0;0]);
                mesh.defineSize([1;1;1]);
                mesh.defineMirrored(false);
            end
            
            mesh.setName(meshstruct.name);
            
            if nodestruct.materialinds(j) >= 1
                materialstruct = materialstructs{nodestruct.materialinds(j)};
                
                material = SceneMaterial(materialstruct.name);
                
                material.ambient = materialstruct.ambient;
                material.diffuse = materialstruct.diffuse;
                material.specular = materialstruct.specular;
                material.transmittance = materialstruct.transmittance;
                material.emission = materialstruct.emission;
                material.shininess = materialstruct.shininess;
                material.ior = materialstruct.ior;
                
                material.ambient_texname = materialstruct.ambient_texname;
                material.diffuse_texname = materialstruct.diffuse_texname;
                material.specular_texname = materialstruct.specular_texname;
                material.normal_texname = materialstruct.normal_texname;
                
                mesh.setMaterials(material,ones(1,size(mesh.faces,2)));
            end
            
            for s=1:numel(meshstruct.surffaceinds)
                surf = mesh.addSurfaces(meshstruct.surffaceinds{s},meshstruct.surfvparams{s});
                surf.setName(meshstruct.surfnames{s});
            end
            
            allmeshes{nodestruct.meshinds(j)} = mesh;
            
            meshes(end+1) = mesh; %#ok<AGROW>
        end
        
        sgroup.addElements(meshes);
        
        nodequeue = [nodequeue,childnodeinds{nodeind}]; %#ok<AGROW>
    end
end

function scene = readoffscene(obj,filename)
    
    obj.clear;
    
    if iscell(filename)
        % multiple filenames
        filenames = filename;
    elseif exist(filename,'file') == 2
        % a single file
        filenames = {filename};
    elseif exist(filename,'dir') == 7
        % all files in the directory
        filedir = filename;
        filenames = dir(filedir);
        filenames([filenames.isdir]) = [];
        filenames = {filenames.name};        
        filenames = fullfile(repmat({filedir},1,numel(filenames)),filenames);
    else
        error(['Invalid filename: ''',filename,'''.']);
    end
    
    % read meshes
    meshes = SceneMesh.empty(1,0);
    comments = cell(1,numel(filenames));
    for i=1:numel(filenames)
        if not(exist(filenames{i},'file') == 2)
            error(['Cannot find file ''',filenames{i},'''.']);
        end
        
        [verts,faces,comments{i}] = readoffmesh(filenames{i},'triangulate',true);
        
        if obj.settings.rotytoz
            % rotate y to z
            verts = [1,0,0; 0,0,-1; 0,1,0] * verts;
        end
        
        [~,meshname,meshext] = fileparts(filenames{i});
        
        meshes(end+1) = SceneMesh(verts,faces); %#ok<AGROW>
        if obj.settings.meshidentpose
            meshes(end).definePosition([0;0;0]);
            meshes(end).defineOrientation([1;0;0;0]);
            meshes(end).defineSize([1;1;1]);
            meshes(end).defineMirrored(false);
        end
        meshes(end).setName([meshname,meshext]);
    end
    
    % add surfaces from comments to meshes
    if not(isempty(comments))
        obj.elmcomments = cell(1,numel(meshes));
        for i=1:numel(meshes)
            if not(isempty(comments{i}))
                obj.elmcomments{i} = comments{i};
                
                commentstr = strtrim(comments{i});
                labelstr = commentstr(strncmp('_label_ ',commentstr,8));
                if isempty(labelstr)
                    meshes(i).setLabels(cell(1,0));
                else
                    if numel(labelstr) > 1
                        warning('Object with multiple label definitions found, using the last one.');
                    end
                    labels = strsplit(strtrim(labelstr{end}(9:end)));
                    labels(cellisempty_mex(labels)) = [];
                    meshes(i).setLabels(labels);
                end
                
                surfstr = commentstr(strncmp('_surface_ ',commentstr,10));
                for j=1:numel(surfstr)
                    surfstr{j} = surfstr{j}(10:end);
                    [faceinds,~,~,nextind] = sscanf(surfstr{j},'%lu');
                    faceinds = faceinds';
                    if not(isempty(faceinds))
                        if any(faceinds < 0 | faceinds > size(meshes(i).faces,2))
                            error('Invalid surface definition: face indices out of bounds.');
                        end
                        meshes(i).addSurfaces(faceinds);
                        
                        surfstr{j} = strtrim(surfstr{j}(nextind:end));
                        if strncmp('_vparams_ ',surfstr{j},10)
                            surfstr{j} = surfstr{j}(10:end);
                            vparams = sscanf(surfstr{j},'%f');
                            if mod(numel(vparams),2) ~= 0
                                error('Invalid number of vertex parameters.');
                            end
                            vparams = reshape(vparams,2,[]);
                            meshes(i).surfaces(end).setVparams(vparams);
                        end
                    end
                end
                
            end
        end
    end
    
    scene = Scene;
    scenegroup = SceneGroup(scene);
    scenegroup.addElements(meshes);
end

function scene = readobjscene(obj,filename)
    
    if not(exist(filename,'file') == 2)
        error(['file ''',filename,''' not found']);
    end
    
    obj.clear;
    
    scene = Scene;
%     scene.catattributes = struct('walkable',{},'isroom',{});
    
    elms = ScenePolygon.empty;
    
    [mtlbasepath,~,~] = fileparts(filename);
    if not(isempty(mtlbasepath))
        mtlbasepath = [mtlbasepath,filesep];
    end
    
    [verts,vnormals,texcoords,...
     faces,facevnormalinds,facetexcoordinds,...
     meshnames,smoothgroups,matgroups,matnames] = ...
        readobjmesh_mex(filename,mtlbasepath);
    
    if obj.settings.flipobjyz
        for i=1:numel(verts)
            verts{i} = verts{i}([1,3,2],:);
        end
        for i=1:numel(vnormals)
            vnormals{i} = vnormals{i}([1,3,2],:);
        end
    end
    if obj.settings.flipobjxy
        for i=1:numel(verts)
            verts{i} = verts{i}([2,1,3],:);
        end
        for i=1:numel(vnormals)
            vnormals{i} = vnormals{i}([2,1,3],:);
        end
    end
    if obj.settings.flipobjy
        for i=1:numel(verts)
            verts{i}(2,:) = -verts{i}(2,:);
        end
        for i=1:numel(vnormals)
            vnormals{i}(2,:) = -vnormals{i}(2,:);
        end
    end
    if obj.settings.rotytoz
        for i=1:numel(verts)
            verts{i} = [1,0,0; 0,0,-1; 0,1,0] * verts{i};
        end
        for i=1:numel(vnormals)
            vnormals{i} = [1,0,0; 0,0,-1; 0,1,0] * vnormals{i};
        end
    end
    if obj.settings.rotztoy
        for i=1:numel(verts)
            verts{i} = [1,0,0; 0,0,-1; 0,1,0]' * verts{i};
        end
        for i=1:numel(vnormals)
            vnormals{i} = [1,0,0; 0,0,-1; 0,1,0]' * vnormals{i};
        end
    end
    for i=1:numel(verts)
        if isempty(meshnames{i})
            labels = meshnames(i);
        else
            labels = cell(1,0);
        end
        elms(end+1) = SceneMesh(verts{i},faces{i},...
            vnormals{i},facevnormalinds{i},...
            texcoords{i},facetexcoordinds{i},...
            labels); %#ok<AGROW>
        if obj.settings.meshidentpose
            elms(end).definePosition([0;0;0]);
            elms(end).defineOrientation([1;0;0;0]);
            elms(end).defineSize([1;1;1]);
            elms(end).defineMirrored(false);
        end
        elms(end).setName(meshnames{i});
%         elms(end).setTexcoords(texcoords{i},facetexcoordinds{i});
        materials = SceneMaterial.empty;
        for j=1:numel(matnames{i})
            materials(:,j) = SceneMaterial(matnames{i}{j});
        end
        elms(end).setMaterials(materials,matgroups{i});
        elms(end).setSmoothgroups(smoothgroups{i});
    end
    
    sgroup = SceneGroup(scene);
    sgroup.addElements(elms);
%     scene.rootgroups(end+1) = SceneGroup(scene);
%     scene.rootgroups(end).addElements(elms);
end

function scene = readsvgscene(obj,filename)

    if not(exist(filename,'file') == 2)
        error(['file ''',filename,''' not found']);
    end
    
    obj.clear;
    
    % replace dtd file on the web with local ones
    fp = fopen(filename,'rt');
    filestr = fread(fp,inf,'*char')';
    fclose(fp);
    filestr = regexprep(filestr, '<!DOCTYPE [^>]*>','','once','ignorecase');

    xmldoc = xmlreadstring(filestr);
    svgnodelist = getDOMChildElements(xmldoc,'svg');
    if isempty(svgnodelist)
        error('The xml document does not appear to be an svg file.');
    end
    svgnode = svgnodelist(end);
    
    scene = obj.readSceneSvgNode(svgnode);
end
    
function scene = readSceneSvgNode(obj,node)
        
    obj.readSvgNode(node);
    
    % get scene node (if available)
    fpnodelist = getDOMChildElements(node,'scene');
    if not(isempty(fpnodelist))
        % scene node found
        scenenode = fpnodelist(end);
    else
        % scene node not found - check if scene contents are
        % direct childs of the svg node (without a scene node)
        catnodelist = getDOMChildElements(node,{'categories'});
        if not(isempty(catnodelist));
            scenenode = node;
        else
            scenenode = [];
        end
    end
    
    if isempty(scenenode)
        % no scene found, create new scene with polylines as
        % elements and images as texture layers
        [scene,obj.translation,obj.scale] = SceneImporter.createScene(...
            obj.polylines,obj.polylinesclosed,obj.polylineangles,obj.polylinelabels,obj.polylinegroups,...
            obj.images,obj.imagemin,obj.imagemax,obj.svgmeshstructs,...
            obj.settings.scaleandcenter,obj.settings.createpolygonsets);
    else
        scene = obj.readSceneNode(scenenode,[]);
    end
end

function readSvgNode(obj,node)
    % read svg part of the scene
    [obj.polylines,obj.polylinesclosed,obj.polylineids,...
     obj.polylineangles,obj.polylinelabels,obj.polylinegroups,...
     canvasmin,canvasmax] = ...
        SceneImporter.readsvgscenepolylines(node,...
        obj.settings.maxerror,obj.settings.flipy);

    obj.canvasbbox = [canvasmin,canvasmax];
    [obj.images,obj.imageids,obj.imagemin,obj.imagemax] = ...
        SceneImporter.readsvgsceneimages(node,...
        obj.canvasbbox(:,1),obj.canvasbbox(:,2),obj.settings.flipy);
    
    [obj.svgmeshstructs,obj.svgmeshstructids] = ...
        SceneImporter.readsvgscenemeshes(node);
end

function scene = readSceneNode(obj,node,scene)
    
    if isempty(scene)
        scene = Scene;
%         scene.catattributes = struct('walkable',{},'isroom',{});
    end
    
    if node.hasAttribute('name')
        scene.setName(char(node.getAttribute('name')));
    end
    
    % read categories
    c = 1;
    catnodelists = getDOMChildElements(node,'categories');
    if not(isempty(catnodelists))
        catnodes = getDOMChildElements(catnodelists(end));
        for j=1:numel(catnodes)
            catNode = catnodes(j);
            if not(catNode.hasAttributes) || ...
               not(catNode.getNodeType() == catNode.ELEMENT_NODE)
                continue;
            end
            scene.categories{c} = char(catNode.getAttribute('name'));
%             if catNode.hasAttribute('walkable')
%                 scene.catattributes(c).walkable = sscanf(char(catNode.getAttribute('walkable')),'%f');
%             else
%                 scene.catattributes(c).walkable = 1;
%             end
%             if catNode.hasAttribute('isroom')
%                 scene.catattributes(c).isroom = sscanf(char(catNode.getAttribute('isroom')),'%f');
%             else
%                 scene.catattributes(c).isroom = 0;
%             end
            c = c + 1;
        end
    end
    
    % read groups
    directchilds = getDOMChildElements(node,{'elements','objects','texturelayers'});
    if not(isempty(directchilds))
        % read direct childs of the scene and put them into a group,
        % all other groups are childs of that group
        scene.rootgroups = obj.readSceneGroup(...
            node,[],scene);
    else
        scene.rootgroups = SceneGroup.empty;
        grpnodelists = getDOMChildElements(node,'groups');
        if not(isempty(grpnodelists))
            grpnodes = getDOMChildElements(grpnodelists(end));
            for j=1:numel(grpnodes)
                scene.rootgroups = [scene.rootgroups,obj.readSceneGroup(...
                    grpnodes(j),[],scene)];
            end
        end
    end
end

function group = readSceneGroup(obj,node,group,parentgroup) % pgroupsurfaces,pgroupsurfaceids,pgroupsurfacebpolyingroup
    
    if isempty(group)
%         group = SceneGroup.empty;
%         csStr = char(node.getAttribute('coordsys'));
        trStr = char(node.getAttribute('pose'));
        if isempty(trStr)
            % default: Cartesian coordinates with identity pose
            group = SceneGroup(parentgroup,identpose3D);
        else

            originpose = reshape(sscanf(trStr,'%f,'),[],1);
            if size(originpose,1) == 9 % legacy
                originpose = originpose([1:7,8,8,8,9],:);
            end

            group = SceneGroup(parentgroup,originpose);
        end
        
        if node.hasAttribute('name')
            group.setName(char(node.getAttribute('name')));
        end
        
        if node.hasAttribute('grpid')
            grpid = sscanf(char(node.getAttribute('grpid')),'%f');
            obj.groups{end+1} = group;
            obj.groupids(end+1) = grpid;
        end
    end
    
%     obj.surfaces = SceneSurface.empty;
%     obj.groupsurfaceids = [];
%     obj.groupsurfacebpolyingroup = {};
    
    % read elements
%     grpsurfindoffset = numel(obj.surfaces);
    grpelements = ScenePoint.empty;
    elmnodelists = getDOMChildElements(node,{'elements','objects'}); % objects for compatibility with older version
    if not(isempty(elmnodelists))
        elmnodes = getDOMChildElements(elmnodelists(end));
        for j=1:numel(elmnodes)
            nodeName = elmnodes(j).getNodeName();
            if strcmp(nodeName,'poly') || strcmp(nodeName,'object') % object for compatibility with older version
                element = obj.readScenePolygon(elmnodes(j),[]);
            elseif strcmp(nodeName,'pline') || strcmp(nodeName,'curve') % curve for compatibility with older version
                element = obj.readScenePolyline(elmnodes(j),[]);
            elseif strcmp(nodeName,'point')
                element = obj.readScenePoint(elmnodes(j),[]);
            elseif strcmp(nodeName,'mesh')
                [element,planarproxyingroup] = obj.readSceneMesh(elmnodes(j),[]);
                element = [element,element.planarproxy(planarproxyingroup)]; %#ok<AGROW>
            else
                warning('Unkown element in scene element list, skipping.');
                continue;
            end
            
            grpelements(end+1:end+numel(element)) = element;
        end
    end
    group.addElements(grpelements,false,false,false);
%     grpsurfinds = grpsurfindoffset+1:numel(obj.surfaces);
    
    % read texture layers
    texlayers = zeros(0,0,0,0);
    texmin = zeros(2,0);
    texmax = zeros(2,0);
    tlaynodelists = getDOMChildElements(node,'texturelayers');
    if not(isempty(tlaynodelists))
        tlaynodes = getDOMChildElements(tlaynodelists(end));
        for j=1:numel(tlaynodes)
        
            [texlayer,texlayermin,texlayermax] = obj.readSceneTexlayer(tlaynodes(j));

            if not(isempty(texlayer))
                if not(isempty(texlayers))
                    res = size(texlayers);
                    if any(res ~= size(texlayer))
                        error('Format of texture layers does not match.');
                    end
                    if any(texmin ~= texlayermin) || any(texmax ~= texlayermax)
                        error('Position of texture layers does not match.');
                    end
                end

                texlayers(:,:,:,end+1) = texlayer; %#ok<AGROW>
                texmin(:,end+1) = texlayermin; %#ok<AGROW>
                texmax(:,end+1) = texlayermax; %#ok<AGROW>
            end
        end
    end
    if not(isempty(texlayers))
        group.addTexlayer(texlayers,texmin,texmax);
    end
    
    % read child groups
    childgroups = SceneGroup.empty;
    grpnodelists = getDOMChildElements(node,'groups');
    if not(isempty(grpnodelists))
        grpnodes = getDOMChildElements(grpnodelists(end));
        for j=1:numel(grpnodes)
            childgroups = [childgroups,obj.readSceneGroup(...
                grpnodes(j),[],group)]; %#ok<AGROW>
%                 obj.surfaces(grpsurfinds),...
%                 obj.surfaceids(grpsurfinds),...
%                 obj.surfacebpolyingroup(grpsurfinds)
        end
    end
    group.childgroups = childgroups;
end

function [texlayer,texmin,texmax] = readSceneTexlayer(obj,node)
%     texlayer = [];
%     texmin = [];
%     texmax = [];
    
    id = sscanf(char(node.getAttribute('id')),'%f');
    imageind = find(obj.imageids==id,1);
    if isempty(imageind)
        error('Cannot find texture layer.');
    end
    texlayer = obj.images{imageind};
    texmin = obj.imagemin(:,imageind);
    texmax = obj.imagemax(:,imageind);
end

function feat = readSceneFeature(obj,node,feat,parentelement)
    
    if nargin < 4
        parentelement = [];
    end
    
    nodeName = node.getNodeName();
    
    if strcmp(nodeName,'poly') || strcmp(nodeName,'object') % object for compatibility with older version
        feat = obj.readScenePolygon(node,feat);
    elseif strcmp(nodeName,'pline') || strcmp(nodeName,'curve') % curve for compatibility with older version
        feat = obj.readScenePolyline(node,feat);
    elseif strcmp(nodeName,'point')
        feat = obj.readScenePoint(node,feat);
    elseif strcmp(nodeName,'mesh')
        feat = obj.readSceneMesh(node,feat);
    elseif strcmp(nodeName,'corner')
        feat = obj.readCorner(node,feat,parentelement);
    elseif strcmp(nodeName,'edge')
        feat = obj.readEdge(node,feat,parentelement);
    elseif strcmp(nodeName,'segment')
        feat = obj.readSegment(node,feat,parentelement);
    elseif strcmp(nodeName,'surface')
        feat = obj.readSurface(node,feat,parentelement);
    else
        warning('Unknown scene feature type, skipping.');
    end
end

function elm = readSceneElementBase(obj,node,elm) %#ok<INUSL>
    if node.hasAttribute('name')
        elm.setName(char(node.getAttribute('name')));
    end
    if node.hasAttribute('position')
        attrStr = char(node.getAttribute('position'));
        elm.position = reshape(sscanf(attrStr,'%f,'),[],1);    
    elseif node.hasAttribute('centroid') % legacy
        attrStr = char(node.getAttribute('centroid'));
        elm.position = reshape(sscanf(attrStr,'%f,'),[],1);
    end
    if node.hasAttribute('orientation')
        attrStr = char(node.getAttribute('angle'));
        elm.orientation = reshape(sscanf(attrStr,'%f,'),[],1);
    elseif node.hasAttribute('angle') % legacy
        attrStr = char(node.getAttribute('angle'));
        elm.orientation = reshape(sscanf(attrStr,'%f,'),[],1);
    end
    if node.hasAttribute('size')
        attrStr = char(node.getAttribute('size'));
        elm.size = reshape(sscanf(attrStr,'%f,'),[],1);
        
        if numel(elm.size) == 1 % legacy
%             elm.size = repmat(elm.size,numel(elm.p_centroid),1);
            % re-compute the size
            defpose = elm.defaultPose;
            if size(defpose,1) == 11
                elm.size = defpose(8:10,:);
            elseif size(defpose,1) == 6
                elm.size = defpose(4:5,:);
            else
                warning('Invalid pose read for element, this should not happen.');
            end
        end
%         sscanf(char(node.getAttribute('angle')),'%f');
    end
    if node.hasAttribute('mirrored')
        attrStr = char(node.getAttribute('mirrored'));
        elm.mirrored = logical(reshape(sscanf(attrStr,'%f,'),[],1));
%         sscanf(char(node.getAttribute('angle')),'%f');
    end
end

function poly = readScenePolygon(obj,node,poly)
%     hasverts = false;
%     if node.hasAttribute('p')
%         pidStr = char(node.getAttribute('p'));
%     elseif node.hasAttribute('v')
%         pidStr = char(node.getAttribute('v'));
%         hasverts = true;
%     else
%         error('Polygon has neither polylines nor vertices.');
%     end
    
%     pids = reshape(sscanf(pidStr,'%f,'),1,[]);
%     if isempty(pids) || any(pids < 0) || numel(pids) ~= numel(unique(pids))
%         error('Invalid polyline id in a polygon.');
%     end
%     pinds = find(ismember(polylineids,pids));
%     if numel(pinds) ~= numel(pids)
%         error('A polygon could not find all of its polylines.');
%     end
    
    if isempty(poly)    
        labels = {char(node.getAttribute('label'))}; % only a single label for now
    
        if node.hasAttribute('vertids')
            pidStr = char(node.getAttribute('vertids'));
            pids = reshape(sscanf(pidStr,'%f,'),1,[]);
            if numel(pids) ~= 1 || any(pids < 0)
                error('Invalid polyline id in a polygon.');
                % need exactly one polyline for the polygon (holes not
                % supported)
            end
            pinds = find(ismember(obj.polylineids,pids));
            if numel(pinds) ~= numel(pids)
                error('A polygon could not find all of its polylines.');
            end
            
            poly = ScenePolygon(obj.polylines{pinds},labels);
        elseif node.hasAttribute('p') % legacy: re-create polygon from visual representation
            pidStr = char(node.getAttribute('p'));
            pids = reshape(sscanf(pidStr,'%f,'),1,[]);
            if isempty(pids) || any(pids < 0) || numel(pids) ~= numel(unique(pids))
                error('Invalid polyline id in a polygon.');
            end
            pinds = find(ismember(obj.polylineids,pids));
            if numel(pinds) ~= numel(pids)
                error('A polygon could not find all of its polylines.');
            end

            poly = ScenePolygon(obj.polylines(pinds),labels);
        else
            error('A polygon could not find its vertices or polylines.');
        end
        
        if node.hasAttribute('featid')
            featid = sscanf(char(node.getAttribute('featid')),'%f');
            obj.feats{end+1} = poly;
            obj.featids(end+1) = featid;
        end
        if node.hasAttribute('elmid')
            elmid = sscanf(char(node.getAttribute('elmid')),'%f');
            obj.elements{end+1} = poly;
            obj.elementids(end+1) = elmid;
        end
    end
    
    obj.readScenePolyline(node,poly);
    
%     if node.hasAttribute('visrep')
%         pidStr = char(node.getAttribute('visrep'));
%         pids = reshape(sscanf(pidStr,'%f,'),1,[]);
%         if isempty(pids) || any(pids < 0) || numel(pids) ~= numel(unique(pids))
%             error('Invalid visual representation id in a polygon.');
%         end
%         pinds = find(ismember(obj.polylineids,pids));
%         if numel(pinds) ~= numel(pids)
%             error('A polygon could not find all of its visual representation polylines.');
%         end
%     
%         poly.p_visrep = obj.polylines(pinds);
%         poly.p_visrepclosed = obj.polylinesclosed(pinds);
%     elseif node.hasAttribute('polylineids') % legacy
%         pidStr = char(node.getAttribute('polylineids'));
%         pids = reshape(sscanf(pidStr,'%f,'),1,[]);
%         if isempty(pids) || any(pids < 0) || numel(pids) ~= numel(unique(pids))
%             error('Invalid visual representation id in a polygon.');
%         end
%         pinds = find(ismember(obj.polylineids,pids));
%         if numel(pinds) ~= numel(pids)
%             error('A polygon could not find all of its visual representation polylines.');
%         end
%     
%         poly.p_visrep = obj.polylines(pinds);
%         poly.p_visrepclosed = obj.polylinesclosed(pinds);
%     end
    
%     if hasverts
%         if numel(pinds) ~= 1
%             error('A polygon has multiple polylines as vertices.');
%         end
%         poly = ScenePolygon(polylines{pinds},label);
%     else
%         poly = ScenePolygon(polylines(pinds),label);
%     end
    
%     if node.hasAttribute('centroid')
%         ctrStr = char(node.getAttribute('centroid'));
%         poly.p_centroid = reshape(sscanf(ctrStr,'%f,'),[],1);
%         poly.p_centroid = poly.p_centroid(1:2);
%     end
%     if node.hasAttribute('angle')
%         poly.p_angle = sscanf(char(node.getAttribute('angle')),'%f');
%     end
%     skelnodes = getDOMChildElements(node,'skeleton');
%     if not(isempty(skelnodes))
%         if numel(skelnodes) > 1
%             warning('Multiple skeletons for a polygon detected, only using last one.');
%         end
%         poly.p_skeleton = obj.readPolygonSkeleton(skelnodes(end),[]);
%     end
%     if node.hasAttribute('freeareabound')
%         faStr = char(node.getAttribute('freeareabound'));
%         poly.p_freeareabounds = reshape(sscanf(faStr,'%f,'),2,[]);
%     end
%     if node.hasAttribute('offsetpaths') && node.hasAttribute('offsetpathorigin')
%         opStr = char(node.getAttribute('offsetpaths'));
%         poly.p_insetpaths = reshape(sscanf(opStr,'%f,'),6,[]);
%         opoStr = char(node.getAttribute('offsetpathorigin'));
%         poly.p_insetpathorigin = reshape(sscanf(opoStr,'%f,'),3,[]);
%     end
%     cells = {};
%     cellnodes = getDOMChildElements(node,'cell');
%     for i=1:numel(cellnodes)
%         if not(cellnodes(i).hasAttributes)
%             continue;
%         end
%         cells{end+1} = sscanf(char(cellnodes(i).getAttribute('inds')),'%f,')'; %#ok<AGROW>
%     end
%     if not(isempty(cells)) && node.hasAttribute('cellverts')
%         cellverts = reshape(sscanf(char(node.getAttribute('cellverts')),'%f,'),2,[]);
%         poly.p_cellgraph = CellGraph(cells,cellverts);
%     end
end

function skel = readPolygonSkeleton(obj,node,skel) %#ok<INUSL>
    if isempty(skel)
        skel = PolygonSkeleton;
    end
    
    skel.verts = reshape(sscanf(char(node.getAttribute('verts')),'%f,'),2,[]);
    skel.edges = reshape(sscanf(char(node.getAttribute('edges')),'%f,'),2,[]);
    skel.verttime = sscanf(char(node.getAttribute('verttime')),'%f,')';

    skel.boundaryvertinds = sscanf(char(node.getAttribute('boundaryvertinds')),'%f,')';
    skel.prunededgemask = logical(sscanf(char(node.getAttribute('prunededgemask')),'%f,')');
end

function pline = readScenePolyline(obj,node,pline)
    if isempty(pline)
        labels = {char(node.getAttribute('label'))}; % only a single label for now
        
        if node.hasAttribute('vertids')
            pid = sscanf(char(node.getAttribute('vertids')),'%f,');
            if isempty(pid) || pid < 0
                error('Invalid polyline id in a polyline.');
            end
            pind = find(obj.polylineids == pid,1,'first');
            if isempty(pind)
                error('A polyline could not find its polyline.');
            end
            
            pline = ScenePolyline(obj.polylines{pind},labels,obj.polylinesclosed(pind));
        elseif node.hasAttribute('p') % legacy
            pid = sscanf(char(node.getAttribute('p')),'%f,');
            if isempty(pid) || pid < 0
                error('Invalid polyline id in a polyline.');
            end
            pind = find(obj.polylineids == pid,1,'first');
            if isempty(pind)
                error('A polyline could not find its polyline.');
            end
            
            pline = ScenePolyline(obj.polylines{pind},labels);
        else
            error('No vertices found for the scene polyline.');
        end

        if node.hasAttribute('featid')
            featid = sscanf(char(node.getAttribute('featid')),'%f');
            obj.feats{end+1} = pline;
            obj.featids(end+1) = featid;
        end
        if node.hasAttribute('elmid')
            elmid = sscanf(char(node.getAttribute('elmid')),'%f');
            obj.elements{end+1} = pline;
            obj.elementids(end+1) = elmid;
        end
    end
    
    obj.readSceneElementBase(node,pline);
    
    cornernodes = getDOMChildElements(node,'corner');
    pline.p_corners = SceneCorner.empty;
    for i=1:numel(cornernodes)
        pline.p_corners(end+1) = obj.readCorner(cornernodes(i),[],pline);
    end
    edgenodes = getDOMChildElements(node,'edge');
    pline.p_edges = SceneEdge.empty;
    for i=1:numel(edgenodes)
        pline.p_edges(end+1) = obj.readEdge(edgenodes(i),[],pline);
    end
    segmentnodes = getDOMChildElements(node,'segment');
    pline.segments = SceneBoundarySegment.empty;
    for i=1:numel(segmentnodes)
        pline.addSegments(obj.readSegment(segmentnodes(i),[],pline));
    end
end

function point = readScenePoint(obj,node,point)
    if isempty(point)
        pid = sscanf(char(node.getAttribute('p')),'%f,');
        if isempty(pid) || pid < 0
            error('Invalid polyline id in a point.');
        end
        pind = find(obj.polylineids == pid,1,'first');
        if isempty(pind)
            error('A point could not find its polyline.');
        end
        labels = {char(node.getAttribute('label'))}; % only a single label for now
%         angle = char(node.getAttribute('angle'));

        if size(obj.polylines{pind},2) ~= 1
            error('Invalid geometry for scene point, geometry must be a point.');
        end
        point = ScenePoint(obj.polylines{pind},angle,labels);
        
        if node.hasAttribute('featid')
            featid = sscanf(char(node.getAttribute('featid')),'%f');
            obj.feats{end+1} = point;
            obj.featids(end+1) = featid;
        end
        if node.hasAttribute('elmid')
            elmid = sscanf(char(node.getAttribute('elmid')),'%f');
            obj.elements{end+1} = point;
            obj.elementids(end+1) = elmid;
        end
    end
    
    obj.readSceneElementBase(node,pline);
end

function [mesh,planarproxyingroup] = readSceneMesh(obj,node,mesh)
    if isempty(mesh)
        pid = sscanf(char(node.getAttribute('p')),'%f,');
        if isempty(pid) || pid < 0
            error('Invalid meshstruct id in a mesh.');
        end
        pind = find(obj.svgmeshstructids == pid,1,'first');
        if isempty(pind)
            error('A mesh could not find its meshstruct.');
        end
        labels = {char(node.getAttribute('label'))}; % only a single label for now

        % temp
        wasconverted = false;
        meshstruct = obj.svgmeshstructs{pind};
        
%         if obj.doconvert
%             surfnodes = getDOMChildElements(node,'surface');
%             if isempty(surfnodes) || (numel(surfnodes) == 1 && strcmp(label,'balcony'))
%                 [meshstruct,wasconverted] = obj.tempConvertMeshstruct(obj.svgmeshstructs(pind));
%                 meshstruct = meshstruct{1};
%             end
%         end
        
        mesh = SceneMesh(...
            meshstruct.v',...
            meshstruct.f',...
            labels);
        if obj.settings.meshidentpose
            mesh.definePosition([0;0;0]);
            mesh.defineOrientation([1;0;0;0]);
            mesh.defineSize([1;1;1]);
            mesh.defineMirrored(false);
        end
        
        % temp
        if wasconverted
            obj.convertedmeshes(:,end+1) = mesh;
        else
            obj.notconvertedmeshes(:,end+1) = mesh;
        end
        
        if isfield(meshstruct,'name')
            mesh.setName(meshstruct.name);
        else
            meshlabels = mesh.labelStrs;
            if not(isempty(meshlabels))
                mesh.setName(['unnamed_',meshlabels{1}]);
            else
                mesh.setName('unnamed_');
            end
        end
        
        if isfield(meshstruct,'n')
            if not(isfield(meshstruct,'fvn')); % legacy
                mesh.setVnormals(meshstruct.n');
            else
                mesh.setVnormals(meshstruct.n',meshstruct.fvn);
            end
        end
        if isfield(meshstruct,'u')
            if not(isfield(meshstruct,'ftc')); % legacy
                mesh.setTexcoords(meshstruct.u');
            else
                mesh.setTexcoords(meshstruct.u',meshstruct.ftc);
            end
        end
        
        if isfield(meshstruct,'fmg')
            % create materials
            mats = SceneMaterial.empty(1,0);
            for j=1:numel(meshstruct.matnames)
                mats(:,end+1) = SceneMaterial(meshstruct.matnames{j}); %#ok<AGROW>
            end
            mesh.setMaterials(mats,meshstruct.fmg);
        end
        
        if isfield(meshstruct,'fsg')
            mesh.setSmoothgroups(meshstruct.fsg);
        end
        
        mesh.p_fnormals = meshstruct.fn';
        if isfield(meshstruct,'fa')
            mesh.p_fareas = meshstruct.fa;
        end
        mesh.p_edges = meshstruct.e;
        mesh.p_edgefaces = meshstruct.te;
        mesh.p_valence = meshstruct.valence;
        if isfield(meshstruct,'isboundarye')
            mesh.p_isboundarye = meshstruct.isboundarye;
        end
        mesh.p_isboundaryv = meshstruct.isboundaryv;
        mesh.p_isboundaryf = meshstruct.isboundaryf;
        
        if node.hasAttribute('featid')
            featid = sscanf(char(node.getAttribute('featid')),'%f');
            obj.feats{end+1} = mesh;
            obj.featids(end+1) = featid;
        end
        if node.hasAttribute('elmid')
            elmid = sscanf(char(node.getAttribute('elmid')),'%f');
            obj.elements{end+1} = mesh;
            obj.elementids(end+1) = elmid;
        end
        
%         obj.meshes(end+1) = mesh;
%         obj.meshplanarproxyingroup(end+1) = false;
    end
    
    obj.readSceneElementBase(node,mesh);
        
    surfacenodes = getDOMChildElements(node,'surface');
    for i=1:numel(surfacenodes)
        obj.surfaces{end} = mesh.addSurfaces(obj.readSurface(surfacenodes(i),[],mesh));
    end
    
    % read planar proxys
    planarproxyingroup = [];
    boutlinelistnode = getDOMChildElements(node,'planarproxys');
    if not(isempty(boutlinelistnode)) % might not exist in older versions
        boutlinelistnode = boutlinelistnode(end);
        boutlinenodes = getDOMChildElements(boutlinelistnode,{'poly','pline','curve'}); % curve for compatibility with older version
        boutlines = ScenePolyline.empty;
        for i=1:numel(boutlinenodes)
            nodeName = boutlinenodes(i).getNodeName();
            if strcmp(nodeName,'poly')
                boutlines(end+1) = obj.readScenePolygon(boutlinenodes(i),[]); %#ok<AGROW>
            elseif strcmp(nodeName,'pline') || strcmp(nodeName,'curve')  % curve for compatibility with older version
                boutlines(end+1) = obj.readScenePolyline(boutlinenodes(i),[]); %#ok<AGROW>
            else
                error('Planar proxy node is neither polygon nor polyline, this should not happen.');
            end
            planarproxyingroup(end+1) = strcmp(char(boutlinenodes(i).getAttribute('ingroup')),'1'); %#ok<AGROW>
        end
        mesh.setPlanarproxy(boutlines);
    end
    
    
%     planarproxyingroup = false;
%     planarproxyList = node.getElementsByTagName('planarproxy');
%     if planarproxyList.getLength() > 0
%         planarproxynode = planarproxyList.item(0);
%         mesh.setPlanarproxy(obj.readScenePolyline(planarproxynode,[]))
%         planarproxyingroup = strcmp(char(planarproxynode.getAttribute('ingroup')),'1');
%     else
%         basepolyList = node.getElementsByTagName('basepoly');
%         if basepolyList.getLength() > 0
%             basepolynode = basepolyList.item(0);
%             mesh.setPlanarproxy(obj.readScenePolygon(basepolynode,[]))
%             planarproxyingroup = strcmp(char(basepolynode.getAttribute('ingroup')),'1');
%         end
%     end
end

function corner = readCorner(obj,node,corner,parentelement)
    if isempty(corner)
        vidx = sscanf(char(node.getAttribute('vidx')),'%f');
        if vidx <= 0
            warning('invalid corner indices found, skipping.');
            corner = SceneCorner.empty;
            return;
        end
        if node.hasAttribute('label');
            labels = {char(node.getAttribute('label'))}; % only a single label for now
            corner = SceneCorner(parentelement,vidx,labels);
        else
            corner = SceneCorner(parentelement,vidx);
        end
        
        if node.hasAttribute('featid')
            featid = sscanf(char(node.getAttribute('featid')),'%f');
            obj.feats{end+1} = corner;
            obj.featids(end+1) = featid;
        end
    end
end

function edge = readEdge(obj,node,edge,parentelement)
    if isempty(edge)
        startarclen = sscanf(char(node.getAttribute('startarclen')),'%f');
        endarclen = sscanf(char(node.getAttribute('endarclen')),'%f');
        if node.hasAttribute('label');
            labels = {char(node.getAttribute('label'))}; % only a single label for now
            edge = SceneEdge(parentelement,startarclen,endarclen,labels);
        else
            edge = SceneEdge(parentelement,startarclen,endarclen);
        end
        
        if node.hasAttribute('featid')
            featid = sscanf(char(node.getAttribute('featid')),'%f');
            obj.feats{end+1} = edge;
            obj.featids(end+1) = featid;
        end
    end
end

function segment = readSegment(obj,node,segment,parentelement)
    if isempty(segment)
        startarclen = sscanf(char(node.getAttribute('startarclen')),'%f');
        endarclen = sscanf(char(node.getAttribute('endarclen')),'%f');
        if node.hasAttribute('label');
            labels = {char(node.getAttribute('label'))}; % only a single label for now
            segment = SceneBoundarySegment(parentelement,startarclen,endarclen,labels);
        else
            segment = SceneBoundarySegment(parentelement,startarclen,endarclen);
        end
        
        if node.hasAttribute('featid')
            featid = sscanf(char(node.getAttribute('featid')),'%f');
            obj.feats{end+1} = segment;
            obj.featids(end+1) = featid;
        end
    end
end

function surface = readSurface(obj,node,surface,parentelement)
        
    if isempty(surface)

        faceinds = sscanf(char(node.getAttribute('faceindices')),'%f,');
        if isempty(faceinds) || any(faceinds) <= 0
            warning('invalid surfaces found, skipping.');
            surface = SceneSurface.empty;
            return;
        end
        
        vparamnodes = getDOMChildElements(node,'vertexparams');
        vparams = [];
        for j=1:numel(vparamnodes)
            vparamnode = vparamnodes(j);
            paramsStr = char(vparamnode.getAttribute('p'));
            vparams(j,:) = reshape(sscanf(paramsStr,'%f,'),1,[]); %#ok<AGROW>    
        end
        
        if node.hasAttribute('label');
            labels = {char(node.getAttribute('label'))}; % only a single label for now
            surface = SceneSurface(parentelement,faceinds,vparams,labels);
        else
            surface = SceneSurface(parentelement,faceinds,vparams);
        end
        
        % read boundary polygons
        bpolysingroup = [];
        bpolys = ScenePolygon.empty;
        bpolylistnode = getDOMChildElements(node,'boundarypolys');
        if not(isempty(bpolylistnode)) % might not exist in older versions
            bpolylistnode = bpolylistnode(end);
            bpolynodes = getDOMChildElements(bpolylistnode,'poly');
            for i=1:numel(bpolynodes)
                bpolys(end+1) = obj.readScenePolygon(bpolynodes(i),[]); %#ok<AGROW>
                bpolysingroup(end+1) = strcmp(char(bpolynodes(i).getAttribute('ingroup')),'1'); %#ok<AGROW>
            end
            surface.setBoundarypoly(bpolys);
        end
        
%         bpolyList = node.getElementsByTagName('boundarypoly');
%             bpolyList.getLength() > 0
%             for i=1:bpolyList.getLength()
%                 bpolynode = bpolyList.item(i-1);
%                 bpolys(end+1) = obj.readScenePolygon(bpolynode,[]); %#ok<AGROW>
%                 bpolysingroup(end+1) = strcmp(char(bpolynode.getAttribute('ingroup')),'1'); %#ok<AGROW>
%             end
            
%         end

        if node.hasAttribute('id') % for older version
            surfid = sscanf(char(node.getAttribute('id')),'%f');
        else
            surfid = sscanf(char(node.getAttribute('surfid')),'%f');
        end
        if not(isempty(find(obj.surfaceids == surfid,1,'first')))
            warning('Duplicate surface id detected, skipping');
            delete(surface(isvalid(surface)));
            surface = SceneSurface.empty;
            return;
        end
        obj.surfaces{end+1} = surface;
        obj.surfaceids(end+1) = surfid;
        obj.surfacebpolyingroup{end+1} = bpolysingroup;
        
        if node.hasAttribute('featid')
            featid = sscanf(char(node.getAttribute('featid')),'%f');
            obj.feats{end+1} = surface;
            obj.featids(end+1) = featid;
        end
    end
end

end

methods(Static)
    
function scene = importScene(filename,varargin)
    importer = SceneImporter;
    scene = importer.import(filename,varargin{:});
    delete(importer(isvalid(importer)));
end

function [polylines,polylinesclosed,polylineids,polylineangles,polylinelabels,polylinegroups,canvasmin,canvasmax] = ...
        readsvgscenepolylines(xmlnode,maxerror,flipy)
    
    % get canvas size
    canvasmin = [];
    canvasmax = [];
    docRootNode = xmlnode.getOwnerDocument().getDocumentElement();
    if docRootNode.hasAttribute('x') && docRootNode.hasAttribute('y') && ...
       docRootNode.hasAttribute('width') && docRootNode.hasAttribute('height')
        
        x = sscanf(char(docRootNode.getAttribute('x')),'%f');
        y = sscanf(char(docRootNode.getAttribute('y')),'%f');
        width = sscanf(char(docRootNode.getAttribute('width')),'%f');
        height = sscanf(char(docRootNode.getAttribute('height')),'%f');
        
        canvasmin = [x;y];
        canvasmax = [x+width;y+height];
    end
    
    % read geometry
    [types,polylines,~,attr,polylinegroups] = readsvggeometry(xmlnode,true,maxerror);
    polylinesclosed = strcmp(types,'polygon');
    if not(all(strcmp(types(not(polylinesclosed)),'polyline')))
        error('Some svg geometry was not discretized correctly.');
    end
    
    % update canvas size if it was not given explicitly
    if isempty(canvasmin) || isempty(canvasmax)
        if isempty(polylines)
            canvasmin = [-1;-1];
            canvasmax = [1;1];
        else
            [canvasmin,canvasmax] = pointsetBoundingbox(polylines);
        end
    end
    
    if flipy
        for i=1:numel(polylines)
            % flip y coordinate
            polylines{i}(2,:) = canvasmin(2) + (canvasmax(2) - polylines{i}(2,:));
        end
    end
    
    % get boundingboxes
    polybbmin = zeros(2,numel(polylines));
    polybbmax = zeros(2,numel(polylines));
    for i=1:numel(polylines)
        [polybbmin(:,i),polybbmax(:,i)] = pointsetBoundingbox(polylines{i});
    end
    
    % read attributes
    polylineids = ones(1,numel(polylines)).*-1;
    polylineangles = nan(1,numel(polylines));
    polylinelabels = repmat({cell(1,0)},1,numel(polylines));
%     polylineremovemask = false(1,numel(polylines));
    for i=1:numel(polylines)
        
        % get id (-1 if none)
        if attr{i}.isKey('id')
            polylineids(i) = str2double(char(attr{i}('id')));
        end
        
%         % check fill colors, some fill colors have special meanings
%         if attr{i}.isKey('fill')
%             
%             if strcmp(attr{i}('fill'),'#F15728')
%                 % angle markers (they have the special color #F15728)
%             
%                 % get centroid of angle marker
%                 mc = zeros(2,1);
%                 [mc(1),mc(2)] = polygonCentroid(polylines{i}(1,:),polylines{i}(2,:));
% 
%                 % get polyline edge of all polylines closest to centroid of marker
%                 % (closed polylines have last vertex == first vertex)
%                 [bbclosestdist,bbfarthestdist] = pointAABoxDistance(mc,polybbmin,polybbmax);
%                 polyinds = find(bbclosestdist <= min(bbfarthestdist));
%                 mind = inf;
%                 sind = 0;
%                 pind = 0;
%                 for j=polyinds
%                     [d,si] = pointPolylineDistance_mex(mc(1),mc(2),...
%                         polylines{j}(1,:),polylines{j}(2,:));
%                     if d < mind
%                         mind = d;
%                         sind = si;
%                         pind = j;
%                     end
%                 end
% 
%                 normal = [-(polylines{pind}(2,sind+1)-polylines{pind}(2,sind));...
%                             polylines{pind}(1,sind+1)-polylines{pind}(1,sind)];
% 
%                 % angle is normal of edge closest to centroid of marker
%                 polylineangles(pind) = cart2pol(normal(1),normal(2));
% 
%                 polylineremovemask(i) = true;
%             elseif strcmp(attr{i}('fill'),'#F0EDE5')
%                 polylinelabels{i} = 'parcel';
%             elseif strcmp(attr{i}('fill'),'#95979A')
%                 polylinelabels{i} = 'street';
%             elseif strcmp(attr{i}('fill'),'#AFD8A3')
%                 polylinelabels{i} = 'greenspace';
%             elseif strcmp(attr{i}('fill'),'#B7CFEC')
%                 polylinelabels{i} = 'water';
%             end
%         end
    end
    
%     if any(polylineremovemask)
%         polylines(polylineremovemask) = [];
%         polylineids(polylineremovemask) = [];
%         polylineangles(polylineremovemask) = [];
%         polylinelabels(polylineremovemask) = [];
%         polylinegroups(polylineremovemask,:) = [];
%         polylinegroups(not(any(polylinegroups,1))) = [];
%     end
end

function [images,imageids,imagemin,imagemax] = readsvgsceneimages(xmlnode,canvasmin,canvasmax,flipy)

    % read images
    [coords,images,~,attr] = readsvgimages(xmlnode,true);
    
    % check if the images are rotated (cannot handle rotated texture layers)
    imagemin = zeros(2,numel(coords));
    imagemax = zeros(2,numel(coords));
    for i=1:numel(coords)
        if coords{i}(2,2) ~= coords{i}(2,1) || ...
           coords{i}(1,3) ~= coords{i}(1,1)
            error('Cannot use rotated images as texure layers.');
        end
        
        if flipy
            % flip y coordinate
            coords{i}(2,:) = canvasmin(2) + (canvasmax(2) - coords{i}(2,:));
        end
        
        % get coordinates of lower left and upper right corner
        imagemin(:,i) = coords{i}(:,3);
        imagemax(:,i) = [coords{i}(1,2);coords{i}(2,1)];
    end
    
    % get image ids
    imageids = ones(1,numel(images)).*-1;
    for i=1:numel(images)
        % get id (-1 if none)
        if attr{i}.isKey('id')
            imageids(i) = str2double(char(attr{i}('id')));
        end
    end
end

function [meshstructs,meshstructids] = readsvgscenemeshes(xmlnode)

    % read images
    [~,meshstructs,~,attr] = readsvgmeshes(xmlnode);
    
%     % convert meshes to have material ids etc.
%     meshtructs = tempConvertMeshes(meshtructs);
    
    % get mesh ids
    meshstructids = ones(1,numel(meshstructs)).*-1;
    for i=1:numel(meshstructs)
        % get id (-1 if none)
        if attr{i}.isKey('id')
            meshstructids(i) = str2double(char(attr{i}('id')));
        end
    end
end

function [scene,trans,scale] = createScene(...
        polylines,polylinesclosed,polylineangles,polylinelabels,polylinegroups,...
        images,imagemin,imagemax,meshstructs,...
        scaleandcenter,createpolygonsets)

    scene = Scene;
%     scene.catattributes = struct('walkable',{},'isroom',{});
    
    elms = SceneImporter.createScenePolygons(polylines,polylinesclosed,polylineangles,polylinelabels,polylinegroups,createpolygonsets);
    elms = [elms,SceneImporter.createSceneMeshes(meshstructs,repmat({cell(1,0)},1,numel(meshstructs)))];
    [texlayers,texmin,texmax] = SceneImporter.createSceneTexlayers(images,imagemin,imagemax);
    
    % translate and scale so the elements are centered in the coordinate
    % system with bounding box diagonal of a given standard size
    if scaleandcenter
        [bbmin,bbmax] = elms.boundingbox;
        bbmin = min(bbmin,[],2);
        bbmax = max(bbmax,[],2);
        if size(bbmin,1) < 2
            bbmin = [bbmin;0];
            bbmax = [bbmax;0];
        end
        bbdiag = sqrt(sum((bbmax-bbmin).^2,1));

        trans = -(bbmax+bbmin)./2;
        defaultsize = 20;
        scale = defaultsize/bbdiag;

        for i=1:numel(elms)
            elms(i).move(trans(1:size(elms(i).position,1)),'relative');
            posscaled = elms(i).position .* scale;
            elms(i).scale(scale,'relative');
            elms(i).move(posscaled,'absolute');
        end

        if not(isempty(texlayers))
            texmin = texmin + trans(1:2);
            texmax = texmax + trans(1:2);
            texmin = texmin .* scale;
            texmax = texmax .* scale;
        end
    else
        trans = [0;0;0];
        scale = 1;
    end
    
    sgroup = SceneGroup(scene);
    sgroup.addElements(elms);
%     sgroup.addTexlayer(texlayers,texmin,texmax);
end

function meshes = createSceneMeshes(meshstructs,labels)
    meshes = SceneMesh.empty;
    for i=1:numel(meshstructs)
        meshes(i) = SceneMesh(...
            meshstructs{i}.v',...
            meshstructs{i}.f',...
            labels{i});
        if obj.settings.meshidentpose
            meshes(i).definePosition([0;0;0]);
            meshes(i).defineOrientation([1;0;0;0]);
            meshes(i).defineSize([1;1;1]);
            meshes(i).defineMirrored(false);
        end
        
        if isfield(meshstructs{i},'name')
            meshes(i).setName(meshstructs{i}.name);
        else
            meshlabels = meshes(i).labelStrs;
            if not(isempty(meshlabels))
                meshes(i).setName(['unnamed_',meshlabels{1}]);
            else
                meshes(i).setName('unnamed_');
            end
        end
        
        if isfield(meshstructs{i},'n')
            if not(isfield(meshstructs{i},'fvn')); % legacy
                meshes(i).setVnormals(meshstructs{i}.n');
            else
                meshes(i).setVnormals(meshstructs{i}.n',meshstructs{i}.fvn);
            end
        end
        if isfield(meshstructs{i},'u')
            if not(isfield(meshstructs{i},'ftc')); % legacy
                meshes(i).setTexcoords(meshstructs{i}.u');
            else
                meshes(i).setTexcoords(meshstructs{i}.u',meshstructs{i}.ftc);
            end
        end
        
        if isfield(meshstructs{i},'fmg')
            % create materials
            mats = SceneMaterial.empty(1,0);
            for j=1:numel(meshstructs{i}.matnames)
                mats(:,end+1) = SceneMaterial(meshstructs{i}.matnames{j}); %#ok<AGROW>
            end
            meshes(i).setMaterials(mats,meshstructs{i}.fmg);
        end
        
        if isfield(meshstructs{i},'fsg')
            meshes(i).setSmoothgroups(meshstructs{i}.fsg);
        end
        
        meshes(i).p_fnormals = meshstructs{i}.fn';
        if isfield(meshstructs{i},'fa')
            meshes(i).p_fareas = meshstructs{i}.fa;
        end
        meshes(i).p_edges = meshstructs{i}.e;
        meshes(i).p_edgefaces = meshstructs{i}.te;
        meshes(i).p_valence = meshstructs{i}.valence;
        if isfield(meshstructs{i},'isboundarye')
            meshes(i).p_isboundarye = meshstructs{i}.isboundarye;
        end
        meshes(i).p_isboundaryv = meshstructs{i}.isboundaryv;
        meshes(i).p_isboundaryf = meshstructs{i}.isboundaryf;
    end
end

function [texlayers,texmin,texmax] = createSceneTexlayers(images,imagemin,imagemax)

    if isempty(images)
        texlayers = zeros(0,0,0,0);
        texmin = zeros(2,0);
        texmax = zeros(2,0);
    else
        imagebbdiag = sqrt(sum((max(imagemax,[],2)-min(imagemin,[],2)).^2,1));
        imageposeps = imagebbdiag * 0.001;
        
        % check if images are approx. aligned
        % todo: check with imagemin/max -/+ pixsize/2 so different
        % resolution don't change min/max position
        texmin = mean(imagemin,2);
        texmax = mean(imagemax,2);
        if sqrt(sum(max(abs(bsxfun(@minus,imagemin,texmin)),[],2).^2,1)) > imageposeps || ...
           sqrt(sum(max(abs(bsxfun(@minus,imagemax,texmax)),[],2).^2,1)) > imageposeps
            error('Images are not aligned, cannot create texture layers.');
        end

        if not(all(cellfun(@(x) size(x,3) == 4,images)))
            error('Images are not in the correct format, cannot create texture layers.');
        end

        % check if all images have the same format and resolution, resample if
        % necessary
        texres = max([cellfun(@(x) size(x,1),images);...
                      cellfun(@(x) size(x,2),images)],[],2);
        texlayers = zeros(texres(1),texres(2),4,numel(images));
        xvec = linspace(texmin(1),texmax(1),texres(2));
        yvec = linspace(texmin(2),texmax(2),texres(1));
        for i=1:numel(images)
            if any([size(images{i},1);size(images{i},2)] ~= texres)
                gi = griddedInterpolant(...
                    {linspace(imagemin(1,i),imagemax(1,i),size(images{i},2)),...
                     linspace(imagemin(2,i),imagemax(2,i),size(images{i},1))},...
                     images{i}(:,:,1),'linear','linear');

                texlayers(:,:,1,i) = gi({xvec,yvec});

                for j=2:4
                    gi.Values = images{i}(:,:,j);
                    texlayers(:,:,j,i) = gi({xvec,yvec});
                end
            else
                texlayers(:,:,:,i) = images{i};
            end
        end
    
    end
end

function polys = createScenePolygons(polylines,polylinesclosed,polylineangles,polylinelabels,polylinegroups,createpolygonsets)
    % remove polylines with less than 2 points
    mask = cellfun(@(x) size(x,2)==1 || isempty(x), polylines);
    polylines(mask) = [];
    polylinesclosed(mask) = [];
    polylineangles(mask) = [];
    polylinelabels(mask) = [];
    polylinegroups(mask,:) = [];

    polys = ScenePolygon.empty(1,0);
    
    for j=1:size(polylinegroups,2)
        polylineinds = find(polylinegroups(:,j));
        grppolys = ScenePolygon.empty(1,0);
        for i=1:numel(polylineinds)
            pind = polylineinds(i);
            grppolys(end+1) = ScenePolygon(polylines(pind),polylinelabels{pind},polylinesclosed(pind)); %#ok<AGROW>
            if not(isnan(polylineangles(pind)))
                grppolys(end).defineOrientation(polylineangles(pind));
            end
        end
        if not(isempty(grppolys))
            if j>1 && createpolygonsets % first entry is for ungrouped polys
                polys(end+1) = ScenePolygonset(grppolys); %#ok<AGROW>
                delete(grppolys(isvalid(grppolys)));
            else
                polys = [polys,grppolys]; %#ok<AGROW>
            end
        end
    end
end

end

end
