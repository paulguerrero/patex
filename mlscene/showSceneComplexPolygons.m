% show polygons
% Polygons may be grouped together in cells, in this case all polygons in a
% cell are created as a single patch object to improve performance.
% Note that for filled polygons, this requires creating a n x d_max matrix,
% where n is the number of polygons in the cell and d_max the maximum
% number of vertices of any polygon in the cell. If polygons are displayed
% without a fill, there is no overhead and it should always be faster. Note
% that some properties (such as the line width) can only be specified per
% patch.
function gobjs = showSceneComplexPolygons(complexpolys,gobjs,parent,varargin)
    
    if iscell(complexpolys)
        verts = cell(1,numel(complexpolys));
        for i=1:numel(complexpolys)
            batchboundaries = [complexpolys{i}.boundaries];
            verts{i} = {batchboundaries.verts};
        end
    else
        verts = cell(1,numel(complexpolys));
        for i=1:numel(complexpolys)
            verts{i} = {complexpolys(i).boundaries.verts};
        end
    end
    
    options = nvpairs2struct(varargin);
    if isfield(options,'facecolor') && not(strcmp(options.facecolor,'none'))

        gobjs = showFaces(verts,gobjs,parent,...
            varargin{:});
        
        % todo: if multiple components are present, triangulating is
        % probably better to save space
    else
       
        [verts,varargin] = closeGraphicsPolylines(verts,varargin);

        gobjs = showPolygons(verts,gobjs,parent,...
            'facecolor','none',... % default for non-filled polygons
            'edgecolor',[0;0;0],... % default for non-filled polygons
            varargin{:});
    end
end
