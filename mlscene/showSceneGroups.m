function gobjs = showSceneGroups(groups,gobjs,parent,varargin)
    
    matrix = cell(1,numel(groups));
    for i=1:numel(groups)
        matrix{i} = pose2mat(groups(i).originpose);
    end
    
    gobjs = showGroups(matrix,gobjs,parent,varargin{:});
end
