function handle = showScenePolygonSkeletons(skels,handle,parent,stackz)

    if nargin < 4 || isempty(stackz)
        stackz = 0;
    end
    
    if not(iscell(skels))
        skels = num2cell(skels);
    end
    
    if numel(handle) ~= numel(skels) || not(all(isgraphics(handle(:))))
        delete(handle(isgraphics(handle)));
        handle = gobjects(1,numel(skels));
        
        for i=1:numel(skels)
            handle(i) = patch([0,0],[0,0],0,...
                'Parent',parent(i),...        
                'FaceColor','none',...
                'EdgeColor','blue',...
                'LineWidth',1,...
                'Visible','off');
            handle(i).addprop('Stackz').SetMethod = @changeStackz;
            handle(i).Stackz = stackz;
        end
    end
    
    for j=1:numel(skels)

        xyzdata = zeros(3,0);
        for i=1:numel(skels{j})
            edges = skels{j}(i).edges(:,not(skels{j}(i).prunededgemask));
            
            skelxyzdata = [skels{j}(i).verts(:,edges(1,:));...
                           skels{j}(i).verts(:,edges(2,:));...
                           nan(2,size(edges,2))];
            skelxyzdata = reshape(skelxyzdata,2,[]);
            skelxyzdata = [reshape(skelxyzdata,2,[]);zeros(1,size(skelxyzdata,2))];
                                  
            xyzdata(:,end+1:end+size(skelxyzdata,2)) = skelxyzdata;
        end
        
        % parent might have changed, but only set when necessary
        if get(handle(j),'Parent') ~= parent(j)
            set(handle(j),'Parent',parent(j));
        end
        set(handle(j),...
            'XData',xyzdata(1,:),...
            'YData',xyzdata(2,:),...
            'ZData',xyzdata(3,:),...
            'Visible','on');
    end
    
end
