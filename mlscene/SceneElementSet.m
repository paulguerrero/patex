classdef SceneElementSet < handle & matlab.mixin.Heterogeneous

properties(SetAccess=protected)
    elements = ScenePolygon.empty;
    
    % updated from elements:
    
    points = ScenePoint.empty;
    polylines = ScenePolyline.empty;
    polygons = ScenePolygon.empty;
    complexpolygons = SceneComplexPolygon.empty;
    meshes = SceneMesh.empty;
    
    features = SceneCorner.empty;
    
    corners = SceneCorner.empty;
    edges = SceneEdge.empty;
    segments = SceneBoundarySegment.empty;
    surfaces = SceneSurface.empty;
    
    elements2D = ScenePolygon.empty;
    elements3D = SceneMesh.empty;
end


methods
   
% obj = SceneElementSet
% obj = SceneElementSet(obj2)
function obj = SceneElementSet(varargin)
    % do nothing for now
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneElementSet')
        obj.copyFrom(obj2);
    else
        error('Invalid arguments.');
    end
end

function copyFrom(obj,obj2)
    obj.elements = obj2.elements;

    obj.points = obj2.points;
    obj.polylines = obj2.polylines;
    obj.polygons = obj2.polygons;
    obj.complexpolygons = obj2.complexpolygons;
    obj.meshes = obj2.meshes;

    obj.features = obj2.features;

    obj.corners = obj2.corners;
    obj.edges = obj2.edges;
    obj.segments = obj2.segments;
    obj.surfaces = obj2.surfaces;

    obj.elements2D = obj2.elements2D;
    obj.elements3D = obj2.elements3D;
end

function obj2 = clone(obj)
    obj2 = SceneElementSet(obj);
end

function inds = featureInds(obj,feats)
    inds = zeros(1,numel(feats));
    allfeats = obj.features;
    for i=1:numel(feats)
        ind = find(allfeats==feats(i),1,'first');
        if not(isempty(ind))
            inds(i) = ind;
        end
    end
end

function inds = elementInds(obj,elms)
    inds = zeros(1,numel(elms));
    allelms = obj.elements;
    for i=1:numel(elms)
        ind = find(allelms==elms(i),1,'first');
        if not(isempty(ind))
            inds(i) = ind;
        end
    end
end

function elminds = poly2elmind(obj,pinds)
    if isempty(pinds)
        elminds = [];
    else
%         polygontype = SceneFeatureType.fromName('polygon');
        polymask = strcmp({obj.elements.type},'polygon');
        elmmap = 1:numel(obj.elements);
        elmmap(not(polymask)) = [];
        elminds = elmmap(pinds);
    end
end

function elminds = pt2elmind(obj,pinds)
    if isempty(pinds)
        elminds = [];
    else
%         pointtype = SceneFeatureType.fromName('point');
%         ptmask = [obj.elements.type]==pointtype.id;
        ptmask = strcmp({obj.elements.type},'point');
        elmmap = 1:numel(obj.elements);
        elmmap(not(ptmask)) = [];
        elminds = elmmap(pinds);
    end
end

function elminds = pline2elmind(obj,cinds)
    if isempty(cinds)
        elminds = [];
    else
        plinemask = strcmp({obj.elements.type},'polyline');
        elmmap = 1:numel(obj.elements);
        elmmap(not(plinemask)) = [];
        elminds = elmmap(cinds);
    end
end

function elminds = msh2elmind(obj,cinds)
    if isempty(cinds)
        elminds = [];
    else
        mshmask = strcmp({obj.elements.type},'mesh');
        elmmap = 1:numel(obj.elements);
        elmmap(not(mshmask)) = [];
        elminds = elmmap(cinds);
    end
end

function pinds = elm2polyind(obj,elminds)
    if isempty(elminds)
        pinds = [];
    else
        polymask = strcmp({obj.elements.type},'polygon');
        polymap = zeros(1,numel(obj.elements));
        polymap(polymask) = 1:sum(polymask);
        pinds = polymap(elminds);
        pinds(pinds == 0) = [];
    end
end

function pinds = elm2ptind(obj,elminds)
    if isempty(elminds)
        pinds = [];
    else
        ptmask = strcmp({obj.elements.type},'point');
        ptmap = zeros(1,numel(obj.elements));
        ptmap(ptmask) = 1:sum(ptmask);
        pinds = ptmap(elminds);
        pinds(pinds == 0) = [];
    end
end

function cinds = elm2plineind(obj,elminds)
    if isempty(elminds)
        cinds = [];
    else
        plinemask = strcmp({obj.elements.type},'polyline');
        plinemap = zeros(1,numel(obj.elements));
        plinemap(plinemask) = 1:sum(plinemask);
        cinds = plinemap(elminds);
        cinds(cinds == 0) = [];
    end
end

function cinds = elm2mshind(obj,elminds)
    if isempty(elminds)
        cinds = [];
    else
        mshmask = strcmp({obj.elements.type},'mesh');
        mshmap = zeros(1,numel(obj.elements));
        mshmap(mshmask) = 1:sum(mshmask);
        cinds = mshmap(elminds);
        cinds(cinds == 0) = [];
    end
end

function featinds = elm2featind(obj,elminds) %#ok<INUSL>
    % the updateElements function always places the elements in the front
    % of the features list
    featinds = elminds;
end

function elminds = feat2elmind(obj,featinds)
    elminds = featinds;
    elminds(elminds > numel(obj.elements)) = [];
end

function edgeinds = feat2edgeind(obj,featinds)
    if isempty(featinds)
        edgeinds = [];
    else
        [~,edgeinds] = ismember(obj.features(featinds),obj.edges);
    
        edgeinds(edgeinds == 0) = [];
    end
end

function corinds = feat2corind(obj,featinds)
    if isempty(featinds)
        corinds = [];
    else
        [~,corinds] = ismember(obj.features(featinds),obj.corners);
    
        corinds(corinds == 0) = [];
    end
end

function seginds = feat2segind(obj,featinds)
    if isempty(featinds)
        seginds = [];
    else
        [~,seginds] = ismember(obj.features(featinds),obj.segments);
    
        seginds(seginds == 0) = [];
    end
end

function surfinds = feat2surfind(obj,featinds)
    if isempty(featinds)
        surfinds = [];
    else
        [~,surfinds] = ismember(obj.features(featinds),obj.surfaces);
    
        surfinds(surfinds == 0) = [];
    end
end

function featinds = cor2featind(obj,corinds)
    [~,featinds] = ismember(obj.corners(corinds),obj.features);
end

function featinds = edge2featind(obj,edgeinds)
    [~,featinds] = ismember(obj.edges(edgeinds),obj.features);
end

function featinds = seg2featind(obj,seginds)
    [~,featinds] = ismember(obj.segments(seginds),obj.features);
end

function featinds = surf2featind(obj,surfinds)
    [~,featinds] = ismember(obj.surfaces(surfinds),obj.features);
end

function updateElements(obj)
%     elmtypeids = [obj.elements.type];
    
%     pointtype = SceneFeatureType.fromName('point');
%     polylinetype = SceneFeatureType.fromName('polyline');
%     polygontype = SceneFeatureType.fromName('polygon');
%     meshtype = SceneFeatureType.fromName('mesh');

    elmtypenames = {obj.elements.type};
    
    obj.points = obj.elements(strcmp(elmtypenames,'point'));
    obj.polylines = obj.elements(strcmp(elmtypenames,'polyline'));
    obj.polygons = obj.elements(strcmp(elmtypenames,'polygon'));
    obj.complexpolygons = obj.elements(strcmp(elmtypenames,'complexpolygon'));
    obj.meshes = obj.elements(strcmp(elmtypenames,'mesh'));
    obj.elements3D = obj.elements(arrayfun(@(x) isa(x,'SceneElement3D'),obj.elements));
    obj.elements2D = obj.elements(arrayfun(@(x) isa(x,'SceneElement2D'),obj.elements));
    
%     cornertype = SceneFeatureType.fromName('corner');
%     edgetype = SceneFeatureType.fromName('edge');
%     segmenttype = SceneFeatureType.fromName('segment');
%     surfacetype = SceneFeatureType.fromName('surface');
    
    elmfeats = obj.elements.subfeatures;
    obj.features = [obj.elements,[elmfeats{:}]];
%     featuretypeids = [obj.features.type];
    featuretypenames = {obj.features.type};
    obj.corners = obj.features(strcmp(featuretypenames,'corner'));
    obj.edges = obj.features(strcmp(featuretypenames,'edge'));
    obj.segments = obj.features(strcmp(featuretypenames,'segment'));
    obj.surfaces = obj.features(strcmp(featuretypenames,'surface'));
end

function addElements(obj,elms)
    if not(isempty(elms))
        obj.elements = [obj.elements,elms];
        obj.updateElements;
    end
end

function removeElements(obj,elms)
    if not(isempty(elms))
        obj.elements(ismember(obj.elements,elms)) = [];
        obj.updateElements;
    end
end

end

end
