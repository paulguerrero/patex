classdef SceneCorner < SceneElementFeature

properties(SetAccess=protected)
    cornervertex = []; % number of the vertex on the parent element
end

properties(Dependent, SetAccess=protected)
    pos;
    edge1;
    edge2;
    seg1;
    seg2;
%     cell;
    angle;
    nextcornervertex;
    previouscornervertex;
    extent;
end

properties(Access={?SceneCorner,?SceneImporter,?SceneExporter})
    p_edge1 = [];
    p_edge2 = [];
    p_seg1 = [];
    p_seg2 = [];
    p_angle = [];
    p_nextcornervertex = [];
    p_previouscornervertex = [];
    p_extent = [];
end

methods(Static)

function n = type_s
    n = 'corner';
end

function d = dimension_s
    d = 0;
end

end

methods
    
% SceneCorner()
% SceneCorner(obj2)
% SceneCorner(polyline,cornervertex)
% SceneCorner(polyline,cornervertex,labels)
function obj = SceneCorner(varargin)
    
    if numel(varargin) == 0
        superargs = {};
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneCorner')
        superargs = {};
    elseif numel(varargin) == 2 || numel(varargin) == 3
        superargs = varargin(1);
        varargin(1) = [];
        
        if numel(varargin) >= 2
            superargs(end+1) = varargin(2);
            varargin(2) = [];
        end
    else
        error('Invalid arguments.');
    end
    
    obj@SceneElementFeature(superargs{:});
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) == 1 && isa(varargin{1},'SceneCorner')
        obj.copyFrom(varargin{1});
    elseif numel(varargin) == 1
        obj.setCornervertex(varargin{1});
    else
        error('Invalid number of arguments.');
    end
end

function copyFrom(obj,obj2)
	obj.copyFrom@SceneElementFeature(obj2);
    
%     obj.parentelement = obj2.parentelement;
    obj.cornervertex = obj2.cornervertex;
    
    obj.p_edge1 = obj2.p_edge1;
    obj.p_edge2 = obj2.p_edge2;
    obj.p_seg1 = obj2.p_seg1;
    obj.p_seg2 = obj2.p_seg2;
    obj.p_angle = obj2.p_angle;
    obj.p_nextcornervertex = obj2.p_nextcornervertex;
    obj.p_previouscornervertex = obj2.p_previouscornervertex;
    obj.p_extent = obj2.p_extent;
end

function obj2 = clone(obj)
    obj2 = SceneCorner(obj);
end
    
function setCornervertex(obj,val)
    obj.cornervertex = val;
    obj.dirty;
end

function val = get.pos(obj)
    if not(isempty(obj.parentelement))
        val = obj.parentelement.verts(:,obj.cornervertex);
    else
        error('Corner not attached to a parent element, cannot get position.');
    end
end

function val = get.extent(obj)
    if not(obj.hasExtent)
        obj.updateExtent;
    end
    
    val = obj.p_extent;
end
function updateExtent(obj)
    mask = not(obj.hasSegments);
    if any(mask)
        obj(mask).updateSegments;
    end
    
    for k=1:numel(obj)
        o = obj(k);
    
        seg1 = o.seg1;
        seg2 = o.seg2;
        alen1 = polylineArclen(seg1,false);
        alen2 = polylineArclen(seg2,false);
        alen1 = alen1(end);
        alen2 = alen2(end);

        o.p_extent = min(alen1/2,alen2/2);
    end
end
function b = hasExtent(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_extent));
    end
end

function val = get.edge1(obj)
    if isempty(obj.p_edge1)
        obj.updateEdges;
    end
    val = obj.p_edge1;
end
function val = get.edge2(obj)
    if isempty(obj.p_edge2)
        obj.updateEdges;
    end
    val = obj.p_edge2;
end
function updateEdges(obj)
    
    for k=1:numel(obj)
        o = obj(k);
    
        if not(isempty(o.parentelement))
            vind1 = mod(o.cornervertex-1-1,size(o.parentelement.verts,2))+1;
            o.p_edge1 = o.parentelement.verts(:,vind1)-o.pos;
            vind2 = mod(o.cornervertex+1-1,size(o.parentelement.verts,2))+1;
            o.p_edge2 = o.parentelement.verts(:,vind2)-o.pos;
        end
    end
end
function b = hasEdges(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_edge1)) && not(isempty(obj(k).p_edge2));
    end
end

function val = get.seg1(obj)
    if isempty(obj.p_seg1)
        obj.updateSegments;
    end
    val = obj.p_seg1;
end
function val = get.seg2(obj)
    if isempty(obj.p_seg2)
        obj.updateSegments;
    end
    val = obj.p_seg2;
end
function updateSegments(obj)
    mask = not(obj.hasNeighbourcornervertices);
    if any(mask)
        obj(mask).updateNeighbourcornervertices;
    end
    
    for k=1:numel(obj)
        o = obj(k);
        
        if not(isempty(o.parentelement))
            if o.previouscornervertex >= o.cornervertex
                inds = [o.cornervertex:-1:1,...
                        size(o.parentelement.verts,2):-1:o.previouscornervertex];
            else
                inds = o.cornervertex:-1:o.previouscornervertex;
            end
            o.p_seg1 = [o.parentelement.verts(1,inds)-o.pos(1);...
                          o.parentelement.verts(2,inds)-o.pos(2)];    

            if o.nextcornervertex <= o.cornervertex
                inds = [o.cornervertex:size(o.parentelement.verts,2),...
                        1:o.nextcornervertex];
            else
                inds = o.cornervertex:o.nextcornervertex;
            end
            o.p_seg2 = [o.parentelement.verts(1,inds)-o.pos(1);...
                          o.parentelement.verts(2,inds)-o.pos(2)];
        end
    end
end
function b = hasSegments(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_seg1)) && not(isempty(obj(k).p_seg2));
    end
end

function val = get.angle(obj)
    if isempty(obj.p_angle)
        obj.updateAngle;
    end
    val = obj.p_angle;
end
function updateAngle(obj)
    mask = not(obj.hasEdges);
    if any(mask)
        obj(mask).updateEdges;
    end
    
    for k=1:numel(obj)
        o = obj(k);
    
        e1 = o.edge1;
        e2 = o.edge2;
        edge1angle = cart2pol(e1(1),e1(2));
        edge2angle = cart2pol(e2(1),e2(2));
        o.p_angle = mod(edge1angle - edge2angle,2*pi);
    end
end
function b = hasAngle(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_angle));
    end
end

function val = get.nextcornervertex(obj)
    if isempty(obj.p_nextcornervertex)
        obj.updateNeighbourcornervertices;
    end
    val = obj.p_nextcornervertex;
end
function val = get.previouscornervertex(obj)
    if isempty(obj.p_previouscornervertex)
        obj.updateNeighbourcornervertices;
    end
    val = obj.p_previouscornervertex;
end
function updateNeighbourcornervertices(obj)
    % don't update parent element corners, this might delete the corner we
    % are in
    
    for k=1:numel(obj)
        o = obj(k);
        
        if not(isempty(o.parentelement))
            vertinds = [o.parentelement.corners.cornervertex];
%             cornerinds = 1:numel(vertinds);
            vertinds = sort(vertinds,'ascend');
%             cornerinds = cornerinds(perm);
            ind = find(vertinds == o.cornervertex,1,'first');
            o.p_nextcornervertex = vertinds(mod(ind,numel(vertinds))+1);
            o.p_previouscornervertex = vertinds(mod(ind-2,numel(vertinds))+1);
        end
    end
end
function b = hasNeighbourcornervertices(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_nextcornervertex)) && not(isempty(obj(k).p_previouscornervertex));
    end
end

end

methods(Access=protected)
    
function dirtyImpl(obj)
    obj.dirtyImpl@SceneElementFeature;
    
    for k=1:numel(obj)
        o = obj(k);
        
        o.p_edge1 = [];
        o.p_edge2 = [];
        o.p_seg1 = [];
        o.p_seg2 = [];
        o.p_angle = [];
        o.p_nextcornervertex = [];
        o.p_previouscornervertex = [];
        o.p_extent = [];
    end
end

function updateLocalImpl(obj)
    obj.updateLocalImpl@SceneElementFeature;
    
    mask = not(obj.hasEdges);
    if any(mask)
        obj(mask).updateEdges;
    end
    mask = not(obj.hasExtent);
    if any(mask)
        obj(mask).updateExtent;
    end
    mask = not(obj.hasSegments);
    if any(mask)
        obj(mask).updateSegments;
    end
    mask = not(obj.hasAngle);
    if any(mask)
        obj(mask).updateAngle;
    end
    mask = not(obj.hasNeighbourcornervertices);
    if any(mask)
        obj(mask).updateNeighbourcornervertices;
    end
end
    
function updateBoundingboxImpl(obj)
    for i=1:numel(obj)
        o = obj(i); % for performance
        p = o.pos;
        o.p_boundingboxmin = p;
        o.p_boundingboxmax = p;
    end
end

% function [center,width,height,orientation] = posedBoundingbox2DImpl(obj,framepose,featpose)
%     center = [obj.pos];
%     width = 0;
%     height = 0;
%     orientation = pose(3);
% end
    
% relations 0 and 2 are never returned, because only relevant poly indices
% (i.e. feature is inside poly or intersecting) are returned
% relation 3 is never return for a point, since a point is always
% considered to be either inside or outside a poly (never on boundary due
% to numeric precision)
% relation: 0: feature and poly are separate
% relation: 1: feature completely inside poly
% relation: 2: poly completely inside feature
% relation: 3: feature and poly are intersecting
function [polyinds,relation] = inPolygonImpl(obj,poly,polycandidates)
%     b = zeros(1,numel(poly));
    polyinds = cell(numel(obj),1);
    if nargout >= 2
        relation = cell(numel(obj),1);
    end
    if not(isempty(poly)) && not(isempty(obj))
        
        [polybbmin,polybbmax] = poly.boundingbox;
        centroids = [obj.pos];
        
        for j=1:numel(obj)
    
            % find polygons with bounding boxes containing the point
            if nargin < 3 || isempty(polycandidates)
                polyinds{j} = find(...
                    centroids(1,j) > polybbmin(1,:) & centroids(2,j) > polybbmin(2,:) & ...
                    centroids(1,j) < polybbmax(1,:) & centroids(2,j) < polybbmax(2,:));
            else
                polyinds{j} = polycandidates{j}(...
                    centroids(1,j) > polybbmin(1,polycandidates{j}) & centroids(2,j) > polybbmin(2,polycandidates{j}) & ...
                    centroids(1,j) < polybbmax(1,polycandidates{j}) & centroids(2,j) < polybbmax(2,polycandidates{j}));
            end

            % find all polygons containing the point
            p = centroids(:,j);
            keepmask = false(1,numel(polyinds{j}));
            for i=1:numel(polyinds{j})
                % test if point is inside the polygon
                keepmask(i) = pointInPolygon_mex(p(1),p(2),poly(polyinds{j}(i)).verts(1,:),poly(polyinds{j}(i)).verts(2,:));
            end
            polyinds{j} = polyinds{j}(keepmask);
            if nargout >= 2
                relation{j} = ones(1,numel(polyinds{j}));
            end
        end
    end
end

function [d,cp] = pointDistanceImpl(obj,p,pose)
    if size(p,1) == 2
        d = inf(numel(obj),size(p,2));
        cp = zeros(numel(obj),size(p,2),2);
        for i=1:numel(obj)
            pt = obj(i).pos;
            if nargin >= 3 && not(isempty(pose)) && not(all(pose{i}==obj(i).parentelement.pose))
                pt = posetransform(pt,pose{i},obj(i).parentelement.pose);
            end
            d(i,:) = sqrt(sum(bsxfun(@minus,pt,p).^2,1));
            cp(i,:,:) = permute(repmat(pt,1,size(p,2)),[3,2,1]);
        end
    elseif size(p,1) == 3
        % assume z-coordinate of point is zero
        pts = [obj.pos];
        pts = [pts;zeros(1,size(pts,2))];
        if nargin >= 3 && not(isempty(pose))
            pelms = [obj.parentelement];
            pts = posetransform(pts,[pose{:}],pelms.getPose3D);
        else
            mask = obj.hasScenegroup;

            sgroups = obj(mask).getScenegroup;
            sgroups = [sgroups{:}];
            grouppose = [sgroups.globaloriginpose];
            pts(:,mask) = transform3D(pts(:,mask),...
                grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
        end
        
        d = pdist2(pts,p);
        cp = permute(repmat(pts,1,1,size(p,2)),[2,3,1]);
    else
        error('Only implemented for two or three dimensions.');
    end
end

function [d,t,cp] = linesegDistanceImpl(obj,p1,p2,pose) % distance to a 3D line segment
    if size(p1,1) == 2
        d = inf(numel(obj),size(p1,2));
        t = nan(numel(obj),size(p1,2));
        cp = zeros(numel(obj),size(p1,2),2);
        pts = [obj.pos];
        if nargin >= 4 && not(isempty(pose))
            pelms = [obj.parentelement];
            pts = posetransform(pts,[pose{:}],pelms.getPose);            
        end
        for i=1:size(p1,2)
            [d(:,i),~,t(:,i)] = pointPolylineDistance_mex(pts(1,:), pts(2,:),...
                p1(1,i),p1(2,i),p2(1,i),p2(2,i));
            cp(:,i,:) = permute(pts,[2,3,1]);
        end
    elseif size(p1,1) == 3
        % assume z-coordinate of point is zero
        pts = [obj.pos];
        pts = [pts;zeros(1,size(pts,2))];
        if nargin >= 4 && not(isempty(pose))
            pelms = [obj.parentelement];
            pts = posetransform(pts,[pose{:}],pelms.getPose3D);
        else
            mask = obj.hasScenegroup;

            sgroups = obj(mask).getScenegroup;
            sgroups = [sgroups{:}];
            grouppose = [sgroups.globaloriginpose];
            pts(:,mask) = transform3D(pts(:,mask),...
                grouppose(8:10,:),grouppose(4:7,:),grouppose(1:3,:),grouppose(11,:));
        end
        [d,t] = pointLinesegDistance3D_mex(pts,p1,p2);
        cp = permute(repmat(pts,1,1,size(p1,2)),[2,3,1]);
    else
        error('Only implemented for two or three dimensions.');
    end
end

function s = similarityImpl(obj,obj2)
    s = zeros(numel(obj),numel(obj2));
    for k=1:numel(obj)
        o = obj(k);
        for i=1:numel(obj2)
            o2 = obj2(i);
            s(i) = 1 - max(0,min(1,abs(o.angle - o2.angle) / (pi)));
        end
    end
    % todo: maybe also the (log?) length of edge1 and edge2 or the ratio of
    % lengths
    % todo: and maybe also the angles of the two adjacent corners
end

end

end % classdef
