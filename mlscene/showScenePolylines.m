function gobjs = showScenePolylines(polylines,gobjs,parent,varargin)
    
    hasclosedlines = false;
    
    if iscell(polylines)
        verts = cell(1,numel(polylines));
        closedmask = cell(1,numel(polylines));
        for i=1:numel(polylines)
            verts{i} = {polylines{i}.verts};
            closedmask{i} = [polylines{i}.closed];
            hasclosedlines = hasclosedlines | any(closedmask{i});
        end
    else
        verts = {polylines.verts};
        closedmask = [polylines.closed];
        hasclosedlines = any(closedmask);
    end
    
    if not(hasclosedlines)
        closedmask = [];
    end
    
    [verts,varargin] = closeGraphicsPolylines(verts,varargin,closedmask);
    
    
    gobjs = showPolylines(verts,gobjs,parent,varargin{:});
end
