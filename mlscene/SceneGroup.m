classdef SceneGroup < SceneElementSet

% group has ownership of all elements
    
properties(SetAccess={?SceneGroup,?SceneImporter,?SceneExporter})
    type = [];
    name = '';
    
    scene = Scene.empty;
    
    parentgroup = SceneGroup.empty;
    childgroups = SceneGroup.empty(1,0);
end

properties(Dependent)
    originpose;
    globaloriginpose; % in Scene coordinates (global)
    elementposes; % todo: store in p_elementpose if this takes too long
    hierarchydepth;
    isroot;
    rootgroup;
end

properties(Access=protected)
	p_originpose;
    p_globaloriginpose = [];
end

methods(Static)
    
function n = type_s
    n = 'group';
end

end

methods
    

% SceneGroup()
% SceneGroup(parentgroup)
% SceneGroup(scene)
% SceneGroup(...,originpose)
function obj = SceneGroup(varargin)
    
    obj = obj@SceneElementSet;
    
    obj.type = obj.type_s;
    
    if numel(varargin) == 0
        % do nothing
    elseif numel(varargin) >= 1 && isa(varargin{1},'Scene')
%         obj.parentgroup = SceneGroup.empty;
%         obj.scene = varargin{1};
%         obj.scene.addRootgroups(obj);
        obj.makeRootgroup(varargin{1});
    elseif numel(varargin) >= 1 && isa(varargin{1},'SceneGroup')    
        obj.setParentgroup(varargin{1});
%         varargin{1}.addChildgroups(obj);
    else
        error('Invalid arguments.');
    end
    
    if numel(varargin) < 2 || isempty(varargin{2})
        obj.p_originpose = identpose3D;
    elseif numel(varargin) >= 2 && isnumeric(varargin{2})
        obj.p_originpose = varargin{2};
    else
        error('Invalid coordinate system.');
    end
end

function copyFrom(obj,obj2) %#ok<INUSD>
    error('Copying a scene group is not supported.');
end

function obj2 = clone(obj) %#ok<STOUT,MANU>
    error('Cloning a scene group is not supported.');
end

function delete(obj)
    delete(obj.childgroups(isvalid(obj.childgroups)));
    obj.childgroups = [];

    obj.removeElements(obj.elements);
end

function val = get.isroot(obj)
    val = isempty(obj.parentgroup);
end

function val = get.rootgroup(obj)
    val = obj;
    parentgrp = obj.parentgroup;
    while not(isempty(parentgrp))
        val = parentgrp;
        parentgrp = parentgrp.parentgroup;
    end
end

function val = get.originpose(obj)
    val = obj.p_originpose;
end

function val = get.globaloriginpose(obj)
    if isempty(obj.p_globaloriginpose)
        obj.updateGlobaloriginpose;
    end
    
    val = obj.p_globaloriginpose;
end

function val = get.elementposes(obj)
%     val = obj.globalpose([obj.elements.pose3D]);
    elms = [obj.elements];
    val = obj.globalpose(elms.getPose3D);
end

function val = get.hierarchydepth(obj)
    val = 0;
    parentgrp = obj.parentgroup;
    while not(isempty(parentgrp))
        val = val+1;
        parentgrp = parentgrp.parentgroup;
    end
end

% function updateOriginpose(obj)
%     % do nothing
% end

end % methods

methods(Sealed)
    
function setName(obj,name)
    obj.name = name;
end

function updateElements(obj,updatescene)
    if nargin < 2 || isempty(updatescene)
        updatescene = true;
    end
    
    obj.updateElements@SceneElementSet;
    
    if updatescene && not(isempty(obj.scene))
        obj.scene.updateElements(false);
    end
end

function dirtyGlobaloriginpose(obj)
    for k=1:numel(obj)
        o = obj(k);
        o.p_globaloriginpose = [];
    end
    
    cgroups = [obj.childgroups];
    if not(isempty(cgroups))
        cgroups.dirtyGlobaloriginpose;
    end
end


function setOriginpose(obj,val)
    for k=1:numel(obj)
        o = obj(k);
        o.p_originpose = val(:,k);
    end
    obj.dirtyGlobaloriginpose;
end
% function updateOriginpose(obj) %#ok<MANU>
%     % do nothing
% end

function updateGlobaloriginpose(obj)
    for k=1:numel(obj)
        o = obj(k);
        o.p_globaloriginpose = o.parentgroup.globalpose(o.originpose);
    end
end
function b = hasGlobaloriginpose(obj)
    b = false(1,numel(obj));
    for k=1:numel(obj)
        b(k) = not(isempty(obj(k).p_globaloriginpose));
    end
end

function pose = globalpose(obj,pose,ancest)
    if nargin < 3
        while not(isempty(obj))
            pose = posetransform(pose,obj.originpose,identpose3D);
            obj = obj.parentgroup;
        end
    else
        while not(isempty(obj))
            if obj == ancest
                break;
            end
            pose = posetransform(pose,obj.originpose,identpose3D);
            obj = obj.parentgroup;
        end
        if isempty(obj)
            error('The given group does not seem to be an ancestor.');
        end
    end
end

function pose = localpose(obj,pose)
    ancest = SceneGroup.empty;
    
    while not(isempty(obj))
        ancest = [obj,ancest]; %#ok<AGROW>
        obj = obj.parentgroup;
    end
    
    for i=1:numel(ancest)
        pose = posetransform(pose,identpose3D,ancest(i).originpose);
    end
end

function [bbmin,bbmax,bbdiag] = boundingbox(obj)

    bbmin = zeros(3,numel(obj));
    bbmax = zeros(3,numel(obj));
    for i=1:numel(obj)
        [ebbmin,ebbmax] = obj(i).elements.boundingbox;
        ebbmin = min(ebbmin,[],2);
        ebbmax = max(ebbmax,[],2);

        if size(ebbmin,1) < 3
            % pad with zero if there are only 2D elements present
            % (2D elements are assumed to lie in the z=0 plane)
            ebbmin = [ebbmin;0]; %#ok<AGROW>
            ebbmax = [ebbmax;0]; %#ok<AGROW>
        end
        
        bbmin(:,i) = ebbmin;
        bbmax(:,i) = ebbmax;
    end
    
    if nargout >= 3
        bbdiag = sqrt(sum((bbmax-bbmin).^2,1));
    end
end

function val = getDescendantgroups(obj)
    val = SceneGroup.empty;
    
    newvals = [obj.childgroups];
    while not(isempty(newvals))
        val = [val,newvals]; %#ok<AGROW>
        newvals = [newvals.childgroups];
    end
end

function ancest = getAncestorgroups(obj)
    ancest = SceneGroup.empty;
    
    while not(isempty(obj))
        ancest = [obj,ancest]; %#ok<AGROW>
        obj = obj.parentgroup;
    end
end

function [bbmin,bbmax,bbdiag] = boundingbox2D(obj)
    
    [bbmin,bbmax] = obj.boundingbox;
    bbmin = bbmin(1:2,:);
    bbmax = bbmax(1:2,:);

    if nargout >= 3
        bbdiag = sqrt(sum((bbmax-bbmin).^2,1));
    end
end

function rescale(obj,scale)
    if size(scale,1) ~= 1
        error('Can only rescale with uniform scaling.');
    end
    
    elms = obj.elements;
    
    % ignore mesh planar proxies, they are automatically rescaled by their meshes
    if not(isempty(obj.meshes))
        elms = elms(not(ismember(elms,[obj.meshes.planarproxy])));
    end
    
    % rescale elements
    for i=1:numel(elms)
        elms(i).scale(scale,'relative',[0;0;0]);
    end
%     obj.texmin = obj.texmin .* scale;
%     obj.texmax = obj.texmax .* scale;
    
    % originpose is in local coordinates of parent group, scale just the
    % same as any element in the parent group to effictively move the whole
    % group towards/away from the parent group origin (cannot scale the
    % originpose itself, because for now it is assumed that the scale is
    % always one)
%     obj.coordsys.originpose(1:3) = obj.coordsys.originpose(1:3).*scale;
    opose = obj.originpose;
    opose(1:3) = opose(1:3).*scale;
    obj.setOriginpose(opose);
    for i=1:numel(obj.childgroups)
        obj.childgroups(i).rescale(scale);
    end
    obj.dirtyGlobaloriginpose;
end

function [newelmfeatinds,newsubfeatinds] = addElements(obj,newelms)
    
    newelmfeatinds = zeros(1,0);
    newsubfeatinds = zeros(1,0);
    
    if isempty(newelms)
        return;
    end
    
    if isempty(obj.scene)
        error('Can only add element when the group is attached to a scene.');
    end
    
    if numel(newelms) ~= numel(unique(newelms))
        error('Some elements are given multiple times.');
    end
    if any(ismember(newelms,obj.elements))
        error('Some elements are already in the group.');
    end
    
%     if any(newelms.hasScenegroup)
%         error('New element is already in a group, remove it first.');
%     end
    
    % get subfeatures of new elements
    newsubfeats = newelms.subfeatures;
    newsubfeats = [newsubfeats{:}];
    newfeats = [newelms,newsubfeats];
    
    % add new labels to the categories
    if not(all(newfeats.labelsAreStr))
        error('Labels of new feature are not strings.');
    end
    newlabels = newfeats.getLabelStrs;
    newlabels = unique([newlabels{:}]);
%     newlabels(strcmp(newlabels,'')) = [];
    newlabels = newlabels(not(ismember(newlabels,obj.scene.categories)));
    for j=1:numel(newlabels)
        obj.scene.addCategory(newlabels{j});
    end
    
    oldelmcount = numel(obj.elements);
    oldfeatcount = numel(obj.features);
    
    obj.elements = [obj.elements,newelms];
    obj.updateElements;
    obj.scene.elementsAdded(newelms);
    
    newelms.setScenegroup(obj);
    
    newelmfeatinds = oldelmcount+1:numel(obj.elements);
    newsubfeatinds = (oldfeatcount-oldelmcount)+1:(numel(obj.features)-numel(obj.elements));
    newsubfeatinds = newsubfeatinds + numel(obj.elements);
end

function setParentgroup(obj,parent)
    if numel(obj.parentgroup) ~= numel(parent) || any(obj.parentgroup ~= parent)
        oldgroup = obj.parentgroup;

        obj.parentgroup = SceneGroup.empty;
        if not(isempty(oldgroup)) && any(oldgroup.childgroups == obj)
            [~] = oldgroup.removeChildgroups(obj);
        end

        obj.parentgroup = parent;
        if not(isempty(parent)) && not(any(parent.childgroups == obj))
            parent.addChildgroups(obj);
        end

        obj.updateScene;
    end
end

function makeRootgroup(obj,sc)
    if not(isempty(obj.parentgroup))
        oldgroup = obj.parentgroup;

        obj.parentgroup = SceneGroup.empty;
        if not(isempty(oldgroup)) && any(oldgroup.childgroups == obj)
            [~] = oldgroup.removeChildgroups(obj);
        end
    end
    
    if numel(obj.scene) ~= numel(sc) || any(obj.scene ~= sc)
        obj.updateScene(sc);
        
        if not(isempty(sc)) && not(any(sc.rootgroups == obj))
            sc.addRootgroups(obj);
        end
    end
end

function updateScene(obj,sc)
    if isempty(obj.parentgroup)
        if nargin < 2
            newscene = Scene.empty;
        else
            newscene = sc;
        end
    else
        newscene = obj.parentgroup.scene;
    end
    
    if numel(obj.scene) ~= numel(newscene) || any(obj.scene ~= newscene)
        oldscene = obj.scene;
        
        subtreegroups = [obj,obj.getDescendantgroups];
        
        emptyscene = Scene.empty;
        for i=1:numel(subtreegroups)
            subtreegroups(i).elements.labelsToStr;
            subtreegroups(i).scene = emptyscene;
%             for j=1:numel(subtreegroups.elements)
%                 subtreegroups.elements(i).labelToStr
%             end
        end
        if not(isempty(oldscene))
            oldscene.groupsRemoved(subtreegroups);
        end
        for i=1:numel(subtreegroups)
            subtreegroups(i).scene = newscene;
            if not(isempty(newscene)) && not(isempty(subtreegroups(i).elements))
                newlabels = subtreegroups(i).elements.getLabelStrs;
                newlabels = unique([newlabels{:}]);
                newlabels = newlabels(not(ismember(newlabels,newscene.categories)));
                for j=1:numel(newlabels)
                    newscene.addCategory(newlabels{j});
                end
                subtreegroups(i).elements.labelsToInd;
            end
        end
        if not(isempty(newscene))
            newscene.groupsAdded(subtreegroups);
        end
    end
end

function addChildgroups(obj,groups)
    % ignore groups that are already childs
    removemask = false(1,numel(groups));
    for i=1:numel(groups)
        removemask(i) = any(obj.childgroups == groups(i));
    end
    groups(removemask) = [];
    
    if isempty(groups)
        return;
    end
    
    obj.childgroups(end+1:end+numel(groups)) = groups;
    
    for i=1:numel(groups)
        groups(i).setParentgroup(obj);
    end
end

function rgroups = removeChildgroups(obj,grpinds)
    if isa(grpinds,'SceneGroup')
        [~,grpinds] = ismember(grpinds,obj.childgroups);
        if any(grpinds<1)
            error('Not all given groups seem to be childs of this group.');
        end
    end
    
    if isempty(grpinds)
        return;
    end
    if numel(grpinds) ~= numel(unique(grpinds))
        error('Some groups are given multiple times.');
    end
    
    % remove groups from list
    rgroups = obj.childgroups(grpinds);
    obj.childgroups(grpinds) = [];
    emptygroup = SceneGroup.empty;
    for i=1:numel(rgroups)
        if not(isempty(rgroups(i).parentgroup)) && rgroups(i).parentgroup == obj
            rgroups(i).setParentgroup(emptygroup);
        end
    end
    
    if nargout < 1
        if not(isempty(rgroups))
            delete(rgroups([rgroups.isroot]));
        end
    end
end

function relms = removeElements(obj,elminds)

    relms = ScenePoint.empty;
    
    if isa(elminds,'SceneElement')
        % if element handles are given, get their index in the group
        % and ignore elements that are not in the group
        [~,elminds] = ismember(elminds,obj.elements);
        if any(elminds<1)
            error('Some elements are not in the group.');
        end
    end
    
    if isempty(elminds)
        return;
    end
    if numel(elminds) ~= numel(unique(elminds))
        error('Some elements are given multiple times.');
    end
    
    relms = obj.elements(elminds);
    
    % remove elements from list
    if not(isempty(obj.scene))
        obj.scene.elementsRemoved(relms);
    end
    obj.elements(ismember(obj.elements,relms)) = [];
    obj.updateElements;

    validrelms = relms(isvalid(relms));
    
    % set scene group of elements to empty
    validrelmgrp = validrelms.getScenegroup;
    mask = false(1,numel(validrelmgrp));
    for i=1:numel(validrelmgrp)
        mask(i) = not(isempty(validrelmgrp{i})) && validrelmgrp{i} == obj;
    end
    validrelms(mask).setScenegroup(SceneGroup.empty);
%     validrelms.setScenegroup(SceneGroup.empty); % this also clears the context of the removed elements
    
    
%     % remove child suface groups that are bound to one of the removed surfaces
%     rmeshmask = strcmp({validrelms.type},'mesh');
%     rsurfgroups = SceneSurfaceGroup.empty(1,0);
%     if any(rmeshmask)
%         rsurfs = [validrelms(rmeshmask).surfaces];
%         if not(isempty(rsurfs))
%             surfchildgroupinds = find(isa(obj.childgroups,'SceneSurfaceGroup'));
%             if not(isempty(surfchildgroupinds))
%                 surfchildgroups = obj.childgroups(surfchildgroupinds);
%                 mask = not(surfchildgroups.hasSurface);
%                 surfchildgroups(mask) = [];
%                 surfchildgroupinds(mask) = [];
%                 if not(isempty(surfchildgroups))
%                     mask = ismember(surfchildgroups.surface,rsurfs);
%                     rsurfgroups = obj.removeChildgroups(surfchildgroupinds(mask));
%                 end
%             end
%         end
%     end
    
%     if nargin < 2
%         delete(rsurfgroups(isvalid(rsurfgroups)));

        if nargout < 1
            for i=1:numel(relms) % one after the other elements may delete other elements
                if isvalid(relms(i))
                    delete(relms(i));
                end
            end
        end
%     end
end

% function subfeaturesChanged(obj,elminds)
%     if isa(elminds,'SceneElement')
%         % if element handles are given, get their index in the group
%         % and ignore elements that are not in the group
%         [~,elminds] = ismember(elminds,obj.elements);
%         if any(elminds<=0)
%             error('Some elements are not in the group.');
%         end
%     end
%     
%     if isempty(elminds)
%         return;
%     end
%     
%     
% end

% need sealing so i can call these methods on heterogenous arrays of
% Scene features
function b = eq(obj,obj2)
    b = obj.eq@handle(obj2);
end

function b = ne(obj,obj2)
    b = obj.ne@handle(obj2);
end

end

methods(Static)

function poses = featureGlobalposes(feats)
    
    featposeinds = 1:numel(feats);
    
    % get parent elements of element features
    iselmfeat = false(1,numel(feats));
    for i=1:numel(feats)
        iselmfeat(i) = isa(feats(i),'SceneElementFeature');
    end
    
    % replace element features with their parent elements
    elms = feats;
    if any(iselmfeat)
        elms(iselmfeat) = feats(iselmfeat).parentelement;
    end
    
    % get unique list of elements and parent elements
    [elms,~,ind] = unique(elms);
    featposeinds = ind(featposeinds);
    
    % sort elements by parent group
    [grps,~,elmgrpinds] = unique([elms.scenegroup]);
    grpelminds = array2cell_mex(1:numel(elms),elmgrpinds',numel(grps),2);
    
    % transform to global pose
    poses = [elms.pose3D];
    for i=1:numel(grps)
        poses(:,grpelminds{i}) = grps(i).globalpose(poses(:,grpelminds{i}));
    end
    
    poses = poses(:,featposeinds);
end

end

end % classdef
