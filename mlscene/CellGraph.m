classdef CellGraph < handle
% adjacency graph of cells defined by polylines
% each cell is a node and adjacent cells are connected by an edge
% there is one edge for each continous border, so if two cells share more
% than one continous border, they are connected by more than one edge
    
properties
    cellverts = []; % generally positions are not needed, but good to have them here
    cells = {}; % a list of vertex indices defining each cell

    edges = []; % graph edges
    edgeduals = {}; % continous borders of the adjacent cells
end

methods

function obj = CellGraph(cells,cellverts)
        
	if nargin == 1 && isa(cells,'CellGraph')
        cg = cells;
        obj.cellverts = cg.cellverts;
        obj.cells = cg.cells;
        obj.edges = cg.edges;
        obj.edgeduals = cg.edgeduals;
    elseif nargin >= 1 && not(isempty(cells))
        obj.cells = cells;

%         % make cell border edge to cell map
%         becells = cell2mat(cellfun(@(x,y) ones(1,numel(x)).*y,obj.cells,num2cell(1:numel(obj.cells)),'UniformOutput',false));
% 
%         % cell border edges
%         be1 = cell2mat(cellfun(@(x) x,obj.cells,'UniformOutput',false));
%         be2 = cell2mat(cellfun(@(x) x([2:end,1]),obj.cells,'UniformOutput',false));
% 
%         % find edges that are defined by the same points
%         twinborderedges = zeros(1,numel(be1));
%         [beind1,beind2] = find(pdist2([be1;be2]',[be1;be2]')+tril(nan(numel(be1)))==0 | ...
%                                pdist2([be1;be2]',[be2;be1]')+tril(nan(numel(be1)))==0);
%         twinborderedges(beind1) = beind2;
%         twinborderedges(beind2) = beind1;
        
        [~,becells,twinborderedges] = obj.cellborderedges;


        % walk along cell borders and add an edge for each continous piece of
        % border that is adjacent to the same two cells
        c = 1;
        for i=1:numel(cells)
            lastadjcell = -1;
            cellfirstedgeind = size(obj.edges,2)+1;

            % walk along cell border and add edges and borders for each edge
            for j=1:numel(cells{i})

%                 twinborderedge = twinborderedges(cells{i}(j));
                twinborderedge = twinborderedges(c);
                if twinborderedge == 0
                    adjcell = 0;
                else
                    adjcell = becells(twinborderedge);
                end

                if j>1
                    obj.edgeduals{end}(end+1) = cells{i}(j);
                end

                if adjcell ~= lastadjcell
                    obj.edges(:,end+1) = [i;adjcell];
                    obj.edgeduals{end+1} = cells{i}(j);
                end

                lastadjcell = adjcell;
                c = c + 1;
            end

            % add closing segment of cell to the border
            obj.edgeduals{end}(end+1) = cells{i}(1);

            % merge first and last edge of the cell if the adjacent cell at the
            % last border edge is the same as the ad
            % same 
            if lastadjcell == obj.edges(2,cellfirstedgeind)
                obj.edgeduals{cellfirstedgeind} = [obj.edgeduals{end}(1:end-1),obj.edgeduals{cellfirstedgeind}];
                obj.edgeduals(end) = [];
                obj.edges(:,end) = [];
            end

            % remove edges with no adjacent cell
            mask = obj.edges(2,:) == 0;
            obj.edges(:,mask) = [];
            obj.edgeduals(mask) = [];
        end

        % remove twin edges of the graph (borders of both should be the same,
        % just in opposite directions)
        twinmask = pdist2(obj.edges',obj.edges')+tril(nan(size(obj.edges,2)))==0 | ...
                   pdist2(obj.edges',[obj.edges(2,:);obj.edges(1,:)]')+tril(nan(size(obj.edges,2)))==0;
        [eind1,eind2] = find(twinmask);
        mask = cellfun(@(x,y) all(x==y(end:-1:1)),obj.edgeduals(eind1),obj.edgeduals(eind2));
        eind1 = eind1(mask);
        eind2 = eind2(mask);

        % just for safety: check that exactly half of the edges are removed
        if numel(unique(eind1)) ~= size(obj.edges,2) / 2
            any(sum(twinmask,1) + sum(twinmask,2)' ~= 1)
            error('there are some graph edges that have ~= 1 twin edges');
        end

        obj.edges(:,eind2) = [];
        obj.edgeduals(eind2) = [];
	end
    
    if nargin >= 2
        obj.cellverts = cellverts;
    end
end

% function delete(obj)
% end

function [sgraph,einds] = subgraph(obj,cellinds,multicomp)
%     sgraph = CellGraph(obj.cells(cellinds),obj.cellverts);
    
    if nargin < 3
        multicomp = false;
    end

    % copy cellgraph
    sgraph = CellGraph(obj);
    
    % remove cells and update edge cell indices
    cellindmap = zeros(1,numel(sgraph.cells));
    cellindmap(cellinds) = 1:numel(cellinds);
    
    sgraph.cells = sgraph.cells(cellinds);
    sgraph.edges(1,:) = cellindmap(sgraph.edges(1,:));
    sgraph.edges(2,:) = cellindmap(sgraph.edges(2,:));
    
    % remove edges and edgeduals
    mask = sgraph.edges(1,:) == 0 | sgraph.edges(2,:) == 0;
    sgraph.edges(:,mask) = [];
    sgraph.edgeduals(mask) = [];
    
    if nargout > 1
        einds = find(not(mask));
    end
    
    % check for multiple components
    if not(multicomp)
        ncomp = graphconncomp(sgraph.adjacency);
        if ncomp ~= 1
            error('given cells result in a graph with multiple components');
        end
    end
    
    % leave cellvertices as they are to keep vertex indices in the edges
    % the same (there just might be some in there that are not used)
end

function [bedges,becells,twinbedges] = cellborderedges(obj)
    % make cell border edge to cell map
    becells = cell2mat(cellfun(@(x,y) ones(1,numel(x)).*y,obj.cells,num2cell(1:numel(obj.cells)),'UniformOutput',false));

    % cell border edges
    be1 = cell2mat(cellfun(@(x) x,obj.cells,'UniformOutput',false));
    be2 = cell2mat(cellfun(@(x) x([2:end,1]),obj.cells,'UniformOutput',false));

    % find edges that are defined by the same points
    twinbedges = zeros(1,numel(be1));
    [beind1,beind2] = find(pdist2([be1;be2]',[be1;be2]')+tril(nan(numel(be1)))==0 | ...
                           pdist2([be1;be2]',[be2;be1]')+tril(nan(numel(be1)))==0);
    twinbedges(beind1) = beind2;
    twinbedges(beind2) = beind1;
    
    bedges = [be1;be2];
end

function [border,borderci] = outerborder(obj)
	
%     [ve,vn] = sgraph.vertexConnectivity;
    
    % get border edges that are only adjacent to one cell of the subgraph
    [bedges,becells,twinbedges] = obj.cellborderedges;
    mask = twinbedges > 0;
    bedges(:,mask) = [];
    becells(mask) = [];
    
    % connect the border edges (border edge should already be ccw, or cw
    % for holes)
    outstanding = 1:size(bedges,2);
    sgbeinds = {[]};
    while not(isempty(outstanding))
        if isempty(sgbeinds{end}) % start of new border component
            sgbeinds{end}(end+1) = outstanding(1); % pick first outstanding edge
            outstanding(1) = [];
        else
            if sgbeinds{end}(end) < size(bedges,2) && ...
               bedges(1,sgbeinds{end}(end)+1) == bedges(2,sgbeinds{end}(end)) % next edge is adjacent
                sgbeinds{end}(end+1) = sgbeinds{end}(end)+1; % add next edge
               outstanding(outstanding == sgbeinds{end}(end)) = [];
            else                                                          % next edge is not adjacent
                beind = find(bedges(1,outstanding) == bedges(2,sgbeinds{end}(end)),1,'first'); % find adjacent edge
                if isempty(beind)                     % no outstanding adjacent edge
                    sgbeinds{end+1} = []; %#ok<AGROW> % make space for new border component
                else                                  % found outstanding adjacent edge
                    sgbeinds{end}(end+1) = outstanding(beind); % add adjacent edge
                    outstanding(outstanding == sgbeinds{end}(end)) = [];
                end
            end
        end
    end
    
    border = cell(1,numel(sgbeinds));
    borderci = cell(1,numel(sgbeinds));
    for i=1:numel(sgbeinds)
        border{i} = bedges(:,sgbeinds{i});
        borderci{i} = becells(sgbeinds{i});
    end
    
    % move the outer contour first to the first position
    if not(isempty(obj.cellverts))
        for i=1:numel(border{i})
            verts = obj.cellverts(:,border{i}(1,:));
            if not(ispolycw(verts(1,:),verts(2,:)))
                if i > 1
                    temp = border{1};
                    border{1} = border{i};
                    border{i} = temp;
                end
                break;
            end
        end
    end
end

function [border,borderci] = subgraphborder(obj,cellinds)
	
    sgraph = obj.subgraph(cellinds);
    [border,borderci] = sgraph.outerborder;
end

% Returns the adjacency matrix of the graph. Since a CellGraph is a
% multi-graph, each entry indicates the number of edges connecting two cells.
function a = adjacency(obj)
    a = spalloc(numel(obj.cells),numel(obj.cells),size(obj.edges,2)*2);
    for i=1:size(obj.edges,2)
        a(obj.edges(1,i),obj.edges(2,i)) = a(obj.edges(1,i),obj.edges(2,i)) + 1; %#ok<SPRIX>
        a(obj.edges(2,i),obj.edges(1,i)) = a(obj.edges(2,i),obj.edges(1,i)) + 1; %#ok<SPRIX>
    end
end

function [ve,vn] = vertexConnectivity(obj)
    % build connectivity datastructures
    ve = cell(1,numel(obj.cells));
    vn = cell(1,numel(obj.cells));
    for i=1:size(obj.edges,2)
        ve{obj.edges(1,i)}(end+1) = i;
        ve{obj.edges(2,i)}(end+1) = i;
        
        vn{obj.edges(1,i)}(end+1) = obj.edges(2,i);
        vn{obj.edges(2,i)}(end+1) = obj.edges(1,i);
    end
    % make the vert edges (and neighbours) ccw on each vert
    for i=1:numel(ve)
        eind = ve{i};

        % index of the first vertex index in the cell border corresponding
        % to the edges around this vertex
        borderind = cellfun(@(x) find(obj.cells{i}==x(1),1,'first'),obj.edgeduals(eind));
        [~,perm] = sort(borderind,'ascend');
        ve{i} = ve{i}(perm);
        vn{i} = vn{i}(perm);
    end
end

function [tsegs,tedge] = arrangeAndSimplify(obj,cellinds)
    
    tg = obj.subgraph(cellinds,false);
    
    if isempty(tg.edges)
        if numel(tg.cells) ~= 1
            error('template graph with multiple disconnected components');
        end
        onecelltemplate = true;
        tg.cells{end+1} = tg.cells{1}(1); % virtual cell with shared border = first cell edge
        tg.edges = [1;2]; % edge loop, so there is at least one edge in the graph
        tg.edgeduals = {tg.cells{1}(1)};
    end
    
    [tsegs,tedge] = tg.simplified;

    % build connectivity datastructures of simplified template graph
    tvertedges = cell(1,numel(tg.cells));
    tvertneighbours = cell(1,numel(tg.cells));
    for i=1:size(tsegs,2)
        tvertedges{tsegs(1,i)}(end+1) = i;
        tvertedges{tsegs(2,i)}(end+1) = i;
        
        tvertneighbours{tsegs(1,i)}(end+1) = tsegs(2,i);
        tvertneighbours{tsegs(2,i)}(end+1) = tsegs(1,i);
    end
    % make the vert edges (and neighbours) ccw on each vert
    for i=1:numel(tvertedges)
        if isempty(tvertedges{i})
            continue;
        end
        
        seind = tvertedges{i};
        eind = zeros(1,numel(seind));
        for j=1:numel(seind)
            if tsegs(1,seind(j)) == i
                % simplified edge is leaving, take first non-simplified edge
                eind(j) = tedge{seind(j)}(1);
            elseif tsegs(2,seind(j)) == i
                % simplified edge is arriving, take last non-simplified edge
                eind(j) = tedge{seind(j)}(end);
            else
                error('edge vertex adjacency error');
            end
        end
        
        % index of the first vertex index in the cell border corresponding
        % to the edges around this vertex
        borderind = cellfun(@(x) find(tg.cells{i}==x(1),1,'first'),tg.edgeduals(eind));
        [~,perm] = sort(borderind,'ascend');
        tvertedges{i} = tvertedges{i}(perm);
        tvertneighbours{i} = tvertneighbours{i}(perm);
    end
    
    tvertorder = cellfun(@(x) numel(x),tvertedges);
    tvertorder(tvertorder == 0) = nan;
    
    % define order of the template vertices (must be same order as graph is
    % traversed, depth-first with edges in ccw order around each vertex)
    % by defining a start vertex
    [~,tstartvertind] = min(tvertorder);
    [travvinds,traveinds] = CellGraph.depthFirstTraversal(tstartvertind,tvertneighbours,tvertedges);
    
    clear tvertneighbours tvertedges; % these are not right anymore if the edge order changes
    
    tedge = tedge(traveinds);
    tsegs = tsegs(:,traveinds);
    
    tsegisreversed = tsegs(2,:) ~= travvinds(2:end);
    tsegsoriented = tsegs;
    tsegsoriented(:,tsegisreversed) = tsegsoriented([2,1],tsegisreversed);

    % check which edges of the template graph are reversed in the traversal
    tedgeisreversed = cell(size(tedge));
    for i=1:numel(tedge)
        if tsegisreversed(i)
            tedge{i} = tedge{i}(end:-1:1);
        end
        tedgeisreversed{i} = false(1,numel(tedge{i}));
        tedgeisreversed{i}(1) = tg.edges(1,tedge{i}(1)) ~= tsegsoriented(1,i);
        
        for j=2:numel(tedge{i})
            if tedgeisreversed{i}(j-1)
                tedgeisreversed{i}(j) = tg.edges(1,tedge{i}(j)) ~= tg.edges(1,tedge{i}(j-1));    
            else
                tedgeisreversed{i}(j) = tg.edges(1,tedge{i}(j)) ~= tg.edges(2,tedge{i}(j-1));    
            end
        end
        
        if not(tedgeisreversed{i}(end) == (tg.edges(2,tedge{i}(end)) ~= tsegsoriented(2,i)))
            error('could not orient template graph segments');
        end
    end
    
    % todo: check if this is correct (because of tseg, which should have
    % the eind in the correct order)
    % update connectivity datastructures of simplified template graph
    tvertsegs = cell(1,numel(tg.cells));
    tvertneighbours = cell(1,numel(tg.cells));
    for i=1:size(tsegsoriented,2)
        tvertsegs{tsegsoriented(1,i)}(end+1) = i;
        tvertsegs{tsegsoriented(2,i)}(end+1) = i;
        
        tvertneighbours{tsegsoriented(1,i)}(end+1) = tsegsoriented(2,i);
        tvertneighbours{tsegsoriented(2,i)}(end+1) = tsegsoriented(1,i);
    end
    % make the vert edges (and neighbours) ccw on each vert
    for i=1:numel(tvertsegs)
        if isempty(tvertsegs{i})
            continue;
        end
        
        seind = tvertsegs{i};
        eind = zeros(1,numel(seind));
        for j=1:numel(seind)
            if tsegsoriented(1,seind(j)) == i
                % simplified edge is leaving, take first non-simplified edge
                eind(j) = tedge{seind(j)}(1);
            elseif tsegsoriented(2,seind(j)) == i
                % simplified edge is arriving, take last non-simplified edge
                eind(j) = tedge{seind(j)}(end);
            else
                error('edge vertex adjacency error');
            end
        end
        
        % index of the first vertex index in the cell border corresponding
        % to the edges around this vertex
        borderind = cellfun(@(x) find(tg.cells{i}==x(1),1,'first'),tg.edgeduals(eind));
        [~,perm] = sort(borderind,'ascend');
        tvertsegs{i} = tvertsegs{i}(perm);
        tvertneighbours{i} = tvertneighbours{i}(perm);
    end
    
    for i=1:numel(tedge)    
        tedge{i} = [tedge{i};tedgeisreversed{i}];
    end
    
    if onecelltemplate
        tedge = {[cellinds;-1]};
    end
end

% tedge: template graph egdes, 1xm cell of 2xn: [edge index;edge is reversed or -1 for single cell]
%        1 cell for each segment of the simplified template graph
% gedge: matched graph egdes, 1xm cell of 2xn: [edge index;edge is reversed or -1 for single cell]
%        1 cell for each segment of the simplified template graph
function [tedge,gedge,score,mirrored] = templateMatches(obj,tg,cellinds)
    
    % todo: context graph is the entire template graph, for each match of
    % the template subgraph check if the context graph matches as well
    % (with same paths on segments), if so increase matching score
    

%     tg = CellGraph(tg.cells(cellinds),tg.cellverts);
    
    tg = tg.subgraph(cellinds,false);
    
    % check that both graphs don't have disconnected components
%     ncomp = graphconncomp(tg.adjacency);
%     if ncomp ~= 1
%         error('template graph has multiple disconnected components, cannot use template matching');
%     end
    ncomp = graphconncomp(obj.adjacency);
    if ncomp ~= 1
        error('graph has multiple disconnected components, cannot use template matching');
    end
    
    if isempty(tg.edges)
        if numel(tg.cells) ~= 1
            error('template graph with multiple disconnected components');
        end
        onecelltemplate = true;
        tg.cells{end+1} = tg.cells{1}(1); % virtual cell with shared border = first cell edge
        tg.edges = [1;2]; % edge loop, so there is at least one edge in the graph
        tg.edgeduals = {tg.cells{1}(1)};
    end
    
    % remove all order-2 vertices in the template graph
    [tsegs,tedge] = tg.simplified;
    % tsegs: simplifiede edges
    % tedge: corresponding indices of non-simplified edges in obj.edges
    
	% handle graphs with just one node
	if numel(obj.cells) == 1
        if size(tsegs,2) == 1
            if onecelltemplate
                tedge = {[cellinds;-1]};
            end
            gedge = {[1;-1]};
            score = 1;
            mirrored = false;
            return;
        else
            % cannot match graphs, given template graph has too many nodes
            % (even after simplification)
            if onecelltemplate
                tedge = {[cellinds;-1]};
            end
            gedge = {};
            score = [];
            mirrored = [];
            return;
        end
	end
    
    % build connectivity datastructures
    [gvertedges,gvertneighbours] = obj.vertexConnectivity;
    
    % build connectivity datastructures of simplified template graph
    tvertedges = cell(1,numel(tg.cells));
    tvertneighbours = cell(1,numel(tg.cells));
    for i=1:size(tsegs,2)
        tvertedges{tsegs(1,i)}(end+1) = i;
        tvertedges{tsegs(2,i)}(end+1) = i;
        
        tvertneighbours{tsegs(1,i)}(end+1) = tsegs(2,i);
        tvertneighbours{tsegs(2,i)}(end+1) = tsegs(1,i);
    end
    % make the vert edges (and neighbours) ccw on each vert
    for i=1:numel(tvertedges)
        if isempty(tvertedges{i})
            continue;
        end
        
        seind = tvertedges{i};
        eind = zeros(1,numel(seind));
        for j=1:numel(seind)
            if tsegs(1,seind(j)) == i
                % simplified edge is leaving, take first non-simplified edge
                eind(j) = tedge{seind(j)}(1);
            elseif tsegs(2,seind(j)) == i
                % simplified edge is arriving, take last non-simplified edge
                eind(j) = tedge{seind(j)}(end);
            else
                error('edge vertex adjacency error');
            end
        end
        
        % index of the first vertex index in the cell border corresponding
        % to the edges around this vertex
        borderind = cellfun(@(x) find(tg.cells{i}==x(1),1,'first'),tg.edgeduals(eind));
        [~,perm] = sort(borderind,'ascend');
        tvertedges{i} = tvertedges{i}(perm);
        tvertneighbours{i} = tvertneighbours{i}(perm);
    end
    
    gvertorder = cellfun(@(x) numel(x),gvertedges);
    tvertorder = cellfun(@(x) numel(x),tvertedges);
    tvertorder(tvertorder == 0) = nan;
    
    ng = numel(gvertorder);
    nt = numel(tvertorder(not(isnan(tvertorder))));
    
    if nt > ng
        % can not match the template, even the simplified template graph
        % has more nodes than the graph it is being matched against
        if onecelltemplate
            tedge = {[cellinds;-1]};
        end
        gedge = {};
        score = [];
        mirrored = [];
        return;
    end
    
    % define order of the template vertices (must be same order as graph is
    % traversed, depth-first with edges in ccw order around each vertex)
    % by defining a start vertex
    [~,tstartvertind] = min(tvertorder);
    [travvinds,traveinds] = CellGraph.depthFirstTraversal(tstartvertind,tvertneighbours,tvertedges);
    
    clear tvertneighbours tvertedges; % these are not right anymore if the edge order changes
    
    tedge = tedge(traveinds);
    tsegs = tsegs(:,traveinds);
    
    tsegisreversed = tsegs(2,:) ~= travvinds(2:end);
    tsegsoriented = tsegs;
    tsegsoriented(:,tsegisreversed) = tsegsoriented([2,1],tsegisreversed);

    
    % check which edges of the template graph are reversed in the traversal
    tedgeisreversed = cell(size(tedge));
    for i=1:numel(tedge)
        if tsegisreversed(i)
            tedge{i} = tedge{i}(end:-1:1);
        end
        tedgeisreversed{i} = false(1,numel(tedge{i}));
        tedgeisreversed{i}(1) = tg.edges(1,tedge{i}(1)) ~= tsegsoriented(1,i);
        
        for j=2:numel(tedge{i})
            if tedgeisreversed{i}(j-1)
                tedgeisreversed{i}(j) = tg.edges(1,tedge{i}(j)) ~= tg.edges(1,tedge{i}(j-1));    
            else
                tedgeisreversed{i}(j) = tg.edges(1,tedge{i}(j)) ~= tg.edges(2,tedge{i}(j-1));    
            end
        end
        
        if not(tedgeisreversed{i}(end) == (tg.edges(2,tedge{i}(end)) ~= tsegsoriented(2,i)))
            error('could not orient template graph segments');
        end
    end
    
    
    % todo: check if this is correct (because of tseg, which should have
    % the eind in the correct order)
    % update connectivity datastructures of simplified template graph
    tvertsegs = cell(1,numel(tg.cells));
    tvertneighbours = cell(1,numel(tg.cells));
    for i=1:size(tsegsoriented,2)
        tvertsegs{tsegsoriented(1,i)}(end+1) = i;
        tvertsegs{tsegsoriented(2,i)}(end+1) = i;
        
        tvertneighbours{tsegsoriented(1,i)}(end+1) = tsegsoriented(2,i);
        tvertneighbours{tsegsoriented(2,i)}(end+1) = tsegsoriented(1,i);
    end
    % make the vert edges (and neighbours) ccw on each vert
    for i=1:numel(tvertsegs)
        if isempty(tvertsegs{i})
            continue;
        end
        
        seind = tvertsegs{i};
        eind = zeros(1,numel(seind));
        for j=1:numel(seind)
            if tsegsoriented(1,seind(j)) == i
                % simplified edge is leaving, take first non-simplified edge
                eind(j) = tedge{seind(j)}(1);
            elseif tsegsoriented(2,seind(j)) == i
                % simplified edge is arriving, take last non-simplified edge
                eind(j) = tedge{seind(j)}(end);
            else
                error('edge vertex adjacency error');
            end
        end
        
        % index of the first vertex index in the cell border corresponding
        % to the edges around this vertex
        borderind = cellfun(@(x) find(tg.cells{i}==x(1),1,'first'),tg.edgeduals(eind));
        [~,perm] = sort(borderind,'ascend');
        tvertsegs{i} = tvertsegs{i}(perm);
        tvertneighbours{i} = tvertneighbours{i}(perm);
    end
    
    
    
    % compute a quick upper bound on the number of combinations and exit if
    % there are too many
    if prod(ng-nt+1:ng) > 1e+6
        error('too many combinations')
    end
    
    % find all combinations of matches for the vertices of the simplified
    % template graph
    combqueue1 = zeros(1,0);
    combtvertinds = [];
    maxorder = max(tvertorder);
    for i=maxorder:-1:1
        tvinds = find(tvertorder == i);
        combtvertinds = [combtvertinds,tvinds]; %#ok<AGROW>
        combqueue2 = [];
        for j=1:size(combqueue1,1)
             gvertinds = 1:numel(gvertorder);
             gvertinds(combqueue1(j,:)) = [];
%              if i == 1
%                 gvertinds(gvertorder(gvertinds) ~= 1) = []; % testing...
%              else
                gvertinds(gvertorder(gvertinds) < i) = [];
%              end
             combs = nchoosek(gvertinds,numel(tvinds));
             f = factorial(size(combs,2));
             combs2 = zeros(f*size(combs,1),size(combs,2));
             for k=1:size(combs,1)
                combs2((k-1)*f+1:k*f,:) = perms(combs(k,:));
             end
             combqueue2 = [combqueue2;[combqueue1(ones(size(combs2,1),1).*j,:),combs2]]; %#ok<AGROW>
        end
        combqueue1 = combqueue2;
    end
    
    clear combqueue2;
    
    tvertind2combtvertind = zeros(1,numel(tvertorder));
    tvertind2combtvertind(combtvertinds) = 1:numel(combtvertinds);
    
    tvertind2travvertind = zeros(1,numel(tvertorder));
    tvertind2travvertind(travvinds) = 1:numel(travvinds);
    
    
%     % all combinations now in combqueue1 (each column (:,a) correspondes to
%     % vertex of the simplified template graph with index tvertinds(a))
%     gedge = {};
%     score = [];
%     mirrored = [];
%     for i=1:size(combqueue1,1)
%         disp([num2str(i),' / ',num2str(size(combqueue1,1))]);
%         
%         segments = [combqueue1(i,tvertind2combtvertind(tsegsoriented(1,:)));...
%                     combqueue1(i,tvertind2combtvertind(tsegsoriented(2,:)))];
%         
%         [pathv,pathe,pathseginds,pathmirrored] = ...
%             CellGraph.allPathsBetween(segments,gvertneighbours,gvertedges,false,true);
%         
%         validpathinds = cellfun(@(x) not(isempty(x)),pathe);
%         offset = size(gedge,1);
%         gedge(end+1:end+numel(validpathinds),:) = cell(numel(validpathinds),numel(tedge));
%         mirrored(end+1:end+numel(validpathinds)) = pathmirrored(validpathinds);
%         for j=1:numel(validpathinds)
%             patheisreversed = obj.edges(2,pathe{validpathinds(j)}) ~= pathv{validpathinds(j)}(2:end);
%             gedge(offset+j,:) = mat2cell([pathe{validpathinds(j)};patheisreversed],...
%                 2,diff(find([1,diff(pathseginds{validpathinds(j)}),...
%                 numel(pathseginds{validpathinds(j)})]))); %#ok<AGROW>
%         end
%         
%         % compute matching score: quality of matching between cells (same order) and
%         % to a lesser extent the shortness of the whole matching (the shorter
%         % the better, up to the length of the simplified template)
%         tendvertmask = tvertorder(combtvertinds) == 1;
%         endvertmatches = sum(tvertorder(combtvertinds(tendvertmask)) == ...
%                              gvertorder(combqueue1(i,tendvertmask)));
%         branchvertmatches = sum(tvertorder(combtvertinds(not(tendvertmask))) == ...
%                                 gvertorder(combqueue1(i,not(tendvertmask))));
%         ordermatchscore = (endvertmatches*2+branchvertmatches) / (sum(tendvertmask)*2+sum(not(tendvertmask)));
%         for j=1:numel(validpathinds)
%             lengthpenalty = mean(cellfun(@(x) size(x,2),gedge(offset+j,:)));
%             score(offset+j) = ordermatchscore/lengthpenalty; %#ok<AGROW>
%         end
%     end
    
    
    
    % alternative: compute all paths on each segment separately, then
    % combine each path of the prior segment with all paths of the
    % following segment

    combqueue1 = combqueue1(:,tvertind2combtvertind(travvinds));
    combssorted = sortrows(combqueue1,1:size(combqueue1,2));
    
    % make a tree of the combinations, with the first vertex of the
    % choices for the first vertex of the traversal at the root, etc.
    childinds = {};
    leafinds = {};
    leafends = find(diff(combssorted(:,1)))';
    leafinds{1} = [[1,leafends+1];[leafends,size(combssorted,1)]];
    parentinds{1} = zeros(1,numel(leafends)+1);
    for i=2:size(combssorted,2)
        leafinds{i} = [];
        for j=1:size(leafinds{i-1},2)
            leafends = find(diff(combssorted(leafinds{i-1}(1,j):leafinds{i-1}(2,j),i)))';
            leafends = leafends + leafinds{i-1}(1,j)-1;
            childinds{i-1}{j} = size(leafinds{i},2)+1:size(leafinds{i},2)+numel(leafends)+1;
            leafinds{i}(:,childinds{i-1}{j}) = [[leafinds{i-1}(1,j),leafends+1];[leafends,leafinds{i-1}(2,j)]];
            parentinds{i}(childinds{i-1}{j}) = j;
        end
    end
    
    % describe the ordering of the branches around a vertex. For each
    % segment store the segment directly before the segment on the same
    % vertex and after the segment on the same vertex (in ccw order) for
    % the start and end vertex of each segment.
    % second row: arriving (0) or leaving (1)
    segbefore_start = zeros(2,size(tsegsoriented,2));
    segafter_start = zeros(2,size(tsegsoriented,2));
    segbefore_end = zeros(2,size(tsegsoriented,2));
    segafter_end = zeros(2,size(tsegsoriented,2));
    for i=1:size(tsegsoriented,2)
        veind_start = find(i == tvertsegs{tsegsoriented(1,i)});
        traversedbeforeveinds = find(tvertsegs{tsegsoriented(1,i)} < i);
        if numel(traversedbeforeveinds) > 1
            traversedbeforeveinds = sort(traversedbeforeveinds,'ascend');
            beforeveind = traversedbeforeveinds(find(traversedbeforeveinds<veind_start,1,'last'));
            afterveind = traversedbeforeveinds(find(traversedbeforeveinds>veind_start,1,'first'));
            if isempty(beforeveind)
                beforeveind = traversedbeforeveinds(end);
            end
            if isempty(afterveind)
                afterveind = traversedbeforeveinds(1);
            end
            segbefore_start(1,i) = tvertsegs{tsegsoriented(1,i)}(beforeveind);
            segbefore_start(2,i) = tsegsoriented(1,segbefore_start(1,i)) == tsegsoriented(1,i);
            segafter_start(1,i) = tvertsegs{tsegsoriented(1,i)}(afterveind);
            segafter_start(2,i) = tsegsoriented(1,segafter_start(1,i)) == tsegsoriented(1,i);
        end
        
        veind_end = find(i == tvertsegs{tsegsoriented(2,i)});
        traversedbeforeveinds = find(tvertsegs{tsegsoriented(2,i)} < i);
        if numel(traversedbeforeveinds) > 1
            traversedbeforeveinds = sort(traversedbeforeveinds,'ascend');
            beforeveind = traversedbeforeveinds(find(traversedbeforeveinds<veind_end,1,'last'));
            afterveind = traversedbeforeveinds(find(traversedbeforeveinds>veind_end,1,'first'));
            if isempty(beforeveind)
                beforeveind = traversedbeforeveinds(end);
            end
            if isempty(afterveind)
                afterveind = traversedbeforeveinds(1);
            end
            segbefore_end(1,i) = tvertsegs{tsegsoriented(2,i)}(beforeveind);
            segbefore_end(2,i) = tsegsoriented(1,segbefore_end(1,i)) == tsegsoriented(2,i);
            segafter_end(1,i) = tvertsegs{tsegsoriented(2,i)}(afterveind);
            segafter_end(2,i) = tsegsoriented(1,segafter_end(1,i)) == tsegsoriented(2,i);
        end
    end
    
    % for each segment, find a path between each valid combination of
    % matches for the start/endpoint of the segment
    pathv = cell(size(leafinds{1},2),0);
    pathe = cell(size(leafinds{1},2),0);
    pathmirrored = ones(size(leafinds{1},2),1).*-1;
    pathcombtreeinds = 1:size(leafinds{1},2);
    nverts = numel(obj.cells);
    nedges = size(obj.edges,2);
    for i=1:size(tsegsoriented,2)

        lastpathv = pathv;
        lastpathe = pathe;
        lastpathmirrored = pathmirrored;
        lastpathcombtreeinds = pathcombtreeinds;
        
        pathv = cell(0,i);
        pathe = cell(0,i);
        pathmirrored = false(0,1);
        pathcombtreeinds = zeros(0,1);

        for j=1:size(lastpathv,1)
            lastpathcombtreeind = lastpathcombtreeinds(j);
            combtreeinds = childinds{i}{lastpathcombtreeind}';
            combinds = leafinds{i+1}(1,combtreeinds); % pick the first combination of the child (all leaf combinations should have same start)
%             segments = [combssorted(combinds,i),...
%                         combssorted(combinds,i+1)]';
            segments = [combssorted(combinds,tvertind2travvertind(tsegsoriented(1,i)))';...
                        combssorted(combinds,tvertind2travvertind(tsegsoriented(2,i)))'];
            
            % mark the vertices and edges already visited by the path
            vvalid = true(1,nverts);
            vvalid([lastpathv{j,:}]) = false;
            evalid = true(1,nedges);
            evalid([lastpathe{j,:}]) = false;

            for k=1:size(segments,2)
                % If segment starts or ends in a branch, find the edges that
                % would make the path mirrored and those that would make it
                % unmirrored. If the path mirroring is already decided,
                % constrain start and/or end edges of the segment subpath
                startveinds = [];
                startveinds_ismirrored = [];
                if segbefore_start(1,i) > 0
                    if segbefore_start(2,i) == 0 % segment before is arriving
                        beforeeind = lastpathe{j,segbefore_start(1,i)}(end);
                    else % segment before is leaving
                        beforeeind = lastpathe{j,segbefore_start(1,i)}(1);
                    end

                    if segafter_start(2,i) == 0 % segment after is arriving
                        aftereind = lastpathe{j,segafter_start(1,i)}(end);
                    else % segment before is leaving
                        aftereind = lastpathe{j,segafter_start(1,i)}(1);
                    end

                    beforeveind = find(gvertedges{segments(1,k)} == beforeeind,1);
                    afterveind = find(gvertedges{segments(1,k)} == aftereind,1);

                    if isempty(beforeveind) || isempty(afterveind)
                        error('could not find edges in the branch')
                    end

                    nveinds = numel(gvertedges{segments(1,k)});
                    if lastpathmirrored(j) == 1 || lastpathmirrored(j) == 0
                        if lastpathmirrored(j) == 1;
                            temp = afterveind;
                            afterveind = beforeveind;
                            beforeveind = temp;
                        end

                        if beforeveind > afterveind
                            startveinds = [beforeveind+1:nveinds,1:afterveind-1];
                        else
                            startveinds = beforeveind+1:afterveind-1;
                        end

                        if isempty(startveinds)
                            % no free branch to continue the path, discontinue this path
                            continue;
                        end
                    else
                        % if mirroring is still undecided, all unvisited edges are ok, each
                        % choice just determines if mirrored or not
                        startveinds_ismirrored = false(1,nveinds);

                        temp = afterveind;
                        afterveind = beforeveind;
                        beforeveind = temp;

                        if beforeveind > afterveind
                            startveinds_ismirrored([beforeveind+1:nveinds,1:afterveind-1]) = true;
                        else
                            startveinds_ismirrored(beforeveind+1:afterveind-1) = true;
                        end
                    end
                end
                endveinds = [];
                endveinds_ismirrored = [];
                if segbefore_end(1,i) > 0
                    if segbefore_end(2,i) == 0 % segment before is arriving
                        beforeeind = lastpathe{j,segbefore_end(1,i)}(end);
                    else % segment before is leaving
                        beforeeind = lastpathe{j,segbefore_end(1,i)}(1);
                    end

                    if segafter_end(2,i) == 0 % segment after is arriving
                        aftereind = lastpathe{j,segafter_end(1,i)}(end);
                    else % segment before is leaving
                        aftereind = lastpathe{j,segafter_end(1,i)}(1);
                    end

                    beforeveind = find(gvertedges{segments(2,k)} == beforeeind,1);
                    afterveind = find(gvertedges{segments(2,k)} == aftereind,1);

                    if isempty(beforeveind) || isempty(afterveind)
                        error('could not find edges in the branch')
                    end

                    nveinds = numel(gvertedges{segments(2,k)});
                    if lastpathmirrored(j) == 1 || lastpathmirrored(j) == 0
                        if lastpathmirrored(j) == 1;
                            temp = afterveind;
                            afterveind = beforeveind;
                            beforeveind = temp;
                        end

                        if beforeveind > afterveind
                            endveinds = [beforeveind+1:nveinds,1:afterveind-1];
                        else
                            endveinds = beforeveind+1:afterveind-1;
                        end

                        if isempty(endveinds)
                            % no free branch to continue the path, discontinue this path
                            continue;
                        end
                    else
                        endveinds_ismirrored = false(1,nveinds);

                        temp = afterveind;
                        afterveind = beforeveind;
                        beforeveind = temp;

                        if beforeveind > afterveind
                            endveinds_ismirrored([beforeveind+1:nveinds,1:afterveind-1]) = true;
                        else
                            endveinds_ismirrored(beforeveind+1:afterveind-1) = true;
                        end
                    end
                end

                % find all paths between the matches for the start and endpoint
                % of the current segment
                [segpathv,segpathe] = CellGraph.allPathsBetween2(segments(:,k),...
                    startveinds,endveinds,...
                    gvertneighbours,gvertedges,...
                    vvalid,evalid,false);
                validmask = cellfun(@(x) not(isempty(x)),segpathe);
                segpathv = segpathv(validmask)';
                segpathe = segpathe(validmask)';
                
                if isempty(segpathv) || isempty(segpathe)
                    continue;
                end

                % if the mirroring of the paths is still undecided and the segments
                % starts or ends in a branch, decide mirroring of the paths
                if not(isempty(startveinds_ismirrored)) || not(isempty(endveinds_ismirrored))
                    startismirrored = [];
                    if not(isempty(startveinds_ismirrored))
                        startveinds = cellfun(@(x) find(gvertedges{segments(1,k)} == x(1)),segpathe);
                        startismirrored = startveinds_ismirrored(startveinds)';
                    end
                    endismirrored = [];
                    if not(isempty(endveinds_ismirrored))
                        endveinds = cellfun(@(x) find(gvertedges{segments(2,k)} == x(end)),segpathe);
                        endismirrored = endveinds_ismirrored(endveinds)';
                    end
                    if not(isempty(startismirrored)) && not(isempty(endismirrored))
                        validmask = endismirrored == startismirrored;
                        segpathv = segpathv(validmask);
                        segpathe = segpathe(validmask);
                    end
                    if not(isempty(startismirrored))
                        segpathmirrored = double(startismirrored);
                    elseif not(isempty(endismirrored))
                        segpathmirrored = double(endismirrored);
                    end
                else
                    segpathmirrored = ones(numel(segpathv),1).*lastpathmirrored(j);
                end

                % remove the duplicated first vertex from the list
                if i>1
                    for l=1:numel(segpathv)
                        segpathv{l} = segpathv{l}(2:end);
                    end
                end
                
                % add paths to the list
                pathv = [pathv;[repmat(lastpathv(j,:),numel(segpathv),1),segpathv]]; %#ok<AGROW>
                pathe = [pathe;[repmat(lastpathe(j,:),numel(segpathv),1),segpathe]]; %#ok<AGROW>
                pathmirrored = [pathmirrored;segpathmirrored]; %#ok<AGROW>
                pathcombtreeinds = [pathcombtreeinds;ones(numel(segpathv),1).*combtreeinds(k)]; %#ok<AGROW>
            end
        end
    end
    
    clear lastpathv lastpathe lastpathmirrored lastpathcombtreeinds pathcombtreeinds;
    
    % find if edges are reversed for a path and
    % compute matching score: quality of matching between cells (same order) and
    % to a lesser extent the shortness of the whole matching (the shorter
    % the better, up to the length of the simplified template)
    score = zeros(size(pathv,1),1);
    gedge = cell(size(pathv));
    for i=1:size(pathv,1)
        
        pathverts = [pathv{i,:}];
        pathedges = [pathe{i,:}];
        
        pathedgeisreversed = obj.edges(2,pathedges) ~= pathverts(2:end);
        gedge(i,:) = mat2cell([pathedges;pathedgeisreversed],2,cellfun(@(x) numel(x),pathe(i,:)));
        
        pathsegverts = [pathv{i,1}(1),cellfun(@(x) x(end),pathv(i,:))];
        
        
        % todo: also include score for the ratio of areas of the cells
        % corresponding to each segment
        tendvertmask = tvertorder(travvinds) == 1;
        endvertmatches = sum(tvertorder(travvinds(tendvertmask)) == ...
                             gvertorder(pathsegverts(tendvertmask)));
        branchvertmatches = sum(tvertorder(travvinds(not(tendvertmask))) == ...
                                gvertorder(pathsegverts(not(tendvertmask))));
        ordermatchscore = (endvertmatches*2+branchvertmatches) / (sum(tendvertmask)*2+sum(not(tendvertmask)));
        
        lengthpenalty = numel(pathedges)/size(tsegsoriented,2);
        
        score(i) = ordermatchscore/lengthpenalty;
    end
    mirrored = pathmirrored;
    
    clear pathv pathe pathmirrored;

    
    
    % sort matches by score
    [~,perm] = sort(score,'descend');
    score = score(perm);
    gedge = gedge(perm,:);

    for i=1:numel(tedge)    
        tedge{i} = [tedge{i};tedgeisreversed{i}];
    end
    
    if onecelltemplate
        tedge = {[cellinds;-1]};
    end
    
    if isvalid(tg)
        delete(tg);
    end
end

% contract all order-2 vertices
function [se,e] = simplified(obj)
    vertedges = cell(1,numel(obj.cells));
    for i=1:size(obj.edges,2)
        vertedges{obj.edges(1,i)}(end+1) = i;
        vertedges{obj.edges(2,i)}(end+1) = i;
    end
    
    vertorder = cellfun(@(x) numel(x),vertedges);
    order2vertinds = find(vertorder == 2);
    
    deleinds = [];
    se = obj.edges;
	e = num2cell(1:size(obj.edges,2));
    while(not(isempty(order2vertinds)))
        vind = order2vertinds(1);
        if vertedges{vind}(1) == vertedges{vind}(2)
            % graph is a loop of order-2 vertices and only one vertex
            % in loop left with edge connecting to itself
            break;
        end
        deleinds = [deleinds,vertedges{vind}]; %#ok<AGROW>

        e1veind = find(se(:,vertedges{vind}(1)) == vind);
        e2veind = find(se(:,vertedges{vind}(2)) == vind);
        e1vind = se(mod(e1veind,2)+1,vertedges{vind}(1));
        e2vind = se(mod(e2veind,2)+1,vertedges{vind}(2));

        if e1veind == 2 % first edge is arriving at the vertex
            se(:,end+1) = [e1vind;e2vind]; %#ok<AGROW>
            if e2veind == 1 % second edge is leaving the vertex
                e{size(se,2)} = [e{vertedges{vind}(1)},e{vertedges{vind}(2)}];
            else % second edge is arriving at the vertex
                e{size(se,2)} = [e{vertedges{vind}(1)},e{vertedges{vind}(2)}(end:-1:1)];
            end
        else % first edge is leaving the vertex
            se(:,end+1) = [e2vind;e1vind]; %#ok<AGROW>
            if e2veind == 1 % second edge is leaving the vertex
                e{size(se,2)} = [e{vertedges{vind}(2)}(end:-1:1),e{vertedges{vind}(1)}];
            else % second edge is arriving at the vertex
                e{size(se,2)} = [e{vertedges{vind}(2)},e{vertedges{vind}(1)}];    
            end
        end
        
        vertedges{e1vind}(any(se(:,vertedges{e1vind}) == vind,1)) = [];
        vertedges{e2vind}(any(se(:,vertedges{e2vind}) == vind,1)) = [];
        vertedges{e1vind}(end+1) = size(se,2);
        vertedges{e2vind}(end+1) = size(se,2);
        vertedges{vind} = [];
        vertorder = cellfun(@(x) numel(x),vertedges);
        order2vertinds = find(vertorder == 2);
    end
    
    se(:,deleinds) = [];
    e(deleinds) = [];
end

end % metohds

methods(Static)
    
function [pathv,pathe] = ...
        allPathsBetween2(segment,startveinds,endveinds,vertneighbours,vertedges,vvalid,evalid,crossing)
    
%     if not(vvalid(segment(1)))
%         error('given start vertex not valid');
%     end
    
    vindstack = segment(1);
    eindstack = [];
    pathlenstack = 0;
    
    pathv = {};
    pathe = {};
    
    currentpath = [];
    currentedges = [];
    vvisited = not(vvalid);
    evisited = not(evalid);
%     vvisited = false(1,numel(vertneighbours));
%     nedges = max(cellfun(@(x) max(x),vertedges));
%     evisited = false(1,nedges);
    
    lastpathlen = -1;
    c = 0;
    while not(isempty(vindstack))
        
        % get info from the stack
        vind = vindstack(end);
        vindstack(end) = [];
        if not(isempty(eindstack))
            eind = eindstack(end);
            eindstack(end) = [];
        else
            eind = [];
        end
        pathlen = pathlenstack(end);
        pathlenstack(end) = [];
        
        % continuing an older partial path from the stack, this means the
        % current path did not reach the goal
        if pathlen < lastpathlen 
            % restore visited and currentpath to the status they had when
            % last visiting the junction vertex
            vvisited(currentpath(pathlen+1:end)) = false;
            evisited(currentedges(pathlen:end)) = false;
            currentpath = currentpath(1:pathlen);
            currentedges = currentedges(1:pathlen-1);
        end
        
        if not(isempty(eind)) && evisited(eind)
            continue; % same edge may be added twice if it is part of a loop
        end
        
        % update current path
        currentpath(end+1) = vind; %#ok<AGROW>
        if not(isempty(eind))
            currentedges(end+1) = eind; %#ok<AGROW>
        end
        vvisited(vind) = true;
        if not(isempty(eind))
            evisited(eind) = true;
        end
        pathlen = pathlen + 1;
        lastpathlen = pathlen;
        
        % at first vertex of segment = last vertex of previous segment:
        % chek if at end, find last branching segment with same start
        % position
        if vind == segment(2) && ...
           (isempty(endveinds) || ismember(find(vertedges{vind} == eind),endveinds))
            
            pathv{end+1} = currentpath; %#ok<AGROW>
            pathe{end+1} = currentedges; %#ok<AGROW>
            vind = 0;
        end
        
        % add new edges and vertices to the stack
        if vind > 0
            if crossing
                mask = not(evisited(vertedges{vind}));
            else
                mask = not(vvisited(vertneighbours{vind}));
                
                % check if a neighbour is the end mask and reachable via an
                % endveind edge, if so add the vertex even though the
                % vertex may already have been visited
                segendmask = vertneighbours{vind} == segment(2);
                if not(isempty(endveinds)) && sum(segendmask) > 0
                    einds = vertedges{vind}(segendmask);
                    veind = zeros(1,numel(einds));
                    for i=1:numel(einds)
                        veind(i) = find(vertedges{segment(2)} == einds(i));
                    end
                    validmask = ismember(veind,endveinds);
                    inds = find(segendmask);
                    segendmask(inds(not(validmask))) = false;
                end
                mask = mask | segendmask;
                
                mask = mask & not(evisited(vertedges{vind}));
                
%                 mask = not(vvisited(vertneighbours{vind})) & ...
%                        not(evisited(vertedges{vind}));
            end
            
            if c == 0 && not(isempty(startveinds))
                invalidmask = true(1,numel(mask));
                invalidmask(startverteinds) = false;
                mask(invalidmask) = false;
%                 mask([1:startveind-1,startveind+1:end]) = false;
            end
            
            nneighbours = sum(mask);
            vindstack = [vindstack,vertneighbours{vind}(mask)]; %#ok<AGROW>
            eindstack = [eindstack,vertedges{vind}(mask)]; %#ok<AGROW>
            pathlenstack = [pathlenstack,ones(1,nneighbours).*pathlen]; %#ok<AGROW>
        end
        c = c + 1;
    end
end

% find all combinations of paths between the given segments that do not overlap
% (and optionally do not cross each other)
% but careful with larger graphs, there can be a lot of paths
% segments must be in ccw order, as well as vertneighbours and vertedges
function [pathv,pathe,pathseginds,pathmirrored] = ...
        allPathsBetween(segments,vertneighbours,vertedges,crossing,mirrored)%,vvalid,evalid,crossing)
    
    vindstack = segments(1,1);
    eindstack = [];
    pathlenstack = 0;
    segindstack =[];
    
    pathv = {};
    pathe = {};
    pathseginds = {};
    pathmirrored = [];
    
    currentpath = [];
    currentedges = [];
    currentseginds = [];
%     vvisited = not(vvalid);
%     evisited = not(evalid);
    vvisited = false(1,numel(vertneighbours));
    nedges = max(cellfun(@(x) max(x),vertedges));
    evisited = false(1,nedges);
    
    % paths should mantain same ccw order as segments, so if a path starts
    % at a vertex where another segment has already started, always use an
    % vertex edge with vertex edge index larger than the one used visited
    % at the last segemtn start from that vertex.
%     lastsegstartveind = zeros(1,numel(vertneighbours));
%     segstartlastveind = zeros(1,size(segments,2));
%     pathmirrored = ones(1,size(segments,2)).*-1;
    segstartlastveind = 0;
    pismirrored = -1;
    
    
    branchsegind = [];
    lastpathlen = -1;
    c = 0;
    while not(isempty(vindstack))
        
        % get info from the stack
        vind = vindstack(end);
        vindstack(end) = [];
        if not(isempty(eindstack))
            eind = eindstack(end);
            eindstack(end) = [];
        else
            eind = [];
        end
        pathlen = pathlenstack(end);
        pathlenstack(end) = [];
        if not(isempty(segindstack))
            segind = segindstack(end);
            segindstack(end) = [];
        else
            segind = [];
        end
        
        % continuing an older partial path from the stack, this means the
        % current path did not reach the goal
        if pathlen < lastpathlen 
            % restore visited and currentpath to the status they had when
            % last visiting the junction vertex
            vvisited(currentpath(pathlen+1:end)) = false;
            evisited(currentedges(pathlen:end)) = false;
            currentpath = currentpath(1:pathlen);
            currentedges = currentedges(1:pathlen-1);
            currentseginds = currentseginds(1:pathlen-1);
            if isempty(currentseginds)
                segstartlastveind = 0;
                pismirrored = -1;
            else
                segstartlastveind = segstartlastveind(1:currentseginds(end));
                pismirrored = pismirrored(1:currentseginds(end));
            end
            
        end
        
        % update current path
        currentpath(end+1) = vind; %#ok<AGROW>
        if not(isempty(eind))
            currentedges(end+1) = eind; %#ok<AGROW>
        end
        if not(isempty(segind))
            currentseginds(end+1) = segind; %#ok<AGROW>
        end
        vvisited(vind) = true;
        if not(isempty(eind))
            evisited(eind) = true;
        end
        pathlen = pathlen + 1;
        lastpathlen = pathlen;
        
        
        % at first edge of segment: store the choice of branch and determine if path is mirrored
        if numel(currentseginds) == 1 || (numel(currentseginds) > 1 && currentseginds(end-1) ~= currentseginds(end)) % first edge of the segment
            veind = find(vertedges{segments(1,segind)} == eind);
%             find(vertedges{currentpath(end-1)} == eind);
            segstartlastveind(segind) = veind; %#ok<AGROW>
            branchsegind = find(segments(1,1:segind-1) == segments(1,segind),1,'last');
            if not(isempty(branchsegind)) %&& pathmirrored(segind) < 0
                pismirrored(segind) = double(veind < segstartlastveind(branchsegind));
            elseif segind > 1
                pismirrored(segind) = pismirrored(segind-1);
            else
                pismirrored(segind) = -1;
            end
        end
        
        % at first vertex of segment = last vertex of previous segment:
        % chek if at end, find last branching segment with same start
        % position
%         branchsegind = [];
        segstart = false;
        if vind == segments(2,segind)
            if segind == size(segments,2)
                % we have a winner!!
                pathv{end+1} = currentpath; %#ok<AGROW>
                pathe{end+1} = currentedges; %#ok<AGROW>
                pathseginds{end+1} = currentseginds; %#ok<AGROW>
                pathmirrored(end+1) = pismirrored(end); %#ok<AGROW>
                vind = 0;
            else
                segind = segind + 1;
                vind = segments(1,segind);
                segstart = true;
                pismirrored(segind) = pismirrored(segind-1);
                branchsegind = find(segments(1,1:segind-1) == segments(1,segind),1,'last');
            end
        end
        
        if c == 0
            segstart = true;
            branchsegind = [];
            segind = 1;
%             pismirrored(segind) = -1;
        end
        
        % add new edges and vertices to the stack
        if vind > 0
            if crossing
                mask = not(evisited(vertedges{vind}));
            else
                mask = not(vvisited(vertneighbours{vind})) & ...
                       not(evisited(vertedges{vind}));
            end
            
            % at segment start, remove outgoing edges that are not in the right order
            if segstart && not(isempty(branchsegind))
                if mirrored 
                    if pismirrored(end) == 1
                        mask(segstartlastveind(branchsegind):end) = false;
                    elseif pismirrored(end) == 0
                        mask(1:segstartlastveind(branchsegind)) = false;
                    end
                else
                    mask(1:segstartlastveind(branchsegind)) = false;
                end
            end
            
            nneighbours = sum(mask);
            vindstack = [vindstack,vertneighbours{vind}(mask)]; %#ok<AGROW>
            eindstack = [eindstack,vertedges{vind}(mask)]; %#ok<AGROW>
            pathlenstack = [pathlenstack,ones(1,nneighbours).*pathlen]; %#ok<AGROW>
            segindstack = [segindstack,ones(1,nneighbours).*segind]; %#ok<AGROW>

        end
        c = c + 1;
    end
end

function [tverts,tedges] = depthFirstTraversal(startvertind,vertneighbours,vertedges)
    vertstack = startvertind;
    edgestack = [];
%     visited = false(1,numel(vertneighbours));
%     nedges = max(cellfun(@(x) max(x),vertedges));
    nedges = 0;
    for i=1:numel(vertedges)
        if max(vertedges{i}) > nedges
            nedges = max(vertedges{i});
        end
    end
    tverts = zeros(1,nedges+1);
    tedges = zeros(1,nedges);
    visited = false(1,nedges);
    c = 1;
    while not(isempty(vertstack))
        vind = vertstack(end);
        vertstack(end) = [];
        if not(isempty(edgestack))
            eind = edgestack(end);
            edgestack(end) = [];
        else
            eind = [];
        end

        if not(isempty(eind)) && visited(eind)
            continue; % same edge might get added to the stack twice if it is part of a loop
        end
        
        tverts(c) = vind;
        if not(isempty(eind))
            tedges(c-1) = eind;
            visited(eind) = true;
        end
        
%         visited(vind) = true;
%         mask = not(visited(vertneighbours{vind}));
        mask = not(visited(vertedges{vind}));
        vertstack = [vertstack,vertneighbours{vind}(mask)]; %#ok<AGROW>
        edgestack = [edgestack,vertedges{vind}(mask)]; %#ok<AGROW>
        c = c + 1;
    end
    
    tverts(c:end) = [];
    tedges(c-1:end) = [];
end

end % methods

end % classdef
