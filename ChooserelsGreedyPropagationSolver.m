classdef ChooserelsGreedyPropagationSolver < PatternSolver
    
properties(SetAccess=protected)
    % last inputs
    newelmposes = zeros(2,0);
    
    % last solution
    solutionchoiceinds = zeros(1,0);
    solutioncost = zeros(1,0);
    solutionstate = [];
    solutionstatevar = [];
    solutionelmposes = cell(1,0);
    solutionrelvals = cell(1,0);

%     groupmode = 'implicit'; % 'explicit' or 'implicit' (extend pattern graph)
    groupmode = 'explicit'; % 'explicit' or 'implicit' (extend pattern graph)
    
    synctolerance = 0.1;
end

properties
    fullstatevar = zeros(0,1);
end

% properties(Access=protected)
properties(SetAccess=protected)
    oldelmposes = zeros(2,0);
    targetrelvals = zeros(1,0);
    targetelmposes = zeros(2,0);
    relweights = zeros(1,0);
    elmweights = zeros(1,0);

    choicestate = [];
    choicestatevar = [];
    choicenodevisited = [];
    choicegroupvisited = [];
    choicenodesync = [];
    choicedecision = [];
    choicecost = zeros(1,0);    
    choiceclosed = false(1,0);
    choicetree = sparse([]);
    
    fullstate = zeros(0,1);
%     fullstatevar = zeros(0,1);
    fullstateunsync = zeros(0,1);
    substateinds = zeros(0,1);
    preferredstate = zeros(0,1);
    
    % just for visualization
    currentnodeinds = zeros(1,0);
    currentvisitedmask = zeros(0,1);
    currentsyncmask = zeros(0,1);
    
    callbackfun = [];
    
    stop = false;
end

methods

% obj = ChooserelsGreedyPropagationSolver
% obj = ChooserelsGreedyPropagationSolver(pgraph)
function obj = ChooserelsGreedyPropagationSolver(varargin)
    superarginds = zeros(1,0);
    if numel(varargin) == 1
        superarginds = 1;
    end
    
    obj@PatternSolver(varargin{superarginds});
    varargin(superarginds) = [];
    
    if numel(varargin) == 0
        % do nothing
    else
        error('Invalid arguments.');
    end
end

function clear(obj)
    obj.clear@PatternSolver;
    
    obj.solutionchoiceinds = zeros(1,0);
    obj.solutioncost = zeros(1,0);
    obj.solutionstate = [];
    obj.solutionstatevar = [];
    obj.solutionelmposes = cell(1,0);
    obj.solutionrelvals = cell(1,0);
    
    obj.oldelmposes = zeros(2,0);
    obj.targetrelvals = zeros(1,0);
    obj.targetelmposes = zeros(2,0);
    obj.relweights = zeros(1,0);
    obj.elmweights = zeros(1,0);
    
    obj.choicestate = [];
    obj.choicestatevar = [];
    obj.choicenodevisited = [];
    obj.choicegroupvisited = [];
    obj.choicenodesync = [];
    obj.choicedecision = [];
    obj.choicecost = zeros(1,0);
    obj.choiceclosed = false(1,0);
    obj.choicetree = sparse([]);
    
    obj.fullstate = zeros(0,1);
    obj.fullstatevar = zeros(0,1);
    obj.fullstateunsync = zeros(0,1);
    obj.substateinds = zeros(0,1);
    obj.preferredstate = zeros(0,1);
    
    obj.currentvisitedmask = zeros(0,1);
    obj.currentsyncmask = zeros(0,1);
    
    obj.stop = false;
end

function prepare(obj,newelmposes)

    obj.prepare@PatternSolver;
    
    nodecount = numel(obj.pgraph.nodes);
    statedim = numel(obj.patternstate);
    groupcount = size(obj.pgraph.groups,2);
    
    obj.oldelmposes = [obj.pgraph.elms.pose];
    obj.oldelmposes = obj.oldelmposes(1:obj.elmposedim,:);
    
    obj.newelmposes = PatternSolver.affine2simpose(newelmposes);
    obj.newelmposes = obj.newelmposes(1:obj.elmposedim,:);
    
    obj.targetrelvals = nan(1,numel(obj.pgraph.rels));
    obj.targetelmposes = nan(obj.elmposedim,numel(obj.pgraph.elms));
    obj.relweights = ones(1,numel(obj.pgraph.rels));
    obj.elmweights = ones(1,numel(obj.pgraph.elms));
    
    userchangedelmmask = any(obj.oldelmposes ~= obj.newelmposes,1);
    obj.targetelmposes(:,userchangedelmmask) = obj.newelmposes(:,userchangedelmmask);
    obj.elmweights(userchangedelmmask) = 1000;
    
    obj.choicestate = zeros(statedim,0);
    obj.choicestatevar = zeros(statedim,0);
    obj.choicenodevisited = false(nodecount,0);
    obj.choicegroupvisited = false(groupcount,0);
    obj.choicenodesync = true(nodecount,0);
    obj.choicedecision = zeros(nodecount,0);
    obj.choicecost = zeros(1,0);    
    obj.choiceclosed = false(1,0);
    obj.choicetree = sparse([]);
    
    obj.fullstate = nan(statedim,1);
    obj.fullstatevar = nan(statedim,1);
    obj.fullstateunsync = nan(statedim,1);
    obj.substateinds = zeros(statedim,1);
    obj.preferredstate = nan(statedim,1);

    obj.currentvisitedmask = zeros(0,1);
    obj.currentsyncmask = zeros(0,1);
    
    obj.stop = false;
end

function [relval,relvar] = computerel(obj,state,nodeind)
                
    input1stateinds = obj.node2stateinds{find(obj.pgraph.adjacency(nodeind,:) == -1)}; %#ok<FNDSB>
    input2stateinds = obj.node2stateinds{find(obj.pgraph.adjacency(nodeind,:) == 1)}; %#ok<FNDSB>
    
    rel = obj.pgraph.rels(obj.pgraph.node2relinds(nodeind));
    [relval,relvar] = rel.compute_o(...
        state(input1stateinds),...
        state(input2stateinds));
end

function [state,statevar,objectiveval,allpnodeinds,visitpnodeinds,visitpunsync] = optimizenodes(...
        obj,state,statevar,nodeinds,visitedmask,syncmask)
    
%     parentnodeinds = cell(1,numel(nodeinds));
%     for i=1:numel(nodeinds)
%         parentnodeinds{i} = obj.pgraph.parents(nodeinds(i));
%     end
    
    allpnodeinds = unique([obj.parentnodeinds{nodeinds}]);
%     visitpnodeinds = allpnodeinds(visitedmask(allpnodeinds));
    visitpnodeinds = allpnodeinds(visitedmask(allpnodeinds) & syncmask(allpnodeinds));
    visitprelinds = obj.pgraph.node2relinds(visitpnodeinds);
    visitpstateinds = cat(1,obj.node2stateinds{visitpnodeinds});

    obj.substateinds = cat(1,obj.node2stateinds{nodeinds});
    obj.targetrelvals = nan(1,numel(obj.pgraph.rels));
    obj.targetrelvals(visitprelinds) = state(visitpstateinds);
    obj.fullstate = state;
    obj.fullstatevar = statevar;
    obj.fullstateunsync = nan(numel(state),1);
    obj.preferredstate = nan(numel(state),1);
    
%     obj.preferredstate = nan(size(state));
%     visitedstateinds = obj.node2stateinds(find(visitedmask)); %#ok<FNDSB>
%     visitedstateinds = cat(1,visitedstateinds{:});
%     obj.preferredstate(visitedstateinds) = state(visitedstateinds);

    startsubstate = obj.fullstate(obj.substateinds);

    warning('off','optim:fminunc:SwitchingMethod');
    
    % optimize
    [bestsubstate,objectiveval,exitflag,output,valgrad,~] = fminunc(...
        @(x) obj.evalsubstate_nograd(x),startsubstate,...
        optimoptions(@fminunc,...
        'MaxFunEvals',100000,... % matlab default is 100*number of variables
        'TolFun',1e-7,... % matlab default is 1e-6
        'GradObj','off',...
        'Diagnostics','on',...
        'Diagnostics','off',... % 'on',...
        'Display','off',... % temp
        'FunValCheck','on',...
        'OutputFcn',@(state,optimValues,solverflag) obj.outfun(state,optimValues,solverflag),...
        'DerivativeCheck','off'));
    
    if exitflag == -1
        obj.stop = true;
    end
    
    warning('on','optim:fminunc:SwitchingMethod');

    state(obj.substateinds) = bestsubstate;
    statevar = obj.fullstatevar;
    
    visitpunsync = obj.fullstateunsync(visitpstateinds);
end

function [selmposes,srelvals,scost] = solve(obj,newelmposes,newrelvals)
    
    
%     % temp: clone the pgraph so it does not get deleted after the solver
%     % has finished
%     obj.pgraph = obj.pgraph.clone;
%     for i=1:numel(obj.pgraph.elms)
%         obj.pgraph.elms(i) = obj.pgraph.elms(i).clone;
%     end
%     obj.pgraph.elms.setScenegroup(SceneGroup.empty);
    
    % above this threshold for the objective function (which inlcudes the
    % variance), a node is considered out of sync
    
    
    obj.clear;

%     if not(obj.pgraph.balancedchildlevels)
%         error('Only graph with balanced child levels are supported for now.');
%         % otherwise getting all queued nodes of a single level does not work
%     end

    if strcmp(obj.groupmode,'implicit')
        obj.pgraph.createImplicitRelgroups;
    end
    
    obj.prepare(newelmposes);
    
    nodelevel = obj.pgraph.level;
    
    elmoffset = obj.pgraph.elmoffset;
    
%     parentnodeinds = cell(1,nodecount);
%     childnodeinds = cell(1,nodecount);
%     for i=1:nodecount
%         parentnodeinds{i} = obj.pgraph.parents(i);
%         childnodeinds{i} = obj.pgraph.childs(i);
%     end

    state = obj.patternstate;
    statevar = obj.patternstatevar;
    
    visitedmask = false(1,numel(obj.pgraph.nodes));
    groupvisitedmask = false(1,size(obj.pgraph.groups,2));
    syncmask = true(1,numel(obj.pgraph.nodes));
    decisionmask = zeros(1,numel(obj.pgraph.nodes));
    
    userchangedelmind = find(all(not(isnan(obj.targetelmposes)),1));
    userchangednodeind = userchangedelmind + elmoffset;
    
    if numel(userchangedelmind) > 1
        error('Only one changed element is supported for now.');
    elseif numel(userchangedelmind) == 1
        state(obj.node2stateinds{userchangednodeind}) = obj.newelmposes(:,userchangedelmind);
        state(obj.nodefixorig2stateinds(userchangednodeind)) = 0;
        state(obj.nodefixnew2stateinds(userchangednodeind)) = 1;

        visitedmask(userchangedelmind+elmoffset) = true;
        syncmask(obj.pgraph.parents(userchangedelmind+elmoffset)) = false;
        decisionmask(userchangedelmind+elmoffset) = 1; % 1 is update (change moved to parent)
    end
    
    obj.choicestate(:,end+1) = state;
    obj.choicestatevar(:,end+1) = statevar;
    obj.choicenodevisited(:,end+1) = visitedmask;
    obj.choicegroupvisited(:,end+1) = groupvisitedmask;
    obj.choicenodesync(:,end+1) = syncmask;
    obj.choicedecision(:,end+1) = decisionmask;
    obj.choicecost(:,end+1) = 0;
    obj.choiceclosed(:,end+1) = false;
    obj.choicetree(1,1) = 1;

    bestsolutioncost = inf;
    
    while(true)
        if obj.stop
            break;
        end
        
        choiceopeninds = find(not(obj.choiceclosed));
        [bestcost,ind] = min(obj.choicecost(choiceopeninds));
        choiceind = choiceopeninds(ind);
        
                
        disp('--------------');
        disp(['choosing choice ',num2str(choiceind),' (cost ',num2str(obj.choicecost(choiceind)),' - best solution: ',num2str(bestsolutioncost),')']);
        disp(['active paths: ',num2str(numel(choiceopeninds))]);
        
        if isempty(choiceind) || bestcost > bestsolutioncost * 3
            % no more choices to be made or best available choice is too
            % expensive
            break;
        end
        
        % get the state of the choice node
        state = obj.choicestate(:,choiceind);
        statevar = obj.choicestatevar(:,choiceind);
        visitedmask = obj.choicenodevisited(:,choiceind);
        groupvisitedmask = obj.choicegroupvisited(:,choiceind);
        syncmask = obj.choicenodesync(:,choiceind);
        decisionmask = obj.choicedecision(:,choiceind);
        
        % check for duplicate states
        allothermask = true(1,size(obj.choicestate,2));
        allothermask(choiceind) = false;
        mask = ...
            all(bsxfun(@eq,visitedmask,obj.choicenodevisited(:,allothermask)),1) & ...
            all(bsxfun(@eq,syncmask,obj.choicenodesync(:,allothermask)),1) & ...
            all(bsxfun(@eq,groupvisitedmask,obj.choicegroupvisited(:,allothermask)),1) & ...
            all(bsxfun(@rdivide,bsxfun(@minus,state,obj.choicestate(:,allothermask)).^2,statevar) < obj.synctolerance,1);
        
        if any(mask)
%             obj.choicestate(:,choiceind) = [];
%             obj.choicestatevar(:,choiceind) = [];
%             obj.choicenodevisited(:,choiceind) = [];
%             obj.choicegroupvisited(:,choiceind) = [];
%             obj.choicenodesync(:,choiceind) = [];
%             obj.choicedecision(:,choiceind) = [];
%             obj.choicecost(:,choiceind) = [];
%             obj.choiceclosed(:,choiceind) = [];
%             obj.choicetree(choiceind,:) = [];
%             obj.choicetree(:,choiceind) = [];
            
            obj.choiceclosed(:,choiceind) = true; % temp
            obj.choicecost(:,choiceind) = inf; % temp
            continue;
        end
        
        
        if all(syncmask)
            
            % show current state
            obj.showState(zeros(0,1),zeros(0,1),state,statevar,zeros(0,1),visitedmask,syncmask);
            
%             obj.choicecost(choiceind) = obj.eval_regularization_visited(state);
            obj.choicecost(choiceind) = obj.computeRegularizer(state,statevar,visitedmask,syncmask,false);%true);
            if obj.choicecost(choiceind) < bestsolutioncost
                bestsolutioncost = obj.choicecost(choiceind);
            end
            
%             obj.choicecost(choiceind) = obj.eval_regularization_full(state);
            obj.choicecost(choiceind) = obj.computeRegularizer(state,statevar,visitedmask,syncmask,false);
            obj.choiceclosed(choiceind) = true;
            
            disp(['choice ',num2str(choiceind),' all done (cost ',num2str(obj.choicecost(choiceind)),' - best solution: ',num2str(bestsolutioncost),')']);
            
            continue;
        end
            
        nodeinds = find(not(syncmask));
        nodeinds = nodeinds(nodelevel(nodeinds) == max(nodelevel(nodeinds)));
        visitedchildcount = zeros(1,numel(nodeinds));
        for i=1:numel(nodeinds)
            visitedchildcount(i) = nnz(visitedmask(obj.childnodeinds{nodeinds(i)}));
        end
        nodeinds = nodeinds(visitedchildcount == max(visitedchildcount));
        nodeinds = nodeinds(1); % enable here (uncomment this)
        
        disp(['nodeind: ',num2str(nodeinds)]);
        
        % show current state
%         obj.fullstate = state;
%         obj.substateinds = zeros(0,1);
%         obj.currentnodeinds = nodeinds;
%         obj.currentvisitedmask = visitedmask;
%         obj.currentsyncmask = syncmask;
%         obj.outfun(zeros(0,1),[],[]);
        obj.showState(zeros(0,1),zeros(0,1),state,statevar,nodeinds,visitedmask,syncmask);
        
        wasvisited = visitedmask;
        wassynced = syncmask;
        
        visitedmask(nodeinds) = true;
        syncmask(nodeinds) = true;

        % check if the node is synced
        synced = false(1,numel(nodeinds));
        relval = cell(1,numel(nodeinds));
        relvar = cell(1,numel(nodeinds));
        for i=1:numel(nodeinds)
            nind = nodeinds(i);
            nrelind = obj.pgraph.node2relinds(nind);
            nstateind = obj.node2stateinds{nind};

            if nodelevel(nind) <= 0
                % all synced parents of element nodes stay synced,
                % since the update does not change element nodes
                relval{i} = state(obj.node2stateinds{nind});
                relvar{i} = statevar(obj.node2stateinds{nind});
                synced(i) = true;
                continue;
            end

            [relval{i},relvar{i}] = obj.computerel(state,nind);
            
            residual = relval{i} - state(nstateind);
            if obj.pgraph.rels(nrelind).iscircular
                residual = smod(residual,-pi,pi);
            end
            if (residual^2 / statevar(nstateind)) * obj.relweights(nrelind)  < obj.synctolerance;
                synced(i) = true;
            end
        end
        
        oldnodeinds = nodeinds; % temp
        
        % remove nodes that are already synced
        state(cat(1,obj.node2stateinds{nodeinds(synced)})) = cat(1,relval{synced});
        statevar(cat(1,obj.node2stateinds{nodeinds(synced)})) = cat(1,relvar{synced});
        relval(synced) = [];
        relvar(synced) = [];
        nodeinds(synced) = [];
        
        if isempty(nodeinds)
            obj.choicestate(:,choiceind) = state;
            obj.choicestatevar(:,choiceind) = statevar;
            obj.choicenodevisited(:,choiceind) = visitedmask;
            obj.choicegroupvisited(:,choiceind) = groupvisitedmask;
            obj.choicenodesync(:,choiceind) = syncmask;
            obj.choicedecision(:,choiceind) = decisionmask;
            
%             obj.currentnodeinds = nodeinds;
%             obj.currentvisitedmask = visitedmask;
%             obj.currentsyncmask = syncmask;
%             obj.fullstatevar = statevar;
%             obj.choicecost(choiceind) = obj.eval_regularization_visited(state);
            obj.choicecost(choiceind) = obj.computeRegularizer(state,statevar,visitedmask,syncmask,false);%true);
            
            disp(['choice ',num2str(choiceind),' continues: ',num2str(oldnodeinds),' already synced (cost ',num2str(obj.choicecost(choiceind)),')']);
            
            continue;
        end
        
        % all nodes after this point should be relationship nodes (element
        % nodes cannot be unsynced)
        if any(isnan(obj.pgraph.node2relinds(nodeinds)))
            error('Some element nodes are on a graph level > 1, this should not happen.');
        end
        relval = cat(1,relval{:});
        relvar = cat(1,relvar{:});
        
        % try to bring nodes in sync by updating them to the value of
        % their child nodes
        syncedparentnodeinds = cell(1,numel(nodeinds));
        syncedparentsstillsynced = cell(1,numel(nodeinds));
        for i=1:numel(nodeinds)
            nind = nodeinds(i);

            syncedparentnodeinds{i} = obj.parentnodeinds{nind};
            syncedparentnodeinds{i}(not(syncmask(syncedparentnodeinds{i}))) = [];

            syncedparentsstillsynced{i} = false(1,numel(syncedparentnodeinds{i}));
            for j=1:numel(syncedparentnodeinds{i})
                pind = syncedparentnodeinds{i}(j);
                prelind = obj.pgraph.node2relinds(pind);
                pstateind = obj.node2stateinds{pind};

                input1nodeind = find(obj.pgraph.adjacency(pind,:) == -1);
                input2nodeind = find(obj.pgraph.adjacency(pind,:) == 1);

                tempstate = state;
                if input1nodeind == nind
                    tempstate(obj.node2stateinds{input1nodeind}) = relval(i);
                elseif input2nodeind == nind
                    tempstate(obj.node2stateinds{input2nodeind}) = relval(i);
                else
                    error('Invalid pattern graph, parents do not match childs.')
                end

                [newparentval,~] = obj.computerel(tempstate,pind);

                residual = newparentval - state(pstateind);
                if obj.pgraph.rels(prelind).iscircular
                    residual = smod(residual,-pi,pi);
                end
                if (residual^2 / statevar(pstateind)) * obj.relweights(prelind) < obj.synctolerance;
                    syncedparentsstillsynced{i}(j) = true;
                end
            end

        end
        
        % => even if all parents remain synced, we might still want to
        % update either the node itself or its childs (e.g. the case of a
        % top-level node in the graph which has no parents, we might want
        % to update the node or keep it and update the children instead)
%         % if all synced parents (visited and unvisited) remain synced,
%         % do the update (without introducing the choice to optimize the
%         % children)
%         allparentsstillsynced = cellfun(@(x) all(x),syncedparentsstillsynced);
%         state(cat(1,obj.node2stateinds{nodeinds(allparentsstillsynced)})) = relval(allparentsstillsynced);
%         statevar(cat(1,obj.node2stateinds{nodeinds(allparentsstillsynced)})) = relvar(allparentsstillsynced);
%         parentnodeinds(allparentsstillsynced) = [];
%         childnodeinds(allparentsstillsynced) = [];
%         relval(allparentsstillsynced) = [];
%         relvar(allparentsstillsynced) = [];
%         nodeinds(allparentsstillsynced) = [];
% 
%         if isempty(nodeinds)
%             obj.choicestate(:,choiceind) = state;
%             obj.choicestatevar(:,choiceind) = statevar;
%             obj.choicenodevisited(:,choiceind) = visitedmask;
%             obj.choicenodesync(:,choiceind) = syncmask;
%             obj.choicedecision(:,choiceind) = decisionmask;
% 
% %             obj.currentnodeinds = nodeinds;
% %             obj.currentvisitedmask = visitedmask;
% %             obj.currentsyncmask = syncmask;
% %             obj.choicecost(choiceind) = obj.eval_regularization_visited(state);
%             obj.choicecost(choiceind) = obj.computeRegularizer(state,statevar,visitedmask,syncmask,false);%true);
%             continue;
%         end

        % for all cases where a node update would unsync the synced visited
        % parents, optimize the child nodes only
        visitedparentsunsynced = false(1,numel(nodeinds));
        for i=1:numel(nodeinds)
            visitedparentsunsynced(i) = not(all(syncedparentsstillsynced{i}(visitedmask(syncedparentnodeinds{i}))));
        end
        
        if any(visitedparentsunsynced)
            optimchildnodeinds = [obj.childnodeinds{nodeinds(visitedparentsunsynced)}];
            if not(all(visitedmask(optimchildnodeinds)))
                optimchildnodeinds(visitedmask(optimchildnodeinds)) = [];
            end

            [state,statevar,objectiveval,allpnodeinds,visitpnodeinds,visitpunsync] = obj.optimizenodes(...
                state,statevar,optimchildnodeinds,visitedmask,syncmask);

            % mark all unvisited parents of the child nodes as unsynced
            % (visited parents were used as constraints)
            unvisitpnodeinds = setdiff(allpnodeinds,visitpnodeinds);
            syncmask(unvisitpnodeinds) = false;
            % mark non-element optimized child nodes as unsynced
            syncmask(optimchildnodeinds(nodelevel(optimchildnodeinds) > 0)) = false;

            oldnodeinds = nodeinds; % temp
            
%             % mark optimized child nodes as visited (only if element nodes)
%             visitedmask(optimchildnodeinds(nodelevel(optimchildnodeinds) == 0)) = true;
            
%             parentnodeinds(visitedparentsunsynced) = [];
%             childnodeinds(visitedparentsunsynced) = [];
            relval(visitedparentsunsynced) = [];
            relvar(visitedparentsunsynced) = [];
            nodeinds(visitedparentsunsynced) = [];

            if objectiveval > obj.synctolerance * numel(visitpnodeinds) || any(visitpunsync > obj.synctolerance)
                % no valid state can be found for some of the nodes, this
                % choice path is not valid and should be stopped
                obj.choicestate(:,choiceind) = state;
                obj.choicestatevar(:,choiceind) = statevar;
                obj.choicenodevisited(:,choiceind) = visitedmask;
                obj.choicegroupvisited(:,choiceind) = groupvisitedmask;
                obj.choicenodesync(:,choiceind) = syncmask;
                obj.choicedecision(:,choiceind) = decisionmask;
                obj.choicecost(choiceind) = inf;
                obj.choiceclosed(choiceind) = true;
                
                disp(['choice ',num2str(choiceind),' ends: ',num2str(nodeinds),' optimize childs (cost ',num2str(obj.choicecost(choiceind)),')']);
                
                continue;
            end

            if isempty(nodeinds)
                obj.choicestate(:,choiceind) = state;
                obj.choicestatevar(:,choiceind) = statevar;
                obj.choicenodevisited(:,choiceind) = visitedmask;
                obj.choicegroupvisited(:,choiceind) = groupvisitedmask;
                obj.choicenodesync(:,choiceind) = syncmask;
                obj.choicedecision(:,choiceind) = decisionmask;
                
%                 obj.currentnodeinds = nodeinds;
%                 obj.currentvisitedmask = visitedmask;
%                 obj.currentsyncmask = syncmask;
%                 obj.fullstatevar = statevar;
%                 obj.choicecost(choiceind) = obj.eval_regularization_visited(state);
                obj.choicecost(choiceind) = obj.computeRegularizer(state,statevar,visitedmask,syncmask,false);%true);
                
                disp(['choice ',num2str(choiceind),' continues: ',num2str(oldnodeinds),' optimize childs (cost ',num2str(obj.choicecost(choiceind)),')']);
                
                continue;
            end
        
        end

        % for all other cases: all synced visited parents remain synced,
        % but some synced unvisited parents do not, add two choices:
        % update node or optimize child nodes
        
        % end the current choice path
        obj.choicestate(:,choiceind) = state;
        obj.choicestatevar(:,choiceind) = statevar;
        obj.choicenodevisited(:,choiceind) = visitedmask;
        obj.choicegroupvisited(:,choiceind) = groupvisitedmask;
        obj.choicenodesync(:,choiceind) = syncmask;
        obj.choicedecision(:,choiceind) = decisionmask;
        
%         obj.currentnodeinds = nodeinds;
%         obj.currentvisitedmask = visitedmask;
%         obj.currentsyncmask = syncmask;
%         obj.fullstatevar = statevar;
%         obj.choicecost(choiceind) = obj.eval_regularization_visited(state);
        obj.choicecost(choiceind) = obj.computeRegularizer(state,statevar,visitedmask,syncmask,false);%true);
        obj.choiceclosed(choiceind) = true;
        
        disp(['choice ',num2str(choiceind),' branches into: ']);
        
        % add two new choice paths for each node separately (while keeping
        % the other nodes unchanged)
        for i=1:numel(nodeinds)
            
            % decision 1: update node value and mark unvisited parents as
            % unsynced
            if true % && (any(decisionmask(nodeinds(i)) == 0) || any(decisionmask(nodeinds(i)) == 1))
                choice1state = state;
                choice1statevar = statevar;
                choice1visitedmask = visitedmask;
                choice1groupvisitedmask = groupvisitedmask;
                choice1syncmask = syncmask;
                choice1decisionmask = decisionmask;
                
                % all other nodes remain unchanged
                choice1visitedmask(nodeinds) = wasvisited(nodeinds);
                choice1syncmask(nodeinds) = wassynced(nodeinds);
                
                choice1visitedmask(nodeinds(i)) = true;
                choice1syncmask(nodeinds(i)) = true;
%                 choice1decisionmask(nodeinds(i)) = 1; % enable here
                choice1decisionmask(nodeinds(i)) = 0;
                
                choice1state(obj.node2stateinds{nodeinds(i)}) = relval(i);
                choice1statevar(obj.node2stateinds{nodeinds(i)}) = relvar(i);
                
                % unvisited parent nodes might be unsynced by the update
                unvisitedparentnodeinds = obj.parentnodeinds{nodeinds(i)}(not(visitedmask(obj.parentnodeinds{nodeinds(i)})));
                choice1syncmask(unvisitedparentnodeinds) = false;
                
                obj.choicestate(:,end+1) = choice1state;
                obj.choicestatevar(:,end+1) = choice1statevar;
                obj.choicenodevisited(:,end+1) = choice1visitedmask;
                obj.choicegroupvisited(:,end+1) = choice1groupvisitedmask;
                obj.choicenodesync(:,end+1) = choice1syncmask;
                obj.choicedecision(:,end+1) = choice1decisionmask;
                obj.choicetree(end+1,:) = 0;
                obj.choicetree(:,end+1) = 0;
                obj.choicetree(end,choiceind) = 1;
                obj.choiceclosed(end+1) = false;
                
%                 obj.currentnodeinds = nodeinds;
%                 obj.currentvisitedmask = choice1visitedmask;
%                 obj.currentsyncmask = choice1syncmask;
%                 obj.fullstatevar = statevar;
%                 obj.choicecost(end+1) = obj.eval_regularization_visited(choice1state);
                obj.choicecost(end+1) = obj.computeRegularizer(...
                    choice1state,choice1statevar,choice1visitedmask,choice1syncmask,false);%true);
                
                disp(['---> choice ',num2str(numel(obj.choicecost)),' starts: ',num2str(nodeinds),' update node (cost ',num2str(obj.choicecost(end)),')']);
            end
            
            % decision 2: optimize node childs and mark non-element child
            % nodes as well as unvisited parents of the childs as unsynced
            if true % && (any(decisionmask(nodeinds(i)) == 0) || any(decisionmask(nodeinds(i)) == 2))
                choice2state = state;
                choice2statevar = statevar;
                choice2visitedmask = visitedmask;
                choice2groupvisitedmask = groupvisitedmask;
                choice2syncmask = syncmask;
                choice2decisionmask = decisionmask;
                
                % all other nodes remain unchanged
                choice2visitedmask(nodeinds) = wasvisited(nodeinds);
                choice2syncmask(nodeinds) = wassynced(nodeinds);
                
                choice2visitedmask(nodeinds(i)) = true;
                choice2syncmask(nodeinds(i)) = true;
%                 choice2decisionmask(nodeinds(i)) = 2; % enable here
                choice2decisionmask(nodeinds(i)) = 0;

                optimchildnodeinds = obj.childnodeinds{nodeinds(i)};
                if not(all(visitedmask(optimchildnodeinds)))
                    optimchildnodeinds(visitedmask(optimchildnodeinds)) = [];
                end
                [choice2state,choice2statevar,objectiveval,allpnodeinds,visitpnodeinds,visitpunsync] = obj.optimizenodes(...
                    choice2state,choice2statevar,optimchildnodeinds,choice2visitedmask,choice2syncmask);
                
                % mark all unvisited parents of the child nodes as unsynced
                % (visited parents were used as constraints)
                unvisitpnodeinds = setdiff(allpnodeinds,visitpnodeinds);
                choice2syncmask(unvisitpnodeinds) = false;
                % mark non-element optimized child nodes as unsynced
                choice2syncmask(optimchildnodeinds(nodelevel(optimchildnodeinds) > 0)) = false;
                
                % todo: if visited parent nodes were unsynced (because they
                % were set as group), the objectiveval mey be large because
                % these visited parent nodes were not synced to begin with
                
%                 % mark optimized child nodes as visited (only if element nodes)
%                 choice2visitedmask(optimchildnodeinds(nodelevel(optimchildnodeinds) == 0)) = true;

                obj.choicestate(:,end+1) = choice2state;
                obj.choicestatevar(:,end+1) = choice2statevar;
                obj.choicenodevisited(:,end+1) = choice2visitedmask;
                obj.choicegroupvisited(:,end+1) = choice2groupvisitedmask;
                obj.choicenodesync(:,end+1) = choice2syncmask;
                obj.choicedecision(:,end+1) = choice2decisionmask;
                obj.choicetree(end+1,:) = 0;
                obj.choicetree(:,end+1) = 0;
                obj.choicetree(end,choiceind) = 1;
                if objectiveval > obj.synctolerance * numel(visitpnodeinds) || any(visitpunsync > obj.synctolerance)
                    % the new choice path is immediately invalid
                    obj.choiceclosed(end+1) = true;
                    obj.choicecost(end+1) = inf;
                    
                    disp(['---> choice ',num2str(numel(obj.choicecost)),' starts & ends: ',num2str(nodeinds),' optimize childs (cost ',num2str(obj.choicecost(end)),')']);
                else
                    obj.choiceclosed(end+1) = false;
                    
%                     obj.currentnodeinds = nodeinds;
%                     obj.currentvisitedmask = choice2visitedmask;
%                     obj.currentsyncmask = choice2syncmask;
%                     obj.fullstatevar = statevar;
%                     obj.choicecost(end+1) = obj.eval_regularization_visited(choice2state);
                    obj.choicecost(end+1) = obj.computeRegularizer(...
                        choice2state,choice2statevar,choice2visitedmask,choice2syncmask,false);%true);
                    
                    disp(['---> choice ',num2str(numel(obj.choicecost)),' starts: ',num2str(nodeinds),' optimize childs (cost ',num2str(obj.choicecost(end)),')']);
                end
            end
            
            % decision 3: update node value and all group members of
            % unvisited groups and mark unvisited parents, as well as group
            % members and their unvisited parents as unsynced
            % (a group is unvisited if it has not been used to propagate
            % a value to all its members yet)
            if strcmp(obj.groupmode,'explicit')
                [grpmembernodeinds,grpinds] = obj.pgraph.groupmembers(nodeinds(i),find(not(groupvisitedmask))); %#ok<FNDSB>
                if numel(grpmembernodeinds) > 1 % && (any(decisionmask(nodeinds(i)) == 0) || any(decisionmask(nodeinds(i)) == 3))

                    choice3state = state;
                    choice3statevar = statevar;
                    choice3visitedmask = visitedmask;
                    choice3groupvisitedmask = groupvisitedmask;
                    choice3syncmask = syncmask;
                    choice3decisionmask = decisionmask;

                    choice3groupvisitedmask(grpinds) = true;

                    % all other nodes remain unchanged
                    choice3visitedmask(nodeinds) = wasvisited(nodeinds);
                    choice3syncmask(nodeinds) = wassynced(nodeinds);

                    choice3visitedmask(nodeinds(i)) = true;
                    choice3syncmask(nodeinds(i)) = true;
    %                 choice3decisionmask(nodeinds(i)) = 1; % enable here
                    choice3decisionmask(nodeinds(i)) = 0;

    %                 choice3state(obj.node2stateinds{nodeinds(i)}) = relval(i);
    %                 choice3statevar(obj.node2stateinds{nodeinds(i)}) = relvar(i);
                    choice3state(cat(1,obj.node2stateinds{grpmembernodeinds})) = relval(i);
                    choice3statevar(cat(1,obj.node2stateinds{grpmembernodeinds})) = relvar(i);

                    % unvisited parent nodes of all group members might be
                    % unsynced by the update, as well as the group members
                    % themselves
                    unvisitedparentnodeinds = obj.parentnodeinds{grpmembernodeinds};
                    unvisitedparentnodeinds = unvisitedparentnodeinds(not(visitedmask(unvisitedparentnodeinds)));
                    choice3syncmask(unvisitedparentnodeinds) = false;
                    choice3syncmask(grpmembernodeinds(grpmembernodeinds~=nodeinds(i))) = false;
                    choice3visitedmask(grpmembernodeinds) = true;


                    obj.choicestate(:,end+1) = choice3state;
                    obj.choicestatevar(:,end+1) = choice3statevar;
                    obj.choicenodevisited(:,end+1) = choice3visitedmask;
                    obj.choicegroupvisited(:,end+1) = choice3groupvisitedmask;
                    obj.choicenodesync(:,end+1) = choice3syncmask;
                    obj.choicedecision(:,end+1) = choice3decisionmask;
                    obj.choicetree(end+1,:) = 0;
                    obj.choicetree(:,end+1) = 0;
                    obj.choicetree(end,choiceind) = 1;
                    obj.choiceclosed(end+1) = false;
                    
%                     obj.currentnodeinds = nodeinds;
%                     obj.currentvisitedmask = choice3visitedmask;
%                     obj.currentsyncmask = choice3syncmask;
%                     obj.fullstatevar = statevar;
%                     obj.choicecost(end+1) = obj.eval_regularization_visited(choice3state);
                    obj.choicecost(end+1) = obj.computeRegularizer(...
                        choice3state,choice3statevar,choice3visitedmask,choice3syncmask,false);%true);

                    disp(['---> choice ',num2str(numel(obj.choicecost)),' starts: ',num2str(nodeinds),' update groups (cost ',num2str(obj.choicecost(end)),')']);
                end
            end
        end
        

    end
    
    % non-leaf nodes are only partial solutions
    obj.solutionchoiceinds = find(sum(abs(obj.choicetree),1)==0 & obj.choiceclosed & not(isinf(obj.choicecost)));
    obj.solutioncost = obj.choicecost(obj.solutionchoiceinds);
    obj.solutionstate = obj.choicestate(:,obj.solutionchoiceinds);
    obj.solutionstatevar = obj.choicestatevar(:,obj.solutionchoiceinds);
    
%     elmoffset = obj.pgraph.elmoffset;
%     elmstateinds = cat(1,obj.node2stateinds{elmoffset+1 : elmoffset+numel(obj.pgraph.elms)});
%     relstateinds = cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds(1:numel(obj.pgraph.rels))});
%     
%     obj.solutionelmposes = cell(1,size(obj.solutionstate,2));
%     obj.solutionrelvals = cell(1,size(obj.solutionstate,2));
%     for i=1:size(obj.solutionstate,2)
%         obj.solutionelmposes{i} = reshape(obj.solutionstate(elmstateinds,i),obj.elmposedim,numel(obj.pgraph.elms));
%         obj.solutionrelvals{i} = obj.solutionstate(relstateinds,i);
%     end
    
    % get element poses and relationship values from states
    obj.solutionelmposes = cell(1,size(obj.solutionstate,2));
    obj.solutionrelvals = cell(1,size(obj.solutionstate,2));
    for i=1:size(obj.solutionstate,2)
        eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
        eposes(1:obj.elmposedim,:) = reshape(obj.solutionstate(obj.elmposestateinds(:),i),obj.elmposedim,numel(obj.pgraph.elms));
        eposes = PatternSolver.sim2affinepose(eposes);
        obj.solutionelmposes{i} = eposes;
        obj.solutionrelvals{i} = obj.solutionstate(obj.relstateinds,i)';
    end
    
    [~,perm] = sort(obj.solutioncost,'ascend');
    obj.solutionchoiceinds = obj.solutionchoiceinds(perm);
    obj.solutioncost = obj.solutioncost(perm);
    obj.solutionstate = obj.solutionstate(:,perm);
    obj.solutionstatevar = obj.solutionstatevar(:,perm);
    obj.solutionelmposes = obj.solutionelmposes(perm);
    obj.solutionrelvals = obj.solutionrelvals(perm);
    
    if size(obj.solutionstate,2) > 1
        clusterthreshold = 0.4;
        keepinds = StandardPatternSolver.findUniqueStates(...
            obj.solutionstate,obj.solutionstatevar,clusterthreshold);

        obj.solutionchoiceinds = obj.solutionchoiceinds(keepinds);
        obj.solutioncost = obj.solutioncost(keepinds);
        obj.solutionstate = obj.solutionstate(:,keepinds);
        obj.solutionstatevar = obj.solutionstatevar(:,keepinds);
        obj.solutionelmposes = obj.solutionelmposes(keepinds);
        obj.solutionrelvals = obj.solutionrelvals(keepinds);
    end
    
    
    selmposes = obj.solutionelmposes;
    srelvals = obj.solutionrelvals;
    scost = obj.solutioncost;
    
    if strcmp(obj.groupmode,'implicit')
        obj.pgraph.removeImplicitRelgroups;
    end
end

function cost = computeRegularizer(obj,state,statevar,visitedmask,syncmask,onlyvisited)
    
    obj.fullstatevar = statevar;
    obj.currentvisitedmask = visitedmask;
    obj.currentsyncmask = syncmask;
    
    cost = obj.evalregularization(state,onlyvisited);
%     if onlyvisited
%         cost = obj.eval_regularization_visited(state);
%     else
%         cost = obj.eval_regularization_full(state);
%     end
end

function duplicatechoiceinds = findDuplicateChoices(obj,state,visitedmask,changedmask,queuednodeops)
    duplicatechoiceinds = find(...
        all(bsxfun(@eq,obj.choicestate,state),1) & ...
        all(bsxfun(@eq,obj.choicenodevisited,visitedmask),1) & ...    
        all(bsxfun(@eq,obj.choicechangednodesmask,changedmask),1) & ...
        cellfun(@(x) numel(x) == numel(queuednodeops) && all(x == queuednodeops),obj.choicequeuednodeops));
end

function setVisfunction(obj,func)
    obj.callbackfun = func;
end

function showState(obj,substate,substateinds,state,statevar,nodeinds,visitedmask,syncmask)
    obj.fullstate = state;
    obj.fullstatevar = statevar;
    obj.substateinds = substateinds;
    obj.currentnodeinds = nodeinds;
    obj.currentvisitedmask = visitedmask;
    obj.currentsyncmask = syncmask;
    
    obj.stop = obj.outfun(substate,[],[]); 
end

function stop = outfun(obj,substate,optimValues,optimizerstateflag)
%     stop = false;
%     return; % temp
    
    state = obj.fullstate;
    state(obj.substateinds) = substate;
    
%     if not(isempty(optimValues))
%         disp(['*** iteration ',num2str(optimValues.iteration),':']);
%         disp(['    objective function value: ',num2str(optimValues.fval)]);
%     end
    
%     disp('    gradient:');
%     disp(optimValues.gradient);
    
    if not(isempty(obj.callbackfun))
        eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);

        elmoffset = obj.pgraph.elmoffset;
        elmstateinds = cat(1,obj.node2stateinds{elmoffset+1 : elmoffset+numel(obj.pgraph.elms)});
        
        eposes(1:obj.elmposedim,:) = reshape(state(elmstateinds),obj.elmposedim,[]);
        
        nongrpmask = true(1,numel(obj.currentvisitedmask));
        offset = obj.pgraph.grpnodeoffset;
        nongrpmask(offset+1:offset+numel(obj.pgraph.grpnodes)) = false;
        offset = obj.pgraph.grpreloffset;
        nongrpmask(offset+1:offset+numel(obj.pgraph.grprels)) = false;
        
        eposes = PatternSolver.sim2affinepose(eposes);
        
        stop = obj.callbackfun(...
            obj,...
            eposes,...
            obj.currentvisitedmask(nongrpmask),...
            obj.currentsyncmask(nongrpmask),...
            double(obj.currentvisitedmask(nongrpmask)).*2-1,...
            obj.currentnodeinds(nongrpmask(obj.currentnodeinds)));
        
    else
        stop = false;
    end
end

% function val = eval_regularization(obj,state)
% %     val = eval_regularization_full(obj,state);
%     val = eval_regularization_visited(obj,state);
% end

function val = evalregularization(obj,state,onlyvisited)
    
    state = state';
    val = 0;
    
    nrels = numel(obj.pgraph.rels);
    nelms = numel(obj.pgraph.elms);
    
    if nargin < 3
        onlyvisited = false;
    end
    
    if strcmp(obj.groupmode,'implicit')
        % do not use the nodes and relationships of the implicit groups
        % groups are included in a separate term
        
        relmask = false(1,nrels);
        relmask(obj.pgraph.node2relinds([...
            obj.pgraph.elmreloffset+1 : obj.pgraph.elmreloffset+numel(obj.pgraph.elmrels),...
            obj.pgraph.metareloffset+1 : obj.pgraph.metareloffset+numel(obj.pgraph.metarels)])) = true;
        elmmask = true(1,nelms);
    else
        relmask = true(1,nrels);
        elmmask = true(1,nelms);
    end
    
    if onlyvisited
        % only consider visited relationships and elements
        relmask(not(obj.currentvisitedmask(obj.pgraph.rel2nodeinds(relmask)))) = false;
        elmmask(not(obj.currentvisitedmask(find(elmmask)+obj.pgraph.elmoffset))) = false;
    end

    rvals = nan(1,nrels);
    rvars = nan(1,nrels);
    
    for i=1:numel(obj.relfs)
        
        mask = relmask(obj.relfrelinds{i});
        
        if any(mask)
        
            % evaluate all relationships functions of one type
            [v,var] = obj.relfs{i}(...
                reshape(state(obj.relfinput1inds{i}(:,mask)),size(obj.relfinput1inds{i}(:,mask))),...
                reshape(state(obj.relfinput2inds{i}(:,mask)),size(obj.relfinput2inds{i}(:,mask))));

%             residual = v-state(obj.relfoutputinds{i}(mask));
%             if obj.relfcircular(i)
%                 residual = smod(residual,-pi,pi);
%             end
%             unsync = (residual.^2 ./ var) * obj.relweights(obj.relfrelinds{i}(mask))';
%             val = val + unsync * 1000; % weighted mahalanobis distance

            rvals(obj.relfrelinds{i}(mask)) = v;
            rvars(obj.relfrelinds{i}(mask)) = var;
        end
    end
    
    origrvals = [obj.pgraph.rels.value];
    origrvars = [obj.pgraph.rels.variance];
    
%     % how many relationships changed (change in relationships normalized by variance)
%     relchange = min(1,abs(rvals(relmask) - origrvals(relmask))./(sqrt(origrvars(relmask)).*0.5));
%     val = val + sum(relchange)^2 * 0.05;
    
%     synctolerance = 0.1;
    maxunsync = 5; % in multiples of the synctolerance
    
    residual(relmask) = rvals(relmask) - origrvals(relmask);
    residual(not(relmask)) = 0;
    for i=1:numel(obj.relgrprelinds)
        if not(isempty(obj.relgrprelinds{i})) % && all(relmask(obj.relgrprelinds{i}))
            maxrelchange = max(residual(obj.relgrprelinds{i}).^2 ./ origrvars(obj.relgrprelinds{i}) );
            val = val + min(maxunsync,(maxrelchange / obj.synctolerance)) * 10 ;
        end
    end
    
%         % sum of average element changes in all element groups (or how many
%         % elment groups changed)
%         elmposediff = obj.oldelmposes - reshape(...
%                 state(obj.elmposestateinds),size(obj.elmposestateinds));
%         for i=1:numel(obj.elmgrpelminds)
%             if not(isempty(obj.elmgrpelminds{i})) % && not(any(activemask(obj.elmgrpelminds{i})))
%                 val = val + mean(sum(elmposediff(:,obj.elmgrpelminds{i}).^2,1) ./ obj.elmposevar);
%             end
%         end
        
    residual = zeros(obj.elmposedim,nelms);
    residual(:,elmmask) = obj.oldelmposes(:,elmmask) - reshape(...
            state(obj.elmposestateinds(:,elmmask)),size(obj.elmposestateinds(:,elmmask)));
    residual(:,not(elmmask)) = 0;
    for i=1:numel(obj.elmgrpelminds)
        if not(isempty(obj.elmgrpelminds{i})) % && all(elmmask(obj.elmgrpelminds{i}))
            maxelmchange = max( sum(bsxfun(@rdivide,residual(:,obj.elmgrpelminds{i}).^2,obj.elmposevar),1) );
            val = val + min(maxunsync,(maxelmchange / obj.synctolerance)) * 50;
        end
    end
        
        
%     for i=1:numel(obj.relgrprelinds)
%         if numel(obj.relgrprelinds{i}) > 1 && all(relmask(obj.relgrprelinds{i}))
%             grpstateinds = cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds(obj.relgrprelinds{i})});
%             grprelvals = state(grpstateinds);
%             if obj.relgrpcircular(i)
%                 grpavg = circ_mean(grprelvals,[],2);
%             else
%                 grpavg = mean(grprelvals);
%             end
%             residual = grprelvals - grpavg;
%             if obj.relgrpcircular(i)
%                 residual = smod(residual,-pi,pi);
%             end
% 
% %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*9999;
% %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*1000;
%             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*10;
% %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i})) * mean(1-changedrelmask(obj.relgrprelinds{i})) * 100;
% %             val = val + sum(((residual).^2 ./ rvars(obj.relgrprelinds{i})) .* rellagrangemult_combined(obj.relgrprelinds{i})) * 100;
%         end
%     end

        for i=1:numel(obj.relgrprelinds)
            if numel(obj.relgrprelinds{i}) > 1 && all(relmask(obj.relgrprelinds{i}))
                grpstateinds = cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds(obj.relgrprelinds{i})});
                grprelvals = state(grpstateinds);
                grprelvar = obj.fullstatevar(grpstateinds)';
                if obj.relgrpcircular(i)
                    grpavg = circ_mean(grprelvals,[],2);
                else
                    grpavg = mean(grprelvals);
                end
                residual = grprelvals - grpavg;
                if obj.relgrpcircular(i)
                    residual = smod(residual,-pi,pi);
                end
                
                maxrelchange = max(residual.^2 ./ grprelvar);
                val = val + min(maxunsync,(maxrelchange / obj.synctolerance)) * 100;
            end
        end
end

function val = eval_regularization_visited(obj,state)

    state = state';
    val = 0;
    
    nrels = numel(obj.pgraph.rels);
    
    % relationships that have been fixed
    nongrpnodemask = false(size(obj.currentvisitedmask));
    nongrpnodemask([...
            obj.pgraph.elmoffset+1 : obj.pgraph.elmoffset+numel(obj.pgraph.elms),...        
            obj.pgraph.elmreloffset+1 : obj.pgraph.elmreloffset+numel(obj.pgraph.elmrels),...
            obj.pgraph.metareloffset+1 : obj.pgraph.metareloffset+numel(obj.pgraph.metarels)]) = true;
    
    nodefixedmask = obj.currentvisitedmask & obj.currentsyncmask & nongrpnodemask;
    relfixedmask = nodefixedmask(obj.pgraph.rel2nodeinds(1:nrels));
    statefixedmask = nodefixedmask(obj.state2nodeinds(1:numel(state)));
    elmfixedmask = nodefixedmask(obj.pgraph.elmoffset+1:obj.pgraph.elmoffset+numel(obj.pgraph.elms));
    
    origrvals = [obj.pgraph.rels.value];
    origrvars = [obj.pgraph.rels.variance];
    
    rvals = nan(1,nrels);
    rvars = nan(1,nrels);
    for i=1:numel(obj.relfs)
        
        mask = relfixedmask(obj.relfrelinds{i});
        
        if any(mask)
            % evaluate all relationships functions of one type
            [v,var] = obj.relfs{i}(...
                reshape(state(obj.relfinput1inds{i}(:,mask)),size(obj.relfinput1inds{i}(:,mask))),...
                reshape(state(obj.relfinput2inds{i}(:,mask)),size(obj.relfinput2inds{i}(:,mask))));

            rvals(obj.relfrelinds{i}(:,mask)) = v;
            rvars(obj.relfrelinds{i}(:,mask)) = var;
        end
    end
    
%     rvals = rvals(relfixedmask);
%     rvars = rvars(relfixedmask);
%     origrvals = origrvals(relfixedmask);
%     origrvars = origrvars(relfixedmask);
    
    % total change in the relationship values
    relchange = min(1,abs(rvals(relfixedmask) - origrvals(relfixedmask))./(sqrt(origrvars(relfixedmask)).*0.5));
    val = val + sum(relchange)^2 * 0.05;
    
    % how many elment groups changed (sum of average element changes in all element groups)
    % only fixed elements are counted but divided by total number of group members (fixed and unfixed) 
    if any(elmfixedmask)
%         elmposediff = obj.oldelmposes(:,elmfixedmask) - reshape(...
%                 state(obj.elmposestateinds(:,elmfixedmask)),size(obj.elmposestateinds(:,elmfixedmask)));
        elmposediff = obj.oldelmposes - reshape(...
                state(obj.elmposestateinds),size(obj.elmposestateinds));
        for i=1:numel(obj.elmgrpelminds)
            fixedgrpelminds = obj.elmgrpelminds{i}(elmfixedmask(obj.elmgrpelminds{i}));
            if not(isempty(fixedgrpelminds)) % && not(any(activemask(obj.elmgrpelminds{i})))
                val = val + sum(sum(bsxfun(@rdivide,elmposediff(:,fixedgrpelminds).^2,obj.elmposevar),1)) ./ numel(obj.elmgrpelminds{i});
    %             val = val + mean(1 ./ (1 + exp(-sqrt(sum(elmposediff.^2,1) ./ obj.elmposevar) ./ 2))) * 100;
            end
        end
    end
    
    % how many element groups were broken (residual of the group members
    % to the group average)
    if any(relfixedmask)
        for i=1:numel(obj.relgrprelinds)

            if numel(obj.relgrprelinds{i}) > 1 && all(relfixedmask(obj.relgrprelinds{i}))
                grprelvals = state(cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds(obj.relgrprelinds{i})}));
                if obj.relgrpcircular(i)
                    grpavg = circ_mean(grprelvals,[],2);
                else
                    grpavg = mean(grprelvals);
                end
                residual = grprelvals - grpavg;
                if obj.relgrpcircular(i)
                    residual = smod(residual,-pi,pi);
                end
    %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*9999;
%                 val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*1000;
                val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*1;
    %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i})) * mean(1-changedrelmask(obj.relgrprelinds{i})) * 100;
    %             val = val + sum(((residual).^2 ./ rvars(obj.relgrprelinds{i})) .* rellagrangemult_combined(obj.relgrprelinds{i})) * 100;
            end
        end
    end
    
end

% todo: elements are never visited

function val = eval_regularization_full(obj,state)
    
    state = state';
    val = 0;
    
    nrels = numel(obj.pgraph.rels);
    
    if strcmp(obj.groupmode,'implicit')
        % do not use the nodes and relationships of the implicit groups
        % groups are included in a separate term
        
        relmask = false(1,nrels);
        relmask(obj.pgraph.node2relinds([...
            obj.pgraph.elmreloffset+1 : obj.pgraph.elmreloffset+numel(obj.pgraph.elmrels),...
            obj.pgraph.metareloffset+1 : obj.pgraph.metareloffset+numel(obj.pgraph.metarels)])) = true;
    else
        relmask = true(1,nrels);
    end

    rvals = nan(1,nrels);
    rvars = nan(1,nrels);
    
    for i=1:numel(obj.relfs)
        % evaluate all relationships functions of one type
        [v,var] = obj.relfs{i}(...
            reshape(state(obj.relfinput1inds{i}),size(obj.relfinput1inds{i})),...
            reshape(state(obj.relfinput2inds{i}),size(obj.relfinput2inds{i})));
        
        rvals(obj.relfrelinds{i}) = v;
        rvars(obj.relfrelinds{i}) = var;
    end
    
    origrvals = [obj.pgraph.rels.value];
    origrvars = [obj.pgraph.rels.variance];
    
%     % how many relationships changed (change in relationships normalized by variance)
%     relchange = min(1,abs(rvals(relmask) - origrvals(relmask))./(sqrt(origrvars(relmask)).*0.5));
%     val = val + sum(relchange)^2 * 0.05;
    
%     synctolerance = 0.1;
    maxunsync = 5; % in multiples of the synctolerance
    
    residual = rvals(relmask) - origrvals(relmask);
    residual(not(relmask)) = 0;
    for i=1:numel(obj.relgrprelinds)
        if not(isempty(obj.relgrprelinds{i})) % && all(relmask(obj.relgrprelinds{i}))
            maxrelchange = max(residual(obj.relgrprelinds{i}).^2 ./ origrvars(obj.relgrprelinds{i}) );
            val = val + min(maxunsync,(maxrelchange / obj.synctolerance)) * 10 ;
        end
    end
    
%         % sum of average element changes in all element groups (or how many
%         % elment groups changed)
%         elmposediff = obj.oldelmposes - reshape(...
%                 state(obj.elmposestateinds),size(obj.elmposestateinds));
%         for i=1:numel(obj.elmgrpelminds)
%             if not(isempty(obj.elmgrpelminds{i})) % && not(any(activemask(obj.elmgrpelminds{i})))
%                 val = val + mean(sum(elmposediff(:,obj.elmgrpelminds{i}).^2,1) ./ obj.elmposevar);
%             end
%         end
        
    residual = obj.oldelmposes - reshape(...
            state(obj.elmposestateinds),size(obj.elmposestateinds));
    for i=1:numel(obj.elmgrpelminds)
        if not(isempty(obj.elmgrpelminds{i})) % && not(any(activemask(obj.elmgrpelminds{i})))
            maxelmchange = max( sum(bsxfun(@rdivide,residual(:,obj.elmgrpelminds{i}).^2,obj.elmposevar),1) );
            val = val + min(maxunsync,(maxelmchange / obj.synctolerance)) * 50;
        end
    end
        
        
%     for i=1:numel(obj.relgrprelinds)
%         if numel(obj.relgrprelinds{i}) > 1 && all(relmask(obj.relgrprelinds{i}))
%             grpstateinds = cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds(obj.relgrprelinds{i})});
%             grprelvals = state(grpstateinds);
%             if obj.relgrpcircular(i)
%                 grpavg = circ_mean(grprelvals,[],2);
%             else
%                 grpavg = mean(grprelvals);
%             end
%             residual = grprelvals - grpavg;
%             if obj.relgrpcircular(i)
%                 residual = smod(residual,-pi,pi);
%             end
% 
% %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*9999;
% %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*1000;
%             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i}))*10;
% %             val = val + sum((residual).^2 ./ rvars(obj.relgrprelinds{i})) * mean(1-changedrelmask(obj.relgrprelinds{i})) * 100;
% %             val = val + sum(((residual).^2 ./ rvars(obj.relgrprelinds{i})) .* rellagrangemult_combined(obj.relgrprelinds{i})) * 100;
%         end
%     end

        for i=1:numel(obj.relgrprelinds)
            if numel(obj.relgrprelinds{i}) > 1 && all(relmask(obj.relgrprelinds{i}))
                grpstateinds = cat(1,obj.node2stateinds{obj.pgraph.rel2nodeinds(obj.relgrprelinds{i})});
                grprelvals = state(grpstateinds);
                grprelvar = obj.fullstatevar(grpstateinds)';
                if obj.relgrpcircular(i)
                    grpavg = circ_mean(grprelvals,[],2);
                else
                    grpavg = mean(grprelvals);
                end
                residual = grprelvals - grpavg;
                if obj.relgrpcircular(i)
                    residual = smod(residual,-pi,pi);
                end
                
                maxrelchange = max(residual.^2 ./ grprelvar);
                val = val + min(maxunsync,(maxrelchange / obj.synctolerance)) * 100;
            end
        end
end


function val = evalsubstate_nograd(obj,substate)
    
    % todo: for relationships that need other properties of the elements
    % other than their pose (e.g. distance to the boundary): need to move
    % elements to the position given by the state and pass them to the
    % relationship function (boundary vertex positions relative to the pose
    % act like constants in the relationship function)
    
    state = obj.fullstate;
    state(obj.substateinds) = substate;
    state = state';
    val = 0;

%     obj.fullstatevar = nan(numel(state),1);
    
    for i=1:numel(obj.relfs)
        
        activemask = not(isnan(obj.targetrelvals(obj.relfrelinds{i})));
        
        if any(activemask)
            % evaluate all relationships functions of one type
            [v,var] = obj.relfs{i}(...
                reshape(state(obj.relfinput1inds{i}(:,activemask)),size(obj.relfinput1inds{i}(:,activemask))),...
                reshape(state(obj.relfinput2inds{i}(:,activemask)),size(obj.relfinput2inds{i}(:,activemask))));
            residual = v-obj.targetrelvals(obj.relfrelinds{i}(activemask));
            if obj.relfcircular(i)
                residual = smod(residual,-pi,pi);
            end
            unsync = (residual.^2 ./ var) * obj.relweights(obj.relfrelinds{i}(activemask))';
            val = val + unsync; % weighted mahalanobis distance
        
            obj.fullstatevar(obj.relfoutputinds{i}(activemask)) = var;
            obj.fullstateunsync(obj.relfoutputinds{i}(activemask)) = unsync;
        end
    end
    
    % residual between element poses and target poses (squared L2 norm)
%     obj.elmposevar = 0.05.^2;
    activemask = not(any(isnan(obj.targetelmposes),1));
    if any(activemask)
        poseresidual = obj.targetelmposes(1:obj.elmposedim,activemask) - reshape(...
            state(obj.elmposestateinds(:,activemask)),...
            size(obj.elmposestateinds(:,activemask)));
        poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)) = smod(...
            poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)),-pi,pi);
        val = val + sum(bsxfun(@rdivide,poseresidual.^2,obj.elmposevar),1) * obj.elmweights(:,activemask)';
    end
end

end

end
