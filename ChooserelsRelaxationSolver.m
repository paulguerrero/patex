classdef ChooserelsRelaxationSolver < PatternSolver
    
properties(SetAccess=protected)
%     relweights = zeros(1,0);
    
    % last solution
    solutionelmposes = cell(1,0);
    solutionrelvals = cell(1,0);
    solutioncost = zeros(1,0);
    solutionrelactive = cell(1,0);
end

properties(Access=protected)
%     elmtargetposes = zeros(2,0);
%     elmws = zeros(1,0); % one column for each element that is not ignored
    
%     relftargetvals = cell(1,0); % target values for each function (each cell: one column for each individual relationship, one row)
%     relfweights = cell(1,0); % weights for each function (each cell: one column for each individual relationship, one row)

%     changedrelmask = zeros(1,0);
    
    targetelmposes = zeros(2,0);
    targetrelvals = zeros(1,0); % inactive if nan
    targetrelvals_changed = zeros(1,0);
    
    rellagrangeinds_changed = zeros(1,0);

    visfunc = [];
    
    oldelmposes = zeros(2,0);
    
    elmweights = zeros(1,0);
end

methods

% obj = ChooserelsRelaxationSolver
% obj = ChooserelsRelaxationSolver(pgraph)
function obj = ChooserelsRelaxationSolver(varargin)
    superarginds = zeros(1,0);
    if numel(varargin) == 1
        superarginds = 1;
    end
    
    obj@PatternSolver(varargin{superarginds});
    varargin(superarginds) = [];
    
    if numel(varargin) == 0
        % do nothing
    else
        error('Invalid arguments.');
    end
end

function clear(obj)
    obj.clear@PatternSolver;
    
    obj.solutionelmposes = cell(1,0);
    obj.solutionrelvals = cell(1,0);
    obj.solutioncost = zeros(1,0);
    obj.solutionrelactive = cell(1,0);

    obj.targetelmposes = zeros(2,0);
    obj.targetrelvals = zeros(1,0);
    obj.targetrelvals_changed = zeros(1,0); % inactive if not changed
    
    obj.rellagrangeinds_changed = zeros(1,0);
    
    obj.oldelmposes = zeros(2,0);
    
    obj.elmweights = zeros(1,0);
end

function prepare(obj,newelmposes,newrelvals)
    
    obj.prepare@PatternSolver;
    
    rels = obj.pgraph.rels;
    elms = obj.pgraph.elms;
    
    oldrelvals = [rels.value];
    oldrelvars = [rels.variance];
    obj.oldelmposes = [elms.pose];
    obj.oldelmposes = obj.oldelmposes(1:obj.elmposedim,:);
    
    obj.targetrelvals = oldrelvals;
    obj.targetelmposes = nan(size(obj.oldelmposes));
    
    changedelmmask = any(obj.oldelmposes ~= newelmposes(1:obj.elmposedim,:),1);
    obj.targetelmposes(:,changedelmmask) = newelmposes(1:obj.elmposedim,changedelmmask);
    
%     changedrelmask = oldrelvals ~= newrelvals;
    changedrelmask = abs(oldrelvals-newrelvals) > sqrt(oldrelvars).*0.5;
    
    obj.targetrelvals_changed = nan(1,numel(obj.targetrelvals));

    changedrelinds = find(changedrelmask);
    for i=1:numel(changedrelinds)
        grelinds = obj.pgraph.groupmembers(changedrelinds(i)+numel(elms))-numel(elms);
        changedrelmask(grelinds) = true;
        obj.targetrelvals_changed(grelinds) = newrelvals(changedrelinds(i));
    end
    
    %     obj.targetrelvals_changed(changedrelmask) = newrelvals(changedrelmask);
    
    obj.rellagrangeinds_changed = zeros(1,numel(obj.targetrelvals));
    obj.rellagrangeinds_changed(changedrelmask) = numel(obj.targetrelvals)+1:numel(obj.targetrelvals)+sum(changedrelmask);
    
    if any(sum(obj.pgraph.groups,2)>1)
        error('Elements or relationships that are members of more than one group not yet supported.');
    end
   
    % todo: probably better to implement groups as centeral unconstrained
    % node connected to all group members with reldiff relationships (or reldirdiff) with
    % target value = 0
    % and single lagrange multiplier for connections of entire group
    
%     changedreltype = {rels(obj.changedrelinds).type};
%     [~,changedreltypeind] = ismember(changedreltype,obj.relftype);
% 
%     for i=1:numel(obj.changedrelinds)
%         typeind = changedreltypeind(i);
%         typerelind = find(obj.relfrelinds{typeind} == obj.changedrelinds(i),1,'first');
%         
%         obj.relfinput1inds{typeind}(:,end+1) = obj.relfinput1inds{typeind}(:,typerelind);
%         obj.relfinput2inds{typeind}(:,end+1) = obj.relfinput2inds{typeind}(:,typerelind);
%         obj.relfoutputinds{typeind}(:,end+1) = obj.relfoutputinds{typeind}(:,typerelind);
%         obj.relfrelinds{typeind}(:,end+1) = numel(obj.targetrelvals) + 1;
%         
%         obj.targetrelvals(end+1) = newrelvals(obj.changedrelinds(i));
%     end
%     obj.targetrelinds = [1:numel(rels),obj.changedrelinds];
end

function [elmposes,relvals,relactive] = solve(obj,newelmposes,newrelvals)
    
    obj.clear;
    
    obj.prepare(newelmposes,newrelvals);
    
    rels = obj.pgraph.rels;
    nelms = numel(obj.pgraph.elms);
    relvaloffset = nelms*obj.elmposedim;
    rellagrangeoffset = relvaloffset + numel(obj.targetrelvals);
    rellagrangeoffset_changed = rellagrangeoffset + numel(obj.targetrelvals);
    
%     rellagrangemult = ones(numel(obj.targetrelvals)+sum(not(isnan(obj.targetrelvals_changed))),1);
    rellagrangemult = [...
        ones(numel(obj.targetrelvals),1);...
        zeros(sum(not(isnan(obj.targetrelvals_changed))),1)];
    
%     obj.elmweights = ones(1,numel(obj.pgraph.elms));
    obj.elmweights(not(any(isnan(obj.targetelmposes),1))) = 1000;
    
    % solve for pattern state and langrange multipliers at the same time
    startstate = [...
        obj.oldelmposes(:);...
        cat(1,rels.value);...
        rellagrangemult];
    
    
%     % unconstrainted optimization
%     [beststate,val,exitflag,output,valgrad,~] = fminunc(...
%         @(x) obj.evalstate_nograd(x),startstate,...
%         optimoptions(@fminunc,...
%         'MaxFunEvals',100000,... % matlab default is 100*number of variables
%         'TolFun',1e-7,... % matlab default is 1e-6
%         'GradObj','off',...
%         'Diagnostics','on',...
%         'FunValCheck','on',...
%         'OutputFcn',@(x,optimValues,state) obj.outfun(x,optimValues,state),...
%         'DerivativeCheck','off'));
    
    
    % constrainted optimization
    statelagrinds_changed = [...
        find(not(isnan(obj.targetrelvals_changed)))+rellagrangeoffset;...
        rellagrangeoffset_changed+1:numel(startstate)];
    
    % constraints that the sum of lagrange multipliers for same
    % relationship should be 1
    linconMat_ineq = sparse(...
        reshape(repmat(1:size(statelagrinds_changed,2),2,1),[],1),...
        statelagrinds_changed(:),...
        ones(numel(statelagrinds_changed),1),...
        size(statelagrinds_changed,2),numel(startstate));
    linconRhs_ineq = ones(size(statelagrinds_changed,2),1);
    
    linconMat_eq = [];
    linconRhs_eq = [];
    
    % upper and lower bound [0,1] on lagrange multipliers
    lb = -inf(size(startstate));
    ub = inf(size(startstate));
    lb(rellagrangeoffset+1:end) = 0;
    ub(rellagrangeoffset+1:end) = 1;
    
    [beststate,bestcost,exitflag,output,~,valgrad,~] = fmincon(...
        @(x) obj.evalstate_nograd_cons(x),startstate,...
        linconMat_ineq,linconRhs_ineq,... % linear inequality constraints
        linconMat_eq,linconRhs_eq,... % linear equality constraints
        lb,ub,...
        [],...
        optimoptions(@fmincon,...
        'MaxFunEvals',100000,... % matlab default is 100*number of variables
        'TolFun',1e-7,... % matlab default is 1e-6
        'GradObj','off',...
        'Diagnostics','on',...
        'FunValCheck','on',...
        'OutputFcn',@(x,optimValues,state) obj.outfun(x,optimValues,state),...
        'DerivativeCheck','off'));
    
    % get outputs from state
    elmposes = reshape(beststate(1:relvaloffset),obj.elmposedim,nelms);
    relvals = beststate(relvaloffset+1 : rellagrangeoffset)';
    
    relactive = zeros(2,numel(obj.targetrelvals));
    relactive(1,:) = beststate(rellagrangeoffset+1:rellagrangeoffset_changed)';
    relactive(2,not(isnan(obj.targetrelvals_changed))) = beststate(rellagrangeoffset_changed+1:end)';
    
%     oldmask = relactive(1) > 0.5;
%     newmask = relactive(2) > 0.5;
    
    obj.solutionelmposes = {elmposes};
    obj.solutionrelvals = {relvals};
    obj.solutionrelactive = {relactive};
    obj.solutioncost = bestcost;
end

function setVisfunction(obj,func)
    obj.visfunc = func;
end

function stop = outfun(obj,state,optimValues,optimizerstateflag)
    disp(['*** iteration ',num2str(optimValues.iteration),':']);
    disp(['    objective function value: ',num2str(optimValues.fval)]);
%     disp('    gradient:');
%     disp(optimValues.gradient);
    
    if not(isempty(obj.visfunc))
        nelms = numel(obj.pgraph.elms);
        relvaloffset = nelms*obj.elmposedim;
        relactiveoffset = relvaloffset + numel(obj.targetrelvals);
        relactiveoffset_changed = relactiveoffset + numel(obj.targetrelvals);
        
        eposes = PatternSolver.affine2simpose([obj.pgraph.elms.pose2D]);
%         eposes(1:obj.elmposedim,:) = reshape(x(1:numel(obj.pgraph.elms)*obj.elmposedim),obj.elmposedim,[]);
        eposes(1:obj.elmposedim,:) = reshape(state(1:relvaloffset),obj.elmposedim,nelms);
        
        ractive = zeros(2,numel(obj.targetrelvals));
        ractive(1,:) = state(relactiveoffset+1:relactiveoffset_changed)';
        ractive(2,not(isnan(obj.targetrelvals_changed))) = state(relactiveoffset_changed+1:end)';
        
        disp('relactive: ');
        disp(ractive);
        
%         nrels = numel(obj.pgraph.rels);
%         ractive_old = obj.solutionrelactive(1:nrels);
%         ractive_new = zeros(1,nrels);
%         ractive_new(obj.changedrelinds) = obj.solutionrelactive(nrels+1:end);

        disp('groups:');
        statereloffset = numel(obj.targetelmposes);
        for i=1:numel(obj.relgrprelinds)
            if numel(obj.relgrprelinds{i}) > 1
                grprelvals = state(obj.relgrprelinds{i}+statereloffset);
                grpavg = mean(grprelvals);
                disp([num2str(grpavg),' : ',num2str(grprelvals'),' : maxdiff: ',num2str(max(grprelvals-grpavg))]);
            end
        end
        
        stop = obj.visfunc(obj,eposes,ractive);
    else
        stop = false;
    end
end

% for constrained optimization
function val = evalstate_nograd_cons(obj,state)
    state = state';
    val = 0;
    
    statereloffset = numel(obj.targetelmposes);
    statelagrangeoffset = statereloffset+numel(obj.targetrelvals);
    rellagrangemult = state(statelagrangeoffset+1:end);

    relvars = zeros(1,numel(numel(obj.targetrelvals)));
    
    for i=1:numel(obj.relfs)
        
        % evaluate all relationships functions of one type
        [v,var] = obj.relfs{i}(...
            reshape(state(obj.relfinput1inds{i}),size(obj.relfinput1inds{i})),...
            reshape(state(obj.relfinput2inds{i}),size(obj.relfinput2inds{i})));
        
        relvars(obj.relfoutputinds{i}-statereloffset) = var;
        
        residual = v-obj.targetrelvals(obj.relfrelinds{i});
        if obj.relfcircular(i)
            residual = smod(residual,-pi,pi);
        end
        val = val + (residual.^2 ./ var) * rellagrangemult(obj.relfrelinds{i})';
            
        changedrelmask = not(isnan(obj.targetrelvals_changed(obj.relfrelinds{i})));
        if any(changedrelmask)
            residual = v(changedrelmask)-obj.targetrelvals_changed(obj.relfrelinds{i}(changedrelmask));
            if obj.relfcircular(i)
                residual = smod(residual,-pi,pi);
            end
            val = val + (residual.^2 ./ var(changedrelmask)) * rellagrangemult(...
                obj.rellagrangeinds_changed(obj.relfrelinds{i}(changedrelmask)))'; % weighted mahalanobis distance
        end
        
%         % residual between relationships and target values (squared L2 norm)
%         activemask = not(isnan(obj.targetrelvals(obj.relfrelinds{i})));
%         if any(activemask)
% 
%             residual = v(activemask)-obj.targetrelvals(obj.relfrelinds{i}(activemask));
%             if obj.relfcircular(i)
%                 residual = smod(residual,-pi,pi);
%             end
%             val = val + (residual.^2 ./ var(activemask)) * rellagrangemult(obj.relfrelinds{i}(activemask))'; % weighted mahalanobis distance
%         end
        
        % residual between relationship values in the state and actual
        % relationship values (squared L2 norm)
        residual = v-state(obj.relfoutputinds{i});
        if obj.relfcircular(i)
            residual = smod(residual,-pi,pi);
        end
        val = val + sum(residual.^2 ./ var)*999999;
    end
    
    % residual between element poses and target poses (squared L2 norm)
%     obj.elmposevar = 0.05.^2;
    activemask = not(any(isnan(obj.targetelmposes),1));
    if any(activemask)
        poseresidual = obj.targetelmposes(1:obj.elmposedim,activemask) - reshape(...
            state(obj.elmposeinds(:,activemask)),...
            size(obj.elmposeinds(:,activemask)));
        poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)) = smod(...
            poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)),-pi,pi);
        val = val + (sum(poseresidual.^2,1) ./ obj.elmposevar) * obj.elmweights(:,activemask)';
    end
    
    % add global regularization terms (global quality measures for solution)
    % structure preservation (not too many inactive constraints)
%     val = val + (sum(rellagrangemult(:)) - numel(obj.targetrelvals))^2 * 0.05;
    val = val + (sum(rellagrangemult(1,:)) - numel(obj.targetrelvals))^2 * 0.05;
    
%     % local edits (not too many changed elements - currently the total magnitude of changes is used)
%     elmposediff = obj.oldelmposes(:,not(activemask)) - reshape(...
%             state(obj.elmposeinds(:,not(activemask))),...
%             size(obj.elmposeinds(:,not(activemask))));
%     val = val + sum(sum(elmposediff.^2,1) ./ obj.elmposevar);
% %     val = val + sum(1 ./ (1 + exp(-sqrt(sum(elmposediff.^2,1) ./ obj.elmposevar) ./ 2))) * 100;

    
    % how many element groups changed
    elmposediff = obj.oldelmposes - reshape(...
            state(obj.elmposeinds),size(obj.elmposeinds));
    for i=1:numel(obj.elmgrpelminds)
        if not(isempty(obj.elmgrpelminds{i})) && not(any(activemask(obj.elmgrpelminds{i})))
            val = val + mean(sum(elmposediff(:,obj.elmgrpelminds{i}).^2,1) ./ obj.elmposevar);
%             val = val + mean(1 ./ (1 + exp(-sqrt(sum(elmposediff.^2,1) ./ obj.elmposevar) ./ 2))) * 100;
        end
    end
    
    % for each group member: the difference to the average value of all
    % group members
    changedrelmask = not(isnan(obj.targetrelvals_changed));
    rellagrangemult_combined = rellagrangemult(1:numel(obj.targetrelvals));
    rellagrangemult_combined(changedrelmask) = ...
        rellagrangemult_combined(changedrelmask) + ...
        rellagrangemult(numel(obj.targetrelvals)+1:end);
    for i=1:numel(obj.relgrprelinds)
        if numel(obj.relgrprelinds{i}) > 1
            grprelvals = state(obj.relgrprelinds{i}+statereloffset);
            grpavg = mean(grprelvals);
            residual = grprelvals - grpavg;
            if obj.relgrpcircular(i)
                residual = smod(residual,-pi,pi);
            end
%             val = val + sum((residual).^2 ./ relvars(obj.relgrprelinds{i}))*9999;
%             val = val + sum((residual).^2 ./ relvars(obj.relgrprelinds{i}))*100;
            val = val + sum((residual).^2 ./ relvars(obj.relgrprelinds{i})) * mean(rellagrangemult_combined(obj.relgrprelinds{i})) * 100;
%             val = val + sum(((residual).^2 ./ relvars(obj.relgrprelinds{i})) .* rellagrangemult_combined(obj.relgrprelinds{i})) * 100;
        end
    end
    
    % cost of destroying groups:
    for i=1:numel(obj.relgrprelinds)
        if numel(obj.relgrprelinds{i}) > 1
            val = val + max(0,numel(obj.relgrprelinds{i}) - sum(rellagrangemult_combined(obj.relgrprelinds{i})))^2 * 100;
        end
    end
end

% function val = evalstate_nograd(obj,state)
%     
%     % todo: for relationships that need other properties of the elements
%     % other than their pose (e.g. distance to the boundary): need to move
%     % elements to the position given by the state and pass them to the
%     % relationship function (boundary vertex positions relative to the pose
%     % act like constants in the relationship function)
%     
%     state = state';
%     val = 0;
%     
%     rellagrangemult = state(numel(obj.targetelmposes)+numel(obj.targetrelvals)+1:end);
% 
%     for i=1:numel(obj.relfs)
%         
%         % evaluate all relationships functions of one type
%         [v,var] = obj.relfs{i}(...
%             reshape(state(obj.relfinput1inds{i}),size(obj.relfinput1inds{i})),...
%             reshape(state(obj.relfinput2inds{i}),size(obj.relfinput2inds{i})));
%         
%         
%         residual = v-obj.targetrelvals(obj.relfrelinds{i});
%         if obj.relfcircular(i)
%             residual = smod(residual,-pi,pi);
%         end
%         val = val + (residual.^2 ./ var) * rellagrangemult(obj.relfrelinds{i})';
%             
%         changedrelmask = not(isnan(obj.targetrelvals_changed(obj.relfrelinds{i})));
%         if any(changedrelmask)
%             residual = v(changedrelmask)-obj.targetrelvals_changed(obj.relfrelinds{i}(changedrelmask));
%             if obj.relfcircular(i)
%                 residual = smod(residual,-pi,pi);
%             end
%             val = val + (residual.^2 ./ var(changedrelmask)) * rellagrangemult(...
%                 obj.rellagrangeinds_changed(obj.relfrelinds{i}(changedrelmask)))'; % weighted mahalanobis distance
%         end
%         
% %         % residual between relationships and target values (squared L2 norm)
% %         activemask = not(isnan(obj.targetrelvals(obj.relfrelinds{i})));
% %         if any(activemask)
% % 
% %             residual = v(activemask)-obj.targetrelvals(obj.relfrelinds{i}(activemask));
% %             if obj.relfcircular(i)
% %                 residual = smod(residual,-pi,pi);
% %             end
% %             val = val + (residual.^2 ./ var(activemask)) * rellagrangemult(obj.relfrelinds{i}(activemask))'; % weighted mahalanobis distance
% %         end
%         
%         % residual between relationship values in the state and actual
%         % relationship values (squared L2 norm)
%         residual = v-state(obj.relfoutputinds{i});
%         if obj.relfcircular(i)
%             residual = smod(residual,-pi,pi);
%         end
%         val = val + sum(residual.^2 ./ var);
%     end
%     
%     % residual between element poses and target poses (squared L2 norm)
% %     obj.elmposevar = 0.05.^2;
%     activemask = not(any(isnan(obj.targetelmposes),1));
%     if any(activemask)
%         poseresidual = obj.targetelmposes(1:obj.elmposedim,activemask) - reshape(...
%             state(obj.elmposeinds(:,activemask)),...
%             size(obj.elmposeinds(:,activemask)));
%         poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)) = smod(...
%             poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)),-pi,pi);
%         val = val + (sum(poseresidual.^2,1) ./ obj.elmposevar) * obj.elmweights(:,activemask)';
%     end
%     
%     % add global regularization terms (global quality measures for solution)
%     % structure preservation (not too many inactive constraints)
% 	val = val + (sum(rellagrangemult) - numel(obj.targetrelvals))^2;
%     val = val + sum(max(0,rellagrangemult-1).^2).*999999999; % lagrange multiplier above 1
%     val = val + sum(min(0,rellagrangemult).^2).*999999999; % lagrange multiplier below 0
%     changedrelmask = not(isnan(obj.targetrelvals_changed));
%     val = val + sum(max(0,(rellangrangemult(changedrelmask) + rellagrangemult(numel(obj.targetrelvals)+1:end)) - 1).^2).*99999999; % sum of lagrange multipliers not larger than 1
%     % local edits (not too many changed elements - currently the total magnitude of changes is used)
%     elmposediff = obj.oldelmposes(:,not(activemask)) - reshape(...
%             state(obj.elmposeinds(:,not(activemask))),...
%             size(obj.elmposeinds(:,not(activemask))));
%     val = val + sum(sum(elmposediff.^2,1) ./ obj.elmposevar);
% end

% function grad = gradient(obj,state)
%     if isempty(obj.targetrelvals) && isempty(obj.elmtargetposes)
%         warning('Must set objective values before computing the gradient.');
%         return;
%     end
%     
%     if nargin < 2 || isempty(state)
%         % default is current pgraph state
%         state = [...
%             cat(1,obj.pgraph.elms.position);...
%             cat(1,obj.pgraph.rels.value)];
%     end
%     
%     grad = gradest(@(x) obj.evalstate_nograd(x),state);
% end
% 
% % separate gradients for the contribution of each element to the objective function
% % (individual contributions are summed up to get the objective function value,
% % so gradients are summed up to get objective function gradient as well)
% function jac = jacobian_separate(obj,state)
%     if isempty(obj.targetrelvals) && isempty(obj.elmtargetposes)
%         warning('Must set objective values before computing the gradient.');
%         return;
%     end
%     
%     if nargin < 2 || isempty(state)
%         % default is current pgraph state
%         state = [...
%             cat(1,obj.pgraph.elms.position);...
%             cat(1,obj.pgraph.rels.value)];
%     end
%     
%     jac = jacobianest(@(x) obj.evalstate_nograd_separate(x),state);    
% end
% 
% % separate gradients for each relationship function, put in a vector the
% % same size as the state
% function jac = reljacobian_separate(obj,state)
%     if isempty(obj.targetrelvals) && isempty(obj.elmtargetposes)
%         warning('Must set objective values before computing the gradient.');
%         return;
%     end
%     
%     if nargin < 2 || isempty(state)
%         % default is current pgraph state
%         state = [...
%             cat(1,obj.pgraph.elms.position);...
%             cat(1,obj.pgraph.rels.value)];
%     end
%     
%     jac = jacobianest(@(x) obj.evalrels_nograd_separate(x),state);    
% end

% function [val,grad] = evalstate(obj,state)
%     
%     % todo: for relationships that need other properties of the elements
%     % other than their pose (e.g. distance to the boundary): need to move
%     % elements to the position given by the state and pass them to the
%     % relationship function (boundary vertex positions relative to the pose
%     % act like constants in the relationship function)
%     
%     state = state';
%     
%     rellagrangemult = state(end-numel(obj.targetrelvals)+1:end);
%     
%     
%     
%     val = 0;
%     grad = zeros(1,numel(state));
%     
%     for i=1:numel(obj.relfs)
%         
%         
%         
%         
%         % evaluate all relationships functions of one type
%         [v,var,g] = obj.relfs{i}(...
%             reshape(state(obj.relfinput1inds{i}),size(obj.relfinput1inds{i})),...
%             reshape(state(obj.relfinput2inds{i}),size(obj.relfinput2inds{i})));
%         
%         % residual between relationships and target values (squared L2 norm)
%         activemask = not(isnan(obj.targetrelvals{i}(obj.relfrelinds{i})));
%         if any(activemask)
%             residual = v(activemask)-obj.targetrelvals{i}(obj.relfrelinds{i}(activemask));
%             if obj.relfcircular(i)
%                 residual = smod(residual,-pi,pi);
%             end
%             val = val + (residual.^2 ./ var(activemask)) * rellagrangemult(obj.relfrelinds{i}(activemask))'; % weighted mahalanobis distance
%             
%             % gradient w*f(x)^2 is w*2*f(x) * g where g is df/dx
%             relinputdim = size(obj.relfinput1inds{i},1);
%             g = bsxfun(@times,2.*(residual./var(activemask))*rellagrangemult(obj.relfrelinds{i}(activemask)),g(1:relinputdim,:,:));
%             grad(obj.relfinput1inds{i}(:,activemask)) = reshape(...
%                 grad(obj.relfinput1inds{i}(:,activemask)),...
%                 size(obj.relfinput1inds{i}(:,activemask))) ...
%                 + g(:,:,1);
%             grad(obj.relfinput2inds{i}(:,activemask)) = reshape(...
%                 grad(obj.relfinput2inds{i}(:,activemask)),...
%                 size(obj.relfinput2inds{i}(:,activemask))) ...
%                 + g(:,:,2);
%         end
%             
%         % residual between relationship values in the state and actual
%         % relationship values (squared L2 norm)
%         residual = v-state(obj.relfoutputinds{i});
%         if obj.relfcircular(i)
%             residual = smod(residual,-pi,pi);
%         end
%         val = val + (residual.^2 ./ var) * rellagrangemult(obj.relfrelinds{i})';
% 
%         warning('todo: gradient not fully computed.');
% %         % for gradient:
% %         grad(obj.relfoutputinds{i}) = ...
%     end
%     
%     % residual between element poses and target poses (squared L2 norm)
%     activemask = not(any(isnan(obj.targetelmposes),1));
%     if any(activemask)
%         elmconstraintvar = 0.05.^2;
%         poseresidual = obj.targetelmposes(1:obj.elmposeind,activemask) - reshape(...
%             state(obj.elmposeinds(:,activemask)),...
%             size(obj.elmposeinds(:,activemask)));
%         poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)) = smod(...
%             poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)),-pi,pi);
%         val = val + (sum(poseresidual.^2,1) ./ elmconstraintvar) * obj.elmweights(:,activemask)';
%         
%         % gradient of distance length one, but since it would be
%         % 2*totargetdist*(totarget/totargetdist), totargetdist cancels out
%         g = bsxfun(@times,2.*obj.elmweights(:,activemask).*(1./elmconstraintvar),-poseresidual);
%         grad(obj.elmposeinds(:,activemask)) = reshape(...
%             grad(obj.elmposeinds(:,activemask)),...
%             size(obj.elmposeinds(:,activemask))) ...
%             + g;
%     end
%     
%     % add global regularization terms (global quality measures for solution)
%     % structure preservation (not too many inactive constraints)
% 	val = val + numel(rellagrangemult) - rellagrangemult;
%     % local edits (not too many changed elements - currently the total magnitude of changes is used)
%     val = val + sum((obj.oldelmposes(:,not(activemask)) - reshape(...
%             state(obj.elmposeinds(:,not(activemask))),...
%             size(obj.elmposeinds(:,not(activemask))))).^2,1) ./ obj.elmposevar;
%         
%     warning('todo: gradient not fully computed.');
%     % also compute gradient of global regularization terms
%     
%     grad = grad';
% end

% % contribution of each element and each relationship to the objective
% % function
% function val = evalstate_nograd_separate(obj,state)
%     
%     % todo: for relationships that need other properties of the elements
%     % other than their pose (e.g. distance to the boundary): need to move
%     % elements to the position given by the state and pass them to the
%     % relationship function (boundary vertex positions relative to the pose
%     % act like constants in the relationship function)
%     
%     state = state';
%     val = zeros(size(state));
% 
%     for i=1:numel(obj.relfs)
%         
%         % evaluate all relationships functions of one type
%         [v,var] = obj.relfs{i}(...
%             reshape(state(obj.relfinput1inds{i}),size(obj.relfinput1inds{i})),...
%             reshape(state(obj.relfinput2inds{i}),size(obj.relfinput2inds{i})));
%         
%         % residual between relationships and target values (squared L2 norm)
%         activemask = not(isnan(obj.targetrelvals(obj.relfrelinds{i})));
%         if any(activemask)
% 
%             residual = v(activemask)-obj.targetrelvals(obj.relfrelinds{i}(activemask));
%             if obj.relfcircular(i)
%                 residual = smod(residual,-pi,pi);
%             end
%             val(obj.relfoutputinds{i}(activemask)) = val(obj.relfoutputinds{i}(activemask)) + ...
%                 (residual.^2 ./ var(activemask)) * obj.relweights(obj.relfrelinds{i}(activemask))'; % weighted mahalanobis distance
%         end
%         
%         % residual between relationship values in the state and actual
%         % relationship values (squared L2 norm)
%         residual = v-state(obj.relfoutputinds{i});
%         if obj.relfcircular(i)
%             residual = smod(residual,-pi,pi);
%         end
%         val(obj.relfoutputinds{i}) = val(obj.relfoutputinds{i}) + ...
%             (residual.^2 ./ var) * obj.relweights(obj.relfrelinds{i})';
%     end
%     
%     % residual between element poses and target poses (squared L2 norm)
%     activemask = not(any(isnan(obj.targetelmposes),1));
%     if any(activemask)
%         elmconstraintvar = 0.05.^2;
%         poseresidual = obj.targetelmposes(1:obj.elmposedim,activemask) - reshape(...
%             state(obj.elmposeinds(:,activemask)),...
%             size(obj.elmposeinds(:,activemask)));
%         poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)) = smod(...
%             poseresidual(obj.elmposecircular(1:obj.elmposedim,activemask)),-pi,pi);
%         val(obj.elmposeinds(:,activemask)) = reshape(...
%             val(obj.elmposeinds(:,activemask)),...
%             size(obj.elmposeinds(:,activemask))) + ...
%             (sum(poseresidual.^2,1) ./ elmconstraintvar) * obj.elmweights(:,activemask)';
%     end
%     
%     val = val';
% end
% 
% % contribution of each element and each relationship to the objective
% % function
% function val = evalrels_nograd_separate(obj,state)
%     
%     % todo: for relationships that need other properties of the elements
%     % other than their pose (e.g. distance to the boundary): need to move
%     % elements to the position given by the state and pass them to the
%     % relationship function (boundary vertex positions relative to the pose
%     % act like constants in the relationship function)
%     
%     state = state';
%     val = zeros(size(state));
%     
%     for i=1:numel(obj.relfs)
%         [v,var] = obj.relfs{i}(...
%             reshape(state(obj.relfinput1inds{i}),size(obj.relfinput1inds{i})),...
%             reshape(state(obj.relfinput2inds{i}),size(obj.relfinput2inds{i})));
%         
%         val(obj.relfoutputinds{i}) = val(obj.relfoutputinds{i}) + ...
%             (v ./ sqrt(var)) .* obj.relweights(obj.relfrelinds{i});
%     end
%     
%     val = val';
% end

end

end
